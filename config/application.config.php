<?php

return array(
    'modules' => array(
        'Application',
        'Admin',
        'Usuarios',
        'Avisos',
        'Contacto',
        'Vehiculos',
        'Servicios',
        'Personal',
        'Brigadas',
        'Instalaciones',
        'Listadeprecios',
        'Riat',
        'Ddescontar',
        'Auditor',
        'Ans',
        'Rim',
		'DOMPDFModule',		
    ),
    'module_listener_options' => array(
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php',
        ),
        'module_paths' => array(
            './module',
            './vendor',
        ),
    ),
);
