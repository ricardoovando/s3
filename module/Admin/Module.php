<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Config\Config;
use Zend\Mvc\MvcEvent;

class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
    }

    public function initAuth(MvcEvent $e) {
        
        $application = $e->getApplication();
        $matches = $e->getRouteMatch();
        $controller = $matches->getParam('controller');
        $action = $matches->getParam('action');
        
        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index','autenticar','b2a553d36975dda502e24014d2161a27'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index, autenticar, b2a553d36975dda502e24014d2161a27.
                        return;
                    }
                    break;    
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
                }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('admin/default', array('controller' => 'login'));
            }
        }
    }

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniAdmin', function ($services) {
            $reader = new Ini();
            $data = $reader->fromFile(__DIR__ . '/config/config.ini');
            return new Config($data);
        });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Admin\Model\Login' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            return new \Admin\Model\Login($dbAdapter);
        },
            ),
        );
    }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Admin\Controller\Login' => function ($sm) {

                    $locator = $sm->getServiceLocator();

                    $config = $locator->get('ConfigIniAdmin');
                    $logger = $locator->get('Zend\Log\StartSession');
                    $login = $locator->get('Admin\Model\Login');
                    $usuarioDao = $locator->get('Usuarios\Model\UsuarioDao');

                    $controller = new \Admin\Controller\LoginController($config);
                    $controller->setLogin($login);
                    $controller->setUsuarioDao($usuarioDao);
                    $controller->setLogger($logger);

                    return $controller;
                },
                'Admin\Controller\Usuario' => function ($sm) {

                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAdmin');

                    $usuarioDao = $locator->get('Usuarios\Model\UsuarioDao');

                    $controller = new \Admin\Controller\UsuarioController($config);
                    $controller->setUsuarioDao($usuarioDao);

                    return $controller;
                }
            )
        );
    }

}
