<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    private $login;
    private $config;

    public function getConfig() {
        if (!$this->config) {
            $sm = $this->getServiceLocator();
            $this->config = $sm->get('ConfigIniAdmin');
        }
        return $this->config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function indexAction() {
    	
        return $this->forward()->dispatch('Admin\Controller\Login', array('action' => 'index'));
		
    }

    public function menuAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        return new ViewModel();
        
    }

}
