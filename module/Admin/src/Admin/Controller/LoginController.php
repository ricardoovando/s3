<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Admin\Form\Login;
use Admin\Form\LoginValidator;
use Admin\Form\b55c711b1be9bc03d671c41b1a2306d1;
use Admin\Form\b55c711b1be9bc03d671c41b1a2306d1Validator;

use Admin\Model\Login as LoginService;
use Usuarios\Model\Entity\Usuario;


class LoginController extends AbstractActionController {

    private $login;
    private $config;
    private $logger;
    private $email;
    private $clave;
    
    function __construct($config = null) {
        $this->config = $config;
    }

    private function getForm() {
        return new Login();
    }

    private function getCambio() {
        $form = new b55c711b1be9bc03d671c41b1a2306d1();
        $form->get('id_us')->setValue($this->login->getIdentity()->id);
        return $form;
    }

    public function setLogin(LoginService $login) {
        $this->login = $login;
    }

    public function setUsuarioDao($usuarioDao) {
        $this->usuarioDao = $usuarioDao;
    }

    public function getUsuarioDao() {   
        return $this->usuarioDao;
    }

    public function setLogger($logger){
        $this->logger = $logger;
    }
    
    public function getLogger(){
        return $this->logger;
    }
    
    /*
     * 
     * Se indica cual es la ip real del Usuario 
     * 
     * 
     */
    public function get_real_ip()
    {
 
        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
            return $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
            return $_SERVER["REMOTE_ADDR"];
        }
 
    }
    
    
    public function indexAction() {

        if ($this->login->isLoggedIn()) {
        	
            return $this->forward()->dispatch('Admin\Controller\Index', array('action' => 'menu'));
			
        }

        return new ViewModel(array('titulo' => 'Sistema de gestión técnica de información - S3 -', 'form' => $this->getForm()));
    }

    public function autenticarAction() {
        
        if(!$this->getRequest()->isPost())
            return $this->forward()->dispatch('Admin\Controller\Login', array('action' => 'index'));
        

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        $form = $this->getForm();
        $form->setInputFilter(new LoginValidator());
        $form->setData($postParams);

        if (!$form->isValid()) {
            $msg = '';
            foreach ($form->getMessages() as $value) {
                foreach ($value as $v) {
                    $msg = $msg . $v . ' | ';
                }
            }

            $this->getLogger()->err("Error de validacion de datos : " . $msg . " IP: " . $this->get_real_ip());

            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('titulo' => 'Error en el Formulario', 'form' => $form));
            $modelView->setTemplate('admin/login/index');
            return $modelView;
        }


        // Obtenemos los parámetros del formulario es similar a $_POST
        $values = $form->getData();
 
        if (!empty($values["5ac4c962a1220b30c26e994b0ecfa755b4e0389f"])) {
            $this->email = trim($values["5ac4c962a1220b30c26e994b0ecfa755b4e0389f"]);
        } else {
            $this->login->logout();
            exit;
        }


        if (!empty($values["5326ee20f69411ffc6d821a9bdb513832e3409a0"])) {
            $this->clave = trim($values["5326ee20f69411ffc6d821a9bdb513832e3409a0"]);
        } else {
            $this->login->logout();
            exit;
        }


        try {
            
            $this->login->setMessage($this->config->texto->admin->login->notIdentity, LoginService::NOT_IDENTITY);
            $this->login->setMessage($this->config->texto->admin->login->invalidCredential, LoginService::INVALID_CREDENTIAL);
            $this->login->setMessage($this->config->texto->admin->login->invalidLogin, LoginService::INVALID_LOGIN);
            

/*echo $this->email;

echo $this->clave;

exit;*/

            $this->login->logout();
            $this->login->login($this->email,$this->clave);
   
   
           /*
            * 
            * Se registra en el log que el login fue correcto
            *              
            */
            
            $this->getLogger()->info( "Login : " . $this->email . " IP: " . $this->get_real_ip() );
            
            $this->layout()->mensaje = $this->config->texto->admin->login->correcto;
            
            $this->layout()->colorMensaje = "green";

            $this->layout()->usuario = $this->login->getIdentity();

            
//            $view = new JsonModel(array($this->layout()->usuario));
//            
//            echo '<pre>';
//            var_dump($view);
//            exit;
            
            //return $this->forward()->dispatch('Admin\Controller\Index', array('action' => 'menu'));

            return $this->redirect()->toRoute('home');
			
            
        } catch (\Exception $e) {

            $this->getLogger()->err($e->getMessage() . ' Cuenta : ' . $this->email .' Clave : '. $this->clave  . " IP: " . $this->get_real_ip() );
            
            $this->layout()->mensaje = $e->getMessage();
            
            return $this->forward()->dispatch('Admin\Controller\Login', array('action' => 'index'));

       }
    }
    
    
    
    /* Sistema de autentificacion para sitio movil S3 monofasico */
    
    public function b2a553d36975dda502e24014d2161a27Action() {
        
        $uri_autorizada = 'http://movil.medicions3.com/newhtmlAjax.html';
        
//        if(!$_SERVER['HTTP_REFERER']==$uri_autorizada){
//              $this->login->logout();
//              $view = new ViewModel(array(
//                'callback' => 'ERROR 1'
//              ));
//            $view->setTerminal(true);
//            return $view;
//           }

        
         if(!$this->getRequest()->isGet()){ 
            $this->login->logout(); 
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Solo Get" })',
              ));
            $view->setTerminal(true);
            return $view;
          }
        

        // Obtenemos los parámetros del formulario es similar a $_POST
        $getParams = $this->request->getQuery();
                
        
        /*
         *
         *  Validaciones filtros para seguridad 
         * 
         */
        
        if(empty($getParams['5ac4c962a1220b30c26e994b0ecfa755b4e0389f'])){
            $this->login->logout();
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Falta un Parametros" })',
              ));
            $view->setTerminal(true);
            return $view;
          }
          
        if(empty($getParams['5326ee20f69411ffc6d821a9bdb513832e3409a0'])){
            $this->login->logout();
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Falta un Parametros" })',
              ));
            $view->setTerminal(true);
            return $view;
          }
          
       
         if(empty( $getParams['callback'])){
            $this->login->logout();
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Falta un Parametros" })',
              ));
            $view->setTerminal(true);
            return $view;
          }  
        
        $this->email = trim($getParams['5ac4c962a1220b30c26e994b0ecfa755b4e0389f']);
        $this->clave = trim($getParams['5326ee20f69411ffc6d821a9bdb513832e3409a0']);        
        

        /*Email*/
        if(!(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$this->email))){
            $this->login->logout();
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Formato de Correo no es correcto" })',
              ));
            $view->setTerminal(true);
            return $view;
          }

                 
        $validatorEmailAddress = new \Zend\Validator\EmailAddress();
        if (!$validatorEmailAddress->isValid($this->email)){
            $this->login->logout();
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"Formato de Correo no es correcto" })',
              ));
            $view->setTerminal(true);
            return $view;
          }


       
       /*Largo Email*/
       $validatorStringLength = new \Zend\Validator\StringLength(array('min' => 10, 'max' => 40));
        
       if(!$validatorStringLength->isValid($this->email)){
            $this->login->logout(); 
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"el largo del correo no es permitido" })',
              ));
            $view->setTerminal(true);
            return $view;
          }
               
          
        /*Largo PassWord*/
        $validatorStringLength = new \Zend\Validator\StringLength(array('min' => 4, 'max' => 20));
          
        if(!$validatorStringLength->isValid($this->clave)){
            $this->login->logout(); 
            $view = new ViewModel(array(
                'callback' => $getParams['callback'] . '({ "success":false , "error":"el largo de la password no es permitido" })',
              ));
            $view->setTerminal(true);
            return $view;
          }
          
          
       
//        $validatorAlnum = new \Zend\I18n\Validator\Alnum();
//        if ($validator->isValid('Abcd12')) {
//
//           
//             
//            }  
        
        
        
        $this->email = strip_tags($this->email);
        $this->clave = strip_tags($this->clave);
        $getParams['callback'] = strip_tags($getParams['callback']);
                    

        try {
                        
            $this->login->setMessage($this->config->texto->admin->login->notIdentity, LoginService::NOT_IDENTITY);
            $this->login->setMessage($this->config->texto->admin->login->invalidCredential, LoginService::INVALID_CREDENTIAL);
            $this->login->setMessage($this->config->texto->admin->login->invalidLogin, LoginService::INVALID_LOGIN);
            
            $this->login->logout();
            
            $this->login->login($this->email,$this->clave);
            
            /* Se registra en el log que el login fue correcto */
            
            $this->getLogger()->info( "Login Movil: " . $this->email . " IP: " . $this->get_real_ip() );
            
            
           /*
            * Retornamos a la Vista con un OK
            */
           $view = new ViewModel(array(
               
               'callback' => $getParams['callback'] . '({ "success":true , "tecnico":'  . json_encode($this->login->getIdentity()) . '})',
               
             ));

           $view->setTerminal(true);
           
           return $view;
        
            
            
        } catch (\Exception $e) {

            
           $this->getLogger()->err($e->getMessage() . ' Cuenta Movil : ' . $this->email .' Clave : '. $this->clave  . " IP: " . $this->get_real_ip() );
            
           /*
            * Retornamos a la Vista con un OK
           */
           $view = new ViewModel(array(
               
               'callback' => $getParams['callback'] . '({ "success":false , "error":'  . json_encode($e->getMessage()) . '})',
               
             ));

           $view->setTerminal(true);
           
           return $view;
           
            
       }
    }
    
    
    /*FIN*/
    
    
    /*
     * Se ejecuta accion para salir del sistema. 
     */
    public function logoutAction() {
        
      $this->getLogger()->info( "Logout : " . $this->login->getIdentity()->email . " IP: " . $this->get_real_ip() );
      $this->login->logout();
      $this->layout()->mensaje = $this->config->texto->admin->login->cerrar;
      $this->layout()->colorMensaje = "green";
      //return $this->forward()->dispatch('Admin\Controller\Login', array('action' => 'index'));
      return $this->redirect()->toRoute('home');
          
    }
    
    
    /*
     * Sistema puede cambiar la contraseña formulario de cambio action que ejecuta  
     */
    public function b55c711b1be9bc03d671c41b1a2306d1Action() {

        $this->layout()->usuario = $this->login->getIdentity();

        if (!$this->getRequest()->isPost()) {
            return new ViewModel(array('titulo' => 'Cambiar contraseña', 'form' => $this->getCambio()));
          }

        //Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        $form = $this->getCambio();
        $form->setInputFilter(new b55c711b1be9bc03d671c41b1a2306d1Validator());
        $form->setData($postParams);

        if (!$form->isValid()) {
            
            //Falla la validación; volvemos a generar el formulario
            
            $this->getLogger()->info( "Error al cambiar contraseña ");
            
            return new ViewModel(array('titulo' => 'Cambiar contraseña', 'form' => $form));
            
          }

        $values = $form->getData();
        
        /*Comprobar Usuario y Clave Actual*/
        $data = array();
        $data['id'] = $values['id_us'];
        $data['clave'] = MD5($values['password_actual']);
        
        $usuario = new Usuario();
        $usuario->exchangeArray($data);

        $result = $this->getUsuarioDao()->comprobarusuariopass($usuario);
        
           if($result->count()>0){

                /*Si el resultado es exitoso modifico clave actual por nueva clave*/
                $data = array();
                $data['id'] = $values['id_us'];
                $data['clave'] = MD5($values['password_nueva']);
                $usuario = new Usuario();
                $usuario->exchangeArray($data);

                $this->getLogger()->info( "Cambio contraseña : ");
                
                $this->getUsuarioDao()->updatepass($usuario);
               
                return new ViewModel(array('titulo' => 'Se ha cambio la contraseña correctamente', 'form' => $this->getCambio()));

              }
            
            return new ViewModel(array('titulo' => 'La contraseña actual ingresada no coincide', 'form' => $form));
        
        }

}
