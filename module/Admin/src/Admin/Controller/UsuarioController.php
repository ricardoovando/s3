<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Usuarios\Model\Entity\Usuario;

use Admin\Form\Registro as UsuarioForm;
use Admin\Form\RegistroValidator;


class UsuarioController extends AbstractActionController {

    private $usuarioDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }
    
    public function setUsuarioDao($usuarioDao) {
        $this->usuarioDao = $usuarioDao;
    }

    public function getUsuarioDao() {
        return $this->usuarioDao;
    }

    public function getConfig() {
        return $this->config;
    }

    public function indexAction() {
        return $this->forward()->dispatch('Admin\Controller\Usuario', array('action' => 'listar'));
    }

    public function listarAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
         
        return new ViewModel(array('listaUsuario' => $this->getUsuarioDao()->obtenerTodos(),
                    'titulo' => $this->config['titulo']['admin']['usuario']['listado']));
    }

    public function crearAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $form = $this->getForm();
        return new ViewModel(array('titulo' => $this->config['titulo']['admin']['usuario']['crear'], 'form' => $form));
    }

    public function editarAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
        }

        $form = $this->getForm();

        $usuario = $this->getUsuarioDao()->obtenerPorId($id);

        $form->bind($usuario);
        $form->get('send')->setAttribute('value', 'Editar');

        $modelView = new ViewModel(array('title' => $this->config['titulo']['admin']['usuario']['editar'], 'form' => $form));
        $modelView->setTemplate('admin/usuario/crear');
        return $modelView;
    }

    public function registrarAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getForm();
        $form->setInputFilter(new RegistroValidator());

        $form->setData($postParams);

        if (!$form->isValid()) {
            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('titulo' => $this->config['titulo']['admin']['usuario']['valida'], 'form' => $form));
            $modelView->setTemplate('admin/usuario/crear');
            return $modelView;
        }

        $values = $form->getData();

        $usuario = new Usuario();
        $usuario->exchangeArray($values);

        $this->getUsuarioDao()->guardar($usuario);
        return $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
    }

    public function eliminarAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
        }
        $usuario = new Usuario();
        $usuario->setId($id);

        $this->getUsuarioDao()->eliminar($usuario);
        return $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
    }

    private function getForm() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        return new UsuarioForm();
    }

}
