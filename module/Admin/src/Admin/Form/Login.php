<?php



use Zend\Captcha\AdapterInterface;

use Zend\Captcha;

use Zend\Form\Element;

namespace Admin\Form;
use Zend\Form\Form;

class Login extends Form {
    
    
     protected $captcha;

    public function setCaptcha(CaptchaAdapter $captcha)
    {
        $this->captcha = $captcha;
    }

    

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'type' => 'Zend\Form\Element\Email',
            'name' => '5ac4c962a1220b30c26e994b0ecfa755b4e0389f',
            'attributes' => array(
                'placeholder' => 'Correo electrónico',
                'spellcheck' => 'false',
               )
            ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => '5326ee20f69411ffc6d821a9bdb513832e3409a0',
            'attributes' => array(
                'placeholder' => 'Contraseña',
                'spellcheck' => 'false',
               )
            ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' =>  'submit',
                'value' => 'Iniciar Sesión',
                'class' => 'btn btn-primary',
                'style' => 'width:220px;height:30px;',
            ),
        ));
        
        
//       $this->add(array(
//            'type' => 'Zend\Form\Element\Captcha',
//            'name' => 'captcha',
//            'options' => array(
//                'label' => 'Please verify you are human.',
//                'captcha' => $this->captcha,
//            ),
//        ));

       
       $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));
        
        
        
    }

}

//deja el boton como el de gmail de inicio de sesion
//background-color:#4D90FE;background-image: -moz-linear-gradient(center top , #4D90FE, #4787ED);