<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\NotEmpty;
use Zend\Validator\EmailAddress;

class LoginValidator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => false,
        'messages' => array(
            Alnum::INVALID => "Tipo inválido dado. Cadena, un entero o flotante esperada",
            Alnum::NOT_ALNUM => "La entrada contiene caracteres que no son alfabéticos y sin dígitos",
            Alnum::STRING_EMPTY => "La entrada es una cadena vacía",
        )
    );
    protected $opcionesStringLength = array(
        'min' => 4,
        'max' => 15,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener tener al menos 4 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 15 caracteres",
        )
    );
    protected $optionesNotEmpty = array(
        'messages' => array(
            NotEmpty::IS_EMPTY => "Se requiere de valor y no puede estar vacío",
            NotEmpty::INVALID => "Tipo inválido dado. Cadena, entero, flotante, booleano o matriz esperada",
        ),
    );
    protected $optionesEmailAddress = array(
        'messages' => array(
            EmailAddress::INVALID => "Tipo inválido dado. cadena esperada",
            EmailAddress::INVALID_FORMAT => "La entrada no es una dirección de correo electrónico válida. Utilice el formato básico local-part@hostname",
            EmailAddress::INVALID_HOSTNAME => "'%hostname%' no es un nombre de host válido para la dirección de correo electrónico",
            EmailAddress::INVALID_MX_RECORD => "'%hostname%' no parece tener ningún MX válido o registros A para la dirección de correo electrónico",
            EmailAddress::INVALID_SEGMENT => "'%hostname%' no está en un segmento de red enrutable. La dirección de correo electrónico no debe ser resuelto desde la red pública",
            EmailAddress::DOT_ATOM => "'%localPart%' no puede ser comparada con formato de punto-átomo",
            EmailAddress::QUOTED_STRING => "'%localPart%' no puede ser comparada con formato de cadena entre comillas",
            EmailAddress::INVALID_LOCAL_PART => "'%localPart%' no es una parte local válida para la dirección de correo electrónico",
            EmailAddress::LENGTH_EXCEEDED => "La entrada supera la longitud permitida",
        ),
    );

    public function __construct() {

        /*
         * para usuario se pide el mail de la empresa @tecnet
         * 
         */
        $this->add(
                array(
                    'name' => '5ac4c962a1220b30c26e994b0ecfa755b4e0389f',
                    'required' => true,
                    'filters' => array(
//                        array('name' => 'StringToLower'),
//                        array('name' => 'StringTrim'),
//                        array('name' => 'StripTags'),
//                        array('name' => 'StripNewlines'),
//                        array('name' => 'HtmlEntities'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'EmailAddress',
                            'options' => $this->optionesEmailAddress,
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
                    ),
                )
        );

        /*
         * 
         * este es el campo de la contraseña 
         * 
         */
        $this->add(
                array(
                    'name' => '5326ee20f69411ffc6d821a9bdb513832e3409a0',
                    'required' => true,
                    'filters' => array(
                        //array('name' => 'StringToLower'),
                        array('name' => 'StringTrim'),
                        array('name' => 'StripTags'),
                        array('name' => 'StripNewlines'),
                        array('name' => 'HtmlEntities'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
                    ),
                )
        );
    }

}

