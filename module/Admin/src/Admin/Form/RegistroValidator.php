<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;

class RegistroValidator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => false,
        'messages' => array(
            'notAlnum' => "El valor no es alfanúmerico",
        )
    );
    protected $opcionesStringLength = array(
        'min' => 4,
        'max' => 8,
        'messages' => array(
            StringLength::TOO_SHORT => "El campo debe tener tener al menos 4 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 8 caracteres",
        )
    );

    public function __construct() {

        $this->add(
                array(
                    'name' => 'nombre',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                    ),
                )
        );
        $this->add(
                array(
                    'name' => 'apellido',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum
                        ),
                    ),
                )
        );
        $this->add(
                array(
                    'name' => 'email',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'EmailAddress',
                        ),
                    ),
                )
        );

        $this->add(
                array(
                    'name' => 'clave',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                    ),
                )
        );

        $confirmarPassword = new Input('confirmarPassword');
        $confirmarPassword->setRequired(true);
        $confirmarPassword->getValidatorChain()
                ->addValidator(new StringLength($this->opcionesStringLength))
                ->addValidator(new Alnum($this->opcionesAlnum))
                ->addValidator(new Identical(
                                array(
                                    'token' => 'clave',
                                    'messages' => array(
                                        'notSame' => "Las contraseñas no coinciden, por favor intente de nuevo",
                                    )
                                )
                ));

        $this->add($confirmarPassword);
    }

}

