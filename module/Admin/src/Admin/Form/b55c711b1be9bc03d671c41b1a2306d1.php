<?php

namespace Admin\Form;

use Zend\Form\Form;

class b55c711b1be9bc03d671c41b1a2306d1 extends Form {

    public function __construct($name = null) {
        parent::__construct('b55c711b1be9bc03d671c41b1a2306d1');

        $this->add(array(
            'name' => 'id_us',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password_actual',
            'options' => array(
                'label' => 'Contraseña actual : ',
            ),
            'attributes' => array(
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password_nueva',
            'options' => array(
                'label' => 'Nueva contraseña : ',
            ),
            'attributes' => array(
                'required' => 'required',
            ),
        ));

        
                
        // Crear y configurar el elemento confirmarPassword:
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirmar_password',
            'options' => array(
                'label' => 'Confirmar contraseña : ',
            ),
            'attributes' => array(
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cambiar contraseña',
                'class' => 'btn btn-primary',
                'style' => 'width:220px;height:30px;',
            ),
        ));
        
    }

  }

