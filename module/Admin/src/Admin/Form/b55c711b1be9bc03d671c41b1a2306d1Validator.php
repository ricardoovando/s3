<?php

namespace Admin\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;

class b55c711b1be9bc03d671c41b1a2306d1Validator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => false,
        'messages' => array(
            'notAlnum' => "El valor no es alfanúmerico",
        )
    );
    protected $opcionesStringLength = array(
        'min' => 5,
        'max' => 15,
        'messages' => array(
            StringLength::TOO_SHORT => "El campo debe tener al menos 5 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 15 caracteres",
        )
    );

    public function __construct() {

        $this->add(
                array(
                    'name' => 'password_actual',
                    'required' => true,
                    'validators' => array(
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                    ),
                )
        );

        $this->add(
                array(
                    'name' => 'password_nueva',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                    ),
                )
        );

        $confirmarPassword = new Input('confirmar_password');
        $confirmarPassword->setRequired(true);
        $confirmarPassword->getValidatorChain()
                ->addValidator(new StringLength($this->opcionesStringLength))
                ->addValidator(new Alnum($this->opcionesAlnum))
                ->addValidator(new Identical(
                                array(
                                    'token' => 'password_nueva',
                                    'messages' => array(
                                        'notSame' => "Las contraseñas no coinciden, por favor intente de nuevo",
                                    )
                                )
                ));

        $this->add($confirmarPassword);
    }

}

