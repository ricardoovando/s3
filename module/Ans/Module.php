<?php

namespace Ans;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


use Ans\Model\Entity\ProductividadBrigadas;
use Ans\Model\Dao\ProductividadBrigadasDao;

use Ans\Model\Entity\EfectividadBrigadas;
use Ans\Model\Dao\EfectividadBrigadasDao;

use Ans\Model\Entity\TasaViajesPerdidosCausaTecnet;
use Ans\Model\Dao\TasaViajesPerdidosCausaTecnetDao;

use Ans\Model\Entity\ViewProductividadBrigadas;
use Ans\Model\Dao\ViewProductividadBrigadasDao;

use Ans\Model\Entity\ViewEfectividadBrigadas;
use Ans\Model\Dao\ViewEfectividadBrigadasDao;
                
use Ans\Model\Entity\ViewTasaViajesPerdidosCausaTecnet;
use Ans\Model\Dao\ViewTasaViajesPerdidosCausaTecnetDao;


class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, ServiceProviderInterface, ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
    }

    public function initAuth(MvcEvent $e) {
        
        $application = $e->getApplication();
        
        $matches = $e->getRouteMatch();
        
        $controller = $matches->getParam('controller');
        
        $action = $matches->getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index', 'autenticar'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index y autenticar.
                        return;
                    }
                    break;
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
            }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('admin/default', array('controller' => 'login'));
            }
        }
    }

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniAns', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return $data;
                });
             }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Ans\Model\ProductividadBrigadasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayProductividadBrigadas');
                    $dao = new ProductividadBrigadasDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayProductividadBrigadas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new ProductividadBrigadas() );
                    return new TableGateway('productividad_brigadas', $dbAdapter, null, $resultSetPrototype);
                  },
                 'Ans\Model\EfectividadBrigadasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayEfectividadBrigadas');
                    $dao = new EfectividadBrigadasDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayEfectividadBrigadas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new EfectividadBrigadas() );
                    return new TableGateway('efectividad_brigadas', $dbAdapter, null, $resultSetPrototype);
                  },
                 'Ans\Model\TasaViajesPerdidosCausaTecnetDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayTasaViajesPerdidosCausaTecnet');
                    $dao = new TasaViajesPerdidosCausaTecnetDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayTasaViajesPerdidosCausaTecnet' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new TasaViajesPerdidosCausaTecnet() );
                    return new TableGateway('tasa_viajes_perdidos_causa_tecnet', $dbAdapter, null, $resultSetPrototype);
                  },
                'Ans\Model\ViewProductividadBrigadasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayViewProductividadBrigadas');
                    $dao = new ViewProductividadBrigadasDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayViewProductividadBrigadas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new ViewProductividadBrigadas() );
                    return new TableGateway('view_productividad_brigadas', $dbAdapter, null, $resultSetPrototype);
                  },
                'Ans\Model\ViewEfectividadBrigadasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayViewEfectividadBrigadas');
                    $dao = new ViewEfectividadBrigadasDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayViewEfectividadBrigadas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new ViewEfectividadBrigadas() );
                    return new TableGateway('view_efectividad_brigadas', $dbAdapter, null, $resultSetPrototype);
                  },
                 'Ans\Model\ViewTasaViajesPerdidosCausaTecnetDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayViewTasaViajesPerdidosCausaTecnet');
                    $dao = new ViewTasaViajesPerdidosCausaTecnetDao($tableGateway);
                    return $dao;
                  },
                'TableGatewayViewTasaViajesPerdidosCausaTecnet' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new ViewTasaViajesPerdidosCausaTecnet() );
                    return new TableGateway('view_tasa_viajes_perdidos_causatecnet', $dbAdapter, null, $resultSetPrototype);
                  },         
               ),
            );
          }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Ans\Controller\Index' => function ($sm) {
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAns');    
                    
                    $productividadBrigadasDao = $locator->get('Ans\Model\ProductividadBrigadasDao');
                    $efectividadBrigadasDao = $locator->get('Ans\Model\EfectividadBrigadasDao');
                    $tasaViajesPerdidosCausaTecnetDao = $locator->get('Ans\Model\TasaViajesPerdidosCausaTecnetDao');
                    
                    $viewProductividadBrigadasDao = $locator->get('Ans\Model\ViewProductividadBrigadasDao');
                    $viewEfectividadBrigadasDao = $locator->get('Ans\Model\ViewEfectividadBrigadasDao');
                    $viewTasaViajesPerdidosCausaTecnetDao = $locator->get('Ans\Model\ViewTasaViajesPerdidosCausaTecnetDao');
                    
                    $controller = new \Ans\Controller\IndexController($config);
                    
                    $controller->setProductividadBrigadasDao($productividadBrigadasDao);
                    $controller->setEfectividadBrigadasDao($efectividadBrigadasDao);
                    $controller->setTasaViajesPerdidosCausaTecnetDao($tasaViajesPerdidosCausaTecnetDao);
                    
                    $controller->setViewProductividadBrigadasDao($viewProductividadBrigadasDao);
                    $controller->setViewEfectividadBrigadasDao($viewEfectividadBrigadasDao);
                    $controller->setViewTasaViajesPerdidosCausaTecnetDao($viewTasaViajesPerdidosCausaTecnetDao);
                    
                    
                    return $controller;
                }
            )
        );
    }
    

}
