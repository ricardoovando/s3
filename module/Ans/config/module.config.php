<?php

return array(
    'controllers' => array(
        'invokables' => array(

        ),
    ),
    'router' => array(
        'routes' => array(
            'ans' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ans[/:controller][/:action][/:id][/:m][/:a]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'm' => '[0-9]+',
                        'a' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Ans\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'paginator_ans' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ans[/:controller][/:action]/id[/:id]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Ans\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'ans' => __DIR__ . '/../view',
        ),
    ),
);
