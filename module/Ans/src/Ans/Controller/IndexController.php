<?php

namespace Ans\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/*ENTITYS*/
use Ans\Model\Entity\ProductividadBrigadas;
        

class IndexController extends AbstractActionController {

    private $config;
    private $login;
    private $ProductividadBrigadasDao;
    private $EfectividadBrigadasDao;
    private $TasaViajesPerdidosCausaTecnetDao;
    private $ViewProductividadBrigadasDao;
    private $ViewEfectividadBrigadasDao;
    private $ViewTasaViajesPerdidosCausaTecnetDao;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setProductividadBrigadasDao($productividadBrigadasDao) {
        $this->ProductividadBrigadasDao = $productividadBrigadasDao;
    }

    public function getProductividadBrigadasDao() {
        return $this->ProductividadBrigadasDao;
    }

    public function setEfectividadBrigadasDao($efectividadBrigadasDao) {
        $this->EfectividadBrigadasDao = $efectividadBrigadasDao;
    }

    public function getEfectividadBrigadasDao() {
        return $this->EfectividadBrigadasDao;
    }

    public function setTasaViajesPerdidosCausaTecnetDao($tasaViajesPerdidosCausaTecnetDao) {
        $this->TasaViajesPerdidosCausaTecnetDao = $tasaViajesPerdidosCausaTecnetDao;
    }

    public function getTasaViajesPerdidosCausaTecnetDao() {
        return $this->TasaViajesPerdidosCausaTecnetDao;
    }

    public function setViewProductividadBrigadasDao($viewProductividadBrigadasDao) {
        $this->ViewProductividadBrigadasDao = $viewProductividadBrigadasDao;
    }

    public function getViewProductividadBrigadasDao() {
        return $this->ViewProductividadBrigadasDao;
    }

    public function setViewEfectividadBrigadasDao($viewEfectividadBrigadasDao) {
        $this->ViewEfectividadBrigadasDao = $viewEfectividadBrigadasDao;
    }

    public function getViewEfectividadBrigadasDao() {
        return $this->ViewEfectividadBrigadasDao;
    }

    public function setViewTasaViajesPerdidosCausaTecnetDao($viewTasaViajesPerdidosCausaTecnetDao) {
        $this->ViewTasaViajesPerdidosCausaTecnetDao = $viewTasaViajesPerdidosCausaTecnetDao;
    }

    public function getViewTasaViajesPerdidosCausaTecnetDao() {
        return $this->ViewTasaViajesPerdidosCausaTecnetDao;
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $productividadBrigadas = $this->getProductividadBrigadasDao()->obtenerTodos();
        $efectividadBrigadas = $this->getEfectividadBrigadasDao()->obtenerTodos();
        $tasaViajesPerdidosCausaTecnet = $this->getTasaViajesPerdidosCausaTecnetDao()->obtenerTodos();

        return new ViewModel(array(
            'title' => 'Indicadores ANS Tecnet Medición 2014',
            'productividadBrigadas' => $productividadBrigadas,
            'efectividadBrigadas' => $efectividadBrigadas,
            'tasaViajesPerdidosCausaTecnet' => $tasaViajesPerdidosCausaTecnet,
        ));
    }

    public function auditoriaAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $postParams = $this->request->getPost();

        $m = (int) $this->params()->fromRoute('m', 0); /*puede capturar la variable por request*/
        
        /*MES*/
        if(!empty($postParams["select_mes"])){
            $m = $postParams["select_mes"]; /*o por post*/
          }
         
        /*MES*/
        if(empty($m)){
            $m = date("n");
           }
           
        $viewProductividadBrigadasDao = $this->getViewProductividadBrigadasDao()->obtenerPorMes($m);
        $viewEfectividadBrigadasDao = $this->getViewEfectividadBrigadasDao()->obtenerPorMes($m);
        $viewTasaViajesPerdidosCausaTecnetDao = $this->getViewTasaViajesPerdidosCausaTecnetDao()->obtenerPorMes($m);

        /*AÑO*/
        $A = date("Y");
        
        return new ViewModel(array(
            'title' => 'Indicadores ANS Tecnet Medición '. $A ,
            'viewProductividadBrigadasDao' => $viewProductividadBrigadasDao,
            'viewEfectividadBrigadasDao' => $viewEfectividadBrigadasDao,
            'viewTasaViajesPerdidosCausaTecnetDao' => $viewTasaViajesPerdidosCausaTecnetDao,
        ));
    }

    public function publicarProductividadBrigadaAction() {

      $this->layout()->usuario = $this->getLogin()->getIdentity();
      
      /* captura datos desde el request */
      $id = (int) $this->params()->fromRoute('id', 0);
      $mes = (int) $this->params()->fromRoute('m', 0);
      $anio = (int) $this->params()->fromRoute('a', 0);

      if (!$id) {
            return $this->redirect()->toRoute('ans', array('controller' => 'index' , 'action' => 'auditoria' ));
          }
          
      if (!$mes) {
            return $this->redirect()->toRoute('ans', array('controller' => 'index' , 'action' => 'auditoria' ));
          }
      
     if (!$anio) {
            return $this->redirect()->toRoute('ans', array('controller' => 'index' , 'action' => 'auditoria' ));
          }     
          
      $viewProductividadBrigadasDao = $this->getViewProductividadBrigadasDao()->obtenerPorId($id,$mes,$anio);
      
      $nombre_mes = $this->nombremes($mes); 

      $values["id_region"] = $viewProductividadBrigadasDao->getId_region();
      $values['nombre_region'] = $viewProductividadBrigadasDao->getNombre_region();
      $values['anio'] = $viewProductividadBrigadasDao->getAnio();
      if($viewProductividadBrigadasDao->getProductividad_brigada() == NULL){ $d = 0; }else{ $d = $viewProductividadBrigadasDao->getProductividad_brigada(); }
      $values[$nombre_mes] = $d;
      
      $productividadbrigada = new ProductividadBrigadas();
      $productividadbrigada->exchangeArray($values);

      $this->getProductividadBrigadasDao()->guardar($productividadbrigada);
      
      return $this->redirect()->toRoute('ans', array('controller' => 'index' , 'action' => 'auditoria' , 'id' => $id , 'm' => $mes , 'a' => $anio ));
        
    }
    
    
   public function nombremes($mes){
        
        $nombre_mes = "";
        if ($mes == 1) {
            $nombre_mes = 'enero';
        }
        if ($mes == 2) {
            $nombre_mes = 'febrero';
        }
        if ($mes == 3) {
            $nombre_mes = 'marzo';
        }
        if ($mes == 4) {
            $nombre_mes = 'abril';
        }
        if ($mes == 5) {
            $nombre_mes = 'mayo';
        }
        if ($mes == 6) {
            $nombre_mes = 'junio';
        }
        if ($mes == 7) {
            $nombre_mes = 'julio';
        }
        if ($mes == 8) {
            $nombre_mes = 'agosto';
        }
        if ($mes == 9) {
            $nombre_mes = 'septiembre';
        }
        if ($mes == 10) {
            $nombre_mes = 'octubre';
        }
        if ($mes == 11) {
            $nombre_mes = 'noviembre';
        }
        if ($mes == 12) {
            $nombre_mes = 'diciembre';
        }

        return $nombre_mes;
        
    }
    
    

}
