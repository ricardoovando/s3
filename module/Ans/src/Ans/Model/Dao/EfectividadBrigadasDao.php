<?php

namespace Ans\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

class EfectividadBrigadasDao {

    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
        try {
            return $this->tableGateway->select();
        } catch (\Exception $e) {
            \Zend\Debug\Debug::dump($e->__toString());
            exit;
        }
    }

}
