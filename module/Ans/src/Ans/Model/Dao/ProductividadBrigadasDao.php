<?php

namespace Ans\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

use Ans\Model\Entity\ProductividadBrigadas;

class ProductividadBrigadasDao {

    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
        try {
            return $this->tableGateway->select();
        } catch (\Exception $e) {
            \Zend\Debug\Debug::dump($e->__toString());
            exit;
        }
    }
    
    
    public function obtenerPorId($id,$anio) {
        $id = (Int) $id;
        $anio = (Int) $anio;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id_region' => $id));
        $select->where(array('anio' => $anio));
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        return $row;
    }
    
    
   public function guardar($productividadBrigadas) {
    
       if($productividadBrigadas->getEnero() != NULL) 
           $mes = 'enero';
           $dato = $productividadBrigadas->getEnero();
                             
       if($productividadBrigadas->getFebrero() != NULL)
       {  $mes = 'febrero';
          $dato = $productividadBrigadas->getFebrero();}
       
       if($productividadBrigadas->getMarzo() != NULL)
       {  $mes = 'marzo';
          $dato = $productividadBrigadas->getMarzo(); }
       
       if($productividadBrigadas->getAbril() != NULL)
       {  $mes = 'abril';
          $dato = $productividadBrigadas->getAbril(); }

       if($productividadBrigadas->getMayo() >= 0)
       {  $mes = 'mayo';
          $dato = $productividadBrigadas->getMayo(); }

       if($productividadBrigadas->getJunio() != NULL)
       {  $mes = 'junio';
          $dato = $productividadBrigadas->getJunio(); }

       if($productividadBrigadas->getJulio() != NULL)
       {  $mes = 'julio';
          $dato = $productividadBrigadas->getJulio(); }

       if($productividadBrigadas->getAgosto() != NULL)
       {  $mes = 'agosto';
          $dato = $productividadBrigadas->getAgosto(); }

       if($productividadBrigadas->getSeptiembre() != NULL)
       {  $mes = 'septiembre';
          $dato = $productividadBrigadas->getSeptiembre(); }

       if($productividadBrigadas->getOctubre() != NULL)
       {  $mes = 'octubre';
          $dato = $productividadBrigadas->getOctubre(); }

       if($productividadBrigadas->getNoviembre() != NULL)
       {  $mes = 'noviembre';
          $dato = $productividadBrigadas->getNoviembre(); }

       if($productividadBrigadas->getDiciembre() != NULL) 
       {  $mes = 'diciembre';
          $dato = $productividadBrigadas->getDiciembre(); }
        
       
        $data = array(
            'id_region' => $productividadBrigadas->getId_region(),
            'nombre_region' => $productividadBrigadas->getNombre_region(),
            'anio' => $productividadBrigadas->getAnio(),
            $mes => $dato,
          );

                
        if ($this->obtenerPorId($productividadBrigadas->getId_region(),$productividadBrigadas->getAnio())) {
            
            $this->tableGateway->update($data, array('id_region' => $productividadBrigadas->getId_region(),'anio' => $productividadBrigadas->getAnio()));
            
        } else {
            
            $this->tableGateway->insert($data);
            
        }
        
    }
    
    

}
