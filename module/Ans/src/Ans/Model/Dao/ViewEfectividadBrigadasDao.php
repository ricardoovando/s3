<?php

namespace Ans\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

class ViewEfectividadBrigadasDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
        try {
            return $this->tableGateway->select();
        } catch (\Exception $e) {
            \Zend\Debug\Debug::dump($e->__toString());
            exit;
        }
    }

    public function obtenerPorMes($mes) {
        $mes = (Int) $mes;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('mes' => $mes));
        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }

}
