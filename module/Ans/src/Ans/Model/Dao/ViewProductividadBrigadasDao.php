<?php

namespace Ans\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

class ViewProductividadBrigadasDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
       
         $select = $this->tableGateway->getSql()->select();
         
         $select->join(array('prob' => 'productividad_brigadas'), 
                  new \Zend\Db\Sql\Expression('view_productividad_brigadas.id_region = prob.id_region AND view_productividad_brigadas.anio = prob.anio'), array('*'));

         $select->order('id_region ASC'); 
         
         $rowset = $this->tableGateway->selectWith($select);
         return $rowset;
    }

    public function obtenerPorMes($mes) {
        $mes = (Int) $mes;
        
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('prob' => 'productividad_brigadas'), 
                  new \Zend\Db\Sql\Expression('view_productividad_brigadas.id_region = prob.id_region AND view_productividad_brigadas.anio = prob.anio'), array('*'));
        $select->where(array('mes' => $mes));
        $select->order('view_productividad_brigadas.id_region ASC'); 

        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }
    
    public function obtenerPorId($id,$mes,$anio) {
        $id = (Int) $id;
        $mes = (Int) $mes;
        $anio = (Int) $anio;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id_region' => $id));
        $select->where(array('mes' => $mes));
        $select->where(array('anio' => $anio));
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        return $row;
    }

}
