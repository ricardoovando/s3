<?php

namespace Ans\Model\Entity;

class ProductividadBrigadas {

    private $id_pro_bri;
    private $id_region;
    private $nombre_region;
    private $anio;
    private $enero;
    private $febrero;
    private $marzo;
    private $abril;
    private $mayo;
    private $junio;
    private $julio;
    private $agosto;
    private $septiembre;
    private $octubre;
    private $noviembre;
    private $diciembre;

    function __construct($id_pro_bri = null, $id_region = null, $nombre_region = null, $anio = null, $enero = null, $febrero = null, $marzo = null, $abril = null, $mayo = null, $junio = null, $julio = null, $agosto = null, $septiembre = null, $octubre = null, $noviembre = null, $diciembre = null) {
        $this->id_pro_bri = $id_pro_bri;
        $this->id_region = $id_region;
        $this->nombre_region = $nombre_region;
        $this->anio = $anio;
        $this->enero = $enero;
        $this->febrero = $febrero;
        $this->marzo = $marzo;
        $this->abril = $abril;
        $this->mayo = $mayo;
        $this->junio = $junio;
        $this->julio = $julio;
        $this->agosto = $agosto;
        $this->septiembre = $septiembre;
        $this->octubre = $octubre;
        $this->noviembre = $noviembre;
        $this->diciembre = $diciembre;
    }

    public function getId_pro_bri() {
        return $this->id_pro_bri;
    }

    public function setId_pro_bri($id_pro_bri) {
        $this->id_pro_bri = $id_pro_bri;
    }

    public function getId_region() {
        return $this->id_region;
    }

    public function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    public function getNombre_region() {
        return $this->nombre_region;
    }

    public function setNombre_region($nombre_region) {
        $this->nombre_region = $nombre_region;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function getEnero() {
        return $this->enero;
    }

    public function setEnero($enero) {
        $this->enero = $enero;
    }

    public function getFebrero() {
        return $this->febrero;
    }

    public function setFebrero($febrero) {
        $this->febrero = $febrero;
    }

    public function getMarzo() {
        return $this->marzo;
    }

    public function setMarzo($marzo) {
        $this->marzo = $marzo;
    }

    public function getAbril() {
        return $this->abril;
    }

    public function setAbril($abril) {
        $this->abril = $abril;
    }

    public function getMayo() {
        return $this->mayo;
    }

    public function setMayo($mayo) {
        $this->mayo = $mayo;
    }

    public function getJunio() {
        return $this->junio;
    }

    public function setJunio($junio) {
        $this->junio = $junio;
    }

    public function getJulio() {
        return $this->julio;
    }

    public function setJulio($julio) {
        $this->julio = $julio;
    }

    public function getAgosto() {
        return $this->agosto;
    }

    public function setAgosto($agosto) {
        $this->agosto = $agosto;
    }

    public function getSeptiembre() {
        return $this->septiembre;
    }

    public function setSeptiembre($septiembre) {
        $this->septiembre = $septiembre;
    }

    public function getOctubre() {
        return $this->octubre;
    }

    public function setOctubre($octubre) {
        $this->octubre = $octubre;
    }

    public function getNoviembre() {
        return $this->noviembre;
    }

    public function setNoviembre($noviembre) {
        $this->noviembre = $noviembre;
    }

    public function getDiciembre() {
        return $this->diciembre;
    }

    public function setDiciembre($diciembre) {
        $this->diciembre = $diciembre;
    }

    public function exchangeArray($data) {

        $this->id_pro_bri = (isset($data['id_pro_bri'])) ? $data['id_pro_bri'] : null;
        $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
        $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->enero = (isset($data['enero'])) ? $data['enero'] : null;
        $this->febrero = (isset($data['febrero'])) ? $data['febrero'] : null;
        $this->marzo = (isset($data['marzo'])) ? $data['marzo'] : null;
        $this->abril = (isset($data['abril'])) ? $data['abril'] : null;
        $this->mayo = (isset($data['mayo'])) ? $data['mayo'] : null;
        $this->junio = (isset($data['junio'])) ? $data['junio'] : null;
        $this->julio = (isset($data['julio'])) ? $data['julio'] : null;
        $this->agosto = (isset($data['agosto'])) ? $data['agosto'] : null;
        $this->septiembre = (isset($data['septiembre'])) ? $data['septiembre'] : null;
        $this->octubre = (isset($data['octubre'])) ? $data['octubre'] : null;
        $this->noviembre = (isset($data['noviembre'])) ? $data['noviembre'] : null;
        $this->diciembre = (isset($data['diciembre'])) ? $data['diciembre'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

