<?php

namespace Ans\Model\Entity;

class ViewProductividadBrigadas {
    
    private $id_region;
    private $nombre_region;
    private $mes;
    private $anio;
    private $dias_descontar;
    private $cantidad_brigadas;
    private $diasdelmes;
    private $ddm_xttbr_msdd;
    private $avisos_frustrados;
    private $avisos_sin_frustrados;
    private $productividad_brigada;
    
    
    /*Objetos Relacionales*/
        private $PRODUCTIVIDAD;
         
          
    function __construct($id_region = null, $nombre_region = null, $mes = null, $anio = null, $dias_descontar = null, $cantidad_brigadas = null, $diasdelmes = null, $ddm_xttbr_msdd = null, $avisos_frustrados = null, $avisos_sin_frustrados = null, $productividad_brigada = null) {
        $this->id_region = $id_region;
        $this->nombre_region = $nombre_region;
        $this->mes = $mes;
        $this->anio = $anio;
        $this->dias_descontar = $dias_descontar;
        $this->cantidad_brigadas = $cantidad_brigadas;
        $this->diasdelmes = $diasdelmes;
        $this->ddm_xttbr_msdd = $ddm_xttbr_msdd;
        $this->avisos_frustrados = $avisos_frustrados;
        $this->avisos_sin_frustrados = $avisos_sin_frustrados;
        $this->productividad_brigada = $productividad_brigada;
    }

    public function getId_region() {
        return $this->id_region;
    }

    public function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    public function getNombre_region() {
        return $this->nombre_region;
    }

    public function setNombre_region($nombre_region) {
        $this->nombre_region = $nombre_region;
    }

    public function getMes() {
        return $this->mes;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function getDias_descontar() {
        return $this->dias_descontar;
    }

    public function setDias_descontar($dias_descontar) {
        $this->dias_descontar = $dias_descontar;
    }

    public function getCantidad_brigadas() {
        return $this->cantidad_brigadas;
    }

    public function setCantidad_brigadas($cantidad_brigadas) {
        $this->cantidad_brigadas = $cantidad_brigadas;
    }

    public function getDiasdelmes() {
        return $this->diasdelmes;
    }

    public function setDiasdelmes($diasdelmes) {
        $this->diasdelmes = $diasdelmes;
    }

    public function getDdm_xttbr_msdd() {
        return $this->ddm_xttbr_msdd;
    }

    public function setDdm_xttbr_msdd($ddm_xttbr_msdd) {
        $this->ddm_xttbr_msdd = $ddm_xttbr_msdd;
    }

    public function getAvisos_frustrados() {
        return $this->avisos_frustrados;
    }

    public function setAvisos_frustrados($avisos_frustrados) {
        $this->avisos_frustrados = $avisos_frustrados;
    }

    public function getAvisos_sin_frustrados() {
        return $this->avisos_sin_frustrados;
    }

    public function setAvisos_sin_frustrados($avisos_sin_frustrados) {
        $this->avisos_sin_frustrados = $avisos_sin_frustrados;
    }

    public function getProductividad_brigada() {
        return $this->productividad_brigada;
    }

    public function setProductividad_brigada($productividad_brigada) {
        $this->productividad_brigada = $productividad_brigada;
    }
    
    public function getPRODUCTIVIDAD() {
        return $this->PRODUCTIVIDAD;
    }

    public function setPRODUCTIVIDAD(ProductividadBrigadas $PRODUCTIVIDAD) {
        $this->PRODUCTIVIDAD = $PRODUCTIVIDAD;
    }

    
    public function exchangeArray($data) {
        
        $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
        $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
        $this->mes = (isset($data['mes'])) ? $data['mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->dias_descontar = (isset($data['dias_descontar'])) ? $data['dias_descontar'] : null;
        $this->cantidad_brigadas = (isset($data['cantidad_brigadas'])) ? $data['cantidad_brigadas'] : null;
        $this->diasdelmes = (isset($data['diasdelmes'])) ? $data['diasdelmes'] : null;
        $this->ddm_xttbr_msdd = (isset($data['ddm_xttbr_msdd'])) ? $data['ddm_xttbr_msdd'] : null;
        $this->avisos_frustrados = (isset($data['avisos_frustrados'])) ? $data['avisos_frustrados'] : null;
        $this->avisos_sin_frustrados = (isset($data['avisos_sin_frustrados'])) ? $data['avisos_sin_frustrados'] : null;
        $this->productividad_brigada = (isset($data['productividad_brigada'])) ? $data['productividad_brigada'] : null;
                
        /*Objeto para relacionar*/        
        $this->PRODUCTIVIDAD = new \Ans\Model\Entity\ProductividadBrigadas();
        $this->PRODUCTIVIDAD->setId_pro_bri((isset($data['id_pro_bri'])) ? $data['id_pro_bri'] : null);
        $this->PRODUCTIVIDAD->setId_region((isset($data['id_region'])) ? $data['id_region'] : null);
        $this->PRODUCTIVIDAD->setNombre_region((isset($data['nombre_region'])) ? $data['nombre_region'] : null);
        $this->PRODUCTIVIDAD->setAnio((isset($data['anio'])) ? $data['anio'] : null);
        $this->PRODUCTIVIDAD->setEnero((isset($data['enero'])) ? $data['enero'] : null);
        $this->PRODUCTIVIDAD->setFebrero((isset($data['febrero'])) ? $data['febrero'] : null);
        $this->PRODUCTIVIDAD->setMarzo((isset($data['marzo'])) ? $data['marzo'] : null);
        $this->PRODUCTIVIDAD->setAbril((isset($data['abril'])) ? $data['abril'] : null);
        $this->PRODUCTIVIDAD->setMayo((isset($data['mayo'])) ? $data['mayo'] : null);
        $this->PRODUCTIVIDAD->setJunio((isset($data['junio'])) ? $data['junio'] : null);
        $this->PRODUCTIVIDAD->setJulio((isset($data['julio'])) ? $data['julio'] : null);
        $this->PRODUCTIVIDAD->setAgosto((isset($data['agosto'])) ? $data['agosto'] : null);
        $this->PRODUCTIVIDAD->setSeptiembre((isset($data['septiembre'])) ? $data['septiembre'] : null);
        $this->PRODUCTIVIDAD->setOctubre((isset($data['octubre'])) ? $data['octubre'] : null);
        $this->PRODUCTIVIDAD->setNoviembre((isset($data['noviembre'])) ? $data['noviembre'] : null);
        $this->PRODUCTIVIDAD->setDiciembre((isset($data['diciembre'])) ? $data['diciembre'] : null);
                
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }
          
}
