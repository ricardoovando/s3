<?php

namespace Ans\Model\Entity;

class ViewTasaViajesPerdidosCausaTecnet {
    
    private $id_region;
    private $nombre_region;
    private $mes;
    private $anio;
    private $avisos_frustrados_causatecnet;
    private $avisos_frustrados;
    private $avisos_sin_frustrados;
    private $total_visitas_mes;
    private $tasa_viajes_perdidos_causa_tecnet;
    
          
    function __construct($id_region = null, $nombre_region = null, $mes = null, $anio = null, $avisos_frustrados_causatecnet = null, $avisos_frustrados = null, $avisos_sin_frustrados = null, $total_visitas_mes = null, $tasa_viajes_perdidos_causa_tecnet = null) {
        $this->id_region = $id_region;
        $this->nombre_region = $nombre_region;
        $this->mes = $mes;
        $this->anio = $anio;
        $this->avisos_frustrados_causatecnet = $avisos_frustrados_causatecnet;
        $this->avisos_frustrados = $avisos_frustrados;
        $this->avisos_sin_frustrados = $avisos_sin_frustrados;
        $this->total_visitas_mes = $total_visitas_mes;
        $this->tasa_viajes_perdidos_causa_tecnet = $tasa_viajes_perdidos_causa_tecnet;
    }

    public function getId_region() {
        return $this->id_region;
    }

    public function getNombre_region() {
        return $this->nombre_region;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function getAvisos_frustrados_causatecnet() {
        return $this->avisos_frustrados_causatecnet;
    }

    public function getAvisos_frustrados() {
        return $this->avisos_frustrados;
    }

    public function getAvisos_sin_frustrados() {
        return $this->avisos_sin_frustrados;
    }

    public function getTotal_visitas_mes() {
        return $this->total_visitas_mes;
    }

    public function getTasa_viajes_perdidos_causa_tecnet() {
        return $this->tasa_viajes_perdidos_causa_tecnet;
    }

    public function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    public function setNombre_region($nombre_region) {
        $this->nombre_region = $nombre_region;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function setAvisos_frustrados_causatecnet($avisos_frustrados_causatecnet) {
        $this->avisos_frustrados_causatecnet = $avisos_frustrados_causatecnet;
    }

    public function setAvisos_frustrados($avisos_frustrados) {
        $this->avisos_frustrados = $avisos_frustrados;
    }

    public function setAvisos_sin_frustrados($avisos_sin_frustrados) {
        $this->avisos_sin_frustrados = $avisos_sin_frustrados;
    }

    public function setTotal_visitas_mes($total_visitas_mes) {
        $this->total_visitas_mes = $total_visitas_mes;
    }

    public function setTasa_viajes_perdidos_causa_tecnet($tasa_viajes_perdidos_causa_tecnet) {
        $this->tasa_viajes_perdidos_causa_tecnet = $tasa_viajes_perdidos_causa_tecnet;
    }       

    public function exchangeArray($data) {
        $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
        $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
        $this->mes = (isset($data['mes'])) ? $data['mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->avisos_frustrados_causatecnet = (isset($data['avisos_frustrados_causatecnet'])) ? $data['avisos_frustrados_causatecnet'] : null;
        $this->avisos_frustrados = (isset($data['avisos_frustrados'])) ? $data['avisos_frustrados'] : null;
        $this->avisos_sin_frustrados = (isset($data['avisos_sin_frustrados'])) ? $data['avisos_sin_frustrados'] : null;
        $this->total_visitas_mes = (isset($data['total_visitas_mes'])) ? $data['total_visitas_mes'] : null;
        $this->tasa_viajes_perdidos_causa_tecnet = (isset($data['tasa_viajes_perdidos_causa_tecnet'])) ? $data['tasa_viajes_perdidos_causa_tecnet'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }
          
}
