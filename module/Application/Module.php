<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
//use Zend\ModuleManager\Feature\ControllerProviderInterface;
//use Zend\ModuleManager\ModuleManagerInterface;
use Zend\EventManager\EventInterface;
use Zend\Config\Reader\Ini;
use Zend\Config\Config;
//use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;


class Module implements BootstrapListenerInterface,ConfigProviderInterface, AutoloaderProviderInterface {
    

    public function onBootstrap(EventInterface $e) {

        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->initConfig($e);
        $this->initViewRender($e);
        $this->initEnvironment($e);
        $this->initLayout($e);

    }

    protected function initConfig($e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIni', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return new Config($data);
                });
    }

    protected function initViewRender($e) {
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $viewRender = $sm->get('ViewManager')->getRenderer();
        $config = $sm->get('ConfigIni');

        $viewRender->headTitle($config['parametros']['titulo']);
        $viewRender->headMeta()->setCharset($config['parametros']['view']['charset']);
        $viewRender->doctype($config['parametros']['view']['doctype']);
    }

    protected function initEnvironment($e) {
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $config = $sm->get('ConfigIni');
        error_reporting(E_ALL | E_STRICT);
        ini_set("display_errors", true);

        $timeZone = (string) $config['parametros']['timezone'];

        if (empty($timeZone)) {
            $timeZone = "America/Santiago";
        }

        date_default_timezone_set($timeZone);
    }

    protected function initLayout($e) {
        // Set header y footer layout template
        $layout = $e->getViewModel();

        $header = new ViewModel();
        $header->setTemplate('layout/header');
        $layout->addChild($header, 'header');
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    
    public function getServiceConfig() {
        return array(
            'factories' => array(
               'Zend\Log\StartSession' => function ($sm) {
                $stream = fopen( '/home/meditres/public_html/data/logs/start_session.log', 'a', false);
                if (!$stream) {
                   throw new\Exception('Error al abrir el stream');
                 }
                $log = new \Zend\Log\Logger();
                $writer = new \Zend\Log\Writer\Stream($stream);
                $log->addWriter($writer);
                return $log;
              },
              'Zend\Log\GetTabletData' => function ($sm) {
                $stream = fopen( '/home/meditres/public_html/data/logs/get_tablet_data.log', 'a', false);
                if (!$stream) {
                   throw new\Exception('Error al abrir el stream');
                 }
                $log = new \Zend\Log\Logger();
                $writer = new \Zend\Log\Writer\Stream($stream);
                $log->addWriter($writer);
                return $log;
              }        
          ),
       );
    }

}
