<?php

return array(
    'controllers' => array(
        'invokables' => array(
            //'Auditor\Controller\Cargar' => 'Auditor\Controller\CargarController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'auditor' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/auditor[/:controller][/:action][/:id][/:br]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                        'br' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Auditor\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
          'paginatorauditor' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/auditor[/:controller][/:action]/id[/:id]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                     ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'auditor\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Auditor' => __DIR__ . '/../view',
        ),
    ),
);
