<?php

namespace Auditor\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/* FORMULARIOS */

/***************************************************/

use Avisos\Form\Buscador as BuscadorForm;
use Avisos\Form\BuscadorValidator;

use Avisos\Form\BuscadorInstalacion;
use Avisos\Form\BuscadorInstalacionValidator;

use Auditor\Form\Buscador as FormBusCom;
use Auditor\Form\BuscadorValidator as VFormBusCom;

/**********************************************/

/* ENTITYS */
use Avisos\Model\Entity\Aviso;


class IndexController extends AbstractActionController {

    private $avisoDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function setAvisoDao($avisoDao) {
        $this->avisoDao = $avisoDao;
    }

    public function getAvisoDao() {
        return $this->avisoDao;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    private function getFormBuscador() {
        return new BuscadorForm();
    }
    
    private function getFormBuscadorInstalacion(){   
        return new BuscadorInstalacion();
    }
    
    private function getFormBusCom(){
        $form = new FormBusCom();
        $form->get('poblacion')->setValueOptions($this->getAvisoDao()->obtenerPoblacionesSelect());
        $form->get('persona_tecnico')->setValueOptions($this->getAvisoDao()->obtenerPersonalTecnicoSelect());
        $form->get('estado_aviso')->setValueOptions($this->getAvisoDao()->obtenerEstadosSelect());
        $form->get('sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form; 
    }
    
    
    /* 
     * 
     * Muestra la pantalla principal
     * 
     * 
     */

    public function indexAction() {

        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
          
        /* Sino esta creada la variable de seccion se crea */        
        if(empty($_SESSION['datosFiltran'])) $_SESSION['datosFiltran'] = array();

       
       if ($this->getRequest()->isPost()) { 

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();



            if($postParams["send"] === 'Filtro Combinado')
            { 

               if ((!empty($postParams["poblacion"]))){
                    $_SESSION['datosFiltran']["poblacion"] = $postParams["poblacion"];
               }else{                   
                    unset($_SESSION['datosFiltran']['poblacion']);                 
               }

               if ((!empty($postParams["sucursal"]))){
                    $_SESSION['datosFiltran']["sucursal"] = $postParams["sucursal"];
               }else{                   
                    unset($_SESSION['datosFiltran']['sucursal']);                 
               }

               if ((!empty($postParams["estado_aviso"]))){
                    $_SESSION['datosFiltran']["estado_aviso"] = $postParams["estado_aviso"];
               }else{                   
                    unset($_SESSION['datosFiltran']['estado_aviso']);                 
               }
              
              if ((!empty($postParams["persona_tecnico"]))){
                    $_SESSION['datosFiltran']["persona_tecnico"] = $postParams["persona_tecnico"];
               }else{                   
                    unset($_SESSION['datosFiltran']['persona_tecnico']);                 
               }

              if ((!empty($postParams["estado_riat"]))){
                    $_SESSION['datosFiltran']["estado_riat"] = $postParams["estado_riat"];
               }else{                   
                    unset($_SESSION['datosFiltran']['estado_riat']);                 
               }
                
               if ((!empty($postParams["meses_anio"]))){
                    $_SESSION['datosFiltran']["meses_anio"] = $postParams["meses_anio"];
               }else{                   
                    unset($_SESSION['datosFiltran']['meses_anio']);                 
               }

               if ((!empty($postParams["anio"]))){
                    $_SESSION['datosFiltran']["anio"] = $postParams["anio"];
               }else{                   
                    unset($_SESSION['datosFiltran']['anio']);                 
               }
                
            }else{
                
                $_SESSION['datosFiltran'] = array();
                
            }

         }


        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBusCom = $this->getFormBusCom();


             if((!empty($_SESSION['datosFiltran']["poblacion"])))
                        $formBusCom->get('poblacion')->setValue($_SESSION['datosFiltran']["poblacion"]);

             if((!empty($_SESSION['datosFiltran']["sucursal"])))
                        $formBusCom->get('sucursal')->setValue($_SESSION['datosFiltran']["sucursal"]);

             if((!empty($_SESSION['datosFiltran']["estado_aviso"])))
                       $formBusCom->get('estado_aviso')->setValue($_SESSION['datosFiltran']["estado_aviso"]);

             if((!empty($_SESSION['datosFiltran']["persona_tecnico"])))
                       $formBusCom->get('persona_tecnico')->setValue($_SESSION['datosFiltran']["persona_tecnico"]);

             if((!empty($_SESSION['datosFiltran']["estado_riat"]))) 
                      $formBusCom->get('estado_riat')->setValue($_SESSION['datosFiltran']["estado_riat"]);

             if((!empty($_SESSION['datosFiltran']["meses_anio"])))
                      $formBusCom->get('meses_anio')->setValue($_SESSION['datosFiltran']["meses_anio"]);

             if((!empty($_SESSION['datosFiltran']["anio"]))) 
                     $formBusCom->get('anio')->setValue($_SESSION['datosFiltran']["anio"]);

        
        /* Filtrar solo por los estados Necesarios */
        $paginator = $this->getAvisoDao()->obtenerTodosAuditor($_SESSION['datosFiltran']);       
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(100);
        
        return new ViewModel(array(
                'a' => $this->params()->fromRoute('action'), 
                'title' => 'Auditar Avisos Trifásicos',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBusCom' => $formBusCom,
              ));
        
        }

    
    
    /* 
     * Buscador por Codigo de Aviso 
     */

    public function buscarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('auditor', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        
        /* LLamar a Formularios */
        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBusCom = $this->getFormBusCom();
        
        /*Validar Buscar Aviso*/
        $formBuscador->setInputFilter(new BuscadorValidator());
        $formBuscador->setData($postParams);
        
        /*
         * Falla de la Validacion del form. 
         */
        if (!$formBuscador->isValid())
        {
            
            //Falla la validación; volvemos a generar el formulario     
            $paginator = $this->getAvisoDao()->obtenerTodos();

            $paginator->setCurrentPageNumber(0);

            $paginator->setItemCountPerPage(100);

            $modelView = new ViewModel(array(
                'a' => $this->params()->fromRoute('action'),
                'title' => 'Parametros mal ingresados',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBusCom' => $formBusCom,
            ));

            $modelView->setTemplate('auditor/index/index');

            return $modelView;
            
        }


        $values = $formBuscador->getData();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getAvisoDao()->obtenerPorNumAviso($values['numeroaviso']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(200);


        $viewModel = new ViewModel(array(
                'a' => $this->params()->fromRoute('action'),
                'title' => 'Aviso Encontrado : ' . $values['numeroaviso'] ,
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBusCom' => $formBusCom,
              ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("auditor/index/index");

        return $viewModel;
    }

    
   
    /* 
     * Buscador por Numero Instalacion 
     */

    public function buscarNumInstalacionAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('avisos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        
        /* LLamar a Formularios */
        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBusCom = $this->getFormBusCom();

        /* Validar Formulario Buscar por Num Instalacion */
        $formBuscadorInstalacion->setInputFilter(new BuscadorInstalacionValidator());
        $formBuscadorInstalacion->setData($postParams);
        
        
        /*
         * Falla de la Validacion del form. 
         */
        if (!$formBuscadorInstalacion->isValid())
        {
            
            //Falla la validación; volvemos a generar el formulario     
            $paginator = $this->getAvisoDao()->obtenerTodos();

            $paginator->setCurrentPageNumber(0);

            $paginator->setItemCountPerPage(100);

            $modelView = new ViewModel(array(
                'a' => $this->params()->fromRoute('action'),
                'title' => 'Parametros mal ingresados',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBusCom' => $formBusCom,
              ));

            $modelView->setTemplate('auditor/index/index');

            return $modelView; 
            
         }

        $values = $formBuscadorInstalacion->getData();        

        /* Reconfiguracion de Paginador */
        $paginator = $this->getAvisoDao()->obtenerPorNumInstalacion($values['numeroinstalacion']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(100);

        $viewModel = new ViewModel(array(
                'a' => $this->params()->fromRoute('action'),
                'title' => 'N° Instalación Encontrado : ' . $values['numeroinstalacion'],
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBusCom' => $formBusCom,
               ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("auditor/index/index");

        return $viewModel;
    }
    
    
    
    public function descargarZipAction(){
    	
        
       $this->layout()->usuario = $this->getLogin()->getIdentity();
              
       $num_instalacion = $this->params()->fromRoute('id', 0);
       
       $num_aviso = $this->params()->fromRoute('br', 0);

       
       /*Creamos el Archivo Zip*/ 
       $zip = new \ZipArchive();
       
       $filename = "/home/meditres/public_html/module/Auditor/view/auditor/index/test.zip";
            
       if ($zip->open($filename, \ZIPARCHIVE::CREATE )!==TRUE) {
            printf('Erróneo con el código %d', $ret);
            exit;
          }
        
          
       $directory = '/home/meditres/public_html/public/images/'.$num_instalacion.'/'.$num_aviso.'/';
            
       if(file_exists($directory)){
       
            $scanned_directory = array_diff(scandir($directory), array('..', '.'));
            
            foreach ($scanned_directory as $value) {
              $zip->addFile( $directory . $value ,"/".$value);
             }
            
            $zip->close();

            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename=".$num_aviso.".zip");
            header("Cache-Control: no-cache, must-revalidate");
            header("Expires: 0");

            readfile($filename);
            
            
            /*Elimino cada vez el archivo para evitar que continuen ingresando archivos al zip*/
            unlink( $filename );
       
            
       }
       
       exit; 
        
       /*Fin de Crear el Archivo Zip*/
       
       $view = new ViewModel(array(
            'url' => $url,
        ));

        $view->setTerminal(true);

        return $view;
        
      }
    
    
    
}
