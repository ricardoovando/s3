<?php

namespace Auditor\Form;

use Zend\Form\Form;

class Buscador extends Form {

    public function __construct($name = null) {
        parent::__construct('buscadorfiltro');
        
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'estado_aviso',
            'options' => array(
                'label' => '',
                'empty_option' => 'Estados Aviso',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico',
            'options' => array(
                'label' => '',
                'empty_option' => 'Personal Tecnico',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'poblacion',
            'options' => array(
                'label' => '',
                'empty_option' => 'Comunas',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'marca_medidores',
            'options' => array(
                'label' => '',
                'empty_option' => 'Marca Medidores',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sucursal',
            'options' => array(
                'label' => '',
                'empty_option' => 'Sucursales',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
                
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'estado_riat',
            'options' => array(
                'label' => '',
                'empty_option' => 'Resultado Riat',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'options' => array(
                    1 => 'NORMAL',
                    2 => 'FRUSTRADO',
                    3 => 'CON OBS',
                    4 => 'CNR',
                ),
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'meses_anio',
            'options' => array(
                'label' => '',
                'empty_option' => 'Meses del Año',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'options' => array(
                    1 => 'Enero',
                    2 => 'Febrero',
                    3 => 'Marzo',
                    4 => 'Abril',
                    5 => 'Mayo',
                    6 => 'Junio',
                    7 => 'Julio',
                    8 => 'Agosto',
                    9 => 'Septiembre',
                    10 => 'Octubre',
                    11 => 'Noviembre',
                    12 => 'Diciembre',
                  ),
                )
             ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'anio',
            'options' => array(
                'label' => '',
                'empty_option' => 'Año',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'options' => array(
                    2013 => '2013',
                    2014 => '2014',
                    2015 => '2015',
                    2016 => '2016',
                    2017 => '2017',
                  ),
                )
             ));
        
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Filtro Combinado',
                'class' => 'btn btn-warning',
                //'style' => 'height:30px;',
            ),
        ));
        
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type' => 'reset',
                'value' => 'Reset',
                'class' => 'btn btn-warning',
                //'style' => 'height:30px;',
            ),
        ));
        
    }

}

