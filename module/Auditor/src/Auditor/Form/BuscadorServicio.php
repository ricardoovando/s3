<?php

namespace Avisos\Form;

use Zend\Form\Form;

class BuscadorServicio extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombreservicio',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Buscar Nombre Servicio',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn btn-success',
                'style' => 'height:30px;',
            ),
        ));
    }

}

