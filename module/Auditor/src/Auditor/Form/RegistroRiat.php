<?php

namespace Avisos\Form;

use Zend\Form\Form;

class RegistroRiat extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));


        /* CODIGOS SERVICIOS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'distribuidora',
            'options' => array(
                'label' => 'Distribuidora : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_aviso',
            'options' => array(
                'label' => 'N° Aviso / OT : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_form_riat',
            'options' => array(
                'label' => 'N° Form Riat : ',
            ),
        ));

        /* TERMINO */

        /* DATOS CLIENTES */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombrecliente',
            'options' => array(
                'label' => 'Nombre Cliente : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'direccion',
            'options' => array(
                'label' => 'Dirección : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'comuna',
            'options' => array(
                'label' => 'Comuna : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_servicio',
            'options' => array(
                'label' => 'N° Servicio o Cliente : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tarifa',
            'options' => array(
                'label' => 'Tarifa : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'ruta_lectura',
            'options' => array(
                'label' => 'Porción/Proceso/Ruta Lectura : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_aviso',
            'options' => array(
                'label' => 'Tipo Aviso : ',
                //'empty_option' => 'Seleccione un tipo',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Inspección',
                    2 => 'Auditoría DIR.',
                    3 => 'Auditoría IND.',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'proviene_gavis',
            'options' => array(
                'label' => 'Proviene de Gavis : ',
            ),
        ));

        /* TERMINO */


        /* VIAJES FRUSTRADOS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_visita_1',
            'options' => array(
                'label' => 'Fecha Hora Visita 1 : '
            )
        ));



        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_visita_2',
            'options' => array(
                'label' => 'Fecha Hora Visita 2 : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_visita_3',
            'options' => array(
                'label' => 'Fecha Hora Visita 3 : ',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_1',
            'options' => array(
                'label' => 'No realiza Servicio 1 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
            )
        ));




        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_2',
            'options' => array(
                'label' => 'No realiza Servicio 2 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_3',
            'options' => array(
                'label' => 'No realiza Servicio 3 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
            )
        ));



        /* ANTECEDENTES EMPALME */

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_empalme',
            'options' => array(
                'label' => 'Tipo empalme : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Residencial',
                    2 => 'Comercial',
                    3 => 'Al Público',
                    4 => 'Industrial',
                    5 => 'Agrícola',
                    6 => 'Otro',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'acometida',
            'options' => array(
                'label' => 'Acometida : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme Aéreo',
                    2 => 'Emp. Subterraneo',
                    3 => 'Celda Medida',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'circuito',
            'options' => array(
                'label' => 'Circuito : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Monofásico',
                    2 => 'Bifásico',
                    3 => 'Trifásico',
                ),
            )
        ));



        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'conexion',
            'options' => array(
                'label' => 'Conexión : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Conexión BT',
                    2 => 'Conexión MT',
                    3 => 'Voltaje Red (v)',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'medida',
            'options' => array(
                'label' => 'Medida : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Directa',
                    2 => 'Indirecta TTCC',
                    3 => 'Indirecta ECM',
                ),
            )
        ));

        /* TERMINO */

        /* SELLOS ENCONTRADOS DEJADOS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_cupula_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_cupula_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_block_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_block_medidor',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_reset_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_reset_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_bateria_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_bateria_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_optico_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_optico_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_regleta_prueba',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_regleta_prueba',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_int_horario',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_int_horario',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_caja_medidor',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_caja_medidor',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_ttcc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_ttcc',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_caja_ttcc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_caja_ttcc',
        ));



        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_enc_caja_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sello_dej_caja_ecm',
        ));

        /* TERMINO */


        /* FECHA ATENCION */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_atencion_servicio',
            'options' => array(
                'label' => 'Fecha Atencion Servicio : ',
            ),
        ));

        /* TERMINO */

        /* OTROS DATOS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'placa_poste_empalme',
            'options' => array(
                'label' => 'Placa poste empalme : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'capacidad_automatico',
            'options' => array(
                'label' => 'Capacidad Automático (A) : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_ssee',
            'options' => array(
                'label' => 'Potencia SSEE : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_ssee',
            'options' => array(
                'label' => 'N° SSEE : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'pot_contratada',
            'options' => array(
                'label' => 'Pot Contratada (KW) : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'alimentador',
            'options' => array(
                'label' => 'Alimentador : ',
            ),
        ));


        /* DATOS ADJUNTOS LEVANTADOS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fotos_empalme',
            'options' => array(
                'label' => 'Fotos Empalme : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'diagrama_fasorial',
            'options' => array(
                'label' => 'Diagrama Fasorial : ',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'data_perfil_carga',
            'options' => array(
                'label' => 'Data/Perfil Carga : ',
            ),
        ));

        /* TERMINO */


        /* MEDIDORES */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_medidor_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_medidor_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_serie_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_serie_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_fabricacion_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_fabricacion_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeros_elementos_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeros_elementos_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corriente_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corriente_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_kh_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_kh_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_interna_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_interna_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tecnologia_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tecnologia_reac',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'reg_dda_maxima_act',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'reg_dda_maxima_reac',
        ));

        /* TERMINO */


        /* EQUIPOS DE MEDIDA */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_modelo_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_modelo_tra',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_series_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_series_tra',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'Anio_fabricación_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'Anio_fabricación_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_elementos_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_elementos_tra',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_voltaje_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_voltaje_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_tra',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_pasadas_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttcc_conectado_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttcc_conectado_tra',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttpp_conectado_ecm',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttpp_conectado_tra',
        ));


        /* TERMINO */


        /* LECTURA MEDIDORES */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'energia_activa_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'energia_activa_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'energia_reactiva_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'energia_reactiva_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_suministrada_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_suministrada_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_demanda_max_suministrada_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_demanda_max_suministrada_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_suministrada_acumulada_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_suministrada_acumulada_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_horas_punta_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_horas_punta_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_dda_max_horas_punta_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_dda_max_horas_punta_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_horas_punta_acumulada_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'demanda_max_horas_punta_acumulada_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_reset_medidor_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_reset_medidor_dej',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_ultimo_reset_enc',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_hora_ultimo_reset_dej',
        ));

        /* TERMINO */



        /* ESTADO INSPECCION VISUAL EMPALME */

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_acometida_empalme',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_limitador_potencia_proteccion',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_medidor',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_alambrado_medidor_regleta_ttcc_ecm',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_estado_regleta_block',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_estado_regleta_block',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_caja_medidor',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_caja_ttcc',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_estado_equipo_compacto',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_estado_interruptor_reloj',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ive_conexion_tierra_proteccion',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un estado',
                    1 => 'Bueno',
                    2 => 'Regular',
                    3 => 'Malo',
                ),
            )
        ));

        /* TERMINO */
        
        
       /* CODIGOS CONDICION SUBESTANDAR */ 
        
      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_acometida_empalme',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_limitador_potencia_proteccion',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_medidor',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_alambrado_medidor_regleta_ttcc_ecm',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_estado_regleta_block',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_caja_medidor',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_caja_ttcc',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_estado_equipo_compacto',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_estado_interruptor_reloj',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ccs_conexion_tierra_proteccion',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un codigo',
                    1 => 'Con Intervención',
                    2 => 'Fuera de Norma',
                    3 => 'Dañado o destruido',
                    4 => 'Mal Alambrado',
                    5 => 'Pernos aprietes sueltos',
                    6 => 'Sin placa o identificación',
                    7 => 'Mal fijado a su base',
                    8 => 'Sobredimencionado',
                    9 => 'Saturado',
                ),
            )
        ));

        /* TERMINO */
       
       
        /* RELOJ REGLETA TELEMEDIDA CALENDARIO */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_modelo_reloj_servicio',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_serie_reloj_servicio',
        ));
     
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_reloj_servicio',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_regleta_servicio',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_regleta_servicio',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'terminales_regleta_servicio',
        )); 

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modem_telemedida',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'alim_auxiliar_telemedida',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_serie_telemedida',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'puente_corriente_abierto',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'puente_corriente_cerrado',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'puente_voltaje_abierto',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'puente_voltaje_cerrado',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_encontrada_medidor_calendario_medidor',
        )); 
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_oficial_pais_calendario_medidor',
        ));
                        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_dejada_medidor_calendario_medidor',
        ));
                        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_encontrada_medidor_calendario_medidor',
        ));        

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_oficial_pais_calendario_medidor',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_dejada_medidor_calendario_medidor',
        )); 
        
        /*TERMINADO*/
        
        
        /* MEDICIONES INSTANTANEAS ANALIZADOR DE REDES ELECTRICAS */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_p',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_p',
        )); 
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_p',
        )); 

       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_p',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_p',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_p',
        )); 
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_p',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_p',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_p',
        )); 
        
        /*TERMINO*/

        
        /* VALOR TOTAL */
   
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_pm',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_pa',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_err',
        ));   
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_pm',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_pa',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_err',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_pm',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_pa',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_err',
        )); 
       
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_pm',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_pa',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_err',
        )); 

        /*TERMINO*/

        
        /* CORRIENTES SECUNDARIAS */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_rezonttcc',
        )); 
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_rezonttcc',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_rezonttcc',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_promedio_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_promedio_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_promedio_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_promedio_rezonttcc',
        )); 
        
        
        /*TERMINO*/
        
        /*CORRESPONDENCIA DE TENSION Y CORRIENTE EN MEDIDOR*/
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'correspondencia_elemento_1',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:122px;',
                )
              ));
        
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'correspondencia_elemento_2',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:122px;',
                )
              ));
          
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'correspondencia_elemento_3',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:122px;',
                )
              ));
          
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'correspondencia_secuencia',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'Positiva',
                    2 => 'Negativa',
                  ),
                 'style' => 'width:122px;',
                )
              ));
         
         
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'correspondencia_flujo_potencia',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'Directo',
                    2 => 'Inverso',
                  ),
                 'style' => 'width:122px;',
                )
              ));
        
        /*TERMINO*/
        
        
        /* POTENCIA MEDIDOR */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_revol_pulsos_med_activo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_revol_pulsos_med_reactivo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_tc_calculada',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tiempo_segundos_med_activo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tiempo_segundos_med_reactivo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_tp_placa',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_calculada_activo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_calculada_reactivo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contante_multiplicacion_facturacion_calculada',
        ));

        /* TERMINO */
        
        /*RESUMEN REGISTRO INSPECCION*/
        
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'registroinspeccion_1',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
              
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'registroinspeccion_2',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
        
         
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'registroinspeccion_3',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
        
                 
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'registroinspeccion_4',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
        
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'registroinspeccion_cod_adicional',
            'attributes' => array(
                 'style' => 'width:36px;',
                )
        ));  
        
        /*TERMINO*/
        
        
        
        /* Normalizacion o Mantención Realizada */
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_1',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_2',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                 
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_3',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
                  
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_4',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_5',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
                 
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_6',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                
                
           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_7',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_8',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                 
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_9',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1  => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'mantrealizada_cod_adicional',
            'attributes' => array(
                 'style' => 'width:36px;',
                )
        ));  

        /*TERMINO*/
        
        /*irregularidades detectadas*/
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_1',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
            
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_2',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_3',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                    
           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_4',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
           
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_5',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                   
           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_6',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                     
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'irregularidades_detectadas_cod_7',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    true => 'X',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                     
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'irregularidades_detectadas_cod_adicional',
            'attributes' => array(
                 'style' => 'width:36px;',
                )
        ));       
        
        /*TERMINO*/
        
        /*Origen Afecta a la Medida*/
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_afecta_select',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione Tipo',
                    1 => 'Falta Propia Medidor',
                    2 => 'Intervención Terceros',
                    3 => 'Otro Factor',
                  ),
                 'style' => 'width:190px;',
                )
              ));
                     
        
        $this->add(array( 
            'name' => 'origen_afecta_comentario', 
            'type' => 'Zend\Form\Element\Textarea', 
            'attributes' => array( 
                 'rows'=> '5',
                 'cols' => '300',
                 'style' => 'width:578px;',
                ), 
              'options' => array( 
                ), 
            ));
        
        /*TERMINO*/
        
        
        
        /* Remplazo elemento empalme */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiomedidor_numeroserie',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiomedidor_marca',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiomedidor_codigomodelo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiomedidor_propiedad',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meidorprovisorio_numeroserie',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meidorprovisorio_marca',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meidorprovisorio_codigomodelo',
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meidorprovisorio_propiedad',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiolimitador_numeroserie',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiolimitador_marca',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiolimitador_codigomodelo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiolimitador_propiedad',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiottcc_numeroserie',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiottcc_marca',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiottcc_codigomodelo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambiottcc_propiedad',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambioregletablock_numeroserie',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambioregletablock_marca',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambioregletablock_codigomodelo',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cambioregletablock_propiedad',
        ));


        /* TERMINO */
        
        
        /* LECTURA MEDIDOR INSTALADO */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwh',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kvar',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwpunta',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwsum',
        ));

        /* TERMINO */
        
        
        /*QUIEN AUTORIZA CLIENTE*/
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cliente_nombre',
        ));            
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cliente_rut',
        ));
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cliente_telefono',
        ));        
        
        /*TERMINO*/

        
        /*QUIEN EJECUTA*/
        
           
        
        
        /*TERMINO*/

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg btn-block',
                'style' => 'width:210px;height:50px;',
            ),
        ));
    }

}

