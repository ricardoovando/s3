<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectBrigadas extends Form {

    public function __construct($name = null) {
        parent::__construct('selectBrigadas');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'brigadas',
            'options' => array(
                'label' => '',
                'empty_option' => 'Seleccione una brigada',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Asignar',
                'class' => 'btn btn-success',
                'style' => 'height:30px;',
            ),
        ));
        
        $this->add(array(
            'name' => 'buscarbrigada',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar Brigada',
                'class' => 'btn btn-info',
                'style' => 'height:30px;',
            ),
        ));
    }

}

