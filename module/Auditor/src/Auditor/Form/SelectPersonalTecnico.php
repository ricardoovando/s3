<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectPersonalTecnico extends Form {

    public function __construct($name = null) {
        parent::__construct('selectPersonalTecnico');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico',
            'options' => array(
                'label' => '',
                'empty_option' => 'Personal Tecnico',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Asignar',
                'class' => 'btn btn-warning',
                'style' => 'height:30px;',
            ),
        ));
        
       $this->add(array(
            'name' => 'buscartecnico',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        )); 
       
       $this->add(array(
            'name' => 'informe_asignacion',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Informe Asignación',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));

    }

}

