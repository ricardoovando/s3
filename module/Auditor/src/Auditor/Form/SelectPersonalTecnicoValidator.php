<?php

namespace Avisos\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\NotEmpty;


class SelectPersonalTecnicoValidator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => false,
        'messages' => array(
            Alnum::INVALID => "Tipo inválido dado. Cadena, un entero o flotante esperada",
            Alnum::NOT_ALNUM => "La entrada contiene caracteres que no son alfabéticos y sin dígitos",
            Alnum::STRING_EMPTY => "La entrada es una cadena vacía",
        )
    );
    protected $opcionesStringLength = array(
        'min' => 1,
        'max' => 15,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener tener al menos 1 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 8 caracteres",
        )
    );
    protected $optionesNotEmpty = array(
        'messages' => array(
            NotEmpty::IS_EMPTY => "Se requiere de valor y no puede estar vacío",
            NotEmpty::INVALID => "Tipo inválido dado. Cadena, entero, flotante, booleano o matriz esperada",
        ),
    );


    public function __construct() {

        $this->add(
                array(
                    'name' => 'persona_tecnico',
                    'required' => true,
                    'filters' => array(
                          array( 'name' => 'StringToLower' ),
                          array( 'name' => 'StringTrim' ),
                          array( 'name' => 'StripTags' ),
                          array( 'name' => 'StripNewlines' ),
                          array( 'name' => 'HtmlEntities' ),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum,
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
                    ),
                )
        );

    }

}