<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectPoblacion extends Form {

    public function __construct($name = null) {
        parent::__construct('selectPoblacion');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'poblacion',
            'options' => array(
                'label' => '',
                'empty_option' => 'Comunas',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));


    }

}

