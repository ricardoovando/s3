<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectPrioridad extends Form {

    public function __construct($name = null) {
        parent::__construct('selectPrioridad');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'prioridad',
            'options' => array(
                'label' => '',
                'empty_option' => 'Prioridad',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'options' => array(
                    1 => 'Urgente => 1',
                    2 => 'Especial => 2',
                    3 => 'Normal => 3',
                 ),
              ),
          ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));


    }

}

