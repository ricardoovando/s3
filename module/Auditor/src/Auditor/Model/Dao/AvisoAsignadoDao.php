<?php

namespace Avisos\Model\Dao;

use Zend\Db\TableGateway\TableGateway;

/* ENTITYS */
use Avisos\Model\Entity\AvisoAsignado;

class AvisoAsignadoDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();        
    }

    public function obtenerPorId($id) {

        $id = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('avisos_id_aviso' => $id));
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        return $row;
        
    }

    public function guardarAvisoAsignado(AvisoAsignado $avisoasignado) {

        $data = array(
            'avisos_id_aviso' => $avisoasignado->getAvisos_id_aviso(),
            'brigadas_id_brigada' => $avisoasignado->getBrigadas_id_brigada(),
           );
        
        $id = $avisoasignado->getAvisos_id_aviso();

        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('avisos_id_aviso' => $id));
        } else {
            $this->tableGateway->insert($data);
        }
    }

}
