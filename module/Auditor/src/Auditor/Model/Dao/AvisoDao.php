<?php

namespace Avisos\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* Entity */
use Avisos\Model\Entity\Aviso;

class AvisoDao {

    protected $tableGateway;
    protected $login;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway, $login, Adapter $dbAdapter) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
        $this->dbAdapter = $dbAdapter;
    }

    public function obtenerTodos() {

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
        
        $select->join(array('od' => 'ors_riat'),'od.avisos_id_aviso = avisos.id_aviso', array('num_form_riat' => 'num_form_riat','fecha_atencion_servicio' => 'fecha_atencion_servicio' , 'estado_riat' => 'estado_riat' ));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.estados_avisos_id_estado_aviso <> 4'));

        $select->order('avisos.estados_avisos_id_estado_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {

        $id = (int) $id;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.id_aviso' => $id));

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        return $row;
    }

    public function obtenerPorPrioridad($id) {

        $id = (int) $id;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.prioridad' => $id));

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorIdServicio($id) {

        $id = (int) $id;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.servicios_id_servicio' => $id));

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorNumAviso($num_aviso) {

        $num_aviso = (String) $num_aviso;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where->like('numero_aviso', '%' . $num_aviso . '%');

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorNumInstalacion($num_instalacion) {

        $num_instalacion = (String) $num_instalacion;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where->like('inst.num_instalacion', '%' . $num_instalacion . '%');

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorPoblacion($nom_poblacion) {

        $nom_poblacion = (String) $nom_poblacion;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where->like('inst.nom_poblacion', '%' . $nom_poblacion . '%');

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorTecnicoAviso($id_tecnico) {

        $id_tecnico = (String) $id_tecnico;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.responsable_personal_id_persona' => $id_tecnico));

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerAvisosAsignadosTecnico($id_tecnico) {

        $id_tecnico = (String) $id_tecnico;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('avisos.responsable_personal_id_persona' => $id_tecnico));

        $select->where(array('est.id_estado_aviso' => 2));

        $select->order('id_aviso ASC');

        return $this->tableGateway->selectWith($select);
    }

    public function obtenerPorNumUnidadLectura($numerounidadlectura) {

        $numerounidadlectura = (String) $numerounidadlectura;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where->like('inst.unidad_lectura', '%' . $numerounidadlectura . '%');

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorEstadoAviso($estado_aviso) {

        $estado_aviso = (int) $estado_aviso;

        $id_usuario = $this->login->id;

        $select = $this->tableGateway->getSql()->select();

        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $select->where(array('est.id_estado_aviso' => $estado_aviso));

        $select->order('id_aviso ASC');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

//    public function obtenerPorNumBrigada($id) {
//
//        $id = (int) $id;
//
//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
//        $select->join(array('avas' => 'avisos_asignados'), 'avas.avisos_id_aviso = avisos.id_aviso', array('brigadas_id_brigada' => 'brigadas_id_brigada'), 'left');
//        $select->join(array('bri' => 'brigadas'), 'avas.brigadas_id_brigada = bri.id_brigada', array('nombre_brigada' => 'nombre_brigada'), 'left');
//        $select->where(array('avisos.sucursales_id_sucursal' => 1));
//
//        $select->where(array('bri.id_brigada' => $id));
//        $select->order('id_aviso ASC');
//
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
//    }

    public function obtenerPorNumAviso_2($num_aviso) {

        $num_aviso = (String) $num_aviso;

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('numero_aviso' => $num_aviso));

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        return $row;
    }

    public function guardar(Aviso $aviso) {

        $data = array(
            'sucursales_id_sucursal' => $aviso->getSucursales_id_sucursal(),
            'responsable_personal_id_persona' => $aviso->getResponsable_personal_id_persona(),
            'supervisor_personal_id_persona' => $aviso->getSupervisor_personal_id_persona(),
            'instalaciones_id_instalacion' => $aviso->getInstalaciones_id_instalacion(),
            'estados_avisos_id_estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'numero_aviso' => $aviso->getNumero_aviso(),
            'descripcion' => $aviso->getDescripcion(),
            'tipo_aviso' => $aviso->getTipo_aviso(),
            //'fecha_carga'  => $aviso->getFecha_carga(),
            'fecha_asignacion' => $aviso->getFecha_asignacion(),
            'fecha_valorizacion' => $aviso->getFecha_valorizacion(),
            'fecha_finalizacion' => $aviso->getFecha_finalizacion(),
            'vehiculos_id_vehiculo' => $aviso->getVehiculos_id_vehiculo(),
            'subot' => $aviso->getSubot(),
            'solicitante_id_solicitante' => $aviso->getSolicitante_id_solicitante(),
            'prioridad' => $aviso->getPrioridad(),
            'cuenta_contrato' => $aviso->getCuenta_contrato(),
            'servicios_id_servicio' => $aviso->getServicios_id_servicio(),
        );

        $id = (int) $aviso->getId_aviso();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_aviso' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function updateNumAviso(Aviso $aviso) {

        $data = array(
            //'numero_aviso' => $aviso->getNumero_aviso(),
            'responsable_personal_id_persona' => $aviso->getResponsable_personal_id_persona(),
            'tipo_aviso' => $aviso->getTipo_aviso(),
        );

        $id = (int) $aviso->getId_aviso();

        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('id_aviso' => $id));
        } else {
            throw new \Exception('Form id does not exist');
        }
    }

    public function updateTecnicoResponsableAsignado(Aviso $aviso) {

        $data = array(
            'responsable_personal_id_persona' => $aviso->getResponsable_personal_id_persona(),
            'estados_avisos_id_estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'fecha_asignacion' => $aviso->getFecha_asignacion()
        );

        $id = (int) $aviso->getId_aviso();

        if ($this->obtenerPorId($id)) {

            $this->tableGateway->update($data, array(
                'id_aviso' => $id,
                'estados_avisos_id_estado_aviso' => 1,
                new \Zend\Db\Sql\Predicate\IsNull('responsable_personal_id_persona'),
                    )
            );
        } else {
            throw new \Exception('Form id does not exist');
        }
    }

    public function updateConfirmaValoriza(Aviso $aviso) {

        $data = array(
            'estados_avisos_id_estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'fecha_valorizacion' => $aviso->getFecha_valorizacion(),
        );

        $id = (int) $aviso->getId_aviso();

        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('id_aviso' => $id));
        } else {
            throw new \Exception('Form id does not exist');
        }
    }

    public function updateConfirmaFinalizacion(Aviso $aviso) {

        $data = array(
            'estados_avisos_id_estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'fecha_finalizacion' => $aviso->getFecha_finalizacion(),
        );

        $id = (int) $aviso->getId_aviso();

        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('id_aviso' => $id));
        } else {
            throw new \Exception('Form id does not exist');
        }
    }

//    public function obtenerBrigadas() {
//
//        $sql = new Sql($this->tableGateway->getAdapter());
//        $select = $sql->select();
//        $select->from('brigadas');
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $results = $statement->execute();
//        $brigadas = new \ArrayObject();
//
//        foreach ($results as $row) {
//            $brigada = new \Brigadas\Model\Entity\Brigada();
//            $brigada->exchangeArray($row);
//            $brigadas->append($brigada);
//        }
//
//        return $brigadas;
//    }
//    public function obtenerBrigadasSelect() {
//
//        $brigadas = $this->obtenerBrigadas();
//
//        $result = array();
//
//        foreach ($brigadas as $brigada) {
//
//            $result[$brigada->getId_brigada()] = $brigada->getNombre_brigada();
//        }
//
//        return $result;
//    }



    /*
     * 
     * INFORMES DE PRODUCCION SON VISTAS EN LA BASE DE DATOS EN EL ESQUEMA AVISOS 
     * 
     *
     */

    public function obtenerListadoServiciosSucursal() {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('listado_servicios_sucursales');

        if ($sucursales_id_sucursal !== 1) {
            $select->where(array('id_sucursal' => $sucursales_id_sucursal));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $listado_servicios = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\ListadoServiciosSucursales();
            $obj->exchangeArray($row);
            $listado_servicios->append($obj);
        }

        return $listado_servicios;
    }

    public function obtenerProduccionMensual() {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('resumen_produccion');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resumen_produccion = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\ResumenProduccion;
            $obj->exchangeArray($row);
            $resumen_produccion->append($obj);
        }

        return $resumen_produccion;
    }

    public function obtenerServiciosNacional() {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('servicios');
        $select->order('id_servicio ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $servicios = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Servicios\Model\Entity\Servicio();
            $obj->exchangeArray($row);
            $servicios->append($obj);
        }

        return $servicios;
    }

    public function obtenerModelosMedidores() {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('modelos_medidores_new');
        $select->order('id_modelo ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $modelos_medidores = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Instalaciones\Model\Entity\ModelosMedidores();
            $obj->exchangeArray($row);
            $modelos_medidores->append($obj);
        }

        return $modelos_medidores;
    }

    public function obtenerProduccionSemanal() {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('informe_auditoria_trifasico_semanal');
        $select->where(array('id_sucursal' => $sucursales_id_sucursal));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $produccion_semanal = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\InformeProduccionSemanal();
            $obj->exchangeArray($row);
            $produccion_semanal->append($obj);
        }

        return $produccion_semanal;
    }

    public function ObtenerRegionesInformeEjecutivo() {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('regiones_informe_ejecutivo');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $regiones = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\Region();
            $obj->exchangeArray($row);
            $regiones->append($obj);
        }

        return $regiones;
    }

    public function ObtenerInstalacionesAuditadasRegiones($mes, $anio) {

        $mes = (int) $mes;
        $anio = (int) $anio;

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('instalaciones_auditadas_region');
        $select->where(array('mes' => $mes, 'anio' => $anio));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $total_auditados = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\TotalAvisosAuditados();
            $obj->exchangeArray($row);
            $total_auditados->append($obj);
        }

        return $total_auditados;
    }

    public function ObtenerResultadosAuditoriasTotalesEstadosRegiones($mes, $anio) {
        
        $mes = (int) $mes; 
        $anio = (int) $anio;

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('resultados_auditorias_totales_estados_regiones');
        $select->where(array('mes' => $mes, 'anio' => $anio));


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultados_auditorias = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\ResultadoEstadoAuditoria();
            $obj->exchangeArray($row);
            $resultados_auditorias->append($obj);
        }

        return $resultados_auditorias;
    }

    public function ObtenerResultadosAuditoriasTotalesEstadosPorcentajesRegiones($mes, $anio) {
        
        $mes = (int) $mes; 
        $anio = (int) $anio;
 
//        $sql = new Sql($this->tableGateway->getAdapter());
//
//        $select = $sql->select();
//        $select->from('resultado_auditorias_totales_estados_porcentajes');
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $results = $statement->execute();

         $statement = $this->dbAdapter->query("SELECT * FROM total_auditorias_regiones_porcentajes({$mes})");
         $results = $statement->execute();
         $resultados_auditorias = new \ArrayObject();

         foreach ($results as $row) {
            $obj = new \Avisos\Model\Entity\ResultadoEstadoAuditoriaPorcentaje();
            $obj->exchangeArray($row);
            $resultados_auditorias->append($obj);
           }
           
         return $resultados_auditorias;
            
      }

      
    public function ObtenerSumResumenEjecutivo($mes,$anio) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $mes = (int) $mes; 
        $anio = (int) $anio;
        
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('sum_resumen_ejecutivo_anormalidades_cnr');
        $select->where(array('mes' => $mes, 'anio' => $anio));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultados = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\SumResumenEjecutivoAnormalidadesCnr();
            $obj->exchangeArray($row);
            $resultados->append($obj);
        }

        return $resultados;
    }

    public function ObtenerResumenEjecutivo($id_region,$mes,$anio) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('resumen_ejecutivo');
        $select->where(array('id_region' => $id_region));
        $select->where(array('mes' => $mes, 'anio' => $anio));
        $select->order('nombre_region ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultados = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\ResumenEjecutivo();
            $obj->exchangeArray($row);
            $resultados->append($obj);
        }

        return $resultados;
    }

    public function ObtenerMesesEjecutados() {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('meses_ejecutados');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultados = new \ArrayObject();

        foreach ($results as $row) {

            $obj = new \Avisos\Model\Entity\MesesEjecutados();
            $obj->exchangeArray($row);
            $resultados->append($obj);
        }

        return $resultados;
    }

    /*     * *********************************************************************** */

    public function obtenerSucursales() {

        $id_usuario = $this->login->id;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('sucursales');
        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = sucursales.id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $sucursales = new \ArrayObject();

        foreach ($results as $row) {

            $sucursal = new \Avisos\Model\Entity\Sucursal;
            $sucursal->exchangeArray($row);
            $sucursales->append($sucursal);
        }

        return $sucursales;
    }

    public function obtenerSucursalesSelect() {

        $sucursales = $this->obtenerSucursales();

        $result = array();

        foreach ($sucursales as $sucursal) {

            $result[$sucursal->getId_sucursal()] = $sucursal->getNombre_sucursal() . ' (' . $sucursal->getId_sucursal() . ')';
        }

        return $result;
    }

    public function obtenerEstados() {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('estados_avisos');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $estados_avisos = new \ArrayObject();

        foreach ($results as $row) {

            $estado = new \Avisos\Model\Entity\EstadoAviso;
            $estado->exchangeArray($row);
            $estados_avisos->append($estado);
        }

        return $estados_avisos;
    }

    public function obtenerEstadosSelect() {

        $estados_avisos = $this->obtenerEstados();

        $result = array();

        foreach ($estados_avisos as $estado) {

            $result[$estado->getId_estado_aviso()] = $estado->getNombreEstadoAviso();
        }

        return $result;
    }

    public function obtenerPersonalTecnico() {

        $id_usuario = $this->login->id;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('personaltecnico');
        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = personaltecnico.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $personal_tecnico = new \ArrayObject();

        foreach ($results as $row) {

            $tecnico = new \Personal\Model\Entity\Persona;
            $tecnico->exchangeArray($row);
            $personal_tecnico->append($tecnico);
        }

        return $personal_tecnico;
    }

    public function obtenerPersonalTecnicoSelect() {

        $personal_tecnico = $this->obtenerPersonalTecnico();

        $result = array();

        foreach ($personal_tecnico as $tecnico) {

            $result[$tecnico->getId_persona()] = ucfirst(strtolower($tecnico->getNombres())) . ' ' . ucfirst(strtolower($tecnico->getApellidos())) . ' (' . $tecnico->getSucursales_id_sucursal() . ')';
        }

        return $result;
    }

    public function obtenerPoblaciones() {

        $id_usuario = $this->login->id;

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('comunas_instalacion');

        $select->join(array('sucdis' => 'sucursal_distribuidoras'), 'sucdis.distribuidoras_id_distribuidora = comunas_instalacion.id_distribuidora', array());

        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = sucdis.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $comunas_instalacion = new \ArrayObject();

        foreach ($results as $row) {

            $comuna = new \Avisos\Model\Entity\ComunaInstalacion;
            $comuna->exchangeArray($row);
            $comunas_instalacion->append($comuna);
        }

        return $comunas_instalacion;
    }

    public function obtenerPoblacionesSelect() {

        $comunas_instalacion = $this->obtenerPoblaciones();

        $result = array();

        foreach ($comunas_instalacion as $comuna) {

            $result[$comuna->getComunas()] = $comuna->getComunas();

            //ucfirst(strtolower($comuna->getComunas()));
        }

        return $result;
    }

//    public function eliminar(Usuario $usuario) {
//        $this->tableGateway->delete(array('id' => $usuario->getId()));
//    }    
//    public function buscarPorNombre($nombre) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where->like('nombre', '%' . $nombre . '%');
//        $select->order("nombre");
//
//        return $this->tableGateway->selectWith($select);
//    }
//    public function obtenerCuenta($email, $clave) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where(array('email' => $email, 'clave' => $clave,));
//
//        return $this->tableGateway->selectWith($select)->current();
//    }


    public function obtenerDistribuidoras($SucId) {

        $SucId = (int) $SucId;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('distribuidoras');
        $select->join(array('sucdis' => 'sucursal_distribuidoras'), new \Zend\Db\Sql\Expression('sucdis.distribuidoras_id_distribuidora = distribuidoras.id_distribuidora AND sucdis.sucursales_id_sucursal = ' . $SucId . ''), array());
        $select->order('distribuidoras.id_distribuidora ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $distribuidoras = new \ArrayObject();

        foreach ($results as $row) {
            $distribuidora = new \Instalaciones\Model\Entity\Distribuidoras();
            $distribuidora->exchangeArray($row);
            $distribuidoras->append($distribuidora);
        }

        $result = array();

        foreach ($distribuidoras as $reg) {
            $result[$reg->getIddistribuidora()] = ucwords(strtolower($reg->getNombre()));
        }

        return $result;
    }

    public function obtenerSolicitantes($IdDis) {

        $IdDis = (int) $IdDis;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('solicitantes');
        $select->where(array('distribuidoras_id_distribuidora' => $IdDis));
        $select->order('id_solicitante_distribuidora ASC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $solicitantes = new \ArrayObject();

        foreach ($results as $row) {
            $solicitante = new \Avisos\Model\Entity\Solicitante();
            $solicitante->exchangeArray($row);
            $solicitantes->append($solicitante);
        }

        $result = array();

        foreach ($solicitantes as $reg) {
            $result[$reg->getId_solicitante_distribuidora()] = ucwords(strtolower($reg->getNombre_completo())) . ' ( ' . ucwords(strtolower($reg->getDepartamento())) . ' )';
        }

        return $result;
    }

}
