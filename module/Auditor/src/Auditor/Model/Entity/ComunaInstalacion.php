<?php

namespace Avisos\Model\Entity;

class ComunaInstalacion {

    private $id_distribuidora;
    private $comunas;

    public function __construct( $comunas = null, $id_distribuidora = null ) {
        $this->id_distribuidora = $id_distribuidora;
        $this->comunas = $comunas;
    }  
    
    public function getId_distribuidora() {
        return $this->id_distribuidora;
    }

    public function setId_distribuidora($id_distribuidora) {
        $this->id_distribuidora = $id_distribuidora;
    }
       
    public function getComunas() {
        return $this->comunas;
    }

    public function setComunas($comunas) {
        $this->comunas = $comunas;
    }
           
    public function exchangeArray($data) {
        $this->id_distribuidora = (isset($data['id_distribuidora'])) ? $data['id_distribuidora'] : null;
        $this->comunas = (isset($data['comunas'])) ? $data['comunas'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

