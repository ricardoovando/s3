<?php

namespace Avisos\Model\Entity;

class DetalleServicioPrestado {

    private $avisos_id_aviso;
    private $servicios_id_servicio;
    private $precio_uf;
    private $cantidad;
    private $total_precio_uf;

    

    /*Objeto que relaciona con la tabla servicio*/
    private $servicio;
    
    function __construct($avisos_id_aviso = null, $servicios_id_servicio = null, $precio_uf = null, $cantidad = null, $total_precio_uf = null) {
        $this->avisos_id_aviso = $avisos_id_aviso;
        $this->servicios_id_servicio = $servicios_id_servicio;
        $this->precio_uf = $precio_uf;
        $this->cantidad = $cantidad;
        $this->total_precio_uf = $total_precio_uf;
    }

    public function getAvisos_id_aviso() {
        return $this->avisos_id_aviso;
    }

    public function setAvisos_id_aviso($avisos_id_aviso) {
        $this->avisos_id_aviso = $avisos_id_aviso;
    }

    public function getServicios_id_servicio() {
        return $this->servicios_id_servicio;
    }

    public function setServicios_id_servicio($servicios_id_servicio) {
        $this->servicios_id_servicio = $servicios_id_servicio;
    }

    public function getPrecio_uf() {
        return $this->precio_uf;
    }

    public function setPrecio_uf($precio_uf) {
        $this->precio_uf = $precio_uf;
    }

    public function getCantidad() {
        return $this->cantidad;
    }
    
    public function getTotal_precio_uf() {
        return $this->total_precio_uf;
    }

    public function setTotal_precio_uf($total_precio_uf) {
        $this->total_precio_uf = $total_precio_uf;
    }


    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function getServicio() {
        return $this->servicio;
    }

    public function setServicio(Servicio $servicio) {
        $this->servicio = $servicio;
    }
        
    public function exchangeArray($data) {

        $this->avisos_id_aviso = (isset($data['avisos_id_aviso'])) ? $data['avisos_id_aviso'] : null;
        $this->servicios_id_servicio = (isset($data['servicios_id_servicio'])) ? $data['servicios_id_servicio'] : null;
        $this->precio_uf = (isset($data['precio_uf'])) ? $data['precio_uf'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        
        $this->total_precio_uf = (isset($data['total_precio_uf'])) ? $data['total_precio_uf'] : null;
        
        $this->servicio = new \Servicios\Model\Entity\Servicio();
        $this->servicio->setNombre_servicio( (isset($data['nombre_servicio'])) ? $data['nombre_servicio'] : null );

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

