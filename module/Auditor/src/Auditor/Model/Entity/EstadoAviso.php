<?php

namespace Avisos\Model\Entity;

class EstadoAviso {

    private $id_estado_aviso;
    private $nombre_estado_aviso;
    private $clase_color;

    public function __construct(
    $id_estado_aviso = null, $nombre_estado_aviso = null, $clase_color = null
    ) {
        $this->id_estado_aviso = $id_estado_aviso;
        $this->nombre_estado_aviso = $nombre_estado_aviso;
        $this->clase_color = $clase_color;
    }

    public function getId_estado_aviso() {
        return $this->id_estado_aviso;
    }

    public function setId_estado_aviso($id_estado_aviso) {
        $this->id_estado_aviso = $id_estado_aviso;
    }

    public function getNombreEstadoAviso() {
        return $this->nombre_estado_aviso;
    }

    public function setNombreEstadoAviso($nombre_estado_aviso) {
        $this->nombre_estado_aviso = $nombre_estado_aviso;
    }

    public function getClase_color() {
        return $this->clase_color;
    }

    public function setClase_color($clase_color) {
        $this->clase_color = $clase_color;
    }

    public function exchangeArray($data) {
        $this->id_estado_aviso = (isset($data['id_estado_aviso'])) ? $data['id_estado_aviso'] : null;
        $this->nombre_estado_aviso = (isset($data['nombre_estado_aviso'])) ? $data['nombre_estado_aviso'] : null;
        $this->clase_color = (isset($data['clase_color'])) ? $data['clase_color'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

