<?php

namespace Avisos\Model\Entity;

class ListadoServiciosSucursales {

    private $id_sucursal;
    private $nombre_sucursal;
    private $id_servicio;
    private $nombre_servicio;
    private $precio_uf;

    function __construct($id_sucursal = null, $nombre_sucursal = null, $id_servicio = null, $nombre_servicio = null, $precio_uf = null) {
        $this->id_sucursal = $id_sucursal;
        $this->nombre_sucursal = $nombre_sucursal;
        $this->id_servicio = $id_servicio;
        $this->nombre_servicio = $nombre_servicio;
        $this->precio_uf = $precio_uf;
    }
    
    public function getId_sucursal() {
        return $this->id_sucursal;
    }

    public function getNombre_sucursal() {
        return $this->nombre_sucursal;
    }

    public function getId_servicio() {
        return $this->id_servicio;
    }

    public function getNombre_servicio() {
        return $this->nombre_servicio;
    }

    public function getPrecio_uf() {
        return $this->precio_uf;
    }

    public function setId_sucursal($id_sucursal) {
        $this->id_sucursal = $id_sucursal;
    }

    public function setNombre_sucursal($nombre_sucursal) {
        $this->nombre_sucursal = $nombre_sucursal;
    }

    public function setId_servicio($id_servicio) {
        $this->id_servicio = $id_servicio;
    }

    public function setNombre_servicio($nombre_servicio) {
        $this->nombre_servicio = $nombre_servicio;
    }

    public function setPrecio_uf($precio_uf) {
        $this->precio_uf = $precio_uf;
    }

    public function exchangeArray($data) {
        $this->id_sucursal = (isset($data['id_sucursal'])) ? $data['id_sucursal'] : null;
        $this->nombre_sucursal = (isset($data['nombre_sucursal'])) ? $data['nombre_sucursal'] : null;
        $this->id_servicio = (isset($data['id_servicio'])) ? $data['id_servicio'] : null;
        $this->nombre_servicio = (isset($data['nombre_servicio'])) ? $data['nombre_servicio'] : null;
        $this->precio_uf = (isset($data['precio_uf'])) ? $data['precio_uf'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
