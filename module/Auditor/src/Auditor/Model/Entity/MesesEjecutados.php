<?php

namespace Avisos\Model\Entity;

class MesesEjecutados {

    private $meses_ejecutivo;
    private $anios_ejecutivo;
    private $fechas;

    function __construct($meses_ejecutivo = null, $anios_ejecutivo = null, $fechas = null) {
        $this->meses_ejecutivo = $meses_ejecutivo;
        $this->anios_ejecutivo = $anios_ejecutivo;
        $this->fechas = $fechas;
    }

    public function getMeses_ejecutivo() {
        return $this->meses_ejecutivo;
    }

    public function getAnios_ejecutivo() {
        return $this->anios_ejecutivo;
    }

    public function getFechas() {
        return $this->fechas;
    }

    public function setMeses_ejecutivo($meses_ejecutivo) {
        $this->meses_ejecutivo = $meses_ejecutivo;
    }

    public function setAnios_ejecutivo($anios_ejecutivo) {
        $this->anios_ejecutivo = $anios_ejecutivo;
    }

    public function setFechas($fechas) {
        $this->fechas = $fechas;
    }
    
    public function exchangeArray($data) {
        $this->meses_ejecutivo = (isset($data['meses_ejecutivo'])) ? $data['meses_ejecutivo'] : null;
        $this->anios_ejecutivo = (isset($data['anios_ejecutivo'])) ? $data['anios_ejecutivo'] : null;
        $this->fechas = (isset($data['fechas'])) ? $data['fechas'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

