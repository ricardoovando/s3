<?php

namespace Avisos\Model\Entity;

class Region {

  private $id_region;
  private $nombre_region;
  private $cod_region;
  private $nombre_corto;

  function __construct($id_region = null, $nombre_region = null, $nombre_corto = null, $cod_region = null) {
      $this->id_region = $id_region;
      $this->nombre_region = $nombre_region;
      $this->cod_region = $cod_region;
      $this->nombre_corto = $nombre_corto;
  }

  public function getId_region() {
      return $this->id_region;
  }

  public function setId_region($id_region) {
      $this->id_region = $id_region;
  }

  public function getNombre_region() {
      return $this->nombre_region;
  }

  public function setNombre_region($nombre_region) {
      $this->nombre_region = $nombre_region;
  }
  
  public function getNombre_corto() {
      return $this->nombre_corto;
  }

  public function setNombre_corto($nombre_corto) {
      $this->nombre_corto = $nombre_corto;
  }
  
  public function getCod_region() {
      return $this->cod_region;
  }

  public function setCod_region($cod_region) {
      $this->cod_region = $cod_region;
  }

  public function exchangeArray($data) {
      $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
      $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
      $this->cod_region = (isset($data['cod_region'])) ? $data['cod_region'] : null;
      $this->nombre_corto = (isset($data['nombre_corto'])) ? $data['nombre_corto'] : null;
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

