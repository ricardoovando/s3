<?php

namespace Avisos\Model\Entity;

class ResultadoEstadoAuditoria {
    
    private $mes;
    private $anio;
    private $region;
    private $orden;
    private $resultado;
    private $cantidad;

    function __construct($mes = null,$anio = null,$region = null, $orden = null, $resultado = null, $cantidad = null) {
        $this->mes = $mes;
        $this->anio = $anio;
        $this->region = $region;
        $this->orden = $orden;
        $this->resultado = $resultado;
        $this->cantidad = $cantidad;
    }
    
    public function getMes() {
        return $this->mes;
    }

    public function getAnio() {
        return $this->anio;
    }
    
    public function getRegion() {
        return $this->region;
    }

    public function getOrden() {
        return $this->orden;
    }

    public function getResultado() {
        return $this->resultado;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function setRegion($region) {
        $this->region = $region;
    }

    public function setOrden($orden) {
        $this->orden = $orden;
    }

    public function setResultado($resultado) {
        $this->resultado = $resultado;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    
    public function exchangeArray($data) {
        $this->mes = (isset($data['mes'])) ? $data['mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->region = (isset($data['region'])) ? $data['region'] : null;
        $this->orden = (isset($data['orden'])) ? $data['orden'] : null;
        $this->resultado = (isset($data['resultado'])) ? $data['resultado'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;    
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

