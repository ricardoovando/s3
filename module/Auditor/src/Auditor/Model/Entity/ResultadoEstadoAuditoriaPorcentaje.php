<?php

namespace Avisos\Model\Entity;

class ResultadoEstadoAuditoriaPorcentaje {

    private $id;
    private $region;
    private $orde;
    private $resultado;
    private $cantidad;
    private $total;
    private $porcentaje;

    function __construct($id = null, $region = null, $orde = null, $resultado = null, $cantidad = null, $total = null, $porcentaje = null) {
        $this->id = $id;
        $this->region = $region;
        $this->orde = $orde;
        $this->resultado = $resultado;
        $this->cantidad = $cantidad;
        $this->total = $total;
        $this->porcentaje = $porcentaje;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getRegion() {
        return $this->region;
    }

    public function setRegion($region) {
        $this->region = $region;
    }

    public function getOrde() {
        return $this->orde;
    }

    public function setOrde($orde) {
        $this->orde = $orde;
    }

    public function getResultado() {
        return $this->resultado;
    }

    public function setResultado($resultado) {
        $this->resultado = $resultado;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function getPorcentaje() {
        return $this->porcentaje;
    }

    public function setPorcentaje($porcentaje) {
        $this->porcentaje = $porcentaje;
    }
    
    public function exchangeArray($data) {
        
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->region = (isset($data['region'])) ? $data['region'] : null;
        $this->orde = (isset($data['order'])) ? $data['order'] : null;
        $this->resultado = (isset($data['resultado'])) ? $data['resultado'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->total = (isset($data['total'])) ? $data['total'] : null;
        $this->porcentaje = (isset($data['porcentaje'])) ? $data['porcentaje'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

