<?php

namespace Avisos\Model\Entity;

class ResumenEjecutivo {

    private $id_region;
    private $nombre_region;
    private $nombre_corto;
    private $num_cliente;
    private $aviso;
    private $fecha_ejecucion;
    private $nombre_cliente;
    private $direccion;
    private $comuna;
    private $tarifa;
    private $tipo_medida;
    private $constante_distribuidora;
    private $constante_terreno;
    private $resultadoauditorias;
    private $observacion;
    private $oficina;
    private $num_riat;
    private $ejecudado;
    private $anor_encajadeempalme;
    private $anor_enecm;
    private $anor_enmedidor;
    private $anor_esquemademedida;
    private $anor_faltamedidareactiva;
    private $anor_alarmadebateria;
    private $anor_cargabajavariable;
    private $anor_tarifanocorresponde;
    private $anor_faltasello;
    private $anor_ttcc;
    private $anor_otro;
    private $clas_cnr_1;
    private $clas_cnr_2;
    private $clas_cnr_3;
    private $clas_cnr_4;
    private $clas_cnr_5;
    private $clas_cnr_6;
    private $clas_cnr_7;
    private $clas_cnr_8;

    function __construct($id_region = null, $nombre_region = null, $nombre_corto = null, $num_cliente = null, $aviso = null, $fecha_ejecucion = null, $nombre_cliente = null, $direccion = null, $comuna = null, $tarifa = null, $tipo_medida = null, $constante_distribuidora = null, $constante_terreno = null, $resultadoauditorias = null, $observacion = null, $oficina = null, $num_riat = null, $ejecudado = null, $anor_encajadeempalme = null, $anor_enecm = null, $anor_enmedidor = null, $anor_esquemademedida = null, $anor_faltamedidareactiva = null, $anor_alarmadebateria = null, $anor_cargabajavariable = null, $anor_tarifanocorresponde = null, $anor_faltasello = null, $anor_ttcc = null, $anor_otro = null, $clas_cnr_1 = null, $clas_cnr_2 = null, $clas_cnr_3 = null, $clas_cnr_4 = null, $clas_cnr_5 = null, $clas_cnr_6 = null, $clas_cnr_7 = null, $clas_cnr_8 = null) {
        $this->id_region = $id_region;
        $this->nombre_region = $nombre_region;
        $this->nombre_corto = $nombre_corto;
        $this->num_cliente = $num_cliente;
        $this->aviso = $aviso;
        $this->fecha_ejecucion = $fecha_ejecucion;
        $this->nombre_cliente = $nombre_cliente;
        $this->direccion = $direccion;
        $this->comuna = $comuna;
        $this->tarifa = $tarifa;
        $this->tipo_medida = $tipo_medida;
        $this->constante_distribuidora = $constante_distribuidora;
        $this->constante_terreno = $constante_terreno;
        $this->resultadoauditorias = $resultadoauditorias;
        $this->observacion = $observacion;
        $this->oficina = $oficina;
        $this->num_riat = $num_riat;
        $this->ejecudado = $ejecudado;
        $this->anor_encajadeempalme = $anor_encajadeempalme;
        $this->anor_enecm = $anor_enecm;
        $this->anor_enmedidor = $anor_enmedidor;
        $this->anor_esquemademedida = $anor_esquemademedida;
        $this->anor_faltamedidareactiva = $anor_faltamedidareactiva;
        $this->anor_alarmadebateria = $anor_alarmadebateria;
        $this->anor_cargabajavariable = $anor_cargabajavariable;
        $this->anor_tarifanocorresponde = $anor_tarifanocorresponde;
        $this->anor_faltasello = $anor_faltasello;
        $this->anor_ttcc = $anor_ttcc;
        $this->anor_otro = $anor_otro;
        $this->clas_cnr_1 = $clas_cnr_1;
        $this->clas_cnr_2 = $clas_cnr_2;
        $this->clas_cnr_3 = $clas_cnr_3;
        $this->clas_cnr_4 = $clas_cnr_4;
        $this->clas_cnr_5 = $clas_cnr_5;
        $this->clas_cnr_6 = $clas_cnr_6;
        $this->clas_cnr_7 = $clas_cnr_7;
        $this->clas_cnr_8 = $clas_cnr_8;
    }

    public function getId_region() {
        return $this->id_region;
    }

    public function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    public function getNombre_region() {
        return $this->nombre_region;
    }

    public function setNombre_region($nombre_region) {
        $this->nombre_region = $nombre_region;
    }
    
    public function getNombre_corto() {
        return $this->nombre_corto;
    }

    public function setNombre_corto($nombre_corto) {
        $this->nombre_corto = $nombre_corto;
    }

    public function getNum_cliente() {
        return $this->num_cliente;
    }

    public function setNum_cliente($num_cliente) {
        $this->num_cliente = $num_cliente;
    }

    public function getAviso() {
        return $this->aviso;
    }

    public function setAviso($aviso) {
        $this->aviso = $aviso;
    }

    public function getFecha_ejecucion() {
        return $this->fecha_ejecucion;
    }

    public function setFecha_ejecucion($fecha_ejecucion) {
        $this->fecha_ejecucion = $fecha_ejecucion;
    }

    public function getNombre_cliente() {
        return $this->nombre_cliente;
    }

    public function setNombre_cliente($nombre_cliente) {
        $this->nombre_cliente = $nombre_cliente;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function getComuna() {
        return $this->comuna;
    }

    public function setComuna($comuna) {
        $this->comuna = $comuna;
    }

    public function getTarifa() {
        return $this->tarifa;
    }

    public function setTarifa($tarifa) {
        $this->tarifa = $tarifa;
    }

    public function getTipo_medida() {
        return $this->tipo_medida;
    }

    public function setTipo_medida($tipo_medida) {
        $this->tipo_medida = $tipo_medida;
    }

    public function getConstante_distribuidora() {
        return $this->constante_distribuidora;
    }

    public function setConstante_distribuidora($constante_distribuidora) {
        $this->constante_distribuidora = $constante_distribuidora;
    }

    public function getConstante_terreno() {
        return $this->constante_terreno;
    }

    public function setConstante_terreno($constante_terreno) {
        $this->constante_terreno = $constante_terreno;
    }

    public function getResultadoauditorias() {
        return $this->resultadoauditorias;
    }

    public function setResultadoauditorias($resultadoauditorias) {
        $this->resultadoauditorias = $resultadoauditorias;
    }

    public function getObservacion() {
        return $this->observacion;
    }

    public function setObservacion($observacion) {
        $this->observacion = $observacion;
    }

    public function getOficina() {
        return $this->oficina;
    }

    public function setOficina($oficina) {
        $this->oficina = $oficina;
    }

    public function getNum_riat() {
        return $this->num_riat;
    }

    public function setNum_riat($num_riat) {
        $this->num_riat = $num_riat;
    }

    public function getEjecudado() {
        return $this->ejecudado;
    }

    public function setEjecudado($ejecudado) {
        $this->ejecudado = $ejecudado;
    }

    public function getAnor_encajadeempalme() {
        return $this->anor_encajadeempalme;
    }

    public function setAnor_encajadeempalme($anor_encajadeempalme) {
        $this->anor_encajadeempalme = $anor_encajadeempalme;
    }

    public function getAnor_enecm() {
        return $this->anor_enecm;
    }

    public function setAnor_enecm($anor_enecm) {
        $this->anor_enecm = $anor_enecm;
    }

    public function getAnor_enmedidor() {
        return $this->anor_enmedidor;
    }

    public function setAnor_enmedidor($anor_enmedidor) {
        $this->anor_enmedidor = $anor_enmedidor;
    }

    public function getAnor_esquemademedida() {
        return $this->anor_esquemademedida;
    }

    public function setAnor_esquemademedida($anor_esquemademedida) {
        $this->anor_esquemademedida = $anor_esquemademedida;
    }

    public function getAnor_faltamedidareactiva() {
        return $this->anor_faltamedidareactiva;
    }

    public function setAnor_faltamedidareactiva($anor_faltamedidareactiva) {
        $this->anor_faltamedidareactiva = $anor_faltamedidareactiva;
    }

    public function getAnor_alarmadebateria() {
        return $this->anor_alarmadebateria;
    }

    public function setAnor_alarmadebateria($anor_alarmadebateria) {
        $this->anor_alarmadebateria = $anor_alarmadebateria;
    }

    public function getAnor_cargabajavariable() {
        return $this->anor_cargabajavariable;
    }

    public function setAnor_cargabajavariable($anor_cargabajavariable) {
        $this->anor_cargabajavariable = $anor_cargabajavariable;
    }

    public function getAnor_tarifanocorresponde() {
        return $this->anor_tarifanocorresponde;
    }

    public function setAnor_tarifanocorresponde($anor_tarifanocorresponde) {
        $this->anor_tarifanocorresponde = $anor_tarifanocorresponde;
    }

    public function getAnor_faltasello() {
        return $this->anor_faltasello;
    }

    public function setAnor_faltasello($anor_faltasello) {
        $this->anor_faltasello = $anor_faltasello;
    }

    public function getAnor_ttcc() {
        return $this->anor_ttcc;
    }

    public function setAnor_ttcc($anor_ttcc) {
        $this->anor_ttcc = $anor_ttcc;
    }

    public function getAnor_otro() {
        return $this->anor_otro;
    }

    public function setAnor_otro($anor_otro) {
        $this->anor_otro = $anor_otro;
    }

    public function getClas_cnr_1() {
        return $this->clas_cnr_1;
    }

    public function setClas_cnr_1($clas_cnr_1) {
        $this->clas_cnr_1 = $clas_cnr_1;
    }

    public function getClas_cnr_2() {
        return $this->clas_cnr_2;
    }

    public function setClas_cnr_2($clas_cnr_2) {
        $this->clas_cnr_2 = $clas_cnr_2;
    }

    public function getClas_cnr_3() {
        return $this->clas_cnr_3;
    }

    public function setClas_cnr_3($clas_cnr_3) {
        $this->clas_cnr_3 = $clas_cnr_3;
    }

    public function getClas_cnr_4() {
        return $this->clas_cnr_4;
    }

    public function setClas_cnr_4($clas_cnr_4) {
        $this->clas_cnr_4 = $clas_cnr_4;
    }

    public function getClas_cnr_5() {
        return $this->clas_cnr_5;
    }

    public function setClas_cnr_5($clas_cnr_5) {
        $this->clas_cnr_5 = $clas_cnr_5;
    }

    public function getClas_cnr_6() {
        return $this->clas_cnr_6;
    }

    public function setClas_cnr_6($clas_cnr_6) {
        $this->clas_cnr_6 = $clas_cnr_6;
    }

    public function getClas_cnr_7() {
        return $this->clas_cnr_7;
    }

    public function setClas_cnr_7($clas_cnr_7) {
        $this->clas_cnr_7 = $clas_cnr_7;
    }

    public function getClas_cnr_8() {
        return $this->clas_cnr_8;
    }

    public function setClas_cnr_8($clas_cnr_8) {
        $this->clas_cnr_8 = $clas_cnr_8;
    }

    public function exchangeArray($data) {
        $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
        $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
        $this->num_cliente = (isset($data['num_cliente'])) ? $data['num_cliente'] : null;
        $this->aviso = (isset($data['aviso'])) ? $data['aviso'] : null;
        $this->fecha_ejecucion = (isset($data['fecha_ejecucion'])) ? $data['fecha_ejecucion'] : null;
        $this->nombre_cliente = (isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : null;
        $this->direccion = (isset($data['direccion'])) ? $data['direccion'] : null;
        $this->comuna = (isset($data['comuna'])) ? $data['comuna'] : null;
        $this->tarifa = (isset($data['tarifa'])) ? $data['tarifa'] : null;
        $this->tipo_medida = (isset($data['tipo_medida'])) ? $data['tipo_medida'] : null;
        $this->constante_distribuidora = (isset($data['constante_distribuidora'])) ? $data['constante_distribuidora'] : null;
        $this->constante_terreno = (isset($data['constante_terreno'])) ? $data['constante_terreno'] : null;
        $this->resultadoauditorias = (isset($data['resultadoauditorias'])) ? $data['resultadoauditorias'] : null;
        $this->observacion = (isset($data['observacion'])) ? $data['observacion'] : null;
        $this->oficina = (isset($data['oficina'])) ? $data['oficina'] : null;
        $this->num_riat = (isset($data['num_riat'])) ? $data['num_riat'] : null;
        $this->ejecudado = (isset($data['ejecudado'])) ? $data['ejecudado'] : null;
        $this->anor_encajadeempalme = (isset($data['anor_encajadeempalme'])) ? $data['anor_encajadeempalme'] : null;
        $this->anor_enecm = (isset($data['anor_enecm'])) ? $data['anor_enecm'] : null;
        $this->anor_enmedidor = (isset($data['anor_enmedidor'])) ? $data['anor_enmedidor'] : null;
        $this->anor_esquemademedida = (isset($data['anor_esquemademedida'])) ? $data['anor_esquemademedida'] : null;
        $this->anor_faltamedidareactiva = (isset($data['anor_faltamedidareactiva'])) ? $data['anor_faltamedidareactiva'] : null;
        $this->anor_alarmadebateria = (isset($data['anor_alarmadebateria'])) ? $data['anor_alarmadebateria'] : null;
        $this->anor_cargabajavariable = (isset($data['anor_cargabajavariable'])) ? $data['anor_cargabajavariable'] : null;
        $this->anor_tarifanocorresponde = (isset($data['anor_tarifanocorresponde'])) ? $data['anor_tarifanocorresponde'] : null;
        $this->anor_faltasello = (isset($data['anor_faltasello'])) ? $data['anor_faltasello'] : null;
        $this->anor_ttcc = (isset($data['anor_ttcc'])) ? $data['anor_ttcc'] : null;
        $this->anor_otro = (isset($data['anor_otro'])) ? $data['anor_otro'] : null;
        $this->clas_cnr_1 = (isset($data['clas_cnr_1'])) ? $data['clas_cnr_1'] : null;
        $this->clas_cnr_2 = (isset($data['clas_cnr_2'])) ? $data['clas_cnr_2'] : null;
        $this->clas_cnr_3 = (isset($data['clas_cnr_3'])) ? $data['clas_cnr_3'] : null;
        $this->clas_cnr_4 = (isset($data['clas_cnr_4'])) ? $data['clas_cnr_4'] : null;
        $this->clas_cnr_5 = (isset($data['clas_cnr_5'])) ? $data['clas_cnr_5'] : null;
        $this->clas_cnr_6 = (isset($data['clas_cnr_6'])) ? $data['clas_cnr_6'] : null;
        $this->clas_cnr_7 = (isset($data['clas_cnr_7'])) ? $data['clas_cnr_7'] : null;
        $this->clas_cnr_8 = (isset($data['clas_cnr_8'])) ? $data['clas_cnr_8'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

