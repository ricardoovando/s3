<?php

namespace Avisos\Model\Entity;

class Sucursal {

    private $id_sucursal;
    private $comunas_id_comuna;
    private $nombre_sucursal;
    private $telefono;
    private $direccion;
    private $correo_sucursal;

    public function __construct(
    $id_sucursal = null, $comunas_id_comuna = null, $nombre_sucursal = null, $telefono = null, $direccion = null, $correo_sucursal = null
    ) {
        $this->id_sucursal = $id_sucursal;
        $this->comunas_id_comuna = $comunas_id_comuna;
        $this->nombre_sucursal = $nombre_sucursal;
        $this->telefono = $telefono;
        $this->direccion = $direccion;
        $this->correo_sucursal = $correo_sucursal;
    }

    public function getId_sucursal() {
        return $this->id_sucursal;
    }

    public function setId_sucursal($id_sucursal) {
        $this->id_sucursal = $id_sucursal;
    }

    public function getComunas_id_comuna() {
        return $this->comunas_id_comuna;
    }

    public function setComunas_id_comuna($comunas_id_comuna) {
        $this->comunas_id_comuna = $comunas_id_comuna;
    }

    public function getNombre_sucursal() {
        return $this->nombre_sucursal;
    }

    public function setNombre_sucursal($nombre_sucursal) {
        $this->nombre_sucursal = $nombre_sucursal;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function getCorreo_sucursal() {
        return $this->correo_sucursal;
    }

    public function setCorreo_sucursal($correo_sucursal) {
        $this->correo_sucursal = $correo_sucursal;
    }

    public function exchangeArray($data) {
        $this->id_sucursal = (isset($data['id_sucursal'])) ? $data['id_sucursal'] : null;
        $this->comunas_id_comuna = (isset($data['comunas_id_comuna'])) ? $data['comunas_id_comuna'] : null;
        $this->nombre_sucursal = (isset($data['nombre_sucursal'])) ? $data['nombre_sucursal'] : null;
        $this->telefono = (isset($data['telefono'])) ? $data['telefono'] : null;
        $this->direccion = (isset($data['direccion'])) ? $data['direccion'] : null;
        $this->correo_sucursal = (isset($data['correo_sucursal'])) ? $data['correo_sucursal'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

