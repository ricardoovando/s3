<?php

namespace Avisos\Model\Entity;

class SumResumenEjecutivoAnormalidadesCnr {

    private $mes;
    private $anio;
    private $id_region;
    private $nombre_region;
    private $sum;
    private $sum_1;
    private $sum_2;
    private $sum_3;
    private $sum_4;
    private $sum_5;
    private $sum_6;
    private $sum_7;
    private $sum_8;
    private $sum_9;
    private $sum_10;
    private $sum_11;
    private $sum_12;
    private $sum_13;
    private $sum_14;
    private $sum_15;
    private $sum_16;
    private $sum_17;
    private $sum_18;
    
    function __construct($mes = null,$anio = null,$id_region = null, $nombre_region = null, $sum = null, $sum_1 = null, $sum_2 = null, $sum_3 = null, $sum_4 = null, $sum_5 = null, $sum_6 = null, $sum_7 = null, $sum_8 = null, $sum_9 = null, $sum_10 = null, $sum_11 = null, $sum_12 = null, $sum_13 = null, $sum_14 = null, $sum_15 = null, $sum_16 = null, $sum_17 = null, $sum_18 = null) {
        $this->mes = $mes;
        $this->anio = $anio;
        $this->id_region = $id_region;
        $this->nombre_region = $nombre_region;
        $this->sum = $sum;
        $this->sum_1 = $sum_1;
        $this->sum_2 = $sum_2;
        $this->sum_3 = $sum_3;
        $this->sum_4 = $sum_4;
        $this->sum_5 = $sum_5;
        $this->sum_6 = $sum_6;
        $this->sum_7 = $sum_7;
        $this->sum_8 = $sum_8;
        $this->sum_9 = $sum_9;
        $this->sum_10 = $sum_10;
        $this->sum_11 = $sum_11;
        $this->sum_12 = $sum_12;
        $this->sum_13 = $sum_13;
        $this->sum_14 = $sum_14;
        $this->sum_15 = $sum_15;
        $this->sum_16 = $sum_16;
        $this->sum_17 = $sum_17;
        $this->sum_18 = $sum_18;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function getId_region() {
        return $this->id_region;
    }

    public function getNombre_region() {
        return $this->nombre_region;
    }

    public function getSum() {
        return $this->sum;
    }

    public function getSum_1() {
        return $this->sum_1;
    }

    public function getSum_2() {
        return $this->sum_2;
    }

    public function getSum_3() {
        return $this->sum_3;
    }

    public function getSum_4() {
        return $this->sum_4;
    }

    public function getSum_5() {
        return $this->sum_5;
    }

    public function getSum_6() {
        return $this->sum_6;
    }

    public function getSum_7() {
        return $this->sum_7;
    }

    public function getSum_8() {
        return $this->sum_8;
    }

    public function getSum_9() {
        return $this->sum_9;
    }

    public function getSum_10() {
        return $this->sum_10;
    }

    public function getSum_11() {
        return $this->sum_11;
    }

    public function getSum_12() {
        return $this->sum_12;
    }

    public function getSum_13() {
        return $this->sum_13;
    }

    public function getSum_14() {
        return $this->sum_14;
    }

    public function getSum_15() {
        return $this->sum_15;
    }

    public function getSum_16() {
        return $this->sum_16;
    }

    public function getSum_17() {
        return $this->sum_17;
    }

    public function getSum_18() {
        return $this->sum_18;
    }

    public function setId_region($id_region) {
        $this->id_region = $id_region;
    }

    public function setNombre_region($nombre_region) {
        $this->nombre_region = $nombre_region;
    }

    public function setSum($sum) {
        $this->sum = $sum;
    }

    public function setSum_1($sum_1) {
        $this->sum_1 = $sum_1;
    }

    public function setSum_2($sum_2) {
        $this->sum_2 = $sum_2;
    }

    public function setSum_3($sum_3) {
        $this->sum_3 = $sum_3;
    }

    public function setSum_4($sum_4) {
        $this->sum_4 = $sum_4;
    }

    public function setSum_5($sum_5) {
        $this->sum_5 = $sum_5;
    }

    public function setSum_6($sum_6) {
        $this->sum_6 = $sum_6;
    }

    public function setSum_7($sum_7) {
        $this->sum_7 = $sum_7;
    }

    public function setSum_8($sum_8) {
        $this->sum_8 = $sum_8;
    }

    public function setSum_9($sum_9) {
        $this->sum_9 = $sum_9;
    }

    public function setSum_10($sum_10) {
        $this->sum_10 = $sum_10;
    }

    public function setSum_11($sum_11) {
        $this->sum_11 = $sum_11;
    }

    public function setSum_12($sum_12) {
        $this->sum_12 = $sum_12;
    }

    public function setSum_13($sum_13) {
        $this->sum_13 = $sum_13;
    }

    public function setSum_14($sum_14) {
        $this->sum_14 = $sum_14;
    }

    public function setSum_15($sum_15) {
        $this->sum_15 = $sum_15;
    }

    public function setSum_16($sum_16) {
        $this->sum_16 = $sum_16;
    }

    public function setSum_17($sum_17) {
        $this->sum_17 = $sum_17;
    }

    public function setSum_18($sum_18) {
        $this->sum_18 = $sum_18;
    }

    public function exchangeArray($data) {
        $this->mes = (isset($data['mes'])) ? $data['mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->id_region = (isset($data['id_region'])) ? $data['id_region'] : null;
        $this->nombre_region = (isset($data['nombre_region'])) ? $data['nombre_region'] : null;
        $this->sum = (isset($data['sum'])) ? $data['sum'] : null;
        $this->sum_1 = (isset($data['sum_1'])) ? $data['sum_1'] : null;
        $this->sum_2 = (isset($data['sum_2'])) ? $data['sum_2'] : null;
        $this->sum_3 = (isset($data['sum_3'])) ? $data['sum_3'] : null;
        $this->sum_4 = (isset($data['sum_4'])) ? $data['sum_4'] : null;
        $this->sum_5 = (isset($data['sum_5'])) ? $data['sum_5'] : null;
        $this->sum_6 = (isset($data['sum_6'])) ? $data['sum_6'] : null;
        $this->sum_7 = (isset($data['sum_7'])) ? $data['sum_7'] : null;
        $this->sum_8 = (isset($data['sum_8'])) ? $data['sum_8'] : null;
        $this->sum_9 = (isset($data['sum_9'])) ? $data['sum_9'] : null;
        $this->sum_10 = (isset($data['sum_10'])) ? $data['sum_10'] : null;
        $this->sum_11 = (isset($data['sum_11'])) ? $data['sum_11'] : null;
        $this->sum_12 = (isset($data['sum_12'])) ? $data['sum_12'] : null;
        $this->sum_13 = (isset($data['sum_13'])) ? $data['sum_13'] : null;
        $this->sum_14 = (isset($data['sum_14'])) ? $data['sum_14'] : null;
        $this->sum_15 = (isset($data['sum_15'])) ? $data['sum_15'] : null;
        $this->sum_16 = (isset($data['sum_16'])) ? $data['sum_16'] : null;
        $this->sum_17 = (isset($data['sum_17'])) ? $data['sum_17'] : null;
        $this->sum_18 = (isset($data['sum_18'])) ? $data['sum_18'] : null;
     }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
