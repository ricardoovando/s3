<?php

namespace Avisos;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


/*Entity*/
use Avisos\Model\Dao\AvisoDao;
use Avisos\Model\Entity\Aviso;

use Avisos\Model\Dao\AvisoAsignadoDao;
use Avisos\Model\Entity\AvisoAsignado;

use Avisos\Model\Dao\DetalleServicioPrestadoDao;
use Avisos\Model\Entity\DetalleServicioPrestado;

use Avisos\Model\Dao\BitacoraDao;
use Avisos\Model\Entity\Bitacora;

/*Entity Instalaciones*/
use Instalaciones\Model\Dao\InstalacionDao;
use Instalaciones\Model\Entity\Instalacion;




class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, ServiceProviderInterface, ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();

        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
        
    }
    
        public function initAuth(MvcEvent $e) {
        $application = $e->getApplication();
        $matches = $e->getRouteMatch();
        $controller = $matches->getParam('controller');
        $action = $matches->getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index', 'autenticar'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index y autenticar.
                        return;
                    }
                    break;
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
            }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('home');
            }
        }
    }
    

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniAviso', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return $data;
                });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }


    /*Inyecccion de dependencia para el dao se carga la tabla de base de datos segun se indique*/
    public function getServiceConfig() {
     return array(
            'factories' => array(
                'Avisos\Model\AvisoDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayAviso');
                    $login = $sm->get('Admin\Model\Login');
                    $dbAdapter = $sm->get('dbAdapter');
                    $dao = new AvisoDao($tableGateway,$login,$dbAdapter);
                    return $dao;
                },
                'Avisos\Model\AvisoAsignadoDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayAvisoAsignado');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new AvisoAsignadoDao($tableGateway,$login);
                    return $dao;
                },        
                'Avisos\Model\InstalacionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayInstalaciones');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new InstalacionDao($tableGateway,$login);
                    return $dao;
                },
                'Avisos\Model\DetalleServicioPrestadoDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayDetalleServicioPrestado');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new DetalleServicioPrestadoDao($tableGateway,$login);
                    return $dao;
                },   
                'Avisos\Model\BitacoraDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayBitacora');
                    $dbAdapter = $sm->get('dbAdapter');
                    $dao = new BitacoraDao($tableGateway,$dbAdapter);
                    return $dao;
                },     
                'TableGatewayAviso' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Aviso());
                    return new TableGateway('avisos', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayInstalaciones' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Instalacion());
                    return new TableGateway('instalaciones', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayAvisoAsignado' => function ($sm) { /* Tabla temporal donde se asignan los avisos a brigadas */
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AvisoAsignado());
                    return new TableGateway('avisos_asignados', $dbAdapter, null, $resultSetPrototype);
                },  
                'TableGatewayDetalleServicioPrestado' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DetalleServicioPrestado());
                    return new TableGateway('detalle_servicio_prestado', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayDetalleServicioPrestado' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DetalleServicioPrestado());
                    return new TableGateway('detalle_servicio_prestado', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayBitacora' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Bitacora());
                    return new TableGateway('bitacora', $dbAdapter, null, $resultSetPrototype);
                },         
                'dbAdapter' => function ($sm) {
                    return $sm->get('Zend\Db\Adapter\Adapter'); /*Se inserta instancia de base de datos*/
                },
            ),
        );
    } 

    /*Inyeccion de dependencia para el controlador se carga el objeto modelo usuario dao*/
     public function getControllerConfig() {
        return  array(
            'factories' => array(
                'Avisos\Controller\Index' => function ($sm) {
            
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAviso');
                    $avisoDao = $locator->get('Avisos\Model\AvisoDao');
                    $avisoasignadoDao = $locator->get('Avisos\Model\AvisoAsignadoDao');
                    
                    $controller = new \Avisos\Controller\IndexController($config);
                    $controller->setAvisoDao($avisoDao);
                    $controller->setAvisoAsignadoDao($avisoasignadoDao);
                    
                    return $controller;
                    
                },
               'Avisos\Controller\Valoriza' => function ($sm) {
                    
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAviso');
                    $avisoDao = $locator->get('Avisos\Model\AvisoDao');
                    $ServicioDao = $locator->get('Servicios\Model\ServicioDao');
                    $orsriatdao = $locator->get('Riat\Model\OrsRiatDao');
                    $DetalleServicioPrestadoDao = $locator->get('Avisos\Model\DetalleServicioPrestadoDao');
                    
                    $controller = new \Avisos\Controller\ValorizaController($config);
                    $controller->setAvisoDao($avisoDao);
                    $controller->setServicioDao($ServicioDao);
                    $controller->setDetalleServicioPrestadoDao($DetalleServicioPrestadoDao);
                    $controller->setOrsRiatDao($orsriatdao);
                    
                    return $controller;
                    
                },
               'Avisos\Controller\Controlling' => function ($sm) {
                    
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAviso');
                    $avisoDao = $locator->get('Avisos\Model\AvisoDao');
                    
                    $controller = new \Avisos\Controller\ControllingController($config);
                    $controller->setAvisoDao($avisoDao);
                    
                    return $controller;
                    
                },         
               'Avisos\Controller\Cargar' => function ($sm) {
                    
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAviso');
                    
                    $controller = new \Avisos\Controller\CargarController($config);
                    
                    return $controller;
                    
                },
               'Avisos\Controller\Bitacora' => function ($sm) {
                    
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniAviso');
                    
                    $controller = new \Avisos\Controller\BitacoraController($config);
                    
                    return $controller;
                    
                },
             )
          );
       } 


       
    public function getViewHelperConfig() {

        return array(
            'factories' => array(
                /* ETC ... */
                'formatDate' => function($sm) {
                    return new View\Helper\FormatDate();
                }
            ),
        );
    }
    
}
