<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Avisos\Controller\Cargar' => 'Avisos\Controller\CargarController',
            'Avisos\Controller\Bitacora' => 'Avisos\Controller\BitacoraController',
            'Avisos\Controller\Laboratorio' => 'Avisos\Controller\LaboratorioController',
            'Avisos\Controller\Facturacion' => 'Avisos\Controller\FacturacionController'
           ),
        ),
    'router' => array(
        'routes' => array(
            'avisos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/avisos[/:controller][/:action][/:id][/:br]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9_-]+',   
                        'br' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Avisos\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
          'paginatoravisos' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/avisos[/:controller][/:action]/id[/:id]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'avisos\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Avisos' => __DIR__ . '/../view',
        ),
      'strategies' => array(
          'ViewJsonStrategy', 
        ),
    ),
);
