<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Avisos\Model\Entity\Bitacora;

/*Form*/
use Avisos\Form\FormCargadorBitacora;

class BitacoraController extends AbstractActionController {

    private $bitacoraDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    private function getFormCargadorBitacora() {
        return new FormCargadorBitacora();
    }

    public function getBitacoraDao() {
        if (!$this->bitacoraDao) {
            $sm = $this->getServiceLocator();
            $this->bitacoraDao = $sm->get('Avisos\Model\BitacoraDao');
        }
        return $this->bitacoraDao;
    }

    public function indexAction() {
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $FormCargador = $this->getFormCargadorBitacora();
        return new ViewModel(array(
            'FormCargador' => $FormCargador,
        ));
    }

    public function cargarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('avisos', array('controller' => 'bitacora'));
        }

        //Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        
        //Funciones extras
        function get_cell($cell, $objPHPExcel) {
            //select one cell
            $objCell = ($objPHPExcel->getActiveSheet()->getCell($cell));
            //get cell value
            return $objCell->getvalue();
        }

        function pp(&$var) {
            $var = chr(ord($var) + 1);
            return true;
        }

        $tname = $_FILES['file']['tmp_name'];
        $type = $_FILES['file']['type'];

        if ($type == 'application/vnd.ms-excel') {
            // Extension excel 97
            $ext = 'xls';
        } else if ($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            // Extension excel 2007 y 2010
            $ext = 'xlsx';
        } else {
            // Extension no valida
            echo -1;
            return new ViewModel(array('Error' => 'Documento Incorrecto'));
        }

        $xlsx = 'Excel2007';
        $xls = 'Excel5';

        //creando el lector
        $objReader = \PHPExcel_IOFactory::createReader($$ext);

        //cargamos el archivo
        $objPHPExcel = $objReader->load($tname);

        $dim = $objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

        // list coloca en array $start y $end
        list($start, $end) = explode(':', $dim);

        if (!preg_match('#([A-Z]+)([0-9]+)#', $start, $rslt)) {
            return false;
        }

        list($start, $start_h, $start_v) = $rslt;

        if (!preg_match('#([A-Z]+)([0-9]+)#', $end, $rslt)) {
            return false;
        }

        list($end, $end_h, $end_v) = $rslt;
       
        
        $arrayobj_cargados = new \Zend\Stdlib\ArrayObject();
        $arrayobj_nocargados = new \Zend\Stdlib\ArrayObject();
 
        for ($v = $start_v + 1; $v <= $end_v; $v++) {
            
            $data = array(
                       'GNR_FECHA' => date("d-m-Y",\PHPExcel_Shared_Date::ExcelToPHP(get_cell('A'.$v,$objPHPExcel)+1)),
                       'GNR_OFICINA' => get_cell('B'.$v,$objPHPExcel),
                       'GNR_PROYECTO' => get_cell('C'.$v,$objPHPExcel),
                       'BGD_ENCARGADO' => get_cell('D'.$v,$objPHPExcel),
                       'BGD_AYUDANTE' => get_cell('E'.$v,$objPHPExcel),
                       'BGD_INSTRUMENTO' => get_cell('F'.$v,$objPHPExcel),
                       'MV_PATENTE' => get_cell('G'.$v,$objPHPExcel),
                       'MV_KM_SALIDA' => get_cell('H'.$v,$objPHPExcel),
                       'MV_KM_LLEGADA' => get_cell('I'.$v,$objPHPExcel),
                       'HRS_HORA_SALIDA' => get_cell('J'.$v,$objPHPExcel),
                       'HRS_HORA_LLEGADA' => get_cell('K'.$v,$objPHPExcel),
                       'TDO_MASIVAS' => get_cell('L'.$v,$objPHPExcel),
                       'TDO_RECLAMOS' => get_cell('M'.$v,$objPHPExcel),
                       'TDO_PERITAJES' => get_cell('N'.$v,$objPHPExcel),
                       'TDO_AUTOGEN' => get_cell('O'.$v,$objPHPExcel),
                       'SE_INSPECCION_VISUAL' => get_cell('P'.$v,$objPHPExcel),
                       'SE_VRF_SIN_CAMBIO' => get_cell('Q'.$v,$objPHPExcel),
                       'SE_VRF_CON_CAMBIO' => get_cell('R'.$v,$objPHPExcel),
                       'SE_CAMBIO_DIRECTO' => get_cell('S'.$v,$objPHPExcel),
                       'SE_LAB_SERVICIO' => get_cell('T'.$v,$objPHPExcel),
                       'OSRV_MAN_MENOR' => get_cell('U'.$v,$objPHPExcel),
                       'OSRV_LAB_TERCEROS' => get_cell('V'.$v,$objPHPExcel),
                       'CCSRV_DEVOLUCION' => get_cell('W'.$v,$objPHPExcel),
                       'CCSRV_REINSTALACION' => get_cell('X'.$v,$objPHPExcel),
                       'CCSRV_FRUSTRADO' => get_cell('Y'.$v,$objPHPExcel),
                       'RSLT_SERVICIOS_SIN_CNR' => get_cell('Z'.$v,$objPHPExcel),
                       'RSLT_SERVICIOS_CON_CNR' => get_cell('AA'.$v,$objPHPExcel),
                       'RSLT_CNR_CONFIGURABLE' => get_cell('AB'.$v,$objPHPExcel),
                       'RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN' => get_cell('AC'.$v,$objPHPExcel),
                       'TCNR_MEDIDOR_FUERA_DE_RANGO' => get_cell('AD'.$v,$objPHPExcel),
                       'TCNR_OTRO' => get_cell('AE'.$v,$objPHPExcel),
                       'NRM_CNR_NORMALIZADOS' => get_cell('AF'.$v,$objPHPExcel),
                       'NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION' => get_cell('AG'.$v,$objPHPExcel),
                       'NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS' => get_cell('AH'.$v,$objPHPExcel),
                       'NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR' => get_cell('AI'.$v,$objPHPExcel),
                       'NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA' => get_cell('AJ'.$v,$objPHPExcel),
                       'NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS' => get_cell('AK'.$v,$objPHPExcel),
                       'TVIPD_CERRADO_EN_2DA_VISITA' => get_cell('AL'.$v,$objPHPExcel),
                       'TVIPD_CERRADO_EN_1RA_VISITA' => get_cell('AM'.$v,$objPHPExcel),
                       'TVIPD_CLIENTE_NO_AUTORIZA' => get_cell('AN'.$v,$objPHPExcel),
                       'TVIPD_DOMICILIO_DESHABITADO' => get_cell('AO'.$v,$objPHPExcel),
                       'TVIPD_SITIO_ERIAZO' => get_cell('AP'.$v,$objPHPExcel),
                       'TVIPD_SIN_ENERGIA' => get_cell('AQ'.$v,$objPHPExcel),
                       'TVIPD_NO_EXISTE_RESPONSABLE' => get_cell('AR'.$v,$objPHPExcel),
                       'TVIPD_NO_UBICADO' => get_cell('AS'.$v,$objPHPExcel),
                       'TVIPD_EMPALME_NO_EXISTE' => get_cell('AT'.$v,$objPHPExcel),
                       'TVIPD_CASA_CERRADA' => get_cell('AU'.$v,$objPHPExcel),
                       'DIAS_DESCONTAR' => get_cell('AV'.$v,$objPHPExcel),
                       'OBSERVACIONES' => get_cell('AW'.$v,$objPHPExcel),
                   );
            
            
            $result = $this->guardarBitacoraAction($data);
            
            $arrayobj_nocargados->append($result);
            
       }
       
       unset($_FILES);
       unset($_POST);        
       
       /*RETORNO A PANTALLA*/
       return new ViewModel(array('nocargados'=>$arrayobj_nocargados));
              
    }


    public function soloLetrasNumeros($cadena) {

        return preg_replace("[^A-Za-z0-9]", "", $cadena);
    }

    public function removerTags($entrada) {

        $busqueda = array(
            '@<script[^>]*?>.*?</script>@si', // javascript
            '@<[\/\!]*?[^<>]*?>@si', // HTML
            '@<style[^>]*?>.*?</style>@siU', // Css
            '@<![\s\S]*?--[ \t\n\r]*>@' // Comentarios multiples
        );

        $salida = preg_replace($busqueda, '', $entrada);
        return $salida;
    }

    public function limpiarVariables($entrada) {
        if (is_array($entrada)) {
            foreach ($entrada as $var => $val) {
                $output[$var] = $this->limpiarVariables($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $entrada = stripslashes($entrada);
            }
            $salida = $this->removerTags($entrada);
        }
        return $salida;
    }

    
    public function buscarInstalacionAction($num_instalacion) {

        return $this->getInstalacionDao()->obtenerPorNumInstalacion($num_instalacion);
    }

    
    public function guardarBitacoraAction(Array $data) {
                  
        $bitacora = new Bitacora();
        $bitacora->exchangeArray($data);
        
        try {
            
            return $this->getBitacoraDao()->guardar($bitacora);
            
        } catch (Exception $exc) {
            
            return $exc->getTraceAsString();
            
        }
        
    }
    
   
    public function reporteBitacoraAction() {
        
      $this->layout()->usuario = $this->getLogin()->getIdentity();
        
      $result_0 = $this->getBitacoraDao()->ObtenerResultadosBitacora('02-06-2014','16-06-2014');
      $result_1 = $this->getBitacoraDao()->ObtenerResultadosAyudantesBitacora('02-06-2014','16-06-2014');
      $result_totales = $this->getBitacoraDao()->ObtenerResultadosTotalesBitacora('02-06-2014','16-06-2014');
      $result_oficina = $this->getBitacoraDao()->obtenerResultadosBitacoraOficina('02-06-2014','16-06-2014');
      
      
      foreach ($result_totales as $reg):

          $se = $reg["se"];

          $cnr = $reg["cnr"];

          $causa_distribuidora = $reg["causa_distribuidora"];

          $causa_tecnet = $reg["causa_tecnet"];

        endforeach;


        return new ViewModel(array( 
                                 'result_0' => $result_0 , 
                                 'result_1' => $result_1 , 
                                 'result_totales' => $result_totales, 
                                 'result_oficina' => $result_oficina,
                                 'total_se' => $se,
                                 'total_cnr' => $cnr,
                                 'total_causa_distribuidora' => $causa_distribuidora,
                                 'total_causa_tecnet' => $causa_tecnet,
                               )
                           );        
        
    } 
    

}
