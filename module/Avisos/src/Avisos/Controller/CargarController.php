<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Instalaciones\Model\Entity\Instalacion;
use Instalaciones\Model\Entity\Medidor;
use Instalaciones\Model\Entity\MedidorInstalado;
use Avisos\Model\Entity\Aviso;

/**/
use Avisos\Form\SelectSucursal;
use Avisos\Form\SelectSucursalValidator;

use Avisos\Form\FormCargador;
use Avisos\Form\FormCargadorValidator;


class CargarController extends AbstractActionController {

    private $instalacionDao;
    private $medidorDao;
    private $medidorinstaladoDao;
    private $avisoDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
      }
    
    private function getFormCargador() {
        $form = new FormCargador();
        $form->get('sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form;
     }
     
    private function getFormSelectSucursales() {
        $form = new SelectSucursal();
        $form->get('sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form;
    }
    
    public function getInstalacionDao() {
        if (!$this->instalacionDao) {
            $sm = $this->getServiceLocator();
            $this->instalacionDao = $sm->get('Avisos\Model\InstalacionDao');
        }
        return $this->instalacionDao;
      }
      
    public function getMedidorDao(){
        if (!$this->medidorDao) {
            $sm = $this->getServiceLocator();
            $this->medidorDao = $sm->get('Instalaciones\Model\MedidorDao');
        }
        return $this->medidorDao;  
    }

    public function getMedidorInstaladoDao(){
        if (!$this->medidorinstaladoDao) {
            $sm = $this->getServiceLocator();
            $this->medidorinstaladoDao = $sm->get('Instalaciones\Model\MedidorInstaladoDao');
        }
        return $this->medidorinstaladoDao;  
    }
    
    public function getAvisoDao() {
        if (!$this->avisoDao) {
            $sm = $this->getServiceLocator();
            $this->avisoDao = $sm->get('Avisos\Model\AvisoDao');
        }
        return $this->avisoDao;
      }

    public function indexAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $FormCargador = $this->getFormCargador();
        
        return new ViewModel(array(
                               'FormCargador' => $FormCargador, 
                               ));
        
      }

    public function cargarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

         if (!$this->getRequest()->isPost()) {

            $this->redirect()->toRoute('avisos', array('controller' => 'cargar'));
        }


        //Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();


        $FormCargador = $this->getFormCargador();
        $FormCargador->setInputFilter(new FormCargadorValidator());
        $FormCargador->setData($postParams);
        
        
        /*
         * Falla de la Validacion del form. 
         */
        if (!$FormCargador->isValid())
        {
            
            $modelView = new ViewModel(array('FormCargador' => $FormCargador));
            $modelView->setTemplate('avisos/cargar/index');
            return $modelView;
            
        }
        
        $values = $FormCargador->getData();


        //Funciones extras

        function get_cell($cell, $objPHPExcel) {
            //select one cell
            $objCell = ($objPHPExcel->getActiveSheet()->getCell($cell));
            //get cell value
            return $objCell->getvalue();
        }

        function pp(&$var) {
            $var = chr(ord($var) + 1);
            return true;
        }

        $tname = $_FILES['file']['tmp_name'];
        $type = $_FILES['file']['type'];

        if ($type == 'application/vnd.ms-excel') {

            // Extension excel 97
            $ext = 'xls';
        } else if ($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {

            // Extension excel 2007 y 2010
            $ext = 'xlsx';

        } else {

            // Extension no valida
            echo -1;
            return new ViewModel(array('Error' => 'Documento Incorrecto'));

        }


        $xlsx = 'Excel2007';
        $xls = 'Excel5';

        //creando el lector
        $objReader = \PHPExcel_IOFactory::createReader($$ext);

        //cargamos el archivo
        $objPHPExcel = $objReader->load($tname);

        $dim = $objPHPExcel->getActiveSheet()->calculateWorksheetDimension();

        // list coloca en array $start y $end
        list($start, $end) = explode(':', $dim);

        if (!preg_match('#([A-Z]+)([0-9]+)#', $start, $rslt)) {
            return false;
        }

        list($start, $start_h, $start_v) = $rslt;

        if (!preg_match('#([A-Z]+)([0-9]+)#', $end, $rslt)) {
            return false;
        }

        list($end, $end_h, $end_v) = $rslt;
                        
        /*
         * Comprobar si existen todos los campos en la planilla 
         */
        
          $bool = true;
          $A1 = NULL;
          $B1 = NULL;
          $C1 = NULL;
          $D1 = NULL;
          $E1 = NULL;
          $F1 = NULL;
          $G1 = NULL;
          $H1 = NULL;
          $I1 = NULL;
          $J1 = NULL;
          $K1 = NULL;
          $L1 = NULL;
          $M1 = NULL;
          $N1 = NULL;
          $O1 = NULL;
          $P1 = NULL;
          $Q1 = NULL;
          $R1 = NULL;
          $S1 = NULL;
          $T1 = NULL;
          $U1 = NULL;
          $V1 = NULL;
          $W1 = NULL;
          $X1 = NULL;
          $Y1 = NULL;
          $Z1 = NULL;
          $AA1 = NULL;

          $erroresEnLosCampos = array();
          
            if(trim(get_cell('A1' , $objPHPExcel)) !== 'ID de servicio')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo A1 tiene que ser igual a <strong>ID de servicio</strong> y tiene el valor "'.get_cell('A1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('B1' , $objPHPExcel)) !== 'Clase de aviso')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo B1 tiene que ser igual a <strong>Clase de aviso</strong> y tiene el valor "'.get_cell('B1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('C1' , $objPHPExcel)) !== 'Sub. OT')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo C1 tiene que ser igual a <strong>Sub. OT</strong> y tiene el valor "'.get_cell('C1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('D1' , $objPHPExcel)) !== 'Aviso')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo D1 tiene que ser igual a <strong>Aviso</strong> y tiene el valor "'.get_cell('D1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('E1' , $objPHPExcel)) !== 'Fecha Creado SAP')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo E1 tiene que ser igual a <strong>Fecha Creado SAP</strong> y tiene el valor "'.get_cell('E1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('F1' , $objPHPExcel)) !== 'Fecha Asignado SAP')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo F1 tiene que ser igual a <strong>Fecha Asignado SAP</strong> y tiene el valor "'.get_cell('F1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('G1' , $objPHPExcel)) !== 'Instalación')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo G1 tiene que ser igual a <strong>Instalación</strong> y tiene el valor "'.get_cell('G1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('H1' , $objPHPExcel)) !== 'Cuenta contrato')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo H1 tiene que ser igual a <strong>Cuenta contrato</strong> y tiene el valor "'.get_cell('H1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('I1' , $objPHPExcel)) !== 'Descripción')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo I1 tiene que ser igual a <strong>Descripción</strong> y tiene el valor "'.get_cell('I1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('J1' , $objPHPExcel)) !== 'Nombre')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo J1 tiene que ser igual a <strong>Nombre</strong> y tiene el valor "'.get_cell('J1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('K1' , $objPHPExcel)) !== 'Teléfono')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo K1 tiene que ser igual a <strong>Teléfono</strong> y tiene el valor "'.get_cell('K1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('L1' , $objPHPExcel)) !== 'Calle')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo L1 tiene que ser igual a <strong>Calle</strong> y tiene el valor "'.get_cell('L1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('M1' , $objPHPExcel)) !== 'Calle 4')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo M1 tiene que ser igual a <strong>Calle 4</strong> y tiene el valor "'.get_cell('M1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('N1' , $objPHPExcel)) !== 'Departamento')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo N1 tiene que ser igual a <strong>Departamento</strong> y tiene el valor "'.get_cell('N1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('O1' , $objPHPExcel)) !== 'Comuna')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo O1 tiene que ser igual a <strong>Comuna</strong> y tiene el valor "'.get_cell('O1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('P1' , $objPHPExcel)) !== 'Clase de Tarifa')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo P1 tiene que ser igual a <strong>Clase de Tarifa</strong> y tiene el valor "'.get_cell('P1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('Q1' , $objPHPExcel)) !== 'Número de serie')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo Q1 tiene que ser igual a <strong>Número de serie</strong> y tiene el valor "'.get_cell('Q1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('R1' , $objPHPExcel)) !== 'Material')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo R1 tiene que ser igual a <strong>Material</strong> y tiene el valor "'.get_cell('R1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('S1' , $objPHPExcel)) !== 'Marca del Medidor')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo S1 tiene que ser igual a <strong>Marca del Medidor</strong> y tiene el valor "'.get_cell('S1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('T1' , $objPHPExcel)) !== 'Propiedad del Medidor')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo T1 tiene que ser igual a <strong>Propiedad del Medidor</strong> y tiene el valor "'.get_cell('T1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('U1' , $objPHPExcel)) !== 'Texto breve de material')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo U1 tiene que ser igual a <strong>Texto breve de material</strong> y tiene el valor "'.get_cell('U1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('V1' , $objPHPExcel)) !== 'Unidad de lectura')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo V1 tiene que ser igual a <strong>Unidad de lectura</strong> y tiene el valor "'.get_cell('V1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('W1' , $objPHPExcel)) !== 'Porción')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo W1 tiene que ser igual a <strong>Porción</strong> y tiene el valor "'.get_cell('W1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('X1' , $objPHPExcel)) !== 'Poste/Cámara')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo X1 tiene que ser igual a <strong>Poste/Cámara</strong> y tiene el valor "'.get_cell('X1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('Y1' , $objPHPExcel)) !== 'Constante')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo Y1 tiene que ser igual a <strong>Constante</strong> y tiene el valor "'.get_cell('Y1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('Z1' , $objPHPExcel)) !== 'Status usuario')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo Z1 tiene que ser igual a <strong>Status usuario</strong> y tiene el valor "'.get_cell('Z1' , $objPHPExcel).'"'; 
            }

            if(trim(get_cell('AA1' , $objPHPExcel)) !== 'Ce. emplazam.')
            { 
              $bool = false; 
              $erroresEnLosCampos[] = 'El campo AA1 tiene que ser igual a <strong>Sub. OT</strong> y tiene el valor "'.get_cell('AA1' , $objPHPExcel).'"'; 
            }
        
            
       if($bool===false) 
       {
         foreach ($erroresEnLosCampos as $value) {
            $this->flashMessenger()->addMessage($value); 
         }
         $modelView = new ViewModel(array('FormCargador' => $FormCargador));
         $modelView->setTemplate('avisos/cargar/index');
         return $modelView;
       } 
       
            

        //empieza  lectura vertical
        $table = array();
        
        $arrayobj_cargados = new \Zend\Stdlib\ArrayObject();
        
        $arrayobj_nocargados = new \Zend\Stdlib\ArrayObject();
        


        for ($v = $start_v + 1; $v <= $end_v; $v++) {
            
            
            //Empieza lectura horizontal
            $table[$v]['ID de servicio'] = get_cell('A' . $v, $objPHPExcel);
            $table[$v]['Clase de aviso'] = get_cell('B' . $v, $objPHPExcel);
            $table[$v]['Sub. OT'] = get_cell('C' . $v, $objPHPExcel);
            $table[$v]['Aviso'] = get_cell('D' . $v, $objPHPExcel);
            $table[$v]['Fecha Creado SAP'] = date("d-m-Y",\PHPExcel_Shared_Date::ExcelToPHP(get_cell('E'.$v,$objPHPExcel)+1)); 
            $table[$v]['Fecha Asignado SAP'] = date("d-m-Y",\PHPExcel_Shared_Date::ExcelToPHP(get_cell('F'.$v,$objPHPExcel)+1));
            $table[$v]['Instalación'] = get_cell('G' . $v, $objPHPExcel);
            $table[$v]['Cuenta contrato'] = get_cell('H' . $v, $objPHPExcel);
            $table[$v]['Descripción'] = get_cell('I' . $v, $objPHPExcel);
            $table[$v]['Nombre'] = get_cell('J' . $v, $objPHPExcel);
            $table[$v]['Teléfono'] = get_cell('K' . $v, $objPHPExcel);
            $table[$v]['Calle'] = get_cell('L' . $v, $objPHPExcel);
            $table[$v]['Calle 4'] = get_cell('M' . $v, $objPHPExcel);
            $table[$v]['Departamento'] = get_cell('N' . $v, $objPHPExcel);
            $table[$v]['Comuna'] = get_cell('O' . $v, $objPHPExcel);
            $table[$v]['Clase de Tarifa'] = get_cell('P' . $v, $objPHPExcel);
            $table[$v]['Número de serie'] = get_cell('Q' . $v, $objPHPExcel);
            $table[$v]['Material'] = get_cell('R' . $v, $objPHPExcel);
            $table[$v]['Marca del Medidor'] = get_cell('S' . $v, $objPHPExcel);
            $table[$v]['Propiedad del Medidor'] = get_cell('T' . $v, $objPHPExcel);
            $table[$v]['Texto breve de material'] = get_cell('U' . $v, $objPHPExcel);
            $table[$v]['Unidad de lectura'] = get_cell('V' . $v, $objPHPExcel);
            $table[$v]['Porción'] = get_cell('W' . $v, $objPHPExcel);
            $table[$v]['Poste/Cámara'] = get_cell('X' . $v, $objPHPExcel);
            $table[$v]['Constante'] = get_cell('Y' . $v, $objPHPExcel);
            $table[$v]['Status usuario'] = get_cell('Z' . $v, $objPHPExcel);
            $table[$v]['Ce. emplazam.'] = get_cell('AA' . $v, $objPHPExcel);
        
            
           /*
            * 
            * Guardando Datos de Instalacion
            * 
            */
            
            $datos = array();
            /* Datos a Guardar en Tabla Instalacion */
            $datos['Instalación'] = get_cell('G' . $v, $objPHPExcel);
            $datos['Nombre'] = get_cell('J' . $v, $objPHPExcel);
            $datos['Teléfono'] = get_cell('K' . $v, $objPHPExcel);
            $datos['Calle'] = get_cell('L' . $v, $objPHPExcel);
            $datos['Calle 4'] = get_cell('M' . $v, $objPHPExcel);
            $datos['Departamento'] = get_cell('N' . $v, $objPHPExcel);
            $datos['Comuna'] = get_cell('O' . $v, $objPHPExcel);
            $datos['Clase de Tarifa'] = get_cell('P' . $v, $objPHPExcel);
            $datos['Unidad de lectura'] = get_cell('V' . $v, $objPHPExcel);
            $datos['Porción'] = get_cell('W' . $v, $objPHPExcel);
            $datos['Poste/Cámara'] = get_cell('X' . $v, $objPHPExcel);
            $datos['Ce. emplazam.'] = get_cell('AA' . $v, $objPHPExcel);
                    
           /* 
            * A futuro estos campos dejaran de ser 
            * almacenados en esta tabla recuerdalo 
            * o si tu eres nuevo por favor termina 
            * con esto :D
            * 
            */
            
            $datos['Número de serie'] = get_cell('Q' . $v, $objPHPExcel);
            $datos['Material'] = get_cell('R' . $v, $objPHPExcel);
            $datos['Marca del Medidor'] = get_cell('S' . $v, $objPHPExcel);
            $datos['Propiedad del Medidor'] = get_cell('T' . $v, $objPHPExcel);
            $datos['Texto breve de material'] = get_cell('U' . $v, $objPHPExcel);
            $datos['Constante'] = get_cell('Y' . $v, $objPHPExcel);
            
            
            $datos['Aviso'] =(String) get_cell('D' . $v, $objPHPExcel);
                     
           /*ESTOS CAMPOS NO PUEDEN SER VACIOS*/
                       
           /*
            * En caso de ser vacios cargare el numero del aviso en la instalcion
            * 
            */
            if((empty($datos['Instalación'])) && (!empty($datos['Aviso'])))
                 $datos['Instalación'] = $datos['Aviso'];

            
            /*++++++++++++++++++++++++++++++++++++++++++++++*/
            
            /*
             * Tienen que existir sino no funciona la carga
             */

            if(empty($datos['Instalación']))
                  return new ViewModel(array('table' => $table, "Error" => "La columna id_instalacion esta sin datos {$datos['Instalación']} registro n° {$v} "));
                  
            if(empty($datos['Nombre']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Nombre esta sin datos {$datos['Nombre']} registro n° {$v} "));
                                
            if(empty($datos['Calle']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Calle esta sin datos {$datos['Calle']} registro n° {$v} "));

            if(empty($datos['Número de serie']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Número de serie esta sin datos {$datos['Número de serie']} registro n° {$v} "));
    
                
            
            /*
             * NO pueden ser NULL sino no funciona la carga
             */    
            if(is_null((String) $datos['Instalación']))
                  return new ViewModel(array('table' => $table, "Error" => "La columna Instalación esta sin datos {$datos['Instalación']} registro n° {$v} "));                
            if(is_null((String) $datos['Nombre']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Nombre esta sin datos {$datos['Nombre']} registro n° {$v} "));
            if(is_null((String) $datos['Calle']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Calle esta sin datos {$datos['Calle']} registro n° {$v} "));
            if (is_null((String) $datos['Calle 4']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Calle 4 esta sin datos {$datos['Calle 4 ']} registro n° {$v} "));
            if (is_null((String) $datos['Comuna']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Comuna esta sin datos {$datos['Comuna']} registro n° {$v} "));
            if (is_null((String) $datos['Clase de Tarifa']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Clase de Tarifa esta sin datos {$datos['Clase de Tarifa']} registro n° {$v} "));
            if (is_null((String) $datos['Unidad de lectura']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Unidad de lectura esta sin datos {$datos['Unidad de lectura']} registro n° {$v} "));
            if (is_null((String) $datos['Poste/Cámara']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Poste/Cámara esta sin datos {$datos['Poste/Cámara']} registro n° {$v} "));
            if (is_null((String) $datos['Número de serie']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Número de serie esta sin datos {$datos['Número de serie']} registro n° {$v} "));
            if (is_null((String) $datos['Poste/Cámara']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Poste/Cámara esta sin datos {$datos['Poste/Cámara']} registro n° {$v} "));
            if (is_null((String) $datos['Material']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Material esta sin datos {$datos['Material']} registro n° {$v} "));
            if (is_null((String) $datos['Marca del Medidor']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Marca del Medidor esta sin datos {$datos['Marca del Medidor']} registro n° {$v} "));
            if (is_null((String) $datos['Propiedad del Medidor']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Propiedad del Medidor esta sin datos {$datos['Propiedad del Medidor']} registro n° {$v} "));
            if (is_null((String) $datos['Texto breve de material']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Texto breve de material esta sin datos {$datos['Texto breve de material']} registro n° {$v} "));
            if (is_null((String) $datos['Constante']))
                return new ViewModel(array('table' => $table, "Error" => "La columna Constante esta sin datos {$datos['Constante']} registro n° {$v} "));

            
                
            /*
             * Todos los campos con formato texto.
             */    
            if (!is_string((String) $datos['Instalación']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Instalación {$datos['Instalación']} registro n° {$v} "));         
            if (!is_string((String) $datos['Nombre']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Nombre {$datos['Nombre']} registro n° {$v} "));      
            if (!is_string((String) $datos['Teléfono']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Teléfono {$datos['Teléfono']} registro n° {$v} "));    
            if (!is_string((String) $datos['Calle']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Calle {$datos['Calle']} registro n° {$v} "));   
            if (!is_string((String) $datos['Calle 4']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Calle 4 {$datos['Calle 4 ']} registro n° {$v} "));   
            if (!is_string((String) $datos['Departamento']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Departamento {$datos['Departamento']} registro n° {$v} "));                           
            if (!is_string((String) $datos['Comuna']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Comuna {$datos['Comuna']} registro n° {$v} "));   
            if (!is_string((String) $datos['Clase de Tarifa']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Clase de Tarifa {$datos['Clase de Tarifa']} registro n° {$v} "));  
            if (!is_string((String) $datos['Unidad de lectura']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Unidad de lectura {$datos['Unidad de lectura']} registro n° {$v} "));   
            if (!is_string((String) $datos['Porción']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Porción {$datos['Porción']} registro n° {$v} "));           
            if (!is_string((String) $datos['Poste/Cámara']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Poste/Cámara {$datos['Poste/Cámara']} registro n° {$v} "));    
            if (!is_string((String) $datos['Ce. emplazam.']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Ce. emplazam. {$datos['Ce. emplazam.']} registro n° {$v} "));
            
            if (!is_string((String) $datos['Número de serie']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Número de serie {$datos['Número de serie']} registro n° {$v} "));
            if (!is_string((String) $datos['Material']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Material {$datos['Material']} registro n° {$v} "));
            if (!is_string((String) $datos['Marca del Medidor']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Marca del Medidor {$datos['Marca del Medidor']} registro n° {$v} "));
            if (!is_string((String) $datos['Propiedad del Medidor']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Propiedad del Medidor {$datos['Propiedad del Medidor']} registro n° {$v} "));
            if (!is_string((String) $datos['Texto breve de material']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Texto breve de material {$datos['Texto breve de material']} registro n° {$v} "));
            if (!is_string((String) $datos['Constante']))
                return new ViewModel(array('table' => $table, "Error" => "Error en la columna Constante {$datos['Constante']} registro n° {$v} "));
                
            
          /****************************************************************
           * 
           *   Limpiando los datos para evitar ingreso de datos dañinos
           * 
           *****************************************************************/    
                
            $datos['Instalación'] = $this->soloLetrasNumeros($datos['Instalación']);
            $datos['Nombre'] = $this->soloLetrasNumeros($datos['Nombre']);
            $datos['Teléfono'] = $this->soloLetrasNumeros($datos['Teléfono']);
            $datos['Calle'] = $this->soloLetrasNumeros($datos['Calle']);
            $datos['Calle 4'] = $this->soloLetrasNumeros($datos['Calle 4']);
            $datos['Departamento'] = $this->soloLetrasNumeros($datos['Departamento']);
            $datos['Comuna'] = $this->soloLetrasNumeros($datos['Comuna']);
            $datos['Clase de Tarifa'] = $this->soloLetrasNumeros($datos['Clase de Tarifa']);
            $datos['Unidad de lectura'] = $this->soloLetrasNumeros($datos['Unidad de lectura']);
            $datos['Porción'] = $this->soloLetrasNumeros($datos['Porción']);
            $datos['Poste/Cámara'] = $this->soloLetrasNumeros($datos['Poste/Cámara']);
            $datos['Ce. emplazam.'] =  $this->soloLetrasNumeros($datos['Ce. emplazam.']);
            
            $datos['Número de serie'] = $this->soloLetrasNumeros($datos['Número de serie']);
            $datos['Material'] = $this->soloLetrasNumeros($datos['Material']);
            $datos['Marca del Medidor'] = $this->soloLetrasNumeros($datos['Marca del Medidor']);
            $datos['Propiedad del Medidor'] = $this->soloLetrasNumeros($datos['Propiedad del Medidor']);
            $datos['Texto breve de material'] = $this->soloLetrasNumeros($datos['Texto breve de material']);
            $datos['Constante'] = $this->soloLetrasNumeros($datos['Constante']);
            
            $datos['Instalación'] = strip_tags($datos['Instalación']);
            $datos['Nombre'] = strip_tags($datos['Nombre']);
            $datos['Teléfono'] = strip_tags($datos['Teléfono']);
            $datos['Calle'] = strip_tags($datos['Calle']);
            $datos['Calle 4'] = strip_tags($datos['Calle 4']);
            $datos['Departamento'] = strip_tags($datos['Departamento']);
            $datos['Comuna'] = strip_tags($datos['Comuna']);
            $datos['Clase de Tarifa'] = strip_tags($datos['Clase de Tarifa']);
            $datos['Unidad de lectura'] = strip_tags($datos['Unidad de lectura']);
            $datos['Porción'] = strip_tags($datos['Porción']);
            $datos['Poste/Cámara'] = strip_tags($datos['Poste/Cámara']);
            $datos['Ce. emplazam.'] =  strip_tags($datos['Ce. emplazam.']);
            
            $datos['Número de serie'] = strip_tags($datos['Número de serie']);
            $datos['Material'] = strip_tags($datos['Material']);
            $datos['Marca del Medidor'] = strip_tags($datos['Marca del Medidor']);
            $datos['Propiedad del Medidor'] = strip_tags($datos['Propiedad del Medidor']);
            $datos['Texto breve de material'] = strip_tags($datos['Texto breve de material']);
            $datos['Constante'] = strip_tags($datos['Constante']);
            
            $datos['Instalación'] = $this->limpiarVariables($datos['Instalación']);
            $datos['Nombre'] = $this->limpiarVariables($datos['Nombre']);
            $datos['Teléfono'] = $this->limpiarVariables($datos['Teléfono']);
            $datos['Calle'] = $this->limpiarVariables($datos['Calle']);
            $datos['Calle 4'] = $this->limpiarVariables($datos['Calle 4']);
            $datos['Departamento'] = $this->limpiarVariables($datos['Departamento']);
            $datos['Comuna'] = $this->limpiarVariables($datos['Comuna']);
            $datos['Clase de Tarifa'] = $this->limpiarVariables($datos['Clase de Tarifa']);
            $datos['Unidad de lectura'] = $this->limpiarVariables($datos['Unidad de lectura']);
            $datos['Porción'] = $this->limpiarVariables($datos['Porción']);
            $datos['Poste/Cámara'] = $this->limpiarVariables($datos['Poste/Cámara']);
            $datos['Ce. emplazam.'] =  $this->limpiarVariables($datos['Ce. emplazam.']);
            
            $datos['Número de serie'] = $this->limpiarVariables($datos['Número de serie']);
            $datos['Material'] = $this->limpiarVariables($datos['Material']);
            $datos['Marca del Medidor'] = $this->limpiarVariables($datos['Marca del Medidor']);
            $datos['Propiedad del Medidor'] = $this->limpiarVariables($datos['Propiedad del Medidor']);
            $datos['Texto breve de material'] = $this->limpiarVariables($datos['Texto breve de material']);
            $datos['Constante'] = $this->limpiarVariables($datos['Constante']);
            
            $datos['Distribuidora'] = $values["distribuidoras"]; 

            /* Busco el numero de instalacion */
            $result = $this->buscarInstalacionAction($datos['Instalación']);

            /* Si no existe en la base de inserta la informacion */
            if (!$result) {
                
                $id_instalacion = $this->guardarInstalacionAction($datos);
                
            } else {
                
                $id_instalacion = $result->getId_instalacion();
                
            }
           
            
          /*
           * 
           * Guardando datos en tabla medidor y en tabla medidor instalado.
           * 
           */
          
            /* Busco el numero de serie del medidor*/
            $result = $this->buscarNumeroSerieMedidorAction($datos['Número de serie']);
               
            /* Si no existe en la base de inserta la informacion */
            if (!$result) {
                
                $id_medidor = $this->guardarMedidorAction($datos['Número de serie']);    
                
            } else {
                
                $id_medidor = $result->getId_medidor();
                
            }
            
            // obtengo el id del medidor que tiene instalado esta instalacion 
            // luego creo la relacion instalacion con medidor 
            // en la tabla medidor_instalado
            // con esto tengo un historico de los medidores 
            // que ha tenido la instalcion
    
            /* Busco el numero de serie del medidor y la instalacion en la tabla medidor_instalado */
            $result = $this->buscarMedidorInstaladoAction((Int) $id_medidor,(Int) $id_instalacion);
            
            
            /* Si no existe en la base de inserta la informacion */
            if (!$result) {
                
                $medidor_instalado_id = $this->guardarMedidorInstaladoAction((Int) $id_medidor,(Int) $id_instalacion);    
                
            } else {
                
                $medidor_instalado_id = $result->getMedidor_instalado_id();
                
            }
            
            
            
            /*
             * 
             * Guardando datos de Aviso
             * 
             */
            

            /* Datos a Guardar en Tabla Avisos */
            $datos['id_instalacion'] = (Int) $id_instalacion;
            
            $datos['Servicio'] = (Int) get_cell('A' . $v, $objPHPExcel);
            $datos['ClaseAviso'] = get_cell('B' . $v, $objPHPExcel);
            $datos['subot'] = (String) get_cell('C' . $v, $objPHPExcel);
            $datos['Aviso'] =(String) get_cell('D' . $v, $objPHPExcel);
            $datos['FechaCreadoSAP'] = date("d-m-Y",\PHPExcel_Shared_Date::ExcelToPHP(get_cell('E'.$v,$objPHPExcel)+1)); 
            $datos['FechaAsignadoSAP'] = date("d-m-Y",\PHPExcel_Shared_Date::ExcelToPHP(get_cell('F'.$v,$objPHPExcel)+1));
            $datos['cuenta_contrato'] = (String) get_cell('H' . $v, $objPHPExcel);
            $datos['Descripción'] = (String) get_cell('I' . $v, $objPHPExcel);
            $datos['Status'] = (String) get_cell('Z' . $v, $objPHPExcel);
           
            
           /*
            * 
            * Si el aviso es vacio se crea un numero entre el numero de instalacion y un numero aleatorio
            * 
            */
            
            if((empty($datos['Aviso'])) && (!empty($datos['Instalación'])))
                 $datos['Aviso'] = $datos['Instalación'] . '_' . rand();
            
            /*******************************************************************/

            if(empty($datos['Servicio']))
                  return new ViewModel(array('table' => $table, "Error" => "La columna Servicio esta sin datos {$datos['Servicio']} registro n° {$v} "));           
            if(empty($datos['id_instalacion']))
                  return new ViewModel(array('table' => $table, "Error" => "La calumna id_instalacion esta sin datos {$datos['id_instalacion']} registro n° {$v} "));
            if(empty($datos['Aviso']))
                  return new ViewModel(array('table' => $table, "Error" => "La calumna Aviso {$datos['Aviso']} registro n° {$v} "));   
            if(empty($datos['Descripción']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Descripción {$datos['Descripción']} registro n° {$v} "));
                

            if (is_null((String) $datos['id_instalacion']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Instalación es sin datos {$datos['id_instalacion']} registro n° {$v} "));              
            if (is_null((String) $datos['Aviso']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Aviso esta sin datos {$datos['Aviso']} registro n° {$v} "));    
            if (is_null((String) $datos['Descripción']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Calle esta sin datos {$datos['Descripción']} registro n° {$v} "));  
//          if (is_null((String) $datos['Prioridad']))
//                return new ViewModel(array('table' => $table, "Error" => "Es null el campo Calle {$datos['Prioridad']} registro n° {$v} "));
            if (is_null((String) $datos['cuenta_contrato']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Calle esta sin datos {$datos['cuenta_contrato']} registro n° {$v} "));
            if (is_null((String) $datos['Servicio']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Servicio esta sin datos {$datos['Servicio']} registro n° {$v} "));                 
            if (!is_string((String) $datos['id_instalacion']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Instalación esta sin datos {$datos['id_instalacion']} registro n° {$v} "));
            if (!is_string((String) $datos['Aviso']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Aviso esta sin datos {$datos['Aviso']} registro n° {$v} "));
            if (!is_string((String) $datos['Descripción']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Descripción esta sin datos {$datos['Descripción']} registro n° {$v} "));
//          if (!is_string((String) $datos['Prioridad']))
//                return new ViewModel(array('table' => $table, "Error" => "Error en el campo Calle {$datos['Prioridad']} registro n° {$v} "));
            if (!is_string((String) $datos['cuenta_contrato']))
                return new ViewModel(array('table' => $table, "Error" => "La calumna Cuenta contrato esta sin datos {$datos['cuenta_contrato']} registro n° {$v} "));
                
                
            $datos['Servicio'] = $this->soloLetrasNumeros($datos['Servicio']);    
            $datos['id_instalacion'] = $this->soloLetrasNumeros($datos['id_instalacion']);
            $datos['Aviso'] = $this->soloLetrasNumeros($datos['Aviso']);
            $datos['Descripción'] = $this->soloLetrasNumeros($datos['Descripción']);
//            $datos['Prioridad'] = $this->soloLetrasNumeros($datos['Prioridad']);
            $datos['cuenta_contrato'] = $this->soloLetrasNumeros($datos['cuenta_contrato']);
            
            $datos['id_instalacion'] = strip_tags($datos['id_instalacion']);
            $datos['Aviso'] = strip_tags($datos['Aviso']);
            $datos['Descripción'] = strip_tags($datos['Descripción']);
//            $datos['Prioridad'] = strip_tags($datos['Prioridad']);
            $datos['cuenta_contrato'] = strip_tags($datos['cuenta_contrato']);
        
            
            $datos['Servicio'] = $this->limpiarVariables($datos['Servicio']);
            $datos['id_instalacion'] = $this->limpiarVariables($datos['id_instalacion']);
            $datos['Aviso'] = $this->limpiarVariables($datos['Aviso']);
            $datos['Descripción'] = $this->limpiarVariables($datos['Descripción']);
//            $datos['Prioridad'] = $this->limpiarVariables($datos['Prioridad']);
            $datos['cuenta_contrato'] = $this->limpiarVariables($datos['cuenta_contrato']);
           
            $datos['sucursal'] = (Int) $values["sucursal"]; 
            $datos['solicitante'] = (Int) $values["solicitantes"]; 

            $aviso_encontrado = $this->buscarAvisoAction($datos['Aviso']);
            
            
            /* Avisos Cargados y Otros 
             * que no estan 
             * Cargados se carga en arreglos 
             * para entragar respuestas 
             * al usuario*/
            
            if ( $aviso_encontrado->count() == 0 ) {
                
                 $aviso_cargado = $this->guardarAvisoAction($datos);
                 
                 foreach ($aviso_cargado as $value) {
                     
                           $arrayobj_cargados->append($value);
                           
                         }
                
            }else{

                foreach ($aviso_encontrado as $value) {
                    
                    $arrayobj_nocargados->append($value);
                    
                }
                
              }

            unset($datos);
            
       }
       
       unset($_FILES);
       unset($_POST);        
       
       /*RETORNO A PANTALLA*/
       return new ViewModel(array('arrayobj_cargados' => $arrayobj_cargados, 'cont' => $v - 2 , 'arrayobj_nocargados' => $arrayobj_nocargados ));
              
    }
    

    public function isnull(Array $datos) {
        
    }

    public function isstring(Array $datos) {
        
    }

    public function soloLetrasNumeros($cadena) {

        return preg_replace("[^A-Za-z0-9]", "", $cadena);
    }

    public function removerTags($entrada) {

        $busqueda = array(
            '@<script[^>]*?>.*?</script>@si', // javascript
            '@<[\/\!]*?[^<>]*?>@si', // HTML
            '@<style[^>]*?>.*?</style>@siU', // Css
            '@<![\s\S]*?--[ \t\n\r]*>@' // Comentarios multiples
        );

        $salida = preg_replace($busqueda, '', $entrada);
        return $salida;
    }

    public function limpiarVariables($entrada) {
        if (is_array($entrada)) {
            foreach ($entrada as $var => $val) {
                $output[$var] = $this->limpiarVariables($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $entrada = stripslashes($entrada);   
            }
            $salida = $this->removerTags($entrada);
        }
        return $salida;
    }

    public function buscarInstalacionAction($num_instalacion) {
       return $this->getInstalacionDao()->obtenerPorNumInstalacion($num_instalacion);
    }
    
    public function buscarNumeroSerieMedidorAction($num_serie_medidor){        
       return $this->getMedidorDao()->obtenerPorNumSerie($num_serie_medidor);
    }

    public function buscarMedidorInstaladoAction($id_medidor,$id_instalacion){
       return $this->getMedidorInstaladoDao()->obtenerPorIdMedidorIdInstalacion($id_medidor,$id_instalacion); 
    }
        
    public function buscarAvisoAction($num_aviso) {
       return $this->getAvisoDao()->obtenerPorNumAviso_2($num_aviso);
    }

    public function guardarInstalacionAction(Array $datos) {
        
        $data = array(    
                    'distribuidoras_id_distribuidora' => $datos['Distribuidora'],
                    'nombre_cliente' => $datos['Nombre'],
                    'direccion1' => $datos['Calle'],
                    'direccion2' => $datos['Calle 4'],
                    'num_instalacion' => $datos['Instalación'],
                    'tarifa' => $datos['Clase de Tarifa'],
                    'unidad_lectura' => $datos['Unidad de lectura'],
                    'numero_poste_camara' => $datos['Poste/Cámara'],
                    'nom_poblacion' => $datos['Comuna'], 
                    'numero_serie' => $datos['Número de serie'],
                    'material' => $datos['Material'],
                    'marca_medidor' => $datos['Marca del Medidor'],
                    'propiedad_medidor' => $datos['Propiedad del Medidor'],
                    'texto_breve_material' => $datos['Texto breve de material'],
                    'constante' => $datos['Constante'],
            // CAMPOS NUEVOS
                    'telefono' => $datos['Teléfono'],    
                    'departamento' => $datos['Departamento'],
                    'porcion' => $datos['Porción'],
                    'ce_emplazamiento' => $datos['Ce. emplazam.'],
                   );
                  
        $instalacion = new Instalacion();
        $instalacion->exchangeArray($data);

        
        try {
            $this->getInstalacionDao()->guardar($instalacion);
            return $this->getInstalacionDao()->obtenerUltimoId();  
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
        
    }

    
    /*
     * el medidor enviado por la distribuidora se guarda con modelo medidore id modelo 0
     * con esto puedo relacionar este medidor como medidor instalado
     * en terreno podran indicar cual es el id del modelo en la carga solo tengo 
     * la marca y la serie del medidor.
     * 
     */
     public function guardarMedidorAction($numserie) {
                 
        $data = array(
                    'numero_serie' => $numserie,
                    'act_reac' => 1,
                    'estado_medidor' => 1, 
                    'modelos_medidores_id_modelo' => 0,    
                   );
                  
        $medidor = new Medidor();
        $medidor->exchangeArray($data);
        
        try {
            
          $this->getMedidorDao()->guardar($medidor);
          return $this->getMedidorDao()->obtenerUltimoId();
          
        } catch (Exception $exc) {
            
          return $exc->getTraceAsString();
          
        }
        
    }
    
    
    /*
     * 
     * El medidor existiendo en la base de datos como una entidad 
     * puede ser asociado a cualquier otra instalacion asi generamos 
     * el seguimiento del medidor a nivel de estados del medidor
     * instalado,enlaboratorio,stock,devolucion,reinstalacion.
     * 
     */
    public function guardarMedidorInstaladoAction($id_medidor,$id_instalacion){
                 
        $fecha = new \DateTime();
        $fecha_instalacion = $fecha->format('d-m-Y');
                
        $data = array(
                    'medidores_id_medidor' => $id_medidor,
                    'instalaciones_id_instalacion' => $id_instalacion,
                    'medidor_instalado' => true, 
                    'fecha_instalacion' => $fecha_instalacion,    
                   );
                  
        $medidorinstalado = new MedidorInstalado;
        $medidorinstalado->exchangeArray($data);
        
        try {
            
          $this->getMedidorInstaladoDao()->guardar($medidorinstalado);
          return $this->getMedidorInstaladoDao()->obtenerUltimoId();
          
        } catch (Exception $exc) {
            
          return $exc->getTraceAsString();
          
        }       
        
    }
    
    public function guardarAvisoAction(Array $datos) {
        
        $personal_id_personal = $this->getLogin()->getIdentity()->personal_id_personal;

        $data = array(
                        'id_aviso' => null,
                        'sucursales_id_sucursal' => $datos['sucursal'],
                        'responsable_personal_id_persona' => null,
                        'supervisor_personal_id_persona' => $personal_id_personal,
                        'instalaciones_id_instalacion' => (Int) $datos['id_instalacion'],
                        'estados_avisos_id_estado_aviso' => 1,
                        'numero_aviso' => $datos['Aviso'],
                        'descripcion' => $datos['Descripción'],
                        'tipo_aviso' => 1,
                        //'fecha_carga'  => null,
                        'fecha_asignacion' => null,
                        'fecha_valorizacion' => null,
                        'fecha_finalizacion' => null,
                        'vehiculos_id_vehiculo'  => null,
                        'subot' => $datos['subot'],  
                        'solicitante_id_solicitante' => $datos['solicitante'],
                        //'prioridad' => (Int) $datos['Prioridad'],
                        'cuenta_contrato' => $datos['cuenta_contrato'],
                        'servicios_id_servicio' => $datos['Servicio'],
                        'claseaviso' => $datos['ClaseAviso'],
                        'fechacreadosap' => $datos['FechaCreadoSAP'],
                        'fechaasignadosap' => $datos['FechaAsignadoSAP'],
                        'status' => $datos['Status'],
                      );            
        
        $aviso = new Aviso();
        $aviso->exchangeArray($data);

        try {
            
            $this->getAvisoDao()->guardar($aviso);
            
            return $this->buscarAvisoAction($datos['Aviso']);
            
            
        } catch (Exception $exc) {
            
            return $exc->getTraceAsString();
            
        }
        
    }
    
    
    
    public function distribuidorasAction() {

        $SucId = (int) $this->getRequest()->getPost("SucId", 0);

        //crear y configurar el elemento provincia
        $distribuidoras = new \Zend\Form\Element\Select('distribuidoras');
        $distribuidoras->setEmptyOption('Distribuidoras');
        $distribuidoras->setAttributes(
                array(
                    'style' => 'width:350px;',
                    'id' => 'distribuidoras',
                    'onchange' => 'javascript:cargarSelectSolicitantes()',
                )
        );

        $distribuidoras->setValueOptions($this->getAvisoDao()->obtenerDistribuidoras($SucId));

        $view = new ViewModel(array(
            'distribuidoras' => $distribuidoras,
        ));

        $view->setTerminal(true);

        return $view;
    }

    public function solicitantesAction() {

        $IdDis = (int) $this->getRequest()->getPost("IdDis", 0);

        //crear y configurar el elemento provincia
        $solicitantes = new \Zend\Form\Element\Select('solicitantes');
        $solicitantes->setEmptyOption('Solicitantes');
        $solicitantes->setAttributes(
                array(
                    'style' => 'width:350px;',
                    'id' => 'solicitantes',
                )
        );

        $solicitantes->setValueOptions($this->getAvisoDao()->obtenerSolicitantes($IdDis));

        $view = new ViewModel(array(
            'solicitantes' => $solicitantes,
        ));

        $view->setTerminal(true);

        return $view;
    }

    public function reprocesarAction() {

        $id = (int) $this->getRequest()->getPost("id", 0);

        $result = $this->getAvisoDao()->ReprocesarAviso($id);

        $view = new ViewModel(array('result' => $result));

        $view->setTerminal(true);

        return $view;
    }
	
	
	public function repetidoAction() {

        $id = (int) $this->getRequest()->getPost("id", 0);

        $result = $this->getAvisoDao()->RepetidoAviso($id);

        $view = new ViewModel(array('result' => $result));

        $view->setTerminal(true);

        return $view;
    }
	

}
