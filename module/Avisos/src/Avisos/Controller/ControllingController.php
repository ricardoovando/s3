<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/* ENTITYS */
use Avisos\Model\Entity\Aviso;

class ControllingController extends AbstractActionController {
     
     private $login;
     private $avisoDao;

     private $regiones;
     private $regiones_view;

     private $meses;
     private $meses_view;
     private $CantiadMeses;
     private $cantidadDiasLaborales;

     private $resultado0;
     private $resultado1;
     
     private $fechaInicio;
     private $fechaFinal;

     private $empresas;


    function __construct($config = null) {
        $this->config = $config;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setAvisoDao($avisoDao) {
        $this->avisoDao = $avisoDao;
    }
    
    public function getAvisoDao() {
        return $this->avisoDao;
    }
    
    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
                   
        //Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        
        /*echo "<pre>"; 
        var_dump($postParams);
        exit;*/

     $this->fechaInicio = 'null';
     $this->fechaFinal = 'null';
     $this->meses = '{0}';
     $this->regiones = NULL;
     $this->empresas = '{0}';

     $parametroUno = 0;
     $parametroDos = 0;

    if(!empty($postParams["fecha_buscar_1"])  AND !empty($postParams["fecha_buscar_2"])) {
    
         $this->fechaInicio = " ' ".$postParams["fecha_buscar_1"]." ' ";
         $this->fechaFinal = " ' ".$postParams["fecha_buscar_2"]." ' ";
          
         $this->meses_view =  $postParams["fecha_buscar_1"] . " al " .$postParams["fecha_buscar_2"];

         $parametroUno = 1;
    
    }elseif(!empty($postParams["meses"])){
    
        $this->CantiadMeses = count($postParams["meses"]);

        $this->meses =  '{' ; 

        foreach ($postParams["meses"] as $key => $value) {
              
             $this->meses .= $value . ',';

            switch ($value) {
                case 1:
                    $this->meses_view .= 'Enero ';
                    break;
                case 2:
                    $this->meses_view .= 'Febrero ';
                    break;
                case 3:
                    $this->meses_view .= 'Marzo ';
                    break;
                case 4:
                    $this->meses_view .= 'Abril ';
                    break;
                 case 5:
                    $this->meses_view .= 'Mayo ';
                    break;
                 case 6:
                    $this->meses_view .= 'Junio ';
                    break;
                 case 7:
                    $this->meses_view .= 'Julio ';
                    break;
                  case 8:
                    $this->meses_view .= 'Agosto ';
                    break;
                  case 9:
                    $this->meses_view .= 'Septiembre ';
                    break;
                  case 10:
                    $this->meses_view .= 'Octubre ';
                    break;
                   case 11:
                    $this->meses_view .= 'Noviembre ';
                    break;
                   case 12:
                    $this->meses_view .= 'Diciembre ';
                    break;
                }              

        }
        
        $this->meses = $this->meses . '0}';
    
       $parametroUno = 1;

    }


if(!empty($postParams["regiones"])) {

        $this->regiones .= '{' ;


        foreach ($postParams["regiones"] as $value) {

              $this->regiones .= $value . ',';

             switch ($value) {
                    case 1:
                        $this->regiones_view .= "XV ";
                        break;
                    case 2:
                        $this->regiones_view .= "I ";
                        break;
                    case 3:
                        $this->regiones_view .= "II ";
                        break;
                    case 4:
                        $this->regiones_view .= "III ";
                        break;
                    case 5:
                        $this->regiones_view .= "IV ";
                        break;
                    case 6:
                        $this->regiones_view .= "V ";
                        break;
                    case 7:
                        $this->regiones_view .= "VI ";
                        break;
                    case 8:
                        $this->regiones_view .= "VII ";
                        break;
                    case 9:
                        $this->regiones_view .= "VIII ";
                        break;
                    case 10:
                        $this->regiones_view .= "IX ";
                        break;
                    case 15:
                        $this->regiones_view .= "RM ";
                        break;
                }

        }    

         $this->regiones = $this->regiones . '0}';
		
         $parametroDos = 2;

    }elseif(!empty($postParams["empresas"])){

            
                    $this->regiones .= '{' ;

        foreach ($postParams["empresas"] as $value) {

             switch ($value) {
                    case 1:
                        $this->regiones_view .= "CGED ";
                        $this->regiones .= '15,10,7,8,9,';
                        break;
                    case 2:
                        $this->regiones_view .= "CONAFE ";
                        $this->regiones .= '5,6,';
                        break;
                    case 3:
                        $this->regiones_view .= "EMELAT ";
                        $this->regiones .= '4,';
                        break;
                    case 4:
                        $this->regiones_view .= "ELECDA ";
                        $this->regiones .= '3,';
                        break;
                    case 5:
                        $this->regiones_view .= "ELIQSA ";
                        $this->regiones .= '2,';
                        break;
                    case 6:
                        $this->regiones_view .= "EMELARI ";
                        $this->regiones .= '1,';
                        break;
                }

        }    

         $this->regiones = $this->regiones . '0}';

        $parametroDos = 2;

      } 


        if(($parametroUno == 1) AND ($parametroDos == 2)){


             /*
               *  Obtener dias habiles 
              */ 
              $this->cantidadDiasLaborales =  $this->getAvisoDao()->obtenerDiasHabilesdeTrabajo(trim($this->fechaInicio),trim($this->fechaFinal));

              if($this->CantiadMeses == 0) $this->CantiadMeses = 1;
              if($this->cantidadDiasLaborales == 0) $this->cantidadDiasLaborales  = 20;
        

           /*
             * Obtener Panel Control Monofasico Contadores
             */
            $this->resultado0 = $this->getAvisoDao()->obtenerPanelControlMonofasicoContadores($this->meses,$this->regiones,trim($this->fechaInicio),trim($this->fechaFinal));    

            foreach ($this->resultado0 as $key => $value) {

                  $this->resultado0[$key]['parametro_out'] = ucfirst (mb_strtolower($value["parametro_out"],'UTF-8'));

                  $this->resultado0[$key]['cantidad_avisos_out'] = (empty($value["cantidad_avisos_out"]))?0:$value["cantidad_avisos_out"];

                  if($this->CantiadMeses == 1){

                          $this->resultado0[$key]['promedio_mes'] = ' - ';

                    }else{
                          
                         if($this->resultado0[$key]['cantidad_avisos_out'] > 0){

                                        $this->resultado0[$key]['promedio_mes'] =  round(($this->resultado0[$key]['cantidad_avisos_out'] / $this->CantiadMeses),1);                             
                         }else{ 

                                $this->resultado0[$key]['promedio_mes'] =  0; 

                           }

                   }


                  $this->resultado0[$key]['promedio_dia'] =  round(($this->resultado0[$key]['cantidad_avisos_out'] / $this->cantidadDiasLaborales),1);

            }


           /*
             * Obtener Panel Control Monofasico Min Max
             */
             $this->resultado1 = $this->getAvisoDao()->obtenerPanelControlMonofasicoMinMax($this->meses,$this->regiones,trim($this->fechaInicio),trim($this->fechaFinal));;     

 
            }




        return new ViewModel(array(
             'resultado0' => $this->resultado0,
             'resultado1' => $this->resultado1,
             'meses_view' => $this->meses_view,
             'regiones_view' => $this->regiones_view,
             'CantiadMeses' => $this->CantiadMeses,
             'cantidadDiasLaborales' => $this->cantidadDiasLaborales
          ));

    }
    

}
