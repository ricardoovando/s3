<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**/
use Avisos\Form\BuscadorFechas;


class FacturacionController extends AbstractActionController {

    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }
    
    private function getFormBuscadorFechas() {
        return new BuscadorFechas();
    }
    

    public function indexAction() {
        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $formBuscadorFechas = $this->getFormBuscadorFechas();
        
        return new ViewModel(array(
            'msj' => 'Saludos',
            'formBuscadorFechas' => $formBuscadorFechas,
        ));
        
    }
    
    
    public function facturarAction(){
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        
        return new ViewModel(array(
            'msj' => 'Saludos',
        ));
    }
    

}
