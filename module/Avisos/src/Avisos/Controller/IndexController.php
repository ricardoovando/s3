<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


/* FORMULARIOS */

/**/
use Avisos\Form\Buscador as BuscadorForm;
use Avisos\Form\BuscadorValidator;
/**/
use Avisos\Form\BuscadorFechas;

/**/
use Avisos\Form\SelectBrigadas;
use Avisos\Form\SelectBrigadasValidator;
/**/
use Avisos\Form\SelectSucursal;
use Avisos\Form\SelectSucursalValidator;
/**/
use Avisos\Form\SelectPrioridad;
/**/
use Avisos\Form\SelectEstado;
use Avisos\Form\SelectEstadoValidator;
/**/
use Avisos\Form\SelectPersonalTecnico;
use Avisos\Form\SelectPersonalTecnicoValidator;
/**/
use Avisos\Form\SelectPoblacion;
use Avisos\Form\SelectPoblacionValidator;
/**/
use Avisos\Form\BuscadorInstalacion;
use Avisos\Form\BuscadorInstalacionValidator;
/**/
use Avisos\Form\BuscadorUnidadLectura;
use Avisos\Form\BuscadorUnidadLecturaValidator;
/**/
use Avisos\Form\BuscadorIdServicio;
use Avisos\Form\BuscadorIdServicioValidator;

/* ENTITYS */
use Avisos\Model\Entity\Aviso;
use Avisos\Model\Entity\AvisoAsignado;

class IndexController extends AbstractActionController {

    private $avisoDao;
    private $avisoAsignadoDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function setAvisoDao($avisoDao) {
        $this->avisoDao = $avisoDao;
    }

    public function setAvisoAsignadoDao($avisoasignadoDao) {
        $this->avisoAsignadoDao = $avisoasignadoDao;
    }

    public function getAvisoDao() {
        return $this->avisoDao;
    }

    public function getAvisoAsignadoDao() {
        return $this->avisoAsignadoDao;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    private function getFormBuscador() {
        return new BuscadorForm();
    }
    
    private function getFormBuscadorFechas() {
        return new BuscadorFechas();
    }
    
    private function getFormBuscadorInstalacion(){   
        return new BuscadorInstalacion();
    }

    private function getFormBuscadorUnidadLectura(){   
        return new BuscadorUnidadLectura();
    }
    
    private function getFormBuscadorIdServicio(){   
        return new BuscadorIdServicio();
    }
    
    private function getFormSelectBrigada() {
        $form = new SelectBrigadas();
//        $form->get('brigadas')->setValueOptions($this->getAvisoDao()->obtenerBrigadasSelect());
        return $form;
    }
    
    private function getFormSelectSucursales() {
        $form = new SelectSucursal();
        $form->get('sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form;
    }
    
    public function getFormSelectEstados() {
        $form = new SelectEstado();
        $form->get('estado_aviso')->setValueOptions($this->getAvisoDao()->obtenerEstadosSelect());
        return $form; 
    }

   public function getFormSelectPersonalTecnico() {   
        $form = new SelectPersonalTecnico();
        $form->get('persona_tecnico')->setValueOptions($this->getAvisoDao()->obtenerPersonalTecnicoSelect());
        return $form; 
    }
    
    public function getFormSelectPoblacion() {   
        $form = new SelectPoblacion();
        $form->get('poblacion')->setValueOptions($this->getAvisoDao()->obtenerPoblacionesSelect());
        return $form; 
    }
    
    public function getFormSelectPrioridad(){
        return new SelectPrioridad();
    }

    /* Muestra la pantalla principal del sistema o modulo avisos */

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBuscadorUnidadLectura = $this->getFormBuscadorUnidadLectura();
        $formBuscadorIdServicio = $this->getFormBuscadorIdServicio();       
        $formSelectBrigadas = $this->getFormSelectBrigada();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formSelectPersonalTecnico = $this->getFormSelectPersonalTecnico();
        $formSelectPoblacion = $this->getFormSelectPoblacion();
        $formSelectPrioridad = $this->getFormSelectPrioridad();
        $formBuscadorFechas = $this->getFormBuscadorFechas(); 
        
        /*
         * 
         * Sino esta creada la variable de seccion se crea
         * 
         */
        
        if(empty($_SESSION['datosFiltran'])) $_SESSION['datosFiltran'] = array();
        
        
        /*
         * 
         * Si envia datos por medio de Post
         *
         */
        
        if ($this->getRequest()->isPost()) {
        
            // Obtenemos los parámetros del formulario es similar a $_POST
            $postParams = $this->request->getPost();

            
            if ((!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"])) && (!empty($postParams["buscar_tipo_fecha"]))) {

                $_SESSION['datosFiltran']['buscar_tipo_fecha'] = $postParams["buscar_tipo_fecha"];
                $_SESSION['datosFiltran']['fecha_buscar_1'] = $postParams["fecha_buscar_1"];
                $_SESSION['datosFiltran']['fecha_buscar_2'] = $postParams["fecha_buscar_2"];

               }else{
                   
                   unset($_SESSION['datosFiltran']['buscar_tipo_fecha']);
                   unset($_SESSION['datosFiltran']['fecha_buscar_1']);
                   unset($_SESSION['datosFiltran']['fecha_buscar_2']);
                                      
               }

            if ((!empty($postParams["buscar_estado_avisos"]))){
                
                $_SESSION['datosFiltran']["buscar_estado_avisos"] = $postParams["buscar_estado_avisos"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['buscar_estado_avisos']); 
                
               }
               
            if ((!empty($postParams["poblacion"]))){
                
                $_SESSION['datosFiltran']["poblacion"] = $postParams["poblacion"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['poblacion']);
                
               } 
            
            if ((!empty($postParams["sucursal"]))){
                
                $_SESSION['datosFiltran']["sucursal"] = $postParams["sucursal"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['sucursal']);
                
               }  
               
            if ((!empty($postParams["prioridad"]))){
                
                $_SESSION['datosFiltran']["prioridad"] = $postParams["prioridad"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['prioridad']);
                
               }    
               
           
           if ((!empty($postParams["numeroaviso"]))){
                
                $_SESSION['datosFiltran']["numeroaviso"] = $postParams["numeroaviso"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['numeroaviso']);
                
               }
          
               
          if ((!empty($postParams["persona_tecnico"]))){
                
                $_SESSION['datosFiltran']["persona_tecnico"] = $postParams["persona_tecnico"];
                
               }else{
                   
                unset($_SESSION['datosFiltran']['persona_tecnico']);
                
               }     
               
               
           if ((!empty($postParams["buscar_por_numero"])) && (!empty($postParams["numeroidabuscar"]))) {
               
                  $_SESSION['datosFiltran']["buscar_por_numero"] = $postParams["buscar_por_numero"];
                  $_SESSION['datosFiltran']["numeroidabuscar"] = $postParams["numeroidabuscar"];
               
              }else{
                  
                 unset($_SESSION['datosFiltran']['buscar_por_numero']); 
                 unset($_SESSION['datosFiltran']['numeroidabuscar']); 
                 
              }
              
              
       /*
        *************************************
        * ORDENAR POR AVISOS Y OTROS CAMPOS *
        *************************************
        */

          if(!empty($postParams["orden_registro"])){ 
              $_SESSION['datosFiltran']["ordenarRegistro"] = $postParams["orden_registro"];
           }else{
              unset($_SESSION['datosFiltran']["ordenarRegistro"]); 
           }   
              
               
          }else{
              
              $_SESSION['datosFiltran'] = array();
              
          }
          
          
        /*
         *
         * Poblando el Formulario de Busqueda 
         *           
         */ 
          
         if ((!empty($_SESSION['datosFiltran']['buscar_tipo_fecha'])))     
               $formBuscadorFechas->get('buscar_tipo_fecha')->setValue($_SESSION['datosFiltran']['buscar_tipo_fecha']);
         
         if ((!empty($_SESSION['datosFiltran']['fecha_buscar_1'])))     
               $formBuscadorFechas->get('fecha_buscar_1')->setValue($_SESSION['datosFiltran']['fecha_buscar_1']);
         
         if ((!empty($_SESSION['datosFiltran']['fecha_buscar_2']))) 
               $formBuscadorFechas->get('fecha_buscar_2')->setValue($_SESSION['datosFiltran']['fecha_buscar_2']);
         
         if ((!empty($_SESSION['datosFiltran']['buscar_estado_avisos'])))
               $formBuscadorFechas->get('buscar_estado_avisos')->setValue($_SESSION['datosFiltran']["buscar_estado_avisos"]);
         
         if ((!empty($_SESSION['datosFiltran']['poblacion'])))
               $formSelectPoblacion->get('poblacion')->setValue($_SESSION['datosFiltran']['poblacion']); 
         
         if ((!empty($_SESSION['datosFiltran']['sucursal'])))
               $formSelectSucurales->get('sucursal')->setValue($_SESSION['datosFiltran']['sucursal']); 
         
         if ((!empty($_SESSION['datosFiltran']['prioridad'])))
               $formSelectPrioridad->get('prioridad')->setValue($_SESSION['datosFiltran']['prioridad']); 
         
         if ((!empty($_SESSION['datosFiltran']['buscar_por_numero'])))
                $formBuscadorFechas->get('buscar_por_numero')->setValue($_SESSION['datosFiltran']['buscar_por_numero']);
             
         if((!empty($_SESSION['datosFiltran']['numeroidabuscar'])))
                $formBuscadorFechas->get('numeroidabuscar')->setValue($_SESSION['datosFiltran']['numeroidabuscar']);

         if((!empty($_SESSION['datosFiltran']["ordenarRegistro"])))
                $formBuscadorFechas->get('orden_registro')->setValue($_SESSION['datosFiltran']["ordenarRegistro"]);

         if((!empty($_SESSION['datosFiltran']["persona_tecnico"])))
                $formSelectPersonalTecnico->get('persona_tecnico')->setValue($_SESSION['datosFiltran']["persona_tecnico"]);
           
          if((!empty($_SESSION['datosFiltran']["numeroaviso"])))
                 $formBuscador->get('numeroaviso')->setValue($_SESSION['datosFiltran']["numeroaviso"]);
         
        /* Filtrar solo por los estados Necesarios */
        $paginator = $this->getAvisoDao()->obtenerTodos($_SESSION['datosFiltran']);     
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(100);

        
        return new ViewModel(array(
                'title' => 'Asignación y Valorización de Avisos',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
                'formBuscadorIdServicio' => $formBuscadorIdServicio,
                'formEstados' => $formSelectEstados,
                'formBrigadas' => $formSelectBrigadas,
                'formSucursales' => $formSelectSucurales,
                'formPersonalTecnico' => $formSelectPersonalTecnico,
                'formPoblacion' => $formSelectPoblacion,
                'formSelectPrioridad' => $formSelectPrioridad,
                'formBuscadorFechas' => $formBuscadorFechas
              ));
        
        
        }

    
      

      
   /* 
    * 
    * Metodo consultado desde una tablet. 
    * 
    */
    public function b2a553d36975dda502e24014d2161a27Action(){
        
        
        $uri_autorizada = 'http://movil.medicions3.com/newhtmlAjax.html';
        
        
//        if(!$_SERVER['HTTP_REFERER']==$uri_autorizada){    
//              $view = new ViewModel(array(
//                'callback' => 'ERROR 1'
//              ));
//            $view->setTerminal(true);
//            return $view;
//           }

        
       // si los datos de consulta no son enviados por get no se ejecuta el codigo 
       if(!$this->getRequest()->isGet()){            
            exit;
          }
            

        // Obtenemos los parámetros
        $getParams = $this->request->getQuery();     
        
        /**************************
         * 
         *      Validaciones
         *   Si no existen estos campos no se ejecuta el codigo
         * 
         **************************/

         if(empty($getParams['5ac4c962a1220b30c26e994b0ecfa755b4e0389f'])){            
            exit;
          }
          
         if(empty($getParams['5326ee20f69411ffc6d821a9bdb513832e3409a0'])){            
            exit;
          }
          
         if(empty($getParams['callback'])){            
            exit;
          }  
         
         $idPersona = $getParams['5ac4c962a1220b30c26e994b0ecfa755b4e0389f'];
         $idUsuario = $getParams['5326ee20f69411ffc6d821a9bdb513832e3409a0'];
         $callback = $getParams['callback'];
         
  
         /*PARAMETROS TIENEN QUE SER DIGITOS*/
         $validatorDigits = new \Zend\Validator\Digits();
         
         if(!$validatorDigits->isValid($idPersona)){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 6'
              ));
            $view->setTerminal(true);
            return $view;
          }
         
         if(!$validatorDigits->isValid($idUsuario)){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 7'
              ));
            $view->setTerminal(true);
            return $view;
          }
         
          
          
         /*LARGO DE PARAMETROS*/ 
         if (!strlen($idPersona)>1 && strlen($idPersona)<3){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 8'
              ));
            $view->setTerminal(true);
            return $view;
          }
         
         if (!strlen($idUsuario)>1 && strlen($idUsuario)<3){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 9'
              ));
            $view->setTerminal(true);
            return $view;
          }
         
          
          /*LARGO DE PARAMETROS*/         
         $validatorStringLength = new \Zend\Validator\StringLength(array('min' => 1, 'max' => 3));
                   
         if(!$validatorStringLength->isValid($idPersona)){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 10'
              ));
            $view->setTerminal(true);
            return $view;
          }
         
         if(!$validatorStringLength->isValid($idUsuario)){            
            $view = new ViewModel(array(
                'callback' => 'ERROR 11'
              ));
            $view->setTerminal(true);
            return $view;
          }
                  
         $idPersona = strip_tags($idPersona);
         $idUsuario = strip_tags($idUsuario);
         $callback = strip_tags($callback);
        
        // 
        // Consulta a la base de datos si este usuario tiene avisos asignados.
        // 
         
         $avisosAsignados = $this->getAvisoDao()->obtenerAvisosAsignadosTecnico(trim($idPersona),trim($idUsuario));
        
            $data = array();
            $i=0;
            foreach ($avisosAsignados as $aviso) {
                
                $result = $this->getAvisoDao()->ObtenerHistorialNumeroAviso($aviso->getNumero_aviso());
             
                        $data[$i]['idAviso'] = $aviso->getId_aviso();

                        $data[$i]['lat'] = $result["lat"];
                        $data[$i]['lon'] = $result["lon"];       
                        
                        $data[$i]['numAvi'] = $aviso->getNumero_aviso();
                        $data[$i]['tipAvi'] = $aviso->getClaseaviso(); 
                        
                        $data[$i]['origenAviso'] = $aviso->getSolicitante()->getDepartamento_distri();
                                
                        $data[$i]['codigoTipoServicio'] =  $aviso->getServicio()->getId_servicio();       
                        $data[$i]['tipoServicio'] = $aviso->getServicio()->getNombre_servicio();
                        
                        $data[$i]['descripcionAviso'] = ucwords(strtolower($aviso->getDescripcion()));
                        
                        $data[$i]['distri'] =   $aviso->getDistribuidora()->getNombre(); 
                                
                        $data[$i]['sucursal'] = $aviso->getSucursal()->getNombre_sucursal();       
                        
                        $data[$i]['nomCli'] = ucwords(strtolower($aviso->getInstalacion()->getNombre_cliente()));
                        $data[$i]['dir1'] = ucwords(strtolower($aviso->getInstalacion()->getDireccion1()));
                        $data[$i]['dir2'] = ucwords(strtolower($aviso->getInstalacion()->getDireccion2()));
                        $data[$i]['nombreComuna'] = ucwords(strtolower($aviso->getInstalacion()->getNom_poblacion())); 
                        
                        $data[$i]['tel'] = trim($aviso->getInstalacion()->getTelefono());
                        
                        $data[$i]['numInsta'] = $aviso->getInstalacion()->getNum_instalacion();
                                                
                        $data[$i]['rDis'] = $result['r_dis']; 
                        $data[$i]['tiEmp'] = $result['ti_emp'];         
                        $data[$i]['tiAco'] = $result['ti_aco']; 
                        $data[$i]['auto'] = $result['auto'];
                        $data[$i]['plaPos'] = $result['pla_pos'];        
                        
                        /*Medidor que tendria que estar instalado*/ 
                        $data[$i]['coMe'] = $result['co_me_ins'];        
                        $data[$i]['nuSe'] = $result['nu_se_ins'];        
                        $data[$i]['anFa'] = $result['a_fa_ins'];        
                        
                        /*Sellos que tendrian que estar instalados*/
                        $data[$i]['cuEn'] = $result['cu_de'];        
                        $data[$i]['blEn'] = $result['bl_de'];        
                        $data[$i]['cmEn'] = $result['cm_de']; 
                        
                        /*Medidor a Devolver o Reinstalar*/
                        $data[$i]['coMeIns'] = $result['co_me'];        
                        $data[$i]['nuSeIns'] = $result['nu_se'];       
                        $data[$i]['aFaIns'] = $result['an_fa'];
                        
                        /*Antecedenes de la Antencion*/
                        $data[$i]['anormalidadEncontrada'] = $result['res_ev_ins_0'];        
                        $data[$i]['comentarioTecnico'] = $result['co_fin'];        
                        $data[$i]['comentarioLaboratorio'] = $result['comentario_laboratorio'];
                        
                        $data[$i]['marcaMedidor'] = $aviso->getInstalacion()->getMarca_medidor();
                        $data[$i]['numeroSerie'] = $aviso->getInstalacion()->getNumero_serie();
                        $data[$i]['propiedadMedidor'] = $aviso->getInstalacion()->getPropiedad_medidor();
                        $data[$i]['tarifa'] = $aviso->getInstalacion()->getTarifa();
                        $data[$i]['constante'] = $aviso->getInstalacion()->getConstante();
                        $data[$i]['numeroPosteCamara'] = $aviso->getInstalacion()->getNumero_poste_camara();
                        
                        $data[$i]['Linfe'] = null;
 
                        $i++;

                     }

                     
        /*
         * Retornamos a la Vista con un OK
         */
        $view = new ViewModel(array(        
            'callback' => $callback . '({ "success":true , "avisos":'  . json_encode($data) . '})',
          ));

        $view->setTerminal(true);
        
        return $view;
          
      }   
      
 
      

      
      
   /* 
    * 
    * Este informe muestra el estado de las sucursales al dia del mes actual
    * 
    */
      
    public function informeProduccionMensualAction(){
            
        /*
         * Retornamos a la Vista con un OK
        */
        $datos = $this->getAvisoDao()->obtenerProduccionMensual();
    
        $viewModel = new ViewModel(array(
                  'resumen_produccion' => $datos,
                 ));

          $viewModel->setTerminal(true);

          return $viewModel;
          
      }
    
      
   /*
    * 
    * Listado de Servicios que Aplican en la sucursal del usuario 
    * 
    */   
    
    public function listadoServiciosSucursalAction(){
            
        /*
         * Retornamos a la Vista con un OK
        */
        $datos = $this->getAvisoDao()->obtenerListadoServiciosSucursal();
    
        $viewModel = new ViewModel(array(
                  'listado_servicios_sucursal' => $datos,
                 ));

          $viewModel->setTerminal(true);

          return $viewModel;
          
      }  
      
   
   /*
    * 
    * Listado de Todos los Servicios a Nivel Nacional 
    *
    */   
    
    public function listadoServiciosNacionalAction(){
            
        /*
         * Retornamos a la Vista con un OK
        */
        $datos = $this->getAvisoDao()->obtenerServiciosNacional();
    
        $viewModel = new ViewModel(array(
                  'listado_servicios_nacional' => $datos,
                 ));

          $viewModel->setTerminal(true);

          return $viewModel;
          
      }
      
  
   /*
    * 
    * Listado de Modelo-Medidores
    * 
    */   
   
    public function listadoModelosMedidoresAction(){
            
        /*
         * Retornamos a la Vista con un OK
        */
        $datos = $this->getAvisoDao()->obtenerModelosMedidores();
    
        $viewModel = new ViewModel(array(
                  'modelos_medidores' => $datos,
                 ));

          $viewModel->setTerminal(true);

          return $viewModel;
          
      }
      
      
      
    
   /*
     * 
     * Este informe muestra la produccion semanal de la sucursal del usuario
     * 
     */

    public function informeProduccionSemanalAction() {

     /*
      * Retornamos a la Vista con un OK
      */
      $_DATOS = $this->getAvisoDao()->obtenerProduccionSemanal();
             
      $objPHPExcel = new \PHPExcel();

      // Establecer propiedades
      $objPHPExcel->getProperties()
        ->setCreator("Medicion")
        ->setLastModifiedBy("Tecnet")
        ->setTitle("Informe Ejecutivo Mensual")
        ->setSubject("Informe Ejecutivo Mensual")
        ->setDescription("Se entrega un detalle de los trabajos ejecutados en el servicio trifasico medicion.")
        ->setKeywords("Excel")
        ->setCategory("Excel");

       $objPHPExcel->setActiveSheetIndex(0);

       $estiloTituloReporte = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 16,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_NONE
                )
            ),
            
        );
        
        
      $estiloNombresCamposHead = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => '16365C')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        
      
         $estiloCamposTotal = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FFFF00')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
       
         
       $estiloCampos = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => false,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
      
       
        /*TITULOS CON OTRO COLOR*/   
        $_LETRAS = array('A','B','C','D','E','F','G','H',
            'I','J','K','L','M','N','O','P','Q','R','S','T','U','V',
            'W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG');
        
        $_NOMCOLUMNAS = array('NUMSOLICITUDTRI','MESFACTURACION','ZONAL','OFICINA','DISTRIBUIDORAS','ESTADOSOLICITUD',
                              'NUMRIAT','RESULTAUDITORIA','CONSTANTEFACTURACION','AVISO','NUMSERVICIO','FECHASOLICITUD',
                              'FECHAEJECUCION','CLIENTE','DIRECCION','COMUNA','TARIFA','TIPOEMPALME','CIRCUITO','CONEXION',
                              'MEDIDA','NUMEROSERIE','MARCA','ACTIVADEJADA','NUMEROSERIEREAC','MARCAREAC','REACTIVADEJADA',
                              'COMENTARIOS','CAMBIOMEDIDORRESU','SERIECAMBIO','MARCACAMBIO','MODELOCAMBIO','PROPIEDADCAMBIO');
        
    for ($index = 0; $index < count($_LETRAS); $index++) {

        $objPHPExcel->getActiveSheet()->SetCellValue($_LETRAS[$index].'1',$_NOMCOLUMNAS[$index]);
        $objPHPExcel->getActiveSheet()->getStyle($_LETRAS[$index].'1')->applyFromArray($estiloNombresCamposHead);
        $objPHPExcel->getActiveSheet()->getColumnDimension($_LETRAS[$index])->setWidth(20);  

      }
                
    
    $numfila = 2;
    
    foreach ($_DATOS as $fila) {
        
        $objPHPExcel->getActiveSheet()->SetCellValue( 'A'.$numfila , trim( $fila->getNumsolicitudtri() ) ); //NUMSOLICITUDTRI
        $objPHPExcel->getActiveSheet()->SetCellValue( 'B'.$numfila , trim( $fila->getMesfacturacion() ) ); //MESFACTURACION
        $objPHPExcel->getActiveSheet()->SetCellValue( 'C'.$numfila , trim( $fila->getZonal() ) ); //ZONAL
        $objPHPExcel->getActiveSheet()->SetCellValue( 'D'.$numfila , trim( $fila->getOficina() ) ); //OFICINA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'E'.$numfila , trim( $fila->getDistribuidoras() ) ); //DISTRIBUIDORAS
        $objPHPExcel->getActiveSheet()->SetCellValue( 'F'.$numfila , trim( $fila->getEstadosolicitud() ) ); //ESTADOSOLICITUD
        $objPHPExcel->getActiveSheet()->SetCellValue( 'G'.$numfila , trim( $fila->getNum_form_riat() ) ); //NUMRIAT
        $objPHPExcel->getActiveSheet()->SetCellValue( 'H'.$numfila , trim( $fila->getResultadoauditoria() ) ); //RESULTAUDITORIA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'I'.$numfila , trim( $fila->getConstante_facturacion() ) ); //CONSTANTEFACTURACION
        
        $objPHPExcel->getActiveSheet()->getStyle('J'.$numfila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $objPHPExcel->getActiveSheet()->SetCellValue( 'J'.$numfila , trim( $fila->getAviso() ) ); //AVISO
        
        $objPHPExcel->getActiveSheet()->SetCellValue( 'K'.$numfila , trim( $fila->getNumservicio() ) ); //NUMSERVICIO
                
        $fechaCarga = new \DateTime($fila->getFecha_carga());
        $fechaCarga = $fechaCarga->format('d-m-Y');
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$numfila ,$fechaCarga); //FECHASOLICITUD  
        
        $fechaEjecucion = new \DateTime($fila->getFechaejecucion());
        $fechaEjecucion = $fechaEjecucion->format('d-m-Y');
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$numfila,$fechaEjecucion); //FECHAEJECUCION
                
        $objPHPExcel->getActiveSheet()->SetCellValue( 'N'.$numfila , trim( $fila->getCliente() ) ); //CLIENTE
        $objPHPExcel->getActiveSheet()->SetCellValue( 'O'.$numfila , trim( $fila->getDireccion() ) ); //DIRECCION
        $objPHPExcel->getActiveSheet()->SetCellValue( 'P'.$numfila , trim( $fila->getComuna() ) ); //COMUNA 
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Q'.$numfila , trim( $fila->getTarifa() ) ); //TARIFA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'R'.$numfila , trim( $fila->getTipoempalme() ) ); //TIPOEMPALME
        $objPHPExcel->getActiveSheet()->SetCellValue( 'S'.$numfila , trim( $fila->getCircuito() ) ); //CIRCUITO
        $objPHPExcel->getActiveSheet()->SetCellValue( 'T'.$numfila , trim( $fila->getConexion() ) ); //CONEXION
        $objPHPExcel->getActiveSheet()->SetCellValue( 'U'.$numfila , trim( $fila->getMedida() ) ); //MEDIDA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'V'.$numfila , trim( $fila->getNumeroserie() ) ); //NUMEROSERIE
        $objPHPExcel->getActiveSheet()->SetCellValue( 'W'.$numfila , trim( $fila->getMarca() ) ); //MARCA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'X'.$numfila , trim( $fila->getActivadejada() ) ); //ACTIVADEJADA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Y'.$numfila , trim( $fila->getNumeroseriereac() ) ); //NUMEROSERIEREAC
        $objPHPExcel->getActiveSheet()->SetCellValue( 'Z'.$numfila , trim( $fila->getMarcareac() ) ); //MARCAREAC
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AA'.$numfila , trim( $fila->getReactivadejada() ) ); //REACTIVADEJADA
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AB'.$numfila , trim( $fila->getComentarios() ) ); //COMENTARIOS
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AC'.$numfila , trim( $fila->getCambiomedidorresu() ) ); //CAMBIOMEDIDORRESU
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AD'.$numfila , trim( $fila->getSeriecambio() ) ); //SERIECAMBIO
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AE'.$numfila , trim( $fila->getMarcacambio() ) ); //MARCACAMBIO
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AF'.$numfila , trim( $fila->getModelocambio() ) ); //MODELOCAMBIO
        $objPHPExcel->getActiveSheet()->SetCellValue( 'AG'.$numfila , trim( $fila->getPropiedadcambio() ) ); //PROPIEDADCAMBIO

        $numfila++;   
           
      }
      

        // Renombrar Hoja
        $objPHPExcel->getActiveSheet()->setTitle('Informe Semanal');

        // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
        $objPHPExcel->setActiveSheetIndex(0);

        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="informeProduccionSemanal.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

      
    }

    
    
/* 
 * 
 * Este informe muestra la produccion semanal de la sucursal del usuario
 * 
 */
    public function informeProduccionSemanal2Action() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();


            // Obtenemos los parámetros del formulario es similar a $_POST
            $postParams = $this->request->getPost();

            $datos = new \ArrayObject();
            $export = 0;

            if ((!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"]))) {

                $datos = $this->getAvisoDao()->obtenerProduccionSemanal2($postParams["fecha_buscar_1"], $postParams["fecha_buscar_2"]);
                $export = 1;
                
            }
                
        $formBuscadorFechas = $this->getFormBuscadorFechas();

        $viewModel = new ViewModel(array(
            'produccion_semanal' => $datos,
            'formBuscadorFechas' => $formBuscadorFechas,
            'export' => $export,
          ));

        if( (!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"])) ) {
            
            $viewModel->setTerminal(true);
            
        }
        
        return $viewModel;
        
     }
      
      

/* 
 * 
 * Este informe muestra la produccion semanal de la sucursal del usuario del servicio monofasico
 * 
 */
    public function informeProduccionSemanalMonofasicoAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

            // Obtenemos los parámetros del formulario es similar a $_POST
            $postParams = $this->request->getPost();

            $this->regiones = NULL;
            $datos = new \ArrayObject();
            $export = 0;

            if ((!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"]))) {

                $datos = $this->getAvisoDao()->obtenerProduccionSemanalMonofasico($postParams["fecha_buscar_1"], $postParams["fecha_buscar_2"],$this->regiones);
                $export = 1;
                
            }

                
        $formBuscadorFechas = $this->getFormBuscadorFechas();

        $viewModel = new ViewModel(array(
            'produccion_semanal' => $datos,
            'formBuscadorFechas' => $formBuscadorFechas,
            'export' => $export,
          ));

        if( (!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"])) ) {
            
            $viewModel->setTerminal(true);
            
        }
        
        return $viewModel;
        
     }
      
  
/* 
 * 
 * Este informe muestra la produccion semanal de la sucursal del usuario del servicio monofasico desarrollado para ser enviado a la sucursal
 * 
 */
    public function informeProduccionSemanalMonofasicoClienteAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $this->regiones = array();
     
        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        
        
        if(!empty($postParams["regiones"])) {

        foreach ($postParams["regiones"] as $value) {
              $this->regiones[] = $value;
            }
         
        }elseif(!empty($postParams["empresas"])){

        foreach ($postParams["empresas"] as $value) {

             switch ($value) {
                    case 1:
                        //"CGED ";
                        $this->regiones[] = 15;
                        $this->regiones[] = 10;
                        $this->regiones[] = 7;
                        $this->regiones[] = 8;
                        $this->regiones[] = 9;
                        break;
                    case 2:
                        //"CONAFE ";
                        $this->regiones[] = 5;
                        $this->regiones[] = 6;
                        break;
                    case 3:
                        //"EMELAT ";
                        $this->regiones[] = 4;
                        break;
                    case 4:
                        //"ELECDA ";
                        $this->regiones[] = 3;
                        break;
                    case 5:
                        //"ELIQSA ";
                        $this->regiones[] = 2;
                        break;
                    case 6:
                        //"EMELARI ";
                        $this->regiones[] = 1;
                        break;
                }

        }

      } 
            
            
            $datos = new \ArrayObject();
            $export = 0;

            if ((!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"]))) {

                $datos = $this->getAvisoDao()->obtenerProduccionSemanalMonofasico($postParams["fecha_buscar_1"], $postParams["fecha_buscar_2"],$this->regiones);
                $export = 1;
                
            }

                
        $formBuscadorFechas = $this->getFormBuscadorFechas();

        $viewModel = new ViewModel(array(
            'produccion_semanal' => $datos,
            'formBuscadorFechas' => $formBuscadorFechas,
            'export' => $export,
          ));

        if( (!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"])) ) {
            
            $viewModel->setTerminal(true);
            
        }
        
        return $viewModel;
        
     }
                  
      
   /* 
    * 
    * Este informe detalle de la facturacion
    * 
    */
    public function informeDetalleFacturacionAction(){

        $datos = $this->getAvisoDao()->ObtenerDetalleFacturacion();
    
        $viewModel = new ViewModel(array(
                  'detalle_facturacion' => $datos,
                 ));

          $viewModel->setTerminal(true);

          return $viewModel;
          
      }
      
      
      
   /* 
    * 
    * Este informe detalle de la facturacion TALCA
    * 
    */
    public function informeDetalleFacturacion1Action(){
          
      $this->layout()->usuario = $this->getLogin()->getIdentity();

      // Obtenemos los parámetros del formulario es similar a $_POST
      $postParams = $this->request->getPost();

      $datos = new \ArrayObject();
      $export = 0;

      if((!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"]))) {
           $datos = $this->getAvisoDao()->ObtenerDetalleFacturacionTalca($postParams["fecha_buscar_1"], $postParams["fecha_buscar_2"]);
           $export = 1;
         }
                
      $formBuscadorFechas = $this->getFormBuscadorFechas();

      $viewModel = new ViewModel(array(
            'detalle_facturacion' => $datos,
            'formBuscadorFechas' => $formBuscadorFechas,
            'export' => $export,
          ));

      if( (!empty($postParams["fecha_buscar_1"])) && (!empty($postParams["fecha_buscar_2"])) ) {
            $viewModel->setTerminal(true);
       }
        
       return $viewModel;
          
      }
      
      
      
      
   /* 
    * 
    * Este es el informe que se entrega a CGE mensualmente llamado informe ejecutivo auditorias tecnicas.
    * 
    */
    public function informeEjecutivoAuditoriasAction(){   
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
         
        $meses =  $this->getAvisoDao()->ObtenerMesesEjecutados();
        
        $viewModel = new ViewModel(array(
                  'meses' => $meses
                 ));
        
        return $viewModel;
          
      }
      
    
    public function exportarEjecutivoAuditoriasAction(){
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $mes = (int) $this->params()->fromRoute('id', 0);
        $anio = (int) $this->params()->fromRoute('br', 0);
 
        if (!$mes) {
            return $this->redirect()->toRoute('avisos', array('controller' => 'index' , 'action' => 'informeejecutivoauditorias'));
           }
        
        if (!$anio) {
            return $this->redirect()->toRoute('avisos', array('controller' => 'index' , 'action' => 'informeejecutivoauditorias'));
           }
        
       /*
        * 
        * Retornamos a la Vista con un OK
        * 
        */
        
        $regiones_delinforme = $this->getAvisoDao()->ObtenerRegionesInformeEjecutivo();   
           
        $totalAuditorias = $this->getAvisoDao()->ObtenerInstalacionesAuditadasRegiones($mes,$anio);
        
        $totalResultadoEstados = $this->getAvisoDao()->ObtenerResultadosAuditoriasTotalesEstadosRegiones($mes,$anio);
        
        $totalResultadoEstadosPorcentajes = $this->getAvisoDao()->ObtenerResultadosAuditoriasTotalesEstadosPorcentajesRegiones($mes,$anio);
        
        $totalSumResumenEjecutivo = $this->getAvisoDao()->ObtenerSumResumenEjecutivo($mes,$anio);
        
        
        /*
         *
         * INICIO CODIGO 
         * 
         */
        
        
        $objPHPExcel = new \PHPExcel();
        
        $objPHPExcel->getProperties()->setCreator("Patricio Cardenas");
        $objPHPExcel->getProperties()->setLastModifiedBy("Patricio Cardenas");
        $objPHPExcel->getProperties()->setTitle("Informe Auditorias Tecnicas");
        $objPHPExcel->getProperties()->setSubject("Informe Auditorias Tecnicas");
        $objPHPExcel->getProperties()->setDescription("Informe Auditorias Tecnicas");

        $objPHPExcel->setActiveSheetIndex(0);
        
        
        if($mes === 1) $m = 'Enero';
        if($mes === 2) $m = 'Febrero';
        if($mes === 3) $m = 'Marzo';
        if($mes === 4) $m = 'Abril';
        if($mes === 5) $m = 'Mayo';
        if($mes === 6) $m = 'Junio';
        if($mes === 7) $m = 'Julio';
        if($mes === 8) $m = 'Agosto';
        if($mes === 9) $m = 'Septiembre';
        if($mes === 10) $m = 'Octubre';
        if($mes === 11) $m = 'Noviembre';
        if($mes === 12) $m = 'Diciembre';
        
        
        /*TOTALES DE INSTALACIONES AUDITADAS*/
                

        $estiloTituloReporte = array(
            'font' => array(
                'name' => 'Verdana',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 16,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_NONE
                )
            ),
            
        );
        
        
      $estiloNombresCamposHead = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => '16365C')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        
      
         $estiloCamposTotal = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => true,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FFFF00')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
       
         
       $estiloCampos = array(
            'font' => array(
                'name' => 'Calibri',
                'bold' => false,
                'italic' => false,
                'strike' => false,
                'size' => 11,
                'color' => array(
                    'rgb' => '000000'
                )
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
        );
        
        
        /*TITULO DEL EXCEL*/
       
        $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Resumen Auditorias Técnicas '.$m.' '.$anio.' – CGED');
        $objPHPExcel->getActiveSheet()->getStyle('B2:F2')->applyFromArray($estiloTituloReporte);
                
        /*ANCHO DE LAS COLUMNAS*/
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);  
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        
        
        /*TITULOS CON OTRO COLOR*/
                       
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Servicios Auditados');
        $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($estiloNombresCamposHead);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Regiones');
        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($estiloNombresCamposHead);
                
        $objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Cantidad Auditorias');
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($estiloNombresCamposHead);
        
        
        $i = 6;
                
        foreach ( $regiones_delinforme as $value ){            
            
            $objPHPExcel->getActiveSheet()->SetCellValue( 'B'.$i , trim( $value->getNombre_region() ) );
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos); 
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
            
            foreach ( $totalAuditorias as $total ){
             
              if( $total->getRegion() === $value->getNombre_region() ){ 
                  
                    $objPHPExcel->getActiveSheet()->SetCellValue( 'C'.$i , trim($total->getCantidad()) );
                    
                    break;
                
                }
                
              }
            
            $i++;
            
        }
        
            $objPHPExcel->getActiveSheet()->SetCellValue( 'B'.$i , trim('TOTAL') );
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos); 
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
            
            foreach ( $totalAuditorias as $total ){
             
              if( $total->getRegion() === 'TOTAL' ){ 
                  
                    $objPHPExcel->getActiveSheet()->SetCellValue( 'C'.$i , trim($total->getCantidad()) );
                    
                    break;
                
                }
                
              }
        
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':C'.$i)->applyFromArray($estiloCamposTotal);   
        
        
 /*********************************************************************************************/
        
        
        
        /*GRAFICO TORTA 1 ******************************************/
          
        
                $objWorksheet = $objPHPExcel->getActiveSheet();


                //Set the Labels for each data series we want to plot
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $dataseriesLabels1 = array(
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$B$4', NULL, 1),    //  2011
                );
                
                
                //Set the X-Axis Labels
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $xAxisTickValues1 = array(
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$B$6:$B$11', NULL, 4),  //  Q1 to Q4
                );
                
                
                //Set the Data values for each data series we want to plot
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $dataSeriesValues1 = array(
                        new \PHPExcel_Chart_DataSeriesValues('Number', 'Informe!$C$6:$C$10', NULL, 4),
                );

                //Build the dataseries
                $series1 = new \PHPExcel_Chart_DataSeries(
                        \PHPExcel_Chart_DataSeries::TYPE_PIECHART,      // plotType
                        \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,  // plotGrouping
                        range(0, count($dataSeriesValues1)-1),      // plotOrder
                        $dataseriesLabels1,             // plotLabel
                        $xAxisTickValues1,              // plotCategory
                        $dataSeriesValues1                  // plotValues
                     );

                //Set up a layout object for the Pie chart
                $layout1 = new \PHPExcel_Chart_Layout();
                $layout1->setShowVal(TRUE);
                $layout1->setShowPercent(TRUE);

                //Set the series in the plot area
                $plotarea1 = new \PHPExcel_Chart_PlotArea($layout1, array($series1));
                //Set the chart legend
                $legend1 = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, true);

                $title1 = new \PHPExcel_Chart_Title('Servicios Auditados');


                //Create the chart
                $chart1 = new \PHPExcel_Chart(
                        'chart1',       // name
                        $title1,        // title
                        $legend1,       // legend
                        $plotarea1,     // plotArea
                        true,           // plotVisibleOnly
                        1,          // displayBlanksAs
                        NULL,           // xAxisLabel
                        NULL            // yAxisLabel - Pie charts don't have a Y-Axis
                    );

                //Set the position where the chart should appear in the worksheet
                $chart1->setTopLeftPosition('H3');
                $chart1->setBottomRightPosition('P16');

                //Add the chart to the worksheet
                $objWorksheet->addChart($chart1);

          
          /*
           * *********************************************************+
           * 
           */
                
                
            /* GRAFICO 2 */
                
                
                $objWorksheet = $objPHPExcel->getActiveSheet();

                
                //Set the Labels for each data series we want to plot
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $dataseriesLabels = array(
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$C$14', NULL, 1), //NORMAL
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$D$14', NULL, 1), //CON OBS
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$E$14', NULL, 1), //CNR
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$F$14', NULL, 1), //FRUSTRADO
                     );
                
                //Set the X-Axis Labels
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $xAxisTickValues = array(
                        new \PHPExcel_Chart_DataSeriesValues('String', 'Informe!$B$15:$B$19', NULL, 4), //REGIONES
                      );
                
                //Set the Data values for each data series we want to plot
                //Datatype
                //Cell reference for data
                //Format Code
                //Number of datapoints in series
                //Data values
                //Data Marker
                $dataSeriesValues = array(
                        new \PHPExcel_Chart_DataSeriesValues('Number', 'Informe!$C$15:$C$19', NULL, 4),
                        new \PHPExcel_Chart_DataSeriesValues('Number', 'Informe!$D$15:$D$19', NULL, 4),
                        new \PHPExcel_Chart_DataSeriesValues('Number', 'Informe!$E$15:$E$19', NULL, 4),
                        new \PHPExcel_Chart_DataSeriesValues('Number', 'Informe!$F$15:$F$19', NULL, 4),
                     );

                //Build the dataseries
                $series = new \PHPExcel_Chart_DataSeries(
                        \PHPExcel_Chart_DataSeries::TYPE_BARCHART,  // plotType
                        \PHPExcel_Chart_DataSeries::GROUPING_STANDARD,  // plotGrouping
                        range(0, count($dataSeriesValues)-1),       // plotOrder
                        $dataseriesLabels,              // plotLabel
                        $xAxisTickValues,               // plotCategory
                        $dataSeriesValues               // plotValues
                );
                
                //Set additional dataseries parameters
                //Make it a vertical column rather than a horizontal bar graph
                $series->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);

                //Set the series in the plot area
                $plotarea = new \PHPExcel_Chart_PlotArea(NULL, array($series));
                
                //Set the chart legend
                $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

                $title = new \PHPExcel_Chart_Title('Resultados Auditorias');
                $yAxisLabel = new \PHPExcel_Chart_Title('Cantidad Auditorias');


                //Create the chart
                $chart2 = new \PHPExcel_Chart(
                        'chart2',       // name
                        $title,         // title
                        $legend,        // legend
                        $plotarea,      // plotArea
                        true,           // plotVisibleOnly
                        0,              // displayBlanksAs
                        NULL,               // xAxisLabel
                        $yAxisLabel         // yAxisLabel
                   );

                //Set the position where the chart should appear in the worksheet
                $chart2->setTopLeftPosition('Q3');
                $chart2->setBottomRightPosition('AD19');

                //  Add the chart to the worksheet
                $objWorksheet->addChart($chart2);
                
                
        /*+++++++++++++++++++++*/




        /*TOTALES POR ESTADOS DE AUDITORIAS*/
        
        $i = $i + 3;
        
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':F'.$i)->applyFromArray($estiloNombresCamposHead);
                
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, 'Resultados');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'NORMAL');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, 'CON OBS');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, 'CNR');
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, 'VIAJE FRUSTRADO');

        $i++;
        
        foreach ($regiones_delinforme as $value ) {

            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, trim($value->getNombre_region()));
            $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray($estiloCampos);

                  $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray($estiloCampos);
                  $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray($estiloCampos);
                  $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray($estiloCampos);
                  $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray($estiloCampos);
                  
                  
            foreach ($totalResultadoEstados as $total) {
                
                if($value->getNombre_region() === $total->getRegion()){
                  
                        if ($total->getResultado() === 'NORMAL') {
                               $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, trim($total->getCantidad()));
                               continue;    
                           }

                        if ($total->getResultado() === 'CON OBS') {
                               $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, trim($total->getCantidad()));
                               continue;
                           }

                        if ($total->getResultado() === 'CNR') {
                               $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, trim($total->getCantidad()));
                               continue;
                           }

                        if ($total->getResultado() === 'FRUSTRADO') {
                               $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, trim($total->getCantidad()));
                               continue;
                           }  
               
                   }   
                    
               }

            $i++;
        }

        
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, trim('TOTAL'));
        $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray($estiloCampos);

        $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray($estiloCampos);

        foreach ($totalResultadoEstados as $total) {

            if ($total->getRegion() === 'TOTAL') {

                if ($total->getResultado() === 'NORMAL') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, trim($total->getCantidad()));
                    continue;
                }

                if ($total->getResultado() === 'CON OBS') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, trim($total->getCantidad()));
                    continue;
                }

                if ($total->getResultado() === 'CNR') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, trim($total->getCantidad()));
                    continue;
                }

                if ($total->getResultado() === 'FRUSTRADO') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, trim($total->getCantidad()));
                    continue;
                }
            }
        }


        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':F'.$i)->applyFromArray($estiloCamposTotal); 
        
        
        /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/   
        
        
        
        
        /*TOTALES POR ESTADOS DE AUDITORIAS PORCENTAJES*/
        
        $i = $i + 3;
        
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, 'Resultados');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'NORMAL');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, 'CON OBS');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, 'CNR');        
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, 'VIAJE FRUSTRADO');
        
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':F'.$i)->applyFromArray($estiloNombresCamposHead);
                
        $i++;

        foreach ($regiones_delinforme as $value ) {
            
           $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, trim($value->getNombre_region()));
           $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos);
           
           $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
           $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
           $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
           $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
           
           $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
           $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloCampos);
           $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloCampos);
           $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloCampos);
            
            
           foreach ($totalResultadoEstadosPorcentajes as $total) {
            
             if($value->getNombre_region() === $total->getRegion()){
                 
                 if ($total->getResultado() === 'NORMAL') {
                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, trim($total->getPorcentaje()));
                        continue;    
                    }

                 if ($total->getResultado() === 'CON OBS') {
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, trim($total->getPorcentaje()));
                        continue;
                    }

                 if ($total->getResultado() === 'CNR') {
                        $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, trim($total->getPorcentaje()));
                        continue;
                    }

                 if ($total->getResultado() === 'FRUSTRADO') {
                        $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, trim($total->getPorcentaje()));
                        continue;
                    }  
                  
               }
             
           }
           
           $i++;
            
        }
        
        
        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $i, trim('TOTAL'));
        $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray($estiloCampos);

        $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
           
        $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray($estiloCampos);
        $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray($estiloCampos);

        foreach ($totalResultadoEstadosPorcentajes as $total) {

            if ($total->getRegion() === 'TOTAL') {

                if ($total->getResultado() === 'NORMAL') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $i, trim($total->getPorcentaje()));
                    continue;
                }

                if ($total->getResultado() === 'CON OBS') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $i, trim($total->getPorcentaje()));
                    continue;
                }

                if ($total->getResultado() === 'CNR') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $i, trim($total->getPorcentaje()));
                    continue;
                }

                if ($total->getResultado() === 'FRUSTRADO') {
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $i, trim($total->getPorcentaje()));
                    continue;
                }
            }
        }


        $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':F'.$i)->applyFromArray($estiloCamposTotal); 

        /*++++++++++*/ 
        
        
        
        /* TABLA CLASIFICION ANORMALIDADES */
       
       $i = $i + 2;
       
       $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'Anormalidades');
       $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloNombresCamposHead);
       
       $i++;
       
       $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, 'Regiones');
       $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'En caja de empalme');
       $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, 'En ECM');
       $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, 'En medidor');
       $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, 'Esquema de medida');
       $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, 'Falta medida reactiva');
       $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, 'Alarma de bateria');
       $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, 'Carga baja o variable');
       $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, 'Tarifa no corresponde');
       $objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, 'Falta Sello');
       $objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, 'TTCC');
       $objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, 'Otros');
       
       $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($estiloNombresCamposHead);
       $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray($estiloNombresCamposHead);
       
       $i++;

       foreach ($totalSumResumenEjecutivo as $total) {
                
                $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray($estiloCampos);
                                
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $total->getNombre_region());
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $total->getSum());
                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $total->getSum_1());
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $total->getSum_2());
                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $total->getSum_3());
                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $total->getSum_4());
                $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $total->getSum_5());
                $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, $total->getSum_6());
                $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, $total->getSum_7());
                $objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, $total->getSum_8());
                $objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, $total->getSum_9());
                $objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, $total->getSum_10());
                
                $i++;
                
             }
             
             
             $i--;

             $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':M'.$i)->applyFromArray($estiloCamposTotal); 

        
        /*++++++++++++++++++++++++++++++++*/
        
        
        /* TABLA CLASIFICACION CNR */
        
        $i = $i + 2;
        
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'CNR');
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloNombresCamposHead);
            
        $i++;    

            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, 'Regiones');
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, 'Medidor registra solo una fraccion de la energia suministrada');
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, 'Defecto en el cableado del sistema de medida');
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, 'Conexion ilegal a la red de distribucion');
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, 'Medidor sin programacion');
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, 'Medidor presenta falla de funcinamiento');
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, 'Medidor percial o totalmente destruido');
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, 'Tarifa encontrada en terreno no corresponde');
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, 'Medidor Fuera de Clase');

            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($estiloNombresCamposHead);
                   
       $i++;

       foreach ($totalSumResumenEjecutivo as $total) {
                
                $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($estiloCampos);
                                
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $total->getNombre_region());
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $total->getSum_11());
                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $total->getSum_12());
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $total->getSum_13());
                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $total->getSum_14());
                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $total->getSum_15());
                $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $total->getSum_16());
                $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, $total->getSum_17());
                $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, $total->getSum_18());
                
                $i++;
                
             }
             
             
             $i--;

             $objPHPExcel->getActiveSheet()->getStyle('B'.$i.':J'.$i)->applyFromArray($estiloCamposTotal); 
        
        
        /*++++++++++++++++++++++++++*/
        
          
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Informe');

        
        /*CREANDO LIBROS SEGUN REGIONES COMPROMETIDAS*/
        
        $hoja = 0;
        
        foreach ($regiones_delinforme as $region ) {
            
           $hoja++;
        
           $objPHPExcel->createSheet();
        
           $objPHPExcel->setActiveSheetIndex($hoja);
        
           //Set the worksheet tab color
           $objPHPExcel->getActiveSheet()->getTabColor()->setARGB('FF0094FF');;

            /*ANCHO DE LAS COLUMNAS*/
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);   
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);   
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(15);
       
            /*+++++++++++*/

            $objPHPExcel->getActiveSheet()->SetCellValue('I3', 'Constantes');
            $objPHPExcel->getActiveSheet()->getStyle('I3')->applyFromArray($estiloNombresCamposHead);

            $objPHPExcel->getActiveSheet()->SetCellValue('P3', 'Anormalidades');
            $objPHPExcel->getActiveSheet()->getStyle('P3')->applyFromArray($estiloNombresCamposHead);

            $objPHPExcel->getActiveSheet()->SetCellValue('AA3', 'CRN');
            $objPHPExcel->getActiveSheet()->getStyle('AA3')->applyFromArray($estiloNombresCamposHead);

            $objPHPExcel->getActiveSheet()->SetCellValue('A4','Nº Cliente');
            $objPHPExcel->getActiveSheet()->SetCellValue('B4','Aviso');
            $objPHPExcel->getActiveSheet()->SetCellValue('C4','Fecha Ejecución');
            $objPHPExcel->getActiveSheet()->SetCellValue('D4','Cliente');
            $objPHPExcel->getActiveSheet()->SetCellValue('E4','Direccion');
            $objPHPExcel->getActiveSheet()->SetCellValue('F4','Comuna');
            $objPHPExcel->getActiveSheet()->SetCellValue('G4','Tarifa');
            $objPHPExcel->getActiveSheet()->SetCellValue('H4','Tipo de Medida');
            $objPHPExcel->getActiveSheet()->SetCellValue('I4','Informada por la distribuidora');
            $objPHPExcel->getActiveSheet()->SetCellValue('J4','Calculada en terreno');
            $objPHPExcel->getActiveSheet()->SetCellValue('K4','Resultado');
            $objPHPExcel->getActiveSheet()->SetCellValue('L4','Observaciones');
            $objPHPExcel->getActiveSheet()->SetCellValue('M4','Oficina');
            $objPHPExcel->getActiveSheet()->SetCellValue('N4','Numero RIAT');
            $objPHPExcel->getActiveSheet()->SetCellValue('O4','Ejecutado por');
            $objPHPExcel->getActiveSheet()->SetCellValue('P4','EN CAJA DE EMPALME ');
            $objPHPExcel->getActiveSheet()->SetCellValue('Q4','EN ECM');
            $objPHPExcel->getActiveSheet()->SetCellValue('R4','EN MEDIDOR');
            $objPHPExcel->getActiveSheet()->SetCellValue('S4','ESQUEMA DE MEDIDA');
            $objPHPExcel->getActiveSheet()->SetCellValue('T4','FALTA MEDIDA REACTIVA');
            $objPHPExcel->getActiveSheet()->SetCellValue('U4','ALARMA DE BATERIA');
            $objPHPExcel->getActiveSheet()->SetCellValue('V4','CARGA BAJA O VARIABLE');
            $objPHPExcel->getActiveSheet()->SetCellValue('W4','TARIFA NO CORRESPONDE');
            $objPHPExcel->getActiveSheet()->SetCellValue('X4','FALTA SELLO');
            $objPHPExcel->getActiveSheet()->SetCellValue('Y4','TTCC');
            $objPHPExcel->getActiveSheet()->SetCellValue('Z4','OTROS');      
            $objPHPExcel->getActiveSheet()->SetCellValue('AA4','MEDIDOR REGISTRA SOLO UNA FRACCION DE LA ENERGIA SUMINISTRADA');
            $objPHPExcel->getActiveSheet()->SetCellValue('AB4','DEFECTO EN EL CABLEADO DEL SISTEMA DE MEDIDA');
            $objPHPExcel->getActiveSheet()->SetCellValue('AC4','CONEXIÓN ILEGAL A LA RED DE DISTRIBUCION');
            $objPHPExcel->getActiveSheet()->SetCellValue('AD4','MEDIDOR SIN PROGRAMACION');
            $objPHPExcel->getActiveSheet()->SetCellValue('AE4','MEDIDOR PRESENTA FALLA DE FUNCIONAMIENTO');
            $objPHPExcel->getActiveSheet()->SetCellValue('AF4','MEDIDOR PARCIAL O TOTALMENTE DESTRUIDO');
            $objPHPExcel->getActiveSheet()->SetCellValue('AG4','TARIFA ENCONTRADA EN TERRENO NO CORRESPONDE');
            $objPHPExcel->getActiveSheet()->SetCellValue('AH4','MEDIDOR FUERA DE CLASE');
       
            $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('J4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('K4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('L4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('M4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('N4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('O4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('P4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('Q4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('R4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('S4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('T4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('U4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('V4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('W4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('X4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('Y4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('Z4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AA4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AB4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AC4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AD4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AE4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AF4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AG4')->applyFromArray($estiloNombresCamposHead);
            $objPHPExcel->getActiveSheet()->getStyle('AH4')->applyFromArray($estiloNombresCamposHead);
               
       
            $totalResumenEjecutivo = $this->getAvisoDao()->ObtenerResumenEjecutivo($region->getId_region(),$mes,$anio);   /*Consulta por RM*/
               
       $i = 5;

       foreach ($totalResumenEjecutivo as $total) {
                
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('O'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('R'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('S'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('U'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('W'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('Y'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AA'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AC'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AE'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AG'.$i)->applyFromArray($estiloCampos);
                $objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($estiloCampos);           
                
                $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $total->getNum_cliente() );
                
                $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $total->getAviso() );
                
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $total->getFecha_ejecucion());
                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $total->getNombre_cliente());
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $total->getDireccion());
                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $total->getComuna());
                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $total->getTarifa());
                $objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $total->getTipo_medida());
                $objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, $total->getConstante_distribuidora());
                $objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, $total->getConstante_terreno());
                $objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, $total->getResultadoauditorias());
                $objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, $total->getObservacion());
                $objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, $total->getOficina());
                $objPHPExcel->getActiveSheet()->SetCellValue('N'.$i, $total->getNum_riat());
                $objPHPExcel->getActiveSheet()->SetCellValue('O'.$i, $total->getEjecudado());
                $objPHPExcel->getActiveSheet()->SetCellValue('P'.$i, $total->getAnor_encajadeempalme());
                $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$i, $total->getAnor_enecm());
                $objPHPExcel->getActiveSheet()->SetCellValue('R'.$i, $total->getAnor_enmedidor());
                $objPHPExcel->getActiveSheet()->SetCellValue('S'.$i, $total->getAnor_esquemademedida());
                $objPHPExcel->getActiveSheet()->SetCellValue('T'.$i, $total->getAnor_faltamedidareactiva());
                $objPHPExcel->getActiveSheet()->SetCellValue('U'.$i, $total->getAnor_alarmadebateria());
                $objPHPExcel->getActiveSheet()->SetCellValue('V'.$i, $total->getAnor_cargabajavariable());
                $objPHPExcel->getActiveSheet()->SetCellValue('W'.$i, $total->getAnor_tarifanocorresponde());
                $objPHPExcel->getActiveSheet()->SetCellValue('X'.$i, $total->getAnor_faltasello());
                $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$i, $total->getAnor_ttcc());
                $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$i, $total->getAnor_otro());      
                $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$i, $total->getClas_cnr_1());
                $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$i, $total->getClas_cnr_2());
                $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$i, $total->getClas_cnr_3());
                $objPHPExcel->getActiveSheet()->SetCellValue('AD'.$i, $total->getClas_cnr_4());
                $objPHPExcel->getActiveSheet()->SetCellValue('AE'.$i, $total->getClas_cnr_5());
                $objPHPExcel->getActiveSheet()->SetCellValue('AF'.$i, $total->getClas_cnr_6());
                $objPHPExcel->getActiveSheet()->SetCellValue('AG'.$i, $total->getClas_cnr_7());
                $objPHPExcel->getActiveSheet()->SetCellValue('AH'.$i, $total->getClas_cnr_8());
                
                $i++;
                
             } 
                

            $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        
            $objPHPExcel->getActiveSheet()->setTitle($region->getNombre_corto());
        
        
        }
        
        /************************************************/
        
        /************************************************/
                
        // Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
        $objPHPExcel->setActiveSheetIndex(0);
                
        // Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="InformeEjecutivoTrifasico.xlsx"');
        header('Cache-Control: max-age=0');
        
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(TRUE);
        $objWriter->save('php://output');
        
        exit;
        
    }  
      
      
    
    
    
    /*
     * 
     * Recibe el numero de brigada 
     * para buscar los avisos asignados 
     * a esta brigada
     * 
     */
//    public function buscarbrigadaAction($postParams) {
//
//        /* LLamar a Formularios */
//        $formSelectBrigadas = $this->getFormSelectBrigada();
//        $formBuscador = $this->getFormBuscador();
//        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
//        $formBuscadorUnidadLectura = $this->getFormBuscadorUnidadLectura();
//
//        /* Reconfiguracion de Paginador */
//        $paginator = $this->getAvisoDao()->obtenerPorNumBrigada($postParams["brigadas"]);
//        $paginator->setCurrentPageNumber(0);
//        $paginator->setItemCountPerPage(20);
//
//        $viewModel = new ViewModel(array(
//            'title' => 'Avisos Encontrados',
//            'listaAvisos' => $paginator->getIterator(),
//            'paginator' => $paginator,
//            'formBuscador' => $formBuscador,
//            'formBrigadas' => $formSelectBrigadas,
//            'formBuscadorInstalacion' => $formBuscadorInstalacion,
//            'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
//        ));
//
//        /* Cambio del Templede del Accion */
//        $viewModel->setTemplate("avisos/index/index");
//
//        return $viewModel;
//        
//    }

    
    /*
    * 
     * Asignar Aviso a Tecnico
    * 
    */
    public function asignarTecnicoAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            
              $this->redirect()->toRoute('avisos', array('controller' => 'index'));
            
           }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();


        /*
         * Con la Misma Action se puede obtener 
         * un filtro por nombre de brigada
         * si no esta el nombre de brigada 
         * direcciona al index del modulo
         * 
         */
        if (!empty($postParams["buscartecnico"])) {

            if (!empty($postParams["persona_tecnico"])) {

                return $this->buscarTecnicoAction($postParams);
                
            } else {

                return $this->redirect()->toRoute('avisos', array('controller' => 'index'));
            }
        }
        
        
        
       /*
        * imprimir informe asignacion a terreno 
        */
        if (!empty($postParams["informe_asignacion"])) {

            if (!empty($postParams["persona_tecnico"])) {

                return $this->informeAsignacionTerrenoAction($postParams);
                
            } else {

                return $this->redirect()->toRoute('avisos', array('controller' => 'index'));
            }
        }
        

        /*
         * 
         * Asignacion de Avisos a Brigada Seleccionada
         * 
         * 
         */

        /* LLamar a Formularios */
        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBuscadorUnidadLectura = $this->getFormBuscadorUnidadLectura();
        $formBuscadorIdServicio = $this->getFormBuscadorIdServicio();
        
        $formSelectBrigadas = $this->getFormSelectBrigada();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formSelectPersonalTecnico = $this->getFormSelectPersonalTecnico();
        $formSelectPoblacion = $this->getFormSelectPoblacion();
        $formSelectPrioridad = $this->getFormSelectPrioridad();
        
        
        $formSelectPersonalTecnico->setInputFilter(new SelectPersonalTecnicoValidator());
        $formSelectPersonalTecnico->setData($postParams);

        /*
         * 
         * Falla Validacion del Form. Brigadas
         * 
         * 
         */
        if (!$formSelectPersonalTecnico->isValid())
        {
            
            //Falla la validación; volvemos a generar el formulario     
            $paginator = $this->getAvisoDao()->obtenerTodos();
            $paginator->setCurrentPageNumber(0);
            $paginator->setItemCountPerPage(200);

            $modelView = new ViewModel(array(
                'title' => 'Error en la Busqueda',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
                'formBuscadorIdServicio' => $formBuscadorIdServicio,
                'formEstados' => $formSelectEstados,
                'formBrigadas' => $formSelectBrigadas,
                'formSucursales' => $formSelectSucurales,
                'formPersonalTecnico' =>  $formSelectPersonalTecnico,
                'formPoblacion' =>  $formSelectPoblacion,
                'formSelectPrioridad' =>  $formSelectPrioridad,
               ));

            $modelView->setTemplate('avisos/index/index');

            return $modelView;    
            
        }
        
        $values = $formSelectPersonalTecnico->getData();        

        /* Asignar id de aviso uno a uno con la brigada seleccionada */
        $datos = array();

        $avisos_asignados = explode('_',$postParams["avisos_asignados"]);
        
        foreach ($avisos_asignados as $id_aviso) {

          if(!empty($id_aviso)){
            
            $datos['id_aviso'] = $id_aviso;
            $datos['responsable_personal_id_persona'] = $values["persona_tecnico"];
            $datos['estados_avisos_id_estado_aviso'] = 2;

            $fecha = new \DateTime();
            $datos['fecha_asignacion'] = $fecha->format('d-m-Y');

            $aviso = new Aviso();
            $aviso->exchangeArray($datos);
            
            $this->getAvisoDao()->updateTecnicoResponsableAsignado($aviso);

            /************************************* */
            
          } 
            
        }

        return $this->redirect()->toRoute('avisos', array('controller' => 'index'));    
        
        
    }
    
    /*
     * Asignar Aviso a Brigada
     * 
     */

//    public function asignarAction() {
//
//        $this->layout()->usuario = $this->getLogin()->getIdentity();
//
//        if (!$this->getRequest()->isPost()) 
//        {
//            $this->redirect()->toRoute('avisos', array('controller' => 'index'));
//        }
//
//        // Obtenemos los parámetros del formulario es similar a $_POST
//        $postParams = $this->request->getPost();
//        
//        
//        /*
//         * Con la Misma Action se puede obtener 
//         * un filtro por nombre de brigada
//         * si no esta el nombre de brigada 
//         * direcciona al index del modulo
//         * 
//         */
//        if (!empty($postParams["buscarbrigada"])) {
//
//            if (!empty($postParams["brigadas"])) {
//
//                return $this->buscarbrigadaAction($postParams);
//                
//            } else {
//
//                return $this->redirect()->toRoute('avisos', array('controller' => 'index'));
//            }
//        }
//
//        /*
//         * 
//         * Asignacion de Avisos a Brigada Seleccionada
//         * 
//         * 
//         */
//
//        $formBuscador = $this->getFormBuscador();
//        $formSelectBrigadas = $this->getFormSelectBrigada();
//        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
//        $formBuscadorUnidadLectura = $this->getFormBuscadorUnidadLectura();
//        $formBuscadorIdServicio = $this->getFormBuscadorIdServicio();
//        
//        $formSelectPoblacion = $this->getFormSelectPoblacion();
//        $formSelectPrioridad = $this->getFormSelectPrioridad();
//        
//        $formSelectBrigadas->setInputFilter(new SelectBrigadasValidator());
//        $formSelectBrigadas->setData($postParams);
//
//        /*
//         * 
//         * Falla Validacion del Form. Brigadas
//         * 
//         * 
//         */
//        if (!$formSelectBrigadas->isValid())
//        {
//            
//            //Falla la validación; volvemos a generar el formulario     
//            $paginator = $this->getAvisoDao()->obtenerTodos();
//
//            $paginator->setCurrentPageNumber(0);
//
//            $paginator->setItemCountPerPage(200);
//
//            $modelView = new ViewModel(array(
//                'title' => 'Error en la Carga',
//                'listaAvisos' => $paginator->getIterator(),
//                'paginator' => $paginator,
//                'formBuscador' => $formBuscador,
//                'formBrigadas' => $formSelectBrigadas,
//                'formBuscadorInstalacion' => $formBuscadorInstalacion,
//                'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
//                'formBuscadorIdServicio' => $formBuscadorIdServicio,
//                'formSelectPrioridad' =>  $formSelectPrioridad,
//            ));
//
//            $modelView->setTemplate('avisos/index/index');
//
//            return $modelView;
//            
//            
//        }
//
//        
//        
//        $values = $formSelectBrigadas->getData();        
//
//        /* Asignar id de aviso uno a uno con la brigada seleccionada */
//        $datos1 = array();
//        $datos2 = array();
//        
//        if($_POST["asignar_aviso"]>0){
//        
//        foreach ($_POST["asignar_aviso"] as $id_aviso) {
//
//            $datos['avisos_id_aviso'] = $id_aviso;
//            $datos['brigadas_id_brigada'] = $values["brigadas"];
//
//            /* Inserto la relacion entre la brigada y el id de aviso */
//            $avisoasignado = new AvisoAsignado();
//            $avisoasignado->exchangeArray($datos);
//
//            /* Guardando O Actualiando */
//            $this->getAvisoAsignadoDao()->guardarAvisoAsignado($avisoasignado);
//
//
//            /* Rutina para actualizar el estado del aviso una vez que es asignado a una brigada */
//
//            $datos2['id_aviso'] = $id_aviso;
//            $datos2['estados_avisos_id_estado_aviso'] = 2;
//
//            $fecha = new \DateTime();
//            $datos2['fecha_asignacion'] = $fecha->format('d-m-Y');
//
//            $aviso = new Aviso();
//            $aviso->exchangeArray($datos2);
//
//            $this->getAvisoDao()->updateEstado($aviso);
//
//            /************************************* */
//            
//        }
//        
//        }
//
//        return $this->redirect()->toRoute('avisos', array('controller' => 'index'));    
//    }
    
    



/* 
* 
* Buscador por Tecnico Responsable de los Avisos 
* 
*/
public function informeAsignacionTerrenoAction($postParams) {
            
        /*
         * Retornamos a la Vista con un OK
        */
        $avisosAsignados = $this->getAvisoDao()->obtenerAvisosAsignadosTecnico($postParams['persona_tecnico'],null);
    
        $viewModel = new ViewModel(array(
                  'avisosAsignados' => $avisosAsignados,
                 ));

          /* Cambio del Templede del Accion */
          $viewModel->setTemplate("avisos/index/informeasignacionterreno");

          $viewModel->setTerminal(true);

          return $viewModel;
          
      } 

      
    
public function suspenderAction() {
	
	$id = (int) $this->getRequest()->getPost("numeroaviso", 0);
	
	var_dump($_POST);
	
	exit;
	
    foreach ($_POST["asignar_aviso"] as $id_aviso) {
      
            $datos['id_aviso'] = $id_aviso;
            $datos['estados_avisos_id_estado_aviso'] = 5;
            $fecha = new \DateTime();
            $datos['fecha_suspendido'] = $fecha->format('d-m-Y');

            $aviso = new Aviso();
            $aviso->exchangeArray($datos);

            $this->getAvisoDao()->updateEstadoAvisosSuspender($aviso);
      
       }
          
       return $this->redirect()->toRoute('avisos', array('controller' => 'index'));  
         
     }
     
     
     
     
public function cambiarservicioAction() {
  
  
        if (!$this->getRequest()->isPost()) 
        {
            $this->redirect()->toRoute('avisos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
       
        $listado_avisos = explode('_',$postParams["lista_avisos"]);

            foreach ($listado_avisos as $id_aviso) {
              
                    $datos['id_aviso'] = $id_aviso;
                    $datos['servicios_id_servicio'] = $postParams["servicio_seleccionado"];

                    $aviso = new Aviso();
                    $aviso->exchangeArray($datos);

                    $this->getAvisoDao()->updateServiciodelAviso($aviso);
              
               }
          
        return $this->redirect()->toRoute('avisos', array('controller' => 'index'));  
         
     }
     
     
     
     public function reprocesarAction() {

        $id = (int) $this->getRequest()->getPost("numeroaviso", 0);

        $result = $this->getAvisoDao()->ReprocesarNumeroAviso($id);

        return $this->redirect()->toRoute('avisos', array('controller' => 'index'));  

     }
    
    
     
    
    /*
     * 
     * 
     * Registro de Casos
     * 
     * 
     * 
     */

    public function registrarAction() {


        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('avisos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();


        $form = $this->getForm();
        $form->setInputFilter(new RegistroRiatValidator());

        $form->setData($postParams);


        if (!$form->isValid()) {
            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('titulo' => 'Datos ingresados no son correctos', 'form' => $form));

            $modelView->setTemplate('avisos/index/crear');

            return $modelView;
        }

        $values = $form->getData();

        echo '<pre>';
        print_r($values);
        echo '</pre>';

        exit;

        /*
          $usuario = new Usuario();
          $usuario->exchangeArray($values);

          $this->getUsuarioDao()->guardar($usuario);
          return $this->redirect()->toRoute('admin/default', array('controller' => 'usuario'));
         */
    }

}
