<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class LaboratorioController extends AbstractActionController {

    private $login;
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function indexAction() {
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        return new ViewModel(array());
    }

    public function cargarAction() {

        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('avisos', array('controller' => 'laboratorio'));
        }
        
        //Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();
        
		/*DESDE LA HOJA*/
        $hoja_desde = strtoupper($postParams['hoja_desde']);
        
		/*HASTA LA HOJA*/
        $hoja_hasta = strtoupper($postParams['hoja_hasta']);

		
        /*variable to load the table to export to excel*/
        $magic_table = '';
        
        $tname = $_FILES['file']['tmp_name'][0];
        $type = $_FILES['file']['type'][0];
        
        if ($type == 'application/vnd.ms-excel') {
            // Extension excel 97
            $ext = 'xls';
        } else if ($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            // Extension excel 2007 y 2010
            $ext = 'xlsx';
        } else {
            // Extension no valida
            echo -1;
            return new ViewModel(array('Error' => 'Documento Incorrecto'));
        }

        $xlsx = 'Excel2007';
        $xls = 'Excel5';

        //creando el lector
        $objReader = \PHPExcel_IOFactory::createReader($$ext);

        
        //cargamos el archivo
        $objPHPExcel = $objReader->load($tname);
        $allSheetName = $objPHPExcel->getSheetNames(); 
        

        function redondear_dos_decimal($valor) { 
            $float_redondeado=round($valor * 100) / 100; 
            return $float_redondeado; 
         }

         $magic_table .= '<table class="table table-striped table-hover table-bordered table-condensed" >';
         $magic_table .= '<thead><tr style="background-color:#003bb3;color:#ffffff;" class="btn-primary" >';
         $magic_table .= '<th>Num Lista</th>';
         $magic_table .= '<th>Num_Serie_Medidor</th>';
         
		 
         $r=0;
		 
		 /***************************************
		  * 
		  * Creando la Los titulos del excel
		  * 
		  **************************************/
		  
         for ($index1 = (count($allSheetName)-1); $index1 >= 0 ; $index1--) {

              $objPHPExcel->setActiveSheetIndex($index1);
           		   
			  /*******************************
			   * 
			   * VALIDAMOS DESDE LA HOJA
			   * 
			   *******************************/	   
		      if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_desde){
               
					  for ($index2 = $index1; $index2 >= 0 ; $index2--) {
					  	
						  $objPHPExcel->setActiveSheetIndex($index2); 
					   
			              if($r<3){
			              	 
			                  $magic_table .= '<th>';
			                  $magic_table .=   str_replace("Ø", "&Oslash;", $objPHPExcel->getActiveSheet()->getCell("C7")->getValue()) .'_'. $objPHPExcel->getActiveSheet()->getTitle();
			                  $magic_table .= '</th>';
			                  $r++;
							  
			               }else{
			               	
			                  $magic_table .= '<th class="btn-primary" >';
			                  $magic_table .= '_Prom_';
			                  $magic_table .= '</th>';
			                  $r=0;
			                  $index2++;
						   
			               }
						   
						   	  /*********************************
						       * 
						       * VALIDAMOS HASTA LA HOJA
						       * 
						       ********************************/
						    
		                       if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) break;
		                  
					   }


                        if(($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) && ($r==3)){
                        	
							   $magic_table .= '<th class="btn-primary" >';
			                   $magic_table .= '_Prom_';
			                   $magic_table .= '</th>';
							
                          }
 
			   
				 }
			  
			  
			    /******************************
				 * 
				 * VALIDAMOS HASTA LA HOJA
				 * 
				 *****************************/
				 
			    if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) break;
	           
	         }
	 
        $magic_table .= '</tr></thead>';

        
       foreach ( $_FILES['file']['name'] as $key => $value) {
         
        $tname = $_FILES['file']['tmp_name'][$key];
        $type = $_FILES['file']['type'][$key];
        
        if ($type == 'application/vnd.ms-excel') {
            // Extension excel 97
            $ext = 'xls';
        } else if ($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            // Extension excel 2007 y 2010
            $ext = 'xlsx';
        } else {
            // Extension no valida
            echo -1;
            return new ViewModel(array('Error' => 'Documento Incorrecto'));
        }

        $xlsx = 'Excel2007';
        $xls = 'Excel5';

        //creando el lector
        $objReader = \PHPExcel_IOFactory::createReader($$ext);

        //cargamos el archivo
        $objPHPExcel = $objReader->load($tname);
        $allSheetName = $objPHPExcel->getSheetNames();
            
            $magic_table .= '<tr>';
            $magic_table .= '<td style="text-align:center" >'.$key.'</td>';
            
            $objPHPExcel->setActiveSheetIndex(7);
            $magic_table .= '<td style="text-align:right" >';
            $magic_table .=     $objPHPExcel->getActiveSheet()->getCell("C3")->getValue();
            $magic_table .= '</td>';
                   
            $r=0; 
            $sumar = array();
            
			
			/*INICIO DE DATOS CALCULO DE PROMEDIO*/
			
            for ($index1 = (count($allSheetName)-1); $index1 >= 0 ; $index1--) {

              $objPHPExcel->setActiveSheetIndex($index1);
           		   
				   
			  /****************************
			   * 
			   * VALIDAMOS DESDE LA HOJA
			   * 
			   ****************************/	   
		      if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_desde){
               
					  for ($index2 = $index1; $index2 >= 0 ; $index2--) {
					  	
						  $objPHPExcel->setActiveSheetIndex($index2); 
     
		                  if($r<3){
		                  	 
			                    /*EL DATO DE ERROR*/   
			                    $magic_table .= '<td style="text-align:right" >';
			                    $magic_table .=     str_replace(".",",",$objPHPExcel->getActiveSheet()->getCell("D36")->getValue()); 
			                    $magic_table .= '</td>';
			                    $sumar[] = $objPHPExcel->getActiveSheet()->getCell("D36")->getValue();
			                    $r++;
							
		                  }else{
						  	
			                    /*PROMEDIOS*/  
			                    $magic_table .= '<td style="background-color:#e3e3e3;text-align:right" >';
			                    $magic_table .=     str_replace(".",",",round((($sumar[0] + $sumar[1] + $sumar[2]) / 3),3));
			                    $magic_table .= '</td>';
			                    $r=0;
			                    $index2++;
			                    unset($sumar);
			                    $sumar = array();
							
		                  }
						  
							/***************************
							 * VALIDAMOS HASTA LA HOJA *
							 ***************************/
						   
						     if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) break;
						  
		               }


                       if(($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) && ($r==3)){
						     
							 $magic_table .= '<td style="background-color:#e3e3e3;text-align:right" >';
			                 $magic_table .=     str_replace(".",",",round((($sumar[0] + $sumar[1] + $sumar[2]) / 3),3));
			                 $magic_table .= '</td>';
						
                       }


		         }

	               /***************************
				    * VALIDAMOS HASTA LA HOJA *
				    ***************************/
				    
	                if($objPHPExcel->getActiveSheet()->getTitle()==$hoja_hasta) break;

		     }

            
           $magic_table .= '</tr>'; 
           
        }
                
        $magic_table .= '</table>';
        
        unset($_FILES);
        unset($_POST);   
        

        echo $magic_table;
        
        exit;
       
        /*RETORNO A PANTALLA*/
        return new ViewModel(array('magic_table' => $magic_table));
              
    }
     

}
