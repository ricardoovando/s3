<?php

namespace Avisos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/* ENTITYS */
use Avisos\Model\Entity\Aviso;
use Avisos\Model\Entity\DetalleServicioPrestado;

/*Entity*/
use Riat\Model\Dao\OrsRiatDao;
use Riat\Model\Entity\OrsRiat;


/* FORMULARIOS */
use Avisos\Form\BuscadorServicio;
use Avisos\Form\BuscadorServicioValidator;

/**/
use Avisos\Form\Buscador as BuscadorForm;
use Avisos\Form\BuscadorValidator;


class ValorizaController extends AbstractActionController {

    protected $login;
    private $avisoDao;
    private $config;
    private $ServicioDao;   
    private $DetalleServicioPrestadoDao;
    private $OrsRiatDao;
    private $num_form_riat = "";
        
    function __construct($config = null) {
        $this->config = $config;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setAvisoDao($avisoDao) {
        $this->avisoDao = $avisoDao;
    }
    
    public function getAvisoDao() {
        return $this->avisoDao;
    }
    
    public function setServicioDao($serviciodao) {
        $this->ServicioDao = $serviciodao;
    }

    public function getServicioDao() {
        return $this->ServicioDao;
    }
    
    public function setDetalleServicioPrestadoDao($detalleservicioprestadodao){
        $this->DetalleServicioPrestadoDao = $detalleservicioprestadodao;
    }
    
    public function getDetalleServicioPrestadoDao(){
        return $this->DetalleServicioPrestadoDao;
    }
    
    public function setOrsRiatDao($OrsRiatDao){
         $this->OrsRiatDao = $OrsRiatDao;
    }
    
    public function getOrsRiatDao(){
         return $this->OrsRiatDao;
    }
    
    private function getFormBuscadorServicio() {
        return new BuscadorServicio();
    }
    
    private function getFormBuscador() {
        return new BuscadorForm();
    }
    
    
    /* 
     * 
     *  Modulo para Valorizar el Aviso
     * 
     */

    public function indexAction() {
     
        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if ($this->getRequest()->isGet()) {
                    
            $idAviso = $this->params()->fromRoute('id');
            $aviso = $this->getAvisoDao()->obtenerPorId($idAviso);
            
        }else if($this->getRequest()->isPost()) {

            $numeroAviso = $this->request->getPost()->numeroaviso;
            $aviso = $this->getAvisoDao()->obtenerPorIdNumeroAviso($numeroAviso);
            $idAviso = $aviso->getId_aviso();
            
        }else{
            
           return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza'));
                   
        }
        
        
        if(empty($idAviso)){
               $idAviso = $this->getAvisoDao()->obtenerUltimoID();
            if(!empty($idAviso)){
               $idAviso = $idAviso->getId_aviso();
               $aviso = $this->getAvisoDao()->obtenerPorId($idAviso);
            }else{
               return new ViewModel(array('mensaje' => 'No hay avisos para valorizar'));
            }
          }

        
        /*
         * Servicios Suma Total Uf Servicios Cargados a Aviso
         */        
         $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($idAviso);

         $cantidadServicio = 0;
         $totalFacturacionUf = 0;
         
         foreach ($serviciosCargados as $servicio ) {
             $cantidadServicio =  $servicio->getPrecio_uf() * $servicio->getCantidad(); 
             $totalFacturacionUf = $totalFacturacionUf + $cantidadServicio;
         }
         
         if($totalFacturacionUf==0) $totalFacturacionUf = '0.00';

     /*
      * Servicios Cargados al Aviso a modo de Cobranza
      */  
      $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($idAviso);
         
         
      /*
       * Suma el Total de Servicios Cargados al Aviso
       */
       //$totalFacturacionUf = $this->getDetalleServicioPrestadoDao()->obtenerTotalFacturacionUf($idAviso);

        
       /* 
        * 
        * Consulto por Todos los Servicios que no estan cargado y que son de esta sucursal
        * 
        */
        $paginator = $this->getServicioDao()->obtenerServiciosNoCargados($idAviso);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));        
        $paginator->setItemCountPerPage(20);
        $formBuscador = $this->getFormBuscadorServicio();
        $formBuscadorAviso = $this->getFormBuscador();
        
        
    /*
     * 
     * Validacion de Imagenes Adjuntos en el Formulario
     * saver si tienen archivos adjuntos.
     * 
     */

        $tienefoto = 0;
        $url_imagen = '/';
        
        $tienefotosterreno = 0;
        $url_imagen_terreno = '/';
        
        
        /* Primero saber si existe el directorio */

        $directory = '/home/meditres/public_html/public/images/' . $aviso->getInstalacion()->getNum_instalacion() . '/' . $aviso->getNumero_aviso() .'/';
        
        if (is_dir($directory)) {

            /* Si existe busco la imagen relacionada al formulario */

            if ($aviso->getNumero_documento() != "") {

               /* Si existe imagen del formulario riat */
               if (file_exists( $directory .'riat_'. $aviso->getNumero_documento() .'.pdf' )){
                    $tienefoto = 1;
                    $url_imagen = '/images/'. $aviso->getInstalacion()->getNum_instalacion() . '/' . $aviso->getNumero_aviso() . '/riat_' . $aviso->getNumero_documento() .'.pdf'; 
                 }
               
               /* Si existen imagenes de trabajos en terreno */
               if (file_exists( $directory .'img_riat_' . $aviso->getNumero_aviso() . '_'  . $aviso->getNumero_documento() . '_0.jpg' )){
                    $tienefotosterreno = 1;          
                    $url_imagen_terreno = '/images/'. $aviso->getInstalacion()->getNum_instalacion() . '/' . $aviso->getNumero_aviso() . '/img_riat_' . $aviso->getNumero_aviso() . '_'  . $aviso->getNumero_documento() . '_0.jpg'; 
                 }
               
              }
            }
         
                
        return new ViewModel(array(
            'listadeservicios' => $paginator->getIterator(),
            'paginator' => $paginator,
            'servicioscargados' => $serviciosCargados,            
            'totalFacturacionUf' => number_format($totalFacturacionUf,2),
            'aviso' => $aviso->getNumero_aviso(),
            'descrip' => 'Descripción : '. ucfirst(strtolower($aviso->getDescripcion())),
            'instala' => $aviso->getInstalacion()->getNum_instalacion(),
            'tec' => 'Técnico : '. ucfirst(strtolower($aviso->getResponsable_persona()->getNombres())) .' '. ucfirst(strtolower($aviso->getResponsable_persona()->getApellidos())),
            'fechavalorizacion' => $aviso->getFecha_valorizacion(),
            'estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'fecha_ejecucion' => $aviso->getFecha_ejecucion(),
            'mes_facturacion' => $aviso->getMes_facturacion(),
            'numero_documento' => $aviso->getNumero_documento(),
            'id' => $idAviso,
            'formBuscador' => $formBuscador,
            'formBuscadorAviso' => $formBuscadorAviso,
            'tienefoto' => $tienefoto, 
            'url_imagen' => $url_imagen,
            'tienefotosterreno' => $tienefotosterreno,
            'url_imagen_terreno' => $url_imagen_terreno, 
          ));
        
        
    }
    
    
    
    public function detalleserviciosprestadosAction(){
        
        $idAviso = $this->params()->fromRoute('id');

        /*
         * Servicios Suma Total Uf Servicios Cargados a Aviso
         */        
         $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($idAviso);

         $cantidadServicio = 0;
         $totalFacturacionUf = 0;
         foreach ($serviciosCargados as $servicio ) {
             $cantidadServicio =  $servicio->getPrecio_uf() * $servicio->getCantidad(); 
             $totalFacturacionUf = $totalFacturacionUf + $cantidadServicio;
         }
         
                  if($totalFacturacionUf==0) $totalFacturacionUf = '0.00';
        
        
        /*
         * Servicios Cargados al Aviso a modo de Cobranza
         */  
         $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($idAviso);
         
             
         
         /*
          * 
          * Retornamos a la Vista en el formato deseado
          * 
          */
        
          $view = new ViewModel(array(
               'servicioscargados' => $serviciosCargados,            
               'totalFacturacionUf' => number_format($totalFacturacionUf,2),
          ));

          $view->setTerminal(true);

          return $view;
         
     }
    
    

     /*
     * 
     * Buscador por Nombre Instalacion 
     * 
     * 
     */

    public function filtrarserviciosAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza', 'action' => 'index', 'id' => $_POST['id']));
        }

        /*
         * 
         *  Obtenemos los parámetros del formulario es similar a $_POST
         */
        $postParams = $this->request->getPost();



        /*
         * 
         * Cargar Datos del Aviso
         * 
         */
        $aviso = $this->getAvisoDao()->obtenerPorId($_POST['id']);


        /*
         * Servicios Suma Total Uf Servicios Cargados a Aviso
         */        
         $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($_POST['id']);

         $cantidadServicio = 0;
         $totalFacturacionUf = 0;
         foreach ($serviciosCargados as $servicio ) {
             $cantidadServicio =  $servicio->getPrecio_uf() * $servicio->getCantidad(); 
             $totalFacturacionUf = $totalFacturacionUf + $cantidadServicio;
         }
         
         if($totalFacturacionUf==0) $totalFacturacionUf = '0.00';

         /*
         * Servicios Cargados al Aviso a modo de Cobranza
         */  
         $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($_POST['id']);

         $formBuscadorAviso = $this->getFormBuscador();

        /* LLamar a Formularios */
        $formBuscadorServicio = $this->getFormBuscadorServicio();
        $formBuscadorServicio->setInputFilter(new BuscadorServicioValidator());
        $formBuscadorServicio->setData($postParams);

        /*
         * Falla de la Validacion del form. 
         */
        $x = 0;
        if (!$formBuscadorServicio->isValid()) {

            //Falla la validación; volvemos a generar el formulario     
            $paginator = $this->getServicioDao()->obtenerServiciosNoCargados($_POST['id']);
            $paginator->setCurrentPageNumber(0);
            $paginator->setItemCountPerPage(20);
            $x = 1; 
        }


        if($x==0){
            
            $values = $formBuscadorServicio->getData();
            /* Reconfiguracion de Paginador */
            $paginator = $this->getServicioDao()->buscarPorNombreServiciosNoCargados($values['nombreservicio'],$_POST['id']);
            $paginator->setCurrentPageNumber(0);
            $paginator->setItemCountPerPage(20);
        
        }
        
        $viewModel = new ViewModel(array(
            'listadeservicios' => $paginator->getIterator(),
            'paginator' => $paginator,
            'servicioscargados' => $serviciosCargados,            
            'totalFacturacionUf' => number_format($totalFacturacionUf,2),
            'aviso' => $aviso->getNumero_aviso(),
            'descrip' => 'Descripción : '. ucfirst(strtolower($aviso->getDescripcion())),
            'instala' => 'Instalación : '. $aviso->getInstalacion()->getNum_instalacion(),
            'tec' => 'Técnico : '. ucfirst(strtolower($aviso->getResponsable_persona()->getNombres())) .' '. ucfirst(strtolower($aviso->getResponsable_persona()->getApellidos())),
            'fechavalorizacion' => $aviso->getFecha_valorizacion(),
            'estado_aviso' => $aviso->getEstados_avisos_id_estado_aviso(),
            'id' => $_POST['id'],
            'formBuscador' => $formBuscadorServicio,
            'formBuscadorAviso' => $formBuscadorAviso,
        ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("avisos/valoriza/index");

        return $viewModel;
        
    }
    
    
    
    
    /*
     * Inserto Servicio en el aviso para sumar cobro en uf
     */
     public function insertAction(){        

        /*
        * Obtenemos los parámetros del Ajax Jquery
        */  
         $postParams = $this->request->getPost();


        /* 
         * Solamente pueden existir los parametros permitidos 
         */
         foreach ($postParams as $key1 => $value1) {

            $bool = 0;


            if ($key1 == 'idserv') {

                $bool = 1;
                continue;
            }


            if ($key1 == 'cant') {

                $bool = 1;
                continue;
            }

            if ($key1 == 'valoruf') {

                $bool = 1;
                continue;
            }


            if ($key1 == 'idaviso') {

                $bool = 1;
                continue;
            }

            if ($bool == 0) {

                $view = new ViewModel(array(
                    'resposta' => 'Parametro no Identificado no Valido = ' . $value2 . '<>' . $key1,
                ));

                $view->setTerminal(true);

                return $view;
            }
        }
         
         
        /*
        * 
        * Solo se acepta que existan estas 4 variables
        * 
        */
         
        if (!isset($postParams["idserv"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe idserv',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["cant"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe cant',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["valoruf"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe valoruf',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["idaviso"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe idaviso',
            ));

            $view->setTerminal(true);

            return $view;
        };
              
        /*
         * 
         * Aplicar Validadores en los Campos Enviados por Ajax Jquery
         * 
         */
        $NotEmpty = new \Zend\Validator\NotEmpty(array('allowWhiteSpace' => false));    
        $Digits = new \Zend\Validator\Digits();
        $Float = new \Zend\I18n\Validator\Float();
        
        $result = '';

        
        /*
         * Aplicar Filtros a las Variables;
         */  
        $StripTags = new \Zend\Filter\StripTags();
        $StringTrim = new \Zend\Filter\StringTrim();
        $StripNewlines = new \Zend\Filter\StripNewlines();
        $HtmlEntities = new \Zend\Filter\HtmlEntities();
        $Int = new \Zend\Filter\Int();
        

        /*
         * Validar que los datos no sean NULL y filtrar los datos
         * 
         */
        foreach ( $postParams as $key => $value ) {
            
            if (!$NotEmpty->isValid($value)) {

                foreach ($NotEmpty->getMessages() as $message) {
                    $result = $result . "$message\n";
                }

                $view = new ViewModel(array(
                    'resposta' => $result.'valor : '.$value ,
                ));

                $view->setTerminal(true);

                return $view;
            }
         
            if( $key != "valoruf" ){     

                    $value = $StripTags->filter($value);
                    $value = $StringTrim->filter($value);
                    $value = $StripNewlines->filter($value);
                    $value = $HtmlEntities->filter($value);
                    $value = $Int->filter($value);

                    $postParams[$key] = $value;

              }

        }
        

        if (!$Float->isValid($postParams["valoruf"])) {

            foreach ($Float->getMessages() as $message) {

                $result = $result . "$message\n";
            }

            $view = new ViewModel(array(
                'resposta' => $result . 'valor : ' . $postParams["valoruf"],
            ));

            $view->setTerminal(true);

            return $view;
        }
        
        
        foreach ($postParams as $key => $value) {

            if ($key != "valoruf") {

                if (!$Digits->isValid($value)) {

                    foreach ($Digits->getMessages() as $message) {
                        $result = $result . "$message\n";
                    }

                    $view = new ViewModel(array(
                        'resposta' => $result . 'valor : ' . $value,
                    ));

                    $view->setTerminal(true);

                    return $view;
                }

                $postParams[$key] = $value;
            }
        }


        /*
         * 
         * Insertamos los datos en la tabla correspondiente 
         * 
         */     
        $detalleServicioPrestado = new DetalleServicioPrestado();
        $detalleServicioPrestado->setAvisos_id_aviso($postParams["idaviso"]);
        $detalleServicioPrestado->setServicios_id_servicio($postParams["idserv"]);
        $detalleServicioPrestado->setPrecio_uf($postParams["valoruf"]);
        $detalleServicioPrestado->setCantidad($postParams["cant"]);
                
        $this->getDetalleServicioPrestadoDao()->insertServicioAviso($detalleServicioPrestado);

            
          /*
           * Retornamos a la Vista con un OK
           */
        
          $view = new ViewModel(array(
              'resposta' => 'OK',
          ));

          $view->setTerminal(true);

          return $view;

     }
     
     
     /*
     * Elimino Servicio en el aviso para restart cobro en uf
     */
     public function deleteAction(){
         
         
        /*
         * Obtenemos los parámetros del Ajax Jquery
         */
        $postParams = $this->request->getPost();


        /*
         * 
         * Solamente pueden existir los parametros permitidos
         * 
         */
        foreach ($postParams as $key1 => $value1) {

            $bool = 0;


            if ($key1 == 'idserv') {

                $bool = 1;
                continue;
            }


            if ($key1 == 'cant') {

                $bool = 1;
                continue;
            }

            if ($key1 == 'valoruf') {

                $bool = 1;
                continue;
            }


            if ($key1 == 'idaviso') {

                $bool = 1;
                continue;
            }

            if ($bool == 0) {

                $view = new ViewModel(array(
                    'resposta' => 'Parametro no Identificado no Valido = ' . $value2 . '<>' . $key1,
                ));

                $view->setTerminal(true);

                return $view;
            }
        }


        /*
         * 
         * Solo se acepta que existan estas 4 variables
         * 
         */

        if (!isset($postParams["idserv"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe idserv',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["cant"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe cant',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["valoruf"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe valoruf',
            ));

            $view->setTerminal(true);

            return $view;
        };

        if (!isset($postParams["idaviso"])) {

            $view = new ViewModel(array(
                'resposta' => 'No Existe idaviso',
            ));

            $view->setTerminal(true);

            return $view;
        };

        /*
         * 
         * Aplicar Validadores en los Campos Enviados por Ajax Jquery
         * 
         */
        $NotEmpty = new \Zend\Validator\NotEmpty(array('allowWhiteSpace' => false));
        $Digits = new \Zend\Validator\Digits();
        $Float = new \Zend\I18n\Validator\Float();

        $result = '';


        /*
         * Aplicar Filtros a las Variables;
         */
        $StripTags = new \Zend\Filter\StripTags();
        $StringTrim = new \Zend\Filter\StringTrim();
        $StripNewlines = new \Zend\Filter\StripNewlines();
        $HtmlEntities = new \Zend\Filter\HtmlEntities();
        $Int = new \Zend\Filter\Int();


        /*
         * Validar que los datos no sean NULL y filtrar los datos
         * 
         */
        foreach ($postParams as $key => $value) {

            if (!$NotEmpty->isValid($value)) {

                foreach ($NotEmpty->getMessages() as $message) {
                    $result = $result . "$message\n";
                }

                $view = new ViewModel(array(
                    'resposta' => $result . 'valor : ' . $value,
                ));

                $view->setTerminal(true);

                return $view;
            }

            if ($key != "valoruf") {

                $value = $StripTags->filter($value);
                $value = $StringTrim->filter($value);
                $value = $StripNewlines->filter($value);
                $value = $HtmlEntities->filter($value);
                $value = $Int->filter($value);

                $postParams[$key] = $value;
            }
        }


        if (!$Float->isValid($postParams["valoruf"])) {

            foreach ($Float->getMessages() as $message) {

                $result = $result . "$message\n";
            }

            $view = new ViewModel(array(
                'resposta' => $result . 'valor : ' . $postParams["valoruf"],
            ));

            $view->setTerminal(true);

            return $view;
        }


        foreach ($postParams as $key => $value) {

            if ($key != "valoruf") {

                if (!$Digits->isValid($value)) {

                    foreach ($Digits->getMessages() as $message) {
                        $result = $result . "$message\n";
                    }

                    $view = new ViewModel(array(
                        'resposta' => $result . 'valor : ' . $value,
                    ));

                    $view->setTerminal(true);

                    return $view;
                }

                $postParams[$key] = $value;
            }
        }


        /*
         * 
         * Insertamos los datos en la tabla correspondiente 
         * 
         */     
        $detalleServicioPrestado = new DetalleServicioPrestado();
        $detalleServicioPrestado->setAvisos_id_aviso($postParams["idaviso"]);
        $detalleServicioPrestado->setServicios_id_servicio($postParams["idserv"]);
        $detalleServicioPrestado->setPrecio_uf($postParams["valoruf"]);
        $detalleServicioPrestado->setCantidad($postParams["cant"]);
                
        $this->getDetalleServicioPrestadoDao()->deleteServicioAviso($detalleServicioPrestado);

            
          /*
           * Retornamos a la Vista con un OK
           */
        
          $view = new ViewModel(array(
              'resposta' => 'OK',
          ));

          $view->setTerminal(true);

          return $view;
          
    }
     
     
     
    public function limpiarVariables($entrada) {
        if (is_array($entrada)) {
            foreach ($entrada as $var => $val) {
                $output[$var] = $this->limpiarVariables($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $entrada = stripslashes($entrada);
            }
            $salida = $this->removerTags($entrada);
        }
        return $salida;
    }

    public function removerTags($entrada) {

        $busqueda = array(
            '@<script[^>]*?>.*?</script>@si', // javascript
            '@<[\/\!]*?[^<>]*?>@si', // HTML
            '@<style[^>]*?>.*?</style>@siU', // Css
            '@<![\s\S]*?--[ \t\n\r]*>@' // Comentarios multiples
        );

        $salida = preg_replace($busqueda, '', $entrada);
        return $salida;
    }

    public function soloLetrasNumeros($cadena) {

        return preg_replace("[^A-Za-z0-9]", "", $cadena);
    }
    
    
    
    /*
     * 
     *  Confirma su Valorizacion para actualizar el estado del aviso y asignar una orden de digitacion
     * 
     */
    
     public function valorizarAction(){
         
        $id_aviso = (int) $this->params()->fromRoute('id', 0);
        
        if (!$id_aviso) {
            return $this->redirect()->toRoute('avisos', array('controller' => 'index'));
          }
        
      /*
       * 
       * Si existe una valorizacion correcta...
       * podremos enviar a cargar las ordenes de digitacion para los servicios 
       * que requieran digitacion
       * 
       * el aviso tiene que tener servicios cargados para facturar
       * 
       */         
       
      /* 
       * Estos son los avisos que tiene 
       * cargados el sistema hoy que 
       * comprobar que los servicios tengan una orden de 
       * registro si no tienen crearla
       * 
       */
        
       $serviciosCargados = $this->getDetalleServicioPrestadoDao()->obtenerPorIdaviso($id_aviso);
       
       
       /*
        * Si es igual a 0 entra a la condicion            
        */
       
        if($serviciosCargados->count() > 0){

            /* Hay servicios cargados ahora hay que crear ordenes de registro de servicios para las que correspondan */
            
            foreach ($serviciosCargados as $servicio ) {

                
              /*
               * Si el servicio es Auditoria a Esquema de Medida inserto 
               * una orden de registro de servicio para esta sucursal para este servicio
               */
                if ($servicio->getServicios_id_servicio() == 8) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 9) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 10) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 11) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 12) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 13) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 14) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 51) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 27) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 37) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }


             if ($servicio->getServicios_id_servicio() == 35) {
            
                                $datos = array();
            
                                $datos['estados_ors_id_estado_ors'] = 1;
                                $datos['avisos_id_aviso'] = $id_aviso;
            
                                $ors_riat = new OrsRiat;
                                $ors_riat->exchangeArray($datos);
            
                                $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);
            
                                break;
                            }


                if ($servicio->getServicios_id_servicio() == 47) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 90) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 91) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 92) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 102) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 104) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 108) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 109) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }


                if ($servicio->getServicios_id_servicio() == 110) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 111) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 112) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 113) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 114) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 115) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }


                if ($servicio->getServicios_id_servicio() == 116) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 117) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }


                if ($servicio->getServicios_id_servicio() == 118) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 119) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 120) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }


                if ($servicio->getServicios_id_servicio() == 121) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

                if ($servicio->getServicios_id_servicio() == 122) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 133) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 134) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 136) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 137) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 138) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 140) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 143) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 144) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 151) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 152) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 153) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 154) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);
 
                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 157) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 158) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 159) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 160) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 160) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                
                if ($servicio->getServicios_id_servicio() == 162) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 164) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 165) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 166) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 167) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }
                
                if ($servicio->getServicios_id_servicio() == 163) {

                    $datos = array();

                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;
                }

               /*Solo los Codigos del Norte*/
               $codigosNorte = array(219,220,221,222,223,224,225,226,227,228);

                if (in_array($servicio->getServicios_id_servicio(), $codigosNorte)) {

                    $datos = array();
                    $datos['estados_ors_id_estado_ors'] = 1;
                    $datos['avisos_id_aviso'] = $id_aviso;

                    $ors_riat = new OrsRiat;
                    $ors_riat->exchangeArray($datos);

                    $this->getOrsRiatDao()->insertNuevoOrsRiat($ors_riat);

                    break;

                 }

                /*
                 * FIN 
                 */
            }
            
            
                   /******
                    * 
                    * Confirmar Valoriza
                    * se actuliza el registro
                    * del aviso
                    * 
                    ********/

                     $datos = array();

                        $datos['id_aviso'] = $id_aviso;
                        $datos['estados_avisos_id_estado_aviso'] = 3;
                        $fecha = new \DateTime();
                        $datos['fecha_valorizacion'] = $fecha->format('d-m-Y');

                        $aviso = new Aviso();
                        $aviso->exchangeArray($datos);

                        $this->getAvisoDao()->updateConfirmaValoriza($aviso);

                     /**************************************/
          

        }else{
            
            
            return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza','action'=>'index','id'=> $id_aviso ));
            
            
        }

        
          return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza','action'=>'index','id'=>$id_aviso ));  
          
           
     }
    
       
       
       
     /*
      * Se activo la funcion para cargar fotos desde el modulo para valorizar asi 
      * el tecnico de terreno podra realizar las dos actividades 
      * valorizar y adjuntar archivos como las fotos el riat pdf otros
      */  
       
     public function cargarArchivosAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza'));
           }

            /* Parametros Post desde el formulario VALORIZA */
             $postParams = $this->request->getPost();

            /* Array con todos los FILES ELEMENTS */
             $fileParams = $this->params()->fromFiles();
            
            
            /*
             * Variable GLobal con numero del RIAT
             * HAY una orden de digitacion pendiente.
             * 
             */
            $this->num_form_riat = $postParams["num_form_riat"];
        
            /*
             * 
             * Todos los Avisos tienen que tener un directorio para cargar sus archivos adjuntos  
             * 
             */
             $filename = '/home/meditres/public_html/public/images/'. $postParams['num_instalacion'];

             if (!is_dir($filename)) {

                 mkdir($filename, 0777);

                 $filename = $filename . '/' . $postParams['numero_aviso'];

                   if (!is_dir($filename)) {

                       mkdir($filename, 0777);

                   }    

               }else{

                  $filename = $filename . '/' . $postParams['numero_aviso'];

                   if (!is_dir($filename)) {

                       mkdir($filename, 0777);

                   } 

               }

        
            /*
             * 
             * funcion que ejecuta validaciones a las imagenes 
             * y carga en carpeta del aviso todas las imagenes.
             * 
             */
            
          
          /* EL mismo formulario riat se escanea y se adjunta en este formulario que son los mismos datos */
          if (!empty($fileParams["img_form_riat"]["name"])) {
              
              $nombre_imagen = 'riat' . '_' . $postParams["num_form_riat"];
              
              /*
               * Nombre del Element en el Form html
               * Array con datos de la Imagen
               * Nombre con el que se Guardara la Imagen 
               * Directorio o nombre de carpeta donde se guargara la imagen
               * 
               */
                
               $this->cargaImgRiatAction( 'img_form_riat', $fileParams["img_form_riat"], $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso'] );
                        
            }
            
          if (!empty($fileParams["img_riat"])) {

                  /* Arreglo de Imagenes que son las evidencias del trabajo realizado (Auditoria Trifasica) */
                  foreach ($fileParams["img_riat"] as $key => $value) {

                      if (!empty($value["name"])) {

                           $nombre_imagen = 'img_riat' . '_' . $postParams['numero_aviso'] . '_' . $postParams["num_form_riat"] . '_' . $key;

                          /*
                           * Nombre del Element en el Form html
                           * Array con datos de la Imagen
                           * Nombre con el que se Guardara la Imagen 
                           * Directorio o nombre de carpeta donde se guargara la imagen
                           * 
                           */

                           $this->cargaFotosRiatAction( 'img_riat_'.$key.'_' , $value, $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso'] );

                      }

                  }

               }
               

            /******
             * 
             * Con los datos del formulario 
             * completamos el registro del aviso
             * para poder trabajarlo sin mas detalles de digitacion 
             * solo las auditorias tendran ordenes de digitacion
             * los demas trabajos podran ser procesados solo desde este
             * modulo de valorizacion.
             * 
             ********/

             $datos = array();

             $datos['id_aviso'] = $postParams['id_aviso'];
             $datos['mes_facturacion'] = $postParams['mes_facturacion'];
             $datos['fecha_ejecucion'] = $postParams['fecha_ejecucion'];
             $datos['numero_documento'] = $postParams['num_form_riat'];
             
             $aviso = new Aviso();
             $aviso->exchangeArray($datos);
             
             $this->getAvisoDao()->updateDatosServicioPrestadoEnTerreno($aviso);
             
               
         return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza','action'=>'index','id'=>$postParams['id_aviso']));  

        }  
        
    
        
        public function eliminarImgFormAction() { 
            
           $this->layout()->usuario = $this->getLogin()->getIdentity();

           if (!$this->getRequest()->isPost()) {
                 
               return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza'));
               
             }
                        
            /*Parametros Post desde el formulario VALORIZA*/
            $postParams = $this->request->getPost();
                        
            $file = '/home/meditres/public_html/public' . $postParams['url'];
                                
            unlink($file);
                                                
            return $this->redirect()->toRoute('avisos', array('controller' => 'valoriza','action'=>'index','id'=>$postParams['id_aviso']));  

         }
        
        
        
        
        
   
           /*
    * 
    * Funcion para cargar la imagen del riat
    * esto es un pdf con el documento de terreno como imagen
    * 
    * 
    */
    
    private function cargaImgRiatAction($nombreElementFile, $arrayElementFile, $NombreImagen, $Dir) {

        $nombresImagenes = (String) "";

        /*
         * se ejecuta el ciclo segun la cantidad de elementos type file 
         */

        if ($arrayElementFile['error'] == 0) {

            $cargado = false;

            $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 10000000));
            $validatorExt = new \Zend\Validator\File\Extension('pdf');
            $validatorMime = new \Zend\Validator\File\MimeType('application/pdf');
            //$validatorIsImage = new \Zend\Validator\File\IsImage();
            //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));

            $adapter = new \Zend\File\Transfer\Adapter\Http();

            $results = array();
            $results['size'] = $validatorSize->isValid($arrayElementFile['name']);
            $results['ext'] = $validatorExt->isValid($arrayElementFile['name']);
            $results['mime'] = $validatorMime->isValid($arrayElementFile['name']);
            //$results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
            //$results['Count'] = $validatorCount->isValid($fileParams['name']);

            $adapter->setValidators(array(
                $validatorSize,
                $validatorExt,
                $validatorMime,
                    ), $arrayElementFile['name']);

            //\Zend\Debug\Debug::dump($results);


            /* Si existe error en la validacion se retorna al formulario con los msg de error */
            if (!$adapter->isValid($nombreElementFile)) {


                $dataError = $adapter->getMessages();
                $error = array();

                foreach ($dataError as $key => $row) {
                    $error[] = $row;
                }

                echo 'el archivo que adjunto no es correcto por favor volver atras e intentar nuevamente <br>';
                var_dump($error);

                exit;
                
            } else {


                /* Directorio donde se cargan las fotos dentro del modulo */
                ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');

                $adapter->setDestination('/home/meditres/public_html/public/images/' . $Dir . '/');


                /*
                 * extencion del archivo
                 */

                $extension = pathinfo($arrayElementFile['name'], PATHINFO_EXTENSION);


                /*
                 * se modifica el nombre del archivo se anade un calculo aleatorio
                 */

                ///var/www/html/sam/public/images/fotos/

                $adapter->addFilter('Rename', array(
                    'target' => '/home/meditres/public_html/public/images/' . $Dir . '/' . $NombreImagen . '.' . strtolower($extension),
                    'overwrite' => true,
                ));

                /*
                 * se cargar el archivo en forma exitosa
                 */

                if ($adapter->receive($arrayElementFile['name'])) {

                    //$nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                    //$nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                }
            }
        }

        /*
         *  si el archivo no llega al servidor se envia msg de error
         */

        if (!$arrayElementFile['error'] == 0) {

            $error = array('Error del Archivo Cargado Codigo Error : ' . $arrayElementFile['error']);

            echo 'EL archivo cargado tiene errores volver atras <br>';         
            var_dump($error);
            exit;
            
        }


        unset($adapter);

        /* Se retorna un arregle con los nombres de las imagenes */
        //echo 'ok';
        
    }
   
    
    
    
   /*
    * 
    * Funcion para cargar las fotos de la Auditoria
    * 
    * 
    */
    
    private function cargaFotosRiatAction($nombreElementFile, $arrayElementFile, $NombreImagen, $Dir) {

        $nombresImagenes = (String) "";

        /*
         * se ejecuta el ciclo segun la cantidad de elementos type file 
         */

        if ($arrayElementFile['error'] == 0) {

            $cargado = false;

            $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 10000000));
            $validatorExt = new \Zend\Validator\File\Extension('jpg,jpeg');
            $validatorMime = new \Zend\Validator\File\MimeType('image/jpg,image/jpeg');
            $validatorIsImage = new \Zend\Validator\File\IsImage();
            //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));

            $adapter = new \Zend\File\Transfer\Adapter\Http();

            $results = array();
            $results['size'] = $validatorSize->isValid($arrayElementFile['name']);
            $results['ext'] = $validatorExt->isValid($arrayElementFile['name']);
            $results['mime'] = $validatorMime->isValid($arrayElementFile['name']);
            $results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
            //$results['Count'] = $validatorCount->isValid($fileParams['name']);

            $adapter->setValidators( array(
                                           $validatorSize,
                                           $validatorExt,
                                           $validatorMime,
                                           $validatorIsImage,
                                          ), $arrayElementFile['name'] );

            //\Zend\Debug\Debug::dump($results);


            /* Si existe error en la validacion se retorna al formulario con los msg de error */
            if (!$adapter->isValid($nombreElementFile)) {


                $dataError = $adapter->getMessages();
                $error = array();

                foreach ($dataError as $key => $row) {
                    $error[] = $row;
                }

                echo 'la imagen que adjunto no esta correcta por favor volver atras e intentar nuevamente <br>';
                var_dump($error);
                
            } else {


                /* Directorio donde se cargan las fotos dentro del modulo */
                ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');

                $adapter->setDestination('/home/meditres/public_html/public/images/' . $Dir . '/');


                /*
                 * extencion del archivo
                 */

                $extension = pathinfo($arrayElementFile['name'], PATHINFO_EXTENSION);


                /*
                 * se modifica el nombre del archivo se anade un calculo aleatorio
                 */

                ///var/www/html/sam/public/images/fotos/

                $adapter->addFilter('Rename', array(
                    'target' => '/home/meditres/public_html/public/images/' . $Dir . '/' . $NombreImagen . '.' . strtolower($extension),
                    'overwrite' => true,
                ));

                /*
                 * se cargar el archivo en forma exitosa
                 */

                if ($adapter->receive($arrayElementFile['name'])) {

                    //$nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                    //$nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                }
            }
        }

        /*
         *  si el archivo no llega al servidor se envia msg de error
         */

        if (!$arrayElementFile['error'] == 0) {

            $error = array('Error del Archivo Cargado Codigo Error : ' . $arrayElementFile['error']);

            echo 'EL archivo cargado tiene errores volver atras <br>';
            var_dump($error);
            exit;
            
        }


        unset($adapter);

        /* Se retorna un arregle con los nombres de las imagenes */
        //echo 'ok';
        
    }


}