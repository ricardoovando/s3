<?php

namespace Avisos\Form;

use Zend\Form\Form;

class Buscador extends Form {

    public function __construct($name = null) {
        parent::__construct('buscador');

        $this->setAttribute('method', 'post');


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroaviso',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Buscar N° Aviso',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn btn-primary',
                //'style' => 'height:30px;',
            ),
        ));
        
    }

}

