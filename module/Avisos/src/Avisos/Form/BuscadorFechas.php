<?php

namespace Avisos\Form;

use Zend\Form\Form;

class BuscadorFechas extends Form {

    public function __construct($name = null) {
        parent::__construct('buscadorfechas');
        $this->setAttribute('method', 'post');


        $this->add(array(
             'type' => 'Zend\Form\Element\Text',
             'name' => 'fecha_buscar_1',
             'id' => 'fecha_buscar_1',
             'attributes' => array(
                 'style' => 'width:100px;margin-bottom:3px;',
                 'placeholder' => 'Fecha Inicio',
                )
             ));
         
        $this->add(array(
             'type' => 'Zend\Form\Element\Text',
             'name' => 'fecha_buscar_2',
             'id' => 'fecha_buscar_2',
             'attributes' => array(
                 'style' => 'width:100px;margin-bottom:3px;',
                 'placeholder' => 'Fecha Final',
                )
            ));
        
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'orden_registro',
            'options' => array(
                'label' => '',
                'empty_option' => 'Orden registro',
            ),
            'attributes' => array(
                'style' => 'margin-bottom:3px;',
                'options' => array(
                    
                    1 => 'Aviso ASC',
                    2 => 'Aviso DESC',
                    
                    3 => 'Instalación ASC',
                    4 => 'Instalación DESC',
                    
                    5 => 'IdServicio ASC',
                    6 => 'IdServicio DESC',
                    
                    7 => 'Comuna ASC',
                    8 => 'Comuna DESC',
                    
                    9 => 'RLectura ASC', 	
                    10 => 'RLectura DESC', 	
                    
                    11 => 'CEmpla ASC',
                    12 => 'CEmpla DESC',
                    
                    13 => 'Porcion ASC',
                    14 => 'Porcion DESC',
                    
                  ),
                ),
             ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'buscar_tipo_fecha',
            'options' => array(
                'label' => '',
                'empty_option' => 'Buscar Tipo Fecha',
            ),
            'attributes' => array(
                'style' => 'margin-bottom:3px;',
                'options' => array(
                    1 => 'FechaCarga',
                    2 => 'FechaAsignación',
                    3 => 'FechaValorización',
                    4 => 'FechaFinalización',
                  ),
                ),
             ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'buscar_estado_avisos',
            'options' => array(
                'label' => '',
                'empty_option' => 'Buscar por estado avisos',
            ),
            'attributes' => array(
                'style' => 'margin-bottom:3px;',
                'options' => array(
                    1 => 'Pendiente',
                    2 => 'Asignado',
                    3 => 'Valorizado',
                    4 => 'Finalizado',
                    5 => 'Suspendido',
                    6 => 'Todos',
                   ),
                 ),
              ));
        
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'buscar_por_numero',
            'options' => array(
                'label' => '',
                'empty_option' => 'Buscar por Campo',
            ),
            'attributes' => array(
                'style' => 'margin-bottom:3px;',
                'options' => array(
                    1 => 'Instalación',
                    2 => 'Ruta Lectura',
                    3 => 'ID Servicio',
                    4 => 'Emplazamiento',
                    5 => 'Porcion'
                  ),
                 ),
              ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroidabuscar',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Ingresar N°',
            ),
        ));
        
         
        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'buscar',
            'attributes' => array(  
                'type' => 'button',  
                'value' => 'Exportar a Excel',
                'class' => 'btn btn-success',
               'style' => '',
            ),
         ));
        
    }

}

