<?php

namespace Avisos\Form;

use Zend\Form\Form;

class BuscadorIdServicio extends Form {

    public function __construct($name = null) {
        parent::__construct('buscadorIdServicio');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroidservicio',
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Buscar Id Servicio',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));
        
    }

}

