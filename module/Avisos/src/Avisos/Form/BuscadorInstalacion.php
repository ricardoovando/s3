<?php

namespace Avisos\Form;

use Zend\Form\Form;

class BuscadorInstalacion extends Form {

    public function __construct($name = null) {
        parent::__construct('buscadorinstalacion');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroinstalacion',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Buscar N° Instalación',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));
        
    }

}

