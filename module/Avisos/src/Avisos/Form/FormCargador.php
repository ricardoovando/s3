<?php

namespace Avisos\Form;

use Zend\Form\Form;

class FormCargador extends Form {

    public function __construct($name = null) {
        parent::__construct('FormCargador');

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
             
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sucursal',
            'options' => array(
                'label' => '',
                'empty_option' => 'Sucursales',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'style' => 'width:350px;',
                'id' => 'sucursal',
                'onchange' => 'javascript:cargarSelectDistribuidoras()',
            ),
        ));
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'distribuidoras',
            'options' => array(
                'label' => '',
                'empty_option' => 'Distribuidoras',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'style' => 'width:350px;',
                'id' => 'distribuidoras',
                'onchange' => 'javascript:cargarSelectSolicitantes()',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'solicitantes',
            'options' => array(
                'label' => '',
                'empty_option' => 'Solicitantes',
                'disable_inarray_validator' => true,
            ),
            'attributes' => array(
                'style' => 'width:350px;',
                'id' => 'solicitantes',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'file',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:10px;',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cargar Avisos',
                'class' => 'btn btn-danger',
                'style' => 'height:300px;',
                'style' => 'margin:15px;',
            ),
        ));

    }

}

