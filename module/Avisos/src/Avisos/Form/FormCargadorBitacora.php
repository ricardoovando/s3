<?php

namespace Avisos\Form;

use Zend\Form\Form;

class FormCargadorBitacora extends Form {

    public function __construct($name = null) {
        
        parent::__construct('FormCargadorBitacora');

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'file',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:10px;',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cargar Avisos',
                'class' => 'btn btn-warning',
                'style' => 'height:30px;',
                'style' => 'margin:15px;',
            ),
        ));

    }

}

