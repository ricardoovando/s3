<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectPersonalTecnico extends Form {

    public function __construct($name = null) {
        parent::__construct('selectPersonalTecnico');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico',
            'options' => array(
                'label' => '',  
            ),
            'attributes' => array(
                'style' => 'margin:auto;width:300px;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Asignar',
                'class' => 'btn btn-warning',
                'style' => 'height:30px;',
                'onclick' => 'asignar_avisos()',
            ),
        ));
        
       $this->add(array(
            'name' => 'btn_buscartecnico',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Buscar Tecnico',
                'class' => 'btn btn-info',
                'style' => 'height:30px;',
                'onclick' => 'buscartecnico()',
            ),
        )); 
       
       $this->add(array(
            'name' => 'informe_asignacion',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Informe Asignación',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));

    }

}

