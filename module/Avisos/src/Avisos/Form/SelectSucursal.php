<?php

namespace Avisos\Form;

use Zend\Form\Form;

class SelectSucursal extends Form {

    public function __construct($name = null) {
        parent::__construct('selectSucursal');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sucursal',
            'options' => array(
                'label' => '',
                'empty_option' => 'Buscar por Sucursal',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));

    }

}

