<?php

namespace Avisos\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* Entity */
use Avisos\Model\Entity\Aviso;

class AvisoDao {

	protected $tableGateway;
	protected $login;
	protected $dbAdapter;

	public function __construct(TableGateway $tableGateway, $login, Adapter $dbAdapter) {
		$this -> tableGateway = $tableGateway;
		$this -> login = $login -> getIdentity();
		$this -> dbAdapter = $dbAdapter;
	}

	public function obtenerTodos($datosFiltran) {

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('ser' => 'servicios'), 'ser.id_servicio = avisos.servicios_id_servicio', array('*'));
		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));
		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');
		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
		$select -> join(array('suc' => 'sucursales'), 'suc.id_sucursal = avisos.sucursales_id_sucursal', array('nombre_sucursal' => 'nombre_sucursal'));

		if ((!empty($datosFiltran["buscar_estado_avisos"]))) {

			if ($datosFiltran["buscar_estado_avisos"] != 6)
				$select -> where(array("avisos.estados_avisos_id_estado_aviso" => (int)$datosFiltran["buscar_estado_avisos"]));

		} else {

			if (empty($datosFiltran["numeroaviso"])) {
				$select -> where(array('avisos.estados_avisos_id_estado_aviso < 3'));
			}

		}

		if ((!empty($datosFiltran["poblacion"]))) {

			$select -> where(array('inst.nom_poblacion' => (String)$datosFiltran["poblacion"]));

		}

		if ((!empty($datosFiltran["persona_tecnico"]))) {

			$select -> where(array('avisos.responsable_personal_id_persona' => (Int)$datosFiltran["persona_tecnico"]));

		}

		if ((!empty($datosFiltran["sucursal"]))) {

			$select -> where(array('suc.id_sucursal' => (String)$datosFiltran["sucursal"]));

		}

		if ((!empty($datosFiltran["prioridad"]))) {

			$select -> where(array('avisos.prioridad' => (String)$datosFiltran["prioridad"]));

		}

		if ((!empty($datosFiltran['fecha_buscar_1'])) && (!empty($datosFiltran['fecha_buscar_2']))) {

			if ($datosFiltran['buscar_tipo_fecha'] == 1)
				$tipodefechabuscar = 'avisos.fecha_carga';
			if ($datosFiltran['buscar_tipo_fecha'] == 2)
				$tipodefechabuscar = 'avisos.fecha_asignacion';
			if ($datosFiltran['buscar_tipo_fecha'] == 3)
				$tipodefechabuscar = 'avisos.fecha_valorizacion';
			if ($datosFiltran['buscar_tipo_fecha'] == 4)
				$tipodefechabuscar = 'avisos.fecha_finalizacion';
			$select -> where -> between($tipodefechabuscar, $datosFiltran['fecha_buscar_1'], $datosFiltran['fecha_buscar_2']);

		} else {

			if (empty($datosFiltran["numeroaviso"])) {

				if ((empty($datosFiltran["persona_tecnico"]))) {

					if ((empty($datosFiltran["buscar_por_numero"])) && (empty($datosFiltran["numeroidabuscar"]))) {

						$select -> where -> addPredicate(new \Zend\Db\Sql\Predicate\Expression("(EXTRACT(MONTH FROM avisos.fecha_carga)) = (EXTRACT(MONTH FROM CURRENT_DATE))"));

					}

				}

			}

		}

		if ((!empty($datosFiltran["buscar_por_numero"])) && (!empty($datosFiltran["numeroidabuscar"]))) {

			if ($datosFiltran["buscar_por_numero"] == 1)
				$select -> where(array('inst.num_instalacion' => (String)$datosFiltran["numeroidabuscar"]));
			if ($datosFiltran["buscar_por_numero"] == 2)
				$select -> where(array('inst.unidad_lectura' => (String)$datosFiltran["numeroidabuscar"]));
			if ($datosFiltran["buscar_por_numero"] == 3)
				$select -> where(array('avisos.servicios_id_servicio' => (Int)$datosFiltran["numeroidabuscar"]));
			if ($datosFiltran["buscar_por_numero"] == 4)
				$select -> where(array('inst.ce_emplazamiento' => (String)$datosFiltran["numeroidabuscar"]));
			if ($datosFiltran["buscar_por_numero"] == 5)
				$select -> where(array('inst.porcion' => (String)$datosFiltran["numeroidabuscar"]));

		}

		if ((!empty($datosFiltran["numeroaviso"]))) {

			$select -> where -> like('numero_aviso', '%' . $datosFiltran["numeroaviso"] . '%');
		}

		/*
		 * Ordenando los registro
		 */

		if ((!empty($datosFiltran["ordenarRegistro"]))) {

			if ($datosFiltran["ordenarRegistro"] == 1)
				$select -> order('avisos.numero_aviso ASC');

			if ($datosFiltran["ordenarRegistro"] == 2)
				$select -> order('avisos.numero_aviso DESC');

			if ($datosFiltran["ordenarRegistro"] == 3)
				$select -> order('inst.num_instalacion ASC');

			if ($datosFiltran["ordenarRegistro"] == 4)
				$select -> order('inst.num_instalacion DESC');

			if ($datosFiltran["ordenarRegistro"] == 5)
				$select -> order('avisos.servicios_id_servicio ASC');

			if ($datosFiltran["ordenarRegistro"] == 6)
				$select -> order('avisos.servicios_id_servicio DESC');

			if ($datosFiltran["ordenarRegistro"] == 7)
				$select -> order('inst.nom_poblacion ASC');

			if ($datosFiltran["ordenarRegistro"] == 8)
				$select -> order('inst.nom_poblacion DESC');

			if ($datosFiltran["ordenarRegistro"] == 9)
				$select -> order('inst.unidad_lectura ASC');

			if ($datosFiltran["ordenarRegistro"] == 10)
				$select -> order('inst.unidad_lectura DESC');

			if ($datosFiltran["ordenarRegistro"] == 11)
				$select -> order('inst.ce_emplazamiento ASC');

			if ($datosFiltran["ordenarRegistro"] == 12)
				$select -> order('inst.ce_emplazamiento DESC');

			if ($datosFiltran["ordenarRegistro"] == 13)
				$select -> order('inst.porcion ASC');

			if ($datosFiltran["ordenarRegistro"] == 14)
				$select -> order('inst.porcion DESC');

		} else {

			$select -> order('avisos.estados_avisos_id_estado_aviso ASC');

		}

		/*echo '<pre>';
		 echo $select->getSqlString();
		 exit;*/

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;

	}

	public function obtenerUltimoID() {

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
		$select -> where(array('avisos.estados_avisos_id_estado_aviso <> 4'));
		$select -> order('avisos.id_aviso DESC');
		$select -> limit('1');

		$rowset = $this -> tableGateway -> selectWith($select);
		$row = $rowset -> current();
		return $row;

	}

	public function obtenerTodosAuditor($datosFiltran) {

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'));

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> join(array('suc' => 'sucursales'), 'suc.id_sucursal = avisos.sucursales_id_sucursal', array('nombre_sucursal' => 'nombre_sucursal'));

        //$select -> join(array('rat' => 'resultado_aviso_trifasico'), 'rat.id_aviso = avisos.id_aviso', array('*') ,'left');


		if (!empty($datosFiltran)) {

			if ((!empty($datosFiltran["poblacion"])))
				$select -> where -> like('inst.nom_poblacion', '%' . $datosFiltran["poblacion"] . '%');

			if ((!empty($datosFiltran["sucursal"])))
				$select -> where(array('avisos.sucursales_id_sucursal' => (int)$datosFiltran["sucursal"]));

			if ((!empty($datosFiltran["estado_aviso"])))
				$select -> where(array('est.id_estado_aviso' => (int)$datosFiltran["estado_aviso"]));

			if ((!empty($datosFiltran["persona_tecnico"])))
				$select -> where(array('avisos.responsable_personal_id_persona' => (int)$datosFiltran["persona_tecnico"]));

			if ((!empty($datosFiltran["estado_riat"])))
				$select -> where(array('od.estado_riat' => (int)$datosFiltran["estado_riat"]));

			if ((!empty($datosFiltran["meses_anio"])))
				$select -> where -> addPredicate(new \Zend\Db\Sql\Predicate\Expression("EXTRACT( MONTH FROM od.fecha_atencion_servicio) = ?", (int)$datosFiltran['meses_anio']));

			if ((!empty($datosFiltran["anio"])))
				$select -> where -> addPredicate(new \Zend\Db\Sql\Predicate\Expression("EXTRACT( YEAR FROM od.fecha_atencion_servicio) = ?", (int)$datosFiltran['anio']));

		}


		$select -> order('avisos.fecha_carga DESC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;

	}

	public function obtenerPorId($id) {

		$id = (int)$id;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> join(array('suc' => 'sucursales'), 'suc.id_sucursal = avisos.sucursales_id_sucursal', array('nombre_sucursal' => 'nombre_sucursal'));

		$select -> where(array('avisos.id_aviso' => $id));

		$rowset = $this -> tableGateway -> selectWith($select);
		$row = $rowset -> current();
		return $row;
	}

	public function obtenerPorIdNumeroAviso($num_aviso) {

		$num_aviso = (String)$num_aviso;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> join(array('suc' => 'sucursales'), 'suc.id_sucursal = avisos.sucursales_id_sucursal', array('nombre_sucursal' => 'nombre_sucursal'));

		$select -> where -> like('avisos.numero_aviso', '%' . $num_aviso . '%');

		$rowset = $this -> tableGateway -> selectWith($select);
		$row = $rowset -> current();
		return $row;

	}

	public function obtenerPorPrioridad($id) {

		$id = (int)$id;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where(array('avisos.prioridad' => $id));

		$select -> order('id_aviso ASC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorIdServicio($id) {

		$id = (int)$id;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where(array('avisos.servicios_id_servicio' => $id));

		$select -> order('id_aviso ASC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorNumAviso($num_aviso) {

		$num_aviso = (String)$num_aviso;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where -> like('numero_aviso', '%' . $num_aviso . '%');

		$select -> order('id_aviso ASC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorNumInstalacion($num_instalacion) {

		$num_instalacion = (String)$num_instalacion;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where -> like('inst.num_instalacion', '%' . $num_instalacion . '%');

		$select -> order('id_aviso ASC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorPoblacion($nom_poblacion) {

		$nom_poblacion = (String)$nom_poblacion;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));

		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));

		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');

		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where -> like('inst.nom_poblacion', '%' . $nom_poblacion . '%');

		$select -> order('avisos.fecha_carga DESC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorTecnicoAviso($id_tecnico) {

		$id_tecnico = (String)$id_tecnico;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();
		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));
		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');
		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where(array('avisos.responsable_personal_id_persona' => $id_tecnico));

		$select -> order('avisos.fecha_carga DESC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;

	}

	/*
	 *
	 * Obtengo los avisos asignados al tecnico.
	 *
	 */
	public function obtenerAvisosAsignadosTecnico($id_tecnico, $id) {

		$id_tecnico = (String)$id_tecnico;

		if (empty($id)) {
			$id_usuario = $this -> login -> id;
		} else {
			$id_usuario = $id;
		}

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));
		$select -> join(array('distri' => 'distribuidoras'), 'inst.distribuidoras_id_distribuidora = distri.id_distribuidora', array('nombre_distribuidora' => 'nombre'));
		$select -> join(array('serv' => 'servicios'), 'serv.id_servicio = avisos.servicios_id_servicio', array('*'));
		$select -> join(array('sucu' => 'sucursales'), 'sucu.id_sucursal = avisos.sucursales_id_sucursal', array('nombre_sucursal' => 'nombre_sucursal'));
		$select -> join(array('soli' => 'solicitantes'), 'soli.id_solicitante_distribuidora = avisos.solicitante_id_solicitante', array('*'));
		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
		//$select->join(array('od' => 'ors_riat'),'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat','num_form_riat' => 'num_form_riat','fecha_atencion_servicio' => 'fecha_atencion_servicio' , 'estado_riat' => 'estado_riat' ), 'left');
		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
		$select -> where(array('avisos.responsable_personal_id_persona' => $id_tecnico));
		$select -> where(array('est.id_estado_aviso' => 2));
		$select -> order('avisos.fecha_carga DESC');

		/*echo '<pre>';
        echo $select->getSqlString();
        exit;*/

		return $this -> tableGateway -> selectWith($select);

	}

	/*
	 *
	 * Buscar en el Historico de Ejecutados.
	 *
	 */

	public function ObtenerHistorialNumeroAviso($numAviso) {

		try {
			$numAviso = (Int)$numAviso;
		} catch (Exception $exc) {
			echo $exc -> getMessege();
			echo $exc -> getTraceAsString();
			exit ;
		}

		$statement = $this -> dbAdapter -> query('SELECT 
                                                 r.lat,
                                                 r.lon,
                                                 r."r_dis",
                                                 r."ti_emp",
                                                 r."ti_aco",
                                                 r.auto,
                                                 r."pla_pos",
                                                 r."co_me",
                                                 r."nu_se",
                                                 r."an_fa",
                                                 r."cu_de",
                                                 r."bl_de",
                                                 r."cm_de",
                                                 r."co_me_ins",
                                                 r."a_fa_ins",
                                                 r."nu_se_ins",
                                                 r."res_ev_ins_0",
                                                 r."co_fin",
                                                 r."comentario_laboratorio"
                                                FROM
                                                  servicio_rim.rim_ejecutados_tablet as r
                                                WHERE r."num_avi" = ' . $numAviso . ' AND r."ti_serv" in (197,199,200)
                                                ORDER BY r."fe_eje" desc LIMIT 1;');
		$results = $statement -> execute();

		foreach ($results as $value) {
			return $value;
		}

	}

	public function obtenerPorNumUnidadLectura($numerounidadlectura) {

		$numerounidadlectura = (String)$numerounidadlectura;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();
		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));
		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'), 'left');
		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
		$select -> where -> like('inst.unidad_lectura', '%' . $numerounidadlectura . '%');
		$select -> order('avisos.fecha_carga DESC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	public function obtenerPorEstadoAviso($estado_aviso) {

		$estado_aviso = (int)$estado_aviso;

		$id_usuario = $this -> login -> id;

		$select = $this -> tableGateway -> getSql() -> select();
		$select -> join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('*'));
		$select -> join(array('od' => 'ors_riat'), 'od.avisos_id_aviso = avisos.id_aviso', array('id_ors_riat' => 'id_ors_riat', 'num_form_riat' => 'num_form_riat', 'fecha_atencion_servicio' => 'fecha_atencion_servicio', 'estado_riat' => 'estado_riat'));
		$select -> join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
		$select -> join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres', 'apellidos' => 'apellidos'), 'left');
		
                $select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avisos.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$select -> where(array('est.id_estado_aviso' => $estado_aviso));

		$select -> order('avisos.fecha_carga DESC');

		$dbAdapter = $this -> tableGateway -> getAdapter();
		$resultSetPrototype = $this -> tableGateway -> getResultSetPrototype();

		$adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
		$paginator = new Paginator($adapter);
		return $paginator;
	}

	//    public function obtenerPorNumBrigada($id) {
	//
	//        $id = (int) $id;
	//
	//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
	//
	//        $select = $this->tableGateway->getSql()->select();
	//        $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avisos.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
	//        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
	//        $select->join(array('avas' => 'avisos_asignados'), 'avas.avisos_id_aviso = avisos.id_aviso', array('brigadas_id_brigada' => 'brigadas_id_brigada'), 'left');
	//        $select->join(array('bri' => 'brigadas'), 'avas.brigadas_id_brigada = bri.id_brigada', array('nombre_brigada' => 'nombre_brigada'), 'left');
	//        $select->where(array('avisos.sucursales_id_sucursal' => 1));
	//
	//        $select->where(array('bri.id_brigada' => $id));
	//        $select->order('id_aviso ASC');
	//
	//        $dbAdapter = $this->tableGateway->getAdapter();
	//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
	//
	//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
	//        $paginator = new Paginator($adapter);
	//        return $paginator;
	//    }

	public function obtenerPorNumAviso_2($num_aviso) {

		$num_aviso = (String)$num_aviso;

		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where(array('numero_aviso' => $num_aviso));
		$select -> order('estados_avisos_id_estado_aviso DESC');

		$rowset = $this -> tableGateway -> selectWith($select);
		return $rowset;

	}

	public function guardar(Aviso $aviso) {

		$data = array(
               'sucursales_id_sucursal' => $aviso -> getSucursales_id_sucursal(), 
               'responsable_personal_id_persona' => $aviso -> getResponsable_personal_id_persona(), 
               'supervisor_personal_id_persona' => $aviso -> getSupervisor_personal_id_persona(), 
               'instalaciones_id_instalacion' => $aviso -> getInstalaciones_id_instalacion(), 
               'estados_avisos_id_estado_aviso' => $aviso -> getEstados_avisos_id_estado_aviso(), 
               'numero_aviso' => $aviso -> getNumero_aviso(), 
               'descripcion' => $aviso -> getDescripcion(), 
               'tipo_aviso' => $aviso -> getTipo_aviso(),
		       //'fecha_carga'  => $aviso->getFecha_carga(),
		      'fecha_asignacion' => $aviso -> getFecha_asignacion(), 
              'fecha_valorizacion' => $aviso -> getFecha_valorizacion(), 
              'fecha_finalizacion' => $aviso -> getFecha_finalizacion(), 
              'vehiculos_id_vehiculo' => $aviso -> getVehiculos_id_vehiculo(), 
              'subot' => $aviso -> getSubot(), 
              'solicitante_id_solicitante' => $aviso -> getSolicitante_id_solicitante(),
		      //'prioridad' => $aviso->getPrioridad(),
		      'cuenta_contrato' => $aviso -> getCuenta_contrato(), 
              'servicios_id_servicio' => $aviso -> getServicios_id_servicio(), 
              'claseaviso' => $aviso -> getClaseaviso(), 
              'fechacreadosap' => $aviso -> getFechacreadosap(), 
              'fechaasignadosap' => $aviso -> getFechaasignadosap(), 
              'status' => $aviso -> getStatus(), );

		$id = (int)$aviso -> getId_aviso();

		if ($id == 0) {
			$this -> tableGateway -> insert($data);
		} else {
			if ($this -> obtenerPorId($id)) {
				$this -> tableGateway -> update($data, array('id_aviso' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function updateNumAviso(Aviso $aviso) {

		$data = array(
		//'numero_aviso' => $aviso->getNumero_aviso(),
		'responsable_personal_id_persona' => $aviso -> getResponsable_personal_id_persona(), 'tipo_aviso' => $aviso -> getTipo_aviso(), );

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {
			$this -> tableGateway -> update($data, array('id_aviso' => $id));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	public function updateEstadoAvisosSuspender(Aviso $aviso) {

		$data = array('estados_avisos_id_estado_aviso' => $aviso -> getEstados_avisos_id_estado_aviso(), 'fecha_suspendido' => $aviso -> getFecha_suspendido(), );

		$numaviso = (int)$aviso -> getNumero_aviso();

		if ($this -> obtenerPorNumAviso_2($numaviso)) {
			$this -> tableGateway -> update($data, array('numero_aviso' => $numaviso));
		} else {
			throw new \Exception('Form id does not exist');
		}

	}

	public function updateTecnicoResponsableAsignado(Aviso $aviso) {

		$data = array('responsable_personal_id_persona' => $aviso -> getResponsable_personal_id_persona(), 'estados_avisos_id_estado_aviso' => $aviso -> getEstados_avisos_id_estado_aviso(), 'fecha_asignacion' => $aviso -> getFecha_asignacion());

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {

			$this -> tableGateway -> update($data, array('id_aviso' => $id,
			//'estados_avisos_id_estado_aviso' => 1,
			//new \Zend\Db\Sql\Predicate\IsNull('responsable_personal_id_persona'),
			));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	public function updateConfirmaValoriza(Aviso $aviso) {

		$data = array('estados_avisos_id_estado_aviso' => $aviso -> getEstados_avisos_id_estado_aviso(), 'fecha_valorizacion' => $aviso -> getFecha_valorizacion(), );

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {
			$this -> tableGateway -> update($data, array('id_aviso' => $id));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	public function updateServiciodelAviso(Aviso $aviso) {

		$data = array('servicios_id_servicio' => $aviso -> getServicios_id_servicio());

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {
			$this -> tableGateway -> update($data, array('id_aviso' => $id));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	public function updateDatosServicioPrestadoEnTerreno(Aviso $aviso) {

		$data = array('numero_documento' => $aviso -> getNumero_Documento(), 'fecha_ejecucion' => $aviso -> getFecha_ejecucion(), 'mes_facturacion' => $aviso -> getMes_facturacion(), );

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {
			$this -> tableGateway -> update($data, array('id_aviso' => $id));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	public function updateConfirmaFinalizacion(Aviso $aviso) {

		$data = array('estados_avisos_id_estado_aviso' => $aviso -> getEstados_avisos_id_estado_aviso(), 'fecha_finalizacion' => $aviso -> getFecha_finalizacion(), );

		$id = (int)$aviso -> getId_aviso();

		if ($this -> obtenerPorId($id)) {
			$this -> tableGateway -> update($data, array('id_aviso' => $id));
		} else {
			throw new \Exception('Form id does not exist');
		}
	}

	//    public function obtenerBrigadas() {
	//
	//        $sql = new Sql($this->tableGateway->getAdapter());
	//        $select = $sql->select();
	//        $select->from('brigadas');
	//
	//        $statement = $sql->prepareStatementForSqlObject($select);
	//        $results = $statement->execute();
	//        $brigadas = new \ArrayObject();
	//
	//        foreach ($results as $row) {
	//            $brigada = new \Brigadas\Model\Entity\Brigada();
	//            $brigada->exchangeArray($row);
	//            $brigadas->append($brigada);
	//        }
	//
	//        return $brigadas;
	//    }
	//    public function obtenerBrigadasSelect() {
	//
	//        $brigadas = $this->obtenerBrigadas();
	//
	//        $result = array();
	//
	//        foreach ($brigadas as $brigada) {
	//
	//            $result[$brigada->getId_brigada()] = $brigada->getNombre_brigada();
	//        }
	//
	//        return $result;
	//    }

	/*
	 *
	 * INFORMES DE PRODUCCION SON VISTAS EN LA BASE DE DATOS EN EL ESQUEMA AVISOS
	 *
	 *
	 */

	public function obtenerListadoServiciosSucursal() {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('listado_servicios_sucursales');

		if ($sucursales_id_sucursal !== 1) {
			$select -> where(array('id_sucursal' => $sucursales_id_sucursal));
		}

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$listado_servicios = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\ListadoServiciosSucursales();
			$obj -> exchangeArray($row);
			$listado_servicios -> append($obj);
		}

		return $listado_servicios;
	    }

	public function obtenerProduccionMensual() {

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('resumen_produccion');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resumen_produccion = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\ResumenProduccion;
			$obj -> exchangeArray($row);
			$resumen_produccion -> append($obj);
		}

		return $resumen_produccion;
	}

	public function obtenerServiciosNacional() {

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('servicios');
		$select -> order('id_servicio ASC');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$servicios = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Servicios\Model\Entity\Servicio();
			$obj -> exchangeArray($row);
			$servicios -> append($obj);
		}

		return $servicios;
	}

	public function obtenerModelosMedidores() {

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('modelos_medidores_new');
		$select -> order('id_modelo ASC');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$modelos_medidores = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Instalaciones\Model\Entity\ModelosMedidores();
			$obj -> exchangeArray($row);
			$modelos_medidores -> append($obj);
		}

		return $modelos_medidores;
	}

	public function obtenerProduccionSemanal() {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('informe_auditoria_trifasico_semanal');
		$select -> where(array('id_sucursal' => $sucursales_id_sucursal));

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$produccion_semanal = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\InformeProduccionSemanal();
			$obj -> exchangeArray($row);
			$produccion_semanal -> append($obj);
		}

		return $produccion_semanal;
	}

	public function obtenerProduccionSemanal2($fecha1, $fecha2) {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;
		
		$id_usuario = $this -> login -> id;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('informe_auditoria_trifasico_semanal_2');		
		$select -> join(array('ussuc' => 'usuario_sucursal'), 
                    new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = informe_auditoria_trifasico_semanal_2.id_sucursal 
                    AND ussuc.usuarios_id_usuario = ' . $id_usuario . ' '), array());				
		$select -> where -> between('fechaejecucion', $fecha1, $fecha2);

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$produccion_semanal = new \ArrayObject();

		foreach ($results as $row) {
			$obj = new \Avisos\Model\Entity\InformeProduccionSemanal();
			$obj -> exchangeArray($row);
			$produccion_semanal -> append($obj);
		}

		return $produccion_semanal;
	}
	
	
	
    public function obtenerProduccionSemanalMonofasico($fecha1, $fecha2 , $regiones) {
		
		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;
                $id_usuario = $this -> login -> id;
		
		$sql = new Sql($this -> tableGateway -> getAdapter());
		
    		$select = $sql -> select();
    		$select -> from('rim_ejecutados');
                
                if(empty($regiones)){
                
                    $select -> join(array('ussuc' => 'usuario_sucursal'), 
                    new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = rim_ejecutados.sucursales_id_sucursal 
                    AND ussuc.usuarios_id_usuario = ' . $id_usuario . ' '), array());
    		
                }else{
                    
                   $select->where->in('id_region',$regiones); 
                    
                }
                
                
                //$select -> where(array('sucursales_id_sucursal' => $sucursales_id_sucursal));		
                
                //$select->where(array('rim_ejecutados_tablet.estado_rim' => 1)); 
                $select -> where -> between('fecha_ejecucion', $fecha1, $fecha2);
		
                
                /*echo '<pre>';
                echo $select->getSqlString();
                exit;*/
           

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();
		
		$produccion_semanal = array();
		
		foreach ($results as $row) {
			
			$produccion_semanal[] = $row;
		}
				
		return $produccion_semanal;
		
	}
	
	
	

	public function ObtenerRegionesInformeEjecutivo() {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('regiones_informe_ejecutivo');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$regiones = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\Region();
			$obj -> exchangeArray($row);
			$regiones -> append($obj);
		}

		return $regiones;
	}

	public function ObtenerInstalacionesAuditadasRegiones($mes, $anio) {

		$mes = (int)$mes;
		$anio = (int)$anio;

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('instalaciones_auditadas_region');
		$select -> where(array('mes' => $mes, 'anio' => $anio));

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$total_auditados = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\TotalAvisosAuditados();
			$obj -> exchangeArray($row);
			$total_auditados -> append($obj);
		}

		return $total_auditados;
	}

	public function ObtenerResultadosAuditoriasTotalesEstadosRegiones($mes, $anio) {

		$mes = (int)$mes;
		$anio = (int)$anio;

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('resultados_auditorias_totales_estados_regiones');
		$select -> where(array('mes' => $mes, 'anio' => $anio));

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resultados_auditorias = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\ResultadoEstadoAuditoria();
			$obj -> exchangeArray($row);
			$resultados_auditorias -> append($obj);
		}

		return $resultados_auditorias;

	}

	public function ObtenerResultadosAuditoriasTotalesEstadosPorcentajesRegiones($mes, $anio) {

		$mes = (int)$mes;
		$anio = (int)$anio;

		//        $sql = new Sql($this->tableGateway->getAdapter());
		//
		//        $select = $sql->select();
		//        $select->from('resultado_auditorias_totales_estados_porcentajes');
		//
		//        $statement = $sql->prepareStatementForSqlObject($select);
		//        $results = $statement->execute();

		$statement = $this -> dbAdapter -> query("SELECT * FROM total_auditorias_regiones_porcentajes({$mes})");
		$results = $statement -> execute();
		$resultados_auditorias = new \ArrayObject();

		foreach ($results as $row) {
			$obj = new \Avisos\Model\Entity\ResultadoEstadoAuditoriaPorcentaje();
			$obj -> exchangeArray($row);
			$resultados_auditorias -> append($obj);
		}

		return $resultados_auditorias;

	}

	public function ObtenerSumResumenEjecutivo($mes, $anio) {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$mes = (int)$mes;
		$anio = (int)$anio;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('sum_resumen_ejecutivo_anormalidades_cnr');
		$select -> where(array('mes' => $mes, 'anio' => $anio));

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resultados = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\SumResumenEjecutivoAnormalidadesCnr();
			$obj -> exchangeArray($row);
			$resultados -> append($obj);
		}

		return $resultados;
	}

	public function ObtenerResumenEjecutivo($id_region, $mes, $anio) {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('resumen_ejecutivo');
		$select -> where(array('id_region' => $id_region));
		$select -> where(array('mes' => $mes, 'anio' => $anio));
		$select -> order('nombre_region ASC');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resultados = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\ResumenEjecutivo();
			$obj -> exchangeArray($row);
			$resultados -> append($obj);
		}

		return $resultados;
	}

	public function ObtenerMesesEjecutados() {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('meses_ejecutados');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resultados = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\MesesEjecutados();
			$obj -> exchangeArray($row);
			$resultados -> append($obj);
		}

		return $resultados;
	}

	public function ObtenerDetalleFacturacion() {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('detallefacturacionsucursal');
		$select -> where(array('sucursales_id_sucursal' => $sucursales_id_sucursal));

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$resultados = new \ArrayObject();

		foreach ($results as $row) {

			$obj = new \Avisos\Model\Entity\DetalleFacturacionSucursal();
			$obj -> exchangeArray($row);
			$resultados -> append($obj);

		}

		return $resultados;

	}

	public function ObtenerDetalleFacturacionTalca($fecha1, $fecha2) {

		$sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();
		$select -> from('detallefacturacionsucursaltalca');
		$select -> where(array('sucursales_id_sucursal' => $sucursales_id_sucursal));
		$select -> where -> between('fecha_ejecucion', $fecha1, $fecha2);

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$detalle_facturacion = new \ArrayObject();

		foreach ($results as $row) {
			$obj = new \Avisos\Model\Entity\DetalleFacturacionSucursalTalca();
			$obj -> exchangeArray($row);
			$detalle_facturacion -> append($obj);
		}

		return $detalle_facturacion;

	}

	/************************************************************************* */

	public function obtenerSucursales() {

		$id_usuario = $this -> login -> id;

		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select();
		$select -> from('sucursales');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = sucursales.id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();
		$sucursales = new \ArrayObject();

		foreach ($results as $row) {

			$sucursal = new \Avisos\Model\Entity\Sucursal;
			$sucursal -> exchangeArray($row);
			$sucursales -> append($sucursal);
		}

		return $sucursales;
	}

	public function obtenerSucursalesSelect() {

		$sucursales = $this -> obtenerSucursales();

		$result = array();

		foreach ($sucursales as $sucursal) {

			$result[$sucursal -> getId_sucursal()] = $sucursal -> getNombre_sucursal() . ' (' . $sucursal -> getId_sucursal() . ')';
		}

		return $result;
	}

	public function obtenerEstados() {

		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select();
		$select -> from('estados_avisos');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();
		$estados_avisos = new \ArrayObject();

		foreach ($results as $row) {

			$estado = new \Avisos\Model\Entity\EstadoAviso;
			$estado -> exchangeArray($row);
			$estados_avisos -> append($estado);
		}

		return $estados_avisos;
	}

	public function obtenerEstadosSelect() {

		$estados_avisos = $this -> obtenerEstados();

		$result = array();

		foreach ($estados_avisos as $estado) {

			$result[$estado -> getId_estado_aviso()] = $estado -> getNombreEstadoAviso();
		}

		return $result;
	}

	public function obtenerPersonalTecnico() {

		$id_usuario = $this -> login -> id;

		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select();
		$select -> from('personaltecnico');
		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = personaltecnico.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$personal_tecnico = new \ArrayObject();

		foreach ($results as $row) {

			$tecnico = new \Personal\Model\Entity\Persona;
			$tecnico -> exchangeArray($row);
			$personal_tecnico -> append($tecnico);
		}

		return $personal_tecnico;
	}

	public function obtenerPersonalTecnicoSelect() {

		$personal_tecnico = $this -> obtenerPersonalTecnico();

		$result = array();

		$result[0] = 'Seleccionar Tecnico';
		foreach ($personal_tecnico as $tecnico) {

			$result[$tecnico -> getId_persona()] = ucfirst(strtolower($tecnico -> getNombres())) . ' ' . ucfirst(strtolower($tecnico -> getApellidos())) . ' (' . $tecnico -> getSucursales_id_sucursal() . ')';
		}

		return $result;
	}

	public function obtenerPoblaciones() {

		$id_usuario = $this -> login -> id;

		$sql = new Sql($this -> tableGateway -> getAdapter());

		$select = $sql -> select();

		$select -> from('comunas_instalacion');

		$select -> join(array('sucdis' => 'sucursal_distribuidoras'), 'sucdis.distribuidoras_id_distribuidora = comunas_instalacion.id_distribuidora', array());

		$select -> join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = sucdis.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$comunas_instalacion = new \ArrayObject();

		foreach ($results as $row) {

			$comuna = new \Avisos\Model\Entity\ComunaInstalacion;
			$comuna -> exchangeArray($row);
			$comunas_instalacion -> append($comuna);
		}

		return $comunas_instalacion;
	}

	public function obtenerPoblacionesSelect() {

		$comunas_instalacion = $this -> obtenerPoblaciones();

		$result = array();

		foreach ($comunas_instalacion as $comuna) {

			$result[$comuna -> getComunas()] = $comuna -> getComunas();

			//ucfirst(strtolower($comuna->getComunas()));
		}

		return $result;
	}

	//    public function eliminar(Usuario $usuario) {
	//        $this->tableGateway->delete(array('id' => $usuario->getId()));
	//    }
	//    public function buscarPorNombre($nombre) {
	//        $select = $this->tableGateway->getSql()->select();
	//        $select->where->like('nombre', '%' . $nombre . '%');
	//        $select->order("nombre");
	//
	//        return $this->tableGateway->selectWith($select);
	//    }
	//    public function obtenerCuenta($email, $clave) {
	//        $select = $this->tableGateway->getSql()->select();
	//        $select->where(array('email' => $email, 'clave' => $clave,));
	//
	//        return $this->tableGateway->selectWith($select)->current();
	//    }

	public function obtenerDistribuidoras($SucId) {

		$SucId = (int)$SucId;

		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select();
		$select -> from('distribuidoras');
		$select -> join(array('sucdis' => 'sucursal_distribuidoras'), new \Zend\Db\Sql\Expression('sucdis.distribuidoras_id_distribuidora = distribuidoras.id_distribuidora AND sucdis.sucursales_id_sucursal = ' . $SucId . ''), array());
		$select -> order('distribuidoras.id_distribuidora ASC');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$distribuidoras = new \ArrayObject();

		foreach ($results as $row) {
			$distribuidora = new \Instalaciones\Model\Entity\Distribuidoras();
			$distribuidora -> exchangeArray($row);
			$distribuidoras -> append($distribuidora);
		}

		$result = array();

		foreach ($distribuidoras as $reg) {
			$result[$reg -> getIddistribuidora()] = ucwords(strtolower($reg -> getNombre()));
		}

		return $result;
	}

	public function obtenerSolicitantes($IdDis) {

		$IdDis = (int)$IdDis;

		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select();
		$select -> from('solicitantes');
		$select -> where(array('distribuidoras_id_distribuidora' => $IdDis));
		$select -> order('id_solicitante_distribuidora ASC');

		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();

		$solicitantes = new \ArrayObject();

		foreach ($results as $row) {
			$solicitante = new \Avisos\Model\Entity\Solicitante();
			$solicitante -> exchangeArray($row);
			$solicitantes -> append($solicitante);
		}

		$result = array();

		foreach ($solicitantes as $reg) {
			$result[$reg -> getId_solicitante_distribuidora()] = ucwords(strtolower($reg -> getNombre_completo())) . ' ( ' . ucwords(strtolower($reg -> getDepartamento_distri())) . ' )';
		}

		return $result;
	}

	/*Reprocesar un Aviso para mantener un Historico de este*/

	public function ReprocesarAviso($id) {
		try {

			$id = (String)$id;

			$statement = $this -> dbAdapter -> query("INSERT INTO 
                                                   avisos (
                                                     sucursales_id_sucursal,
                                                     supervisor_personal_id_persona,
                                                     instalaciones_id_instalacion,
                                                     estados_avisos_id_estado_aviso,
                                                     numero_aviso,
                                                     descripcion,
                                                     tipo_aviso,
                                                     subot,
                                                     solicitante_id_solicitante,
                                                     prioridad,
                                                     cuenta_contrato,
                                                     servicios_id_servicio )
                                                     SELECT 
                                                        a.sucursales_id_sucursal,
                                                        a.supervisor_personal_id_persona,
                                                        a.instalaciones_id_instalacion,
                                                        1,
                                                        a.numero_aviso,
                                                        a.descripcion,
                                                        a.tipo_aviso,
                                                        a.subot,
                                                        a.solicitante_id_solicitante,
                                                        a.prioridad,
                                                        a.cuenta_contrato,
                                                        a.servicios_id_servicio
                                                     FROM avisos a WHERE a.id_aviso = {$id}");

			$results = $statement -> execute();

			return true;

		} catch (Exception $e) {

			echo 'Excepción Message : ', $e -> getMessage(), "\n";
			echo 'Excepción Code : ', $e -> getCode(), "\n";
			echo 'Excepción File : ', $e -> getFile(), "\n";
			echo 'Excepción Line : ', $e -> getLine(), "\n";

			exit ;
		}
	}

	
	/*
	 * 
	 * Se almeccena el aviso que ya se ejecuto antes con el estado 8 que es repetido
	 * asi se entregara en los informes de produccion semanal como repetido y por ende
	 * nose ejecutaran en terreno.
	 * 
	 * 
	 * */
    public function RepetidoAviso($id) {
		try {

			$id = (String)$id;

			$statement = $this -> dbAdapter -> query("INSERT INTO 
                                                   avisos (
                                                     sucursales_id_sucursal,
                                                     supervisor_personal_id_persona,
                                                     instalaciones_id_instalacion,
                                                     estados_avisos_id_estado_aviso,
                                                     numero_aviso,
                                                     descripcion,
                                                     tipo_aviso,
                                                     subot,
                                                     solicitante_id_solicitante,
                                                     prioridad,
                                                     cuenta_contrato,
                                                     servicios_id_servicio )
                                                     SELECT 
                                                        a.sucursales_id_sucursal,
                                                        a.supervisor_personal_id_persona,
                                                        a.instalaciones_id_instalacion,
                                                        8,
                                                        a.numero_aviso,
                                                        a.descripcion,
                                                        a.tipo_aviso,
                                                        a.subot,
                                                        a.solicitante_id_solicitante,
                                                        a.prioridad,
                                                        a.cuenta_contrato,
                                                        a.servicios_id_servicio
                                                     FROM avisos a WHERE a.id_aviso = {$id}");

			$results = $statement -> execute();

			return true;

		} catch (Exception $e) {

			echo 'Excepción Message : ', $e -> getMessage(), "\n";
			echo 'Excepción Code : ', $e -> getCode(), "\n";
			echo 'Excepción File : ', $e -> getFile(), "\n";
			echo 'Excepción Line : ', $e -> getLine(), "\n";

			exit ;
		}
	}
	
	
	
	/***Solo Avisos Finalizados***/
	/***EN trifasico los avisos llegan a finalizado***/
	/***En monofasico llega el aviso hasta valorizado***/
	
	public function ReprocesarNumeroAviso($id) {
		try {

			$id = (String)$id;

			$statement = $this -> dbAdapter -> query("INSERT INTO 
                                                   avisos (
                                                     sucursales_id_sucursal,
                                                     supervisor_personal_id_persona,
                                                     instalaciones_id_instalacion,
                                                     estados_avisos_id_estado_aviso,
                                                     numero_aviso,
                                                     descripcion,
                                                     tipo_aviso,
                                                     subot,
                                                     solicitante_id_solicitante,
                                                     prioridad,
                                                     cuenta_contrato,
                                                     servicios_id_servicio )
                                                     SELECT 
                                                        a.sucursales_id_sucursal,
                                                        a.supervisor_personal_id_persona,
                                                        a.instalaciones_id_instalacion,
                                                        1,
                                                        a.numero_aviso,
                                                        a.descripcion,
                                                        a.tipo_aviso,
                                                        a.subot,
                                                        a.solicitante_id_solicitante,
                                                        a.prioridad,
                                                        a.cuenta_contrato,
                                                        a.servicios_id_servicio
                                                     FROM avisos a WHERE a.numero_aviso = {$id} AND a.estados_avisos_id_estado_aviso = 4");

			$results = $statement -> execute();

			return true;
		} catch (Exception $e) {

			echo 'Excepción Message : ', $e -> getMessage(), "\n";
			echo 'Excepción Code : ', $e -> getCode(), "\n";
			echo 'Excepción File : ', $e -> getFile(), "\n";
			echo 'Excepción Line : ', $e -> getLine(), "\n";

			exit ;
		}
	}

	public function ReprocesarAvisoNuevoServicio($idaviso, $idservicio) {

		try {

			$idaviso = (String)$idaviso;
			$idservicio = (Int)$idservicio;

			$statement = $this -> dbAdapter -> query("INSERT INTO 
                                                   avisos (
                                                     sucursales_id_sucursal,
                                                     supervisor_personal_id_persona,
                                                     instalaciones_id_instalacion,
                                                     estados_avisos_id_estado_aviso,
                                                     numero_aviso,
                                                     descripcion,
                                                     tipo_aviso,
                                                     subot,
                                                     solicitante_id_solicitante,
                                                     prioridad,
                                                     cuenta_contrato,
                                                     servicios_id_servicio )
                                                     SELECT 
                                                        a.sucursales_id_sucursal,
                                                        a.supervisor_personal_id_persona,
                                                        a.instalaciones_id_instalacion,
                                                        1,
                                                        a.numero_aviso,
                                                        a.descripcion || ', ( Segunda visita )',
                                                        a.tipo_aviso,
                                                        a.subot,
                                                        a.solicitante_id_solicitante,
                                                        a.prioridad,
                                                        a.cuenta_contrato,
                                                        {$idservicio}
                                                     FROM avisos a WHERE a.id_aviso = {$idaviso} AND a.estados_avisos_id_estado_aviso = 3");

			$results = $statement -> execute();

			return true;
			
		} catch (Exception $e) {

			echo 'Excepción Message : ', $e -> getMessage(), "\n";
			echo 'Excepción Code : ', $e -> getCode(), "\n";
			echo 'Excepción File : ', $e -> getFile(), "\n";
			echo 'Excepción Line : ', $e -> getLine(), "\n";

			exit ;
		}
	}
        
        
        
        
    /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * 
     * 
     *           ESTADISTICAS DE CONTROL MONOFASICO 
     *
     * 
     *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
        
    public function obtenerPanelControlMonofasicoContadores($meses,$regiones,$fechainicio,$fechafinal) {
	
	    $sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;
        $id_usuario = $this -> login -> id;		
	    
            $statement = $this -> dbAdapter -> query("SELECT * FROM avisos.panel_control_monofasico('0','".$meses."','".$regiones ."',".$fechainicio.",".$fechafinal.")");
                    
            $results = $statement -> execute();
		
            $control = array();

            foreach ($results as $row) {

                    $control[] = $row;
            }

            return $control;
		
	}

    public function obtenerPanelControlMonofasicoMinMax($meses,$regiones,$fechainicio,$fechafinal) {
    
        $sucursales_id_sucursal = $this -> login -> sucursales_id_sucursal;
        $id_usuario = $this -> login -> id;
		
            $statement = $this -> dbAdapter -> query("SELECT * FROM avisos.panel_control_monofasico_min_max('0','".$meses ."','".$regiones."',".$fechainicio." ,".$fechafinal.")");
                    
            $results = $statement -> execute();
        
            $control = array();

            foreach ($results as $row) {

                    $control[] = $row;
            }

            return $control;
        
    }

   
   public function obtenerDiasHabilesdeTrabajo($fechaInicio,$fechaFinal){

           //Cantidad de dias en el periodo seleccionado.
           $statement_0 = $this -> dbAdapter -> query("SELECT CAST(".$fechaFinal." as DATE) - CAST(".$fechaInicio." as DATE) + 1 as  cantidad_dias");
           $results_0 = $statement_0 -> execute();
           $control_0 = array();
            foreach ($results_0 as $row) { $control_0[] = $row; }

           //Cantiadad de Domingos
           $statement_1 = $this -> dbAdapter -> query("SELECT count(*) as cantidad_domingos FROM utilidades.f_encierra_dia(".$fechaInicio.",".$fechaFinal.",0)");
           $results_1 = $statement_1 -> execute();
           $control_1 = array();
            foreach ($results_1 as $row) { $control_1[] = $row; }

           //Cantidad de Sabados
           $statement_2 = $this -> dbAdapter -> query("SELECT count(*) as cantidad_sabados FROM utilidades.f_encierra_dia(".$fechaInicio.",".$fechaFinal.",6)");
           $results_2 = $statement_2 -> execute();
           $control_2 = array();
            foreach ($results_2 as $row) { $control_2[] = $row; }

            $result =  (($control_0[0]['cantidad_dias']  -  $control_1[0]['cantidad_domingos'])  -  $control_2[0]['cantidad_sabados']);

            return $result;

         }

   


}
