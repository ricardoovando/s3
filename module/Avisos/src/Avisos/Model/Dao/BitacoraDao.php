<?php

namespace Avisos\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Avisos\Model\Entity\Bitacora;

class BitacoraDao {

    protected $tableGateway;
    protected $dbAdapter;
    
    public function __construct(TableGateway $tableGateway , Adapter $dbAdapter) {
        $this->tableGateway = $tableGateway; 
        $this->dbAdapter = $dbAdapter;
    }

    public function buscarRegistro($fecha,$oficina,$encargado,$ayudante) {

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('GNR_FECHA' => $fecha));
        $select->where(array('GNR_OFICINA' => $oficina));
        $select->where(array('BGD_ENCARGADO' => $encargado));
        $select->where(array('BGD_AYUDANTE' => $ayudante));
        
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        
        return $row;
        
    }

    public function guardar(Bitacora $bitacora) {

        $data = array(
            //'ID_BITACORA' => $bitacora->getID_BITACORA(),
            'GNR_FECHA' => $bitacora->getGNR_FECHA(),
            'GNR_OFICINA' => $bitacora->getGNR_OFICINA(),
            'GNR_PROYECTO' => $bitacora->getGNR_PROYECTO(),
            'BGD_ENCARGADO' => $bitacora->getBGD_ENCARGADO(),
            'BGD_AYUDANTE' => $bitacora->getBGD_AYUDANTE(),
            'BGD_INSTRUMENTO' => $bitacora->getBGD_INSTRUMENTO(),
            'MV_PATENTE' => $bitacora->getMV_PATENTE(),
            'MV_KM_SALIDA' => $bitacora->getMV_KM_SALIDA(),
            'MV_KM_LLEGADA' => $bitacora->getMV_KM_LLEGADA(),
            'HRS_HORA_SALIDA' => $bitacora->getHRS_HORA_SALIDA(),
            'HRS_HORA_LLEGADA' => $bitacora->getHRS_HORA_LLEGADA(),
            'TDO_MASIVAS' => $bitacora->getTDO_MASIVAS(),
            'TDO_RECLAMOS' => $bitacora->getTDO_RECLAMOS(),
            'TDO_PERITAJES' => $bitacora->getTDO_PERITAJES(),
            'TDO_AUTOGEN' => $bitacora->getTDO_AUTOGEN(),
            'SE_INSPECCION_VISUAL' => $bitacora->getSE_INSPECCION_VISUAL(),
            'SE_VRF_SIN_CAMBIO' => $bitacora->getSE_VRF_SIN_CAMBIO(),
            'SE_VRF_CON_CAMBIO' => $bitacora->getSE_VRF_CON_CAMBIO(),
            'SE_CAMBIO_DIRECTO' => $bitacora->getSE_CAMBIO_DIRECTO(),
            'SE_LAB_SERVICIO' => $bitacora->getSE_LAB_SERVICIO(),
            'OSRV_MAN_MENOR' => $bitacora->getOSRV_MAN_MENOR(),
            'OSRV_LAB_TERCEROS' => $bitacora->getOSRV_LAB_TERCEROS(),
            'CCSRV_DEVOLUCION' => $bitacora->getCCSRV_DEVOLUCION(),
            'CCSRV_REINSTALACION' => $bitacora->getCCSRV_REINSTALACION(),
            'CCSRV_FRUSTRADO' => $bitacora->getCCSRV_FRUSTRADO(),
            'RSLT_SERVICIOS_SIN_CNR' => $bitacora->getRSLT_SERVICIOS_SIN_CNR(),
            'RSLT_SERVICIOS_CON_CNR' => $bitacora->getRSLT_SERVICIOS_CON_CNR(),
            'RSLT_CNR_CONFIGURABLE' => $bitacora->getRSLT_CNR_CONFIGURABLE(),
            'RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN' => $bitacora->getRSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN(),
            'TCNR_MEDIDOR_FUERA_DE_RANGO' => $bitacora->getTCNR_MEDIDOR_FUERA_DE_RANGO(),
            'TCNR_OTRO' => $bitacora->getTCNR_OTRO(),
            'NRM_CNR_NORMALIZADOS' => $bitacora->getNRM_CNR_NORMALIZADOS(),
            'NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION' => $bitacora->getNRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION(),
            'NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS' => $bitacora->getNRM_CNR_SN_NRM_CONDICIONES_INSEGURAS(),
            'NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR' => $bitacora->getNRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR(),
            'NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA' => $bitacora->getNRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA(),
            'NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS' => $bitacora->getNRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS(),
            'TVIPD_CERRADO_EN_2DA_VISITA' => $bitacora->getTVIPD_CERRADO_EN_2DA_VISITA(),
            'TVIPD_CERRADO_EN_1RA_VISITA' => $bitacora->getTVIPD_CERRADO_EN_1RA_VISITA(),
            'TVIPD_CLIENTE_NO_AUTORIZA' => $bitacora->getTVIPD_CLIENTE_NO_AUTORIZA(),
            'TVIPD_DOMICILIO_DESHABITADO' => $bitacora->getTVIPD_DOMICILIO_DESHABITADO(),
            'TVIPD_SITIO_ERIAZO' => $bitacora->getTVIPD_SITIO_ERIAZO(),
            'TVIPD_SIN_ENERGIA' => $bitacora->getTVIPD_SIN_ENERGIA(),
            'TVIPD_NO_EXISTE_RESPONSABLE' => $bitacora->getTVIPD_NO_EXISTE_RESPONSABLE(),
            'TVIPD_NO_UBICADO' => $bitacora->getTVIPD_NO_UBICADO(),
            'TVIPD_EMPALME_NO_EXISTE' => $bitacora->getTVIPD_EMPALME_NO_EXISTE(),
            'TVIPD_CASA_CERRADA' => $bitacora->getTVIPD_CASA_CERRADA(),
            'DIAS_DESCONTAR' => $bitacora->getDIAS_DESCONTAR(),
            'OBSERVACIONES' => $bitacora->getOBSERVACIONES(),
        );
        
       if(!$this->buscarRegistro($bitacora->getGNR_FECHA(), $bitacora->getGNR_OFICINA(), $bitacora->getBGD_ENCARGADO(), $bitacora->getBGD_AYUDANTE())){ 
           
           $this->tableGateway->insert($data);
           
       }else{
           
           return $this->buscarRegistro($bitacora->getGNR_FECHA(), $bitacora->getGNR_OFICINA(), $bitacora->getBGD_ENCARGADO(), $bitacora->getBGD_AYUDANTE());
           
       }
      
    }
    
   
   public function obtenerResultadosBitacoraOficina($fecha_inicio,$fecha_fin){
       
    $fecha_inicio = (String) $fecha_inicio; 
    $fecha_fin = (String) $fecha_fin;

    $statement = $this->dbAdapter->query('SELECT
                                            b."GNR_OFICINA" as OFICINA,
                                            SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                            SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                            SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                            SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                            SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                            SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                            SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                            SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                            SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                            SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                            SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                            SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                            SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                            SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                            SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                            SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                            SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                            (
                                            (float4(0.35) * COALESCE(SUM(b."SE_INSPECCION_VISUAL"),0)) + 
                                            (float4(0.83) * COALESCE(SUM(b."SE_VRF_SIN_CAMBIO"),0)) +
                                            (float4(1) * COALESCE(SUM(b."SE_VRF_CON_CAMBIO"),0)) +
                                            (float4(0.47) * COALESCE(SUM(b."SE_CAMBIO_DIRECTO"),0)) +
                                            (float4(0.43) * COALESCE(SUM(b."OSRV_LAB_TERCEROS"),0)) +
                                            (float4(0.06) * ( COALESCE(SUM(b."TVIPD_CERRADO_EN_2DA_VISITA"),0) +
                                                              COALESCE(SUM(b."TVIPD_CLIENTE_NO_AUTORIZA"),0) +
                                                              COALESCE(SUM(b."TVIPD_DOMICILIO_DESHABITADO"),0) +
                                                              COALESCE(SUM(b."TVIPD_SITIO_ERIAZO"),0) +
                                                              COALESCE(SUM(b."TVIPD_SIN_ENERGIA"),0) +
                                                              COALESCE(SUM(b."TVIPD_NO_EXISTE_RESPONSABLE"),0) +
                                                              COALESCE(SUM(b."TVIPD_NO_UBICADO"),0) +
                                                              COALESCE(SUM(b."TVIPD_EMPALME_NO_EXISTE"),0)) ) 
                                           ) as UBP
                                            FROM bitacora b
                                            WHERE b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                            GROUP BY 1
                                            UNION ALL
                                            SELECT
                                            \'Total_Final\' as OFICINA,
                                            SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                            SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                            SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                            SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                            SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                            SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                            SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                            SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                            SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                            SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                            SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                            SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                            SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                            SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                            SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                            SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                            SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                            1 as UBP
                                            FROM bitacora b
                                            WHERE b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                            GROUP BY 1 ORDER BY 1;');
    
        $results = $statement->execute();
        return $results;  
       
   } 
    
   public function ObtenerResultadosBitacora($fecha_inicio,$fecha_fin) {
        
         $fecha_inicio = (String) $fecha_inicio; 
         $fecha_fin = (String) $fecha_fin;

         $statement = $this->dbAdapter->query('SELECT
                                                b."GNR_OFICINA" as OFICINA,
                                                b."BGD_ENCARGADO" as ENCARGADO,
                                                SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                                SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                                SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                                SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                                SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                                SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                                SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                                SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                                SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                                SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                                SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                                SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                                SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                                SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                                SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                                (
                                                (float4(0.35) * COALESCE(SUM(b."SE_INSPECCION_VISUAL"),0)) + 
                                                (float4(0.83) * COALESCE(SUM(b."SE_VRF_SIN_CAMBIO"),0)) +
                                                (float4(1) * COALESCE(SUM(b."SE_VRF_CON_CAMBIO"),0)) +
                                                (float4(0.47) * COALESCE(SUM(b."SE_CAMBIO_DIRECTO"),0)) +
                                                (float4(0.43) * COALESCE(SUM(b."OSRV_LAB_TERCEROS"),0)) +
                                                (float4(0.06) * ( COALESCE(SUM(b."TVIPD_CERRADO_EN_2DA_VISITA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_CLIENTE_NO_AUTORIZA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_DOMICILIO_DESHABITADO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_SITIO_ERIAZO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_SIN_ENERGIA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_NO_EXISTE_RESPONSABLE"),0) +
                                                                  COALESCE(SUM(b."TVIPD_NO_UBICADO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_EMPALME_NO_EXISTE"),0)) ) 
                                               ) as UBP
                                               FROM bitacora b
                                               WHERE b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                               GROUP BY 1,2
                                               UNION ALL
                                               SELECT
                                                \'Total_Final\' as OFICINA,
                                                \'Total_Final\' as ENCARGADO,
                                                SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                                SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                                SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                                SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                                SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                                SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                                SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                                SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                                SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                                SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                                SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                                SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                                SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                                SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                                SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                                1 as UBP
                                               FROM bitacora b
                                               WHERE b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                               GROUP BY 1,2 ORDER BY 1;');
         
         $results = $statement->execute();           
         return $results;
            
      }
      
   public function ObtenerResultadosAyudantesBitacora($fecha_inicio,$fecha_fin) {
        
         $fecha_inicio = (String) $fecha_inicio; 
         $fecha_fin = (String) $fecha_fin;

         $statement = $this->dbAdapter->query('SELECT
                                                b."GNR_OFICINA" as OFICINA,
                                                b."BGD_AYUDANTE" as AYUDANTE,
                                                SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                                SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                                SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                                SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                                SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                                SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                                SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                                SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                                SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                                SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                                SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                                SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                                SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                                SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                                SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                                (
                                                (float4(0.35) * COALESCE(SUM(b."SE_INSPECCION_VISUAL"),0)) + 
                                                (float4(0.83) * COALESCE(SUM(b."SE_VRF_SIN_CAMBIO"),0)) +
                                                (float4(1) * COALESCE(SUM(b."SE_VRF_CON_CAMBIO"),0)) +
                                                (float4(0.47) * COALESCE(SUM(b."SE_CAMBIO_DIRECTO"),0)) +
                                                (float4(0.43) * COALESCE(SUM(b."OSRV_LAB_TERCEROS"),0)) +
                                                (float4(0.06) * ( COALESCE(SUM(b."TVIPD_CERRADO_EN_2DA_VISITA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_CLIENTE_NO_AUTORIZA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_DOMICILIO_DESHABITADO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_SITIO_ERIAZO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_SIN_ENERGIA"),0) +
                                                                  COALESCE(SUM(b."TVIPD_NO_EXISTE_RESPONSABLE"),0) +
                                                                  COALESCE(SUM(b."TVIPD_NO_UBICADO"),0) +
                                                                  COALESCE(SUM(b."TVIPD_EMPALME_NO_EXISTE"),0)) ) 
                                               ) as UBP
                                               FROM bitacora b
                                               WHERE b."BGD_AYUDANTE" is not NULL AND b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                               GROUP BY 1,2
                                               UNION ALL
                                               SELECT
                                               \'Total_Final\' as OFICINA,
                                                \'Total_Final\' as AYUDANTE,
                                                SUM(b."SE_INSPECCION_VISUAL") as SE_INSPECCION_VISUAL,
                                                SUM(b."SE_VRF_SIN_CAMBIO") as SE_VRF_SIN_CAMBIO,
                                                SUM(b."SE_VRF_CON_CAMBIO") as SE_VRF_CON_CAMBIO,
                                                SUM(b."SE_CAMBIO_DIRECTO") as SE_CAMBIO_DIRECTO,
                                                SUM(b."OSRV_LAB_TERCEROS") as OSRV_LAB_TERCEROS,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR") as RSLT_SERVICIOS_CON_CNR,
                                                SUM(b."RSLT_CNR_CONFIGURABLE") as RSLT_CNR_CONFIGURABLE,
                                                SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN") as RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN,
                                                SUM(b."NRM_CNR_NORMALIZADOS") as NRM_CNR_NORMALIZADOS,
                                                SUM(b."TVIPD_CERRADO_EN_2DA_VISITA") as TVIPD_CERRADO_EN_2DA_VISITA,
                                                SUM(b."TVIPD_CLIENTE_NO_AUTORIZA") as TVIPD_CLIENTE_NO_AUTORIZA,
                                                SUM(b."TVIPD_DOMICILIO_DESHABITADO") as TVIPD_DOMICILIO_DESHABITADO,
                                                SUM(b."TVIPD_SITIO_ERIAZO") as TVIPD_SITIO_ERIAZO,
                                                SUM(b."TVIPD_SIN_ENERGIA") as TVIPD_SIN_ENERGIA,
                                                SUM(b."TVIPD_NO_EXISTE_RESPONSABLE") as TVIPD_NO_EXISTE_RESPONSABLE,
                                                SUM(b."TVIPD_NO_UBICADO") as TVIPD_NO_UBICADO,
                                                SUM(b."TVIPD_EMPALME_NO_EXISTE") as TVIPD_EMPALME_NO_EXISTE,
                                                1 as UBP
                                               FROM bitacora b
                                               WHERE b."BGD_AYUDANTE" is not NULL AND b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\'
                                               GROUP BY 1,2 ORDER BY 1;');
         
         $results = $statement->execute();
           
         return $results;
            
      }  
      
   public function ObtenerResultadosTotalesBitacora($fecha_inicio,$fecha_fin) {
        
         $fecha_inicio = (String) $fecha_inicio; 
         $fecha_fin = (String) $fecha_fin;

         $statement = $this->dbAdapter->query('SELECT
                                               (COALESCE(SUM(b."SE_INSPECCION_VISUAL"),0) + 
                                               COALESCE(SUM(b."SE_VRF_SIN_CAMBIO"),0) + 
                                               COALESCE(SUM(b."SE_VRF_CON_CAMBIO"),0) + 
                                               COALESCE(SUM(b."SE_CAMBIO_DIRECTO"),0) + 
                                               COALESCE(SUM(b."OSRV_LAB_TERCEROS"),0)) as SE, 
                                              (COALESCE(SUM(b."RSLT_SERVICIOS_CON_CNR"),0) +
                                               COALESCE(SUM(b."RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN"),0) ) as CNR,
                                               COALESCE(SUM(b."RSLT_CNR_CONFIGURABLE"),0) as RSLT_CNR_CONFIGURABLE,
                                               COALESCE(SUM(b."NRM_CNR_NORMALIZADOS"),0) as NRM_CNR_NORMALIZADOS,
                                              (COALESCE(SUM(b."TVIPD_EMPALME_NO_EXISTE"),0) +
                                               COALESCE(SUM(b."TVIPD_SITIO_ERIAZO"),0) +
                                               COALESCE(SUM(b."TVIPD_SIN_ENERGIA"),0) +
                                               COALESCE(SUM(b."TVIPD_DOMICILIO_DESHABITADO"),0) ) as causa_distribuidora, 
                                              (COALESCE(SUM(b."TVIPD_NO_EXISTE_RESPONSABLE"),0) + 
                                               COALESCE(SUM(b."TVIPD_CLIENTE_NO_AUTORIZA"),0) + 
                                               COALESCE(SUM(b."TVIPD_NO_UBICADO"),0) + 
                                               COALESCE(SUM(b."TVIPD_CERRADO_EN_2DA_VISITA"),0) )  as causa_tecnet
                                               FROM bitacora b 
                                               WHERE b."GNR_FECHA" BETWEEN \''. $fecha_inicio .'\' AND \''. $fecha_fin .'\';');
         
         $results = $statement->execute();
           
         return $results;
            
      }
      
}
