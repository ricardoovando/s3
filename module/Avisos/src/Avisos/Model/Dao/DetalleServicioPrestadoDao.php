<?php

namespace Avisos\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;


/* ENTITYS */
use Avisos\Model\Entity\DetalleServicioPrestado;

class DetalleServicioPrestadoDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerPorId($id) {

//        $id = (int) $id;
//
//        $select = $this->tableGateway->getSql()->select();
//        $select->where(array('avisos_id_aviso' => $id));
//        $rowset = $this->tableGateway->selectWith($select);
//        $row = $rowset->current();
//        return $row;
    }

    public function obtenerTotalFacturacionUf($idaviso) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('total_precio_uf' => new Expression('SUM(precio_uf)')));
        $select->where(array('avisos_id_aviso' => $idaviso));
        return $this->tableGateway->selectWith($select);
    }

    public function obtenerPorIdaviso($idaviso) {
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('ser' => 'servicios'), 'ser.id_servicio = servicios_id_servicio', array('nombre_servicio' => 'nombre_servicio'));
        $select->where(array('avisos_id_aviso' => $idaviso));
        return $this->tableGateway->selectWith($select);
      }

    public function insertServicioAviso(DetalleServicioPrestado $detalleservicioprestado) {

        $data = array(
            'avisos_id_aviso' => $detalleservicioprestado->getAvisos_id_aviso(),
            'servicios_id_servicio' => $detalleservicioprestado->getServicios_id_servicio(),
            'precio_uf' => $detalleservicioprestado->getPrecio_uf(),
            'cantidad' => $detalleservicioprestado->getCantidad(),
        );

        $this->tableGateway->insert($data);
      }

    public function deleteServicioAviso(DetalleServicioPrestado $detalleservicioprestado) { 

        $this->tableGateway->delete(array(
            'avisos_id_aviso' => $detalleservicioprestado->getAvisos_id_aviso(),
            'servicios_id_servicio' => $detalleservicioprestado->getServicios_id_servicio(),
        ));
    }

}
