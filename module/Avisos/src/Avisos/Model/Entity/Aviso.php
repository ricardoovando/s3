<?php

namespace Avisos\Model\Entity;

class Aviso {

    private $id_aviso;
    private $sucursales_id_sucursal;
    private $responsable_personal_id_persona;
    private $supervisor_personal_id_persona;
    private $instalaciones_id_instalacion;
    private $estados_avisos_id_estado_aviso;
    private $vehiculos_id_vehiculo;
    
    private $numero_aviso;
    private $descripcion;
    private $tipo_aviso;
    private $fecha_carga;
    private $fecha_asignacion;
    private $fecha_valorizacion;
    private $fecha_finalizacion;
    private $subot;
    private $solicitante_id_solicitante;
    private $prioridad;
    private $cuenta_contrato;
    private $servicios_id_servicio;
    private $numero_documento;
    private $fecha_ejecucion;
    private $mes_facturacion;
    
    private $claseaviso;
    private $fechacreadosap;
    private $fechaasignadosap;
    private $status;
    private $fecha_suspendido;
            
    /*Objetos Relacionales*/
        private $instalacion;
        private $estado_aviso;
        private $responsable_persona;
        private $supervisor_persona;
        private $vehiculo;
        private $brigada;
        private $ors_riat;
        private $sucursal;
        private $distribuidora;
        private $servicio;
        private $solicitante;
       private $resultado_aviso_trifasico;  
    /*Fin*/

        
    function __construct($id_aviso = null, 
            $sucursales_id_sucursal = null, 
            $responsable_personal_id_persona = null, 
            $supervisor_personal_id_persona = null, 
            $instalaciones_id_instalacion = null, 
            $estados_avisos_id_estado_aviso = null, 
            $vehiculos_id_vehiculo = null, 
            $numero_aviso = null, 
            $descripcion = null, 
            $tipo_aviso = null, 
            $fecha_carga = null, 
            $fecha_asignacion = null, 
            $fecha_valorizacion = null, 
            $fecha_finalizacion = null, 
            $subot = null, 
            $solicitante_id_solicitante = null, 
            $prioridad = null, 
            $cuenta_contrato = null,
            $servicios_id_servicio = null,
            $numero_documento = null,
            $fecha_ejecucion = null,
            $mes_facturacion = null,
            $claseaviso = null,
            $fechacreadosap = null,
            $fechaasignadosap = null,
            $status = null,
            $fecha_suspendido = null) {

                    $this->id_aviso = $id_aviso;
                    $this->sucursales_id_sucursal = $sucursales_id_sucursal;
                    $this->responsable_personal_id_persona = $responsable_personal_id_persona;
                    $this->supervisor_personal_id_persona = $supervisor_personal_id_persona;
                    $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
                    $this->estados_avisos_id_estado_aviso = $estados_avisos_id_estado_aviso;
                    $this->vehiculos_id_vehiculo = $vehiculos_id_vehiculo;
                    $this->numero_aviso = $numero_aviso;
                    $this->descripcion = $descripcion;
                    $this->tipo_aviso = $tipo_aviso;
                    $this->fecha_carga = $fecha_carga;
                    $this->fecha_asignacion = $fecha_asignacion;
                    $this->fecha_valorizacion = $fecha_valorizacion;
                    $this->fecha_finalizacion = $fecha_finalizacion;
                    $this->subot = $subot;
                    $this->solicitante_id_solicitante = $solicitante_id_solicitante;
                    $this->prioridad = $prioridad;
                    $this->cuenta_contrato = $cuenta_contrato;
                    $this->servicios_id_servicio = $servicios_id_servicio;
                    $this->numero_documento = $numero_documento;
                    $this->fecha_ejecucion = $fecha_ejecucion;
                    $this->mes_facturacion = $mes_facturacion;

                    $this->claseaviso = $claseaviso;
                    $this->fechacreadosap = $fechacreadosap;
                    $this->fechaasignadosap = $fechaasignadosap;
                    $this->status = $status;
                    $this->fecha_suspendido = $fecha_suspendido; 

                  }

   /*Id de la Tabla*/
        
    public function getId_aviso() {
        return $this->id_aviso;
    }

    public function setId_aviso($id_aviso) {
        $this->id_aviso = $id_aviso;
    }
    
    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }
    
    public function getResponsable_personal_id_persona() {
        return $this->responsable_personal_id_persona;
    }

    public function setResponsable_personal_id_persona($responsable_personal_id_persona) {
        $this->responsable_personal_id_persona = $responsable_personal_id_persona;
    }
    
    public function getSupervisor_personal_id_persona() {
        return $this->supervisor_personal_id_persona;
    }

    public function setSupervisor_personal_id_persona($supervisor_personal_id_persona) {
        $this->supervisor_personal_id_persona = $supervisor_personal_id_persona;
    }
    
    public function getInstalaciones_Id_Instalacion() {
        return $this->instalaciones_id_instalacion;
    }

    public function setInstalaciones_Id_Instalacion($instalaciones_id_instalacion) {
        $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
    }
    
    public function getEstados_avisos_id_estado_aviso() {
        return $this->estados_avisos_id_estado_aviso;
    }

    public function setEstados_avisos_id_estado_aviso($estados_avisos_id_estado_aviso) {
        $this->estados_avisos_id_estado_aviso = $estados_avisos_id_estado_aviso;
    }

    public function getVehiculos_id_vehiculo() {
        return $this->vehiculos_id_vehiculo;
    }

    public function setVehiculos_id_vehiculo($vehiculos_id_vehiculo) {
        $this->vehiculos_id_vehiculo = $vehiculos_id_vehiculo;
    }

    /*Fin de IDs*/
    
    
    
    /*Campos de la Tabla*/
    
    public function getNumero_aviso() {
        return $this->numero_aviso;
    }

    public function setNumero_aviso($numero_aviso) {
        $this->numero_aviso = $numero_aviso;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getTipo_aviso() {
        return $this->tipo_aviso;
    }

    public function setTipo_aviso($tipo_aviso) {
        $this->tipo_aviso = $tipo_aviso;
    }

    public function getFecha_carga() {
        return $this->fecha_carga;
    }

    public function setFecha_carga($fecha_carga) {
        $this->fecha_carga = $fecha_carga;
    }

    public function getFecha_asignacion() {
        return $this->fecha_asignacion;
    }

    public function setFecha_asignacion($fecha_asignacion) {
        $this->fecha_asignacion = $fecha_asignacion;
    }

    public function getFecha_valorizacion() {
        return $this->fecha_valorizacion;
    }

    public function setFecha_valorizacion($fecha_valorizacion) {
        $this->fecha_valorizacion = $fecha_valorizacion;
    }

    public function getFecha_finalizacion() {
        return $this->fecha_finalizacion;
    }

    public function setFecha_finalizacion($fecha_finalizacion) {
        $this->fecha_finalizacion = $fecha_finalizacion;
    }

    public function getSubot() {
        return $this->subot;
    }

    public function setSubot($subot) {
        $this->subot = $subot;
    }
    
    public function getSolicitante_id_solicitante() {
        return $this->solicitante_id_solicitante;
    }

    public function setSolicitante_id_solicitante($solicitante_id_solicitante) {
        $this->solicitante_id_solicitante = $solicitante_id_solicitante;
    }
    
    public function getPrioridad() {
        return $this->prioridad;
    }

    public function setPrioridad($prioridad) {
        $this->prioridad = $prioridad;
    }
    
    public function getCuenta_contrato() {
        return $this->cuenta_contrato;
    }

    public function setCuenta_contrato($cuenta_contrato) {
        $this->cuenta_contrato = $cuenta_contrato;
    }

    public function getServicios_id_servicio() {
        return $this->servicios_id_servicio;
    }

    public function setServicios_id_servicio($servicios_id_servicio) {
        $this->servicios_id_servicio = $servicios_id_servicio;
    }

    public function getNumero_documento() {
        return $this->numero_documento;
    }

    public function getFecha_ejecucion() {
        return $this->fecha_ejecucion;
    }

    public function getMes_facturacion() {
        return $this->mes_facturacion;
    }

    public function setNumero_documento($numero_documento) {
        $this->numero_documento = $numero_documento;
    }

    public function setFecha_ejecucion($fecha_ejecucion) {
        $this->fecha_ejecucion = $fecha_ejecucion;
    }

    public function setMes_facturacion($mes_facturacion) {
        $this->mes_facturacion = $mes_facturacion;
    }
    
    public function getClaseaviso() {
        return $this->claseaviso;
    }

    public function setClaseaviso($claseaviso) {
        $this->claseaviso = $claseaviso;
    }

    public function getFechacreadosap() {
        return $this->fechacreadosap;
    }

    public function setFechacreadosap($fechacreadosap) {
        $this->fechacreadosap = $fechacreadosap;
    }

    public function getFechaasignadosap() {
        return $this->fechaasignadosap;
    }

    public function setFechaasignadosap($fechaasignadosap) {
        $this->fechaasignadosap = $fechaasignadosap;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }
    
    public function getFecha_suspendido() {
        return $this->fecha_suspendido;
    }

    public function setFecha_suspendido($fecha_suspendido) {
        $this->fecha_suspendido = $fecha_suspendido;
    }

        /*Fin de Campos de la Tabla*/
    
    
    
    /*Objetos Relacionles*/
    
    public function getEstado_aviso() {
        return $this->estado_aviso;
    }

    public function setEstado_aviso(EstadoAviso $estado_aviso) {
        $this->estado_aviso = $estado_aviso;
    }

    public function getResponsable_persona() {
        return $this->responsable_persona;
    }

    public function setResponsable_persona(Persona $responsable_persona) {
        $this->responsable_persona = $responsable_persona;
    }

    public function getSupervisor_persona() {
        return $this->supervisor_persona;
    }

    public function setSupervisor_persona(Persona $supervisor_persona) {
        $this->supervisor_persona = $supervisor_persona;
    }

    public function getInstalacion() {
        return $this->instalacion;
    }

    public function setInstalacion(Instalacion $instalacion) {
        $this->instalacion = $instalacion;
    }

    public function getVehiculo() {
        return $this->vehiculo;
    }

    public function setVehiculo(Vehiculo $vehiculo) {
        $this->vehiculo = $vehiculo;
    }
    
    public function getBrigada() {
        return $this->brigada;
    }

    public function setBrigada(Brigada $brigada) {
        $this->brigada = $brigada;
    }

    public function getOrs_riat() {
        return $this->ors_riat;
    }

    public function setOrs_riat(OrsRiat $ors_riat) {
        $this->ors_riat = $ors_riat;
    }
    
    public function getSucursal() {
        return $this->sucursal;
    }

    public function setSucursal(Sucursal $sucursal) {
        $this->sucursal = $sucursal;
    }

    public function getDistribuidora() {
        return $this->distribuidora;
    }

    public function getServicio() {
        return $this->servicio;
    }

    public function setDistribuidora(Distribuidoras $distribuidora) {
        $this->distribuidora = $distribuidora;
    }

    public function setServicio(Servicio $servicio) {
        $this->servicio = $servicio;
    }

    public function getSolicitante() {
        return $this->solicitante;
    }

    public function setSolicitante(Solicitante $solicitante) {
        $this->solicitante = $solicitante;
    }
    
    public function getResultado_aviso_trifasico() {
        return $this->resultado_aviso_trifasico;
    }

    public function setResultado_aviso_trifasico(Resultado_aviso_trifasico $resultado_aviso_trifasico) {
        $this->resultado_aviso_trifasico = $resultado_aviso_trifasico;
    }


    /*Fin*/
    
    public function exchangeArray($data) {
        
        $this->id_aviso = (isset($data['id_aviso'])) ? $data['id_aviso'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        $this->responsable_personal_id_persona = (isset($data['responsable_personal_id_persona'])) ? $data['responsable_personal_id_persona'] : null;
        $this->supervisor_personal_id_persona = (isset($data['supervisor_personal_id_persona'])) ? $data['supervisor_personal_id_persona'] : null;
        $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
        $this->estados_avisos_id_estado_aviso = (isset($data['estados_avisos_id_estado_aviso'])) ? $data['estados_avisos_id_estado_aviso'] : null;
        $this->vehiculos_id_vehiculo = (isset($data['vehiculos_id_vehiculo'])) ? $data['vehiculos_id_vehiculo'] : null;
        
        $this->numero_aviso = (isset($data['numero_aviso'])) ? $data['numero_aviso'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
        $this->tipo_aviso = (isset($data['tipo_aviso'])) ? $data['tipo_aviso'] : null;
        $this->fecha_carga = (isset($data['fecha_carga'])) ? $data['fecha_carga'] : null;
        $this->fecha_asignacion = (isset($data['fecha_asignacion'])) ? $data['fecha_asignacion'] : null;
        $this->fecha_valorizacion = (isset($data['fecha_valorizacion'])) ? $data['fecha_valorizacion'] : null;
        $this->fecha_finalizacion = (isset($data['fecha_finalizacion'])) ? $data['fecha_finalizacion'] : null;
        $this->subot = (isset($data['subot'])) ? $data['subot'] : null;
        $this->solicitante_id_solicitante = (isset($data['solicitante_id_solicitante'])) ? $data['solicitante_id_solicitante'] : null;
        $this->prioridad = (isset($data['prioridad'])) ? $data['prioridad'] : null;
        $this->cuenta_contrato = (isset($data['cuenta_contrato'])) ? $data['cuenta_contrato'] : null;
        $this->servicios_id_servicio = (isset($data['servicios_id_servicio'])) ? $data['servicios_id_servicio'] : null;
        
        $this->numero_documento = (isset($data['numero_documento'])) ? $data['numero_documento'] : null;
        $this->fecha_ejecucion = (isset($data['fecha_ejecucion'])) ? $data['fecha_ejecucion'] : null;
        $this->mes_facturacion = (isset($data['mes_facturacion'])) ? $data['mes_facturacion'] : null;
        
        $this->claseaviso = (isset($data['claseaviso'])) ? $data['claseaviso'] : null;
        $this->fechacreadosap = (isset($data['fechacreadosap'])) ? $data['fechacreadosap'] : null;
        $this->fechaasignadosap = (isset($data['fechaasignadosap'])) ? $data['fechaasignadosap'] : null;
        $this->status = (isset($data['status'])) ? $data['status'] : null;
        $this->fecha_suspendido = (isset($data['fecha_suspendido'])) ? $data['fecha_suspendido'] : null; 
                
        /*Objeto para relacionar*/        
        $this->instalacion = new \Instalaciones\Model\Entity\Instalacion();
        $this->instalacion->setId_instalacion((isset($data['id_instalacion'])) ? $data['id_instalacion'] : null);
        $this->instalacion->setNum_instalacion((isset($data['num_instalacion'])) ? $data['num_instalacion'] : null);
        $this->instalacion->setUnidad_lectura((isset($data['unidad_lectura'])) ? $data['unidad_lectura'] : null);
        $this->instalacion->setTarifa((isset($data['tarifa'])) ? $data['tarifa'] : null);
        $this->instalacion->setNombre_cliente((isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : null);
        $this->instalacion->setDireccion1((isset($data['direccion1'])) ? $data['direccion1'] : null);
        $this->instalacion->setDireccion2((isset($data['direccion2'])) ? $data['direccion2'] : null);
        $this->instalacion->setNom_poblacion((isset($data['nom_poblacion'])) ? $data['nom_poblacion'] : null);        
        $this->instalacion->setNumero_poste_camara((isset($data['numero_poste_camara'])) ? $data['numero_poste_camara'] : null);
        $this->instalacion->setNumero_serie((isset($data['numero_serie'])) ? $data['numero_serie'] : null);
        $this->instalacion->setMarca_medidor((isset($data['marca_medidor'])) ? $data['marca_medidor'] : null);
        $this->instalacion->setPropiedad_medidor((isset($data['propiedad_medidor'])) ? $data['propiedad_medidor'] : null);
        $this->instalacion->setConstante((isset($data['constante'])) ? $data['constante'] : null);
        $this->instalacion->setTelefono((isset($data['telefono'])) ? $data['telefono'] : null);
        $this->instalacion->setDepartamento((isset($data['departamento'])) ? $data['departamento'] : null);
        $this->instalacion->setCe_emplazamiento((isset($data['ce_emplazamiento'])) ? $data['ce_emplazamiento'] : null);
        $this->instalacion->setPorcion((isset($data['porcion'])) ? $data['porcion'] : null);
        
        /*Objeto para relacionar*/
        $this->ors_riat = new \Riat\Model\Entity\OrsRiat();
        $this->ors_riat->setId_ors_riat((isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null);
        $this->ors_riat->setNum_form_riat((isset($data['num_form_riat'])) ? $data['num_form_riat'] : null);
        $this->ors_riat->setFecha_atencion_servicio((isset($data['fecha_atencion_servicio'])) ? $data['fecha_atencion_servicio'] : null);    
        //$this->ors_riat->setEstado_riat((isset($data['estado_riat'])) ? $data['estado_riat'] : null);
                    
        /*Objeto para relacionar*/
        $this->estado_aviso = new \Avisos\Model\Entity\EstadoAviso();
        $this->estado_aviso->setNombreEstadoAviso((isset($data['nombre_estado_aviso'])) ? $data['nombre_estado_aviso'] : null);
        $this->estado_aviso->setClase_color((isset($data['clase_color'])) ? $data['clase_color'] : null);
        
        $this->brigada = new \Brigadas\Model\Entity\Brigada();
        $this->brigada->setNombre_brigada((isset($data['nombre_brigada'])) ? $data['nombre_brigada'] : null);
        
        /*Objeto para relacionar con Personal*/
        $this->responsable_persona = new \Personal\Model\Entity\Persona;
        $this->responsable_persona->setNombres((isset($data['nombres'])) ? $data['nombres'] : null);
        $this->responsable_persona->setApellidos((isset($data['apellidos'])) ? $data['apellidos'] : null);
        
        /*Objeto para relacionar con Personal*/        
        $this->sucursal = new \Avisos\Model\Entity\Sucursal();
        $this->sucursal->setNombre_sucursal((isset($data['nombre_sucursal'])) ? $data['nombre_sucursal'] : null);
        
        $this->distribuidora = new \Instalaciones\Model\Entity\Distribuidoras();         
        $this->distribuidora->setNombre((isset($data['nombre'])) ? $data['nombre'] : null);
        
        $this->servicio = new \Servicios\Model\Entity\Servicio();
        $this->servicio->setId_servicio((isset($data['id_servicio'])) ? $data['id_servicio'] : null);
        $this->servicio->setNombre_servicio((isset($data['nombre_servicio'])) ? $data['nombre_servicio'] : null);
        
        $this->solicitante = new \Avisos\Model\Entity\Solicitante();
        $this->solicitante->setDepartamento_distri((isset($data['departamento_distri'])) ? $data['departamento_distri'] : null);
        
        /*Resultado Auditoria Con Nuevos Codigos*/
        $this->resultado_aviso_trifasico = new \Avisos\Model\Entity\ResultadoAuditoria();
        $this->resultado_aviso_trifasico->setResultado_trifasico_new((isset($data['resultado_trifasico_new'])) ? $data['resultado_trifasico_new'] : null);
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

