<?php

namespace Avisos\Model\Entity;

class AvisoAsignado {

    private $avisos_id_aviso;
    private $brigadas_id_brigada;

    public function __construct($avisos_id_aviso = null,$brigadas_id_brigada = null) {
      $this->avisos_id_aviso = $avisos_id_aviso;
      $this->brigadas_id_brigada = $brigadas_id_brigada;
    }

    public function getAvisos_id_aviso() {
        return $this->avisos_id_aviso;
    }

    public function setAvisos_id_aviso($avisos_id_aviso) {
        $this->avisos_id_aviso = $avisos_id_aviso;
    }

    public function getBrigadas_id_brigada() {
        return $this->brigadas_id_brigada;
    }

    public function setBrigadas_id_brigada($brigadas_id_brigada) {
        $this->brigadas_id_brigada = $brigadas_id_brigada;
    }
    
    public function exchangeArray($data) {
        
        $this->avisos_id_aviso = (isset($data['avisos_id_aviso'])) ? $data['avisos_id_aviso'] : null;
        $this->brigadas_id_brigada = (isset($data['brigadas_id_brigada'])) ? $data['brigadas_id_brigada'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

