<?php

namespace Avisos\Model\Entity;

class Bitacora {

    private $ID_BITACORA;
    private $GNR_FECHA;
    private $GNR_OFICINA;
    private $GNR_PROYECTO;
    private $BGD_ENCARGADO;
    private $BGD_AYUDANTE;
    private $BGD_INSTRUMENTO;
    private $MV_PATENTE;
    private $MV_KM_SALIDA;
    private $MV_KM_LLEGADA;
    private $HRS_HORA_SALIDA;
    private $HRS_HORA_LLEGADA;
    private $TDO_MASIVAS;
    private $TDO_RECLAMOS;
    private $TDO_PERITAJES;
    private $TDO_AUTOGEN;
    private $SE_INSPECCION_VISUAL;
    private $SE_VRF_SIN_CAMBIO;
    private $SE_VRF_CON_CAMBIO;
    private $SE_CAMBIO_DIRECTO;
    private $SE_LAB_SERVICIO;
    private $OSRV_MAN_MENOR;
    private $OSRV_LAB_TERCEROS;
    private $CCSRV_DEVOLUCION;
    private $CCSRV_REINSTALACION;
    private $CCSRV_FRUSTRADO;
    private $RSLT_SERVICIOS_SIN_CNR;
    private $RSLT_SERVICIOS_CON_CNR;
    private $RSLT_CNR_CONFIGURABLE;
    private $RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN;
    private $TCNR_MEDIDOR_FUERA_DE_RANGO;
    private $TCNR_OTRO;
    private $NRM_CNR_NORMALIZADOS;
    private $NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION;
    private $NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS;
    private $NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR;
    private $NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA;
    private $NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS;
    private $TVIPD_CERRADO_EN_2DA_VISITA;
    private $TVIPD_CERRADO_EN_1RA_VISITA;
    private $TVIPD_CLIENTE_NO_AUTORIZA;
    private $TVIPD_DOMICILIO_DESHABITADO;
    private $TVIPD_SITIO_ERIAZO;
    private $TVIPD_SIN_ENERGIA;
    private $TVIPD_NO_EXISTE_RESPONSABLE;
    private $TVIPD_NO_UBICADO;
    private $TVIPD_EMPALME_NO_EXISTE;
    private $TVIPD_CASA_CERRADA;
    private $DIAS_DESCONTAR;
    private $OBSERVACIONES;

    function __construct($ID_BITACORA = null, $GNR_FECHA = null, $GNR_OFICINA = null, $GNR_PROYECTO = null, $BGD_ENCARGADO = null, $BGD_AYUDANTE = null, $BGD_INSTRUMENTO = null, $MV_PATENTE = null, $MV_KM_SALIDA = null, $MV_KM_LLEGADA = null, $HRS_HORA_SALIDA = null, $HRS_HORA_LLEGADA = null, $TDO_MASIVAS = null, $TDO_RECLAMOS = null, $TDO_PERITAJES = null, $TDO_AUTOGEN = null, $SE_INSPECCION_VISUAL = null, $SE_VRF_SIN_CAMBIO = null, $SE_VRF_CON_CAMBIO = null, $SE_CAMBIO_DIRECTO = null, $SE_LAB_SERVICIO = null, $OSRV_MAN_MENOR = null, $OSRV_LAB_TERCEROS = null, $CCSRV_DEVOLUCION = null, $CCSRV_REINSTALACION = null, $CCSRV_FRUSTRADO = null, $RSLT_SERVICIOS_SIN_CNR = null, $RSLT_SERVICIOS_CON_CNR = null, $RSLT_CNR_CONFIGURABLE = null, $RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN = null, $TCNR_MEDIDOR_FUERA_DE_RANGO = null, $TCNR_OTRO = null, $NRM_CNR_NORMALIZADOS = null, $NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION = null, $NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS = null, $NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR = null, $NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA = null, $NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS = null, $TVIPD_CERRADO_EN_2DA_VISITA = null, $TVIPD_CERRADO_EN_1RA_VISITA = null, $TVIPD_CLIENTE_NO_AUTORIZA = null, $TVIPD_DOMICILIO_DESHABITADO = null, $TVIPD_SITIO_ERIAZO = null, $TVIPD_SIN_ENERGIA = null, $TVIPD_NO_EXISTE_RESPONSABLE = null, $TVIPD_NO_UBICADO = null, $TVIPD_EMPALME_NO_EXISTE = null, $TVIPD_CASA_CERRADA = null, $DIAS_DESCONTAR = null, $OBSERVACIONES = null) {
        $this->ID_BITACORA = $ID_BITACORA;
        $this->GNR_FECHA = $GNR_FECHA;
        $this->GNR_OFICINA = $GNR_OFICINA;
        $this->GNR_PROYECTO = $GNR_PROYECTO;
        $this->BGD_ENCARGADO = $BGD_ENCARGADO;
        $this->BGD_AYUDANTE = $BGD_AYUDANTE;
        $this->BGD_INSTRUMENTO = $BGD_INSTRUMENTO;
        $this->MV_PATENTE = $MV_PATENTE;
        $this->MV_KM_SALIDA = $MV_KM_SALIDA;
        $this->MV_KM_LLEGADA = $MV_KM_LLEGADA;
        $this->HRS_HORA_SALIDA = $HRS_HORA_SALIDA;
        $this->HRS_HORA_LLEGADA = $HRS_HORA_LLEGADA;
        $this->TDO_MASIVAS = $TDO_MASIVAS;
        $this->TDO_RECLAMOS = $TDO_RECLAMOS;
        $this->TDO_PERITAJES = $TDO_PERITAJES;
        $this->TDO_AUTOGEN = $TDO_AUTOGEN;
        $this->SE_INSPECCION_VISUAL = $SE_INSPECCION_VISUAL;
        $this->SE_VRF_SIN_CAMBIO = $SE_VRF_SIN_CAMBIO;
        $this->SE_VRF_CON_CAMBIO = $SE_VRF_CON_CAMBIO;
        $this->SE_CAMBIO_DIRECTO = $SE_CAMBIO_DIRECTO;
        $this->SE_LAB_SERVICIO = $SE_LAB_SERVICIO;
        $this->OSRV_MAN_MENOR = $OSRV_MAN_MENOR;
        $this->OSRV_LAB_TERCEROS = $OSRV_LAB_TERCEROS;
        $this->CCSRV_DEVOLUCION = $CCSRV_DEVOLUCION;
        $this->CCSRV_REINSTALACION = $CCSRV_REINSTALACION;
        $this->CCSRV_FRUSTRADO = $CCSRV_FRUSTRADO;
        $this->RSLT_SERVICIOS_SIN_CNR = $RSLT_SERVICIOS_SIN_CNR;
        $this->RSLT_SERVICIOS_CON_CNR = $RSLT_SERVICIOS_CON_CNR;
        $this->RSLT_CNR_CONFIGURABLE = $RSLT_CNR_CONFIGURABLE;
        $this->RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN = $RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN;
        $this->TCNR_MEDIDOR_FUERA_DE_RANGO = $TCNR_MEDIDOR_FUERA_DE_RANGO;
        $this->TCNR_OTRO = $TCNR_OTRO;
        $this->NRM_CNR_NORMALIZADOS = $NRM_CNR_NORMALIZADOS;
        $this->NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION = $NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION;
        $this->NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS = $NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS;
        $this->NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR = $NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR;
        $this->NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA = $NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA;
        $this->NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS = $NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS;
        $this->TVIPD_CERRADO_EN_2DA_VISITA = $TVIPD_CERRADO_EN_2DA_VISITA;
        $this->TVIPD_CERRADO_EN_1RA_VISITA = $TVIPD_CERRADO_EN_1RA_VISITA;
        $this->TVIPD_CLIENTE_NO_AUTORIZA = $TVIPD_CLIENTE_NO_AUTORIZA;
        $this->TVIPD_DOMICILIO_DESHABITADO = $TVIPD_DOMICILIO_DESHABITADO;
        $this->TVIPD_SITIO_ERIAZO = $TVIPD_SITIO_ERIAZO;
        $this->TVIPD_SIN_ENERGIA = $TVIPD_SIN_ENERGIA;
        $this->TVIPD_NO_EXISTE_RESPONSABLE = $TVIPD_NO_EXISTE_RESPONSABLE;
        $this->TVIPD_NO_UBICADO = $TVIPD_NO_UBICADO;
        $this->TVIPD_EMPALME_NO_EXISTE = $TVIPD_EMPALME_NO_EXISTE;
        $this->TVIPD_CASA_CERRADA = $TVIPD_CASA_CERRADA;
        $this->DIAS_DESCONTAR = $DIAS_DESCONTAR;
        $this->OBSERVACIONES = $OBSERVACIONES;
    }

    public function getID_BITACORA() {
        return $this->ID_BITACORA;
    }

    public function getGNR_FECHA() {
        return $this->GNR_FECHA;
    }

    public function getGNR_OFICINA() {
        return $this->GNR_OFICINA;
    }

    public function getGNR_PROYECTO() {
        return $this->GNR_PROYECTO;
    }

    public function getBGD_ENCARGADO() {
        return $this->BGD_ENCARGADO;
    }

    public function getBGD_AYUDANTE() {
        return $this->BGD_AYUDANTE;
    }

    public function getBGD_INSTRUMENTO() {
        return $this->BGD_INSTRUMENTO;
    }

    public function getMV_PATENTE() {
        return $this->MV_PATENTE;
    }

    public function getMV_KM_SALIDA() {
        return $this->MV_KM_SALIDA;
    }

    public function getMV_KM_LLEGADA() {
        return $this->MV_KM_LLEGADA;
    }

    public function getHRS_HORA_SALIDA() {
        return $this->HRS_HORA_SALIDA;
    }

    public function getHRS_HORA_LLEGADA() {
        return $this->HRS_HORA_LLEGADA;
    }

    public function getTDO_MASIVAS() {
        return $this->TDO_MASIVAS;
    }

    public function getTDO_RECLAMOS() {
        return $this->TDO_RECLAMOS;
    }

    public function getTDO_PERITAJES() {
        return $this->TDO_PERITAJES;
    }

    public function getTDO_AUTOGEN() {
        return $this->TDO_AUTOGEN;
    }

    public function getSE_INSPECCION_VISUAL() {
        return $this->SE_INSPECCION_VISUAL;
    }

    public function getSE_VRF_SIN_CAMBIO() {
        return $this->SE_VRF_SIN_CAMBIO;
    }

    public function getSE_VRF_CON_CAMBIO() {
        return $this->SE_VRF_CON_CAMBIO;
    }

    public function getSE_CAMBIO_DIRECTO() {
        return $this->SE_CAMBIO_DIRECTO;
    }

    public function getSE_LAB_SERVICIO() {
        return $this->SE_LAB_SERVICIO;
    }

    public function getOSRV_MAN_MENOR() {
        return $this->OSRV_MAN_MENOR;
    }

    public function getOSRV_LAB_TERCEROS() {
        return $this->OSRV_LAB_TERCEROS;
    }

    public function getCCSRV_DEVOLUCION() {
        return $this->CCSRV_DEVOLUCION;
    }

    public function getCCSRV_REINSTALACION() {
        return $this->CCSRV_REINSTALACION;
    }

    public function getCCSRV_FRUSTRADO() {
        return $this->CCSRV_FRUSTRADO;
    }

    public function getRSLT_SERVICIOS_SIN_CNR() {
        return $this->RSLT_SERVICIOS_SIN_CNR;
    }

    public function getRSLT_SERVICIOS_CON_CNR() {
        return $this->RSLT_SERVICIOS_CON_CNR;
    }

    public function getRSLT_CNR_CONFIGURABLE() {
        return $this->RSLT_CNR_CONFIGURABLE;
    }

    public function getRSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN() {
        return $this->RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN;
    }

    public function getTCNR_MEDIDOR_FUERA_DE_RANGO() {
        return $this->TCNR_MEDIDOR_FUERA_DE_RANGO;
    }

    public function getTCNR_OTRO() {
        return $this->TCNR_OTRO;
    }

    public function getNRM_CNR_NORMALIZADOS() {
        return $this->NRM_CNR_NORMALIZADOS;
    }

    public function getNRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION() {
        return $this->NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION;
    }

    public function getNRM_CNR_SN_NRM_CONDICIONES_INSEGURAS() {
        return $this->NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS;
    }

    public function getNRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR() {
        return $this->NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR;
    }

    public function getNRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA() {
        return $this->NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA;
    }

    public function getNRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS() {
        return $this->NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS;
    }

    public function getTVIPD_CERRADO_EN_2DA_VISITA() {
        return $this->TVIPD_CERRADO_EN_2DA_VISITA;
    }

    public function getTVIPD_CERRADO_EN_1RA_VISITA() {
        return $this->TVIPD_CERRADO_EN_1RA_VISITA;
    }

    public function getTVIPD_CLIENTE_NO_AUTORIZA() {
        return $this->TVIPD_CLIENTE_NO_AUTORIZA;
    }

    public function getTVIPD_DOMICILIO_DESHABITADO() {
        return $this->TVIPD_DOMICILIO_DESHABITADO;
    }

    public function getTVIPD_SITIO_ERIAZO() {
        return $this->TVIPD_SITIO_ERIAZO;
    }

    public function getTVIPD_SIN_ENERGIA() {
        return $this->TVIPD_SIN_ENERGIA;
    }

    public function getTVIPD_NO_EXISTE_RESPONSABLE() {
        return $this->TVIPD_NO_EXISTE_RESPONSABLE;
    }

    public function getTVIPD_NO_UBICADO() {
        return $this->TVIPD_NO_UBICADO;
    }

    public function getTVIPD_EMPALME_NO_EXISTE() {
        return $this->TVIPD_EMPALME_NO_EXISTE;
    }

    public function getTVIPD_CASA_CERRADA() {
        return $this->TVIPD_CASA_CERRADA;
    }

    public function getDIAS_DESCONTAR() {
        return $this->DIAS_DESCONTAR;
    }

    public function getOBSERVACIONES() {
        return $this->OBSERVACIONES;
    }

    public function setID_BITACORA($ID_BITACORA) {
        $this->ID_BITACORA = $ID_BITACORA;
    }

    public function setGNR_FECHA($GNR_FECHA) {
        $this->GNR_FECHA = $GNR_FECHA;
    }

    public function setGNR_OFICINA($GNR_OFICINA) {
        $this->GNR_OFICINA = $GNR_OFICINA;
    }

    public function setGNR_PROYECTO($GNR_PROYECTO) {
        $this->GNR_PROYECTO = $GNR_PROYECTO;
    }

    public function setBGD_ENCARGADO($BGD_ENCARGADO) {
        $this->BGD_ENCARGADO = $BGD_ENCARGADO;
    }

    public function setBGD_AYUDANTE($BGD_AYUDANTE) {
        $this->BGD_AYUDANTE = $BGD_AYUDANTE;
    }

    public function setBGD_INSTRUMENTO($BGD_INSTRUMENTO) {
        $this->BGD_INSTRUMENTO = $BGD_INSTRUMENTO;
    }

    public function setMV_PATENTE($MV_PATENTE) {
        $this->MV_PATENTE = $MV_PATENTE;
    }

    public function setMV_KM_SALIDA($MV_KM_SALIDA) {
        $this->MV_KM_SALIDA = $MV_KM_SALIDA;
    }

    public function setMV_KM_LLEGADA($MV_KM_LLEGADA) {
        $this->MV_KM_LLEGADA = $MV_KM_LLEGADA;
    }

    public function setHRS_HORA_SALIDA($HRS_HORA_SALIDA) {
        $this->HRS_HORA_SALIDA = $HRS_HORA_SALIDA;
    }

    public function setHRS_HORA_LLEGADA($HRS_HORA_LLEGADA) {
        $this->HRS_HORA_LLEGADA = $HRS_HORA_LLEGADA;
    }

    public function setTDO_MASIVAS($TDO_MASIVAS) {
        $this->TDO_MASIVAS = $TDO_MASIVAS;
    }

    public function setTDO_RECLAMOS($TDO_RECLAMOS) {
        $this->TDO_RECLAMOS = $TDO_RECLAMOS;
    }

    public function setTDO_PERITAJES($TDO_PERITAJES) {
        $this->TDO_PERITAJES = $TDO_PERITAJES;
    }

    public function setTDO_AUTOGEN($TDO_AUTOGEN) {
        $this->TDO_AUTOGEN = $TDO_AUTOGEN;
    }

    public function setSE_INSPECCION_VISUAL($SE_INSPECCION_VISUAL) {
        $this->SE_INSPECCION_VISUAL = $SE_INSPECCION_VISUAL;
    }

    public function setSE_VRF_SIN_CAMBIO($SE_VRF_SIN_CAMBIO) {
        $this->SE_VRF_SIN_CAMBIO = $SE_VRF_SIN_CAMBIO;
    }

    public function setSE_VRF_CON_CAMBIO($SE_VRF_CON_CAMBIO) {
        $this->SE_VRF_CON_CAMBIO = $SE_VRF_CON_CAMBIO;
    }

    public function setSE_CAMBIO_DIRECTO($SE_CAMBIO_DIRECTO) {
        $this->SE_CAMBIO_DIRECTO = $SE_CAMBIO_DIRECTO;
    }

    public function setSE_LAB_SERVICIO($SE_LAB_SERVICIO) {
        $this->SE_LAB_SERVICIO = $SE_LAB_SERVICIO;
    }

    public function setOSRV_MAN_MENOR($OSRV_MAN_MENOR) {
        $this->OSRV_MAN_MENOR = $OSRV_MAN_MENOR;
    }

    public function setOSRV_LAB_TERCEROS($OSRV_LAB_TERCEROS) {
        $this->OSRV_LAB_TERCEROS = $OSRV_LAB_TERCEROS;
    }

    public function setCCSRV_DEVOLUCION($CCSRV_DEVOLUCION) {
        $this->CCSRV_DEVOLUCION = $CCSRV_DEVOLUCION;
    }

    public function setCCSRV_REINSTALACION($CCSRV_REINSTALACION) {
        $this->CCSRV_REINSTALACION = $CCSRV_REINSTALACION;
    }

    public function setCCSRV_FRUSTRADO($CCSRV_FRUSTRADO) {
        $this->CCSRV_FRUSTRADO = $CCSRV_FRUSTRADO;
    }

    public function setRSLT_SERVICIOS_SIN_CNR($RSLT_SERVICIOS_SIN_CNR) {
        $this->RSLT_SERVICIOS_SIN_CNR = $RSLT_SERVICIOS_SIN_CNR;
    }

    public function setRSLT_SERVICIOS_CON_CNR($RSLT_SERVICIOS_CON_CNR) {
        $this->RSLT_SERVICIOS_CON_CNR = $RSLT_SERVICIOS_CON_CNR;
    }

    public function setRSLT_CNR_CONFIGURABLE($RSLT_CNR_CONFIGURABLE) {
        $this->RSLT_CNR_CONFIGURABLE = $RSLT_CNR_CONFIGURABLE;
    }

    public function setRSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN($RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN) {
        $this->RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN = $RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN;
    }

    public function setTCNR_MEDIDOR_FUERA_DE_RANGO($TCNR_MEDIDOR_FUERA_DE_RANGO) {
        $this->TCNR_MEDIDOR_FUERA_DE_RANGO = $TCNR_MEDIDOR_FUERA_DE_RANGO;
    }

    public function setTCNR_OTRO($TCNR_OTRO) {
        $this->TCNR_OTRO = $TCNR_OTRO;
    }

    public function setNRM_CNR_NORMALIZADOS($NRM_CNR_NORMALIZADOS) {
        $this->NRM_CNR_NORMALIZADOS = $NRM_CNR_NORMALIZADOS;
    }

    public function setNRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION($NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION) {
        $this->NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION = $NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION;
    }

    public function setNRM_CNR_SN_NRM_CONDICIONES_INSEGURAS($NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS) {
        $this->NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS = $NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS;
    }

    public function setNRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR($NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR) {
        $this->NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR = $NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR;
    }

    public function setNRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA($NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA) {
        $this->NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA = $NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA;
    }

    public function setNRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS($NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS) {
        $this->NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS = $NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS;
    }

    public function setTVIPD_CERRADO_EN_2DA_VISITA($TVIPD_CERRADO_EN_2DA_VISITA) {
        $this->TVIPD_CERRADO_EN_2DA_VISITA = $TVIPD_CERRADO_EN_2DA_VISITA;
    }

    public function setTVIPD_CERRADO_EN_1RA_VISITA($TVIPD_CERRADO_EN_1RA_VISITA) {
        $this->TVIPD_CERRADO_EN_1RA_VISITA = $TVIPD_CERRADO_EN_1RA_VISITA;
    }

    public function setTVIPD_CLIENTE_NO_AUTORIZA($TVIPD_CLIENTE_NO_AUTORIZA) {
        $this->TVIPD_CLIENTE_NO_AUTORIZA = $TVIPD_CLIENTE_NO_AUTORIZA;
    }

    public function setTVIPD_DOMICILIO_DESHABITADO($TVIPD_DOMICILIO_DESHABITADO) {
        $this->TVIPD_DOMICILIO_DESHABITADO = $TVIPD_DOMICILIO_DESHABITADO;
    }

    public function setTVIPD_SITIO_ERIAZO($TVIPD_SITIO_ERIAZO) {
        $this->TVIPD_SITIO_ERIAZO = $TVIPD_SITIO_ERIAZO;
    }

    public function setTVIPD_SIN_ENERGIA($TVIPD_SIN_ENERGIA) {
        $this->TVIPD_SIN_ENERGIA = $TVIPD_SIN_ENERGIA;
    }

    public function setTVIPD_NO_EXISTE_RESPONSABLE($TVIPD_NO_EXISTE_RESPONSABLE) {
        $this->TVIPD_NO_EXISTE_RESPONSABLE = $TVIPD_NO_EXISTE_RESPONSABLE;
    }

    public function setTVIPD_NO_UBICADO($TVIPD_NO_UBICADO) {
        $this->TVIPD_NO_UBICADO = $TVIPD_NO_UBICADO;
    }

    public function setTVIPD_EMPALME_NO_EXISTE($TVIPD_EMPALME_NO_EXISTE) {
        $this->TVIPD_EMPALME_NO_EXISTE = $TVIPD_EMPALME_NO_EXISTE;
    }

    public function setTVIPD_CASA_CERRADA($TVIPD_CASA_CERRADA) {
        $this->TVIPD_CASA_CERRADA = $TVIPD_CASA_CERRADA;
    }

    public function setDIAS_DESCONTAR($DIAS_DESCONTAR) {
        $this->DIAS_DESCONTAR = $DIAS_DESCONTAR;
    }

    public function setOBSERVACIONES($OBSERVACIONES) {
        $this->OBSERVACIONES = $OBSERVACIONES;
    }

    public function exchangeArray($data) {

        $this->ID_BITACORA = (isset($data['ID_BITACORA'])) ? $data['ID_BITACORA'] : null;
        $this->GNR_FECHA = (isset($data['GNR_FECHA'])) ? $data['GNR_FECHA'] : null;
        $this->GNR_OFICINA = (isset($data['GNR_OFICINA'])) ? $data['GNR_OFICINA'] : null;
        $this->GNR_PROYECTO = (isset($data['GNR_PROYECTO'])) ? $data['GNR_PROYECTO'] : null;
        $this->BGD_ENCARGADO = (isset($data['BGD_ENCARGADO'])) ? $data['BGD_ENCARGADO'] : null;
        $this->BGD_AYUDANTE = (isset($data['BGD_AYUDANTE'])) ? $data['BGD_AYUDANTE'] : null;
        $this->BGD_INSTRUMENTO = (isset($data['BGD_INSTRUMENTO'])) ? $data['BGD_INSTRUMENTO'] : null;
        $this->MV_PATENTE = (isset($data['MV_PATENTE'])) ? $data['MV_PATENTE'] : null;
        $this->MV_KM_SALIDA = (isset($data['MV_KM_SALIDA'])) ? $data['MV_KM_SALIDA'] : null;
        $this->MV_KM_LLEGADA = (isset($data['MV_KM_LLEGADA'])) ? $data['MV_KM_LLEGADA'] : null;
        $this->HRS_HORA_SALIDA = (isset($data['HRS_HORA_SALIDA'])) ? $data['HRS_HORA_SALIDA'] : null;
        $this->HRS_HORA_LLEGADA = (isset($data['HRS_HORA_LLEGADA'])) ? $data['HRS_HORA_LLEGADA'] : null;
        $this->TDO_MASIVAS = (isset($data['TDO_MASIVAS'])) ? $data['TDO_MASIVAS'] : null;
        $this->TDO_RECLAMOS = (isset($data['TDO_RECLAMOS'])) ? $data['TDO_RECLAMOS'] : null;
        $this->TDO_PERITAJES = (isset($data['TDO_PERITAJES'])) ? $data['TDO_PERITAJES'] : null;
        $this->TDO_AUTOGEN = (isset($data['TDO_AUTOGEN'])) ? $data['TDO_AUTOGEN'] : null;
        $this->SE_INSPECCION_VISUAL = (isset($data['SE_INSPECCION_VISUAL'])) ? $data['SE_INSPECCION_VISUAL'] : null;
        $this->SE_VRF_SIN_CAMBIO = (isset($data['SE_VRF_SIN_CAMBIO'])) ? $data['SE_VRF_SIN_CAMBIO'] : null;
        $this->SE_VRF_CON_CAMBIO = (isset($data['SE_VRF_CON_CAMBIO'])) ? $data['SE_VRF_CON_CAMBIO'] : null;
        $this->SE_CAMBIO_DIRECTO = (isset($data['SE_CAMBIO_DIRECTO'])) ? $data['SE_CAMBIO_DIRECTO'] : null;
        $this->SE_LAB_SERVICIO = (isset($data['SE_LAB_SERVICIO'])) ? $data['SE_LAB_SERVICIO'] : null;
        $this->OSRV_MAN_MENOR = (isset($data['OSRV_MAN_MENOR'])) ? $data['OSRV_MAN_MENOR'] : null;
        $this->OSRV_LAB_TERCEROS = (isset($data['OSRV_LAB_TERCEROS'])) ? $data['OSRV_LAB_TERCEROS'] : null;
        $this->CCSRV_DEVOLUCION = (isset($data['CCSRV_DEVOLUCION'])) ? $data['CCSRV_DEVOLUCION'] : null;
        $this->CCSRV_REINSTALACION = (isset($data['CCSRV_REINSTALACION'])) ? $data['CCSRV_REINSTALACION'] : null;
        $this->CCSRV_FRUSTRADO = (isset($data['CCSRV_FRUSTRADO'])) ? $data['CCSRV_FRUSTRADO'] : null;
        $this->RSLT_SERVICIOS_SIN_CNR = (isset($data['RSLT_SERVICIOS_SIN_CNR'])) ? $data['RSLT_SERVICIOS_SIN_CNR'] : null;
        $this->RSLT_SERVICIOS_CON_CNR = (isset($data['RSLT_SERVICIOS_CON_CNR'])) ? $data['RSLT_SERVICIOS_CON_CNR'] : null;
        $this->RSLT_CNR_CONFIGURABLE = (isset($data['RSLT_CNR_CONFIGURABLE'])) ? $data['RSLT_CNR_CONFIGURABLE'] : null;
        $this->RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN = (isset($data['RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN'])) ? $data['RSLT_SERVICIOS_CON_CNR_EN_ORDENES_AUTOGEN'] : null;
        $this->TCNR_MEDIDOR_FUERA_DE_RANGO = (isset($data['TCNR_MEDIDOR_FUERA_DE_RANGO'])) ? $data['TCNR_MEDIDOR_FUERA_DE_RANGO'] : null;
        $this->TCNR_OTRO = (isset($data['TCNR_OTRO'])) ? $data['TCNR_OTRO'] : null;
        $this->NRM_CNR_NORMALIZADOS = (isset($data['NRM_CNR_NORMALIZADOS'])) ? $data['NRM_CNR_NORMALIZADOS'] : null;
        $this->NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION = (isset($data['NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION'])) ? $data['NRM_CNR_SN_NRM_CLIENTE_NO_AUTORIZA_NORMALIZACION'] : null;
        $this->NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS = (isset($data['NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS'])) ? $data['NRM_CNR_SN_NRM_CONDICIONES_INSEGURAS'] : null;
        $this->NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR = (isset($data['NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR'])) ? $data['NRM_CNR_SN_NRM_REQUIERE_MANTENIMIENTO_MAYOR'] : null;
        $this->NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA = (isset($data['NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA'])) ? $data['NRM_CNR_SN_NRM_COLGADO_DE_ACOMETIDA'] : null;
        $this->NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS = (isset($data['NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS'])) ? $data['NRM_CNR_VALIDACION_CNR_DETECTADOS_VS_CNR_MANEJADOS'] : null;
        $this->TVIPD_CERRADO_EN_2DA_VISITA = (isset($data['TVIPD_CERRADO_EN_2DA_VISITA'])) ? $data['TVIPD_CERRADO_EN_2DA_VISITA'] : null;
        $this->TVIPD_CERRADO_EN_1RA_VISITA = (isset($data['TVIPD_CERRADO_EN_1RA_VISITA'])) ? $data['TVIPD_CERRADO_EN_1RA_VISITA'] : null;
        $this->TVIPD_CLIENTE_NO_AUTORIZA = (isset($data['TVIPD_CLIENTE_NO_AUTORIZA'])) ? $data['TVIPD_CLIENTE_NO_AUTORIZA'] : null;
        $this->TVIPD_DOMICILIO_DESHABITADO = (isset($data['TVIPD_DOMICILIO_DESHABITADO'])) ? $data['TVIPD_DOMICILIO_DESHABITADO'] : null;
        $this->TVIPD_SITIO_ERIAZO = (isset($data['TVIPD_SITIO_ERIAZO'])) ? $data['TVIPD_SITIO_ERIAZO'] : null;
        $this->TVIPD_SIN_ENERGIA = (isset($data['TVIPD_SIN_ENERGIA'])) ? $data['TVIPD_SIN_ENERGIA'] : null;
        $this->TVIPD_NO_EXISTE_RESPONSABLE = (isset($data['TVIPD_NO_EXISTE_RESPONSABLE'])) ? $data['TVIPD_NO_EXISTE_RESPONSABLE'] : null;
        $this->TVIPD_NO_UBICADO = (isset($data['TVIPD_NO_UBICADO'])) ? $data['TVIPD_NO_UBICADO'] : null;
        $this->TVIPD_EMPALME_NO_EXISTE = (isset($data['TVIPD_EMPALME_NO_EXISTE'])) ? $data['TVIPD_EMPALME_NO_EXISTE'] : null;
        $this->TVIPD_CASA_CERRADA = (isset($data['TVIPD_CASA_CERRADA'])) ? $data['TVIPD_CASA_CERRADA'] : null;
        $this->DIAS_DESCONTAR = (isset($data['DIAS_DESCONTAR'])) ? $data['DIAS_DESCONTAR'] : null;
        $this->OBSERVACIONES = (isset($data['OBSERVACIONES'])) ? $data['OBSERVACIONES'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

