<?php

namespace Avisos\Model\Entity;

class Comuna {

    private $id_comuna;
    private $provincias_id_provincia;
    private $nombre_comuna;
    private $cod_comuna;

    public function __construct(
    $id_comuna = null, $provincias_id_provincia = null, $nombre_comuna = null, $cod_comuna = null
    ) {
        $this->id_comuna = $id_comuna;
        $this->provincias_id_provincia = $provincias_id_provincia;
        $this->nombre_comuna = $nombre_comuna;
        $this->cod_comuna = $cod_comuna;
    }

    public function getId_comuna() {
        return $this->id_comuna;
    }

    public function setId_comuna($id_comuna) {
        $this->id_comuna = $id_comuna;
    }

    public function getProvincias_id_provincia() {
        return $this->provincias_id_provincia;
    }

    public function setProvincias_id_provincia($provincias_id_provincia) {
        $this->provincias_id_provincia = $provincias_id_provincia;
    }

    public function getNombre_comuna() {
        return $this->nombre_comuna;
    }

    public function setNombre_comuna($nombre_comuna) {
        $this->nombre_comuna = $nombre_comuna;
    }

    public function getCod_comuna() {
        return $this->cod_comuna;
    }

    public function setCod_comuna($cod_comuna) {
        $this->cod_comuna = $cod_comuna;
    }    
    
    public function exchangeArray($data) {
        $this->id_comuna = (isset($data['id_comuna'])) ? $data['id_comuna'] : null;
        $this->provincias_id_provincia = (isset($data['provincias_id_provincia'])) ? $data['provincias_id_provincia'] : null;
        $this->nombre_comuna = (isset($data['nombre_comuna'])) ? $data['nombre_comuna'] : null;
        $this->cod_comuna = (isset($data['cod_comuna'])) ? $data['cod_comuna'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

