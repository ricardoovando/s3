<?php

namespace Avisos\Model\Entity;

class DetalleFacturacionSucursal {
    
    private $sucursales_id_sucursal;
    private $num_instalacion;
    private $numero_aviso;
    private $fecha_atencion_servicio;
    private $nombre_servicio;
    private $cantidad;
    private $precio_uf;

    function __construct($sucursales_id_sucursal = null, $num_instalacion = null, $numero_aviso = null, $fecha_atencion_servicio = null, $nombre_servicio = null, $cantidad = null, $precio_uf = null) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
        $this->num_instalacion = $num_instalacion;
        $this->numero_aviso = $numero_aviso;
        $this->fecha_atencion_servicio = $fecha_atencion_servicio;
        $this->nombre_servicio = $nombre_servicio;
        $this->cantidad = $cantidad;
        $this->precio_uf = $precio_uf;
      }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function getNum_instalacion() {
        return $this->num_instalacion;
    }

    public function getNumero_aviso() {
        return $this->numero_aviso;
    }

    public function getFecha_atencion_servicio() {
        return $this->fecha_atencion_servicio;
    }

    public function getNombre_servicio() {
        return $this->nombre_servicio;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function getPrecio_uf() {
        return $this->precio_uf;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function setNum_instalacion($num_instalacion) {
        $this->num_instalacion = $num_instalacion;
    }

    public function setNumero_aviso($numero_aviso) {
        $this->numero_aviso = $numero_aviso;
    }

    public function setFecha_atencion_servicio($fecha_atencion_servicio) {
        $this->fecha_atencion_servicio = $fecha_atencion_servicio;
    }

    public function setNombre_servicio($nombre_servicio) {
        $this->nombre_servicio = $nombre_servicio;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function setPrecio_uf($precio_uf) {
        $this->precio_uf = $precio_uf;
    }

    public function exchangeArray($data) {
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        $this->num_instalacion = (isset($data['num_instalacion'])) ? $data['num_instalacion'] : null;
        $this->numero_aviso = (isset($data['numero_aviso'])) ? $data['numero_aviso'] : null;
        $this->fecha_atencion_servicio = (isset($data['fecha_atencion_servicio'])) ? $data['fecha_atencion_servicio'] : null;
        $this->nombre_servicio = (isset($data['nombre_servicio'])) ? $data['nombre_servicio'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->precio_uf = (isset($data['precio_uf'])) ? $data['precio_uf'] : null;
      }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

