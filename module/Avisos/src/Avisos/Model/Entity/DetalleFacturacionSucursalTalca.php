<?php

namespace Avisos\Model\Entity;

class DetalleFacturacionSucursalTalca {

    private $empresa;
    private $proveedor;
    private $oficina;
    private $procedencia;
    private $solicitante;
    private $proceso;
    private $ticket;
    private $instalacion;
    private $fecha_solicitud;
    private $fecha_ejecucion;
    private $accion;
    private $cantidad;
    private $precio_unidad;
    private $total;
    private $cta;
    private $depto;
    private $cod_activ;
    private $sucursales_id_sucursal;

    function __construct($empresa = null, $proveedor = null, $oficina = null, $procedencia = null, $solicitante = null, $proceso = null, $ticket = null, $instalacion = null, $fecha_solicitud = null, $fecha_ejecucion = null, 
            $accion = null, $cantidad = null, $precio_unidad = null, $total = null, 
            $cta = null, $depto = null, $cod_activ = null, $sucursales_id_sucursal = null) {
        $this->empresa = $empresa;
        $this->proveedor = $proveedor;
        $this->oficina = $oficina;
        $this->procedencia = $procedencia;
        $this->solicitante = $solicitante;
        $this->proceso = $proceso;
        $this->ticket = $ticket;
        $this->instalacion = $instalacion;
        $this->fecha_solicitud = $fecha_solicitud;
        $this->fecha_ejecucion = $fecha_ejecucion;
        $this->accion = $accion;
        $this->cantidad = $cantidad;
        $this->precio_unidad = $precio_unidad;
        $this->total = $total;
        $this->cta = $cta;
        $this->depto = $depto;
        $this->cod_activ = $cod_activ;
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getEmpresa() {
        return $this->empresa;
    }

    public function getProveedor() {
        return $this->proveedor;
    }

    public function getOficina() {
        return $this->oficina;
    }

    public function getProcedencia() {
        return $this->procedencia;
    }

    public function getSolicitante() {
        return $this->solicitante;
    }

    public function getProceso() {
        return $this->proceso;
    }

    public function getTicket() {
        return $this->ticket;
    }

    public function getInstalacion() {
        return $this->instalacion;
    }

    public function getFecha_solicitud() {
        return $this->fecha_solicitud;
    }

    public function getFecha_ejecucion() {
        return $this->fecha_ejecucion;
    }

    public function getAccion() {
        return $this->accion;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function getPrecio_unidad() {
        return $this->precio_unidad;
    }

    public function getTotal() {
        return $this->total;
    }

    public function getCta() {
        return $this->cta;
    }

    public function getDepto() {
        return $this->depto;
    }

    public function getCod_activ() {
        return $this->cod_activ;
    }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    public function setProveedor($proveedor) {
        $this->proveedor = $proveedor;
    }

    public function setOficina($oficina) {
        $this->oficina = $oficina;
    }

    public function setProcedencia($procedencia) {
        $this->procedencia = $procedencia;
    }

    public function setSolicitante($solicitante) {
        $this->solicitante = $solicitante;
    }

    public function setProceso($proceso) {
        $this->proceso = $proceso;
    }

    public function setTicket($ticket) {
        $this->ticket = $ticket;
    }

    public function setInstalacion($instalacion) {
        $this->instalacion = $instalacion;
    }

    public function setFecha_solicitud($fecha_solicitud) {
        $this->fecha_solicitud = $fecha_solicitud;
    }

    public function setFecha_ejecucion($fecha_ejecucion) {
        $this->fecha_ejecucion = $fecha_ejecucion;
    }

    public function setAccion($accion) {
        $this->accion = $accion;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function setPrecio_unidad($precio_unidad) {
        $this->precio_unidad = $precio_unidad;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function setCta($cta) {
        $this->cta = $cta;
    }

    public function setDepto($depto) {
        $this->depto = $depto;
    }

    public function setCod_activ($cod_activ) {
        $this->cod_activ = $cod_activ;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function exchangeArray($data) {
        $this->empresa = (isset($data['empresa'])) ? $data['empresa'] : null;
        $this->proveedor = (isset($data['proveedor'])) ? $data['proveedor'] : null;
        $this->oficina = (isset($data['oficina'])) ? $data['oficina'] : null;
        $this->procedencia = (isset($data['procedencia'])) ? $data['procedencia'] : null;
        $this->solicitante = (isset($data['solicitante'])) ? $data['solicitante'] : null;
        $this->proceso = (isset($data['proceso'])) ? $data['proceso'] : null;
        $this->ticket = (isset($data['ticket'])) ? $data['ticket'] : null;
        $this->instalacion = (isset($data['instalacion'])) ? $data['instalacion'] : null;
        $this->fecha_solicitud = (isset($data['fecha_solicitud'])) ? $data['fecha_solicitud'] : null;
        $this->fecha_ejecucion = (isset($data['fecha_ejecucion'])) ? $data['fecha_ejecucion'] : null;
        $this->accion = (isset($data['accion'])) ? $data['accion'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;
        $this->precio_unidad = (isset($data['precio_unidad'])) ? $data['precio_unidad'] : null;
        $this->total = (isset($data['total'])) ? $data['total'] : null;
        $this->cta = (isset($data['cta'])) ? $data['cta'] : null;
        $this->depto = (isset($data['depto'])) ? $data['depto'] : null;
        $this->cod_activ = (isset($data['cod_activ'])) ? $data['cod_activ'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
