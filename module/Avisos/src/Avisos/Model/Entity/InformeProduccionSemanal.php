<?php

namespace Avisos\Model\Entity;

class InformeProduccionSemanal {

    private $estado_aviso;
    private $estado_digitacion;
    private $tecnico;
    private $fecha_carga;
    private $fecha_ejecucion;
    private $digitador;
    private $fecha_digitacion;
    private $id_sucursal;
    private $numsolicitudtri;
    private $mesfacturacion;
    private $zonal;
    private $oficina;
    private $distribuidoras;
    private $estadosolicitud;
    private $fechaejecucion;
    private $num_form_riat;
    private $resultadoauditoria;
    private $constante_facturacion;
    private $constante_informada;
    private $aviso;
    private $actividad;
    private $detalle_actividad;
    private $total_facturacion;
    private $numservicio;
    private $cliente;
    private $direccion;
    private $comuna;
    private $tarifa;
    private $tipoempalme;
    private $circuito;
    private $conexion;
    private $medida;
    private $numeroserie;
    private $marca;
    private $activadejada;
    private $numeroseriereac;
    private $marcareac;
    private $reactivadejada;
    private $comentarios;
    private $cambiomedidorresu;
    private $seriecambio;
    private $marcacambio;
    private $modelocambio;
    private $propiedadcambio;

    function __construct($estado_aviso = null,$estado_digitacion = null,$tecnico = null,$fecha_carga = null,$fecha_ejecucion = null,
    $digitador = null,$fecha_digitacion = null,$id_sucursal = null, $numsolicitudtri = null, $mesfacturacion = null, $zonal = null, 
            $oficina = null, $distribuidoras = null, $estadosolicitud = null, $fechaejecucion = null,
            $num_form_riat = null,$resultadoauditoria = null,$constante_facturacion = null,
            $constante_informada = null,$aviso = null,$actividad = null,$detalle_actividad = null,$total_facturacion = null,
            $numservicio = null,$cliente = null, $direccion = null, $comuna = null, $tarifa = null, 
            $tipoempalme = null, $circuito = null, $conexion = null, $medida = null, 
            $numeroserie = null, $marca = null, $activadejada = null, $numeroseriereac = null, 
            $marcareac = null, $reactivadejada = null, $comentarios = null, 
            $cambiomedidorresu = null, $seriecambio = null, $marcacambio = null, 
            $modelocambio = null, $propiedadcambio = null) {
        
        $this->estado_aviso = $estado_aviso;
        $this->estado_digitacion = $estado_digitacion;
        $this->tecnico = $tecnico;
        $this->fecha_carga = $fecha_carga;
        $this->fecha_ejecucion = $fecha_ejecucion;
        $this->digitador = $digitador;
        $this->fecha_digitacion = $fecha_digitacion;   
        $this->id_sucursal = $id_sucursal;
        $this->numsolicitudtri = $numsolicitudtri;
        $this->mesfacturacion = $mesfacturacion;
        $this->zonal = $zonal;
        $this->oficina = $oficina;
        $this->distribuidoras = $distribuidoras;
        $this->estadosolicitud = $estadosolicitud;
        $this->fechaejecucion = $fechaejecucion;
        $this->num_form_riat = $num_form_riat;
        $this->resultadoauditoria = $resultadoauditoria;
        $this->constante_facturacion = $constante_facturacion; 
        $this->constante_informada = $constante_informada;
        $this->aviso = $aviso;
        $this->actividad = $actividad;
        $this->detalle_actividad = $detalle_actividad;
        $this->total_facturacion = $total_facturacion;
        $this->numservicio = $numservicio;
        $this->cliente = $cliente;
        $this->direccion = $direccion;
        $this->comuna = $comuna;
        $this->tarifa = $tarifa;
        $this->tipoempalme = $tipoempalme;
        $this->circuito = $circuito;
        $this->conexion = $conexion;
        $this->medida = $medida;
        $this->numeroserie = $numeroserie;
        $this->marca = $marca;
        $this->activadejada = $activadejada;
        $this->numeroseriereac = $numeroseriereac;
        $this->marcareac = $marcareac;
        $this->reactivadejada = $reactivadejada;
        $this->comentarios = $comentarios;
        $this->cambiomedidorresu = $cambiomedidorresu;
        $this->seriecambio = $seriecambio;
        $this->marcacambio = $marcacambio;
        $this->modelocambio = $modelocambio;
        $this->propiedadcambio = $propiedadcambio;
    }
    
    public function getEstado_aviso() {
        return $this->estado_aviso;
    }

    public function getEstado_digitacion() {
        return $this->estado_digitacion;
    }

    public function getTecnico() {
        return $this->tecnico;
    }

    public function getFecha_carga() {
        return $this->fecha_carga;
    }

    public function getFecha_ejecucion() {
        return $this->fecha_ejecucion;
    }

    public function getDigitador() {
        return $this->digitador;
    }

    public function getFecha_digitacion() {
        return $this->fecha_digitacion;
    }

    public function getConstante_informada() {
        return $this->constante_informada;
    }

    public function getActividad() {
        return $this->actividad;
    }

    public function getDetalle_actividad() {
        return $this->detalle_actividad;
    }

    public function getTotal_facturacion() {
        return $this->total_facturacion;
    }

            
    public function getId_sucursal() {
        return $this->id_sucursal;
    }

    public function getNumsolicitudtri() {
        return $this->numsolicitudtri;
    }

    public function getMesfacturacion() {
        return $this->mesfacturacion;
    }

    public function getZonal() {
        return $this->zonal;
    }

    public function getOficina() {
        return $this->oficina;
    }

    public function getDistribuidoras() {
        return $this->distribuidoras;
    }

    public function getEstadosolicitud() {
        return $this->estadosolicitud;
    }

    public function getFechaejecucion() {
        return $this->fechaejecucion;
    }

    public function getNum_form_riat() {
        return $this->num_form_riat;
    }

    public function getResultadoauditoria() {
        return $this->resultadoauditoria;
    }

    public function getConstante_facturacion() {
        return $this->constante_facturacion;
    }
    
    public function getAviso() {
        return $this->aviso;
    }

    public function getNumservicio() {
        return $this->numservicio;
    }

    public function getCliente() {
        return $this->cliente;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function getComuna() {
        return $this->comuna;
    }

    public function getTarifa() {
        return $this->tarifa;
    }

    public function getTipoempalme() {
        return $this->tipoempalme;
    }

    public function getCircuito() {
        return $this->circuito;
    }

    public function getConexion() {
        return $this->conexion;
    }

    public function getMedida() {
        return $this->medida;
    }

    public function getNumeroserie() {
        return $this->numeroserie;
    }

    public function getMarca() {
        return $this->marca;
    }

    public function getActivadejada() {
        return $this->activadejada;
    }

    public function getNumeroseriereac() {
        return $this->numeroseriereac;
    }

    public function getMarcareac() {
        return $this->marcareac;
    }

    public function getReactivadejada() {
        return $this->reactivadejada;
    }

    public function getComentarios() {
        return $this->comentarios;
    }

    public function getCambiomedidorresu() {
        return $this->cambiomedidorresu;
    }

    public function getSeriecambio() {
        return $this->seriecambio;
    }

    public function getMarcacambio() {
        return $this->marcacambio;
    }

    public function getModelocambio() {
        return $this->modelocambio;
    }

    public function getPropiedadcambio() {
        return $this->propiedadcambio;
    }
    
    
    public function setEstado_aviso($estado_aviso) {
        $this->estado_aviso = $estado_aviso;
    }

    public function setEstado_digitacion($estado_digitacion) {
        $this->estado_digitacion = $estado_digitacion;
    }

    public function setTecnico($tecnico) {
        $this->tecnico = $tecnico;
    }

    public function setFecha_carga($fecha_carga) {
        $this->fecha_carga = $fecha_carga;
    }

    public function setFecha_ejecucion($fecha_ejecucion) {
        $this->fecha_ejecucion = $fecha_ejecucion;
    }

    public function setDigitador($digitador) {
        $this->digitador = $digitador;
    }

    public function setFecha_digitacion($fecha_digitacion) {
        $this->fecha_digitacion = $fecha_digitacion;
    }

    public function setConstante_informada($constante_informada) {
        $this->constante_informada = $constante_informada;
    }

    public function setActividad($actividad) {
        $this->actividad = $actividad;
    }

    public function setDetalle_actividad($detalle_actividad) {
        $this->detalle_actividad = $detalle_actividad;
    }

    public function setTotal_facturacion($total_facturacion) {
        $this->total_facturacion = $total_facturacion;
    }
        
    public function setId_sucursal($id_sucursal) {
        $this->id_sucursal = $id_sucursal;
    }

    public function setNumsolicitudtri($numsolicitudtri) {
        $this->numsolicitudtri = $numsolicitudtri;
    }

    public function setMesfacturacion($mesfacturacion) {
        $this->mesfacturacion = $mesfacturacion;
    }

    public function setZonal($zonal) {
        $this->zonal = $zonal;
    }

    public function setOficina($oficina) {
        $this->oficina = $oficina;
    }

    public function setDistribuidoras($distribuidoras) {
        $this->distribuidoras = $distribuidoras;
    }

    public function setEstadosolicitud($estadosolicitud) {
        $this->estadosolicitud = $estadosolicitud;
    }

    public function setFechaejecucion($fechaejecucion) {
        $this->fechaejecucion = $fechaejecucion;
    }

    public function setNum_form_riat($num_form_riat) {
        $this->num_form_riat = $num_form_riat;
    }

    public function setResultadoauditoria($resultadoauditoria) {
        $this->resultadoauditoria = $resultadoauditoria;
    }

    public function setConstante_facturacion($constante_facturacion) {
        $this->constante_facturacion = $constante_facturacion;
    }
        
    public function setAviso($aviso) {
        $this->aviso = $aviso;
    }

    public function setNumservicio($numservicio) {
        $this->numservicio = $numservicio;
    }

    public function setCliente($cliente) {
        $this->cliente = $cliente;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function setComuna($comuna) {
        $this->comuna = $comuna;
    }

    public function setTarifa($tarifa) {
        $this->tarifa = $tarifa;
    }

    public function setTipoempalme($tipoempalme) {
        $this->tipoempalme = $tipoempalme;
    }

    public function setCircuito($circuito) {
        $this->circuito = $circuito;
    }

    public function setConexion($conexion) {
        $this->conexion = $conexion;
    }

    public function setMedida($medida) {
        $this->medida = $medida;
    }

    public function setNumeroserie($numeroserie) {
        $this->numeroserie = $numeroserie;
    }

    public function setMarca($marca) {
        $this->marca = $marca;
    }

    public function setActivadejada($activadejada) {
        $this->activadejada = $activadejada;
    }

    public function setNumeroseriereac($numeroseriereac) {
        $this->numeroseriereac = $numeroseriereac;
    }

    public function setMarcareac($marcareac) {
        $this->marcareac = $marcareac;
    }

    public function setReactivadejada($reactivadejada) {
        $this->reactivadejada = $reactivadejada;
    }

    public function setComentarios($comentarios) {
        $this->comentarios = $comentarios;
    }

    public function setCambiomedidorresu($cambiomedidorresu) {
        $this->cambiomedidorresu = $cambiomedidorresu;
    }

    public function setSeriecambio($seriecambio) {
        $this->seriecambio = $seriecambio;
    }

    public function setMarcacambio($marcacambio) {
        $this->marcacambio = $marcacambio;
    }

    public function setModelocambio($modelocambio) {
        $this->modelocambio = $modelocambio;
    }

    public function setPropiedadcambio($propiedadcambio) {
        $this->propiedadcambio = $propiedadcambio;
    }

    public function exchangeArray($data) {
        
        $this->estado_aviso = (isset($data['estado_aviso'])) ? $data['estado_aviso'] : null;
        $this->estado_digitacion = (isset($data['estado_digitacion'])) ? $data['estado_digitacion'] : null;
        $this->tecnico = (isset($data['tecnico'])) ? $data['tecnico'] : null;
        $this->fecha_carga = (isset($data['fecha_carga'])) ? $data['fecha_carga'] : null;
        $this->fecha_ejecucion = (isset($data['fecha_ejecucion'])) ? $data['fecha_ejecucion'] : null;
        $this->digitador = (isset($data['digitador'])) ? $data['digitador'] : null;
        $this->fecha_digitacion = (isset($data['fecha_digitacion'])) ? $data['fecha_digitacion'] : null; 
        
        $this->id_sucursal = (isset($data['id_sucursal'])) ? $data['id_sucursal'] : null;
        $this->numsolicitudtri = (isset($data['numsolicitudtri'])) ? $data['numsolicitudtri'] : null;
        $this->mesfacturacion = (isset($data['mesfacturacion'])) ? $data['mesfacturacion'] : null;
        $this->zonal = (isset($data['zonal'])) ? $data['zonal'] : null;
        $this->oficina = (isset($data['oficina'])) ? $data['oficina'] : null;
        $this->distribuidoras = (isset($data['distribuidoras'])) ? $data['distribuidoras'] : null;
        $this->estadosolicitud = (isset($data['estadosolicitud'])) ? $data['estadosolicitud'] : null;
        $this->fechaejecucion = (isset($data['fechaejecucion'])) ? $data['fechaejecucion'] : null;
        $this->num_form_riat = (isset($data['num_form_riat'])) ? $data['num_form_riat'] : null;
        $this->resultadoauditoria = (isset($data['resultadoauditoria'])) ? $data['resultadoauditoria'] : null;
        
        $this->constante_facturacion = (isset($data['constante_facturacion'])) ? $data['constante_facturacion'] : null;
        $this->constante_informada = (isset($data['constante_informada'])) ? $data['constante_informada'] : null;
        
        $this->aviso = (isset($data['aviso'])) ? $data['aviso'] : null;
        
        $this->actividad = (isset($data['actividad'])) ? $data['actividad'] : null;
        $this->detalle_actividad = (isset($data['detalle_actividad'])) ? $data['detalle_actividad'] : null;
        $this->total_facturacion = (isset($data['total_facturacion'])) ? $data['total_facturacion'] : null;
        
        $this->numservicio = (isset($data['numservicio'])) ? $data['numservicio'] : null;
        $this->cliente = (isset($data['cliente'])) ? $data['cliente'] : null;
        $this->direccion = (isset($data['direccion'])) ? $data['direccion'] : null;
        $this->comuna = (isset($data['comuna'])) ? $data['comuna'] : null;
        $this->tarifa = (isset($data['tarifa'])) ? $data['tarifa'] : null;
        $this->tipoempalme = (isset($data['tipoempalme'])) ? $data['tipoempalme'] : null;
        $this->circuito = (isset($data['circuito'])) ? $data['circuito'] : null;
        $this->conexion = (isset($data['conexion'])) ? $data['conexion'] : null;
        $this->medida = (isset($data['medida'])) ? $data['medida'] : null;
        $this->numeroserie = (isset($data['numeroserie'])) ? $data['numeroserie'] : null;
        $this->marca = (isset($data['marca'])) ? $data['marca'] : null;
        $this->activadejada = (isset($data['activadejada'])) ? $data['activadejada'] : null;
        $this->numeroseriereac = (isset($data['numeroseriereac'])) ? $data['numeroseriereac'] : null;
        $this->marcareac = (isset($data['marcareac'])) ? $data['marcareac'] : null;
        $this->reactivadejada = (isset($data['reactivadejada'])) ? $data['reactivadejada'] : null;
        $this->comentarios = (isset($data['comentarios'])) ? $data['comentarios'] : null;
        $this->cambiomedidorresu = (isset($data['cambiomedidorresu'])) ? $data['cambiomedidorresu'] : null;
        $this->seriecambio = (isset($data['seriecambio'])) ? $data['seriecambio'] : null;
        $this->marcacambio = (isset($data['marcacambio'])) ? $data['marcacambio'] : null;
        $this->modelocambio = (isset($data['modelocambio'])) ? $data['modelocambio'] : null;
        $this->propiedadcambio = (isset($data['propiedadcambio'])) ? $data['propiedadcambio'] : null;   
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
