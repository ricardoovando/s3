<?php

namespace Avisos\Model\Entity;

class Provincia {

  private $id_provincia;
  private $regiones_id_region;
  private $nombre_provincia;
  private $cod_provincia;
  
  function __construct($id_provincia = null, $regiones_id_region = null, $nombre_provincia = null, $cod_provincia = null) {
      $this->id_provincia = $id_provincia;
      $this->regiones_id_region = $regiones_id_region;
      $this->nombre_provincia = $nombre_provincia;
      $this->cod_provincia = $cod_provincia;
  }
  
  public function getId_provincia() {
      return $this->id_provincia;
  }

  public function setId_provincia($id_provincia) {
      $this->id_provincia = $id_provincia;
  }

  public function getRegiones_id_region() {
      return $this->regiones_id_region;
  }

  public function setRegiones_id_region($regiones_id_region) {
      $this->regiones_id_region = $regiones_id_region;
  }

  public function getNombre_provincia() {
      return $this->nombre_provincia;
  }

  public function setNombre_provincia($nombre_provincia) {
      $this->nombre_provincia = $nombre_provincia;
  }

  public function getCod_provincia() {
      return $this->cod_provincia;
  }

  public function setCod_provincia($cod_provincia) {
      $this->cod_provincia = $cod_provincia;
  }

  public function exchangeArray($data) {
      $this->id_provincia = (isset($data['id_provincia'])) ? $data['id_provincia'] : null;
      $this->regiones_id_region = (isset($data['regiones_id_region'])) ? $data['regiones_id_region'] : null;
      $this->nombre_provincia = (isset($data['nombre_provincia'])) ? $data['nombre_provincia'] : null;
      $this->cod_provincia = (isset($data['cod_provincia'])) ? $data['cod_provincia'] : null;
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

