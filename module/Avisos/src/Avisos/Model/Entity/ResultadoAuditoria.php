<?php

namespace Avisos\Model\Entity;

class ResultadoAuditoria {

  private $id_aviso;
  private $id_ors_riat;
  private $resultado_trifasico_new;
  
  function __construct($id_aviso=null,$id_ors_riat=null,$resultado_trifasico_new=null){
      $this->id_aviso = $id_aviso;
      $this->id_ors_riat = $id_ors_riat;
      $this->resultado_trifasico_new = $resultado_trifasico_new;
  }
  
  public function getId_aviso() {
      return $this->id_aviso;
  }

  public function setId_aviso($id_aviso) {
      $this->id_aviso = $id_aviso;
  }

  public function getId_ors_riat() {
      return $this->id_ors_riat;
  }

  public function setId_ors_riat($id_ors_riat) {
      $this->id_ors_riat = $id_ors_riat;
  }

  public function getResultado_trifasico_new() {
      return $this->resultado_trifasico_new;
  }

  public function setResultado_trifasico_new($resultado_trifasico_new) {
      $this->resultado_trifasico_new = $resultado_trifasico_new;
  }


  public function exchangeArray($data) {

      $this->id_aviso = (isset($data['id_aviso'])) ? $data['id_aviso'] : null;
      $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
      $this->resultado_trifasico_new = (isset($data['resultado_trifasico_new'])) ? $data['resultado_trifasico_new'] : null;

    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

