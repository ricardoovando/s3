<?php

namespace Avisos\Model\Entity;

class ResumenProduccion {
    
private $region;
private $sucursal;
private $estado_aviso;
private $cant_auditorias;
private $resultado_auditorias;
private $estado_digitacion;

function __construct($region = null, $sucursal = null, $estado_aviso = null, $cant_auditorias = null, $resultado_auditorias = null, $estado_digitacion = null) {
    $this->region = $region;
    $this->sucursal = $sucursal;
    $this->estado_aviso = $estado_aviso;
    $this->cant_auditorias = $cant_auditorias;
    $this->resultado_auditorias = $resultado_auditorias;
    $this->estado_digitacion = $estado_digitacion;
}

public function getRegion() {
    return $this->region;
}

public function getSucursal() {
    return $this->sucursal;
}

public function getEstado_aviso() {
    return $this->estado_aviso;
}

public function getCant_auditorias() {
    return $this->cant_auditorias;
}

public function getResultado_auditorias() {
    return $this->resultado_auditorias;
}

public function getEstado_digitacion() {
    return $this->estado_digitacion;
}

public function setRegion($region) {
    $this->region = $region;
}

public function setSucursal($sucursal) {
    $this->sucursal = $sucursal;
}

public function setEstado_aviso($estado_aviso) {
    $this->estado_aviso = $estado_aviso;
}

public function setCant_auditorias($cant_auditorias) {
    $this->cant_auditorias = $cant_auditorias;
}

public function setResultado_auditorias($resultado_auditorias) {
    $this->resultado_auditorias = $resultado_auditorias;
}

public function setEstado_digitacion($estado_digitacion) {
    $this->estado_digitacion = $estado_digitacion;
}

public function exchangeArray($data) {
    $this->region = (isset($data['region'])) ? $data['region'] : null;
    $this->sucursal = (isset($data['sucursal'])) ? $data['sucursal'] : null;
    $this->estado_aviso = (isset($data['estado_aviso'])) ? $data['estado_aviso'] : null;
    $this->cant_auditorias = (isset($data['cant_auditorias'])) ? $data['cant_auditorias'] : null;
    $this->resultado_auditorias = (isset($data['resultado_auditorias'])) ? $data['resultado_auditorias'] : null;
    $this->estado_digitacion = (isset($data['estado_digitacion'])) ? $data['estado_digitacion'] : null;
}

public function getArrayCopy() {
    return get_object_vars($this);
}

}

