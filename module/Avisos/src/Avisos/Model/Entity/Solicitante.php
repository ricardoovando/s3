<?php

namespace Avisos\Model\Entity;

class Solicitante {

  private $id_solicitante_distribuidora;
  private $nombre_completo;
  private $cargo;
  private $departamento_distri;
  private $correo_email;
  private $telefono_anexo;
  private $distribuidoras_id_distribuidora;


  function __construct($id_solicitante_distribuidora = null, $nombre_completo = null, $cargo = null, $departamento_distri = null, $correo_email = null, $telefono_anexo = null, $distribuidoras_id_distribuidora = null) {
      $this->id_solicitante_distribuidora = $id_solicitante_distribuidora;
      $this->nombre_completo = $nombre_completo;
      $this->cargo = $cargo;
      $this->departamento_distri = $departamento_distri;
      $this->correo_email = $correo_email;
      $this->telefono_anexo = $telefono_anexo;
      $this->distribuidoras_id_distribuidora = $distribuidoras_id_distribuidora;
  }

  public function getId_solicitante_distribuidora() {
      return $this->id_solicitante_distribuidora;
  }

  public function setId_solicitante_distribuidora($id_solicitante_distribuidora) {
      $this->id_solicitante_distribuidora = $id_solicitante_distribuidora;
  }

  public function getNombre_completo() {
      return $this->nombre_completo;
  }

  public function setNombre_completo($nombre_completo) {
      $this->nombre_completo = $nombre_completo;
  }

  public function getCargo() {
      return $this->cargo;
  }

  public function setCargo($cargo) {
      $this->cargo = $cargo;
  }

  public function getDepartamento_distri() {
      return $this->departamento_distri;
  }

  public function setDepartamento_distri($departamento_distri) {
      $this->departamento_distri = $departamento_distri;
  }

  public function getCorreo_email() {
      return $this->correo_email;
  }

  public function setCorreo_email($correo_email) {
      $this->correo_email = $correo_email;
  }

  public function getTelefono_anexo() {
      return $this->telefono_anexo;
  }

  public function setTelefono_anexo($telefono_anexo) {
      $this->telefono_anexo = $telefono_anexo;
  }

  public function getDistribuidoras_id_distribuidora() {
      return $this->distribuidoras_id_distribuidora;
  }

  public function setDistribuidoras_id_distribuidora($distribuidoras_id_distribuidora) {
      $this->distribuidoras_id_distribuidora = $distribuidoras_id_distribuidora;
  }
  
  public function exchangeArray($data) {
      $this->id_solicitante_distribuidora = (isset($data['id_solicitante_distribuidora'])) ? $data['id_solicitante_distribuidora'] : null;
      $this->nombre_completo = (isset($data['nombre_completo'])) ? $data['nombre_completo'] : null;
      $this->cargo = (isset($data['cargo'])) ? $data['cargo'] : null;
      $this->departamento_distri = (isset($data['departamento_distri'])) ? $data['departamento_distri'] : null;
      $this->correo_email = (isset($data['correo_email'])) ? $data['correo_email'] : null;
      $this->telefono_anexo = (isset($data['telefono_anexo'])) ? $data['telefono_anexo'] : null;
      $this->distribuidoras_id_distribuidora = (isset($data['distribuidoras_id_distribuidora'])) ? $data['distribuidoras_id_distribuidora'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

