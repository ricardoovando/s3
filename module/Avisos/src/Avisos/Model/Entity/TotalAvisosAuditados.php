<?php

namespace Avisos\Model\Entity;

class TotalAvisosAuditados {

    private $mes;
    private $anio;
    private $region;
    private $cantidad;

    function __construct($mes = null,$anio = null,$region = null,$cantidad = null) {
        $this->mes = $mes;
        $this->anio = $anio;
        $this->region = $region;
        $this->cantidad = $cantidad;
    }
    
    public function getMes() {
        return $this->mes;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

        
    public function getRegion() {
        return $this->region;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function setRegion($region) {
        $this->region = $region;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
    
    public function exchangeArray($data) {

        $this->mes = (isset($data['mes'])) ? $data['mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->region = (isset($data['region'])) ? $data['region'] : null;
        $this->cantidad = (isset($data['cantidad'])) ? $data['cantidad'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

