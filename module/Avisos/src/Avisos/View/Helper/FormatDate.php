<?php

namespace Avisos\View\Helper;

use Zend\View\Helper\AbstractHelper;
use DateTime;

class FormatDate extends AbstractHelper {

    public function __invoke($fecha = null, $patron = 'Y-m-d H:i') {
        if (!is_null($fecha)) {

            if (is_string($fecha)) {

                $fecha = new DateTime($fecha);
                return $fecha->format($patron);
            }
        }
    }

}