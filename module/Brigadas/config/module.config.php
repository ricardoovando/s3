<?php

return array(
    'controllers' => array(
        'invokables' => array(
        ),
    ),
    'router' => array(
        'routes' => array(
            'brigadas' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/brigadas[/:controller][/:action][/:id][/:br]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'br' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Brigadas\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'paginatorbrigadas' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/brigadas[/:controller][/:action]/id[/:id]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Brigadas\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'brigadas' => __DIR__ . '/../view',
        ),
    ),
);
