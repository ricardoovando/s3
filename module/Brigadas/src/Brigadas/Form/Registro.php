<?php

namespace Brigadas\Form;

use Zend\Form\Form;

class Registro extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id_brigada',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombre_brigada',
            'options' => array(
                'label' => 'Nombre de Brigada : ',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'vehiculo',
            'options' => array(
                'label' => 'Vehiculos',
                'empty_option' => 'Seleccione una patente',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
    }

}

