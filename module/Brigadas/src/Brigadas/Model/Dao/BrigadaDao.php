<?php

namespace Brigadas\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;


use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* Entity */
use Brigadas\Model\Entity\Brigada;
use Brigadas\Model\Entity\personalBrigada;

class BrigadaDao {

    protected $tableGateway;
    protected $tableGatewayPersonalTecnico;
    protected $tableGatewayPersonalBrigada;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway, $tableGatewayPersonalTecnico, $tableGatewayPersonalBrigada, Adapter $dbAdapter) {
        $this->tableGateway = $tableGateway;
        $this->tableGatewayPersonalTecnico = $tableGatewayPersonalTecnico;
        $this->tableGatewayPersonalBrigada = $tableGatewayPersonalBrigada;
        $this->dbAdapter = $dbAdapter;
    }

    public function obtenerTodos() {

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('veh' => 'vehiculos'), 'veh.id_vehiculo = brigadas.vehiculo_id_vehiculo', array('id_vehiculo' => 'id_vehiculo', 'patente' => 'patente', 'cod_vehiculo' => 'cod_vehiculo'));
        $select->where(array('brigadas.sucursales_id_sucursal' => 1));
        $select->order("id_brigada");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPersonasBrigada($id) {

        $id = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('pebri' => 'personal_brigada'), 'pebri.brigada_id_brigada = brigadas.id_brigada');
        $select->join(array('per' => 'personal'), 'per.id_persona = pebri.personal_id_persona');
        $select->join(array('car' => 'cargos'), 'car.idcargo = per.cargos_idcargo');
        $select->join(array('com' => 'comunas'), 'com.id_comuna = per.comunas_id_comuna');
        $select->where(array('brigadas.sucursales_id_sucursal' => 1));
        $select->where(array('id_brigada' => $id));
        $select->order("id_brigada");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {

        $id = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('veh' => 'vehiculos'), 'veh.id_vehiculo = brigadas.vehiculo_id_vehiculo', array('id_vehiculo' => 'id_vehiculo', 'patente' => 'patente', 'cod_vehiculo' => 'cod_vehiculo'));
        $select->where(array('id_brigada' => $id));

        $rowSet = $this->tableGateway->selectWith($select);
        $row = $rowSet->current();

        if (!$row) {

            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function eliminar(Brigada $brigada) {

        /* Eliminar de la tabla Brigada el registro */
        $this->tableGateway->delete(array('id_brigada' => $brigada->getId_brigada()));

        /* Eliminar todas las relaciones en la tabla personal de brigada */
        $this->tableGatewayPersonalBrigada->delete(array('brigada_id_brigada' => $brigada->getId_brigada()));
    }

    public function guardar(Brigada $brigada) {

        $data = array(
            'nombre_brigada' => $brigada->getNombre_brigada(),
            'sucursales_id_sucursal' => $brigada->getSucursales_id_sucursal(),
        );

        if ($brigada->getVehiculo() != null) {
            $data['vehiculo_id_vehiculo'] = $brigada->getVehiculo()->getId_vehiculo();
        }

        $id = (int) $brigada->getId_brigada();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_brigada' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorNombreBrigada($nombrebrigada) {

        $nombrebrigada = (String) $nombrebrigada;
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('veh' => 'vehiculos'), 'veh.id_vehiculo = brigadas.vehiculo_id_vehiculo', array('id_vehiculo' => 'id_vehiculo', 'patente' => 'patente', 'cod_vehiculo' => 'cod_vehiculo'));
        $select->where(array('brigadas.sucursales_id_sucursal' => 1));
        $select->where->like('nombre_brigada', '%' . $nombrebrigada . '%');
        $select->order('id_brigada');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPersonalTecnico() {

        $select = $this->tableGatewayPersonalTecnico->getSql()->select();
        $select->where(array('personaltecnico.sucursales_id_sucursal' => 1));
        $select->order('id_persona');

        $dbAdapter = $this->tableGatewayPersonalTecnico->getAdapter();
        $resultSetPrototype = $this->tableGatewayPersonalTecnico->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    /* Eliminar el tecnico que esta en la brigada seleccionada */

    public function eliminarTecnicoBrigada(personalBrigada $personalBrigada) {

        $this->tableGatewayPersonalBrigada->delete(array('personal_id_persona' => $personalBrigada->getPersonal_id_personal()));
    }

    /* Genera la relacion entre la persona tecnico y la brigada creada */

    public function guardartecnicobrigada(personalBrigada $personalBrigada) {

        $data = array(
            'brigada_id_brigada' => $personalBrigada->getBrigada_id_brigada(),
            'personal_id_persona' => $personalBrigada->getPersonal_id_personal(),
        );

        $this->tableGatewayPersonalBrigada->insert($data);
    }

    public function obtenerVehiculos() {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('vehiculos');


        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        $vehiculos = new \ArrayObject();


        foreach ($results as $row) {

            $vehiculo = new \Vehiculos\Model\Entity\Vehiculo();

            $vehiculo->exchangeArray($row);

            $vehiculos->append($vehiculo);
        }

        return $vehiculos;
    }

    public function obtenerVehiculosSelect() {

        $vehiculos = $this->obtenerVehiculos();

        $result = array();

        foreach ($vehiculos as $veh) {

            $result[$veh->getId_vehiculo()] = $veh->getPatente();
        }

        return $result;
    }


     // EJECUTA DIRECTAMENTE UNA CONSULTA SQL 
//      $statement = $this->dbAdapter->query("x");
//      $results = $statement->execute();
//    
//      $tecnicos = new \ArrayObject();
//      foreach ($results as $row) {
//      echo '<pre>';
//      var_dump($row);
//      echo '<br></pre>';
//      $persona = New Persona;
//      $persona->exchangeArray($row);
//      $tecnicos->append($persona);
//      }
//      echo '<pre>';
//      var_dump($tecnicos);
//      exit;
     
}
