<?php

namespace Servicios\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Servicios\Model\Entity\Servicio;

class ServicioDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('sucursales_id_sucursal' => 1));
        $select->order("id");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {

        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function eliminar(Servicio $servicio) {
        $this->tableGateway->delete(array('id' => $servicio->getId()));
    }

    public function guardar(Servicio $servicio) {

        $data = array(
            'nombreservicio' => $servicio->getNombreservicio(),
            'descripcion' => $servicio->getDescripcion(),
            'valoruf' => $servicio->getValoruf(),
            'sucursales_id_sucursal' => $servicio->getSucursales_id_sucursal()
        );

        $id = (int) $servicio->getId();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorNombreServicio($nombreservicio) {

        $nombreservicio = (String) $nombreservicio;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('sucursales_id_sucursal' => 1));
        $select->where->like('nombreservicio', '%' . $nombreservicio . '%');
        $select->order('id');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

}
