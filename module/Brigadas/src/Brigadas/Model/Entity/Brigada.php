<?php

namespace Brigadas\Model\Entity;

//use Vehiculos\Model\Entity\Vehiculo;

class Brigada {

    private $id_brigada;
    private $nombre_brigada;
    private $sucursales_id_sucursal;
    
    /*Objeto que enlaza con la tabla vehiculo con la tabla brigada*/
    private $vehiculo;

    /*Se obtiene las personas que estan asociadas a las brigadas*/
    private $personalTecnico;
    
    public function getPersonalTecnico() {
        return $this->personalTecnico;
    }

    public function setPersonalTecnico(Persona $personalTecnico) {
        $this->personalTecnico = $personalTecnico;
    }

        
    public function getId_brigada() {
        return $this->id_brigada;
    }

    public function setId_brigada($id_brigada) {
        $this->id_brigada = $id_brigada;
    }

    public function getNombre_brigada() {
        return $this->nombre_brigada;
    }

    public function setNombre_brigada($nombre_brigada) {
        $this->nombre_brigada = $nombre_brigada;
    }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getVehiculo() {
        return $this->vehiculo;
    }

    public function setVehiculo(Vehiculo $vehiculo) {
        $this->vehiculo = $vehiculo;
    }

    public function exchangeArray($data) {
        
        $this->id_brigada = (isset($data['id_brigada'])) ? $data['id_brigada'] : null;
        $this->nombre_brigada = (isset($data['nombre_brigada'])) ? $data['nombre_brigada'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        
        $this->vehiculo = new \Vehiculos\Model\Entity\Vehiculo();
        $this->vehiculo->setId_vehiculo((isset($data['id_vehiculo'])) ? $data['id_vehiculo'] : null);
        $this->vehiculo->setPatente((isset($data['patente'])) ? $data['patente'] : null);
        $this->vehiculo->setCod_vehiculo((isset($data['cod_vehiculo'])) ? $data['cod_vehiculo'] : null);     
        
        $this->personalTecnico = new \Personal\Model\Entity\Persona();
        $this->personalTecnico->setId_persona((isset($data['id_persona'])) ? $data['id_persona'] : null);
        $this->personalTecnico->setEstado_persona((isset($data['estado_persona'])) ? $data['estado_persona'] : null);
        $this->personalTecnico->setNombres((isset($data['nombres'])) ? $data['nombres'] : null);
        $this->personalTecnico->setApellidos((isset($data['apellidos'])) ? $data['apellidos'] : null); 
        $this->personalTecnico->setEmail((isset($data['email'])) ? $data['email'] : null);
        $this->personalTecnico->setTelefono((isset($data['telefono'])) ? $data['telefono'] : null); 
        $this->personalTecnico->setCelular((isset($data['celular'])) ? $data['celular'] : null);
        $this->personalTecnico->setDireccion((isset($data['direccion'])) ? $data['direccion'] : null); 
        $this->personalTecnico->setFoto_carnet((isset($data['foto_carnet'])) ? $data['foto_carnet'] : null); 
        $this->personalTecnico->setFoto_firma((isset($data['foto_firma'])) ? $data['foto_firma'] : null);
        $this->personalTecnico->setRut_persona((isset($data['rut_persona'])) ? $data['rut_persona'] : null);
        
        $cargo = new \Personal\Model\Entity\Cargo();
        $cargo->setNombre_cargo((isset($data['nombre_cargo'])) ? $data['nombre_cargo'] : null);
        $cargo->setDescripcion((isset($data['descripcion'])) ? $data['descripcion'] : null);
        $this->personalTecnico->setCargo($cargo);
        
        $comuna = new \Personal\Model\Entity\Comuna();
        $comuna->setNombre_comuna((isset($data['nombre_comuna'])) ? $data['nombre_comuna'] : null);
        $comuna->setCod_comuna((isset($data['cod_comuna'])) ? $data['cod_comuna'] : null);
        $this->personalTecnico->setComuna($comuna);
         
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

