<?php

return array(
    'controllers' => array(
        'invokables' => array(
        ),
    ),
    'router' => array(
        'routes' => array(
            'ddescontar' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ddescontar[/:controller][/:action][/:id][/:br]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'br' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Ddescontar\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'paginatorddescontar' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ddescontar[/:controller][/:action]/id[/:id]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'page' => '[0-9]+',
                        
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Ddescontar\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'ddescontar' => __DIR__ . '/../view',
        ),
    ),
);
