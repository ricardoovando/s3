<?php

namespace Ddescontar\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/* Formularios */
use Ddescontar\Form\Registro as DdescontarForm;

//use Ddescontar\Form\RegistroValidator;
//use Ddescontar\Form\Buscador as BuscadorForm;
//use Ddescontar\Form\BuscadorValidator;

/* 
* Entidad que reprecenta un registro o trupa 
*/
use Ddescontar\Model\Entity\Ddescontar;

use Avisos\Model\Entity\Aviso;
use Avisos\Model\Dao\AvisoDao;


class IndexController extends AbstractActionController {

    private $DdescontarDao;
    private $config;
    private $login;
    private $avisoDao;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setDdescontarDao($ddescontar) {
        $this->DdescontarDao = $ddescontar;
    }

    public function getDdescontarDao() {
        return $this->DdescontarDao;
    }

    public function setAvisoDao($avisoDao){
      $this->avisoDao = $avisoDao; 
    }
    
    public function getAvisoDao(){
       return $this->avisoDao;
    }
    
    public function getConfig() {
        return $this->config;
    }

    private function getDdescontarForm() {

        $form = new DdescontarForm("frm_ddescontar");
        
        $form->get('id_personal_falto')->setValueOptions($this->getAvisoDao()->obtenerPersonalTecnicoSelect());
        $form->get('id_sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        $form->get('id_motivo')->setValueOptions($this->getDdescontarDao()->obtenerMotivosValidosSelect());
        
        return $form; 
    }

    
    private function getFormBuscador() {
        return new BuscadorForm();
    }

    /* Listado de Brigadas en la Sucursal */

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        //$form = $this->getFormBuscador();

        $paginator = $this->getDdescontarDao()->obtenerTodos();
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(4);
                
        
        $c = $this->getDdescontarDao()->obtenerTodos();
  
        return new ViewModel(array(
            'title' => 'Listado de Dias a Descontar',
            'listadetecnicos' => $paginator->getIterator(),
            'paginator' => $paginator,
            'TotalDdescontar' => $c->count(),
            //'form' => $form,
        ));
    }

    public function crearAction() { 

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $form = $this->getDdescontarForm();

        return new ViewModel(array('title' => 'Agregar Dias a Descontar', 'form' => $form));
    }

    
    
    /*Registrar Dias a Descontar*/
    public function registrarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('ddescontar', array('controller' => 'index'));
        }

        $postParams = $this->request->getPost();

        $form = $this->getDdescontarForm();
        //$form->setInputFilter(new RegistroValidator());

        $form->setData($postParams);

        if (!$form->isValid()) {
            $modelView = new ViewModel(array('titulo' => 'Datos mal Ingresados', 'form' => $form));
            $modelView->setTemplate('ddescontar/index/crear');
            return $modelView;
        }

        
        foreach ($postParams["id_personal_falto"] as $key => $value) {
            
            /*Datos del Formulario*/
            $values = $form->getData();
            $values["id_personal_falto"] = $value;
            $values['id_responsable'] = 1;
            $values['archivo_certificado'] = 'imagen.png';

            $ddescontar = new Ddescontar();
            $ddescontar->exchangeArray($values);

            $this->getDdescontarDao()->guardar($ddescontar);
        
        }
        
        return $this->redirect()->toRoute('ddescontar', array('controller' => 'index'));
        
    }
    
    
    /*Eliminar Dias a Descontar*/
    public function eliminarAction() {

      $this->layout()->usuario = $this->getLogin()->getIdentity();
      
      $id = (int) $this->params()->fromRoute('id', 0); /* captura datos desde el request */

      if (!$id) {
            return $this->redirect()->toRoute('ddescontar', array('controller' => 'index'));
      }

      $ddescontar = new Ddescontar();
      $ddescontar->setId_dd($id);
      
      $this->getDdescontarDao()->eliminar($ddescontar);

      return $this->redirect()->toRoute('ddescontar', array('controller' => 'index'));
        
    }
    
       
    
    
    
    
    
    
    /*Buscar por Nombre de Brigada*/
    public function buscarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getFormBuscador();
        $form->setInputFilter(new BuscadorValidator());
        $form->setData($postParams);

        if (!$form->isValid()) {
            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('listadebrigadas' => $this->getBrigadaDao()->obtenerTodos(), 'title' => 'Lista de Brigadas', 'form' => $form));

            $modelView->setTemplate('brigadas/index/index');

            return $modelView;
        }

        $values = $form->getData();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getBrigadaDao()->buscarPorNombreBrigada($values['nombreservicio']);
        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(4);

        /* Configuracion de Vista */
        $viewModel = new ViewModel(array(
            'title' => sprintf('Datos Encontrados', $paginator->count()),
            'listadebrigadas' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $this->getFormBuscador(),
        ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("brigadas/index/index");

        return $viewModel;
    }
    
    /*Editar una Brigada*/
    public function editarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        $form = $this->getBrigadaForm();

        $brigada = $this->getBrigadaDao()->obtenerPorId($id);
        
        $form->bind($brigada);

        $form->get('send')->setAttribute('value', 'Editar');

        $modelView = new ViewModel(array('title' => 'Editar Registro', 'form' => $form));

        $modelView->setTemplate('brigadas/index/crear');

        return $modelView;
    }



    /*Muestra los Tecnicos que pertenecen a esta brigada*/
    public function verAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id_brigada = (int) $this->params()->fromRoute('id', 0);

        if (!$id_brigada) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        $paginator = $this->getBrigadaDao()->obtenerPersonasBrigada($id_brigada);

        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $paginator->setItemCountPerPage(4);

        return new ViewModel(array(
            'title' => 'Listado de Tecnicos Brigada N° : ' . $id_brigada,
            'listadetecnicosbrigada' => $paginator->getIterator(),
            'paginator' => $paginator,
            'id_brigada' => $id_brigada,
        ));
    }

    /*Lista todos los tecnicos que tiene disponible la sucursal*/
    public function listartecnicosAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id_brigada = (int) $this->params()->fromRoute('id', 0);

        if (!$id_brigada) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        $paginator = $this->getBrigadaDao()->obtenerPersonalTecnico();

        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $paginator->setItemCountPerPage(4);

        return new ViewModel(array(
            'title' => 'Listado de Personal Tecnico Sucursal',
            'listadepersonaltecnico' => $paginator->getIterator(),
            'paginator' => $paginator,
            'id_brigada' => $id_brigada,
        ));
    }

    
    /*Agregamos el tecnico seleccionado a la brigada*/
    public function agregartecnicoAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id_persona = (int) $this->params()->fromRoute('id', 0); /* captura datos desde el request */
        $id_brigada = (int) $this->params()->fromRoute('br', 0); /* captura datos desde el request */

        if (!$id_persona) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index', 'action' => 'ver', 'id' => $id_brigada));
        }

        if (!$id_brigada) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        $values = array();
        $values['brigada_id_brigada'] = $id_brigada;
        $values['personal_id_personal'] = $id_persona;

        $personalBrigada = new personalBrigada();
        $personalBrigada->exchangeArray($values);

        $this->getBrigadaDao()->guardartecnicobrigada($personalBrigada);

        return $this->redirect()->toRoute('brigadas', array('controller' => 'index', 'action' => 'ver', 'id' => $id_brigada));
    }

    
    /*Eliminar Tecnicos de la Brigada*/
    public function eliminartecnicobrigadaAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id_persona = (int) $this->params()->fromRoute('id', 0); /* captura datos desde el request */
        $id_brigada = (int) $this->params()->fromRoute('br', 0); /* captura datos desde el request */

        if (!$id_persona) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index', 'action' => 'ver', 'id' => $id_brigada));
        }

        if (!$id_brigada) {
            return $this->redirect()->toRoute('brigadas', array('controller' => 'index'));
        }

        $personalBrigada = new personalBrigada();
        $personalBrigada->setPersonal_id_personal($id_persona);

        $this->getBrigadaDao()->eliminarTecnicoBrigada($personalBrigada);

        return $this->redirect()->toRoute('brigadas', array('controller' => 'index', 'action' => 'ver', 'id' => $id_brigada));
    }

}
