<?php

namespace Ddescontar\Form;

use Zend\Form\Form;

class Registro extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id_dd',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array( 
            'name' => 'csrf', 
            'type' => 'Zend\Form\Element\Csrf', 
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id_sucursal',
            'options' => array(
                'empty_option' => 'Sucursales',
            ),
            'attributes' => array(
                'required' => 'required',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id_personal_falto',
            'options' => array(

            ),
            'attributes' => array(
                'multiple' => 'multiple',
                'required' => 'required',
                'style' => 'width:400px;height:210px;'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'id_motivo',
            'options' => array(

            ),
            'attributes' => array(
                'required' => 'required',
                'style' => 'width:600px;',
                'size' => 5,
                )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'num_mes',
            'options' => array(
                'empty_option' => 'Mes',
            ),
            'attributes' => array(
                'options' => array(
                    1 => 'Enero',
                    2 => 'Febrero',
                    3 => 'Marzo',
                    4 => 'Abril',
                    5 => 'Mayo',
                    6 => 'Junio',
                    7 => 'Julio',
                    8 => 'Agosto',
                    9 => 'Septiembre',
                    10 => 'Octubre',
                    11 => 'Noviembre',
                    12 => 'Diciembre',
                  ),
                'required' => 'required',
                )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'anio',
            'options' => array(
                'empty_option' => 'Año',
            ),
            'attributes' => array(
                'options' => array(
                    2014 => '2014',
                    2015 => '2015',
                    2016 => '2016',
                    2017 => '2017',
                  ),
                'required' => 'required',
                )
             ));

        $this->add(array( 
              'type' => 'Zend\Form\Element\Text',
              'name' => 'cant_dias',
              'options' => array(
            ),
            'attributes' => array(
              'placeholder' => 'Cant. Dias',
              'required' => 'required',  
            ),
        ));
        
        
        $this->add(array( 
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'comentario_motivo_falta',
            'attributes' => array( 
                 'rows'=> '3',
                 'cols' => '200',
                 'style' => 'width:300px;',
                ), 
              'options' => array( 
                ), 
            ));
        
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
    }

}

