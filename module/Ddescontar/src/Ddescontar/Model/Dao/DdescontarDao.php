<?php

namespace Ddescontar\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* Entity */
use Ddescontar\Model\Entity\Ddescontar;
//use Ddescontar\Model\Entity\personalBrigada;



class DdescontarDao {

    
    protected $tableGateway;
    protected $dbAdapter;
    protected $login;

    
    public function __construct(TableGateway $tableGateway, Adapter $dbAdapter , $login) {
        
      $this->tableGateway = $tableGateway;
      $this->dbAdapter = $dbAdapter;
      $this->login = $login->getIdentity();
      
    }

      
    public function obtenerTodos() {

        $id_usuario = $this->login->id;
        
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('per' => 'personal'), 'per.id_persona = dias_descontar.id_personal_falto',array('nombres' => 'nombres','apellidos'=>'apellidos','rut_persona'=>'rut_persona'));
        $select->join(array('car' => 'cargos'), 'car.idcargo = per.cargos_idcargo',array('nombre_cargo' => 'nombre_cargo'));
        $select->join(array('mot' => 'motivos_validos'), 'mot.id_motivo = dias_descontar.id_motivo',array('motivo' => 'motivo','descripcion' => 'descripcion'));
        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = dias_descontar.id_sucursal AND ussuc.usuarios_id_usuario = '.$id_usuario),array());
        $select->join(array('suc' => 'sucursales'), 'suc.id_sucursal = ussuc.sucursales_id_sucursal' , array('nombre_sucursal' => 'nombre_sucursal'));
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }
    
    
    public function obtenerNumeroTotal(){
        
      //EJECUTA DIRECTAMENTE UNA CONSULTA SQL 
      $statement = $this->dbAdapter->query("x");
      $results = $statement->execute();
    
      $tecnicos = new \ArrayObject();
      
      foreach ($results as $row) {
        echo '<pre>';
        var_dump($row);
        echo '<br></pre>';
        $persona = New Persona;
        $persona->exchangeArray($row);
        $tecnicos->append($persona);
      }
      
      echo '<pre>';
      var_dump($tecnicos);
      exit;
        
        
        
    }

    
    
    public function guardar(Ddescontar $ddescontar) {

        $data = array(
                    'id_sucursal' => $ddescontar->getId_sucursal(),
                    'id_responsable' => $ddescontar->getId_responsable(),
                    'id_personal_falto' => $ddescontar->getId_personal_falto(),
                    'id_motivo' => $ddescontar->getId_motivo(),
                    'comentario_dias_descontar' => $ddescontar->getComentario_dias_descontar(),
                    'archivo_certificado' => $ddescontar->getArchivo_certificado(),
                    'cant_dias' => $ddescontar->getCant_dias(),
                    'num_mes' => $ddescontar->getNum_mes(),
                    'anio' => $ddescontar->getAnio(),
                   );

        $id = (int) $ddescontar->getId_dd();

        if ($id == 0) {
            
            $this->tableGateway->insert($data);
            
        } else {
            
            if ($this->obtenerPorId($id)) {
                
                $this->tableGateway->update($data, array('id_dd' => $id));
                
            } else {
                
                throw new \Exception('Form id does not exist');
                
            }
            
        }
        
    }
    
    
    /*
     * Se elimina 
     *
     */
    public function eliminar(Ddescontar $ddescontar) {

        /* Eliminar de la tabla Dias_Descontar el registro */
        $this->tableGateway->delete(array('id_dd' => $ddescontar->getId_dd()));

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function obtenerPersonasBrigada($id) {

        $id = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('pebri' => 'personal_brigada'), 'pebri.brigada_id_brigada = brigadas.id_brigada');
        $select->join(array('per' => 'personal'), 'per.id_persona = pebri.personal_id_persona');
        $select->join(array('car' => 'cargos'), 'car.idcargo = per.cargos_idcargo');
        $select->join(array('com' => 'comunas'), 'com.id_comuna = per.comunas_id_comuna');
        $select->where(array('brigadas.sucursales_id_sucursal' => 1));
        $select->where(array('id_brigada' => $id));
        $select->order("id_brigada");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {

        $id = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('veh' => 'vehiculos'), 'veh.id_vehiculo = brigadas.vehiculo_id_vehiculo', array('id_vehiculo' => 'id_vehiculo', 'patente' => 'patente', 'cod_vehiculo' => 'cod_vehiculo'));
        $select->where(array('id_brigada' => $id));

        $rowSet = $this->tableGateway->selectWith($select);
        $row = $rowSet->current();

        if (!$row) {

            throw new \Exception("Could not find row $id");
        }

        return $row;
    }





    public function buscarPorNombreBrigada($nombrebrigada) {

        $nombrebrigada = (String) $nombrebrigada;
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('veh' => 'vehiculos'), 'veh.id_vehiculo = brigadas.vehiculo_id_vehiculo', array('id_vehiculo' => 'id_vehiculo', 'patente' => 'patente', 'cod_vehiculo' => 'cod_vehiculo'));
        $select->where(array('brigadas.sucursales_id_sucursal' => 1));
        $select->where->like('nombre_brigada', '%' . $nombrebrigada . '%');
        $select->order('id_brigada');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPersonalTecnico() {

        $select = $this->tableGatewayPersonalTecnico->getSql()->select();
        $select->where(array('personaltecnico.sucursales_id_sucursal' => 1));
        $select->order('id_persona');

        $dbAdapter = $this->tableGatewayPersonalTecnico->getAdapter();
        $resultSetPrototype = $this->tableGatewayPersonalTecnico->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    /* Eliminar el tecnico que esta en la brigada seleccionada */

    public function eliminarTecnicoBrigada(personalBrigada $personalBrigada) {

        $this->tableGatewayPersonalBrigada->delete(array('personal_id_persona' => $personalBrigada->getPersonal_id_personal()));
    }

    /* Genera la relacion entre la persona tecnico y la brigada creada */

    public function guardartecnicobrigada(personalBrigada $personalBrigada) {

        $data = array(
            'brigada_id_brigada' => $personalBrigada->getBrigada_id_brigada(),
            'personal_id_persona' => $personalBrigada->getPersonal_id_personal(),
        );

        $this->tableGatewayPersonalBrigada->insert($data);
    }

    
    public function obtenerMotivosValidos() {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('motivos_validos');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $motivos_validos = new \ArrayObject();

        foreach ($results as $row) {
            $motivo = new \Ddescontar\Model\Entity\MotivoValido();
            $motivo->exchangeArray($row);
            $motivos_validos->append($motivo);
        }

        return $motivos_validos;
    }

    public function obtenerMotivosValidosSelect() {

        $motivosValidos = $this->obtenerMotivosValidos();

        $result = array();
        
        $i=0;
        foreach ($motivosValidos as $mv) {
           $result[$mv->getId_motivo()] = ++$i . ' _ ' . $mv->getMotivo();
          }

        return $result;
    }


     // EJECUTA DIRECTAMENTE UNA CONSULTA SQL 
//      $statement = $this->dbAdapter->query("x");
//      $results = $statement->execute();
//    
//      $tecnicos = new \ArrayObject();
//      foreach ($results as $row) {
//      echo '<pre>';
//      var_dump($row);
//      echo '<br></pre>';
//      $persona = New Persona;
//      $persona->exchangeArray($row);
//      $tecnicos->append($persona);
//      }
//      echo '<pre>';
//      var_dump($tecnicos);
//      exit;
    
    
    
    
    
    
     
}
