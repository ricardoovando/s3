<?php

namespace Ddescontar\Model\Entity;


class Ddescontar {

    
    private $id_dd;
    private $id_sucursal;
    private $id_responsable;
    private $id_personal_falto;
    private $id_motivo;
    private $comentario_dias_descontar;
    private $archivo_certificado;
    private $cant_dias;
    private $num_mes;
    private $anio;
    private $fecha_ingreso;
    
    /*Objetos*/
    private $sucursal;
    private $personal_falto;
    private $motivo;
    private $cargo;
    /*+++++++++++++++*/

    function __construct($id_dd = null, $id_sucursal = null, $id_responsable = null, $id_personal_falto = null, $id_motivo = null, $comentario_dias_descontar = null, $archivo_certificado = null, $cant_dias = null, $num_mes = null, $anio = null, $fecha_ingreso = null) {
        $this->id_dd = $id_dd;
        $this->id_sucursal = $id_sucursal;
        $this->id_responsable = $id_responsable;
        $this->id_personal_falto = $id_personal_falto;
        $this->id_motivo = $id_motivo;
        $this->comentario_dias_descontar = $comentario_dias_descontar;
        $this->archivo_certificado = $archivo_certificado;
        $this->cant_dias = $cant_dias;
        $this->num_mes = $num_mes;
        $this->anio = $anio;
        $this->fecha_ingreso = $fecha_ingreso;
    }

    
    public function getId_dd() {
        return $this->id_dd;
    }

    public function getId_sucursal() {
        return $this->id_sucursal;
    }

    public function getId_responsable() {
        return $this->id_responsable;
    }

    public function getId_personal_falto() {
        return $this->id_personal_falto;
    }

    public function getId_motivo() {
        return $this->id_motivo;
    }

    public function getComentario_dias_descontar() {
        return $this->comentario_dias_descontar;
    }

    public function getArchivo_certificado() {
        return $this->archivo_certificado;
    }

    public function getCant_dias() {
        return $this->cant_dias;
    }

    public function getNum_mes() {
        return $this->num_mes;
    }

    public function getAnio() {
        return $this->anio;
    }

    public function getFecha_ingreso() {
        return $this->fecha_ingreso;
    }

    public function setId_dd($id_dd) {
        $this->id_dd = $id_dd;
    }

    public function setId_sucursal($id_sucursal) {
        $this->id_sucursal = $id_sucursal;
    }

    public function setId_responsable($id_responsable) {
        $this->id_responsable = $id_responsable;
    }

    public function setId_personal_falto($id_personal_falto) {
        $this->id_personal_falto = $id_personal_falto;
    }

    public function setId_motivo($id_motivo) {
        $this->id_motivo = $id_motivo;
    }

    public function setComentario_dias_descontar($comentario_dias_descontar) {
        $this->comentario_dias_descontar = $comentario_dias_descontar;
    }

    public function setArchivo_certificado($archivo_certificado) {
        $this->archivo_certificado = $archivo_certificado;
    }

    public function setCant_dias($cant_dias) {
        $this->cant_dias = $cant_dias;
    }

    public function setNum_mes($num_mes) {
        $this->num_mes = $num_mes;
    }

    public function setAnio($anio) {
        $this->anio = $anio;
    }

    public function setFecha_ingreso($fecha_ingreso) {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /*Objetos relacionados*/
    
    public function getSucursal() {
        return $this->sucursal;
    }

    public function setSucursal(Sucursal $sucursal) {
        $this->sucursal = $sucursal;
    }

    public function getPersonal_falto() {
        return $this->personal_falto;
    }

    public function setPersonal_falto(Personal $personal_falto) {
        $this->personal_falto = $personal_falto;
    }

    public function getMotivo() {
        return $this->motivo;
    }

    public function setMotivo(MotivoValido $motivo) {
        $this->motivo = $motivo;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function setCargo(Cargo $cargo) {
        $this->cargo = $cargo;
    }
        
    public function exchangeArray($data) {
        
        $this->id_dd = (isset($data['id_dd'])) ? $data['id_dd'] : null;
        $this->id_sucursal = (isset($data['id_sucursal'])) ? $data['id_sucursal'] : null;
        $this->id_responsable = (isset($data['id_responsable'])) ? $data['id_responsable'] : null;
        $this->id_personal_falto = (isset($data['id_personal_falto'])) ? $data['id_personal_falto'] : null;
        $this->id_motivo = (isset($data['id_motivo'])) ? $data['id_motivo'] : null;
        $this->comentario_dias_descontar = (isset($data['comentario_dias_descontar'])) ? $data['comentario_dias_descontar'] : null;
        $this->archivo_certificado = (isset($data['archivo_certificado'])) ? $data['archivo_certificado'] : null;
        $this->cant_dias = (isset($data['cant_dias'])) ? $data['cant_dias'] : null;
        $this->num_mes = (isset($data['num_mes'])) ? $data['num_mes'] : null;
        $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
        $this->fecha_ingreso = (isset($data['fecha_ingreso'])) ? $data['fecha_ingreso'] : null;  
        
        $this->sucursal = new \Avisos\Model\Entity\Sucursal();
        $this->sucursal->setNombre_sucursal((isset($data['nombre_sucursal'])) ? $data['nombre_sucursal'] : null);
       
        $this->personal_falto = new \Personal\Model\Entity\Persona();
        $this->personal_falto->setNombres((isset($data['nombres'])) ? $data['nombres'] : null);
        $this->personal_falto->setApellidos((isset($data['apellidos'])) ? $data['apellidos'] : null);
        $this->personal_falto->setRut_persona((isset($data['rut_persona'])) ? $data['rut_persona'] : null);
              
        $this->motivo = new \Ddescontar\Model\Entity\MotivoValido();
        $this->motivo->setMotivo((isset($data['motivo'])) ? $data['motivo'] : null);
        $this->motivo->setDescripcion((isset($data['descripcion'])) ? $data['descripcion'] : null);
              
        $this->cargo = new \Personal\Model\Entity\Cargo(); 
        $this->cargo->setNombre_cargo((isset($data['nombre_cargo'])) ? $data['nombre_cargo'] : null);
         
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

