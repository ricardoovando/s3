<?php

namespace Ddescontar\Model\Entity;

class MotivoValido {

  private $id_motivo;
  private $descripcion;
  private $motivo;
  private $ans_bono;
  
  function __construct($id_motivo = null, $descripcion = null, $motivo = null, $ans_bono = null) {
      $this->id_motivo = $id_motivo;
      $this->descripcion = $descripcion;
      $this->motivo = $motivo;
      $this->ans_bono = $ans_bono;
  }
  
  public function getId_motivo() {
      return $this->id_motivo;
  }

  public function setId_motivo($id_motivo) {
      $this->id_motivo = $id_motivo;
  }

  public function getDescripcion() {
      return $this->descripcion;
  }

  public function setDescripcion($descripcion) {
      $this->descripcion = $descripcion;
  }

  public function getMotivo() {
      return $this->motivo;
  }

  public function setMotivo($motivo) {
      $this->motivo = $motivo;
  }

  public function getAns_bono() {
      return $this->ans_bono;
  }

  public function setAns_bono($ans_bono) {
      $this->ans_bono = $ans_bono;
  }
   
  public function exchangeArray($data) {   
      $this->id_motivo =  (isset($data['id_motivo'])) ? $data['id_motivo'] : null;
      $this->descripcion =  (isset($data['descripcion'])) ? $data['descripcion'] : null;
      $this->motivo =  (isset($data['motivo'])) ? $data['motivo'] : null;
      $this->ans_bono =  (isset($data['ans_bono'])) ? $data['ans_bono'] : null;     
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

