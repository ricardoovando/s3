<?php

namespace Ddescontar\Model\Entity;

//use Vehiculos\Model\Entity\Vehiculo;

class personalBrigada {

    private $brigada_id_brigada;
    private $personal_id_personal;

    function __construct($brigada_id_brigada = null, $personal_id_personal = null) {
        $this->brigada_id_brigada = $brigada_id_brigada;
        $this->personal_id_personal = $personal_id_personal;
    }
    
    public function getBrigada_id_brigada() {
        return $this->brigada_id_brigada;
    }

    public function setBrigada_id_brigada($brigada_id_brigada) {
        $this->brigada_id_brigada = $brigada_id_brigada;
    }

    public function getPersonal_id_personal() {
        return $this->personal_id_personal;
    }

    public function setPersonal_id_personal($personal_id_personal) {
        $this->personal_id_personal = $personal_id_personal;
    }

    
    public function exchangeArray($data) {

        $this->brigada_id_brigada = (isset($data['brigada_id_brigada'])) ? $data['brigada_id_brigada'] : null;
        $this->personal_id_personal = (isset($data['personal_id_personal'])) ? $data['personal_id_personal'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

