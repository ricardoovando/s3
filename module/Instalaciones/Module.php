<?php

namespace Instalaciones;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


/*Entity*/
use Instalaciones\Model\Dao\AntecedentesEmpalmeDao;
use Instalaciones\Model\Entity\AntecedentesEmpalme;

use Instalaciones\Model\Dao\DistribuidorasDao;
use Instalaciones\Model\Entity\Distribuidoras;

use Instalaciones\Model\Dao\EcmDao;
use Instalaciones\Model\Entity\Ecm;

use Instalaciones\Model\Dao\InstalacionDao;
use Instalaciones\Model\Entity\Instalacion;

use Instalaciones\Model\Dao\MedidorDao;
use Instalaciones\Model\Entity\Medidor;

use Instalaciones\Model\Dao\MedidorInstaladoDao;
use Instalaciones\Model\Entity\MedidorInstalado;

use Instalaciones\Model\Dao\MarcasEcmsDao;
use Instalaciones\Model\Entity\MarcasEcms;

use Instalaciones\Model\Dao\MedidorInstalacionDao;
use Instalaciones\Model\Entity\MedidorInstalacion;

use Instalaciones\Model\Dao\ModelosMedidoresDao;
use Instalaciones\Model\Entity\ModelosMedidores;

use Instalaciones\Model\Dao\RegletaInstalacionDao;
use Instalaciones\Model\Entity\RegletaInstalacion;

use Instalaciones\Model\Dao\RegletasDao;
use Instalaciones\Model\Entity\Regletas;

use Instalaciones\Model\Dao\RelojDao;
use Instalaciones\Model\Entity\Reloj;

use Instalaciones\Model\Dao\SistemaDistribucionDao;
use Instalaciones\Model\Entity\SistemaDistribucion;

use Instalaciones\Model\Dao\TelemedidaDao;
use Instalaciones\Model\Entity\Telemedida;

use Instalaciones\Model\Dao\TransformadorCorrienteDao;
use Instalaciones\Model\Entity\TransformadorCorriente;



class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, ServiceProviderInterface, ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();

        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
        
    }
    
        public function initAuth(MvcEvent $e) {
        $application = $e->getApplication();
        $matches = $e->getRouteMatch();
        $controller = $matches->getParam('controller');
        $action = $matches->getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index', 'autenticar'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index y autenticar.
                        return;
                    }
                    break;
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
            }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('home');
            }
        }
    }
    

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniInstalaciones', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return $data;
                });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }



    /* Inyecccion de dependencia para el dao se carga la tabla de base de datos segun se indique */

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Instalaciones\Model\AntecedentesEmpalmeDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayAntecedentesEmpalme');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new AntecedentesEmpalmeDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\DistribuidorasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayDistribuidoras');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new DistribuidorasDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\EcmDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayEcm');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new EcmDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\InstalacionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayInstalaciones');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new InstalacionDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\MarcasEcmsDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMarcasEcms');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MarcasEcmsDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\MedidorInstalacionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMedidorInstalacion');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MedidorInstalacionDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\ModelosMedidoresDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayModelosMedidores');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ModelosMedidoresDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\RegletaInstalacionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayRegletaInstalacion');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new RegletaInstalacionDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\MedidorDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMedidores');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MedidorDao($tableGateway, $login);
                    return $dao;
                },     
                'Instalaciones\Model\MedidorInstaladoDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMedidorInstalado');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MedidorInstaladoDao($tableGateway, $login);
                    return $dao;
                },        
                'Instalaciones\Model\RegletasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayRegletas');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new RegletasDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\RelojDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayReloj');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new RelojDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\SistemaDistribucionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewaySistemaDistribucion');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new SistemaDistribucionDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\TelemedidaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayTelemedida');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new TelemedidaDao($tableGateway, $login);
                    return $dao;
                },
                'Instalaciones\Model\TransformadorCorrienteDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayTransformadorCorriente');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new TransformadorCorrienteDao($tableGateway, $login);
                    return $dao;
                },
                /*
                 * 
                 * TABLAS 
                 * 
                 */
                'TableGatewayAntecedentesEmpalme' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AntecedentesEmpalme());
                    return new TableGateway('antecedentes_empalme', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayDistribuidoras' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Distribuidoras());
                    return new TableGateway('distribuidoras', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayEcm' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Ecm());
                    return new TableGateway('ecm', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayInstalaciones' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Instalacion());
                    return new TableGateway('instalaciones', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayMedidores' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Medidor());
                    return new TableGateway('medidores', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayMedidorInstalado' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MedidorInstalado());
                    return new TableGateway('medidor_instalado', $dbAdapter, null, $resultSetPrototype);
                },          
                'TableGatewayMarcasEcms' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MarcasEcms());
                    return new TableGateway('marcas_ecms', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayMedidorInstalacion' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MedidorInstalacion());
                    return new TableGateway('medidor_instalacion', $dbAdapter, null, $resultSetPrototype);
                },
                /*
                 * Se creo nueva tabla para poner en produccion nuevos codigos de medidores 
                 */
                'TableGatewayModelosMedidores' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ModelosMedidores());
                    return new TableGateway('modelos_medidores_new', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayRegletaInstalacion' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RegletaInstalacion());
                    return new TableGateway('regleta_instalacion', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayRegletas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Regletas());
                    return new TableGateway('regletas', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayReloj' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Reloj());
                    return new TableGateway('reloj', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewaySistemaDistribucion' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SistemaDistribucion());
                    return new TableGateway('sistema_distribucion', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayTelemedida' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Telemedida());
                    return new TableGateway('telemedida', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayTransformadorCorriente' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TransformadorCorriente());
                    return new TableGateway('transformador_corriente', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

    /*Inyeccion de dependencia para el controlador se carga el objeto modelo usuario dao*/
     public function getControllerConfig() {
        return  array(
            'factories' => array(
                'Instalaciones\Controller\Index' => function ($sm) {
            
                    $locator = $sm->getServiceLocator();
                    
                    $config = $locator->get('ConfigIniInstalaciones');
                    
                    $instalaciondao = $locator->get('Instalaciones\Model\InstalacionDao');
                    
                    $controller = new \Instalaciones\Controller\IndexController($config);
                    
                    $controller->setInstalacionDao($instalaciondao);
                    
                    return $controller;
                    
                },
             )
          );
       } 


    
}
