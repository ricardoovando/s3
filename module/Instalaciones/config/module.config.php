<?php

return array(
    'controllers' => array(
        'invokables' => array(

        ),
    ),
    'router' => array(
        'routes' => array(
            'instalaciones' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/instalaciones[/:controller][/:action][/:id][/:br]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'br' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Instalaciones\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
          'paginatorinstalaciones' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/instalaciones[/:controller][/:action]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Instalaciones\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Instalaciones' => __DIR__ . '/../view',
        ),
    ),
);
