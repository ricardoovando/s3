<?php

namespace Instalaciones\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/* FORMULARIOS */
use Instalaciones\Form\BuscadorInstalacion;
use Instalaciones\Form\BuscadorInstalacionValidator;
use Instalaciones\Form\BuscadorUnidadLectura;
use Instalaciones\Form\BuscadorUnidadLecturaValidator;        


class IndexController extends AbstractActionController {

    private $instalacionDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function setInstalacionDao($instalacionDao) {
        $this->instalacionDao = $instalacionDao;
    }

    public function getInstalacionDao() {
        return $this->instalacionDao;
    }
    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    private function getFormBuscadorInstalacion(){   
        return new BuscadorInstalacion();
    }

    private function getFormBuscadorUnidadLectura(){   
        return new BuscadorUnidadLectura();
    }

    
    /* Muestra la pantalla principal del sistema o modulo avisos */
    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();


        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();


        $paginator = $this->getInstalacionDao()->obtenerTodos();

        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        $paginator->setItemCountPerPage(10);

        return new ViewModel(array(
            'title' => 'Listado de Instalaciónes',
            'listaInstalaciones' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
        ));
    }

    
    
    
    /* 
     * 
     * Se enviar el formulario para crear un nuevo registro 
     * 
     * 
     */

    public function crearAction() {

        $form = $this->getForm();
        return new ViewModel(array('titulo' => 'Registro de Inspección y Auditoria Trifásica', 'form' => $form));
    }


    
   
    /* 
     * 
     * Buscador por Numero Instalacion 
     * 
     * 
     */

    public function buscarNumInstalacionAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('avisos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        
        /* LLamar a Formularios */
        $formSelectBrigadas = $this->getFormSelectBrigada();
        $formBuscador = $this->getFormBuscador();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formBuscadorUnidadLectura = $this->getFormBuscadorUnidadLectura();

        $formBuscadorInstalacion->setInputFilter(new BuscadorInstalacionValidator());
        $formBuscadorInstalacion->setData($postParams);
        
        
        /*
         * 
         * Falla de la Validacion del form. 
         * 
         * 
         */
        if (!$formBuscadorInstalacion->isValid())
        {
            
            //Falla la validación; volvemos a generar el formulario     
            $paginator = $this->getAvisoDao()->obtenerTodos();

            $paginator->setCurrentPageNumber(0);

            $paginator->setItemCountPerPage(20);

            $modelView = new ViewModel(array(
                'title' => 'Error en la Carga',
                'listaAvisos' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscador' => $formBuscador,
                'formBrigadas' => $formSelectBrigadas,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
            ));

            $modelView->setTemplate('avisos/index/index');

            return $modelView;
            
            
        }


        $values = $formBuscadorInstalacion->getData();
        

        /* Reconfiguracion de Paginador */
        $paginator = $this->getAvisoDao()->obtenerPorNumInstalacion($values['numeroinstalacion']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(20);


        $viewModel = new ViewModel(array(
            'title' => 'Avisos Encontrados',
            'listaAvisos' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscador' => $formBuscador,
            'formBrigadas' => $formSelectBrigadas,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
        ));


        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("avisos/index/index");

        return $viewModel;
    }

}
