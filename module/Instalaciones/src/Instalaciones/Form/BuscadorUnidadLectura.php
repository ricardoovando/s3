<?php

namespace Instalaciones\Form;

use Zend\Form\Form;

class BuscadorUnidadLectura extends Form {

    public function __construct($name = null) {
        parent::__construct('buscadorinstalacion');

        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numerounidadlectura',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Buscar N° Unidad Lectura',
            ),
        ));

        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn btn-success',
                'style' => 'height:30px;',
            ),
        ));
        
    }

}

