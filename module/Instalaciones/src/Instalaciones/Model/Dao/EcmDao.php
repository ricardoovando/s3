<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Instalaciones\Model\Entity\Ecm;

class EcmDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }


    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('instalaciones_id_instalacion' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(Ecm $Ecm) {

        try {
            
            $data = array(
                       'instalaciones_id_instalacion' => $Ecm->getInstalaciones_id_instalacion(),
                       'modelo' => $Ecm->getModelo(),
                       'serie' =>  $Ecm->getSerie(),
                       'anio' => $Ecm->getAnio(),
                       'cantidad_elementos' =>  $Ecm->getCantidad_elementos(),
                       'ip1' => $Ecm->getIp1(),
                       'ip2' => $Ecm->getIp2(),
                       'ip3' => $Ecm->getIp3(),
                       'ip4' => $Ecm->getIp4(),
                       'is_ecm' => $Ecm->getIs_ecm(),
                       'vs' => $Ecm->getVs(),
                       'vp' => $Ecm->getVp(),
                       'marca' => $Ecm->getMarca(),
                       'ttcc1' => $Ecm->getTtcc1(),
                       'ttpp1' => $Ecm->getTtpp1(),
                       'ttcc2' => $Ecm->getTtcc2(),
                       'ttpp2' => $Ecm->getTtpp2(),
                     );

            $id = (int) $Ecm->getInstalaciones_id_instalacion();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('instalaciones_id_instalacion' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    
}
