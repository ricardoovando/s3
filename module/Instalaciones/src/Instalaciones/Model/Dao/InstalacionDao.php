<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Instalaciones\Model\Entity\Instalacion;

class InstalacionDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        $select->order('id_instalacion ASC');
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_instalacion' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function obtenerPorNumInstalacion($num_instalacion) {
        
        $num_instalacion = (String) $num_instalacion;
        $rowset = $this->tableGateway->select(array('num_instalacion' => $num_instalacion));
        $row = $rowset->current();

        return $row;
        
    }
    
    public function obtenerComuna($nombrecomuna) {

        $nombrecomuna = (String) $nombrecomuna;
        $nombrecomuna = ucwords(strtolower($nombrecomuna));

        $sql = new sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('comunas');
        $select->where(array('nombre_comuna' => $nombrecomuna));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $comunaArray = new \ArrayObject();

        foreach ($results as $row) {
            $comunaEntity = new Comuna();
            $comunaEntity->exchangeArray($row);
            $comunaArray->append($comunaEntity);
        }

        if (!$comunaArray) {
            throw new \Exception("Comuna no correcta $nombrecomuna");
        }

        return $comunaArray;
    }

    public function obtenerUltimoId() {

        $sql = new sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('instalaciones_id_instalacion_seq');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();
        return $row['last_value'];
        
    }

    public function guardar(Instalacion $instalacion) {

        $data = array(
            'distribuidoras_id_distribuidora' => $instalacion->getDistribuidoras_id_distribuidora(),
            'nombre_cliente' => $instalacion->getNombre_cliente(),
            'direccion1' => $instalacion->getDireccion1(),
            'direccion2' => $instalacion->getDireccion2(),
            'num_instalacion' => $instalacion->getNum_instalacion(),
            'tarifa' => $instalacion->getTarifa(),
            'unidad_lectura' => $instalacion->getUnidad_lectura(),
            'numero_poste_camara' => $instalacion->getNumero_poste_camara(),
            'nom_poblacion' => $instalacion->getNom_poblacion(),  
            'numero_serie' => $instalacion->getNumero_serie(),
            'material' => $instalacion->getMaterial(),
            'marca_medidor' => $instalacion->getMarca_medidor(),
            'propiedad_medidor' => $instalacion->getPropiedad_medidor(),
            'texto_breve_material' => $instalacion->getTexto_breve_material(),
            'constante' => $instalacion->getConstante(),

            // campos nuevos
            'telefono' => $instalacion->getTelefono(),    
            'departamento' => $instalacion->getDepartamento(),
            'porcion' => $instalacion->getPorcion(),
            'ce_emplazamiento' => $instalacion->getCe_emplazamiento(),
            
          );

        $id = (int) $instalacion->getId_instalacion();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_instalacion' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }   
    
    public function updateRiat(Instalacion $instalacion) {    

        $data = array(
                    //'distribuidoras_id_distribuidora' => $instalacion->getDistribuidoras_id_distribuidora(),
                    'nombre_cliente' => $instalacion->getNombre_cliente(),
                    'direccion1' => $instalacion->getDireccion1(),
                    'comunas_id_comuna' => $instalacion->getComunas_id_comuna(),
                    //'num_instalacion' => $instalacion->getNum_instalacion(),
//                    'tarifa' => $instalacion->getTarifa(),
                    'tarifa_terreno' => $instalacion->getTarifa_terreno(),
                    'unidad_lectura' => $instalacion->getUnidad_lectura(),
                    'numero_poste_camara' => $instalacion->getNumero_poste_camara(),
                    'capacidad_automatico' => $instalacion->getCapacidad_automatico(),
                    'potencia_contratada' => $instalacion->getPotencia_contratada(),
                   );

        $id = (int) $instalacion->getId_instalacion();
        
        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('id_instalacion' => $id));
        } else {
            throw new \Exception('Form id does not exist');
        }
    } 
    

//    public function eliminar(Usuario $usuario) {
//        $this->tableGateway->delete(array('id' => $usuario->getId()));
//    }    
//    public function buscarPorNombre($nombre) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where->like('nombre', '%' . $nombre . '%');
//        $select->order("nombre");
//
//        return $this->tableGateway->selectWith($select);
//    }
//    public function obtenerCuenta($email, $clave) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where(array('email' => $email, 'clave' => $clave,));
//
//        return $this->tableGateway->selectWith($select)->current();
//    }
    
    
}
