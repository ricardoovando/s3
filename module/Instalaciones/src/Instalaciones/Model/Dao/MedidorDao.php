<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Instalaciones\Model\Entity\Medidor;

class MedidorDao {


    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        $select = $this->tableGateway->getSql()->select();
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_medidor' => $id));
        $row = $rowset->current();
        return $row;
      }
      
   public function obtenerPorNumSerie($num_serie) {
        $num_serie = (String) $num_serie;
        $rowset = $this->tableGateway->select(array('numero_serie' => $num_serie));
        $row = $rowset->current();
        return $row;
    }   
      
      
   public function obtenerUltimoId() {
        $sql = new sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('medidores_id_medidor_seq');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();
        return $row['last_value'];
     }   
    
    public function guardar(Medidor $medidor) {

        try {
            
            $data = array( 
                    'numero_serie' => $medidor->getNumero_serie(),
                    'act_reac' => $medidor->getAct_reac(),
                    'estado_medidor' => $medidor->getEstado_medidor(),
                    'modelos_medidores_id_modelo' => $medidor->getModelos_medidores_id_modelo(), 
                    );

            $id = (int) $medidor->getId_medidor();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('id_medidor' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
