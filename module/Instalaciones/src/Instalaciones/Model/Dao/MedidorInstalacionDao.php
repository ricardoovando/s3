<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Instalaciones\Model\Entity\MedidorInstalacion;
use Instalaciones\Model\Entity\ModelosMedidores;

class MedidorInstalacionDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
    }

    public function obtenerPorIdInstalacion($idinstalacion) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idinstalacion = (int) $idinstalacion;

        $select = $this->tableGateway->getSql()->select();
        $select->join(array('mo' => 'modelos_medidores_new'), 'mo.id_modelo = medidor_instalacion.modelos_medidores_id_modelo', array('*')); 
        $select->where(array('medidor_instalacion.instalaciones_id_instalacion' => $idinstalacion));

        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('instalaciones_id_instalacion' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(MedidorInstalacion $MedidorInstalacion) {

        try {

            $data = array(
                           'modelos_medidores_id_modelo' => $MedidorInstalacion->getModelos_medidores_id_modelo(),
                           'instalaciones_id_instalacion' => $MedidorInstalacion->getInstalaciones_id_instalacion(),
                           'medidor_actual' => $MedidorInstalacion->getMedidor_actual(),
                           'fecha_instalacion' => $MedidorInstalacion->getFecha_instalacion(),
                           'numero_serie' => $MedidorInstalacion->getNumero_serie(),
                           'anio_fabricacion' => $MedidorInstalacion->getAnio_fabricacion(),
                           'act_reac' => $MedidorInstalacion->getAct_reac(),
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('instalaciones_id_instalacion' => $id));
    }
    
}
