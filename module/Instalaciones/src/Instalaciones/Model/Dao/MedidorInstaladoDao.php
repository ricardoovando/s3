<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Instalaciones\Model\Entity\MedidorInstalado;

class MedidorInstaladoDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        $select = $this->tableGateway->getSql()->select();
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('medidor_instalado_id' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function obtenerPorIdMedidorIdInstalacion($id_medidor, $id_instalacion) {
        $rowset = $this->tableGateway->select(array('medidores_id_medidor' => $id_medidor, 'instalaciones_id_instalacion' => $id_instalacion));
        $row = $rowset->current();
        return $row;
    }

    public function obtenerUltimoId() {
        $sql = new sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('medidor_instalado_medidor_instalado_id_seq');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();
        return $row['last_value'];
    }

    public function guardar(MedidorInstalado $medidorinstalado) {

        try {

            $data = array(
                    'medidores_id_medidor' => $medidorinstalado->getMedidores_id_medidor(),
                    'instalaciones_id_instalacion' => $medidorinstalado->getInstalaciones_id_instalacion(),
                    'medidor_instalado' => $medidorinstalado->getMedidor_instalado(),
                    'fecha_instalacion' => $medidorinstalado->getFecha_instalacion(),
                   );

            $id = (int) $medidorinstalado->getMedidor_instalado_id();

            if ($id == 0) {
                $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('medidor_instalado_id' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
