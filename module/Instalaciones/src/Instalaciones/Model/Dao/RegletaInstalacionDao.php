<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Instalaciones\Model\Entity\RegletaInstalacion;

class RegletaInstalacionDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function guardar(Distribuidoras $distribuidoras) {

        $data = array(
            'distribuidoras_id_distribuidora' => $instalacion->getDistribuidoras_id_distribuidora(),
            'nombre_cliente' => $instalacion->getNombre_cliente(),
            'direccion1' => $instalacion->getDireccion1(),
            'direccion2' => $instalacion->getDireccion2(),
            'num_instalacion' => $instalacion->getNum_instalacion(),
            'tarifa' => $instalacion->getTarifa(),
            'unidad_lectura' => $instalacion->getUnidad_lectura(),
            'numero_poste_camara' => $instalacion->getNumero_poste_camara()
        );

        $id = (int) $instalacion->getId_instalacion();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_instalacion' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
}
