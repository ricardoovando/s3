<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Instalaciones\Model\Entity\Regletas;

class RegletasDao {


    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('instalaciones_id_instalacion' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(Regletas $Regletas) {

        try {
            
            $data = array(
                      'instalaciones_id_instalacion' => $Regletas->getInstalaciones_id_instalacion(),
                      'marca_regleta' => $Regletas->getMarca_regleta(),
                      'modelo_regleta' => $Regletas->getModelo_regleta(),
                      'numero_terminales_regleta' => $Regletas->getNumero_terminales_regleta(),
                      'puentes_corrientes' => $Regletas->getPuentes_corrientes(),
                      'puentes_voltajes' => $Regletas->getPuentes_voltajes(),
                     );

            $id = (int) $Regletas->getInstalaciones_id_instalacion();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('instalaciones_id_instalacion' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
}
