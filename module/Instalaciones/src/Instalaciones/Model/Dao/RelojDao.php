<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Instalaciones\Model\Entity\Reloj;

class RelojDao {


    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('instalaciones_id_instalacion' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(Reloj $Reloj) {

        try {
            
            $data = array(
                    'instalaciones_id_instalacion' => $Reloj->getInstalaciones_id_instalacion(),
                    'codigo_modelo_reloj' => $Reloj->getCodigo_modelo_reloj(),
                    'numero_seria_reloj' => $Reloj->getNumero_seria_reloj(),
                    'marca_reloj' => $Reloj->getMarca_reloj(),
                    );

            $id = (int) $Reloj->getInstalaciones_id_instalacion();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('instalaciones_id_instalacion' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
