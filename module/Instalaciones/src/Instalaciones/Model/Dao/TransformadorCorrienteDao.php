<?php

namespace Instalaciones\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Instalaciones\Model\Entity\TransformadorCorriente;

class TransformadorCorrienteDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
    }

    public function obtenerPorIdInstalacion($idinstalacion) {
        
        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $idinstalacion = (int) $idinstalacion;
        
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('transformador_corriente.instalaciones_id_instalacion' => $idinstalacion));
        
        $rowSet = $this->tableGateway->selectWith($select);
        
        if (!$rowSet) {
            
            throw new \Exception("Could not find row $id");
        }
        
        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('instalaciones_id_instalacion' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(TransformadorCorriente $TransformadorCorriente) {

        try {

            $data = array(
                
                'instalaciones_id_instalacion' => $TransformadorCorriente->getInstalaciones_id_instalacion(),
                'marca' => $TransformadorCorriente->getMarca(), 
                'serie' => $TransformadorCorriente->getSerie(),
                'anio' => $TransformadorCorriente->getAnio(),
                'inp' => $TransformadorCorriente->getInp(),
                'ins' => $TransformadorCorriente->getIns(),
                'num_pasadas' => $TransformadorCorriente->getNum_pasadas(),
                
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('instalaciones_id_instalacion' => $id));
    }
}
