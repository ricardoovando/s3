<?php

namespace Instalaciones\Model\Entity;

class AntecedentesEmpalme {
    
  private $instalaciones_id_instalacion;
  private $tipo_empalme;
  private $acometida;
  private $tipo_circuito;
  private $conexion_circuito;
  private $medida;
  private $voltaje;


  function __construct($instalaciones_id_instalacion = null, $tipo_empalme  = null, 
  $acometida  = null, $tipo_circuito  = null, $conexion_circuito  = null,
   $medida  = null, $voltaje = null) {
   	
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->tipo_empalme = $tipo_empalme;
      $this->acometida = $acometida;
      $this->tipo_circuito = $tipo_circuito;
      $this->conexion_circuito = $conexion_circuito;
      $this->medida = $medida;
	  $this->voltaje = $voltaje;
	  
  }

  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getTipo_empalme() {
      return $this->tipo_empalme;
  }

  public function setTipo_empalme($tipo_empalme) {
      $this->tipo_empalme = $tipo_empalme;
  }

  public function getAcometida() {
      return $this->acometida;
  }

  public function setAcometida($acometida) {
      $this->acometida = $acometida;
  }

  public function getTipo_circuito() {
      return $this->tipo_circuito;
    }

  public function setTipo_circuito($tipo_circuito) {
      $this->tipo_circuito = $tipo_circuito;
    }

  public function getConexion_circuito() {
      return $this->conexion_circuito;
    }

  public function setConexion_circuito($conexion_circuito) {
      $this->conexion_circuito = $conexion_circuito;
    }

  public function getMedida() {
      return $this->medida;
    }

  public function setMedida($medida) {
      $this->medida = $medida;
    }

  public function getVoltaje() {
      return $this->voltaje;
    }

  public function setVoltaje($voltaje) {
       $this->voltaje = $voltaje;
    }

  public function exchangeArray($data) {
  	
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->tipo_empalme = (isset($data['tipo_empalme'])) ? $data['tipo_empalme'] : null;
      $this->acometida = (isset($data['acometida'])) ? $data['acometida'] : null;
      $this->tipo_circuito = (isset($data['tipo_circuito'])) ? $data['tipo_circuito'] : null;
      $this->conexion_circuito = (isset($data['conexion_circuito'])) ? $data['conexion_circuito'] : null;
      $this->medida = (isset($data['medida'])) ? $data['medida'] : null;
	  $this->voltaje = (isset($data['voltaje'])) ? $data['voltaje'] : null;
	  
    }

 public function getArrayCopy() {
        return get_object_vars($this);
    }

}

