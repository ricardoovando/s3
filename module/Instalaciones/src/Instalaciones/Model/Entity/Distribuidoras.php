<?php

namespace Instalaciones\Model\Entity;

class Distribuidoras {

    private $iddistribuidora;
    private $nombre;
    private $direccion;
    private $telefono;

    public function __construct($iddistribuidora = null, $nombre = null, $direccion = null, $telefono = null) {
        $this->iddistribuidora = $iddistribuidora;
        $this->nombre = $nombre;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }

    public function getIddistribuidora() {
        return $this->iddistribuidora;
    }

    public function setIddistribuidora($iddistribuidora) {
        $this->iddistribuidora = $iddistribuidora;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function exchangeArray($data) {
        $this->iddistribuidora = (isset($data['id_distribuidora'])) ? $data['id_distribuidora'] : null;
        $this->nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
        $this->direccion = (isset($data['direccion'])) ? $data['direccion'] : null;
        $this->telefono = (isset($data['telefono'])) ? $data['telefono'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

