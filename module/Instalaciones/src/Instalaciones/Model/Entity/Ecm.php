<?php

namespace Instalaciones\Model\Entity;

class Ecm {

  private $instalaciones_id_instalacion;
  private $modelo;
  private $serie;
  private $anio;
  private $cantidad_elementos;
  private $ip1;
  private $ip2;
  private $ip3;
  private $ip4;
  private $is_ecm;
  private $vs;
  private $vp;
  private $marca;
  private $ttcc1;
  private $ttpp1;
  private $ttcc2;
  private $ttpp2;
    
  function __construct($instalaciones_id_instalacion = null, $modelo = null, $serie = null, $anio = null, $cantidad_elementos = null, $ip1 = null, $ip2 = null, $ip3 = null, $ip4 = null, $is_ecm = null, $vs = null, $vp = null, $marca = null, $ttcc1 = null, $ttpp1 = null, $ttcc2 = null, $ttpp2 = null) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->modelo = $modelo;
      $this->serie = $serie;
      $this->anio = $anio;
      $this->cantidad_elementos = $cantidad_elementos;
      $this->ip1 = $ip1;
      $this->ip2 = $ip2;
      $this->ip3 = $ip3;
      $this->ip4 = $ip4;
      $this->is_ecm = $is_ecm;
      $this->vs = $vs;
      $this->vp = $vp;
      $this->marca = $marca;
      $this->ttcc1 = $ttcc1;
      $this->ttpp1 = $ttpp1;
      $this->ttcc2 = $ttcc2;
      $this->ttpp2 = $ttpp2;
  }
  
  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getModelo() {
      return $this->modelo;
  }

  public function setModelo($modelo) {
      $this->modelo = $modelo;
  }

  public function getSerie() {
      return $this->serie;
  }

  public function setSerie($serie) {
      $this->serie = $serie;
  }

  public function getAnio() {
      return $this->anio;
  }

  public function setAnio($anio) {
      $this->anio = $anio;
  }

  public function getCantidad_elementos() {
      return $this->cantidad_elementos;
  }

  public function setCantidad_elementos($cantidad_elementos) {
      $this->cantidad_elementos = $cantidad_elementos;
  }

  public function getIp1() {
      return $this->ip1;
  }

  public function setIp1($ip1) {
      $this->ip1 = $ip1;
  }

  public function getIp2() {
      return $this->ip2;
  }

  public function setIp2($ip2) {
      $this->ip2 = $ip2;
  }

  public function getIp3() {
      return $this->ip3;
  }

  public function setIp3($ip3) {
      $this->ip3 = $ip3;
  }

  public function getIp4() {
      return $this->ip4;
  }

  public function setIp4($ip4) {
      $this->ip4 = $ip4;
  }

  public function getIs_ecm() {
      return $this->is_ecm;
  }

  public function setIs_ecm($is_ecm) {
      $this->is_ecm = $is_ecm;
  }

  public function getVs() {
      return $this->vs;
  }

  public function setVs($vs) {
      $this->vs = $vs;
  }

  public function getVp() {
      return $this->vp;
  }

  public function setVp($vp) {
      $this->vp = $vp;
  }

  public function getMarca() {
      return $this->marca;
  }

  public function setMarca($marca) {
      $this->marca = $marca;
  }

  public function getTtcc1() {
      return $this->ttcc1;
  }

  public function setTtcc1($ttcc1) {
      $this->ttcc1 = $ttcc1;
  }

  public function getTtpp1() {
      return $this->ttpp1;
  }

  public function setTtpp1($ttpp1) {
      $this->ttpp1 = $ttpp1;
  }

  public function getTtcc2() {
      return $this->ttcc2;
  }

  public function setTtcc2($ttcc2) {
      $this->ttcc2 = $ttcc2;
  }

  public function getTtpp2() {
      return $this->ttpp2;
  }

  public function setTtpp2($ttpp2) {
      $this->ttpp2 = $ttpp2;
  }  

  public function exchangeArray($data) {
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->modelo = (isset($data['modelo'])) ? $data['modelo'] : null;
      $this->serie = (isset($data['serie'])) ? $data['serie'] : null;
      $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
      $this->cantidad_elementos = (isset($data['cantidad_elementos'])) ? $data['cantidad_elementos'] : null;
      $this->ip1 = (isset($data['ip1'])) ? $data['ip1'] : null;
      $this->ip2 = (isset($data['ip2'])) ? $data['ip2'] : null;
      $this->ip3 = (isset($data['ip3'])) ? $data['ip3'] : null;
      $this->ip4 = (isset($data['ip4'])) ? $data['ip4'] : null;
      $this->is_ecm = (isset($data['is_ecm'])) ? $data['is_ecm'] : null;
      $this->vs = (isset($data['vs'])) ? $data['vs'] : null;
      $this->vp = (isset($data['vp'])) ? $data['vp'] : null;
      $this->marca = (isset($data['marca'])) ? $data['marca'] : null;
      $this->ttcc1 = (isset($data['ttcc1'])) ? $data['ttcc1'] : null;
      $this->ttpp1 = (isset($data['ttpp1'])) ? $data['ttpp1'] : null;
      $this->ttcc2 = (isset($data['ttcc2'])) ? $data['ttcc2'] : null;
      $this->ttpp2 = (isset($data['ttpp2'])) ? $data['ttpp2'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

