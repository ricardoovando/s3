<?php

namespace Instalaciones\Model\Entity;

class Instalacion {

    private $id_instalacion;
    private $distribuidoras_id_distribuidora;
    private $comunas_id_comuna;
    private $nombre_cliente;
    private $direccion1;
    private $direccion2;
    private $num_instalacion;
    private $tarifa;
    private $tarifa_terreno;
    private $unidad_lectura;
    private $capacidad_automatico;
    private $potencia_contratada;
    private $geo_poste;
    private $numero_poste_camara;
    private $geo_medidor;
    private $tipo_calle;
    private $nombre_calle;
    private $numero_calle;
    private $blog;
    private $depto_casa;
    private $nombre_condomio;
    private $tipo_condominio;
    private $nom_poblacion;
    
    /*Medidor*/
    private $numero_serie;
    private $material;
    private $marca_medidor;
    private $propiedad_medidor;
    private $texto_breve_material;
    private $constante;
    
    /*Campos Nuevos*/
    private $telefono;    
    private $departamento;
    private $porcion;
    private $ce_emplazamiento;
            

    function __construct($id_instalacion = null, $distribuidoras_id_distribuidora = null, 
            $comunas_id_comuna = null, $nombre_cliente = null, $direccion1 = null, 
            $direccion2 = null, $num_instalacion = null, $tarifa = null,$tarifa_terreno = null, 
            $unidad_lectura = null, $capacidad_automatico = null, $potencia_contratada = null, 
            $geo_poste = null, $numero_poste_camara = null, $geo_medidor = null, $tipo_calle = null, 
            $nombre_calle = null, $numero_calle = null, $blog = null, $depto_casa = null, 
            $nombre_condomio = null, $tipo_condominio = null, $nom_poblacion = null, 
            $numero_serie = null, $material = null, $marca_medidor = null, 
            $propiedad_medidor = null, $texto_breve_material = null, $constante = null,
            $telefono = null, $departamento = null, $porcion = null, $ce_emplazamiento = null) {
        
        $this->id_instalacion = $id_instalacion;
        $this->distribuidoras_id_distribuidora = $distribuidoras_id_distribuidora;
        $this->comunas_id_comuna = $comunas_id_comuna;
        $this->nombre_cliente = $nombre_cliente;
        $this->direccion1 = $direccion1;
        $this->direccion2 = $direccion2;
        $this->num_instalacion = $num_instalacion;
        $this->tarifa = $tarifa;
        $this->tarifa_terreno = $tarifa_terreno;
        $this->unidad_lectura = $unidad_lectura;
        $this->capacidad_automatico = $capacidad_automatico;
        $this->potencia_contratada = $potencia_contratada;
        $this->geo_poste = $geo_poste;
        $this->numero_poste_camara = $numero_poste_camara;
        $this->geo_medidor = $geo_medidor;
        $this->tipo_calle = $tipo_calle;
        $this->nombre_calle = $nombre_calle;
        $this->numero_calle = $numero_calle;
        $this->blog = $blog;
        $this->depto_casa = $depto_casa;
        $this->nombre_condomio = $nombre_condomio;
        $this->tipo_condominio = $tipo_condominio;
        $this->nom_poblacion = $nom_poblacion;
        $this->numero_serie = $numero_serie;
        $this->material = $material;
        $this->marca_medidor = $marca_medidor;
        $this->propiedad_medidor = $propiedad_medidor;
        $this->texto_breve_material = $texto_breve_material;
        $this->constante = $constante;
        
        /*Nuevos Campos*/
        $this->telefono = $telefono;    
        $this->departamento = $departamento;
        $this->porcion = $porcion;
        $this->ce_emplazamiento = $ce_emplazamiento;
    
    }

    
    public function getId_instalacion() {
        return $this->id_instalacion;
    }

    public function setId_instalacion($id_instalacion) {
        $this->id_instalacion = $id_instalacion;
    }

    public function getDistribuidoras_id_distribuidora() {
        return $this->distribuidoras_id_distribuidora;
    }

    public function setDistribuidoras_id_distribuidora($distribuidoras_id_distribuidora) {
        $this->distribuidoras_id_distribuidora = $distribuidoras_id_distribuidora;
    }

    public function getComunas_id_comuna() {
        return $this->comunas_id_comuna;
    }

    public function setComunas_id_comuna($comunas_id_comuna) {
        $this->comunas_id_comuna = $comunas_id_comuna;
    }

    public function getNombre_cliente() {
        return $this->nombre_cliente;
    }

    public function setNombre_cliente($nombre_cliente) {
        $this->nombre_cliente = $nombre_cliente;
    }

    public function getDireccion1() {
        return $this->direccion1;
    }

    public function setDireccion1($direccion1) {
        $this->direccion1 = $direccion1;
    }

    public function getDireccion2() {
        return $this->direccion2;
    }

    public function setDireccion2($direccion2) {
        $this->direccion2 = $direccion2;
    }

    public function getNum_instalacion() {
        return $this->num_instalacion;
    }

    public function setNum_instalacion($num_instalacion) {
        $this->num_instalacion = $num_instalacion;
    }

    public function getTarifa() {
        return $this->tarifa;
    }

    public function setTarifa($tarifa) {
        $this->tarifa = $tarifa;
    }

    public function getTarifa_terreno() {
        return $this->tarifa_terreno;
    }

    public function setTarifa_terreno($tarifa_terreno) {
        $this->tarifa_terreno = $tarifa_terreno;
    }
    
    public function getUnidad_lectura() {
        return $this->unidad_lectura;
    }

    public function setUnidad_lectura($unidad_lectura) {
        $this->unidad_lectura = $unidad_lectura;
    }

    public function getCapacidad_automatico() {
        return $this->capacidad_automatico;
    }

    public function setCapacidad_automatico($capacidad_automatico) {
        $this->capacidad_automatico = $capacidad_automatico;
    }

    public function getPotencia_contratada() {
        return $this->potencia_contratada;
    }

    public function setPotencia_contratada($potencia_contratada) {
        $this->potencia_contratada = $potencia_contratada;
    }

    public function getGeo_poste() {
        return $this->geo_poste;
    }

    public function setGeo_poste($geo_poste) {
        $this->geo_poste = $geo_poste;
    }

    public function getNumero_poste_camara() {
        return $this->numero_poste_camara;
    }

    public function setNumero_poste_camara($numero_poste_camara) {
        $this->numero_poste_camara = $numero_poste_camara;
    }

    public function getGeo_medidor() {
        return $this->geo_medidor;
    }

    public function setGeo_medidor($geo_medidor) {
        $this->geo_medidor = $geo_medidor;
    }

    public function getTipo_calle() {
        return $this->tipo_calle;
    }

    public function setTipo_calle($tipo_calle) {
        $this->tipo_calle = $tipo_calle;
    }

    public function getNombre_calle() {
        return $this->nombre_calle;
    }

    public function setNombre_calle($nombre_calle) {
        $this->nombre_calle = $nombre_calle;
    }

    public function getNumero_calle() {
        return $this->numero_calle;
    }

    public function setNumero_calle($numero_calle) {
        $this->numero_calle = $numero_calle;
    }

    public function getBlog() {
        return $this->blog;
    }

    public function setBlog($blog) {
        $this->blog = $blog;
    }

    public function getDepto_casa() {
        return $this->depto_casa;
    }

    public function setDepto_casa($depto_casa) {
        $this->depto_casa = $depto_casa;
    }

    public function getNombre_condomio() {
        return $this->nombre_condomio;
    }

    public function setNombre_condomio($nombre_condomio) {
        $this->nombre_condomio = $nombre_condomio;
    }

    public function getTipo_condominio() {
        return $this->tipo_condominio;
    }

    public function setTipo_condominio($tipo_condominio) {
        $this->tipo_condominio = $tipo_condominio;
    }
    
    public function getNom_poblacion() {
        return $this->nom_poblacion;
    }

    public function setNom_poblacion($nom_poblacion) {
        $this->nom_poblacion = $nom_poblacion;
    }
    
    public function getNumero_serie() {
        return $this->numero_serie;
    }

    public function setNumero_serie($numero_serie) {
        $this->numero_serie = $numero_serie;
    }

    public function getMaterial() {
        return $this->material;
    }

    public function setMaterial($material) {
        $this->material = $material;
    }

    public function getMarca_medidor() {
        return $this->marca_medidor;
    }

    public function setMarca_medidor($marca_medidor) {
        $this->marca_medidor = $marca_medidor;
    }

    public function getPropiedad_medidor() {
        return $this->propiedad_medidor;
    }

    public function setPropiedad_medidor($propiedad_medidor) {
        $this->propiedad_medidor = $propiedad_medidor;
    }

    public function getTexto_breve_material() {
        return $this->texto_breve_material;
    }

    public function setTexto_breve_material($texto_breve_material) {
        $this->texto_breve_material = $texto_breve_material;
    }

    public function getConstante() {
        return $this->constante;
    }

    public function setConstante($constante) {
        $this->constante = $constante;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function getDepartamento() {
        return $this->departamento;
    }

    public function setDepartamento($departamento) {
        $this->departamento = $departamento;
    }

    public function getPorcion() {
        return $this->porcion;
    }

    public function setPorcion($porcion) {
        $this->porcion = $porcion;
    }

    public function getCe_emplazamiento() {
        return $this->ce_emplazamiento;
    }

    public function setCe_emplazamiento($ce_emplazamiento) {
        $this->ce_emplazamiento = $ce_emplazamiento;
    }

        
    
    public function exchangeArray($data) {

        $this->id_instalacion = (isset($data['id_instalacion'])) ? $data['id_instalacion'] : null;
        $this->distribuidoras_id_distribuidora = (isset($data['distribuidoras_id_distribuidora'])) ? $data['distribuidoras_id_distribuidora'] : null;
        $this->comunas_id_comuna = (isset($data['comunas_id_comuna'])) ? $data['comunas_id_comuna'] : null;
        $this->nombre_cliente = (isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : null;
        $this->direccion1 = (isset($data['direccion1'])) ? $data['direccion1'] : null;
        $this->direccion2 = (isset($data['direccion2'])) ? $data['direccion2'] : null;
        $this->num_instalacion = (isset($data['num_instalacion'])) ? $data['num_instalacion'] : null;
        $this->tarifa = (isset($data['tarifa'])) ? $data['tarifa'] : null;
        $this->tarifa_terreno = (isset($data['tarifa_terreno'])) ? $data['tarifa_terreno'] : null;
        $this->unidad_lectura = (isset($data['unidad_lectura'])) ? $data['unidad_lectura'] : null;
        $this->capacidad_automatico = (isset($data['capacidad_automatico'])) ? $data['capacidad_automatico'] : null;
        $this->potencia_contratada = (isset($data['potencia_contratada'])) ? $data['potencia_contratada'] : null;
        $this->geo_poste = (isset($data['geo_poste'])) ? $data['geo_poste'] : null;
        $this->numero_poste_camara = (isset($data['numero_poste_camara'])) ? $data['numero_poste_camara'] : null;
        $this->geo_medidor = (isset($data['geo_medidor'])) ? $data['geo_medidor'] : null;
        $this->tipo_calle = (isset($data['tipo_calle'])) ? $data['tipo_calle'] : null;
        $this->nombre_calle = (isset($data['nombre_calle'])) ? $data['nombre_calle'] : null;
        $this->numero_calle = (isset($data['numero_calle'])) ? $data['numero_calle'] : null;
        $this->blog = (isset($data['blog'])) ? $data['blog'] : null;
        $this->depto_casa = (isset($data['depto_casa'])) ? $data['depto_casa'] : null;
        $this->nombre_condomio = (isset($data['nombre_condomio'])) ? $data['nombre_condomio'] : null;
        $this->tipo_condominio = (isset($data['tipo_condominio'])) ? $data['tipo_condominio'] : null;
        $this->nom_poblacion = (isset($data['nom_poblacion'])) ? $data['nom_poblacion'] : null;
        $this->numero_serie = (isset($data['numero_serie'])) ? $data['numero_serie'] : null;
        $this->material = (isset($data['material'])) ? $data['material'] : null;
        $this->marca_medidor = (isset($data['marca_medidor'])) ? $data['marca_medidor'] : null;
        $this->propiedad_medidor = (isset($data['propiedad_medidor'])) ? $data['propiedad_medidor'] : null;
        $this->texto_breve_material = (isset($data['texto_breve_material'])) ? $data['texto_breve_material'] : null;
        $this->constante  = (isset($data['constante'])) ? $data['constante'] : null;
        
        /*Nuevos Campos*/
        $this->telefono  = (isset($data['telefono'])) ? $data['telefono'] : null;    
        $this->departamento  = (isset($data['departamento'])) ? $data['departamento'] : null;
        $this->porcion  = (isset($data['porcion'])) ? $data['porcion'] : null;
        $this->ce_emplazamiento  = (isset($data['ce_emplazamiento'])) ? $data['ce_emplazamiento'] : null;
        
        
    }   

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

