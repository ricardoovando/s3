<?php

namespace Instalaciones\Model\Entity;

class MarcasEcms {

    private $idmarca;
    private $nombre_marca;
    private $descripcion;

    public function __construct($idmarca = null, $nombre_marca = null, $descripcion = null) {
        $this->idmarca = $idmarca;
        $this->nombre_marca = $nombre_marca;
        $this->descripcion = $descripcion;
    }

    public function getIdmarca() {
        return $this->idmarca;
    }

    public function setIdmarca($idmarca) {
        $this->idmarca = $idmarca;
    }

    public function getNombre_marca() {
        return $this->nombre_marca;
    }

    public function setNombre_marca($nombre_marca) {
        $this->nombre_marca = $nombre_marca;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idmarca = (isset($data['idmarca'])) ? $data['idmarca'] : null;
        $this->nombre_marca = (isset($data['nombre_marca'])) ? $data['nombre_marca'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

