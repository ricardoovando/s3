<?php

namespace Instalaciones\Model\Entity;

class Medidor {

    private $id_medidor;
    private $modelos_medidores_id_modelo;
    private $numero_serie;
    private $anio_fabricacion;
    private $act_reac;
    private $estado_medidor;

    function __construct($id_medidor = null, $modelos_medidores_id_modelo = null, $numero_serie = null, $anio_fabricacion = null, $act_reac = null, $estado_medidor = null) {

        $this->id_medidor = $id_medidor;
        $this->modelos_medidores_id_modelo = $modelos_medidores_id_modelo;
        $this->numero_serie = $numero_serie;
        $this->anio_fabricacion = $anio_fabricacion;
        $this->act_reac = $act_reac;
        $this->estado_medidor = $estado_medidor;
    }

    public function getId_medidor() {
        return $this->id_medidor;
    }

    public function setId_medidor($id_medidor) {
        $this->id_medidor = $id_medidor;
    }

    public function getModelos_medidores_id_modelo() {
        return $this->modelos_medidores_id_modelo;
    }

    public function setModelos_medidores_id_modelo($modelos_medidores_id_modelo) {
        $this->modelos_medidores_id_modelo = $modelos_medidores_id_modelo;
    }

    public function getNumero_serie() {
        return $this->numero_serie;
    }

    public function setNumero_serie($numero_serie) {
        $this->numero_serie = $numero_serie;
    }

    public function getAnio_fabricacion() {
        return $this->anio_fabricacion;
    }

    public function setAnio_fabricacion($anio_fabricacion) {
        $this->anio_fabricacion = $anio_fabricacion;
    }

    public function getAct_reac() {
        return $this->act_reac;
    }

    public function setAct_reac($act_reac) {
        $this->act_reac = $act_reac;
    }

    public function getEstado_medidor() {
        return $this->estado_medidor;
    }

    public function setEstado_medidor($estado_medidor) {
        $this->estado_medidor = $estado_medidor;
    }

    public function exchangeArray($data) {
        $this->id_medidor = (isset($data['id_medidor'])) ? $data['id_medidor'] : null;
        $this->modelos_medidores_id_modelo = (isset($data['modelos_medidores_id_modelo'])) ? $data['modelos_medidores_id_modelo'] : null;
        $this->numero_serie = (isset($data['numero_serie'])) ? $data['numero_serie'] : null;
        $this->anio_fabricacion = (isset($data['anio_fabricacion'])) ? $data['anio_fabricacion'] : null;
        $this->act_reac = (isset($data['act_reac'])) ? $data['act_reac'] : null;
        $this->estado_medidor = (isset($data['estado_medidor'])) ? $data['estado_medidor'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

