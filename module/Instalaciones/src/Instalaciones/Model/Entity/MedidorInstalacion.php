<?php

namespace Instalaciones\Model\Entity;

use Instalaciones\Model\Entity\ModelosMedidores;

class MedidorInstalacion {

  private $modelos_medidores_id_modelo;
  private $instalaciones_id_instalacion;
  private $medidor_actual;
  private $fecha_instalacion;
  private $numero_serie;
  private $anio_fabricacion;
  private $act_reac;
  
  /*Objeto Para relacional con Medole Medidores*/
  private $modelo_medidor;
              
  function __construct($modelos_medidores_id_modelo = null, $instalaciones_id_instalacion = null, $medidor_actual = null, $fecha_instalacion = null, $numero_serie = null, $anio_fabricacion  = null) {
      $this->modelos_medidores_id_modelo = $modelos_medidores_id_modelo;
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->medidor_actual = $medidor_actual;
      $this->fecha_instalacion = $fecha_instalacion;
      $this->numero_serie = $numero_serie;
      $this->anio_fabricacion = $anio_fabricacion;
  }
  
  public function getModelos_medidores_id_modelo() {
      return $this->modelos_medidores_id_modelo;
  }

  public function setModelos_medidores_id_modelo($modelos_medidores_id_modelo) {
      $this->modelos_medidores_id_modelo = $modelos_medidores_id_modelo;
  }

  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getMedidor_actual() {
      return $this->medidor_actual;
  }

  public function setMedidor_actual($medidor_actual) {
      $this->medidor_actual = $medidor_actual;
  }

  public function getFecha_instalacion() {
      return $this->fecha_instalacion;
  }

  public function setFecha_instalacion($fecha_instalacion) {
      $this->fecha_instalacion = $fecha_instalacion;
  }

  public function getNumero_serie() {
      return $this->numero_serie;
  }

  public function setNumero_serie($numero_serie) {
      $this->numero_serie = $numero_serie;
  }

  public function getAnio_fabricacion() {
      return $this->anio_fabricacion;
  }

  public function setAnio_fabricacion($anio_fabricacion) {
      $this->anio_fabricacion = $anio_fabricacion;
  }

  public function getAct_reac() {
      return $this->act_reac;
  }

  public function setAct_reac($act_reac) {
      $this->act_reac = $act_reac;
  }
  
  public function getModelo_medidor() {
      return $this->modelo_medidor;
  }

  public function setModelo_medidor(ModelosMedidores $modelo_medidor) {
      $this->modelo_medidor = $modelo_medidor;
  }

      
  public function exchangeArray($data) {
      
      $this->modelos_medidores_id_modelo = (isset($data['modelos_medidores_id_modelo'])) ? $data['modelos_medidores_id_modelo'] : null;
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->medidor_actual = (isset($data['medidor_actual'])) ? $data['medidor_actual'] : null;
      $this->fecha_instalacion = (isset($data['fecha_instalacion'])) ? $data['fecha_instalacion'] : null;
      $this->numero_serie = (isset($data['numero_serie'])) ? $data['numero_serie'] : null;
      $this->anio_fabricacion = (isset($data['anio_fabricacion'])) ? $data['anio_fabricacion'] : null;
      $this->act_reac = (isset($data['act_reac'])) ? $data['act_reac'] : null;
      
      $this->modelo_medidor = new \Instalaciones\Model\Entity\ModelosMedidores();
      $this->modelo_medidor->setCodigo_modelo((isset($data['codigo_modelo'])) ? $data['codigo_modelo'] : null);
      
      $this->modelo_medidor->setMarca((isset($data['marca'])) ? $data['marca'] : null);
      
      $this->modelo_medidor->setModelo((isset($data['modelo'])) ? $data['modelo'] : null);
      $this->modelo_medidor->setVoltaje((isset($data['voltaje'])) ? $data['voltaje'] : null);
      
      $this->modelo_medidor->setKp((isset($data['kp'])) ? $data['kp'] : null);
      $this->modelo_medidor->setKq((isset($data['kq'])) ? $data['kq'] : null);        
      
      $this->modelo_medidor->setContante_interna((isset($data['contante_interna'])) ? $data['contante_interna'] : null);
      $this->modelo_medidor->setTecnologia((isset($data['tecnologia'])) ? $data['tecnologia'] : null);
      $this->modelo_medidor->setCorriente_nominal((isset($data['corriente_nominal'])) ? $data['corriente_nominal'] : null);
      $this->modelo_medidor->setCorriente_maxima((isset($data['corriente_maxima'])) ? $data['corriente_maxima'] : null);
      $this->modelo_medidor->setTipo_alimentacion((isset($data['tipo_alimentacion'])) ? $data['tipo_alimentacion'] : null);
      $this->modelo_medidor->setNumero_hilos((isset($data['numero_hilos'])) ? $data['numero_hilos'] : null);
      $this->modelo_medidor->setNumero_elementos((isset($data['numero_elementos'])) ? $data['numero_elementos'] : null);
      $this->modelo_medidor->setTipo_energia((isset($data['tipo_energia'])) ? $data['tipo_energia'] : null);
      $this->modelo_medidor->setDemanda_max((isset($data['demanda_max'])) ? $data['demanda_max'] : null);
      $this->modelo_medidor->setThr((isset($data['thr'])) ? $data['thr'] : null);
      $this->modelo_medidor->setMultifuncion((isset($data['multifuncion'])) ? $data['multifuncion'] : null);
      
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

