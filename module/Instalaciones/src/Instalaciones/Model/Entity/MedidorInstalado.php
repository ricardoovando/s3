<?php

namespace Instalaciones\Model\Entity;

class MedidorInstalado {

    private $medidor_instalado_id;
    private $medidores_id_medidor;
    private $instalaciones_id_instalacion;
    private $medidor_instalado;
    private $fecha_instalacion;

    function __construct($medidor_instalado_id = null, $medidores_id_medidor = null, $instalaciones_id_instalacion = null, $medidor_instalado = null, $fecha_instalacion = null) {

        $this->medidor_instalado_id = $medidor_instalado_id;
        $this->medidores_id_medidor = $medidores_id_medidor;
        $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
        $this->medidor_instalado = $medidor_instalado;
        $this->fecha_instalacion = $fecha_instalacion;
    }

    public function getMedidor_instalado_id() {
        return $this->medidor_instalado_id;
    }

    public function setMedidor_instalado_id($medidor_instalado_id) {
        $this->medidor_instalado_id = $medidor_instalado_id;
    }

    public function getMedidores_id_medidor() {
        return $this->medidores_id_medidor;
    }

    public function setMedidores_id_medidor($medidores_id_medidor) {
        $this->medidores_id_medidor = $medidores_id_medidor;
    }

    public function getInstalaciones_id_instalacion() {
        return $this->instalaciones_id_instalacion;
    }

    public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
        $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
    }

    public function getMedidor_instalado() {
        return $this->medidor_instalado;
    }

    public function setMedidor_instalado($medidor_instalado) {
        $this->medidor_instalado = $medidor_instalado;
    }

    public function getFecha_instalacion() {   
       return $this->fecha_instalacion;
    }

    public function setFecha_instalacion($fecha_instalacion) {
       $this->fecha_instalacion = $fecha_instalacion;
    }
    
    public function exchangeArray($data) {
       $this->medidor_instalado_id = (isset($data['medidor_instalado_id'])) ? $data['medidor_instalado_id'] : null;
       $this->medidores_id_medidor = (isset($data['medidores_id_medidor'])) ? $data['medidores_id_medidor'] : null;
       $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
       $this->medidor_instalado = (isset($data['medidor_instalado'])) ? $data['medidor_instalado'] : null;
       $this->fecha_instalacion = (isset($data['fecha_instalacion'])) ? $data['fecha_instalacion'] : null;    
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

