<?php

namespace Instalaciones\Model\Entity;

class ModelosMedidores {

  private $id_modelo;
  private $codigo_modelo;
  private $marca;
  private $modelo;
  private $voltaje;
  private $kp;
  private $kq;
  private $contante_interna;
  private $tecnologia;
  private $corriente_nominal;
  private $corriente_maxima;
  private $tipo_alimentacion;
  private $numero_hilos;
  private $numero_elementos;
  private $tipo_energia;
  private $demanda_max;
  private $thr;
  private $multifuncion;
  private $amperes;
  private $tipo_conexion;
  private $norma_medida;
  private $clase_medidor;
            
  function __construct($id_modelo = null, $codigo_modelo = null, 
  $marca = null, $modelo = null, $voltaje = null, 
  $kp = null, $kq = null, $contante_interna = null, 
  $tecnologia = null, $corriente_nominal = null, 
  $corriente_maxima = null, $tipo_alimentacion = null, 
  $numero_hilos = null, $numero_elementos = null, 
  $tipo_energia = null, $demanda_max = null, 
  $thr = null, $multifuncion = null, $amperes = null, 
  $tipo_conexion = null, $norma_medida = null, $clase_medidor = null) {
      $this->id_modelo = $id_modelo;
      $this->codigo_modelo = $codigo_modelo;
      $this->marca = $marca;
      $this->modelo = $modelo;
      $this->voltaje = $voltaje;
      $this->kp = $kp;
      $this->kq = $kq;
      $this->contante_interna = $contante_interna;
      $this->tecnologia = $tecnologia;
      $this->corriente_nominal = $corriente_nominal;
      $this->corriente_maxima = $corriente_maxima;
      $this->tipo_alimentacion = $tipo_alimentacion;
      $this->numero_hilos = $numero_hilos;
      $this->numero_elementos = $numero_elementos;
      $this->tipo_energia = $tipo_energia;
      $this->demanda_max = $demanda_max;
      $this->thr = $thr;
      $this->multifuncion = $multifuncion;
	  $this->amperes = $amperes;
      $this->tipo_conexion = $tipo_conexion;
      $this->norma_medida = $norma_medida;
      $this->clase_medidor = $clase_medidor;
  
  }
  
  
  public function getId_modelo() {
      return $this->id_modelo;
  }

  public function setId_modelo($id_modelo) {
      $this->id_modelo = $id_modelo;
  }

  public function getCodigo_modelo() {
      return $this->codigo_modelo;
  }

  public function setCodigo_modelo($codigo_modelo) {
      $this->codigo_modelo = $codigo_modelo;
  }

  public function getMarca() {
      return $this->marca;
  }

  public function setMarca($marca) {
      $this->marca = $marca;
  }

  public function getModelo() {
      return $this->modelo;
  }

  public function setModelo($modelo) {
      $this->modelo = $modelo;
  }

  public function getVoltaje() {
      return $this->voltaje;
  }

  public function setVoltaje($voltaje) {
      $this->voltaje = $voltaje;
  }

  public function getKp() {
      return $this->kp;
  }

  public function setKp($kp) {
      $this->kp = $kp;
  }

  public function getKq() {
      return $this->kq;
  }

  public function setKq($kq) {
      $this->kq = $kq;
  }

  public function getContante_interna() {
      return $this->contante_interna;
  }

  public function setContante_interna($contante_interna) {
      $this->contante_interna = $contante_interna;
  }

  public function getTecnologia() {
      return $this->tecnologia;
  }

  public function setTecnologia($tecnologia) {
      $this->tecnologia = $tecnologia;
  }

  public function getCorriente_nominal() {
      return $this->corriente_nominal;
  }

  public function setCorriente_nominal($corriente_nominal) {
      $this->corriente_nominal = $corriente_nominal;
  }

  public function getCorriente_maxima() {
      return $this->corriente_maxima;
  }

  public function setCorriente_maxima($corriente_maxima) {
      $this->corriente_maxima = $corriente_maxima;
  }

  public function getTipo_alimentacion() {
      return $this->tipo_alimentacion;
  }

  public function setTipo_alimentacion($tipo_alimentacion) {
      $this->tipo_alimentacion = $tipo_alimentacion;
  }

  public function getNumero_hilos() {
      return $this->numero_hilos;
  }

  public function setNumero_hilos($numero_hilos) {
      $this->numero_hilos = $numero_hilos;
  }

  public function getNumero_elementos() {
      return $this->numero_elementos;
  }

  public function setNumero_elementos($numero_elementos) {
      $this->numero_elementos = $numero_elementos;
  }

  public function getTipo_energia() {
      return $this->tipo_energia;
  }

  public function setTipo_energia($tipo_energia) {
      $this->tipo_energia = $tipo_energia;
  }

  public function getDemanda_max() {
      return $this->demanda_max;
  }

  public function setDemanda_max($demanda_max) {
      $this->demanda_max = $demanda_max;
  }

  public function getThr() {
      return $this->thr;
  }

  public function setThr($thr) {
      $this->thr = $thr;
  }

  public function getMultifuncion() {
      return $this->multifuncion;
  }

  public function setMultifuncion($multifuncion) {
      $this->multifuncion = $multifuncion;
  }
 
  public function getAmperes(){
 	 return $this->amperes;		
  }

  public function setAmperes($amperes){
 	  	$this->amperes = $amperes;	
  }

  public function getTipo_conexion(){
 	 return $this->tipo_conexion;		
  }

  public function setTipo_conexion($tipo_conexion){
 	 $this->tipo_conexion = $tipo_conexion;	
  }
  	  
  public function getNorma_medida(){
 	 return $this->norma_medida;		
  }

  public function setNorma_medida($norma_medida){
 	 $this->norma_medida = $norma_medida;	
  }
    
  public function getClase_medidor(){
 	 return $this->clase_medidor;		
  }

  public function setClase_medidor($clase_medidor){
 	 $this->clase_medidor = $clase_medidor;	
  }	  
      

  public function exchangeArray($data) {
      $this->id_modelo = (isset($data['id_modelo'])) ? $data['id_modelo'] : null;
      $this->codigo_modelo = (isset($data['codigo_modelo'])) ? $data['codigo_modelo'] : null;
      $this->marca = (isset($data['marca'])) ? $data['marca'] : null;
      $this->modelo = (isset($data['modelo'])) ? $data['modelo'] : null;
      $this->voltaje = (isset($data['voltaje'])) ? $data['voltaje'] : null;
      $this->kp = (isset($data['kp'])) ? $data['kp'] : null;
      $this->kq = (isset($data['kq'])) ? $data['kq'] : null;
      $this->contante_interna = (isset($data['contante_interna'])) ? $data['contante_interna'] : null;
      $this->tecnologia = (isset($data['tecnologia'])) ? $data['tecnologia'] : null;
      $this->corriente_nominal = (isset($data['corriente_nominal'])) ? $data['corriente_nominal'] : null;
      $this->corriente_maxima = (isset($data['corriente_maxima'])) ? $data['corriente_maxima'] : null;
      $this->tipo_alimentacion = (isset($data['tipo_alimentacion'])) ? $data['tipo_alimentacion'] : null;
      $this->numero_hilos = (isset($data['numero_hilos'])) ? $data['numero_hilos'] : null;
      $this->numero_elementos = (isset($data['numero_elementos'])) ? $data['numero_elementos'] : null;
      $this->tipo_energia = (isset($data['tipo_energia'])) ? $data['tipo_energia'] : null;
      $this->demanda_max = (isset($data['demanda_max'])) ? $data['demanda_max'] : null;
      $this->thr = (isset($data['thr'])) ? $data['thr'] : null;
      $this->multifuncion = (isset($data['multifuncion'])) ? $data['multifuncion'] : null;
	  
	  $this->amperes = (isset($data['amperes'])) ? $data['amperes'] : null;
      $this->tipo_conexion = (isset($data['tipo_conexion'])) ? $data['tipo_conexion'] : null;
      $this->norma_medida = (isset($data['norma_medida'])) ? $data['norma_medida'] : null;
      $this->clase_medidor = (isset($data['clase_medidor'])) ? $data['clase_medidor'] : null;
	  
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}