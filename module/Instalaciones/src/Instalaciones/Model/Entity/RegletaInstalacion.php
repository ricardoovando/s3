<?php

namespace Instalaciones\Model\Entity;

class RegletaInstalacion {

    private $regletas_idregleta;
    private $servicios_idservicio;

    public function __construct(
    $regletas_idregleta = null, $servicios_idservicio = null
    ) {

        $this->regletas_idregleta = $regletas_idregleta;
        $this->servicios_idservicio = $servicios_idservicio;
    }

    public function getRegletas_idregleta() {
        return $this->regletas_idregleta;
    }

    public function setRegletas_idregleta($regletas_idregleta) {
        $this->regletas_idregleta = $regletas_idregleta;
    }

    public function getServicios_idservicio() {
        return $this->servicios_idservicio;
    }

    public function setServicios_idservicio($servicios_idservicio) {
        $this->servicios_idservicio = $servicios_idservicio;
    }

    public function exchangeArray($data) {
        $this->regletas_idregleta = (isset($data['regletas_idregleta'])) ? $data['regletas_idregleta'] : null;
        $this->servicios_idservicio = (isset($data['servicios_idservicio'])) ? $data['servicios_idservicio'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

