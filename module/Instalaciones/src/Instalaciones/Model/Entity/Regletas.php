<?php

namespace Instalaciones\Model\Entity;

class Regletas {

  private $instalaciones_id_instalacion;
  private $marca_regleta;
  private $modelo_regleta;
  private $numero_terminales_regleta;
  private $puentes_corrientes;
  private $puentes_voltajes;
    

  function __construct($instalaciones_id_instalacion = null, $marca_regleta = null, $modelo_regleta = null, $numero_terminales_regleta = null, $puentes_corrientes = null, $puentes_voltajes = null) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->marca_regleta = $marca_regleta;
      $this->modelo_regleta = $modelo_regleta;
      $this->numero_terminales_regleta = $numero_terminales_regleta;
      $this->puentes_corrientes = $puentes_corrientes;
      $this->puentes_voltajes = $puentes_voltajes;
    }

  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getMarca_regleta() {
      return $this->marca_regleta;
  }

  public function setMarca_regleta($marca_regleta) {
      $this->marca_regleta = $marca_regleta;
  }

  public function getModelo_regleta() {
      return $this->modelo_regleta;
  }

  public function setModelo_regleta($modelo_regleta) {
      $this->modelo_regleta = $modelo_regleta;
  }

  public function getNumero_terminales_regleta() {
      return $this->numero_terminales_regleta;
  }

  public function setNumero_terminales_regleta($numero_terminales_regleta) {
      $this->numero_terminales_regleta = $numero_terminales_regleta;
  }
  
  public function getPuentes_corrientes() {
      return $this->puentes_corrientes;
  }

  public function setPuentes_corrientes($puentes_corrientes) {
      $this->puentes_corrientes = $puentes_corrientes;
  }

  public function getPuentes_voltajes() {
      return $this->puentes_voltajes;
  }

  public function setPuentes_voltajes($puentes_voltajes) {
      $this->puentes_voltajes = $puentes_voltajes;
  }

  public function exchangeArray($data) {
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->marca_regleta = (isset($data['marca_regleta'])) ? $data['marca_regleta'] : null;
      $this->modelo_regleta = (isset($data['modelo_regleta'])) ? $data['modelo_regleta'] : null;
      $this->numero_terminales_regleta = (isset($data['numero_terminales_regleta'])) ? $data['numero_terminales_regleta'] : null;
      $this->puentes_corrientes = (isset($data['puentes_corrientes'])) ? $data['puentes_corrientes'] : null;
      $this->puentes_voltajes = (isset($data['puentes_voltajes'])) ? $data['puentes_voltajes'] : null;     
  }

  public function getArrayCopy() {
      return get_object_vars($this);
  }

}

