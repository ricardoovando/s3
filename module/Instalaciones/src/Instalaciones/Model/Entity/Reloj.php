<?php

namespace Instalaciones\Model\Entity;

class Reloj {

  private $instalaciones_id_instalacion;
  private $codigo_modelo_reloj;
  private $numero_seria_reloj;
  private $marca_reloj;

  function __construct($instalaciones_id_instalacion = null, $codigo_modelo_reloj = null, $numero_seria_reloj = null, $marca_reloj = null) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->codigo_modelo_reloj = $codigo_modelo_reloj;
      $this->numero_seria_reloj = $numero_seria_reloj;
      $this->marca_reloj = $marca_reloj;
  }

  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getCodigo_modelo_reloj() {
      return $this->codigo_modelo_reloj;
  }

  public function setCodigo_modelo_reloj($codigo_modelo_reloj) {
      $this->codigo_modelo_reloj = $codigo_modelo_reloj;
  }

  public function getNumero_seria_reloj() {
      return $this->numero_seria_reloj;
  }

  public function setNumero_seria_reloj($numero_seria_reloj) {
      $this->numero_seria_reloj = $numero_seria_reloj;
  }

  public function getMarca_reloj() {
      return $this->marca_reloj;
  }

  public function setMarca_reloj($marca_reloj) {
      $this->marca_reloj = $marca_reloj;
  }

  public function exchangeArray($data) {
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->codigo_modelo_reloj = (isset($data['codigo_modelo_reloj'])) ? $data['codigo_modelo_reloj'] : null;
      $this->numero_seria_reloj = (isset($data['numero_seria_reloj'])) ? $data['numero_seria_reloj'] : null;
      $this->marca_reloj = (isset($data['marca_reloj'])) ? $data['marca_reloj'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

