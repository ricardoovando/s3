<?php

namespace Instalaciones\Model\Entity;

class SistemaDistribucion {
    
  private $instalaciones_id_instalacion;
  private $potencia_ssee;
  private $num_ssee;
  private $alimentador;
  
  function __construct($instalaciones_id_instalacion = null, $potencia_ssee = null, $num_ssee = null, $alimentador = null) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->potencia_ssee = $potencia_ssee;
      $this->num_ssee = $num_ssee;
      $this->alimentador = $alimentador;
  }
  
  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }
  
  public function getPotencia_ssee() {
      return $this->potencia_ssee;
  }
  
  public function setPotencia_ssee($potencia_ssee) {
      $this->potencia_ssee = $potencia_ssee;
  }
  
  public function getNum_ssee() {
      return $this->num_ssee;
  }
  
  public function setNum_ssee($num_ssee) {
      $this->num_ssee = $num_ssee;
  }
  
  public function getAlimentador() {
      return $this->alimentador;
  }
  
  public function setAlimentador($alimentador) {
      $this->alimentador = $alimentador;
  }
  
  public function exchangeArray($data) {
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->potencia_ssee = (isset($data['potencia_ssee'])) ? $data['potencia_ssee'] : null;
      $this->num_ssee = (isset($data['num_ssee'])) ? $data['num_ssee'] : null;
      $this->alimentador = (isset($data['alimentador'])) ? $data['alimentador'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

