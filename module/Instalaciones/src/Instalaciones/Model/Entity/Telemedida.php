<?php

namespace Instalaciones\Model\Entity;

class Telemedida {

    private $instalaciones_id_instalacion;
    private $modem;
    private $alim_auxiliar;
    private $num_serie;

    public function __construct(
    $instalaciones_id_instalacion= null, $modem = null, $alim_auxiliar = null, $num_serie = null, $puentes_corrientes = null, $puentes_voltajes = null
    ) {
        $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
        $this->modem = $modem;
        $this->alim_auxiliar = $alim_auxiliar;
        $this->num_serie = $num_serie;
    }

    public function getInstalaciones_id_instalacion() {
        return $this->instalaciones_id_instalacion;
    }

    public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
        $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
    }

    public function getModem() {
        return $this->modem;
    }

    public function setModem($modem) {
        $this->modem = $modem;
    }

    public function getAlim_auxiliar() {
        return $this->alim_auxiliar;
    }

    public function setAlim_auxiliar($alim_auxiliar) {
        $this->alim_auxiliar = $alim_auxiliar;
    }

    public function getNum_serie() {
        return $this->num_serie;
    }

    public function setNum_serie($num_serie) {
        $this->num_serie = $num_serie;
    }

    public function exchangeArray($data) {
        $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
        $this->modem = (isset($data['modem'])) ? $data['modem'] : null;
        $this->alim_auxiliar = (isset($data['alim_auxiliar'])) ? $data['alim_auxiliar'] : null;
        $this->num_serie = (isset($data['num_serie'])) ? $data['num_serie'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

