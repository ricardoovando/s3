<?php

namespace Instalaciones\Model\Entity;

class TransformadorCorriente {

  private $instalaciones_id_instalacion;
  private $marca;
  private $serie;
  private $anio;
  private $inp;
  private $ins;
  private $num_pasadas;
            

  function __construct($instalaciones_id_instalacion = null, $marca = null, $serie = null, $anio = null, $inp = null, $ins = null, $num_pasadas = null) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
      $this->marca = $marca;
      $this->serie = $serie;
      $this->anio = $anio;
      $this->inp = $inp;
      $this->ins = $ins;
      $this->num_pasadas = $num_pasadas;
  }

  public function getInstalaciones_id_instalacion() {
      return $this->instalaciones_id_instalacion;
  }

  public function setInstalaciones_id_instalacion($instalaciones_id_instalacion) {
      $this->instalaciones_id_instalacion = $instalaciones_id_instalacion;
  }

  public function getMarca() {
      return $this->marca;
  }

  public function setMarca($marca) {
      $this->marca = $marca;
  }

  public function getSerie() {
      return $this->serie;
  }

  public function setSerie($serie) {
      $this->serie = $serie;
  }

  public function getAnio() {
      return $this->anio;
  }

  public function setAnio($anio) {
      $this->anio = $anio;
  }

  public function getInp() {
      return $this->inp;
  }

  public function setInp($inp) {
      $this->inp = $inp;
  }

  public function getIns() {
      return $this->ins;
  }

  public function setIns($ins) {
      $this->ins = $ins;
  }

  public function getNum_pasadas() {
      return $this->num_pasadas;
  }

  public function setNum_pasadas($num_pasadas) {
      $this->num_pasadas = $num_pasadas;
  }
  
  public function exchangeArray($data) {
      $this->instalaciones_id_instalacion = (isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null;
      $this->marca = (isset($data['marca'])) ? $data['marca'] : null;
      $this->serie = (isset($data['serie'])) ? $data['serie'] : null;
      $this->anio = (isset($data['anio'])) ? $data['anio'] : null;
      $this->inp = (isset($data['inp'])) ? $data['inp'] : null;
      $this->ins = (isset($data['ins'])) ? $data['ins'] : null;
      $this->num_pasadas = (isset($data['num_pasadas'])) ? $data['num_pasadas'] : null;
  }

  public function getArrayCopy() {
     return get_object_vars($this);
  }

}

