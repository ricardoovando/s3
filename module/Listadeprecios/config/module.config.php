<?php

return array(
    'controllers' => array(
        'invokables' => array(
        ),
    ),
    'router' => array(
        'routes' => array(
            'listadeprecios' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/listadeprecios[/:controller][/:action][/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Listadeprecios\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'paginatorlistadeprecios' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/listadeprecios[/:controller][/:action]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Listadeprecios\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'listadeprecios' => __DIR__ . '/../view',
        ),
    ),
);
