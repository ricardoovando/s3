<?php

namespace Servicios\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\I18n\Validator\Alnum;
use Zend\I18n\Validator\Int;
use Zend\Validator\StringLength;
use Zend\Validator\NotEmpty;


class RegistroValidator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => true,
        'messages' => array(
            Alnum::INVALID => "Tipo inválido dado. Cadena, un entero o flotante esperada",
            Alnum::NOT_ALNUM => "La entrada contiene caracteres que no son alfabéticos y sin dígitos",
            Alnum::STRING_EMPTY => "La entrada es una cadena vacía",
        )
    );
    
    
    protected $opcionesInt = array(
        'allowWhiteSpace' => false,
        'messages' => array(
           Int::NOT_INT => "La entrada no parece ser un número entero",
           Int::INVALID => "Tipo inválido dado. Cadena o entero espera",
         )  
    );
    
    
   protected $opcionesStringLength = array(
       'allowWhiteSpace' => true,
        'min' => 4,
        'max' => 50,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 4 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 25 caracteres",
        )
    );
        
    protected $opcionesStringLength_1 = array(
        'allowWhiteSpace' => true,
        'min' => 4,
        'max' => 50,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 4 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 50 caracteres",
        )
    );
    
    protected $opcionesStringLength_2 = array(
        'min' => 1,
        'max' => 2,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 1 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 2 caracteres",
        )
    );
        
    protected $optionesNotEmpty = array(
        'messages' => array(
            NotEmpty::IS_EMPTY => "Se requiere de valor y no puede estar vacío",
            NotEmpty::INVALID => "Tipo inválido dado. Cadena, entero, flotante, booleano o matriz esperada",
        ),
    );


    public function __construct() {

        $this->add(
                array(
                    'name' => 'nombre_servicio',
                    'required' => true,
                    'filters' => array(
                          array( 'name' => 'StringToLower' ),
                          array( 'name' => 'StringTrim' ),
                          array( 'name' => 'StripTags' ),
                          array( 'name' => 'StripNewlines' ),
                          array( 'name' => 'HtmlEntities' ),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
                    ),
                )
        );


        
                
    }

}

