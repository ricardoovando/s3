<?php

namespace Listadeprecios\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Listadeprecios\Model\Entity\Listadeprecios;

class ListadepreciosDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {

        $select = $this->tableGateway->getSql()->select();
        $select->order("id_servicio");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {

        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_servicio' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function eliminar(Servicio $servicio) {
        $this->tableGateway->delete(array('id_servicio' => $servicio->getId()));
    }

    public function guardar(Servicio $servicio) {

        $data = array(
            'nombre_servicio' => $servicio->getNombre_servicio(),
            'detalleservicio' => $servicio->getDetalleservicio(),
            'contrato' => $servicio->getContrato(),
        );
        
        
        $id = (int) $servicio->getId_servicio();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_servicio' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorNombreServicio($nombreservicio) {

        $nombreservicio = (String) $nombreservicio;
        $select = $this->tableGateway->getSql()->select();
        $select->where->like('nombre_servicio', '%' . $nombreservicio . '%');
        $select->order('id_servicio');

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

}
