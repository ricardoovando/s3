<?php

namespace Listadeprecios\Model\Entity;

class Listadeprecios {

    private $sucursales_id_sucursal;
    private $servicios_id_servicio;
    private $precio_uf;

    
    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getServicios_id_servicio() {
        return $this->servicios_id_servicio;
    }

    public function setServicios_id_servicio($servicios_id_servicio) {
        $this->servicios_id_servicio = $servicios_id_servicio;
    }

    public function getPrecio_uf() {
        return $this->precio_uf;
    }

    public function setPrecio_uf($precio_uf) {
        $this->precio_uf = $precio_uf;
    }
    
    public function exchangeArray($data) {  
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        $this->servicios_id_servicio = (isset($data['servicios_id_servicio'])) ? $data['servicios_id_servicio'] : null;
        $this->precio_uf = (isset($data['precio_uf'])) ? $data['precio_uf'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

