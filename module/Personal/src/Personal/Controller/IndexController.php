<?php

namespace Personal\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Validator\File\Size;
use Zend\Http\PhpEnvironment\Request;

use Personal\Form\Registro as PersonaForm;
use Personal\Form\RegistroValidator;

use Personal\Form\Buscador as BuscadorForm;
use Personal\Form\BuscadorValidator;

use Personal\Model\Entity\Persona;

class IndexController extends AbstractActionController {

    private $PersonaDao;
    private $config;
    private $login;
    private $avisoDao;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function getAvisoDao() {
        if (!$this->avisoDao) {
            $sm = $this->getServiceLocator();
            $this->avisoDao = $sm->get('Avisos\Model\AvisoDao');
        }
        return $this->avisoDao;
    }
      
    public function setPersonaDao($personadao) {
        $this->PersonaDao = $personadao;
    }

    public function getPersonaDao() {
        return $this->PersonaDao;
    }

    public function getConfig() {
        return $this->config;
    }

    private function getPersonaForm() {
        $form = new PersonaForm();
        $form->get('sucursales_id_sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form;
    }

    private function getFormBuscador() {
        return new BuscadorForm();
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $form = $this->getFormBuscador();
        $paginator = $this->getPersonaDao()->obtenerTodos();
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(20);

        return new ViewModel(array(
            'title' => 'Listado de Personal',
            'listadepersonal' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $form,
        ));
    }

    public function crearAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $form = $this->getPersonaForm();

        return new ViewModel(array('title' => 'Crear Registro', 'form' => $form));
    }

    public function buscarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('personal', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getFormBuscador();
        $form = $this->getFormBuscador();
        $form->setInputFilter(new BuscadorValidator());
        $form->setData($postParams);
        

        if (!$form->isValid()) {
            
            // Falla la validación; volvemos a generar el formulario 
            $paginator = $this->getPersonaDao()->obtenerTodos();
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
            $paginator->setItemCountPerPage(20);

            $viewModel = new ViewModel(array(
                                            'title' => 'Error',
                                            'listadepersonal' => $paginator->getIterator(),
                                            'paginator' => $paginator,
                                            'form' => $form,
                                        ));
            
            $viewModel->setTemplate("personal/index/index");

            return $viewModel;
            
         }

        $values = $form->getData();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getPersonaDao()->buscarPorNombrePersona($values['nombrepersona']);
        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(20);

        /* Configuracion de Vista */
        $viewModel = new ViewModel(array(
            'title' => sprintf('Datos Encontrados', $paginator->count()),
            'listadepersonal' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $form,
        ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("personal/index/index");

        return $viewModel;
    }

    
    /**
     * @method string getString()
     * @method void setInteger(integer $integer)
     * @method setString(integer $integer)
    */
    public function registrarAction(){

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if(!$this->getRequest()->isPost()){
            $this->redirect()->toRoute('personal', array('controller' => 'index'));
          }
        
        $form = $this->getPersonaForm();

        /*Parametros Post*/
        $postParams = $this->request->getPost();

        /* Array con todos los FILES ELEMENTS */
        $fileParams = $this->params()->fromFiles();
            
        /*
         * funcion que ejecuta validaciones a las imagenes y carga en carga en carpeta public
         */
        
          if(!empty($fileParams["img_carnet"]["name"])){
              
                $nombreImagenFotoCarnet = $this->cargaFotosAction("img_carnet",$fileParams["img_carnet"]);
             
              } 
              
          if(!empty($fileParams["img_firma"]["name"])){

                $nombreImagenesImgFirma = $this->cargaFotosAction("img_firma",$fileParams['img_firma']); 
             
              }
              
        /* Si esta funcion retorna true continua con el ingreso de datos en la tabla personal */
        
        /* Validacion de formularios */
        $form = $this->getPersonaForm();
        $form->setInputFilter(new RegistroValidator());
        $form->setData($postParams);

        if (!$form->isValid()) {
               $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
               $modelView->setTemplate('personal/index/crear');
               return $modelView;
            }

        $values = $form->getData();

        $values['sucursales_id_sucursal'] = $this->getLogin()->getIdentity()->sucursales_id_sucursal;
        
        if(!empty($nombreImagenFotoCarnet)) $values['foto_carnet'] = $nombreImagenFotoCarnet;
        
        if(!empty($nombreImagenesImgFirma)) $values['foto_firma'] = $nombreImagenesImgFirma;
             
        $persona = new Persona();
        $persona->exchangeArray($values);

        $this->getPersonaDao()->guardar($persona);

        return $this->redirect()->toRoute('personal', array('controller' => 'index'));
        
    }
    
    
    
   private function cargaFotosAction($nombreElementFile,$arrayElementFile){
        
        $form = $this->getPersonaForm();
        $nombresImagenes = (String) "";
        
        
        /* 
         * se ejecuta el ciclo segun la cantidad de elementos type file 
        */
       
         if($arrayElementFile['error'] == 0){

                $cargado = false; 
                
                $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 10000000));
                $validatorExt = new \Zend\Validator\File\Extension('jpg,jpeg,png,gif');
                $validatorMime = new \Zend\Validator\File\MimeType('image/jpg,image/jpeg,image/png,image/gif');
                $validatorIsImage = new \Zend\Validator\File\IsImage();
                //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));
        
                $adapter = new \Zend\File\Transfer\Adapter\Http(); 
                
                $results = array();
                    $results['size'] = $validatorSize->isValid( $arrayElementFile['name'] );
                    $results['ext'] = $validatorExt->isValid( $arrayElementFile['name'] );
                    $results['mime'] = $validatorMime->isValid( $arrayElementFile['name'] );
                    $results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
                    //$results['Count'] = $validatorCount->isValid($fileParams['name']);
                        
                $adapter->setValidators(array(
                                          $validatorSize,
                                          $validatorExt,
                                          $validatorMime,
                                          $validatorIsImage,
                                        ),  $arrayElementFile['name'] );
                
                //\Zend\Debug\Debug::dump($results);
                    
                    
                /*Si existe error en la validacion se retorna al formulario con los msg de error*/
                if (!$adapter->isValid($nombreElementFile)) {

                                       
                    $dataError = $adapter->getMessages();
                    $error = array();
                    
                    foreach($dataError as $key=>$row)
                    {
                      $error[] = $row;
                    }
                    
                    
                       /* 
                         * retornamos a la vista crear donde esta el formulario con los msg de error
                        */
                        $form->setMessages(array( $nombreElementFile => $error ));
                        $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
                        $modelView->setTemplate('personal/index/crear');
                        return $modelView; 
                        
                    
                } else {

                    
                    /* Directorio donde se cargan las fotos dentro del modulo */
                    ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');
                    
                    $adapter->setDestination('/home/meditres/public_html/public/images/fotos/');
        

                    /*
                     * extencion del archivo
                     */
                    
                    $extension = pathinfo( $arrayElementFile['name'] , PATHINFO_EXTENSION);

                    
                    /*
                     * se modifica el nombre del archivo se anade un calculo aleatorio
                     */
                    
                    ///var/www/html/sam/public/images/fotos/
                    
                    $adapter->addFilter('Rename', array(
                                'target' => '/home/meditres/public_html/public/images/fotos/' . $nombreElementFile . '.' . $extension,
                                'overwrite' => true,
                                'randomize' => true, ));
                    
                    /*
                     * se cargar el archivo en forma exitosa
                     */
                   
                    if ($adapter->receive( $arrayElementFile['name'] )) {
                         
                        $nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                        
                        $nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                        
                    }
                } 
                
         }
         
         /*
          *  si el archivo no llega al servidor se envia msg de error
          */
         
          if(!$arrayElementFile['error'] == 0){
              
              $error = array('Error del Archivo Cargado Codigo Error : '. $arrayElementFile['error'] );
              
              /* 
               * retorna a la vista con los msg de error correspondientes
               */
              $form->setMessages(array( $nombreElementFile => $error ));
              $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
              $modelView->setTemplate('personal/index/crear');
              return $modelView;    
              
            }

          
          unset($adapter);

        /* Se retorna un arregle con los nombres de las imagenes */
        return $nombresImagenes;
        
    }
    

    public function editarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $id = (int) $this->params()->fromRoute('id', 0);
        
        if (!$id) {
            return $this->redirect()->toRoute('personal', array('controller' => 'index'));
          }
        
        $persona = $this->getPersonaDao()->obtenerPorId($id);
        
        $form = $this->getPersonaForm();
        $form->bind($persona);
        $form->get('send')->setAttribute('value', 'Editar');
        
        $nombreFotoCarnet = $persona->getFoto_carnet();
        $nombreFotoFirma = $persona->getFoto_firma();
        
        
        $modelView = new ViewModel(array('title' => 'Editar Registro', 'form' => $form , 'nombreFotoCarnet' => $nombreFotoCarnet , 'nombreFotoFirma' => $nombreFotoFirma ));
        $modelView->setTemplate('personal/index/crear');
        return $modelView;
    }

    public function eliminarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('personal', array('controller' => 'index'));
        }
        $persona = new Persona();
        $persona->setId($id);
        $this->getPersonaDao()->eliminar($persona);
        return $this->redirect()->toRoute('personal', array('controller' => 'index'));
    }

}
