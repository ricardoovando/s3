<?php

namespace Personal\Form;

use Zend\Form\Form;

class Buscador extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombrepersona',
            'options' => array(
                'label' => 'Buscar Nombre :',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn btn-success',
                'style' => 'height:30px;',
            ),
        ));
    }

}

