<?php

namespace Personal\Form;

use Zend\Form\Form;

class Registro extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');


        $this->add(array(
            'name' => 'id_persona',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sucursales_id_sucursal',
            'options' => array(
                'label' => 'Sucursales : ',
                'empty_option' => 'Seleccionar',
            ),
            'attributes' => array(
                //'style' => 'margin:auto;',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rut_persona',
            'options' => array(
                'label' => 'Rut : ',
            ),
            'attributes' => array(
                'required' => 'required', 
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombres',
            'options' => array(
                'label' => 'Nombres : ',
            ),
            'attributes' => array(
            'required' => 'required', 
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'apellidos',
            'options' => array(
                'label' => 'Apellidos : ',
            ),
            'attributes' => array(
            'required' => 'required', 
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cargos_idcargo',
            'options' => array(
                'label' => 'Cargo : ',
            //'empty_option' => 'Seleccione un tipo',
            ),
            'attributes' => array(
                'options' => array(
                      0 => 'Seleccionar',
                      1  => 'ELECTRICISTA JEFE GRUPO',
                      2  => 'INGENIERO MEDICION',              
                      3  => 'JEFE ZONAL',                 
                      4  => 'ELECTRICISTA MAESTRO PRIMERA',
                      5  => 'ELECTRICISTA MAESTRO SEGUNDA',
                      6  => 'LABORATORISTA',     
                      7  => 'AYUDANTE',    
                      8  => 'ADMINISTRATIVO',  
                     10  => 'OPERADOR GAVIS',  
                     11  => 'ASISTENTE ADMINISTRATIVO TRIFASICO',
                     12  => 'ELECTRICISTA  AYUDANTE',
                     13  => 'ASISTENTE GESTIÓN PERSONAS',                            
                     14  => 'GERENTE OPERACIONES TEC.',                            
                     15  => 'JEFE MEDICION TRIFASICO',                           
                     16  => 'ANALISTA CGED',                          
                     17  => 'ANALISTA  DATOS',                            
                     18  => 'AUDITOR INTERNO',                           
                     19  => 'JEFE MEDICION MONOFASICO',                            
                     20  => 'ASISTENTE ADMINISTRATIVO MONOFASICO', 
                 ),
               )
            ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'email',
            'options' => array(
                'label' => 'Email : ',
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'telefono',
            'options' => array(
                'label' => 'Telefono : ',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'celular',
            'options' => array(
                'label' => 'Celular : ',
             )
          ));

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'direccion',
//            'options' => array(
//                'label' => 'Direccion : ',
//            )
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'comunas_id_comuna',
//            'options' => array(
//                'label' => 'Comuna : ',
//                'empty_option' => 'Seleccione',
//            ),
//            'attributes' => array(
//                'options' => array(
//                    1 => 'Comuna 1',
//                  ),
//                'required' => 'required',
//                )
//            ));


        $this->add(array(
            'name' => 'img_carnet',
            'type' => 'File',
            'attributes' => array(
                
            ),
            'options' => array(
                'label' => 'Foto Carnet : ',
            ),
        ));
        
        $this->add(array(
            'name' => 'foto_carnet',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        

        $this->add(array(
            'name' => 'img_firma',
            'type' => 'File',
            'attributes' => array(

            ),
            'options' => array(
                'label' => 'IMG Firma : ',
            ),
        ));

        $this->add(array(
            'name' => 'foto_firma',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        
        $this->add(array( 
            'name' => 'csrf', 
            'type' => 'Zend\Form\Element\Csrf', 
        )); 

        /* TERMINO */
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
    }

}

