<?php

namespace Personal\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;

use Zend\I18n\Validator\Alnum;
use Zend\I18n\Validator\Int;
use Zend\Validator\StringLength;
use Zend\Validator\NotEmpty;

use Zend\InputFilter\FileInput;


class RegistroValidator extends InputFilter {
    
    
    protected $filtros = array(
                        array('name' => 'StringToLower'),
                        array('name' => 'StringTrim'),
                        array('name' => 'StripTags'),
                        array('name' => 'StripNewlines'),
                        array('name' => 'HtmlEntities'),
                    );

    protected $opcionesAlnum = array(
        'encoding' => 'UTF-8',
        'allowWhiteSpace' => true,
        'messages' => array(
            Alnum::INVALID => "Tipo inválido dado. Cadena, un entero o flotante esperada",
            Alnum::NOT_ALNUM => "La entrada contiene caracteres que no son alfabéticos y sin dígitos",
            Alnum::STRING_EMPTY => "La entrada es una cadena vacía",
        )
    );
    
    
    protected $opcionesInt = array(
        'encoding' => 'UTF-8',
        'allowWhiteSpace' => false,
        'messages' => array(
           Int::NOT_INT => "La entrada no parece ser un número entero",
           Int::INVALID => "Tipo inválido dado. Cadena o entero espera",
         )  
    );
    
    
   protected $opcionesStringLength_RUT = array(
       'encoding' => 'UTF-8',
        'min' => 9,
        'max' => 11,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 9 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 11 caracteres",
        )
    );
        
    protected $opcionesStringLength_NOMBRES = array(
        'encoding' => 'UTF-8',
        'min' => 3,
        'max' => 30,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 3 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 30 caracteres",
        )
    );
    
    protected $opcionesStringLength_2 = array(
        'encoding' => 'UTF-8',
        'min' => 1,
        'max' => 2,
        'messages' => array(
            StringLength::INVALID => "Tipo inválido dado. cadena esperada",
            StringLength::TOO_SHORT => "El campo debe tener al menos 1 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 2 caracteres",
        )
    );
        
    protected $optionesNotEmpty = array(
        'encoding' => 'UTF-8',
        'messages' => array(
            NotEmpty::IS_EMPTY => "Se requiere de valor y no puede estar vacío",
            NotEmpty::INVALID => "Tipo inválido dado. Cadena, entero, flotante, booleano o matriz esperada",
        ),
    );


    public function __construct() {

        
        /* INICIO */
        $this->add(
                array(
                    'name' => 'rut_persona',
                    'required' => true,
                    'filters' => $this->filtros ,
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength_RUT
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
                        array(
                            'name' => 'Alnum',
                            'options' => $this->opcionesAlnum
                        )
                    ),
                )
        );
       /* FIN */
        
        /* INICIO */
        $this->add(
                array(
                    'name' => 'nombres',
                    'required' => true,
                    'filters' => $this->filtros ,
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength_NOMBRES
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
//                        array(
//                            'name' => 'Alnum',
//                            'options' => $this->opcionesAlnum
//                        )
                    ),
                )
        );
       /*  FIN */

        
        /* INICIO */
        $this->add(
                array(
                    'name' => 'apellidos',
                    'required' => true,
                    'filters' => $this->filtros ,
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => $this->opcionesStringLength_NOMBRES
                        ),
                        array(
                            'name' => 'NotEmpty',
                            'options' => $this->optionesNotEmpty
                        ),
//                        array(
//                            'name' => 'Alnum',
//                            'options' => $this->opcionesAlnum
//                        )
                    ),
                 )
              );
       /*  FIN */
                
    }

}

