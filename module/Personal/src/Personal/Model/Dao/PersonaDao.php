<?php

namespace Personal\Model\Dao;


use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Personal\Model\Entity\Persona;

class PersonaDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {

        $id_usuario = $this->login->id;
        
        $select = $this->tableGateway->getSql()->select();
        
        $select->join( array( 'car' => 'cargos' ) , 'car.idcargo = personal.cargos_idcargo' , array( 'idcargo' => 'idcargo' , 'nombre_cargo'=>'nombre_cargo' , 'descripcion' => 'descripcion' ));
        
       // $select->join( array( 'com' => 'comunas' ) , 'com.id_comuna = personal.comunas_id_comuna' , array( 'id_comuna' => 'id_comuna','provincias_id_provincia' => 'provincias_id_provincia','nombre_comuna' => 'nombre_comuna','cod_comuna' => 'cod_comuna' )); 
        
        $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = personal.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
            
        $select->order("id_persona");
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {

        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_persona' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function eliminar(Persona $persona) {
        $this->tableGateway->delete(array('id_persona' => $persona->getId_personal()));
    }

    public function guardar(Persona $persona) {

        $data = array(
            'cargos_idcargo' => $persona->getCargos_idcargo(), 
            'sucursales_id_sucursal' => $persona->getSucursales_id_sucursal(), 
            //'comunas_id_comuna' => $persona->getComunas_id_comuna(), 
            //'estado_persona' => $persona->getEstado_persona(), 
            'nombres' => $persona->getNombres(), 
            'apellidos' => $persona->getApellidos(), 
            'email' => $persona->getEmail(), 
            'telefono' => $persona->getTelefono(), 
            'celular' => $persona->getCelular(), 
            'direccion' => $persona->getDireccion(), 
            'foto_carnet' => $persona->getFoto_carnet(), 
            'foto_firma' => $persona->getFoto_firma(), 
            'rut_persona' => $persona->getRut_persona(),    
          );

        $id = (int) $persona->getId_persona();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_persona' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorNombrePersona($nombrepersona) {

        $nombrepersona = (String) $nombrepersona;
        
        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $select = $this->tableGateway->getSql()->select();
        $select->join( array( 'car' => 'cargos' ) , 'car.idcargo = personal.cargos_idcargo' , array( 'idcargo' => 'idcargo' , 'nombre_cargo'=>'nombre_cargo' , 'descripcion' => 'descripcion' ));
        $select->where(array('sucursales_id_sucursal' => $sucursales_id_sucursal ));
        $select->where->like('nombres', '%' . $nombrepersona . '%');
        $select->order('id_persona');
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

}
