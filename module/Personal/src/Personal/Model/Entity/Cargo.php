<?php

namespace Personal\Model\Entity;

class Cargo {

    private $idcargo;
    private $nombre_cargo;
    private $descripcion;

    public function __construct(
    $idcargo = null, $nombre_cargo = null, $descripcion = null
    ) {
        $this->idcargo = $idcargo;
        $this->nombre_cargo = $nombre_cargo;
        $this->descripcion = $descripcion;
    }

    public function getIdcargo() {
        return $this->idcargo;
    }

    public function setIdcargo($idcargo) {
        $this->idcargo = $idcargo;
    }

    public function getNombre_cargo() {
        return $this->nombre_cargo;
    }

    public function setNombre_cargo($nombre_cargo) {
        $this->nombre_cargo = $nombre_cargo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idcargo = (isset($data['idcargo'])) ? $data['idcargo'] : null;
        $this->nombre_cargo = (isset($data['nombre_cargo'])) ? $data['nombre_cargo'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

