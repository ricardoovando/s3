<?php

namespace Personal\Model\Entity;

class Persona {

    private $id_persona;
    private $cargos_idcargo;
    private $sucursales_id_sucursal;
    private $comunas_id_comuna;
    private $estado_persona;
    private $nombres;
    private $apellidos;
    private $email;
    private $telefono;
    private $celular;
    private $direccion;
    private $foto_carnet;
    private $foto_firma;
    private $rut_persona;

    /* Objeto que enlaza la tabla personal con cargo */
    private $cargo;

    /* Objeto que enlaxa la tabla personal con comuna */
    private $comuna;

    function __construct($id_persona = null, $cargos_idcargo = null, $sucursales_id_sucursal = null, $comunas_id_comuna = null, $estado_persona = null, $nombres = null, $apellidos = null, $email = null, $telefono = null, $celular = null, $direccion = null, $foto_carnet = null, $foto_firma = null, $rut_persona = null) {
        $this->id_persona = $id_persona;
        $this->cargos_idcargo = $cargos_idcargo;
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
        $this->comunas_id_comuna = $comunas_id_comuna;
        $this->estado_persona = $estado_persona;
        $this->nombres = $nombres;
        $this->apellidos = $apellidos;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->celular = $celular;
        $this->direccion = $direccion;
        $this->foto_carnet = $foto_carnet;
        $this->foto_firma = $foto_firma;
        $this->rut_persona = $rut_persona;
    }

    public function getId_persona() {
        return $this->id_persona;
    }

    public function setId_persona($id_persona) {
        $this->id_persona = $id_persona;
    }

    public function getCargos_idcargo() {
        return $this->cargos_idcargo;
    }

    public function setCargos_idcargo($cargos_idcargo) {
        $this->cargos_idcargo = $cargos_idcargo;
    }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getComunas_id_comuna() {
        return $this->comunas_id_comuna;
    }

    public function setComunas_id_comuna($comunas_id_comuna) {
        $this->comunas_id_comuna = $comunas_id_comuna;
    }

    public function getEstado_persona() {
        return $this->estado_persona;
    }

    public function setEstado_persona($estado_persona) {
        $this->estado_persona = $estado_persona;
    }

    public function getNombres() {
        return $this->nombres;
    }

    public function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function getCelular() {
        return $this->celular;
    }

    public function setCelular($celular) {
        $this->celular = $celular;
    }

    public function getDireccion() {
        return $this->direccion;
    }

    public function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    public function getFoto_carnet() {
        return $this->foto_carnet;
    }

    public function setFoto_carnet($foto_carnet) {
        $this->foto_carnet = $foto_carnet;
    }

    public function getFoto_firma() {
        return $this->foto_firma;
    }

    public function setFoto_firma($foto_firma) {
        $this->foto_firma = $foto_firma;
    }

    public function getRut_persona() {
        return $this->rut_persona;
    }

    public function setRut_persona($rut_persona) {
        $this->rut_persona = $rut_persona;
    }

    public function getCargo() {
        return $this->cargo;
    }

    public function setCargo(Cargo $cargo) {
        $this->cargo = $cargo;
    }

    public function getComuna() {
        return $this->comuna;
    }

    public function setComuna(Comuna $comuna) {
        $this->comuna = $comuna;
    }

    public function exchangeArray($data) {

        $this->id_persona = (isset($data['id_persona'])) ? $data['id_persona'] : null;
        $this->cargos_idcargo = (isset($data['cargos_idcargo'])) ? $data['cargos_idcargo'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        $this->comunas_id_comuna = (isset($data['comunas_id_comuna'])) ? $data['comunas_id_comuna'] : null;
        $this->estado_persona = (isset($data['estado_persona'])) ? $data['estado_persona'] : null;
        $this->nombres = (isset($data['nombres'])) ? $data['nombres'] : null;
        $this->apellidos = (isset($data['apellidos'])) ? $data['apellidos'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->telefono = (isset($data['telefono'])) ? $data['telefono'] : null;
        $this->celular = (isset($data['celular'])) ? $data['celular'] : null;
        $this->direccion = (isset($data['direccion'])) ? $data['direccion'] : null;
        $this->foto_carnet = (isset($data['foto_carnet'])) ? $data['foto_carnet'] : null;
        $this->foto_firma = (isset($data['foto_firma'])) ? $data['foto_firma'] : null;
        $this->rut_persona = (isset($data['rut_persona'])) ? $data['rut_persona'] : null;

        $this->cargo = New Cargo();
        $this->cargo->setIdcargo((isset($data['idcargo'])) ? $data['idcargo'] : null);
        $this->cargo->setNombre_cargo((isset($data['nombre_cargo'])) ? $data['nombre_cargo'] : null);
        $this->cargo->setDescripcion((isset($data['descripcion'])) ? $data['descripcion'] : null);

        $this->comuna = New Comuna();
        $this->comuna->setId_comuna((isset($data['id_comuna'])) ? $data['id_comuna'] : null);
        $this->comuna->setNombre_comuna((isset($data['nombre_comuna'])) ? $data['nombre_comuna'] : null);
        $this->comuna->setProvincias_id_provincia((isset($data['provincias_id_provincia'])) ? $data['provincias_id_provincia'] : null);
        $this->comuna->setCod_comuna((isset($data['cod_comuna'])) ? $data['cod_comuna'] : null);
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

