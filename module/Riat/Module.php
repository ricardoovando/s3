<?php

namespace Riat;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
//use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/*Entity*/
use Riat\Model\Dao\OrsRiatDao;
use Riat\Model\Entity\OrsRiat;

use Riat\Model\Dao\ViajesFrustradosDao;
use Riat\Model\Entity\ViajesFrustrados;

use Riat\Model\Dao\SellosDao;
use Riat\Model\Entity\Sellos; 

use Riat\Model\Dao\LecturaMedidorDao;
use Riat\Model\Entity\LecturaMedidor;

use Riat\Model\Dao\InspeccionCondicionSubestandarDao;
use Riat\Model\Entity\InspeccionCondicionSubestandar;

use Riat\Model\Dao\MedicionesInstantaneasDao;
use Riat\Model\Entity\MedicionesInstantaneas;

use Riat\Model\Dao\CorrienteSecundariaDao;
use Riat\Model\Entity\CorrienteSecundaria;

use Riat\Model\Dao\ValorTotalPotenciasDao;
use Riat\Model\Entity\ValorTotalPotencias;

use Riat\Model\Dao\PotenciaMedidorInduccionDao;
use Riat\Model\Entity\PotenciaMedidorInduccion;

use Riat\Model\Dao\ResultadoInspeccionAuditoriaDao;
use Riat\Model\Entity\ResultadoInspeccionAuditoria;

use Riat\Model\Dao\MantencionRealizadaDao;
use Riat\Model\Entity\MantencionRealizada;

use Riat\Model\Dao\IrregularidadDetectadaDao;
use Riat\Model\Entity\IrregularidadDetectada;

use Riat\Model\Dao\RemplazoElementoEmpalmeDao;
use Riat\Model\Entity\RemplazoElementoEmpalme;

use Riat\Model\Dao\DatosAdjuntosDao;
use Riat\Model\Entity\DatosAdjuntos;

use Riat\Model\Dao\CalendarioMedidorDao;
use Riat\Model\Entity\CalendarioMedidor;

use Riat\Model\Dao\InspeccionVisualEmpalmeDao;
use Riat\Model\Entity\InspeccionVisualEmpalme;

use Riat\Model\Dao\DiagramaDao;
use Riat\Model\Entity\Diagrama;

use Riat\Model\Dao\RazonEncontradaDao;
use Riat\Model\Entity\RazonEncontrada;

use Riat\Model\Dao\OrigenAfectacionAlamedidaDao;
use Riat\Model\Entity\OrigenAfectacionAlamedida;

use Riat\Model\Dao\LecturaMedidorInstaladoDao;
use Riat\Model\Entity\LecturaMedidorInstalado;

use Riat\Model\Dao\ContactoAutorizaDao;
use Riat\Model\Entity\ContactoAutoriza;


class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, /*ServiceProviderInterface,*/ ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();

        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
        
    }
    
    public function initAuth(MvcEvent $e) {
        $application = $e->getApplication();
        $matches = $e->getRouteMatch();
        $controller = $matches->getParam('controller');
        $action = $matches->getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index', 'autenticar'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index y autenticar.
                        return;
                    }
                    break;
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
            }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('home');
            }
        }
    }
    

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniRiat', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return $data;
                });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    

        

    /*Inyecccion de dependencia para el dao se carga la tabla de base de datos segun se indique*/
    public function getServiceConfig() {
     return array(
            'factories' => array(
                'Riat\Model\OrsRiatDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayOrsRiat');
                    $login = $sm->get('Admin\Model\Login');
                    $dbAdapter = $sm->get('dbAdapter');
                    $dao = new OrsRiatDao($tableGateway,$login,$dbAdapter);
                    return $dao;
                },  
                'Riat\Model\ViajesFrustradosDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayViajesFrustrados');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ViajesFrustradosDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\SellosDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewaySellos');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new SellosDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\LecturaMedidorDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayLecturaMedidor');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new LecturaMedidorDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\InspeccionCondicionSubestandarDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayInspeccionCondicionSubestandar');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new InspeccionCondicionSubestandarDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\CorrienteSecundariaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayCorrienteSecundaria');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new CorrienteSecundariaDao($tableGateway,$login);
                    return $dao;
                },    
                'Riat\Model\MedicionesInstantaneasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMedicionesInstantaneas');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MedicionesInstantaneasDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\ValorTotalPotenciasDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayValorTotalPotencias');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ValorTotalPotenciasDao($tableGateway,$login);
                    return $dao;
                },     
                'Riat\Model\PotenciaMedidorInduccionDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayPotenciaMedidorInduccion');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new PotenciaMedidorInduccionDao($tableGateway,$login);
                    return $dao;
                },   
                'Riat\Model\ResultadoInspeccionAuditoriaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayResultadoInspeccionAuditoria');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ResultadoInspeccionAuditoriaDao($tableGateway,$login);
                    return $dao;
                },   
                'Riat\Model\MantencionRealizadaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayMantencionRealizada');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new MantencionRealizadaDao($tableGateway,$login);
                    return $dao;
                }, 
                'Riat\Model\IrregularidadDetectadaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayIrregularidadDetectada');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new IrregularidadDetectadaDao($tableGateway,$login);
                    return $dao;
                }, 
                'Riat\Model\RemplazoElementoEmpalmeDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayRemplazoElementoEmpalme');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new RemplazoElementoEmpalmeDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\DatosAdjuntosDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayDatosAdjuntos');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new DatosAdjuntosDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\CalendarioMedidorDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayCalendarioMedidor');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new CalendarioMedidorDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\InspeccionVisualEmpalmeDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayInspeccionVisualEmpalme');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new InspeccionVisualEmpalmeDao($tableGateway,$login);
                    return $dao;
                },
                'Riat\Model\DiagramaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayDiagrama');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new DiagramaDao($tableGateway,$login);
                    return $dao;
                },
               'Riat\Model\RazonEncontradaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayRazonEncontrada');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new RazonEncontradaDao($tableGateway,$login);
                    return $dao;
                },
             'Riat\Model\OrigenAfectacionAlamedidaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayOrigenAfectacionAlamedida');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new OrigenAfectacionAlamedidaDao($tableGateway,$login);
                    return $dao;
                },
              'Riat\Model\LecturaMedidorInstaladoDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayLecturaMedidorInstalado');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new LecturaMedidorInstaladoDao($tableGateway,$login);
                    return $dao;
                },
              'Riat\Model\ContactoAutorizaDao' => function($sm) {
                    $tableGateway = $sm->get('TableGatewayContactoAutoriza');
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ContactoAutorizaDao($tableGateway,$login);
                    return $dao;
                }, 
                        
                       
                        
                /*
                 * 
                 * TABLAS
                 * 
                 */
                'TableGatewayOrsRiat' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new OrsRiat());
                    return new TableGateway('ors_riat', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayViajesFrustrados' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ViajesFrustrados());
                    return new TableGateway('viajes_frustrados', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewaySellos' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Sellos());
                    return new TableGateway('sellos', $dbAdapter, null, $resultSetPrototype);
                }, 
               'TableGatewayLecturaMedidor' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LecturaMedidor());
                    return new TableGateway('lectura_medidor', $dbAdapter, null, $resultSetPrototype);
                },
               'TableGatewayInspeccionCondicionSubestandar' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new InspeccionCondicionSubestandar());
                    return new TableGateway('inspeccion_condicion_subestandar', $dbAdapter, null, $resultSetPrototype);
                },
               'TableGatewayMedicionesInstantaneas' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MedicionesInstantaneas());
                    return new TableGateway('mediciones_instantaneas', $dbAdapter, null, $resultSetPrototype);
                },     
               'TableGatewayCorrienteSecundaria' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CorrienteSecundaria());
                    return new TableGateway('corriente_secundaria', $dbAdapter, null, $resultSetPrototype);
                },
                'TableGatewayValorTotalPotencias' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ValorTotalPotencias());
                    return new TableGateway('valor_total_potencias', $dbAdapter, null, $resultSetPrototype);
                },
               'TableGatewayPotenciaMedidorInduccion' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PotenciaMedidorInduccion());
                    return new TableGateway('potencia_medidor_induccion', $dbAdapter, null, $resultSetPrototype);
                },
               'TableGatewayResultadoInspeccionAuditoria' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ResultadoInspeccionAuditoria());
                    return new TableGateway('resultado_inspeccion_auditoria', $dbAdapter, null, $resultSetPrototype);
                },
              'TableGatewayMantencionRealizada' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MantencionRealizada());
                    return new TableGateway('mantencion_realizada', $dbAdapter, null, $resultSetPrototype);
                },
              'TableGatewayIrregularidadDetectada' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new IrregularidadDetectada());
                    return new TableGateway('irregularidades_detectadas', $dbAdapter, null, $resultSetPrototype);
                },
              'TableGatewayRemplazoElementoEmpalme' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RemplazoElementoEmpalme());
                    return new TableGateway('remplazo_elemento_empalme', $dbAdapter, null, $resultSetPrototype);
                },
             'TableGatewayDatosAdjuntos' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DatosAdjuntos());
                    return new TableGateway('datos_adjuntos', $dbAdapter, null, $resultSetPrototype);
                },
             'TableGatewayCalendarioMedidor' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CalendarioMedidor());
                    return new TableGateway('calendario_medidor', $dbAdapter, null, $resultSetPrototype);
                },  
             'TableGatewayInspeccionVisualEmpalme' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new InspeccionVisualEmpalme());
                    return new TableGateway('inspeccion_visual_empalme', $dbAdapter, null, $resultSetPrototype);
                },  
              'TableGatewayDiagrama' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Diagrama());
                    return new TableGateway('diagrama', $dbAdapter, null, $resultSetPrototype);
                },
              'TableGatewayRazonEncontrada' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RazonEncontrada());
                    return new TableGateway('razon_encontrada', $dbAdapter, null, $resultSetPrototype);
                },    
              'TableGatewayOrigenAfectacionAlamedida' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new OrigenAfectacionAlamedida());
                    return new TableGateway('origen_afectacion_alamedida', $dbAdapter, null, $resultSetPrototype);
                },        
              'TableGatewayLecturaMedidorInstalado' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LecturaMedidorInstalado());
                    return new TableGateway('lectura_medidor_instalado', $dbAdapter, null, $resultSetPrototype);
                },       
              'TableGatewayContactoAutoriza' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ContactoAutoriza());
                    return new TableGateway('contacto_autoriza', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    } 




    /*Inyeccion de dependencia para el controlador se carga el objeto modelo usuario dao*/
     public function getControllerConfig() {
        return  array(
            'factories' => array(
                'Riat\Controller\Index' => function ($sm) {
            
                    $locator = $sm->getServiceLocator();
                    
                    $config = $locator->get('ConfigIniRiat');
                    
                    $orsriatdao = $locator->get('Riat\Model\OrsRiatDao');
                    $viajesfrustradosdao = $locator->get('Riat\Model\ViajesFrustradosDao');
                    $sellosdao = $locator->get('Riat\Model\SellosDao');
                    $lecturamedidordao = $locator->get('Riat\Model\LecturaMedidorDao');
                    $inspeccioncondicionsubestandardao = $locator->get('Riat\Model\InspeccionCondicionSubestandarDao');
                    $MedicionesInstantaneasDao = $locator->get('Riat\Model\MedicionesInstantaneasDao');
                    $corrientesecundariadao = $locator->get('Riat\Model\CorrienteSecundariaDao');
                    $valortotalpotenciasdao = $locator->get('Riat\Model\ValorTotalPotenciasDao');
                    $potenciamedidorinducciondao = $locator->get('Riat\Model\PotenciaMedidorInduccionDao');
                    $resultadoinspeccionauditoriadao = $locator->get('Riat\Model\ResultadoInspeccionAuditoriaDao');
                    $mantencionrealizadadao = $locator->get('Riat\Model\MantencionRealizadaDao');
                    $IrregularidadDetectadaDao = $locator->get('Riat\Model\IrregularidadDetectadaDao');
                    $RemplazoElementoEmpalmeDao = $locator->get('Riat\Model\RemplazoElementoEmpalmeDao');
                    $DatosAdjuntosDao = $locator->get('Riat\Model\DatosAdjuntosDao');
                    $CalendarioMedidorDao = $locator->get('Riat\Model\CalendarioMedidorDao');
                    $InspeccionVisualEmpalmeDao = $locator->get('Riat\Model\InspeccionVisualEmpalmeDao');
                    $DiagramaDao = $locator->get('Riat\Model\DiagramaDao');
                    $RazonEncontradaDao = $locator->get('Riat\Model\RazonEncontradaDao');
                    $OrigenAfectacionAlamedidaDao = $locator->get('Riat\Model\OrigenAfectacionAlamedidaDao');
                    $LecturaMedidorInstaladoDao = $locator->get('Riat\Model\LecturaMedidorInstaladoDao');
                    $ContactoAutorizaDao = $locator->get('Riat\Model\ContactoAutorizaDao');
                        

                    $avisoDao = $locator->get('Avisos\Model\AvisoDao');
                    
                    $AntecedentesEmpalmeDao = $locator->get('Instalaciones\Model\AntecedentesEmpalmeDao');
                    $DistribuidorasDao = $locator->get('Instalaciones\Model\DistribuidorasDao');
                    $EcmDao = $locator->get('Instalaciones\Model\EcmDao');
                    $InstalacionDao = $locator->get('Instalaciones\Model\InstalacionDao');
                    $MarcasEcmsDao = $locator->get('Instalaciones\Model\MarcasEcmsDao');
                    $MedidorInstalacionDao = $locator->get('Instalaciones\Model\MedidorInstalacionDao');
                    $ModelosMedidoresDao = $locator->get('Instalaciones\Model\ModelosMedidoresDao');
                    $RegletaInstalacionDao = $locator->get('Instalaciones\Model\RegletaInstalacionDao');
                    $RegletasDao = $locator->get('Instalaciones\Model\RegletasDao');
                    $RelojDao = $locator->get('Instalaciones\Model\RelojDao');
                    $SistemaDistribucionDao = $locator->get('Instalaciones\Model\SistemaDistribucionDao');
                    $TelemedidaDao = $locator->get('Instalaciones\Model\TelemedidaDao');
                    $TransformadorCorrienteDao = $locator->get('Instalaciones\Model\TransformadorCorrienteDao');
                   
                    
                    /*INYECTA AL CONTROLADOR INDEX DEL RIAT*/
                    $controller = new \Riat\Controller\IndexController($config);
                    
                    /*DATOS RIAT*/
                    $controller->setOrsRiatDao($orsriatdao);
                    $controller->setViajesFrustradosDao($viajesfrustradosdao);
                    $controller->setSellosDao($sellosdao);
                    $controller->setLecturaMedidorDao($lecturamedidordao);
                    $controller->setInspeccionCondicionSubestandarDao($inspeccioncondicionsubestandardao);
                    $controller->setMedicionesInstantaneasDao($MedicionesInstantaneasDao);
                    $controller->setCorrienteSecundariaDao($corrientesecundariadao);
                    $controller->setValorTotalPotenciasDao($valortotalpotenciasdao);
                    $controller->setPotenciaMedidorInduccionDao($potenciamedidorinducciondao);
                    $controller->setResultadoInspeccionAuditoriaDao($resultadoinspeccionauditoriadao);
                    $controller->setMantencionRealizadaDao($mantencionrealizadadao);
                    $controller->setIrregularidadDetectadaDao($IrregularidadDetectadaDao);
                    $controller->setRemplazoElementoEmpalmeDao($RemplazoElementoEmpalmeDao);
                    $controller->setDatosAdjuntosDao($DatosAdjuntosDao);
                    $controller->setCalendarioMedidorDao($CalendarioMedidorDao);
                    $controller->setDiagramaDao($DiagramaDao);
                    $controller->setRazonEncontradaDao($RazonEncontradaDao);
                    $controller->setOrigenAfectacionAlamedidaDao($OrigenAfectacionAlamedidaDao);
                    $controller->setLecturaMedidorInstaladoDao($LecturaMedidorInstaladoDao);
                    $controller->setContactoAutorizaDao($ContactoAutorizaDao);
                    
                    /*TABLA AVISO*/
                    $controller->setavisoDao($avisoDao);
                
                    /*TABLAS INSTALACION*/
                    $controller->setAntecedentesEmpalmeDao($AntecedentesEmpalmeDao);
                    $controller->setDistribuidorasDao($DistribuidorasDao);
                    $controller->setEcmDao($EcmDao);
                    $controller->setInstalacionDao($InstalacionDao);
                    $controller->setMarcasEcmsDao($MarcasEcmsDao);
                    $controller->setMedidorInstalacionDao($MedidorInstalacionDao);
                    $controller->setModelosMedidoresDao($ModelosMedidoresDao);
                    $controller->setRegletaInstalacionDao($RegletaInstalacionDao);
                    $controller->setRegletasDao($RegletasDao);
                    $controller->setRelojDao($RelojDao);
                    $controller->setSistemaDistribucionDao($SistemaDistribucionDao);
                    $controller->setTelemedidaDao($TelemedidaDao);
                    $controller->setTransformadorCorrienteDao($TransformadorCorrienteDao);
                    $controller->setInspeccionVisualEmpalmeDao($InspeccionVisualEmpalmeDao);        
                    
                    return $controller;

                },
             )
          );
       } 

    
}
