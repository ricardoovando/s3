<?php

return array(
    'controllers' => array(
        'invokables' => array(
            //'Avisos\Controller\Cargar' => 'Avisos\Controller\CargarController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'riat' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/riat[/:controller][/:action][/:id][/:r]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                        'r' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Riat\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
          'paginatorriat' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/riat[/:controller][/:action]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Riat\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Riat' => __DIR__ . '/../view',
        ),
        'strategies' => array(
          'ViewJsonStrategy', 
        ),
    ),
);
