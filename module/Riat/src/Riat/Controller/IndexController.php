<?php

namespace Riat\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/* FORMULARIOS */
/**/
use Avisos\Form\Buscador as BuscadorFormAvisos;
use Avisos\Form\BuscadorValidator;
/**/
use Avisos\Form\BuscadorInstalacion;
use Avisos\Form\BuscadorInstalacionValidator;
/**/
use Avisos\Form\SelectSucursal;
use Avisos\Form\SelectSucursalValidator;
/**/
use Riat\Form\SelectEstado;
use Riat\Form\SelectEstadoValidator;
/**/
use Riat\Form\FormFinalizar;
use Riat\Form\FormFinalizarValidator;

/* FORM */
use Riat\Form\RegistroRiat;
use Riat\Form\RegistroRiatValidator;

/* ENTITY RIAT */
use Riat\Model\Dao\OrsRiatDao;
use Riat\Model\Entity\OrsRiat;
use Riat\Model\Dao\ViajesFrustradosDao;
use Riat\Model\Entity\ViajesFrustrados;
use Riat\Model\Dao\SellosDao;
use Riat\Model\Entity\Sellos;
use Riat\Model\Dao\LecturaMedidorDao;
use Riat\Model\Entity\LecturaMedidor;
use Riat\Model\Dao\InspeccionCondicionSubestandarDao;
use Riat\Model\Entity\InspeccionCondicionSubestandar;
use Riat\Model\Dao\MedicionesInstantaneasDao;
use Riat\Model\Entity\MedicionesInstantaneas;
use Riat\Model\Dao\CorrienteSecundariaDao;
use Riat\Model\Entity\CorrienteSecundaria;
use Riat\Model\Dao\ValorTotalPotenciasDao;
use Riat\Model\Entity\ValorTotalPotencias;
use Riat\Model\Dao\PotenciaMedidorInduccionDao;
use Riat\Model\Entity\PotenciaMedidorInduccion;
use Riat\Model\Dao\ResultadoInspeccionAuditoriaDao;
use Riat\Model\Entity\ResultadoInspeccionAuditoria;
use Riat\Model\Dao\MantencionRealizadaDao;
use Riat\Model\Entity\MantencionRealizada;
use Riat\Model\Dao\IrregularidadDetectadaDao;
use Riat\Model\Entity\IrregularidadDetectada;
use Riat\Model\Dao\RemplazoElementoEmpalmeDao;
use Riat\Model\Entity\RemplazoElementoEmpalme;
use Riat\Model\Dao\DatosAdjuntosDao;
use Riat\Model\Entity\DatosAdjuntos;
use Riat\Model\Dao\CalendarioMedidorDao;
use Riat\Model\Entity\CalendarioMedidor;
use Riat\Model\Dao\InspeccionVisualEmpalmeDao;
use Riat\Model\Entity\InspeccionVisualEmpalme;
use Riat\Model\Dao\DiagramaDao;
use Riat\Model\Entity\Diagrama;
use Riat\Model\Dao\RazonEncontradaDao;
use Riat\Model\Entity\RazonEncontrada;
use Riat\Model\Dao\OrigenAfectacionAlamedidaDao;
use Riat\Model\Entity\OrigenAfectacionAlamedida;
use Riat\Model\Dao\LecturaMedidorInstaladoDao;
use Riat\Model\Entity\LecturaMedidorInstalado;
use Riat\Model\Dao\ContactoAutorizaDao;
use Riat\Model\Entity\ContactoAutoriza;
use Avisos\Model\Entity\Aviso;
use Avisos\Model\Dao\AvisoDao;

/* Entity Instalacion */
use Instalaciones\Model\Dao\AntecedentesEmpalmeDao;
use Instalaciones\Model\Entity\AntecedentesEmpalme;
use Instalaciones\Model\Dao\DistribuidorasDao;
use Instalaciones\Model\Entity\Distribuidoras;
use Instalaciones\Model\Dao\EcmDao;
use Instalaciones\Model\Entity\Ecm;
use Instalaciones\Model\Dao\InstalacionDao;
use Instalaciones\Model\Entity\Instalacion;
use Instalaciones\Model\Dao\MarcasEcmsDao;
use Instalaciones\Model\Entity\MarcasEcms;
use Instalaciones\Model\Dao\MedidorInstalacionDao;
use Instalaciones\Model\Entity\MedidorInstalacion;
use Instalaciones\Model\Dao\ModelosMedidoresDao;
use Instalaciones\Model\Entity\ModelosMedidores;
use Instalaciones\Model\Dao\RegletaInstalacionDao;
use Instalaciones\Model\Entity\RegletaInstalacion;
use Instalaciones\Model\Dao\RegletasDao;
use Instalaciones\Model\Entity\Regletas;
use Instalaciones\Model\Dao\RelojDao;
use Instalaciones\Model\Entity\Reloj;
use Instalaciones\Model\Dao\SistemaDistribucionDao;
use Instalaciones\Model\Entity\SistemaDistribucion;
use Instalaciones\Model\Dao\TelemedidaDao;
use Instalaciones\Model\Entity\Telemedida;
use Instalaciones\Model\Dao\TransformadorCorrienteDao;
use Instalaciones\Model\Entity\TransformadorCorriente;

class IndexController extends AbstractActionController {

    private $config;
    private $login;
    private $detalleservicioprestado;
        
    /* DATOS RIAT */
    private $OrsRiatDao;
    private $ViajesFrustradosDao;
    private $SellosDao;
    private $LecturaMedidorDao;
    private $InspeccionCondicionSubestandarDao;
    private $MedicionesInstantaneasDao;
    private $CorrienteSecundariaDao;
    private $ValorTotalPotenciasDao;
    private $PotenciaMedidorInduccionDao;
    private $ResultadoInspeccionAuditoriaDao;
    private $mantencionrealizadadao;
    private $IrregularidadDetectadaDao;
    private $RemplazoElementoEmpalmeDao;
    private $DatosAdjuntosDao;
    private $CalendarioMedidorDao;
    private $InspeccionVisualEmpalmeDao;
    private $DiagramaDao;
    private $RazonEncontradaDao;
    private $OrigenAfectacionAlamedidaDao;
    private $LecturaMedidorInstaladoDao;
    private $ContactoAutorizaDao;

    /* AVISO */
    private $avisoDao;

    /* INSTALACION */
    private $AntecedentesEmpalmeDao;
    private $DistribuidorasDao;
    private $EcmDao;
    private $InstalacionDao;
    private $MarcasEcmsDao;
    private $MedidorInstalacionDao;
    private $ModelosMedidoresDao;
    private $RegletaInstalacionDao;
    private $RegletasDao;
    private $RelojDao;
    private $SistemaDistribucionDao;
    private $TelemedidaDao;
    private $TransformadorCorrienteDao;

    function __construct($config = null) {
        
        $this->config = $config;
        
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }
    
    public function getDetalleServicioPrestado() {
        
       if (!$this->detalleservicioprestado) {
             $sm = $this->getServiceLocator();
             $this->detalleservicioprestado = $sm->get('Avisos\Model\DetalleServicioPrestadoDao');
           }
       
       return $this->detalleservicioprestado;
       
    }

    /* Buscador de Avisos */

    private function getFormBuscadorAvisos() {
        return new BuscadorFormAvisos();
    }
    
    private function getFormBuscadorInstalacion(){   
        return new BuscadorInstalacion();
    }

    private function getFormSelectSucursales() {
        $form = new SelectSucursal();
        $form->get('sucursal')->setValueOptions($this->getAvisoDao()->obtenerSucursalesSelect());
        return $form;
    }

    public function getFormSelectEstados() {
        $form = new SelectEstado();
        $form->get('estado_ors_riat')->setValueOptions($this->getOrsRiatDao()->obtenerEstadosSelect());
        return $form;
    }

    public function getFormFinalizar() {
        return new FormFinalizar();
    }

	/*
	 * 
	 * RIAT
	 * 
	 */

    private function getFormRegistroRiat() {
        return new RegistroRiat();
    }

    public function setOrsRiatDao(OrsRiatDao $orsriatdao) {
        $this->OrsRiatDao = $orsriatdao;
    }

    public function getOrsRiatDao() {
        return $this->OrsRiatDao;
    }

    public function setViajesFrustradosDao(ViajesFrustradosDao $ViajesFrustradosDao) {
        $this->ViajesFrustradosDao = $ViajesFrustradosDao;
    }

    public function getViajesFrustradosDao() {
        return $this->ViajesFrustradosDao;
    }

    public function setSellosDao(SellosDao $SellosDao) {
        $this->SellosDao = $SellosDao;
    }

    public function getSellosDao() {
        return $this->SellosDao;
    }

    public function setLecturaMedidorDao(LecturaMedidorDao $LecturaMedidorDao) {
        $this->LecturaMedidorDao = $LecturaMedidorDao;
    }

    public function getLecturaMedidorDao() {
        return $this->LecturaMedidorDao;
    }

    public function setInspeccionCondicionSubestandarDao(InspeccionCondicionSubestandarDao $InspeccionCondicionSubestandarDao) {
        $this->InspeccionCondicionSubestandarDao = $InspeccionCondicionSubestandarDao;
    }

    public function getInspeccionCondicionSubestandarDao() {
        return $this->InspeccionCondicionSubestandarDao;
    }

    public function setMedicionesInstantaneasDao(MedicionesInstantaneasDao $MedicionesInstantaneasDao) {
        $this->MedicionesInstantaneasDao = $MedicionesInstantaneasDao;
    }

    public function getMedicionesInstantaneasDao() {
        return $this->MedicionesInstantaneasDao;
    }

    public function setCorrienteSecundariaDao(CorrienteSecundariaDao $CorrienteSecundariaDao) {
        $this->CorrienteSecundariaDao = $CorrienteSecundariaDao;
    }

    public function getCorrienteSecundariaDao() {
        return $this->CorrienteSecundariaDao;
    }

    public function setValorTotalPotenciasDao(ValorTotalPotenciasDao $ValorTotalPotenciasDao) {
        $this->ValorTotalPotenciasDao = $ValorTotalPotenciasDao;
    }

    public function getValorTotalPotenciasDao() {
        return $this->ValorTotalPotenciasDao;
    }

    public function setPotenciaMedidorInduccionDao(PotenciaMedidorInduccionDao $PotenciaMedidorInduccionDao) {
        $this->PotenciaMedidorInduccionDao = $PotenciaMedidorInduccionDao;
    }

    public function getPotenciaMedidorInduccionDao() {
        return $this->PotenciaMedidorInduccionDao;
    }

    public function setResultadoInspeccionAuditoriaDao(ResultadoInspeccionAuditoriaDao $ResultadoInspeccionAuditoriaDao) {
        $this->ResultadoInspeccionAuditoriaDao = $ResultadoInspeccionAuditoriaDao;
    }

    public function getResultadoInspeccionAuditoriaDao() {
        return $this->ResultadoInspeccionAuditoriaDao;
    }

    public function setMantencionRealizadaDao(MantencionRealizadaDao $mantencionrealizadadao) {
        $this->mantencionrealizadadao = $mantencionrealizadadao;
    }

    public function getMantencionRealizadaDao() {
        return $this->mantencionrealizadadao;
    }

    public function setIrregularidadDetectadaDao(IrregularidadDetectadaDao $IrregularidadDetectadaDao) {
        $this->IrregularidadDetectadaDao = $IrregularidadDetectadaDao;
    }

    public function getIrregularidadDetectadaDao() {
        return $this->IrregularidadDetectadaDao;
    }

    public function setRemplazoElementoEmpalmeDao(RemplazoElementoEmpalmeDao $RemplazoElementoEmpalmeDao) {
        $this->RemplazoElementoEmpalmeDao = $RemplazoElementoEmpalmeDao;
    }

    public function getRemplazoElementoEmpalmeDao() {
        return $this->RemplazoElementoEmpalmeDao;
    }

    public function setDatosAdjuntosDao(DatosAdjuntosDao $DatosAdjuntosDao) {
        $this->DatosAdjuntosDao = $DatosAdjuntosDao;
    }

    public function getDatosAdjuntosDao() {
        return $this->DatosAdjuntosDao;
    }

    public function setCalendarioMedidorDao(CalendarioMedidorDao $CalendarioMedidorDao) {
        $this->CalendarioMedidorDao = $CalendarioMedidorDao;
    }

    public function getCalendarioMedidorDao() {
        return $this->CalendarioMedidorDao;
    }

    public function setInspeccionVisualEmpalmeDao(InspeccionVisualEmpalmeDao $InspeccionVisualEmpalmeDao) {
        $this->InspeccionVisualEmpalmeDao = $InspeccionVisualEmpalmeDao;
    }

    public function getInspeccionVisualEmpalmeDao() {
        return $this->InspeccionVisualEmpalmeDao;
    }

    public function setDiagramaDao(DiagramaDao $DiagramaDao) {
        $this->DiagramaDao = $DiagramaDao;
    }

    public function getDiagramaDao() {
        return $this->DiagramaDao;
    }

    public function setRazonEncontradaDao(RazonEncontradaDao $RazonEncontradaDao) {
        $this->RazonEncontradaDao = $RazonEncontradaDao;
    }

    public function getRazonEncontradaDao() {
        return $this->RazonEncontradaDao;
    }

    public function setOrigenAfectacionAlamedidaDao(OrigenAfectacionAlamedidaDao $OrigenAfectacionAlamedidaDao) {
        $this->OrigenAfectacionAlamedidaDao = $OrigenAfectacionAlamedidaDao;
    }

    public function getOrigenAfectacionAlamedidaDao() {
        return $this->OrigenAfectacionAlamedidaDao;
    }

    public function setLecturaMedidorInstaladoDao(LecturaMedidorInstaladoDao $LecturaMedidorInstaladoDao) {
        $this->LecturaMedidorInstaladoDao = $LecturaMedidorInstaladoDao;
    }

    public function getLecturaMedidorInstaladoDao() {
        return $this->LecturaMedidorInstaladoDao;
    }

    public function setContactoAutorizaDao(ContactoAutorizaDao $ContactoAutorizaDao) {
        $this->ContactoAutorizaDao = $ContactoAutorizaDao;
    }

    public function getContactoAutorizaDao() {
        return $this->ContactoAutorizaDao;
    }

    /*
     * 
     * AVISOS
     * 
     * 
     */

    public function setavisoDao(AvisoDao $avisoDao) {
        $this->avisoDao = $avisoDao;
    }

    public function getavisoDao() {
        return $this->avisoDao;
    }

    /*
     * 
     * 
     * INSTALACION
     * 
     * 
     */

    public function setAntecedentesEmpalmeDao(AntecedentesEmpalmeDao $AntecedentesEmpalmeDao) {
        $this->AntecedentesEmpalmeDao = $AntecedentesEmpalmeDao;
    }

    public function getAntecedentesEmpalmeDao() {
        return $this->AntecedentesEmpalmeDao;
    }

    public function setDistribuidorasDao(DistribuidorasDao $DistribuidorasDao) {
        $this->DistribuidorasDao = $DistribuidorasDao;
    }

    public function getDistribuidorasDao() {
        return $this->DistribuidorasDao;
    }

    public function setEcmDao(EcmDao $EcmDao) {
        $this->EcmDao = $EcmDao;
    }

    public function getEcmDao() {
        return $this->EcmDao;
    }

    public function setInstalacionDao(InstalacionDao $InstalacionDao) {
        $this->InstalacionDao = $InstalacionDao;
    }

    public function getInstalacionDao() {
        return $this->InstalacionDao;
    }

    public function setMarcasEcmsDao(MarcasEcmsDao $MarcasEcmsDao) {
        $this->MarcasEcmsDao = $MarcasEcmsDao;
    }

    public function getMarcasEcmsDao() {
        return $this->MarcasEcmsDao;
    }

    public function setMedidorInstalacionDao(MedidorInstalacionDao $MedidorInstalacionDao) {
        $this->MedidorInstalacionDao = $MedidorInstalacionDao;
    }

    public function getMedidorInstalacionDao() {
        return $this->MedidorInstalacionDao;
    }

    public function setModelosMedidoresDao(ModelosMedidoresDao $ModelosMedidoresDao) {
        $this->ModelosMedidoresDao = $ModelosMedidoresDao;
    }

    public function getModelosMedidoresDao() {
        return $this->ModelosMedidoresDao;
    }

    public function setRegletaInstalacionDao(RegletaInstalacionDao $RegletaInstalacionDao) {
        $this->RegletaInstalacionDao = $RegletaInstalacionDao;
    }

    public function getRegletaInstalacionDao() {
        return $this->RegletaInstalacionDao;
    }

    public function setRegletasDao(RegletasDao $RegletasDao) {
        $this->RegletasDao = $RegletasDao;
    }

    public function getRegletasDao() {
        return $this->RegletasDao;
    }

    public function setRelojDao(RelojDao $RelojDao) {
        $this->RelojDao = $RelojDao;
    }

    public function getRelojDao() {
        return $this->RelojDao;
    }

    public function setSistemaDistribucionDao(SistemaDistribucionDao $SistemaDistribucionDao) {
        $this->SistemaDistribucionDao = $SistemaDistribucionDao;
    }

    public function getSistemaDistribucionDao() {
        return $this->SistemaDistribucionDao;
    }

    public function setTelemedidaDao(TelemedidaDao $TelemedidaDao) {
        $this->TelemedidaDao = $TelemedidaDao;
    }

    public function getTelemedidaDao() {
        return $this->TelemedidaDao;
    }

    public function setTransformadorCorrienteDao(TransformadorCorrienteDao $TransformadorCorrienteDao) {
        $this->TransformadorCorrienteDao = $TransformadorCorrienteDao;
    }

    public function getTransformadorCorrienteDao() {
        return $this->TransformadorCorrienteDao;
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if(empty($_SESSION['envioCorreoAlertaCnr']['enviado']))
                   $_SESSION['envioCorreoAlertaCnr']['enviado'] = \Zend\Db\Sql\Predicate\IsNull::TYPE_VALUE;
        
         
        if($_SESSION['envioCorreoAlertaCnr']['enviado'] != 'no'){
                   
             $_SESSION['envioCorreoAlertaCnr']['enviado'] = 'no';
                   
          }
        
        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();

        /* Filtrar solo por los estados Necesarios */
        $paginator = $this->getOrsRiatDao()->obtenerTodos();
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(200);

        return new ViewModel(array(
            'title' => 'Ordenes de Digitación RIAT',
            'listaorsriat' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorAvisos' => $formBuscadorAvisos,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formSucursales' => $formSelectSucurales,
            'formSelectEstados' => $formSelectEstados,
            'formFinalizar' => $formFinalizar,
                //'formBrigadas' => $formSelectBrigadas,
                //'formBuscadorInstalacion' => $formBuscadorInstalacion,
                //'formBuscadorUnidadLectura' => $formBuscadorUnidadLectura,
           ));
    }

    /*
     * 
     * Buscador desde bandeja principal 
     * 
     */

    public function buscaravisoAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();

        /* Validar Buscar Aviso */
        $formBuscadorAvisos->setInputFilter(new BuscadorValidator());
        $formBuscadorAvisos->setData($postParams);


        /*
         * Falla de la Validacion del form. 
         */
        if (!$formBuscadorAvisos->isValid()) {

            /* Filtrar solo por los estados Necesarios */
            $paginator = $this->getOrsRiatDao()->obtenerTodos();
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
            $paginator->setItemCountPerPage(200);

            $viewModel = new ViewModel(array(
                'title' => 'Fallo la Busqueda por Aviso',
                'listaorsriat' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscadorAvisos' => $formBuscadorAvisos,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formSucursales' => $formSelectSucurales,
                'formSelectEstados' => $formSelectEstados,
                'formFinalizar' => $formFinalizar,
            ));

            $viewModel->setTemplate('riat/index/index');

            return $viewModel;
        }


        $values = $formBuscadorAvisos->getData();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getOrsRiatDao()->obtenerPorNumAviso($values['numeroaviso']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(200);

        $viewModel = new ViewModel(array(
            'title' => 'Aviso encontrado',
            'listaorsriat' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorAvisos' => $formBuscadorAvisos,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formSucursales' => $formSelectSucurales,
            'formSelectEstados' => $formSelectEstados,
            'formFinalizar' => $formFinalizar,
        ));


        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("riat/index/index");

        return $viewModel;
        
     }
    
    
    
    
    /* 
     * 
     * Buscador por Numero Instalacion 
     * 
     * 
     */

    public function buscarNumInstalacionAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        
        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();

        /* Validar Formulario Buscar por Num Instalacion */
        $formBuscadorInstalacion->setInputFilter(new BuscadorInstalacionValidator());
        $formBuscadorInstalacion->setData($postParams);
        
        
        /*
         * 
         * Falla de la Validacion del form. 
         * 
         * 
         */
        if (!$formBuscadorInstalacion->isValid())
        {
            
            /* Filtrar solo por los estados Necesarios */
            $paginator = $this->getOrsRiatDao()->obtenerTodos();
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
            $paginator->setItemCountPerPage(200);

            $viewModel = new ViewModel(array(
                'title' => 'Fallo la Busqueda por Instalacion',
                'listaorsriat' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscadorAvisos' => $formBuscadorAvisos,
                'formBuscadorInstalacion' => $formBuscadorInstalacion,
                'formSucursales' => $formSelectSucurales,
                'formSelectEstados' => $formSelectEstados,
                'formFinalizar' => $formFinalizar,
            ));

            $viewModel->setTemplate('riat/index/index');

            return $viewModel;
            
        }


        $values = $formBuscadorInstalacion->getData();
        

        /* Reconfiguracion de Paginador */
        $paginator = $this->getOrsRiatDao()->obtenerPorNumInstalacion($values['numeroinstalacion']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(200);

        $viewModel = new ViewModel(array(
            'title' => 'Instalacion encontrado',
            'listaorsriat' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorAvisos' => $formBuscadorAvisos,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formSucursales' => $formSelectSucurales,
            'formSelectEstados' => $formSelectEstados,
            'formFinalizar' => $formFinalizar,
        ));


        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("riat/index/index");

        return $viewModel;
        
     }
    
    

    /*
     * 
     * Buscador desde bandeja principal 
     * 
     */

    public function buscasucursalAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getOrsRiatDao()->obtenerPorSucursal($postParams['sucursal']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(200);

        $viewModel = new ViewModel(array(
            'title' => 'Aviso encontrado',
            'listaorsriat' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorAvisos' => $formBuscadorAvisos,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formSucursales' => $formSelectSucurales,
            'formSelectEstados' => $formSelectEstados,
            'formFinalizar' => $formFinalizar,
        ));


        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("riat/index/index");

        return $viewModel;
    }

    /*
     * 
     * Buscador desde bandeja principal 
     * 
     */

    public function buscarestadoAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formBuscadorInstalacion = $this->getFormBuscadorInstalacion();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();


        /* Reconfiguracion de Paginador */
        $paginator = $this->getOrsRiatDao()->obtenerPorEstado($postParams['estado_ors_riat']);
        $paginator->setCurrentPageNumber(0);
        $paginator->setItemCountPerPage(200);

        $viewModel = new ViewModel(array(
            'title' => 'Aviso encontrado',
            'listaorsriat' => $paginator->getIterator(),
            'paginator' => $paginator,
            'formBuscadorAvisos' => $formBuscadorAvisos,
            'formBuscadorInstalacion' => $formBuscadorInstalacion,
            'formSucursales' => $formSelectSucurales,
            'formSelectEstados' => $formSelectEstados,
            'formFinalizar' => $formFinalizar,
          ));


        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("riat/index/index");

        return $viewModel;
    }

    /*
     * 
     * Finalizar los Riat Seleccionado solo si tienen las fechas de entrega de documentacion 
     * 
     */

    public function finalizarriatAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $formBuscadorAvisos = $this->getFormBuscadorAvisos();
        $formSelectSucurales = $this->getFormSelectSucursales();
        $formSelectEstados = $this->getFormSelectEstados();
        $formFinalizar = $this->getFormFinalizar();

        /* Validar Buscar Aviso */
        $formFinalizar->setInputFilter(new FormFinalizarValidator());
        $formFinalizar->setData($postParams);


        /*
         * Falla de la Validacion del form. 
         */
        if (!$formFinalizar->isValid()) {

            /* Filtrar solo por los estados Necesarios */
            $paginator = $this->getOrsRiatDao()->obtenerTodos();
            $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
            $paginator->setItemCountPerPage(200);

            $viewModel = new ViewModel(array(
                'title' => 'Fallo la Busqueda por Aviso',
                'listaorsriat' => $paginator->getIterator(),
                'paginator' => $paginator,
                'formBuscadorAvisos' => $formBuscadorAvisos,
                'formSucursales' => $formSelectSucurales,
                'formSelectEstados' => $formSelectEstados,
                'formFinalizar' => $formFinalizar,
            ));

            $viewModel->setTemplate('riat/index/index');

            return $viewModel;
        }


        $values = $formFinalizar->getData();

        foreach ($postParams["comprometidos"] as $value) {

            if ($value > 0) {

                $id = explode('_', $value);

                $id_ors_riat = $id[0];
                $id_aviso = $id[1];

                $data = array();
                $data['id_ors_riat'] = $id_ors_riat;
                $data['estados_ors_id_estado_ors'] = (int) 4;
                $data['fecha_entrega_dig'] = $values["fecha_entrega_dig"];
                $data['fecha_entrega_fisica'] = $values["fecha_entrega_fisica"];

                $orsRiat = new OrsRiat();
                $orsRiat->exchangeArray($data);

                $this->getOrsRiatDao()->updateOrsRiatFinalizados($orsRiat);
                unset($data);



                /***************************************************************
                 * Se finalizan los ors_riat y tambien se finalizan los avisos *
                 * ************************************************************* */

                $datos = array();

                $datos['id_aviso'] = $id_aviso;
                $datos['estados_avisos_id_estado_aviso'] = 4;
                $fecha = new \DateTime();
                $datos['fecha_finalizacion'] = $fecha->format('d-m-Y');

                $aviso = new Aviso();
                $aviso->exchangeArray($datos);

                $this->getavisoDao()->updateConfirmaFinalizacion($aviso);

                /*                 * *********************************** */
            }
        }


        /* Luego de Finalizar las riat volvemos a la pantalla principal */
        return $this->redirect()->toRoute('riat', array('controller' => 'index', 'action' => 'index'));
    }

    /*
     * 
     * Entra id desde bandeja principal
     * 
     * 
     *  */

    public function editarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);
        $r = (int) $this->params()->fromRoute('r', 0);

        if (!$id) {
            return $this->redirect()->toRoute('riat', array('controller' => 'index'));
           }

        
        /* EL formulario para cargar */
        $form = $this->getFormRegistroRiat();

            if( $r === 1 )
            {  
               /*Busca por el numero de aviso*/
               $servicioriat = $this->getOrsRiatDao()->obtenerPorNumAvisoCnRiat($id,1); 

            }else if( $r === 2 ) {
                
               /*Busca por el numero id de aviso*/      
               $servicioriat = $this->getOrsRiatDao()->obtenerPorNumAvisoCnRiat($id,2); 

            }else{

               /*Busca por el id de Riat*/
               $servicioriat = $this->getOrsRiatDao()->obtenerPorId($id);

             }
         
             
        $id = $servicioriat->getId_ors_riat();

        $estados_ors_id_estado_ors = $servicioriat->getEstados_ors_id_estado_ors();

        
       /*
        * Rutina que asigna el digitador que acepto el trabajo. 
        */
        if ($r===0) {               
        if ($servicioriat->getFecha_asignacion_riat() === null) {

            $data = array();
            $data['id_ors_riat'] = $id;
            $data['estados_ors_id_estado_ors'] = (int) 2;
            $fecha = new \DateTime();
            $data['fecha_asignacion_riat'] = $fecha->format('d-m-Y');

            $orsRiat = new OrsRiat();
            $orsRiat->exchangeArray($data);

            $this->getOrsRiatDao()->updateOrsRiatAsignarDigitador($orsRiat);
            unset($data);

            }
          }
          
          
        /*Busca por el id de Riat*/
        $servicioriat = $this->getOrsRiatDao()->obtenerPorId($id);
            
                 
        /* PRIMERO SE CARGAN LOS DATOS DEL REGISTRO ORS_RIAT */
        $form->bind($servicioriat);
        
        if ($r>0){ 
            
            $form->get('num_form_riat')->setAttributes(array('readonly' => 'readonly')); 
            $form->get('fecha_atencion_servicio')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('persona_tecnico')->setAttributes(array('disabled' => 'disabled')); 
            $form->get('persona_tecnico_2')->setAttributes(array('disabled' => 'disabled'));
            
            $form->get('numero_aviso')->setAttributes(array('readonly' => 'readonly')); 
            
            $form->get('img_form_riat')->setAttributes(array('disabled' => 'disabled'));
            $form->get('img_riat[]')->setAttributes(array('disabled' => 'disabled'));
            
         }
         

        /*
         * 
         * Validacion de Imagenes Adjuntos en el Formulario
         * 
         */

        $tienefoto = 0;
        $url_imagen = '/';
        
        $tienecert = 0;
        $url_cert = '/';
        
        $tienedevo = 0;
        $url_devo = '/';
        
        $tienezip = 0;
        $url_zip = '/';
        
        $tienefotosterreno = 0;
        $url_imagen_terreno = '/';
        
        
        /* Primero saber si existe el directorio */

        $directory = '/home/meditres/public_html/public/images/' . $servicioriat->getInstalacion()->getNum_instalacion() . '/' . $servicioriat->getAviso()->getNumero_aviso() .'/';

        
        if (is_dir($directory)) {
            /* Si existe busco la imagen relacionada al formulario */
            if ($servicioriat->getNum_form_riat() != "") {
                    /* Si existe imagen del formulario riat */
                    if (file_exists( $directory .'riat_'. $servicioriat->getNum_form_riat() .'.pdf' )){
                         $tienefoto = 1;
                         $url_imagen = '/images/'. $servicioriat->getInstalacion()->getNum_instalacion() . '/' . $servicioriat->getAviso()->getNumero_aviso() . '/riat_' . $servicioriat->getNum_form_riat() .'.pdf'; 
                    }
                    
                    if (file_exists( $directory .'cert_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.pdf' )){
                         $tienecert = 1;
                         $url_cert = '/images/'. $servicioriat->getInstalacion()->getNum_instalacion() .'/'. $servicioriat->getAviso()->getNumero_aviso() .'/cert_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.pdf'; 
                    }
                    
                    if (file_exists( $directory .'devo_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.pdf' )){
                         $tienedevo = 1;
                         $url_devo = '/images/'. $servicioriat->getInstalacion()->getNum_instalacion() .'/'. $servicioriat->getAviso()->getNumero_aviso() .'/devo_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.pdf'; 
                    }
                    
                    /* Si existe un archivo zip asociado al registro */
                    if (file_exists( $directory .'zip_'. $servicioriat->getInstalacion()->getNum_instalacion() .'_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.zip' )){
                        $tienezip = 1;
                        $url_zip = '/images/'. $servicioriat->getInstalacion()->getNum_instalacion() . '/' . $servicioriat->getAviso()->getNumero_aviso() . '/zip_'. $servicioriat->getInstalacion()->getNum_instalacion() .'_'. $servicioriat->getAviso()->getNumero_aviso() .'_'. $servicioriat->getNum_form_riat() .'.zip'; 
                    }

                    /* Si existen imagenes de trabajos en terreno */
                    if (file_exists( $directory .'img_riat_' . $servicioriat->getAviso()->getNumero_aviso() . '_'  . $servicioriat->getNum_form_riat() . '_0.jpg' )){
                         $tienefotosterreno = 1;                    
                         $url_imagen_terreno = '/images/'. $servicioriat->getInstalacion()->getNum_instalacion() . '/' . $servicioriat->getAviso()->getNumero_aviso() . '/img_riat_' . $servicioriat->getAviso()->getNumero_aviso() . '_'  . $servicioriat->getNum_form_riat() . '_0.jpg'; 
                    }
                 }
              }

         

        /* DATOS INSTALACION y AVISO */
        $form->get('id_aviso')->setValue($servicioriat->getAviso()->getId_aviso());
                
        /* SERVICIOS PRESTADOS EN EL AVISO */
        $serviciosCargados = $this->getDetalleServicioPrestado()->obtenerPorIdaviso($servicioriat->getAviso()->getId_aviso());
        /*++++++++++++++++++++++++++++++++++*/
                
        $form->get('numero_aviso')->setValue($servicioriat->getAviso()->getNumero_aviso());
        if ($r>0) $form->get('numero_aviso')->setAttributes(array('readonly' => 'readonly')); 
        
        /* Cargo todos los tecnicos */
        $form->get('persona_tecnico')->setValueOptions($this->getavisoDao()->obtenerPersonalTecnicoSelect());
        /* Cargo el tecnico asignado actual */
        $form->get('persona_tecnico')->setValue($servicioriat->getAviso()->getResponsable_personal_id_persona());
        if ($r>0) $form->get('persona_tecnico')->setAttributes(array('readonly' => 'readonly')); 
        
        
        /* Cargo todos los tecnicos */
        $form->get('persona_tecnico_2')->setValueOptions($this->getavisoDao()->obtenerPersonalTecnicoSelect());
        /* Cargo el tecnico asignado actual */
        $form->get('persona_tecnico_2')->setValue($servicioriat->getTecnico2_personal_id_persona());
        if ($r>0) $form->get('persona_tecnico_2')->setAttributes(array('readonly' => 'readonly'));
        
        
        $form->get('id_instalacion')->setValue($servicioriat->getInstalacion()->getId_instalacion());

        $form->get('distribuidora')->setValue($servicioriat->getDistribuidora()->getNombre());

        $form->get('nombrecliente')->setValue($servicioriat->getInstalacion()->getNombre_cliente());
        if ($r>0) $form->get('nombrecliente')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('direccion')->setValue($servicioriat->getInstalacion()->getDireccion1());
        if ($r>0) $form->get('direccion')->setAttributes(array('readonly' => 'readonly'));
        
        
        /* Todas las Regiones */
        $form->get('region')->setValueOptions($this->getOrsRiatDao()->obtenerRegionesSelect());
        /* Region Seleccionada */
        $form->get('region')->setValue($servicioriat->getProvincia()->getRegiones_id_region());
        if ($r>0) $form->get('region')->setAttributes(array('disabled' => 'disabled'));
         
        /* Todas las Provincias de la region */
        $form->get('provincia')->setValueOptions($this->getOrsRiatDao()->obtenerProvincias($servicioriat->getProvincia()->getRegiones_id_region()));
        /* Provincia Seleccionada */
        $form->get('provincia')->setValue($servicioriat->getComuna()->getProvincias_id_provincia());
        if ($r>0) $form->get('provincia')->setAttributes(array('disabled' => 'disabled'));
        
        /* Todas las Comunas de la provincia */
        $form->get('comuna')->setValueOptions($this->getOrsRiatDao()->obtenerComunas($servicioriat->getComuna()->getProvincias_id_provincia()));
        /* Comuna Seleccionada */
        $form->get('comuna')->setValue($servicioriat->getInstalacion()->getComunas_id_comuna());
        if ($r>0) $form->get('comuna')->setAttributes(array('disabled' => 'disabled'));


/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

        /* Todos los Tipos de Anormalidad */

       /* Crear una tablar relacionada con el id del riat almacenar los datos de las anormalidades
           encontradas */

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


        $form->get('num_instalacion')->setValue($servicioriat->getInstalacion()->getNum_instalacion());
        if ($r>0) $form->get('num_instalacion')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('tarifa_terreno')->setValue($servicioriat->getInstalacion()->getTarifa_terreno());
        if ($r>0) $form->get('tarifa_terreno')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('tarifa')->setValue($servicioriat->getInstalacion()->getTarifa());
        
        $form->get('unidad_lectura')->setValue($servicioriat->getInstalacion()->getUnidad_lectura());
        if ($r>0) $form->get('unidad_lectura')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('tipo_aviso')->setValue($servicioriat->getAviso()->getTipo_aviso());
        if ($r>0) $form->get('tipo_aviso')->setAttributes(array('disabled' => 'disabled'));

        
        //$form->get('proviene_gavis')->setValue($servicioriat->getInstalacion()->getNum_instalacion());
        $form->get('placa_poste_empalme')->setValue($servicioriat->getInstalacion()->getNumero_poste_camara());
        if ($r>0) $form->get('placa_poste_empalme')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('capacidad_automatico')->setValue($servicioriat->getInstalacion()->getCapacidad_automatico());
        if ($r>0) $form->get('capacidad_automatico')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('pot_contratada')->setValue($servicioriat->getInstalacion()->getPotencia_contratada());
        if ($r>0) $form->get('pot_contratada')->setAttributes(array('readonly' => 'readonly'));


        /* SISTEMA DISTRIBUCION */
        $form->get('potencia_ssee')->setValue($servicioriat->getSistemadistribucion()->getPotencia_ssee());
        if ($r>0) $form->get('potencia_ssee')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('num_ssee')->setValue($servicioriat->getSistemadistribucion()->getNum_ssee());
        if ($r>0) $form->get('num_ssee')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('alimentador')->setValue($servicioriat->getSistemadistribucion()->getAlimentador());
        if ($r>0) $form->get('alimentador')->setAttributes(array('readonly' => 'readonly'));

        /* ECM */
        //$form->get('codigo_modelo_ecm')->setValue($servicioriat->getEcm()->setModelo
        $form->get('modelo_ecm')->setValue($servicioriat->getEcm()->getModelo());
        if ($r>0) $form->get('modelo_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('marca_ecm')->setValue($servicioriat->getEcm()->getMarca());
        if ($r>0) $form->get('marca_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('numero_series_ecm')->setValue($servicioriat->getEcm()->getSerie());
        if ($r>0) $form->get('numero_series_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('Anio_fabricación_ecm')->setValue($servicioriat->getEcm()->getAnio());
        if ($r>0) $form->get('Anio_fabricación_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('num_elementos_ecm')->setValue($servicioriat->getEcm()->getCantidad_elementos());
        if ($r>0) $form->get('num_elementos_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_corriente_1_ecm')->setValue($servicioriat->getEcm()->getIp1());
        if ($r>0) $form->get('escala_corriente_1_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_corriente_2_ecm')->setValue($servicioriat->getEcm()->getIp2());
        if ($r>0) $form->get('escala_corriente_2_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_corriente_3_ecm')->setValue($servicioriat->getEcm()->getIp3());
        if ($r>0) $form->get('escala_corriente_3_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_corriente_secundaria_ecm')->setValue($servicioriat->getEcm()->getIs_ecm());
        if ($r>0) $form->get('escala_corriente_secundaria_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_voltaje_1_ecm')->setValue($servicioriat->getEcm()->getVp());
        if ($r>0) $form->get('escala_voltaje_1_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('escala_voltaje_2_ecm')->setValue($servicioriat->getEcm()->getVs());
        if ($r>0) $form->get('escala_voltaje_2_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('razon_ttcc_conectado_1_ecm')->setValue($servicioriat->getEcm()->getTtcc1());
        if ($r>0) $form->get('razon_ttcc_conectado_1_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('razon_ttcc_conectado_2_ecm')->setValue($servicioriat->getEcm()->getTtcc2());
        if ($r>0) $form->get('razon_ttcc_conectado_2_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('razon_ttpp_conectado_1_ecm')->setValue($servicioriat->getEcm()->getTtpp1());
        if ($r>0) $form->get('razon_ttpp_conectado_1_ecm')->setAttributes(array('readonly' => 'readonly'));
        
        $form->get('razon_ttpp_conectado_2_ecm')->setValue($servicioriat->getEcm()->getTtpp2());
        if ($r>0) $form->get('razon_ttpp_conectado_2_ecm')->setAttributes(array('readonly' => 'readonly'));
        

        /* ANTECEDENTES EMPALME */
        $form->get('tipo_empalme')->setValue($servicioriat->getAntecedentesempalme()->getTipo_empalme());
        $form->get('acometida')->setValue($servicioriat->getAntecedentesempalme()->getAcometida());
        $form->get('circuito')->setValue($servicioriat->getAntecedentesempalme()->getTipo_circuito());
        $form->get('conexion')->setValue($servicioriat->getAntecedentesempalme()->getConexion_circuito());
        $form->get('medida')->setValue($servicioriat->getAntecedentesempalme()->getMedida());
		$form->get('voltaje')->setValue($servicioriat->getAntecedentesempalme()->getVoltaje());
		        
        if ($r>0) $form->get('tipo_empalme')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('acometida')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('circuito')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('conexion')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('medida')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('voltaje')->setAttributes(array('disabled' => 'disabled'));


        /* REGLETA */
        $form->get('marca_regleta')->setValue($servicioriat->getRegleta()->getMarca_regleta());
        $form->get('modelo_regleta')->setValue($servicioriat->getRegleta()->getModelo_regleta());
        $form->get('terminales_regleta')->setValue($servicioriat->getRegleta()->getNumero_terminales_regleta());
        $form->get('puente_corriente_regleta')->setValue($servicioriat->getRegleta()->getPuentes_corrientes());
        $form->get('puente_voltaje_regleta')->setValue($servicioriat->getRegleta()->getPuentes_voltajes());

        if ($r>0) $form->get('marca_regleta')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('modelo_regleta')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('terminales_regleta')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('puente_corriente_regleta')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('puente_voltaje_regleta')->setAttributes(array('disabled' => 'disabled'));
        
        /* CALENDARIO */
        //$fecha = new \DateTime();
        $form->get('fecha_encontrada_medidor_calendario')->setValue($servicioriat->getCalendariomedidor()->getFecha_encontrada_medidor());
        if ($r>0) $form->get('fecha_encontrada_medidor_calendario')->setAttributes(array('disabled' => 'disabled'));
        
        //$fecha = new \DateTime();
        $form->get('fecha_oficial_pais_calendario')->setValue($servicioriat->getCalendariomedidor()->getFecha_oficial_pais());
        if ($r>0) $form->get('fecha_oficial_pais_calendario')->setAttributes(array('disabled' => 'disabled'));
        
        //$fecha = new \DateTime();
        $form->get('fecha_dejada_medidor_calendario')->setValue($servicioriat->getCalendariomedidor()->getFecha_dejada_medidor());
        if ($r>0) $form->get('fecha_dejada_medidor_calendario')->setAttributes(array('disabled' => 'disabled'));


        /* RAZON ENCONTRADA */
        $form->get('razon_tc_calculada')->setValue($servicioriat->getRazonencontrada()->getRazon_tc_calculada());
        $form->get('razon_tp_placa')->setValue($servicioriat->getRazonencontrada()->getRazon_tp_placa());
        $form->get('contante_multiplicacion_facturacion_calculada_terreno')->setValue($servicioriat->getRazonencontrada()->getConstante_facturacion());
        $form->get('contante_multiplicacion_facturacion_calculada')->setValue($servicioriat->getInstalacion()->getConstante());
                
        if ($r>0) $form->get('razon_tc_calculada')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('razon_tp_placa')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('contante_multiplicacion_facturacion_calculada_terreno')->setAttributes(array('readonly' => 'readonly'));
        
        /* ORIGEN AFECTA A LA MEDIDA */
        $form->get('origen_afecta_select')->setValue($servicioriat->getOrigenafectacionalamedida()->getOrigen_afectacion());
        $form->get('origen_afecta_comentario')->setValue($servicioriat->getOrigenafectacionalamedida()->getComentario());

        if ($r>0) $form->get('origen_afecta_select')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('origen_afecta_comentario')->setAttributes(array('readonly' => 'readonly'));

        /* LECTURA MEDIDOR INSTALADO */
        $form->get('lecturamedidorinstalado_kwh')->setValue($servicioriat->getLecturamedidorinstalado()->getKwh());
        $form->get('lecturamedidorinstalado_kvar')->setValue($servicioriat->getLecturamedidorinstalado()->getKvar());
        $form->get('lecturamedidorinstalado_kwpunta')->setValue($servicioriat->getLecturamedidorinstalado()->getKw_punta());
        $form->get('lecturamedidorinstalado_kwsum')->setValue($servicioriat->getLecturamedidorinstalado()->getKw_sum());

        if ($r>0) $form->get('lecturamedidorinstalado_kwh')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('lecturamedidorinstalado_kvar')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('lecturamedidorinstalado_kwpunta')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('lecturamedidorinstalado_kwsum')->setAttributes(array('readonly' => 'readonly'));
        
        /* CONTACTO AUTORIZA */
        $form->get('contacto_nombre')->setValue($servicioriat->getContactoautoriza()->getNombre_contacto());
        $form->get('contacto_rut')->setValue($servicioriat->getContactoautoriza()->getRun());
        $form->get('contacto_telefono')->setValue($servicioriat->getContactoautoriza()->getFono());
        $form->get('contacto_email')->setValue($servicioriat->getContactoautoriza()->getEmail());

        if ($r>0) $form->get('contacto_nombre')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('contacto_rut')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('contacto_telefono')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('contacto_email')->setAttributes(array('readonly' => 'readonly'));
        
        
        /* PERSONA QUE DIGITO EL RIAT */
        $form->get('digita_nombres')->setValue($servicioriat->getDigitador_persona()->getNombres());
        $form->get('digita_apellidos')->setValue($servicioriat->getDigitador_persona()->getApellidos());
        $form->get('digita_email')->setValue($servicioriat->getDigitador_persona()->getEmail());
        $form->get('digita_run')->setValue($servicioriat->getDigitador_persona()->getRut_persona());
        //$form->get('contacto_nombre')->setValue($servicioriat->getDigitador_persona()->getFoto_carnet());   

        /* TECNICO EJECUTA RIAT */
        $form->get('tecnico_nombres')->setValue($servicioriat->getTecnico_persona()->getNombres());
        $form->get('tecnico_apellidos')->setValue($servicioriat->getTecnico_persona()->getApellidos());
        $form->get('tecnico_email')->setValue($servicioriat->getTecnico_persona()->getEmail());
        $form->get('tecnico_run')->setValue($servicioriat->getTecnico_persona()->getRut_persona());
            
        /* VIAJES FRUSTRADOS */
        $viajesfrustrados = $this->getViajesFrustradosDao()->obtenerPorIdRiat($id);
        foreach ($viajesfrustrados as $viaje) {

            $num = $viaje->getNum_visita();

            $form->get('select_norealizaservicio_' . $num)->setValue($viaje->getCods_viaje_frustrado_idcods_viaje_frustrado());

            $fecha = new \DateTime($viaje->getFecha_visita());

            $form->get('fecha_visita_' . $num)->setValue($fecha->format('d-m-Y'));

            $form->get('hora_visita_' . $num)->setValue($viaje->getHora_visita());
        }
        
        if ($r>0) $form->get('select_norealizaservicio_1')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('fecha_visita_1')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('hora_visita_1')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('select_norealizaservicio_2')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('fecha_visita_2')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('hora_visita_2')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('select_norealizaservicio_3')->setAttributes(array('disabled' => 'disabled'));
        if ($r>0) $form->get('fecha_visita_3')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('hora_visita_3')->setAttributes(array('readonly' => 'readonly'));
        
        
        /* MEDIDORES INSTALADOS */
        $medidoresinstalacion = $this->getMedidorInstalacionDao()->obtenerPorIdInstalacion($servicioriat->getInstalacion()->getId_instalacion());
        foreach ($medidoresinstalacion as $medidor) {

            $tip = $medidor->getAct_reac();

            if ($tip == 1)
                $tip = 'act';
            else
                $tip = 'reac';

            $form->get('id_modelo_' . $tip)->setValue($medidor->getModelos_medidores_id_modelo());
            $form->get('codigo_medidor_' . $tip)->setValue($medidor->getModelo_medidor()->getCodigo_modelo());
            $form->get('numero_serie_' . $tip)->setValue($medidor->getNumero_serie());
            $form->get('marca_' . $tip)->setValue($medidor->getModelo_medidor()->getMarca());
            $form->get('modelo_' . $tip)->setValue($medidor->getModelo_medidor()->getModelo());
            $form->get('anio_fabricacion_' . $tip)->setValue($medidor->getAnio_fabricacion());
            $form->get('numeros_elementos_' . $tip)->setValue($medidor->getModelo_medidor()->getNumero_elementos());
            $form->get('voltaje_' . $tip)->setValue($medidor->getModelo_medidor()->getVoltaje());
            $form->get('corriente_' . $tip)->setValue($medidor->getModelo_medidor()->getCorriente_nominal());

            $form->get('constante_kh_' . $tip)->setValue($medidor->getModelo_medidor()->getKp() . '-' . $medidor->getModelo_medidor()->getKq());

            $form->get('constante_interna_' . $tip)->setValue($medidor->getModelo_medidor()->getContante_interna());
            $form->get('tecnologia_' . $tip)->setValue($medidor->getModelo_medidor()->getTecnologia());
            $form->get('reg_dda_maxima_' . $tip)->setValue($medidor->getModelo_medidor()->getDemanda_max());

        }
        
        if ($r>0) $form->get('codigo_medidor_act')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('numero_serie_act')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('anio_fabricacion_act')->setAttributes(array('readonly' => 'readonly'));
        
        if ($r>0) $form->get('codigo_medidor_reac')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('numero_serie_reac')->setAttributes(array('readonly' => 'readonly'));
        if ($r>0) $form->get('anio_fabricacion_reac')->setAttributes(array('readonly' => 'readonly'));
        

        /* TRANFORMADORES INSTALADOS */
        $tranformadoresinstalacion = $this->getTransformadorCorrienteDao()->obtenerPorIdInstalacion($servicioriat->getInstalacion()->getId_instalacion());
        $tip = 0;
        foreach ($tranformadoresinstalacion as $tranformador) {

            $tip++;

            $form->get('serie_tra_' . $tip)->setValue($tranformador->getSerie());
            $form->get('anio_tra_' . $tip)->setValue($tranformador->getAnio());
            $form->get('rtc_inp_tra_' . $tip)->setValue($tranformador->getInp());
            $form->get('rtc_ins_tra_' . $tip)->setValue($tranformador->getIns());
            $form->get('np_tra_' . $tip)->setValue($tranformador->getNum_pasadas());
            $form->get('marca_tra_' . $tip)->setValue($tranformador->getMarca());
        }
        
          if ($r>0) $form->get('serie_tra_1')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('anio_tra_1')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_inp_tra_1')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_ins_tra_1')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('np_tra_1')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('marca_tra_1')->setAttributes(array('readonly' => 'readonly'));        
                  
          if ($r>0) $form->get('serie_tra_2')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('anio_tra_2')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_inp_tra_2')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_ins_tra_2')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('np_tra_2')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('marca_tra_2')->setAttributes(array('readonly' => 'readonly')); 
          
          if ($r>0) $form->get('serie_tra_3')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('anio_tra_3')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_inp_tra_3')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('rtc_ins_tra_3')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('np_tra_3')->setAttributes(array('readonly' => 'readonly'));
          if ($r>0) $form->get('marca_tra_3')->setAttributes(array('readonly' => 'readonly')); 



        /* VALOR TOTAL POTENCIAS */
        $ValorTotalPotencias = $this->getValorTotalPotenciasDao()->obtenerPorIdRiat($id);
        foreach ($ValorTotalPotencias as $valorpotencia) {

            $tip = $valorpotencia->getTipo_registro();

            $form->get('potencia_activa_sec_' . $tip)->setValue($valorpotencia->getPotencia_activa_sec_kw());
            $form->get('potencia_activa_prim_' . $tip)->setValue($valorpotencia->getPotencia_activa_prim_kw());
            $form->get('potencia_reactiva_' . $tip)->setValue($valorpotencia->getPotencia_reactiva_kvar());

        }
        
        if ($r>0) {
            
            $form->get('potencia_activa_sec_1')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_activa_prim_1')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_reactiva_1')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('potencia_activa_sec_2')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_activa_prim_2')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_reactiva_2')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('potencia_activa_sec_3')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_activa_prim_3')->setAttributes(array('readonly' => 'readonly'));
            $form->get('potencia_reactiva_3')->setAttributes(array('readonly' => 'readonly'));
                    
        }
                 
        /* MANTENCION REALIZADA */
        $MantencionRealizada = $this->getMantencionRealizadaDao()->obtenerPorIdRiat($id);
        foreach ($MantencionRealizada as $resultado) {

            $tip = $resultado->getCods_mant_realiz_idcods_mant_realiz();
            $form->get('mantrealizada_' . $tip)->setValue($resultado->getResultado_mantencion_realizada());
        }

        if ($r>0) {
            
            $form->get('mantrealizada_1')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_2')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_3')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_4')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_5')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_6')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_7')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_8')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_9')->setAttributes(array('disabled' => 'disabled'));
            $form->get('mantrealizada_10')->setAttributes(array('disabled' => 'disabled'));
                    
        }
        

        /* REMPLAZO DE ELEMENTO EMPALME */
        $RemplazoElementoEmpalme = $this->getRemplazoElementoEmpalmeDao()->obtenerPorIdRiat($id);
        foreach ($RemplazoElementoEmpalme as $resultado) {

            $tip = $resultado->getTipo_elementos_idtipo();

            $form->get('numeroserie_' . $tip)->setValue($resultado->getNumero_serie());
            $form->get('marca_' . $tip)->setValue($resultado->getMarca());
            $form->get('codigomodelo_' . $tip)->setValue($resultado->getCodigo_modelo());
            $form->get('propiedad_' . $tip)->setValue($resultado->getPropiedad());
        }

        if ($r>0) {
            
            $form->get('numeroserie_1')->setAttributes(array('readonly' => 'readonly'));
            $form->get('marca_1')->setAttributes(array('readonly' => 'readonly'));
            $form->get('codigomodelo_1')->setAttributes(array('readonly' => 'readonly'));
            $form->get('propiedad_1')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('numeroserie_2')->setAttributes(array('readonly' => 'readonly'));
            $form->get('marca_2')->setAttributes(array('readonly' => 'readonly'));
            $form->get('codigomodelo_2')->setAttributes(array('readonly' => 'readonly'));
            $form->get('propiedad_2')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('numeroserie_3')->setAttributes(array('readonly' => 'readonly'));
            $form->get('marca_3')->setAttributes(array('readonly' => 'readonly'));
            $form->get('codigomodelo_3')->setAttributes(array('readonly' => 'readonly'));
            $form->get('propiedad_3')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('numeroserie_4')->setAttributes(array('readonly' => 'readonly'));
            $form->get('marca_4')->setAttributes(array('readonly' => 'readonly'));
            $form->get('codigomodelo_4')->setAttributes(array('readonly' => 'readonly'));
            $form->get('propiedad_4')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('numeroserie_5')->setAttributes(array('readonly' => 'readonly'));
            $form->get('marca_5')->setAttributes(array('readonly' => 'readonly'));
            $form->get('codigomodelo_5')->setAttributes(array('readonly' => 'readonly'));
            $form->get('propiedad_5')->setAttributes(array('readonly' => 'readonly'));
            
            $form->get('send')->setAttributes(array('disabled' => 'disabled'));
            $form->get('comprometo')->setAttributes(array('disabled' => 'disabled'));
                        
         } 
             
			 
		/*
		 * 
		 *  Anormalidades Detectadas 
		 * 
		 */

		$resultado_medidores = $this->getOrsRiatDao()->obtenerResultadoAnormalidades($id);
		$codigo_anormalidad = array();
		
        foreach ($resultado_medidores as $key => $value) {
        	
			     $codigo_anormalidad[] =  $value['codigo_anormalidad']; 
			
				 $form->get('origen_anormalidad' . ($key+1) )->setValue($value["origen"]);
				 
                 $form->get('tratamiento_anormalidad' . ($key+1) )->setValue($value["tratamiento"]);
				 
                 $form->get('mot_no_normalizado_anormalidad' . ($key+1) )->setValue($value["mot_no_normalizado"]);

              }
	
	
       /*******
		* 
		*    
	    *  Descripcion de codigo anormalidad asociado  
		*   
		* 
	    *******/
		 
		$catalogo = array();
		
	    foreach ($codigo_anormalidad as $key => $value) {
			
			$catalogoString = $this->getOrsRiatDao()->obtenerCodigoAnormalidad($value);

            if(!empty($catalogoString["tipo_anormalidad"])){

			$catalogo[] = 	$catalogoString["tipo_anormalidad"] . '. en ' . $catalogoString["ubicacion_anormalidad"] .', <br>'. $catalogoString["componente"]. '.' . $catalogoString["descripcion_anormalidad"] . '.<br>';

            }

		 }
		

		
		
        if($estados_ors_id_estado_ors == 4 ){
             $form->get('comprometo')->setAttributes(array('disabled' => 'disabled'));
           }
    
	
        /*Busca todos los medidores disponibles*/
        $medidores = $this->getOrsRiatDao()->obtenerMedidores();
		

        $modelView = new ViewModel(array(
                                          'r' => $r,  
                                          'title' => 'Registro de Inspección y Auditoria Trifásica 1.4', 
                                          'serviciosCargados' => $serviciosCargados, 
                                          'form' => $form, 
                                          'tienefoto' => $tienefoto, 
                                          'url_imagen' => $url_imagen,
                                          'tienefotosterreno' => $tienefotosterreno,
                                          'url_imagen_terreno' => $url_imagen_terreno, 
                                          'tienezip' => $tienezip,
                                          'url_zip' => $url_zip,
                                          'tienecert' => $tienecert,
                                          'url_cert' => $url_cert,
                                          'tienedevo' => $tienedevo,
                                          'url_devo' => $url_devo,
                                          'medidores' => $medidores,
                                          'codigo_anormalidad' => $codigo_anormalidad,
                                          'catalogo_resultados' => $catalogo
                                        )
                                    );
        
        
        
        $modelView->setTemplate('riat/index/crear');

        return $modelView;
      
        
    }

    /*
     * 
     * Se enviar el formulario para crear un nuevo registro 
     * 
     * 
     */

    public function crearAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $form = $this->getFormRegistroRiat();

        return new ViewModel(array('title' => 'Registro de Inspección y Auditoria Trifásica', 'form' => $form));
    }

    
   /*
    * Eliminar Imagen del Riat o Formulario
    * 
    */
    public function eliminarImgRiatAction(){
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
               return $this->redirect()->toRoute('riat', array('controller' => 'index'));
            }
                        
        /*Parametros Post desde el formulario VALORIZA*/
        $postParams = $this->request->getPost();
                        
        $file = '/home/meditres/public_html/public' . $postParams["url_riat"];
                                
        unlink($file);
        
        return $this->redirect()->toRoute('riat', array('controller' => 'index','action'=>'editar','id'=> $postParams["id_ors_riat"] )); 
        
     }
    
     
     
      /*
    * Eliminar Imagen del Riat o Formulario
    * 
    */
    public function eliminarCertAction(){
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
               return $this->redirect()->toRoute('riat', array('controller' => 'index'));
            }
                        
        /*Parametros Post desde el formulario VALORIZA*/
        $postParams = $this->request->getPost();
                        
        $file = '/home/meditres/public_html/public' . $postParams["url_cert"];
                                
        unlink($file);
        
        return $this->redirect()->toRoute('riat', array('controller' => 'index','action'=>'editar','id'=> $postParams["id_ors_riat"] )); 
        
     }
     
     
     
     
   /*
    * 
    * Eliminar Imagen del Formulario Devolucion
    * 
    */
    public function eliminarDevoAction(){
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
               return $this->redirect()->toRoute('riat', array('controller' => 'index'));
            }
                        
        /*Parametros Post desde el formulario VALORIZA*/
        $postParams = $this->request->getPost();
                        
        $file = '/home/meditres/public_html/public' . $postParams["url_devo"];
                                
        unlink($file);
        
        return $this->redirect()->toRoute('riat', array('controller' => 'index','action'=>'editar','id'=> $postParams["id_ors_riat"] )); 
        
     }
     
     
     
     /*
    * Eliminar Imagen del Riat o Formulario
    * 
    */
    public function eliminarzipAction(){
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
               return $this->redirect()->toRoute('riat', array('controller' => 'index'));
            }
                        
        /*Parametros Post desde el formulario VALORIZA*/
        $postParams = $this->request->getPost();
                        
        $file = '/home/meditres/public_html/public' . $postParams["url_zip"];
                                
        unlink($file);
        
        return $this->redirect()->toRoute('riat', array('controller' => 'index','action'=>'editar','id'=> $postParams["id_ors_riat"] )); 
        
     }
     
     
    /*
     * 
     * Nos Permite eliminar las fotos de forma recursiva 
     * solo se indica el directorio y seleccionamos todas las imagenes JPG.
     * 
     */
     public function eliminarFotosTerrenoAction() {
               
        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        if (!$this->getRequest()->isPost()) {
               return $this->redirect()->toRoute('riat', array('controller' => 'index'));
            }
                        
        /*Parametros Post desde el formulario VALORIZA*/
        $postParams = $this->request->getPost();

        $dir = "/home/meditres/public_html/public/images/" . $postParams['num_instalacion'] ."/". trim($postParams['numero_aviso']) .'/'; // directory name

        if (is_dir($dir)) {
            
              $mask = "*.jpg";
              array_map( "unlink", glob( $dir . $mask ) );
                
            }
        
        return $this->redirect()->toRoute('riat', array('controller' => 'index','action'=>'editar','id'=> $postParams["id_ors_riat"] )); 
              
     }
    
    

    /*
    * 
    * Funcion para cargar la imagen del riat
    * esto es un pdf con el documento de terreno como imagen
    * 
    * 
    */
    
    private function cargaImgRiatAction($nombreElementFile, $arrayElementFile, $NombreImagen, $Dir) {

        $form = $this->getFormRegistroRiat();
        $nombresImagenes = (String) "";

        /*
         * se ejecuta el ciclo segun la cantidad de elementos type file 
         */

        if ($arrayElementFile['error'] == 0) {

            $cargado = false;

            $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 10000000));
            $validatorExt = new \Zend\Validator\File\Extension('pdf');
            $validatorMime = new \Zend\Validator\File\MimeType('application/pdf');
            //$validatorIsImage = new \Zend\Validator\File\IsImage();
            //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));

            $adapter = new \Zend\File\Transfer\Adapter\Http();

            $results = array();
            $results['size'] = $validatorSize->isValid($arrayElementFile['name']);
            $results['ext'] = $validatorExt->isValid($arrayElementFile['name']);
            $results['mime'] = $validatorMime->isValid($arrayElementFile['name']);
            //$results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
            //$results['Count'] = $validatorCount->isValid($fileParams['name']);

            $adapter->setValidators(array(
                $validatorSize,
                $validatorExt,
                $validatorMime,
                    ), $arrayElementFile['name']);

            //\Zend\Debug\Debug::dump($results);


            /* Si existe error en la validacion se retorna al formulario con los msg de error */
            if (!$adapter->isValid($nombreElementFile)) {


                $dataError = $adapter->getMessages();
                $error = array();

                foreach ($dataError as $key => $row) {
                    $error[] = $row;
                }

                echo 'el archivo que adjunto no es correcto por favor volver atras e intentar nuevamente <br>';
                var_dump($error);

                exit;


                /*
                 * retornamos a la vista crear donde esta el formulario con los msg de error
                 */
                $form->setMessages(array($nombreElementFile => $error));
                $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
                $modelView->setTemplate('riat/index/crear');
                return $modelView;
                
            } else {


                /* Directorio donde se cargan las fotos dentro del modulo */
                ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');

                $adapter->setDestination('/home/meditres/public_html/public/images/' . $Dir . '/');


                /*
                 * extencion del archivo
                 */

                $extension = pathinfo($arrayElementFile['name'], PATHINFO_EXTENSION);


                /*
                 * se modifica el nombre del archivo se anade un calculo aleatorio
                 */

                ///var/www/html/sam/public/images/fotos/

                $adapter->addFilter('Rename', array(
                    'target' => '/home/meditres/public_html/public/images/' . $Dir . '/' . $NombreImagen . '.' . strtolower($extension),
                    'overwrite' => true,
                ));

                /*
                 * se cargar el archivo en forma exitosa
                 */

                if ($adapter->receive($arrayElementFile['name'])) {

                    //$nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                    //$nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                }
            }
        }

        /*
         *  si el archivo no llega al servidor se envia msg de error
         */

        if (!$arrayElementFile['error'] == 0) {

            $error = array('Error del Archivo Cargado Codigo Error : ' . $arrayElementFile['error']);

            echo 'EL archivo cargado tiene errores volver atras <br>';
            
            var_dump($error);

            exit;

            /*
             * retorna a la vista con los msg de error correspondientes
             */
            $form->setMessages(array($nombreElementFile => $error));
            $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
            $modelView->setTemplate('riat/index/crear');
            return $modelView;
            
        }


        unset($adapter);

        /* Se retorna un arregle con los nombres de las imagenes */
        //echo 'ok';
        
    }
   
    
    
    
    /*
    * Funcion para cargar archivo ZIP
    */
    
    private function cargaZipAction($nombreElementFile, $arrayElementFile, $NombreImagen, $Dir) {

        /*
         * se ejecuta el ciclo segun la cantidad de elementos type file 
         */

        if ($arrayElementFile['error'] == 0) {

            $cargado = false;

            $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 30000000));
            $validatorExt = new \Zend\Validator\File\Extension('zip');
            $validatorMime = new \Zend\Validator\File\MimeType('application/zip,application/x-zip,application/octet-stream,application/x-zip-compressed');
            //$validatorIsImage = new \Zend\Validator\File\IsImage();
            //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));

            $adapter = new \Zend\File\Transfer\Adapter\Http();

            $results = array();
            $results['size'] = $validatorSize->isValid($arrayElementFile['name']);
            $results['ext'] = $validatorExt->isValid($arrayElementFile['name']);
            $results['mime'] = $validatorMime->isValid($arrayElementFile['name']);
            //$results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
            //$results['Count'] = $validatorCount->isValid($fileParams['name']);

            $adapter->setValidators(array(
                $validatorSize,
                $validatorExt,
                $validatorMime,
                    ), $arrayElementFile['name']);

            //\Zend\Debug\Debug::dump($results);


            /* Si existe error en la validacion se retorna al formulario con los msg de error */
            if (!$adapter->isValid($nombreElementFile)) {


                $dataError = $adapter->getMessages();
                $error = array();

                foreach ($dataError as $key => $row) {
                    $error[] = $row;
                }

                echo 'el archivo que adjunto no es correcto por favor volver atras e intentar nuevamente <br>';
                var_dump($error);

                exit;
                
            } else {


                /* Directorio donde se cargan las fotos dentro del modulo */
                ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');

                $adapter->setDestination('/home/meditres/public_html/public/images/' . $Dir . '/');


                /*
                 * extencion del archivo
                 */

                $extension = pathinfo($arrayElementFile['name'], PATHINFO_EXTENSION);


                /*
                 * se modifica el nombre del archivo se anade un calculo aleatorio
                 */

                ///var/www/html/sam/public/images/fotos/

                $adapter->addFilter('Rename', array(
                    'target' => '/home/meditres/public_html/public/images/' . $Dir . '/' . $NombreImagen . '.' . strtolower($extension),
                    'overwrite' => true,
                ));

                /*
                 * se cargar el archivo en forma exitosa
                 */

                if ($adapter->receive($arrayElementFile['name'])) {

                    //$nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                    //$nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                }
            }
        }

        /*
         *  si el archivo no llega al servidor se envia msg de error
         */

        if (!$arrayElementFile['error'] == 0) {

            $error = array('Error del Archivo Cargado Codigo Error : ' . $arrayElementFile['error']);

            echo 'EL archivo cargado tiene errores volver atras <br>';
            
            var_dump($error);

            exit;
            
        }

        unset($adapter);
        
    }
    
    
    
    /*
    * 
    * Funcion para cargar las fotos de la Auditoria
    * 
    *      
    */
    
    private function cargaFotosRiatAction($nombreElementFile, $arrayElementFile, $NombreImagen, $Dir) {

        $form = $this->getFormRegistroRiat();
        $nombresImagenes = (String) "";

        /*
         * se ejecuta el ciclo segun la cantidad de elementos type file 
         */   

        if ($arrayElementFile['error'] == 0) {

            $cargado = false;

            $validatorSize = new \Zend\Validator\File\Size(array('min' => 1, "max" => 10000000));
            $validatorExt = new \Zend\Validator\File\Extension('jpg,jpeg,bmp');
            $validatorMime = new \Zend\Validator\File\MimeType('image/jpg,image/jpeg,image/bmp');
            $validatorIsImage = new \Zend\Validator\File\IsImage();
            //$validatorCount = new \Zend\Validator\File\Count(array("min" => 1, "max" => 1));

            $adapter = new \Zend\File\Transfer\Adapter\Http();

            $results = array();
            $results['size'] = $validatorSize->isValid($arrayElementFile['name']);
            $results['ext'] = $validatorExt->isValid($arrayElementFile['name']);
            $results['mime'] = $validatorMime->isValid($arrayElementFile['name']);
            $results['isimage'] = $validatorIsImage->isValid( $arrayElementFile['name'] );
            //$results['Count'] = $validatorCount->isValid($fileParams['name']);

            $adapter->setValidators( array(
                                           $validatorSize,
                                           $validatorExt,
                                           $validatorMime,
                                           $validatorIsImage,
                                          ), $arrayElementFile['name'] );

            //\Zend\Debug\Debug::dump($results);


            /* Si existe error en la validacion se retorna al formulario con los msg de error */
            if (!$adapter->isValid($nombreElementFile)) {


                $dataError = $adapter->getMessages();
                $error = array();

                foreach ($dataError as $key => $row) {
                    $error[] = $row;
                }

                echo 'la imagen que adjunto no esta correcta por favor volver atras e intentar nuevamente <br>';
                var_dump($error);

                /*
                 * retornamos a la vista crear donde esta el formulario con los msg de error
                 */
                $form->setMessages(array($nombreElementFile => $error));
                $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
                $modelView->setTemplate('riat/index/crear');
                return $modelView;
                
            } else {


                /* Directorio donde se cargan las fotos dentro del modulo */
                ///$adapter->setDestination('/var/www/html/sam/public/images/fotos/');

                $adapter->setDestination('/home/meditres/public_html/public/images/' . $Dir . '/');


                /*
                 * extencion del archivo
                 */

                $extension = pathinfo($arrayElementFile['name'], PATHINFO_EXTENSION);


                /*
                 * se modifica el nombre del archivo se anade un calculo aleatorio
                 */

                ///var/www/html/sam/public/images/fotos/

                $adapter->addFilter('Rename', array(
                    'target' => '/home/meditres/public_html/public/images/' . $Dir . '/' . $NombreImagen . '.' . strtolower($extension),
                    'overwrite' => true,
                ));

                /*
                 * se cargar el archivo en forma exitosa
                 */

                if ($adapter->receive($arrayElementFile['name'])) {

                    //$nombresImagenes = explode("/",$adapter->getFileName($nombreElementFile));
                    //$nombresImagenes = $nombresImagenes[count($nombresImagenes)-1];
                }
            }
        }

        /*
         *  si el archivo no llega al servidor se envia msg de error
         */

        if (!$arrayElementFile['error'] == 0) {

            $error = array('Error del Archivo Cargado Codigo Error : ' . $arrayElementFile['error']);

            echo 'EL archivo cargado tiene errores volver atras <br>';
            var_dump($error);

            exit;

            /*
             * retorna a la vista con los msg de error correspondientes
             */
            $form->setMessages(array($nombreElementFile => $error));
            $modelView = new ViewModel(array('title' => 'Datos mal Ingresados', 'form' => $form));
            $modelView->setTemplate('riat/index/crear');
            return $modelView;
            
        }


        unset($adapter);

        /* Se retorna un arregle con los nombres de las imagenes */
        //echo 'ok';
        
    }

    
    
    private function getPlantillaEmailHtml($datos) {

        $fecha = new \DateTime();
        $fecha->setTimezone(new \DateTimeZone('America/Santiago'));

        $renderer = $this->getServiceLocator()->get('ViewManager')->getRenderer();
        $renderereEmail = clone $renderer;

        $renderereEmail->distribuidora = $datos['distribuidora'];
        $renderereEmail->num_instalacion = $datos['num_instalacion'];
        $renderereEmail->numero_aviso = $datos['numero_aviso'];
        $renderereEmail->num_form_riat = $datos['num_form_riat'];
        $renderereEmail->fecha_atencion_servicio = $datos['fecha_atencion_servicio'];
        $renderereEmail->origen_afecta_comentario = $datos['origen_afecta_comentario'];

        $renderereEmail->fecha = $fecha->format('Y/m/d H:i:s');

        $plantillaEmail = $renderereEmail->render('riat/index/plantilla-email-html');

        return $plantillaEmail;
    }

    private function getPlantillaEmailText($datos) {

        $fecha = new \DateTime();
        $fecha->setTimezone(new \DateTimeZone('America/Santiago'));

        $renderer = $this->getServiceLocator()->get('ViewManager')->getRenderer();
        $renderereEmail = clone $renderer;

        $renderereEmail->distribuidora = $datos['distribuidora'];
        $renderereEmail->num_instalacion = $datos['num_instalacion'];
        $renderereEmail->numero_aviso = $datos['numero_aviso'];
        $renderereEmail->num_form_riat = $datos['num_form_riat'];
        $renderereEmail->fecha_atencion_servicio = $datos['fecha_atencion_servicio'];
        $renderereEmail->origen_afecta_comentario = $datos['origen_afecta_comentario'];

        $renderereEmail->fecha = $fecha->format('Y/m/d H:i:s');

        $plantillaEmail = $renderereEmail->render('riat/index/plantilla-email-text');

        return $plantillaEmail;
    }






    /*
     * 
     * registra un riat desde 0
     * 
     */

    public function registrarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
                

        if (!$this->getRequest()->isPost()) {

            $this->redirect()->toRoute('riat', array('controller' => 'index'));
            
        }

        /* Parametros Post desde el formulario riat */
        $postParams = $this->request->getPost();

        /* Array con todos los FILES ELEMENTS */
        $fileParams = $this->params()->fromFiles();

        
       /*
        * 
        * Todos los Avisos tienen que tener un directorio para cargar sus archivos adjuntos  
        * 
        */
        $filename = '/home/meditres/public_html/public/images/'. $postParams['num_instalacion'];

        if (!is_dir($filename)) {

            mkdir($filename, 0777);
            
            $filename = $filename . '/' . $postParams['numero_aviso'];
            
              if (!is_dir($filename)) {
                  
                  mkdir($filename, 0777);
                  
              }    
            
          }else{
               
             $filename = $filename . '/' . $postParams['numero_aviso'];
            
              if (!is_dir($filename)) {
                  
                  mkdir($filename, 0777);
                  
              } 
              
          }

        
        /*
         * 
         * funcion que ejecuta validaciones a las imagenes 
         * y carga en carpeta del aviso todas las imagenes.
         * 
         */
 
          
        /* EL mismo formulario riat se escanea y se adjunta en este formulario que son los mismos datos */
        if (!empty($fileParams["img_form_riat"]["name"])) {

            $nombre_imagen = 'riat' . '_' . $postParams["num_form_riat"];

            /*
             * Nombre del Element en el Form html
             * Array con datos de la Imagen
             * Nombre con el que se Guardara la Imagen 
             * Directorio o nombre de carpeta donde se guargara la imagen
             * 
             */

            $this->cargaImgRiatAction('img_form_riat', $fileParams["img_form_riat"], $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso']);
        }
        
        /* El Certificado de Laboratorio se daja adjunto en la aplicacion asociado al registro de auditoria */
        if (!empty($fileParams["img_certificado_lab"]["name"])) {

            $nombre_imagen = 'cert' . '_' . $postParams['numero_aviso'] .'_'. $postParams["num_form_riat"];

            $this->cargaImgRiatAction('img_certificado_lab', $fileParams["img_certificado_lab"], $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso']);
        }
        
        /* El Formulario de Devolucion */
        if (!empty($fileParams["img_devo"]["name"])) {

            $nombre_imagen = 'devo' . '_' . $postParams['numero_aviso'] .'_'. $postParams["num_form_riat"];

            $this->cargaImgRiatAction('img_devo', $fileParams["img_devo"], $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso']);
        }

        /*
         * En este zip se adjuntan todos los documentos extras que tiene un trabajo en terreno.
         * 
         */
        if (!empty($fileParams["archivozip"]["name"])) {

            $nombre_imagen = 'zip' . '_' . $postParams['num_instalacion'] . '_' . $postParams['numero_aviso'] . '_' . $postParams["num_form_riat"];

            $this->cargaZipAction('archivozip', $fileParams["archivozip"], $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso']);
        }


        /*
         * Verifico si el usuario envio fotos si es asi se envian a la funcion que carga las fotos
         * 
         */
        if (!empty($fileParams["img_riat"])) {

            /* Arreglo de Imagenes que son las evidencias del trabajo realizado (Auditoria Trifasica) */
            foreach ($fileParams["img_riat"] as $key => $value) {

                if (!empty($value["name"])) {

                    $nombre_imagen = 'img_riat' . '_' . $postParams['numero_aviso'] . '_' . $postParams["num_form_riat"] . '_' . $key;

                    /*
                     * Nombre del Element en el Form html
                     * Array con datos de la Imagen
                     * Nombre con el que se Guardara la Imagen 
                     * Directorio o nombre de carpeta donde se guargara la imagen
                     * 
                     */

                    $this->cargaFotosRiatAction('img_riat_' . $key . '_', $value, $nombre_imagen, $postParams['num_instalacion'] . '/' . $postParams['numero_aviso']);
                }
            }
        }
        
        
        $form = $this->getFormRegistroRiat();
        $form->setInputFilter(new RegistroRiatValidator());

        $form->setData($postParams);

        /* CARGAR LOS SELEC DESDE BASE DE DATOS */

        /* Todas las Regiones */
        $form->get('region')->setValueOptions($this->getOrsRiatDao()->obtenerRegionesSelect());

        /* Todas las Provincias de la region */
        $form->get('provincia')->setValueOptions($this->getOrsRiatDao()->obtenerProvincias($postParams['region']));

        /* Todas las Comunas de la provincia */
        $form->get('comuna')->setValueOptions($this->getOrsRiatDao()->obtenerComunas($postParams['provincia']));

        /* Cargo todos los tecnicos */
        $form->get('persona_tecnico')->setValueOptions($this->getavisoDao()->obtenerPersonalTecnicoSelect());
        $form->get('persona_tecnico_2')->setValueOptions($this->getavisoDao()->obtenerPersonalTecnicoSelect());  


        /*
         * 
         * Si falla la validacion del formulario retornamos 
         * los msj de error al formulario y de corta la action
         * 
         */
        if (!$form->isValid()) {

            $messages = $form->getMessages();
            
            $modelView = new ViewModel(array('title' => 'Datos mal Ingresados verificar campos', 'form' => $form , 'messages' => $messages , 'r' => 0 ));
            $modelView->setTemplate('riat/index/crear');
            return $modelView;
            
          }


        /* ESTAN TODOS LOS DATO EN UN SOLO ARREGLO */
        $values = $form->getData();

        /* PROCESO DE DESCOMPONER EL ARRAY E INSERTAR LOS DATOS EN LAS TABLAS CORRESPONDIENTES */

        $hoy = date("d-m-Y H:i:s");
        $fecha_actual = new \DateTime($hoy);
        
        
        /* EN CASO QUE SE COMPROMETA CON LOS DATOS DIGITADOS */
        if (isset($values["comprometo"])) {
            
            if ($values["comprometo"] === 'Comprometido') {

        
                $data = array();
                $data['id_ors_riat'] = $values["id_ors_riat"];
                $data['estados_ors_id_estado_ors'] = (int) 3;
                $fecha = new \DateTime();
                $data['fecha_comprometido'] = $fecha->format('d-m-Y');

                $orsRiat = new OrsRiat();
                $orsRiat->exchangeArray($data);

                $this->getOrsRiatDao()->updateOrsRiatComprometido($orsRiat);
                unset($data);
           
            }
            
         }

        
               /***************************************************************
                *                                                             *
                *                                                             *
                *  SI es un CNR se manda un correo al personal autorizado.    * 
                *                                                             *
                *                                                             *
                **************************************************************/

                /*Si tiene correo de supervisor se carga para enviar correo*/
                /*Si tiene correo de Jefe de Grupo se carga para enviar correo*/
                
                 /*
				  echo '<br>';
                   echo $message->toString();
                   exit;
				  */
                            
                /* if($_SESSION['envioCorreoAlertaCnr']['enviado'] == 'no'):               
                
                if($values["estado_riat"]==4):
                
                if(!empty($values['origen_afecta_comentario'])):  
                    
                    $datos = array(); 
                    $datos['distribuidora'] = $values['distribuidora'];
                    $datos['num_instalacion'] = $values['num_instalacion'];
                    $datos['numero_aviso'] = $values['numero_aviso'];        
                    $datos['num_form_riat'] = $values["num_form_riat"];
                    $datos['fecha_atencion_servicio'] = $values["fecha_atencion_servicio"];
                    $datos['origen_afecta_comentario'] = $values['origen_afecta_comentario'];

                           $htmlPart = new \Zend\Mime\Part($this->getPlantillaEmailHtml($datos));
                           $htmlPart->type = "text/html";

                           $body = new \Zend\Mime\Message();
                           $body->addPart($htmlPart);

                           $message = new \Zend\Mail\Message();  

                           $message->addFrom("pcardenas@medicions3.com","Operador S3")
                                     ->addTo("whurtado@tecnet.cl")
                                     ->addCc("gaparadam@tecnet.cl")
                                     ->addCc("pgcardenasg@tecnet.cl")
                                     ->addCc("vtapiaf@tecnet.cl")
                                     ->addCc("asegeurp@tecnet.cl")
                                   ->setSubject("Alerta CNR Detectado")
                                   ->setBody($body);            


                           
                            if(!empty($this->getLogin()->getIdentity()->sup_email)):  
                               $message->addCc($this->getLogin()->getIdentity()->sup_email);
                            endif;
                            
                            
                            if(!empty($this->getLogin()->getIdentity()->jfgrup_email)):  
                               $message->addCc($this->getLogin()->getIdentity()->jfgrup_email);
                            endif;

                           

                           try {
                               
                              $transport = new \Zend\Mail\Transport\Sendmail();
                              $transport->send($message);
                                 
                              $_SESSION['envioCorreoAlertaCnr']['enviado'] = 'si';
                                 
                            }catch(\Exception $e){

                              echo $e->getMessage();

                            }
                               
                        endif;  

                     endif;
                                        
                  endif;*/


                                        
    /*****************************
     *                           *
     *                           *  
     *  Fin de Envio de Correo.  *
     *                           *
     *                           * 
     *****************************/
     

        /* ORS RIAT */
        $data = array();
        $data['id_ors_riat'] = (int) $values["id_ors_riat"];
        $data['num_form_riat'] = (int) $values["num_form_riat"];
        $data['fecha_atencion_servicio'] = ($values["fecha_atencion_servicio"] === "") ? $hoy : $values["fecha_atencion_servicio"];
		
		$data['tiene_reloj'] = (String) $values["tiene_reloj"];
		$data['tiene_telemedida'] = (String) $values["tiene_telemedida"];
		$data['comportamiento_carga'] = (Int) $values["comportamiento_carga"];
		
        $data['tecnico2_personal_id_persona'] = (int) $values["persona_tecnico_2"];  
        
        $orsRiat = new OrsRiat();
        $orsRiat->exchangeArray($data);

        $this->getOrsRiatDao()->updateOrsRiat($orsRiat);
        unset($data);


        /* AVISO */
        $data = array();
        $data['id_aviso'] = $values['id_aviso'];
        //$data['numero_aviso'] = $values['numero_aviso'];
        $data['responsable_personal_id_persona'] = $values['persona_tecnico'];
        $data['tipo_aviso'] = $values['tipo_aviso'];

        $aviso = new Aviso();
        $aviso->exchangeArray($data);

        $this->getavisoDao()->updateNumAviso($aviso);
        unset($data);


        /* INSTALACION */
        $data = array();
        $data['id_instalacion'] = (int) $values['id_instalacion'];
        //$data['distribuidoras_id_distribuidora'] = (int) $values['distribuidora'];
        $data['nombre_cliente'] = $values['nombrecliente'];
        $data['direccion1'] = $values['direccion'];
        $data['comunas_id_comuna'] = (int) $values['comuna'];
        //$data['num_instalacion'] = $values['num_instalacion'];
        $data['tarifa_terreno'] = $values['tarifa_terreno'];
        $data['unidad_lectura'] = $values['unidad_lectura'];
        $data['numero_poste_camara'] = (int) $values['placa_poste_empalme'];
        $data['capacidad_automatico'] = $values['capacidad_automatico'];
        $data['potencia_contratada'] = $values['pot_contratada'];

        $instalacion = new Instalacion();
        $instalacion->exchangeArray($data);

        $this->getInstalacionDao()->updateRiat($instalacion);
        
        unset($data);



 
		 /**********************************
		  * 
		  *   ANORMALIDADES DETECTADAS
		  * 
		  **********************************/
		  
		      $id_ors_riat = (Int) $values["id_ors_riat"];
              $this->getOrsRiatDao()->deleteResultadoAnormalidades($id_ors_riat);
			  
			  $data = array();
			  $data['id_ors_riat'] = (Int) $values["id_ors_riat"];
			  $data["codigo_anormalidad"] = (String) $postParams["codigo_anormalidad1"];
			  $data["origen_anormalidad"] = (Int) $values["origen_anormalidad1"];
			  $data["tratamiento_anormalidad"] = (Int) $values["tratamiento_anormalidad1"];
			  $data["mot_no_normalizado_anormalidad"] = (Int) $values["mot_no_normalizado_anormalidad1"];
			  $this->getOrsRiatDao()->guardarResultadoAnormalidades($data);
			  unset($data);
			
			  $data = array();
			  $data['id_ors_riat'] = (Int) $values["id_ors_riat"];   
			  $data["codigo_anormalidad"] = (String) $postParams["codigo_anormalidad2"];
			  $data["origen_anormalidad"] = (Int) $values["origen_anormalidad2"];
			  $data["tratamiento_anormalidad"] = (Int) $values["tratamiento_anormalidad2"];
			  $data["mot_no_normalizado_anormalidad"] = (Int) $values["mot_no_normalizado_anormalidad2"];
			  $this->getOrsRiatDao()->guardarResultadoAnormalidades($data);
			   unset($data);
			 
			  $data = array();
			  $data['id_ors_riat'] = (Int) $values["id_ors_riat"];
			  $data["codigo_anormalidad"] = (String) $postParams["codigo_anormalidad3"];
			  $data["origen_anormalidad"] = (Int) $values["origen_anormalidad3"];
			  $data["tratamiento_anormalidad"] = (Int) $values["tratamiento_anormalidad3"];
			  $data["mot_no_normalizado_anormalidad"] = (Int) $values["mot_no_normalizado_anormalidad3"];
			  $this->getOrsRiatDao()->guardarResultadoAnormalidades($data); 
			   unset($data);
			
			  $data = array();
			  $data['id_ors_riat'] = (Int) $values["id_ors_riat"];
			  $data["codigo_anormalidad"] = (String) $postParams["codigo_anormalidad4"];
			  $data["origen_anormalidad"] = (Int) $values["origen_anormalidad4"];
			  $data["tratamiento_anormalidad"] = (Int) $values["tratamiento_anormalidad4"];
			  $data["mot_no_normalizado_anormalidad"] = (Int) $values["mot_no_normalizado_anormalidad4"];
			  $this->getOrsRiatDao()->guardarResultadoAnormalidades($data);	 
			   unset($data);
			
		 /****************************************/
 
 
 

        /* SISTEMA DISTRIBUCION */
        $data = array();
        $data['instalaciones_id_instalacion'] = (int) $values["id_instalacion"];
        $data['potencia_ssee'] = (int) $values["potencia_ssee"];
        $data['num_ssee'] = (int) $values["num_ssee"];
        $data['alimentador'] = (int) $values["alimentador"];

        $SistemaDistribucion = new SistemaDistribucion();
        $SistemaDistribucion->exchangeArray($data);   

        $this->getSistemaDistribucionDao()->guardar($SistemaDistribucion);
        unset($data);


        /* ANTECEDENTES EMPALME */
        $data = array();
        $data['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
        $data['tipo_empalme'] = (int) $values['tipo_empalme'];
        $data['acometida'] = (int) $values['acometida'];
        $data['tipo_circuito'] = (int) $values['circuito'];
        $data['conexion_circuito'] = (int) $values['conexion'];
        $data['medida'] = (int) $values['medida'];
		$data['voltaje'] = (String) $values['voltaje'];

        $AntecedentesEmpalme = new AntecedentesEmpalme();
        $AntecedentesEmpalme->exchangeArray($data);

        $this->getAntecedentesEmpalmeDao()->guardar($AntecedentesEmpalme);
        unset($data);
       

        /* REGLETA */
        $data = array();
        $data['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
        $data['marca_regleta'] = $values['marca_regleta'];
        $data['modelo_regleta'] = $values['modelo_regleta'];
        $data['numero_terminales_regleta'] = (int) $values['terminales_regleta'];
        $data['puentes_corrientes'] = (int) $values['puente_corriente_regleta'];
        $data['puentes_voltajes'] = (int) $values['puente_voltaje_regleta'];

        $Regleta = new Regletas();
        $Regleta->exchangeArray($data);

        $this->getRegletasDao()->guardar($Regleta);
        unset($data);


        /* ECM */
        $data = array();
        $data['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
        $data['modelo'] = $values['modelo_ecm'];
        $data['marca'] = $values['marca_ecm'];
        $data['serie'] = $values['numero_series_ecm'];
        $data['anio'] = (int) $values['Anio_fabricación_ecm'];
        $data['cantidad_elementos'] = (int) $values['num_elementos_ecm'];

        $data['ip1'] = (int) $values['escala_corriente_1_ecm'];
        $data['ip2'] = (int) $values['escala_corriente_2_ecm'];
        $data['ip3'] = (int) $values['escala_corriente_3_ecm'];

        $data['is_ecm'] = (int) $values['escala_corriente_secundaria_ecm'];

        $data['vp'] = $values['escala_voltaje_1_ecm'];
        $data['vs'] = $values['escala_voltaje_2_ecm'];

        $data['ttcc1'] = (int) $values['razon_ttcc_conectado_1_ecm'];
        $data['ttcc2'] = (int) $values['razon_ttcc_conectado_2_ecm'];

        $data['ttpp1'] = (int) $values['razon_ttpp_conectado_1_ecm'];
        $data['ttpp2'] = (int) $values['razon_ttpp_conectado_2_ecm'];

        $Ecm = new Ecm();
        $Ecm->exchangeArray($data);

        $this->getEcmDao()->guardar($Ecm);
        unset($data);


        /* CALENDARIO */
        $data = array();
        $data['ors_riat_id_ors_riat'] = (int) $values['id_ors_riat'];
        $data['fecha_encontrada_medidor'] = $values['fecha_encontrada_medidor_calendario'];
        $data['fecha_oficial_pais'] = $values['fecha_oficial_pais_calendario'];
        $data['fecha_dejada_medidor'] = $values['fecha_dejada_medidor_calendario'];

        $CalendarioMedidor = new CalendarioMedidor();
        $CalendarioMedidor->exchangeArray($data);

        $this->getCalendarioMedidorDao()->guardar($CalendarioMedidor);
        unset($data);


        /* RAZON ENCONTRADA */
        $data = array();
        $data['ors_riat_id_ors_riat'] = (int) $values['id_ors_riat'];
        $data['razon_tc_calculada'] = $values['razon_tc_calculada'];
        $data['razon_tp_placa'] = $values['razon_tp_placa'];
        $data['constante_facturacion'] = $values['contante_multiplicacion_facturacion_calculada_terreno'];

        $RazonEncontrada = new RazonEncontrada();
        $RazonEncontrada->exchangeArray($data);

        $this->getRazonEncontradaDao()->guardar($RazonEncontrada);
        unset($data);


        /* ORIGEN AFECTA A LA MEDIDA */
        $data = array();
        $data['ors_riat_id_ors_riat'] = (int) $values['id_ors_riat'];
        $data['origen_afectacion'] = (int) $values['origen_afecta_select'];
        $data['comentario'] = $values['origen_afecta_comentario'];

        $OrigenAfectacionAlamedida = new OrigenAfectacionAlamedida();
        $OrigenAfectacionAlamedida->exchangeArray($data);

        $this->getOrigenAfectacionAlamedidaDao()->guardar($OrigenAfectacionAlamedida);
        unset($data);


        /* LECTURA MEDIDOR INSTALADO */
        $data = array();
        $data['ors_riat_id_ors_riat'] = (int) $values['id_ors_riat'];
        $data['tipo_elemento_id_tipo'] = (int) 1;
        $data['kwh'] = $values['lecturamedidorinstalado_kwh'];
        $data['kvar'] = $values['lecturamedidorinstalado_kvar'];
        $data['kw_punta'] = $values['lecturamedidorinstalado_kwpunta'];
        $data['kw_sum'] = $values['lecturamedidorinstalado_kwsum'];

        $LecturaMedidorInstalado = new LecturaMedidorInstalado();
        $LecturaMedidorInstalado->exchangeArray($data);

        $this->getLecturaMedidorInstaladoDao()->guardar($LecturaMedidorInstalado);
        unset($data);
  

        /* CONTACTO AUTORIZA */
        $data = array();
        $data['ors_riat_id_ors_riat'] = (int) $values['id_ors_riat'];
        $data['nombre_contacto'] = $values['contacto_nombre'];
        $data['run'] = $values['contacto_rut'];
        $data['fono'] = $values['contacto_telefono'];
        $data['email'] = $values['contacto_email'];

        $ContactoAutoriza = new ContactoAutoriza();
        $ContactoAutoriza->exchangeArray($data);

        $this->getContactoAutorizaDao()->guardar($ContactoAutoriza);
        unset($data);

 

        /* VIAJES FRUSTRADOS */
        $data1 = array();
        $data1['id_ors_riat'] = (int) $values['id_ors_riat'];
        $data1['cods_viaje_frustrado_idcods_viaje_frustrado'] = (int) $values['select_norealizaservicio_1'];
        $data1['num_visita'] = (int) 1;
        $data1['fecha_visita'] = $values['fecha_visita_1'];
        $data1['hora_visita'] = $values['hora_visita_1'];

        $ViajesFrustrados1 = new ViajesFrustrados();
        $ViajesFrustrados1->exchangeArray($data1);

        $data2 = array();
        $data2['id_ors_riat'] = (int) $values['id_ors_riat'];
        $data2['cods_viaje_frustrado_idcods_viaje_frustrado'] = (int) $values['select_norealizaservicio_2'];
        $data2['num_visita'] = (int) 2;
        $data2['fecha_visita'] = ($values['fecha_visita_2'] != '') ? $values['fecha_visita_2'] : '0000-00-00';
        $data2['hora_visita'] = $values['hora_visita_2'];

        $ViajesFrustrados2 = new ViajesFrustrados();
        $ViajesFrustrados2->exchangeArray($data2);

        $data3 = array();
        $data3['id_ors_riat'] = (int) $values['id_ors_riat'];
        $data3['cods_viaje_frustrado_idcods_viaje_frustrado'] = (int) $values['select_norealizaservicio_3'];
        $data3['num_visita'] = (int) 3;
        $data3['fecha_visita'] = ($values['fecha_visita_3'] != '') ? $values['fecha_visita_3'] : '0000-00-00';
        $data3['hora_visita'] = $values['hora_visita_3'];

        $ViajesFrustrados3 = new ViajesFrustrados();
        $ViajesFrustrados3->exchangeArray($data3);

        /* Eliminar Todo regitro Asociado al ID RIAT */
        $this->getViajesFrustradosDao()->eliminar($values['id_ors_riat']);

        /* Insert Datos en Tabla */
        if ($values['fecha_visita_1'] != '' && $values['hora_visita_1'] != '' && $values['fecha_visita_1'] != '0000-00-00')
            $this->getViajesFrustradosDao()->guardar($ViajesFrustrados1);
        if ($values['fecha_visita_2'] != '' && $values['hora_visita_2'] != '' && $values['fecha_visita_2'] != '0000-00-00')
            $this->getViajesFrustradosDao()->guardar($ViajesFrustrados2);
        if ($values['fecha_visita_3'] != '' && $values['hora_visita_3'] != '' && $values['fecha_visita_3'] != '0000-00-00')
            $this->getViajesFrustradosDao()->guardar($ViajesFrustrados3);
        unset($data1);
        unset($data2);
        unset($data3);


        /* MEDIDORES INSTALADOS */
        $data1 = array();
        $data1['modelos_medidores_id_modelo'] = (int) $values['id_modelo_act'];
        $data1['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
        //$data1['medidor_actual']
        //$data1['fecha_instalacion']
        $data1['numero_serie'] = (int) $values['numero_serie_act'];
        $data1['anio_fabricacion'] = (int) $values['anio_fabricacion_act'];
        $data1['act_reac'] = (int) 1;

        $MedidorInstalacion1 = new MedidorInstalacion();
        $MedidorInstalacion1->exchangeArray($data1);

        $data2 = array();
        $data2['modelos_medidores_id_modelo'] = (int) $values['id_modelo_reac'];
        $data2['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
        //$data1['medidor_actual']
        //$data1['fecha_instalacion']
        $data2['numero_serie'] = (int) $values['numero_serie_reac'];
        $data2['anio_fabricacion'] = (int) $values['anio_fabricacion_reac'];
        $data2['act_reac'] = (int) 2;

        $MedidorInstalacion2 = new MedidorInstalacion();
        $MedidorInstalacion2->exchangeArray($data2);

        /* Eliminar los Registros Asociados al Id instalacion */
        $this->getMedidorInstalacionDao()->eliminar($values['id_instalacion']);
        $this->getMedidorInstalacionDao()->guardar($MedidorInstalacion1);
        $this->getMedidorInstalacionDao()->guardar($MedidorInstalacion2);
        unset($data1);
        unset($data2);


        /* TRANFORMADORES INSTALADOS */
        $this->getTransformadorCorrienteDao()->eliminar($values['id_instalacion']);

        for ($index = 1; $index <= 3; $index++) {

            $data = array();
            $data['instalaciones_id_instalacion'] = (int) $values['id_instalacion'];
            $data['marca'] = $values['marca_tra_' . $index];
            $data['serie'] = $values['serie_tra_' . $index];
            $data['anio'] = (int) $values['anio_tra_' . $index];
            $data['inp'] = (int) $values['rtc_inp_tra_' . $index];
            $data['ins'] = (int) $values['rtc_ins_tra_' . $index];
            $data['num_pasadas'] = (int) $values['np_tra_' . $index];

            $TransformadorCorriente = new TransformadorCorriente();
            $TransformadorCorriente->exchangeArray($data);
            $this->getTransformadorCorrienteDao()->guardar($TransformadorCorriente);
            unset($data);
        }


        /* VALOR TOTAL POTENCIAS */
        $this->getValorTotalPotenciasDao()->eliminar($values['id_ors_riat']);

        for ($index = 1; $index <= 3; $index++) {

            $data = array();
            $data['id_ors_riat'] = (int) $values['id_ors_riat'];
            $data['tipo_registro'] = (int) $index;
            $data['potencia_activa_sec_kw'] = $values['potencia_activa_sec_' . $index];
            $data['potencia_activa_prim_kw'] = $values['potencia_activa_prim_' . $index];
            $data['potencia_reactiva_kvar'] = $values['potencia_reactiva_' . $index];

            $ValorTotalPotencias = new ValorTotalPotencias();

            $ValorTotalPotencias->exchangeArray($data);

            $this->getValorTotalPotenciasDao()->guardar($ValorTotalPotencias);

            unset($data);
        }

        /* MANTENCION REALIZADA */
        $this->getMantencionRealizadaDao()->eliminar($values['id_ors_riat']);

        for ($index = 1; $index <= 10; $index++) {

            $data = array();
            $data['id_ors_riat'] = (int) $values['id_ors_riat'];
            $data['cods_mant_realiz_idcods_mant_realiz'] = (int) $index;
            $data['resultado_mantencion_realizada'] = (int) $values['mantrealizada_' . $index];

            $MantencionRealizada = new MantencionRealizada();

            $MantencionRealizada->exchangeArray($data);

            $this->getMantencionRealizadaDao()->guardar($MantencionRealizada);

            unset($data);
        }


        /* REMPLAZO ELEMENTO EMPALME */
        $this->getRemplazoElementoEmpalmeDao()->eliminar($values['id_ors_riat']);

        for ($index = 1; $index <= 5; $index++) {

            $data = array();
            $data['id_ors_riat'] = (int) $values['id_ors_riat'];
            $data['tipo_elementos_idtipo'] = (int) $index;
            $data['numero_serie'] = $values['numeroserie_' . $index];
            $data['marca'] = $values['marca_' . $index];
            $data['codigo_modelo'] = $values['codigomodelo_' . $index];
            $data['propiedad'] = $values['propiedad_' . $index];

            $RemplazoElementoEmpalme = new RemplazoElementoEmpalme();

            $RemplazoElementoEmpalme->exchangeArray($data);

            $this->getRemplazoElementoEmpalmeDao()->guardar($RemplazoElementoEmpalme);

            unset($data);
        }


        return $this->redirect()->toRoute('riat', array('controller' => 'index', 'action' => 'editar', 'id' => $values["id_ors_riat"]));
        
        
     }



/*
 * 
 * Buscar Codigo de Anormalidad
 * 
 * */

	public function buscarCodigoAnormalidadAction(){
		 $codigo = "";
	                     
	     $codAnor = (String) $this->getRequest()->getPost("codAnor", 0);
		
         if($codAnor!=""){	
	          $codigo = $this->getOrsRiatDao()->obtenerCodigoAnormalidad($codAnor);
		 }
		 		 
	     $view = new ViewModel(array(
	            'resultcodigo' => $codigo,
	       ));
			
	     $view->setTerminal(true);
	
	     return $view;
		
	}

/*
 * FIN 
 * 
 * */



    /*
     * 
     * Buscar Provincias por Ajax
     * 
     */

    public function provinciasAction() {

        $regId = (int) $this->getRequest()->getPost("regId", 0);

        //crear y configurar el elemento provincia
        $provincias = new \Zend\Form\Element\Select('provincia');
        $provincias->setLabel('Provincias');
        $provincias->setAttributes(
                array(
                    'id' => 'provincias',
                    'onchange' => 'javascript:cargarSelectComunas()',
                )
        );

        $provincias->setValueOptions($this->getOrsRiatDao()->obtenerProvincias($regId));

        /*
         * Retornamos a la Vista con un OK
         */
        $view = new ViewModel(array(
            'selectProvincias' => $provincias,
        ));

        $view->setTerminal(true);

        return $view;
    }

    /*
     * 
     * Buscar por Ajax Comunas
     * 
     */

    public function comunasAction() {

        $proId = (int) $this->getRequest()->getPost("proId", 0);

        //crear y configurar el elemento provincia
        $comunas = new \Zend\Form\Element\Select('comuna');
        $comunas->setLabel('Comunas');
        $comunas->setAttributes(
                array(
                //'id' => 'provincias',
                //'onchange' => 'javascript:cargarSelectComunas()',                
                )
        );

        $comunas->setValueOptions($this->getOrsRiatDao()->obtenerComunas($proId));

        /*
         * Retornamos a la Vista con un OK
         */
        $view = new ViewModel(array(
            'selectComunas' => $comunas,
        ));

        $view->setTerminal(true);

        return $view;
    }

    /*
     * 
     * Buscar por Ajax Medidor
     * 
     */

    public function medidorAction() {

        $codigoMedidor = (int) $this->getRequest()->getPost("cod", 0);
        $medidor = $this->getModelosMedidoresDao()->obtenerMedidor($codigoMedidor);

        $data = array();

        foreach ($medidor as $value) {

            $data['Id_modelo'] = $value->getId_modelo();
            $data['Codigo_modelo'] = $value->getCodigo_modelo();
            $data['Marca'] = $value->getMarca();
            $data['Modelo'] = $value->getModelo();
            $data['Voltaje'] = $value->getVoltaje();
            $data['Kp'] = $value->getKp();
            $data['Kq'] = $value->getKq();
            $data['Contante_interna'] = $value->getContante_interna();
            $data['Tecnologia'] = $value->getTecnologia();
            $data['Corriente_nominal'] = $value->getCorriente_nominal();
            $data['Corriente_maxima'] = $value->getCorriente_maxima();
            $data['Tipo_alimentacion'] = $value->getTipo_alimentacion();
            $data['Numero_hilos'] = $value->getNumero_hilos();
            $data['Numero_elementos'] = $value->getNumero_elementos();
            $data['Tipo_energia'] = $value->getTipo_energia();
            $data['Demanda_max'] = $value->getDemanda_max();
            $data['Thr'] = $value->getThr();
            $data['Multifuncion'] = $value->getMultifuncion();
        }

        $view = new JsonModel(array('data' => $data));

        //$view = new ViewModel(array('data' => $data,));
        //$view->setTerminal(true);

        return $view;
    }

    /*
     * 
     * Fotos del Servicio Riat
     * 
     */

    public function fotosAction() {

        /*
         * Retornamos a la Vista con un OK
         */

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('riat', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');
        
        $idRiat = $this->params()->fromRoute('r');
        
        
        $datosAviso = $this->getAvisoDao()->obtenerPorId($idAviso);
        
        $datosRiat = $this->getOrsRiatDao()->obtenerPorId($idRiat);
        
        
        $viewModel = new ViewModel(array(
                                          'datosAviso' => $datosAviso,
                                          'datosRiat' => $datosRiat, 
                                        )
                                      );

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("riat/index/fotos");

        $viewModel->setTerminal(true);

        return $viewModel;
    }

}
