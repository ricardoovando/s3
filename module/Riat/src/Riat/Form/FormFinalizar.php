<?php

namespace Riat\Form;

use Zend\Form\Form;

class FormFinalizar extends Form {

    public function __construct($name = null) {
        parent::__construct('formfinalizar');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_entrega_dig',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Fecha Entrega Digital',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_entrega_fisica',
            'options' => array(
                'label' => '',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
                'placeholder' => 'Fecha Entrega Fisica',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Finalizar Riats',
                'class' => 'btn',
            ),
        ));

    }

}

