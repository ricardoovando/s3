<?php

namespace Riat\Form;

use Zend\Form\Form;

class RegistroRiat extends Form {

    public function __construct($name = null) {
        parent::__construct('formRiat');

        $this->add(array(
            'name' => 'id_ors_riat',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'id_aviso',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));   
         
        $this->add(array(
            'name' => 'id_instalacion',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));  
        
//        $this->add(array(    
//            'name' => 'csrf', 
//            'type' => 'Zend\Form\Element\Csrf', 
//        )); 
        
        
        
        /*
         * 
         * Carga Fotografias  
         *
         */
        
        $this->add(array(
            'name' => 'img_riat[]',
            'type' => 'File',
            'attributes' => array(
                'multiple' => 25,
                'accept' => 'image/*',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Fotos de la Auditoria Max. (25) (Formato JPGE y Bmp. peso menor a 1 MB):  ',
            ),
        ));
        
        
       $this->add(array(
            'name' => 'img_form_riat',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Formulario RIAT (Formato PDF. peso menor a 1 MB):  ',
            ),
        ));
                
        
       $this->add(array(
            'name' => 'img_certificado_lab',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Certificado Laboratorio:',
            ),
          )
        );
       
       
       
       $this->add(array(
            'name' => 'img_devo',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Form. Devolucion:',
            ),
          )
        );
       
         
      //////////////////////////////////////777   

        /* CODIGOS SERVICIOS */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'distribuidora',
            'options' => array(
                'label' => 'Distribuidora : ',
            ),
            'attributes' => array(
                'style' => 'width:250px;',
                'options' => array(
                    0 => 'Seleccione Distribuidora',
                    1 => 'CGE 1',
                    2 => 'CGE 2',
                    3 => 'CGE 3',
                ),
            )
        ));
       
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'distribuidora',
            'options' => array(
                'label' => 'Distribuidora : ',
            ),
            'attributes' => array(
                'readonly' => 'readonly',
                'id' => 'distribuidora',
                'style' => 'width:250px;',
                )
        ));


        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico',
            'options' => array(
                'label' => 'Tecnico 1 : ',
            ),
            'attributes' => array(
                'style' => 'width:300px;margin:auto;',
                'id' => 'persona_tecnico',
                //'required' => 'required', 
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico_2',
            'options' => array(
                'label' => 'Tecnico 2 : ',
            ),
            'attributes' => array(
                'style' => 'width:300px;margin:auto;',
                'id' => 'persona_tecnico_2',
                //'required' => 'required', 
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_aviso',
            'options' => array(
                'label' => 'N° Aviso / OT : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                 'readonly' => 'readonly',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_form_riat',
            'options' => array(
                'label' => 'N° Form Riat : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                 'required' => 'required', 
                )
        ));

        /* TERMINO */

        /* DATOS CLIENTES */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombrecliente',
            'options' => array(
                'label' => 'Nombre Cliente : ',
            ),
             'attributes' => array(

                'required' => 'required',
                 'style' => 'width:350px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'direccion',
            'options' => array(
                'label' => 'Dirección : ',
            ),
              'attributes' => array(
                'style' => 'width:300px;',
                'required' => 'required', 
            ),
        ));

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region',
            'options' => array(
                'label' => 'Region : ',
            ),
            'attributes' => array(
                'id' => 'regiones',
                'onchange' => 'javascript:cargarSelectProvincias()',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'provincia',
            'options' => array(
                'label' => 'Provincia : ',
            ),
            'attributes' => array(
                'id' => 'provincias',
                'onchange' => 'javascript:cargarSelectComunas()',   
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'comuna',
            'options' => array(
                'label' => 'Comuna : ',
            ),
            'attributes' => array(
                'id' => 'comunas',
            )
        ));
 
        
 /*++++++++++++++++++++++++++++++++++++++++++++++*/
 
 // ANORMALIDADES Y CNR 

 $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad1',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Falla Propia',
                    2 => 'Intervencion Terceros',
                    3 => 'Otro Factor',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad1',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));		
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad1',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                ),
            )
        ));
	
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad2',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Falla Propia',
                    2 => 'Intervencion Terceros',
                    3 => 'Otro Factor',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad2',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));		
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad2',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                ),
            )
        ));	
  
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad3',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Falla Propia',
                    2 => 'Intervencion Terceros',
                    3 => 'Otro Factor',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad3',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));		
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad3',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                ),
            )
        ));		
	
	
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad4',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Falla Propia',
                    2 => 'Intervencion Terceros',
                    3 => 'Otro Factor',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad4',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));		
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad4',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                ),
            )
        ));	
	
	
		
  /*
   * 
   * ++++++++++++++++++++++++++++++++++++++++++++++
   * 
   * 
   * */
 
 

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_instalacion',
            'options' => array(
                'label' => 'N° Servicio o Cliente : ',
            ),
            'attributes' => array(
                'style' => 'width:100px;',
                'readonly' => 'readonly',
            ),
        ));


        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tarifa_terreno',
            'options' => array(
                'label' => 'Tarifa : ',
            ),
            'attributes' => array(
                'id' => 'tarifa_terreno',
                'style' => 'width:85px;',
                'options' => array(
                   0 => 'Tarifa en Terreno',
                  'BT1' => 'BT1',
                  'BT2' => 'BT2',
                  'BT3' => 'BT3',
                  'BT4.1' => 'BT4.1',
                  'BT4.2' => 'BT4.2',
                  'BT4.3' => 'BT4.3',
                  'AT2' => 'AT2', 
                  'AT3' => 'AT3',   
                  'AT4.1' => 'AT4.1',
                  'AT4.2' => 'AT4.2',
                  'AT4.3' => 'AT4.3',
                ),
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tarifa',
            'options' => array(
                'label' => 'Tarifa Cliente : ',
            ),
            'attributes' => array(
               'id' => 'tarifa', 
               'disabled' => 'disabled',
               'style' => 'width:85px;border: 2px solid #a9302a;',
             )
          ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'unidad_lectura',
            'options' => array(
                'label' => 'Ruta Lectura : ',
            ),
            'attributes' => array(

             )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_aviso',
            'options' => array(
                'label' => 'Tipo Aviso : ',
                'empty_option' => 'Seleccione un tipo',
            ),
            'attributes' => array(
                'options' => array(
                    1 => 'Inspeccion',
                    2 => 'Auditoria Dir.',
                    3 => 'Auditoria Ind.',
                    4 => 'Auditoria SemiDir.',
                    5 => 'Viaje Frustrado',
                    6 => 'Reprogramaciòn',
                    7 => 'Conexiones',
                    8 => 'Otros',
                ),
             )
          ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'proviene_gavis',
            'options' => array(
                'label' => 'Proviene de Gavis : ',
            ),
        ));

        /* TERMINO */


        /* VIAJES FRUSTRADOS */
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_visita_1',
             'attributes' => array(
                 'style' => 'width:100px;',
                )
             ));

       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_visita_1',
            'attributes' => array(
                 'style' => 'width:100px;',
                )
             ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_visita_2',
             'attributes' => array(
                 'style' => 'width:100px;',
                )
            ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_visita_2',
             'attributes' => array(
                 'style' => 'width:100px;',
                )
             ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_visita_3',
             'attributes' => array(
                 'style' => 'width:100px;',
                )
             ));

       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_visita_3',
            'attributes' => array(
                 'style' => 'width:100px;',
                )
            ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_1',
            'options' => array(
                'label' => 'No realiza Servicio 1 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
              )
           ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_2',
            'options' => array(
                'label' => 'No realiza Servicio 2 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
              )
           ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_norealizaservicio_3',
            'options' => array(
                'label' => 'No realiza Servicio 3 : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme no existe',
                    2 => 'Domicilio desocupado o Sitio eriazo',
                    3 => 'Cliente no autoriza',
                    4 => 'Sin adulto responsable',
                    5 => 'Empalme sin energía',
                    6 => 'No ubicado',
                    7 => 'Casa Cerrada',
                    8 => 'Condición insegura',
                ),
              )
           ));



        /* ANTECEDENTES EMPALME */
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_empalme',
            'options' => array(
                'label' => 'Tipo empalme : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Residencial',
                    2 => 'Comercial',
                    3 => 'Al Público',
                    4 => 'Industrial',
                    5 => 'Agrícola',
                    6 => 'Otro',
                ),
              )
           ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'acometida',
            'options' => array(
                'label' => 'Acometida : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Empalme Aéreo',
                    2 => 'Emp. Subterraneo',
                    3 => 'Celda Medida',
                ),
              )
           ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'circuito',
            'options' => array(
                'label' => 'Circuito : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Monofásico',
                    2 => 'Bifásico',
                    3 => 'Trifásico',
                ),
              )
           ));



        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'conexion',
            'options' => array(
                'label' => 'Conexión : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Conexión BT',
                    2 => 'Conexión MT',
                ),
              )
           ));



          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'voltaje',
            'options' => array(
                'label' => 'Voltaje : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    '23 KV' => '23 KV',
                    '15 KV' => '15 KV',
                    '13.2 KV' => '13.2 KV',
                    '12 KV' => '12 KV',
                    '380 V' => '380 V',
                    '280 V' => '280 V',
                    '110 V' => '110 V',
                ),
              )
           ));
		   

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'medida',
            'options' => array(
                'label' => 'Medida : ',
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione un tipo',
                    1 => 'Directa',
                    2 => 'Indirecta TTCC',
                    3 => 'Indirecta ECM',
                ),
              )
           ));

        /* TERMINO */


        /* FECHA ATENCION */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_atencion_servicio',
            'options' => array(
                'label' => 'Fecha Atención Servicio : ',
            ),
            'attributes' => array(
                 'style' => 'width:80px;',
                )
        ));

        /* TERMINO */

        /* OTROS DATOS */

        $this->add(array('type' => 'Zend\Form\Element\Text',
            'name' => 'placa_poste_empalme',
            'options' => array(
                'label' => 'Placa poste empalme : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'capacidad_automatico',
            'options' => array(
                'label' => 'Capacidad Automático (A) : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_ssee',
            'options' => array(
                'label' => 'Potencia SSEE : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_ssee',
            'options' => array(
                'label' => 'N° SSEE : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'pot_contratada',
            'options' => array(
                'label' => 'Pot Contratada (KW) : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'alimentador',
            'options' => array(
                'label' => 'Alimentador : ',
            ),
            'attributes' => array(
                 'style' => 'width:100px;',
                )
        ));



        /* MEDIDOR ACTIVO */
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'id_modelo_act',
            'attributes' => array(
                'id' => 'id_modelo_act'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_medidor_act',
            'attributes' => array(
                'id' => 'codigo_medidor_act',
                'style' => 'width:70px;',
              )
            ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_serie_act',
            'attributes' => array(
                'id' => 'numero_serie_act',
                'style' => 'width:100px;',
              )
           ));

                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_act',
            'attributes' => array('disabled' => 'disabled','id' => 'marca_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_act',
            'attributes' => array('disabled' => 'disabled','id' => 'modelo_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_fabricacion_act',
            'attributes' => array(
                'id' => 'anio_fabricacion_act',
                'style' => 'width:100px;',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeros_elementos_act',
            'attributes' => array('disabled' => 'disabled','id' => 'numeros_elementos_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_act',
            'attributes' => array('disabled' => 'disabled','id' => 'voltaje_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corriente_act',
            'attributes' => array('disabled' => 'disabled','id' => 'corriente_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_kh_act',
            'attributes' => array('disabled' => 'disabled','id' => 'constante_kh_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_interna_act',
            'attributes' => array('disabled' => 'disabled','id' => 'constante_interna_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tecnologia_act',
            'attributes' => array('disabled' => 'disabled','id' => 'tecnologia_act','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'reg_dda_maxima_act',
            'attributes' => array('disabled' => 'disabled','id' => 'reg_dda_maxima_act','style' => 'width:100px;',)
        ));
                
                
                
        /* MEDIDOR REACTIVO */
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'id_modelo_reac',
            'attributes' => array('id' => 'id_modelo_reac'),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_medidor_reac',
            'attributes' => array(
                'id' => 'codigo_medidor_reac',
                'style' => 'width:70px;',
               ),
            ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_serie_reac',
            'attributes' => array(
                               'id' => 'numero_serie_reac',
                               'style' => 'width:100px;',
                               ),
                             ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'marca_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'modelo_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_fabricacion_reac',
            'attributes' => array(
                'id' => 'anio_fabricacion_reac',
                'style' => 'width:100px;',
                ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeros_elementos_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'numeros_elementos_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'voltaje_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corriente_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'corriente_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_kh_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'constante_kh_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'constante_interna_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'constante_interna_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tecnologia_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'tecnologia_reac','style' => 'width:100px;',)
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'reg_dda_maxima_reac',
            'attributes' => array('disabled' => 'disabled','id' => 'reg_dda_maxima_reac','style' => 'width:100px;',)
        ));

        /* TERMINO */


        /* EQUIPOS DE MEDIDA */
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigo_modelo_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_series_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'Anio_fabricación_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_elementos_ecm',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_voltaje_1_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'VP',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_voltaje_2_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'VS',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_1_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'Ip1',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_2_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'Ip2',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_3_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'Ip3',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'escala_corriente_secundaria_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'Is',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttcc_conectado_1_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => '',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttcc_conectado_2_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => '',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttpp_conectado_1_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => '',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_ttpp_conectado_2_ecm',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => '',
            ),
        ));

        /* TERMINO */
        
        
        /* TRANFORMADORES */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'serie_tra_1',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_tra_1',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_inp_tra_1',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INP',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_ins_tra_1',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INS',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'np_tra_1',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_tra_1',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'serie_tra_2',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_tra_2',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_inp_tra_2',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INP',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_ins_tra_2',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INS',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'np_tra_2',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_tra_2',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'serie_tra_3',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'anio_tra_3',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_inp_tra_3',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INP',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'rtc_ins_tra_3',
            'attributes' => array(
                'style' => 'width:30px;',
                'placeholder' => 'INS',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'np_tra_3',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_tra_3',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
        ));
        
        /*FIN*/

       
        /* RELOJ REGLETA TELEMEDIDA CALENDARIO */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tiene_reloj',
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    'SI' => 'SI',
                    'NO' => 'NO',
                  ),
                 'style' => 'width:90px;',
                ),
           ));
		
		$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tiene_telemedida',
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    'SI' => 'SI',
                    'NO' => 'NO',
                  ),
                 'style' => 'width:90px;',
                )
          ));

          $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_regleta',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
          ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'modelo_regleta',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
          )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'terminales_regleta',
            'attributes' => array(
                'style' => 'width:100px;',
            ),
          )); 
        
      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'puente_corriente_regleta',
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'Abierto',
                    2 => 'Cerrado',
                  ),
                 'style' => 'width:90px;',
                )
              ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'puente_voltaje_regleta',
            'attributes' => array(
                'options' => array(
                    0 => 'select',
                    1 => 'Abierto',
                    2 => 'Cerrado',
                  ),
                 'style' => 'width:90px;',
                )
              ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_encontrada_medidor_calendario',
             'attributes' => array(
                 'style' => 'width:120px;',
                )
             )); 
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_oficial_pais_calendario',
            'attributes' => array(
              'style' => 'width:120px;',
            )
          ));
                        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_dejada_medidor_calendario',
            'attributes' => array(
              'style' => 'width:120px;',
            )
          ));
        
        /*TERMINADO*/
        
        
        /* MEDICIONES INSTANTANEAS ANALIZADOR DE REDES ELECTRICAS */
        
        /* DIGITACION SUSPENDIDA */
        
        /*
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ffv_4',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_ftierrav_4',
        )); 
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'voltaje_fnv_4',
        )); 

       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_voltaje_4',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corrientes_4',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'angulo_corrientes_4',
        )); 
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_me_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_me_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_me_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cos_fi_me_4',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_4',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_me_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_me_2',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_me_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_me_4',
        )); 
        */
        
        /*TERMINO*/

        
        /* VALOR TOTAL */
   
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_2',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_sec_3',
        ));   
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_2',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_activa_prim_3',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_1',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_2',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'potencia_reactiva_3',
        )); 
       
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'cos_fi_1',
//        )); 
//        
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'cos_fi_2',
//        ));         
// 
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'cos_fi_3',
//        )); 

        /*TERMINO*/

        
        /* CORRIENTES SECUNDARIAS */
        /* DIGITACION SUSPENDIDA */
        /*
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase1_rezonttcc',
        )); 
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase2_rezonttcc',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase3_rezonttcc',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase4_terminales',
        ));         
 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase4_ladofuente',
        ));  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase4_corr_prim',
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'corr_seg_fase4_rezonttcc',
        )); 
        
        */
        /*TERMINO*/
        
        /*CORRESPONDENCIA DE TENSION Y CORRIENTE EN MEDIDOR*/
        /*DIGITACION SUSPENDIDA*/

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'correspondencia_elemento_1',
//            'options' => array(
//                
//            ),
//            'attributes' => array(
//                'options' => array(
//                    0 => 'select',
//                    1 => 'SI',
//                    2 => 'NO',
//                  ),
//                 'style' => 'width:122px;',
//                )
//              ));
//        
//          $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'correspondencia_elemento_2',
//            'options' => array(
//                
//            ),
//            'attributes' => array(
//                'options' => array(
//                    0 => 'select',
//                    1 => 'SI',
//                    2 => 'NO',
//                  ),
//                 'style' => 'width:122px;',
//                )
//              ));
//          
//          $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'correspondencia_elemento_3',
//            'options' => array(
//                
//            ),
//            'attributes' => array(
//                'options' => array(
//                    0 => 'select',
//                    1 => 'SI',
//                    2 => 'NO',
//                  ),
//                 'style' => 'width:122px;',
//                )
//              ));
//          
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'correspondencia_secuencia',
//            'options' => array(
//                
//            ),
//            'attributes' => array(
//                'options' => array(
//                    0 => 'select',
//                    1 => 'Positiva',
//                    2 => 'Negativa',
//                  ),
//                 'style' => 'width:122px;',
//                )
//              ));
//         
//         
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'correspondencia_flujo_potencia',
//            'options' => array(
//                
//            ),
//            'attributes' => array(
//                'options' => array(
//                    0 => 'select',
//                    1 => 'Directo',
//                    2 => 'Inverso',
//                  ),
//                 'style' => 'width:122px;',
//                )
//              ));

        /*TERMINO*/
        
        
        /*POTENCIA MEDIDOR*/
        /*DIGITACION SUSPENDIDA*/
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'num_revol_pulsos_med_activo',
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'num_revol_pulsos_med_reactivo',
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'tiempo_segundos_med_activo',
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'tiempo_segundos_med_reactivo',
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'potencia_calculada_activo',
//        ));
//
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Text',
//            'name' => 'potencia_calculada_reactivo',
//        ));
        
        
        /*Razon Encontrada*/

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_tc_calculada',
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'razon_tp_placa',
        ));
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contante_multiplicacion_facturacion_calculada',
            'attributes' => array(
               'style' => 'border: 2px solid #a9302a;',
               'readonly' => 'readonly',
               'id' => 'contante_multiplicacion_facturacion_calculada',
             )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contante_multiplicacion_facturacion_calculada_terreno',
            'attributes' => array(
               'id' => 'contante_multiplicacion_facturacion_calculada_terreno',
             )
        ));
        
		
		
		/*Comportamiento de la Carga*/
				
		/*$this->add(array(
        'type' => 'Zend\Form\Element\Radio',
        'name' => 'comportamiento_carga',
        'required' => FALSE,
        'allow_empty ' => TRUE,
        'options' => array(
            'value_options' => array(
                '0' => '    No Aplica',
                '1' => '    Estable',
                '2' => '   Desequilibrado',
                '3' => '   Inestable o Variable',
                '4' => '   Consumo Cero'
                ),
            ),
        ));*/

         $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'comportamiento_carga',
                    'options' => array(
                        'label' => 'Comportamiento Carga : ',
                    ),
                    'attributes' => array(
                        'style' => 'width:150px;',
                        'options' => array(
                           '0' => '    No Aplica',
                           '1' => '    Estable',
                           '2' => '   Desequilibrado',
                           '3' => '   Inestable o Variable',
                           '4' => '   Consumo Cero'
                        ),
                    )
                ));
				
        /* TERMINO */
        
        
        /* Normalizacion o Mantención Realizada */
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_1',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_2',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                 
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_3',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
                  
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_4',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_5',
            'options' => array(
               
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
          
                 
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_6',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                
                
           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_7',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_8',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1 => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                 
          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mantrealizada_9',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => '',
                    1  => 'SI',
                    2 => 'NO',
                  ),
                 'style' => 'width:50px;',
                )
              ));
                  
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'mantrealizada_10',
            'attributes' => array(
                 'style' => 'width:36px;',
                )
        ));  

        /*TERMINO*/
        
        /*ORIGEN AFECTA A LA MEDIDA*/       
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_afecta_select',
            'options' => array(
                
            ),
            'attributes' => array(
                'options' => array(
                    0 => 'Seleccione Tipo',
                    1 => 'Falta Propia Medidor',
                    2 => 'Intervención Terceros',
                    3 => 'Otro Factor',
                  ),
                 'style' => 'width:190px;',
                )
              ));
                     
        
        $this->add(array( 
            'name' => 'origen_afecta_comentario', 
            'type' => 'Zend\Form\Element\Textarea', 
            'attributes' => array( 
                 'rows'=> '5',
                 'cols' => '300',
                 'style' => 'width:578px;',
                ), 
              'options' => array( 
                ), 
            ));        
        /*TERMINO*/
        
        
        
        /* REMPLAZO ELEMENTO EMPALME */
        
        
        /*Cambio Medidor*/
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroserie_1',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_1',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigomodelo_1',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'propiedad_1',
        ));
		
		
        /*Medidor Provisorio*/ 
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroserie_2',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_2',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigomodelo_2',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'propiedad_2',
        ));
        
		
		
        /*Cambio Limitador ITE*/
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroserie_3',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_3',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigomodelo_3',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'propiedad_3',
        ));
        /*Cambio TTCC*/
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroserie_4',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_4',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigomodelo_4',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'propiedad_4',
        ));
		
		
        /*Cambio Regleta o Block o Block de prueba*/
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numeroserie_5',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'marca_5',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'codigomodelo_5',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'propiedad_5',
        ));


        /* TERMINO */
        
        
        /* LECTURA MEDIDOR INSTALADO */

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwh',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kvar',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwpunta',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'lecturamedidorinstalado_kwsum',
        ));

        /* TERMINO */
        
        
        /*QUIEN AUTORIZA CLIENTE*/
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contacto_nombre',
            'attributes' => array( 
                   'style' => 'width:150px;',
                ),
        ));            
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contacto_rut',
            'attributes' => array( 
                   'style' => 'width:150px;',
                ),
        ));
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contacto_telefono',
            'attributes' => array( 
                   'style' => 'width:150px;',
                ),
        )); 
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contacto_email',
            'attributes' => array( 
                   'style' => 'width:150px;',
                ),
        ));
        
        /*TERMINO*/

        /*QUIEN DIGITA*/
        
            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'digita_nombres',
                'attributes' => array( 
                   'disabled' => "disabled",
                   'style' => 'width:350px;',
                ),
            ));            

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'digita_apellidos',
                'attributes' => array( 
                   'disabled' => "disabled",
                   'style' => 'width:350px;',
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'digita_run',
                'attributes' => array( 
                   'disabled' => "disabled",
                   'style' => 'width:150px;',
                ),
            )); 

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'digita_email',
                'attributes' => array( 
                   'disabled' => "disabled",
                   'style' => 'width:150px;', 
                ),
            ));
        
        /*++++*/
        
        /*QUIEN EJECUTA*/
        
            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'tecnico_nombres',
                'attributes' => array( 
                   'disabled' => "disabled",
                    'style' => 'width:350px;',
                ),
            ));            

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'tecnico_apellidos',
                'attributes' => array( 
                   'disabled' => "disabled",
                    'style' => 'width:350px;',
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'tecnico_run',
                'attributes' => array( 
                   'disabled' => "disabled",
                    'style' => 'width:150px;',
                ),
            )); 

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'tecnico_email',
                'attributes' => array( 
                   'disabled' => "disabled",
                   'style' => 'width:150px;',
                ),
            ));
        
        
        /*TERMINO*/

        /*BONOTES DE ENVIO DE DATOS*/    
        $this->add(array(
            'name' => 'send',   
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'onclick' => "javascript:return salida()",
                'class' => 'btn btn-primary btn-lg btn-block',
                'style' => 'margin-left:280px;margin-top:30px;width:610px;height:50px;',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'send2',   
            'attributes' => array(
                'type' => 'submit',
                'onclick' => "javascript:return salida()",
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'comprometo',   
            'attributes' => array(
                'type' => 'submit',
                //'ignore'   => true,
                'value' => 'Comprometido',
                'onclick' => "javascript:return salida()",
                'class' => 'btn btn-success btn-lg btn-block',
                'style' => 'margin-left:280px;margin-top:30px;width:610px;height:50px;',
            ),
        )); 
        
        
    }

}

