<?php

namespace Riat\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\I18n\Validator\Alnum;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;

class RegistroRiatValidator extends InputFilter {

    protected $opcionesAlnum = array(
        'allowWhiteSpace' => false,
        'messages' => array(
            'notAlnum' => "El valor no es alfanúmerico",
        )
    );
    protected $opcionesStringLength = array(
        'min' => 4,
        'max' => 11,
        'messages' => array(
            StringLength::TOO_SHORT => "El campo debe tener tener al menos 4 caracteres",
            StringLength::TOO_LONG => "El campo debe tener un máximo de 11 caracteres",
        )
    );

    public function __construct() {

        $this->add(
                array(
                    'name' => 'region',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StringTrim'),
                        array('name' => 'StripTags'),
                        array('name' => 'StripNewlines'),
                        array('name' => 'HtmlEntities'),
                    ),
                )
        );

        $this->add(
                array(
                    'name' => 'provincia',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StringTrim'),
                        array('name' => 'StripTags'),
                        array('name' => 'StripNewlines'),
                        array('name' => 'HtmlEntities'),
                    ),
                )
        );

        $this->add(
                array(
                    'name' => 'comuna',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StringTrim'),
                        array('name' => 'StripTags'),
                        array('name' => 'StripNewlines'),
                        array('name' => 'HtmlEntities'),
                    ),
                )
        );
    }

}

