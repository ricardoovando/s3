<?php

namespace Riat\Form;

use Zend\Form\Form;

class SelectEstado extends Form {

    public function __construct($name = null) {
        parent::__construct('selectEstados');

        $this->setAttribute('method', 'post');
                
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'estado_ors_riat',
            'options' => array(
                'label' => '',
                'empty_option' => 'Estados Riat',
            ),
            'attributes' => array(
                'style' => 'margin:auto;',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'B',
                'class' => 'btn',
                'style' => 'height:30px;',
            ),
        ));

    }

}

