<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Riat\Model\Entity\CalendarioMedidor;

class CalendarioMedidorDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('ors_riat_id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(CalendarioMedidor $CalendarioMedidor) {

        try {
            
            $data = array(
                    'ors_riat_id_ors_riat' => $CalendarioMedidor->getOrs_riat_id_ors_riat(),
                    'fecha_encontrada_medidor' => $CalendarioMedidor->getFecha_encontrada_medidor(),
                    'fecha_oficial_pais' => $CalendarioMedidor->getFecha_oficial_pais(),
                    'fecha_dejada_medidor' => $CalendarioMedidor->getFecha_dejada_medidor(),
                    );

            $id = (int) $CalendarioMedidor->getOrs_riat_id_ors_riat();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('ors_riat_id_ors_riat' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
}
