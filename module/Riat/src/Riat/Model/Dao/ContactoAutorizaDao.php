<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Riat\Model\Entity\ContactoAutoriza;

class ContactoAutorizaDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('ors_riat_id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(ContactoAutoriza $ContactoAutoriza) {

        try {
            
            $data = array(
                     'ors_riat_id_ors_riat' => $ContactoAutoriza->getOrs_riat_id_ors_riat(),
                     'nombre_contacto' => $ContactoAutoriza->getNombre_contacto(),
                     'run' => $ContactoAutoriza->getRun(),
                     'fono' => $ContactoAutoriza->getFono(),
                     'email' => $ContactoAutoriza->getEmail(),
                    );

            $id = (int) $ContactoAutoriza->getOrs_riat_id_ors_riat();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('ors_riat_id_ors_riat' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
}
