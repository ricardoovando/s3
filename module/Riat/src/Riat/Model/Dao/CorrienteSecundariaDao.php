<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\CorrienteSecundaria;

class CorrienteSecundariaDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }
    
    
    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
        
    }
    

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->order(array('fase'));
        
        $select->where(array('id_ors_riat' => $idriat));
        
        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(CorrienteSecundaria $CorrienteSecundaria) {

        try {

            $data = array(
                          'id_ors_riat' => $CorrienteSecundaria->getId_ors_riat(),
                          'fase' => $CorrienteSecundaria->getFase(),
                          'acometida_meditor_terminal' => $CorrienteSecundaria->getAcometida_meditor_terminal(),
                          'medidor_regleta_lado_fuente' => $CorrienteSecundaria->getMedidor_regleta_lado_fuente(),
                          'corriente_primaria_ttcc' => $CorrienteSecundaria->getCorriente_primaria_ttcc(),
                          'razon_ttcc_calculada' => $CorrienteSecundaria->getRazon_ttcc_calculada(),
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id));
    }



}
