<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Riat\Model\Entity\Diagrama;

class DiagramaDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('ors_riat_id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(Diagrama $Diagrama) {

        try {
            
            $data = array(
                           'ors_riat_id_ors_riat' => $Diagrama->getOrs_riat_id_ors_riat(),
                           'elemento_v1' => $Diagrama->getElemento_v1(),
                           'elemento_v2' => $Diagrama->getElemento_v2(),
                           'elemento_v3' => $Diagrama->getElemento_v3(),
                           'secuencia' => $Diagrama->getSecuencia(),
                           'flujo_potencia' => $Diagrama->getFlujo_potencia(),
                         );

            $id = (int) $Diagrama->getOrs_riat_id_ors_riat();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('ors_riat_id_ors_riat' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
}
