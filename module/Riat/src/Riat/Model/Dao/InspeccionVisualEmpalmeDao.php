<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Riat\Model\Entity\InspeccionVisualEmpalme;

class InspeccionVisualEmpalmeDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('ors_riat_id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
      }
    
    public function guardar(InspeccionVisualEmpalme $InspeccionVisualEmpalme) {

        try {
            
            $data = array(
                            'ors_riat_id_ors_riat' => $InspeccionVisualEmpalme->getOrs_riat_id_ors_riat() ,
                            'acometida_empalme' => $InspeccionVisualEmpalme->getAcometida_empalme(),
                            'limitador_potencia' => $InspeccionVisualEmpalme->getLimitador_potencia(),
                            'medidor' => $InspeccionVisualEmpalme->getMedidor(),
                            'alambrado_medidor_regleta' => $InspeccionVisualEmpalme->getAlambrado_medidor_regleta(),
                            'estado_regleta_block' => $InspeccionVisualEmpalme->getEstado_regleta_block(),
                            'caja_medidor' => $InspeccionVisualEmpalme->getCaja_medidor(),
                            'caja_ttcc' => $InspeccionVisualEmpalme->getCaja_ttcc(),
                            'estado_equipo_compacto' => $InspeccionVisualEmpalme->getEstado_equipo_compacto(),
                            'estado_interruptor_reloj' => $InspeccionVisualEmpalme->getEstado_interruptor_reloj(),
                            'conexion_tierra_proteccion' => $InspeccionVisualEmpalme->getConexion_tierra_proteccion(),
                       );

            $id = (int) $InspeccionVisualEmpalme->getOrs_riat_id_ors_riat();

            if ($id == 0) {
                  $this->tableGateway->insert($data);
            } else {
                if ($this->obtenerPorId($id)) {
                    $this->tableGateway->update($data, array('ors_riat_id_ors_riat' => $id));
                } else {
                    $this->tableGateway->insert($data);
                }
            }
        
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
}
