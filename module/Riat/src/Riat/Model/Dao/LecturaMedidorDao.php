<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\LecturaMedidor;

class LecturaMedidorDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }
    
    
    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
        
    }
    

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id_ors_riat' => $idriat));
        
        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(LecturaMedidor $LecturaMedidor) {

        try {

            $data = array(
                           'id_ors_riat' => $LecturaMedidor->getId_ors_riat(),
                           'lecturas_encontradas_dejadas' => $LecturaMedidor->getLecturas_encontradas_dejadas(),
                           'energia_activa_kwh' => $LecturaMedidor->getEnergia_activa_kwh(),
                           'energia_reactiva_kvarh' => $LecturaMedidor->getEnergia_reactiva_kvarh(),
                           'demanda_max_suministrada_leida_kw' => $LecturaMedidor->getDemanda_max_suministrada_leida_kw(),
                           'fecha_hora_dda_max_sum_leida' => $LecturaMedidor->getFecha_hora_dda_max_sum_leida(),
                           'demanda_max_suministrada_acumulada_kw' => $LecturaMedidor->getDemanda_max_suministrada_acumulada_kw(),
                           'demanda_max_horas_punta_leida_kw' => $LecturaMedidor->getDemanda_max_horas_punta_leida_kw(),
                           'fecha_hora_dda_max_horas_punta_leida' => $LecturaMedidor->getFecha_hora_dda_max_horas_punta_leida(), 
                           'demanda_max_horas_punta_acumulada_kw' => $LecturaMedidor->getDemanda_max_horas_punta_acumulada_kw(),
                           'numero_reset_medidor' => $LecturaMedidor->getNumero_reset_medidor(),
                           'fecha_hora_ultimo_reset' => $LecturaMedidor->getFecha_hora_ultimo_reset(),
                          );
            
            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id));
    }

}
