<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\MedicionesInstantaneas;

class MedicionesInstantaneasDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }
    
    
    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
        
    }
    

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->order(array('fase'));
        
        $select->where(array('id_ors_riat' => $idriat));
        
        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
        
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(MedicionesInstantaneas $MedicionesInstantaneas) {

        try {

            $data = array(
                
                           'id_ors_riat' => $MedicionesInstantaneas->getId_ors_riat(),
                           'fase' => $MedicionesInstantaneas->getFase(),
                           'voltaje_ff_v' => $MedicionesInstantaneas->getVoltaje_ff_v(),
                           'voltaje_f_tierra_v' => $MedicionesInstantaneas->getVoltaje_f_tierra_v(),
                           'voltaje_f_n_v' => $MedicionesInstantaneas->getVoltaje_f_n_v(),
                           'angulo_voltaje' => $MedicionesInstantaneas->getAngulo_voltaje(),
                           'corrientes_a' => $MedicionesInstantaneas->getCorrientes_a(),
                           'angulos_corrientes' => $MedicionesInstantaneas->getAngulos_corrientes(),
                           'cos_fi' => $MedicionesInstantaneas->getCos_fi(),
                           'potencia_activa_kw' => $MedicionesInstantaneas->getPotencia_activa_kw(),
                           'potencia_reactiva_kvar' => $MedicionesInstantaneas->getPotencia_reactiva_kvar(),
                
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id));
    }



}
