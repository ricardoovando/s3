<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\RemplazoElementoEmpalme;

class RemplazoElementoEmpalmeDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }
    
    
    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
        
    }
    

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->order(array('tipo_elementos_idtipo'));
        
        $select->where(array('id_ors_riat' => $idriat));
        
        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(RemplazoElementoEmpalme $RemplazoElementoEmpalme) {

        try {

            $data = array(
                        'id_ors_riat' => $RemplazoElementoEmpalme->getId_ors_riat(),
                        'tipo_elementos_idtipo' => $RemplazoElementoEmpalme->getTipo_elementos_idtipo(),
                        'numero_serie' =>  $RemplazoElementoEmpalme->getNumero_serie(),
                        'marca' => $RemplazoElementoEmpalme->getMarca(), 
                        'codigo_modelo' => $RemplazoElementoEmpalme->getCodigo_modelo(), 
                        'propiedad' =>  $RemplazoElementoEmpalme->getPropiedad(),
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id));
    }



}
