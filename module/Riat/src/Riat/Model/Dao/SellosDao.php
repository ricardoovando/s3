<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\Sellos;

class SellosDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }

    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
    }

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id_ors_riat' => $idriat));

        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(Sellos $Sellos) {

        try {

            $data = array(
                        'id_ors_riat' => $Sellos->getId_ors_riat(),
                        'encontrado_dejado' => $Sellos->getEncontrado_dejado(),
                        'cupula_medidor' => $Sellos->getCupula_medidor(),
                        'block_medidor' => $Sellos->getBlock_medidor(),
                        'reset_medidor' => $Sellos->getReset_medidor(),
                        'bateria_medidor' => $Sellos->getBateria_medidor(),
                        'optico_medidor' => $Sellos->getOptico_medidor(),
                        'regleta_prueba' => $Sellos->getRegleta_prueba(),
                        'int_horario' => $Sellos->getInt_horario(),
                        'caja_medidor' => $Sellos->getCaja_medidor(),
                        'ttcc' => $Sellos->getTtcc(),
                        'caja_ttcc' => $Sellos->getCaja_ttcc(),
                        'caja_ecm' => $Sellos->getCaja_ecm(),
                        );

          $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id ));
    }
    

}
