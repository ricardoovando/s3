<?php

namespace Riat\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Riat\Model\Entity\ValorTotalPotencias;

class ValorTotalPotenciasDao {

    protected $tableGateway;
    protected $login;
    
    public function __construct(TableGateway $tableGateway , $login) {
        $this->tableGateway = $tableGateway;
        $this->login = $login->getIdentity();
    }
    
    
    public function obtenerTodos() {

//        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
//
//        $select = $this->tableGateway->getSql()->select();
//        
//        $dbAdapter = $this->tableGateway->getAdapter();
//        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//        $paginator = new Paginator($adapter);
//        return $paginator;
        
    }
    

    public function obtenerPorIdRiat($idriat) {

        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;

        $idriat = (int) $idriat;

        $select = $this->tableGateway->getSql()->select();
        $select->order(array('tipo_registro'));
        
        $select->where(array('id_ors_riat' => $idriat));
        
        $rowSet = $this->tableGateway->selectWith($select);

        if (!$rowSet) {

            throw new \Exception("Could not find row $id");
        }

        return $rowSet;
    }


    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_ors_riat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function guardar(ValorTotalPotencias $ValorTotalPotencias) {

        try {

            $data = array(
                           'id_ors_riat' => $ValorTotalPotencias->getId_ors_riat(),
                           'tipo_registro' => $ValorTotalPotencias->getTipo_registro(),
                           'potencia_activa_sec_kw' => $ValorTotalPotencias->getPotencia_activa_sec_kw(),
                           'potencia_activa_prim_kw' => $ValorTotalPotencias->getPotencia_activa_prim_kw(),
                           'potencia_reactiva_kvar' => $ValorTotalPotencias->getPotencia_reactiva_kvar(),
                           'cos_fi' => $ValorTotalPotencias->getCos_fi(),
                          );

            $this->tableGateway->insert($data);
            
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        
    }

    public function eliminar($id) {
        $id = (int) $id;
        $this->tableGateway->delete(array('id_ors_riat' => $id));
    }



}
