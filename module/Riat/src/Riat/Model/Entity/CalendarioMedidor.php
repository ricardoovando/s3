<?php

namespace Riat\Model\Entity;

class CalendarioMedidor {

  private $ors_riat_id_ors_riat;
  private $fecha_encontrada_medidor;
  private $fecha_oficial_pais;
  private $fecha_dejada_medidor;

  function __construct($ors_riat_id_ors_riat = null, $fecha_encontrada_medidor = null, $fecha_oficial_pais = null, $fecha_dejada_medidor = null) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
      $this->fecha_encontrada_medidor = $fecha_encontrada_medidor;
      $this->fecha_oficial_pais = $fecha_oficial_pais;
      $this->fecha_dejada_medidor = $fecha_dejada_medidor;
  }
  
  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  
  public function getFecha_encontrada_medidor() {
      return $this->fecha_encontrada_medidor;
  }

  public function setFecha_encontrada_medidor($fecha_encontrada_medidor) {
      $this->fecha_encontrada_medidor = $fecha_encontrada_medidor;
  }

  public function getFecha_oficial_pais() {
      return $this->fecha_oficial_pais;
  }

  public function setFecha_oficial_pais($fecha_oficial_pais) {
      $this->fecha_oficial_pais = $fecha_oficial_pais;
  }

  public function getFecha_dejada_medidor() {
      return $this->fecha_dejada_medidor;
  }

  public function setFecha_dejada_medidor($fecha_dejada_medidor) {
      $this->fecha_dejada_medidor = $fecha_dejada_medidor;
  }
  
  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->fecha_encontrada_medidor = (isset($data['fecha_encontrada_medidor'])) ? $data['fecha_encontrada_medidor'] : null;
      $this->fecha_oficial_pais = (isset($data['fecha_oficial_pais'])) ? $data['fecha_oficial_pais'] : null;
      $this->fecha_dejada_medidor = (isset($data['fecha_dejada_medidor'])) ? $data['fecha_dejada_medidor'] : null;
      
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

