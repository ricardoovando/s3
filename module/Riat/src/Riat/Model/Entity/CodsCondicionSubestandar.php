<?php

namespace Avisos\Model\Entity;

class CodsCondicionSubestandar {

    private $idcods_condicion_subestandar;
    private $descripcion;

    public function __construct($idcods_condicion_subestandar = null, $descripcion = null) {
        $this->idcods_condicion_subestandar = $idcods_condicion_subestandar;
        $this->descripcion = $descripcion;
    }

    public function getIdcods_condicion_subestandar() {
        return $this->idcods_condicion_subestandar;
    }

    public function setIdcods_condicion_subestandar($idcods_condicion_subestandar) {
        $this->idcods_condicion_subestandar = $idcods_condicion_subestandar;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idcods_condicion_subestandar = (isset($data['idcods_condicion_subestandar'])) ? $data['idcods_condicion_subestandar'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

