<?php

namespace Avisos\Model\Entity;

class CodsIrregDetec {

    private $idcod;
    private $descripcion;

    public function __construct($idcod = null, $descripcion = null) {
        $this->idcod = $idcod;
        $this->descripcion = $descripcion;
    }

    public function getIdcod() {
        return $this->idcod;
    }

    public function setIdcod($idcod) {
        $this->idcod = $idcod;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    
    public function exchangeArray($data) {
        $this->idcod = (isset($data['idcod'])) ? $data['idcod'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

