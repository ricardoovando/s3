<?php

namespace Avisos\Model\Entity;

class CodsMantRealiz {

    private $idcods_mant_realiz;
    private $descripcion;

    public function __construct($idcods_mant_realiz = null, $descripcion = null) {
        $this->idcods_mant_realiz = $idcods_mant_realiz;
        $this->descripcion = $descripcion;
    }

    public function getIdcods_mant_realiz() {
        return $this->idcods_mant_realiz;
    }

    public function setIdcods_mant_realiz($idcods_mant_realiz) {
        $this->idcods_mant_realiz = $idcods_mant_realiz;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idcods_mant_realiz = (isset($data['idcods_mant_realiz'])) ? $data['idcods_mant_realiz'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

