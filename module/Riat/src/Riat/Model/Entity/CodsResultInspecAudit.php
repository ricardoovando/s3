<?php

namespace Avisos\Model\Entity;

class CodsResultInspecAudit {

    private $idcods_result_inspec_audit;
    private $descripcion;

    public function __construct($idcods_result_inspec_audit = null, $descripcion = null) {
        $this->idcods_result_inspec_audit = $idcods_result_inspec_audit;
        $this->descripcion = $descripcion;
    }

    public function getIdcods_result_inspec_audit() {
        return $this->idcods_result_inspec_audit;
    }

    public function setIdcods_result_inspec_audit($idcods_result_inspec_audit) {
        $this->idcods_result_inspec_audit = $idcods_result_inspec_audit;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idcods_result_inspec_audit = (isset($data['idcods_result_inspec_audit'])) ? $data['idcods_result_inspec_audit'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

