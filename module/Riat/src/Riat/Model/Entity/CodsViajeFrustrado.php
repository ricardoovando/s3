<?php

namespace Avisos\Model\Entity;

class CodsViajeFrustrado {

    private $idtipo;
    private $nombre;
    private $descripcion;

    public function __construct($idtipo = null, $nombre = null, $descripcion = null) {
        $this->idtipo = $idtipo;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
    }

    public function getIdtipo() {
        return $this->idtipo;
    }

    public function setIdtipo($idtipo) {
        $this->idtipo = $idtipo;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idtipo = (isset($data['idtipo'])) ? $data['idtipo'] : null;
        $this->nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

