<?php

namespace Riat\Model\Entity;

class ContactoAutoriza {

  private $ors_riat_id_ors_riat;
  private $nombre_contacto;
  private $run;
  private $fono;
  private $email;
  
  function __construct($ors_riat_id_ors_riat = null, $nombre_contacto = null, $run = null, $fono = null, $email = null) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
      $this->nombre_contacto = $nombre_contacto;
      $this->run = $run;
      $this->fono = $fono;
      $this->email = $email;
  }

  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  public function getNombre_contacto() {
      return $this->nombre_contacto;
  }

  public function setNombre_contacto($nombre_contacto) {
      $this->nombre_contacto = $nombre_contacto;
  }

  public function getRun() {
      return $this->run;
  }

  public function setRun($run) {
      $this->run = $run;
  }

  public function getFono() {
      return $this->fono;
  }

  public function setFono($fono) {
      $this->fono = $fono;
  }

  public function getEmail() {
      return $this->email;
  }

  public function setEmail($email) {
      $this->email = $email;
  }

  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->nombre_contacto = (isset($data['nombre_contacto'])) ? $data['nombre_contacto'] : null;
      $this->run = (isset($data['run'])) ? $data['run'] : null;
      $this->fono = (isset($data['fono'])) ? $data['fono'] : null;
      $this->email = (isset($data['email'])) ? $data['email'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

