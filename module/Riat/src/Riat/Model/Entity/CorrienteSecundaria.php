<?php

namespace Riat\Model\Entity;

class CorrienteSecundaria {

    private $id_ors_riat;
    private $fase;
    private $acometida_meditor_terminal;
    private $medidor_regleta_lado_fuente;
    private $corriente_primaria_ttcc;
    private $razon_ttcc_calculada;
    
    
    public function __construct($id_ors_riat = null, $fase = null, $acometida_meditor_terminal = null, $medidor_regleta_lado_fuente = null, $corriente_primaria_ttcc = null, $razon_ttcc_calculada = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->fase = $fase;
        $this->acometida_meditor_terminal = $acometida_meditor_terminal;
        $this->medidor_regleta_lado_fuente = $medidor_regleta_lado_fuente;
        $this->corriente_primaria_ttcc = $corriente_primaria_ttcc;
        $this->razon_ttcc_calculada = $razon_ttcc_calculada;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    
    public function getFase() {
        return $this->fase;
    }

    public function setFase($fase) {
        $this->fase = $fase;
    }

    public function getAcometida_meditor_terminal() {
        return $this->acometida_meditor_terminal;
    }

    public function setAcometida_meditor_terminal($acometida_meditor_terminal) {
        $this->acometida_meditor_terminal = $acometida_meditor_terminal;
    }

    public function getMedidor_regleta_lado_fuente() {
        return $this->medidor_regleta_lado_fuente;
    }

    public function setMedidor_regleta_lado_fuente($medidor_regleta_lado_fuente) {
        $this->medidor_regleta_lado_fuente = $medidor_regleta_lado_fuente;
    }

    public function getCorriente_primaria_ttcc() {
        return $this->corriente_primaria_ttcc;
    }

    public function setCorriente_primaria_ttcc($corriente_primaria_ttcc) {
        $this->corriente_primaria_ttcc = $corriente_primaria_ttcc;
    }

    public function getRazon_ttcc_calculada() {
        return $this->razon_ttcc_calculada;
    }

    public function setRazon_ttcc_calculada($razon_ttcc_calculada) {
        $this->razon_ttcc_calculada = $razon_ttcc_calculada;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->fase = (isset($data['fase'])) ? $data['fase'] : null;
        $this->acometida_meditor_terminal = (isset($data['acometida_meditor_terminal'])) ? $data['acometida_meditor_terminal'] : null;
        $this->medidor_regleta_lado_fuente = (isset($data['medidor_regleta_lado_fuente'])) ? $data['medidor_regleta_lado_fuente'] : null;
        $this->corriente_primaria_ttcc = (isset($data['corriente_primaria_ttcc'])) ? $data['corriente_primaria_ttcc'] : null;
        $this->razon_ttcc_calculada = (isset($data['razon_ttcc_calculada'])) ? $data['razon_ttcc_calculada'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

