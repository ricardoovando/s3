<?php

namespace Riat\Model\Entity;

class DatosAdjuntos {

  private $ors_riat_id_ors_riat;
  private $data_perfil_carga;
  private $diagrama_fasorial;
  private $fotos_empalme;

  function __construct($ors_riat_id_ors_riat = null, $data_perfil_carga = null, $diagrama_fasorial = null, $fotos_empalme = null) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
      $this->data_perfil_carga = $data_perfil_carga;
      $this->diagrama_fasorial = $diagrama_fasorial;
      $this->fotos_empalme = $fotos_empalme;
  }

  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  public function getData_perfil_carga() {
      return $this->data_perfil_carga;
  }

  public function setData_perfil_carga($data_perfil_carga) {
      $this->data_perfil_carga = $data_perfil_carga;
  }

  public function getDiagrama_fasorial() {
      return $this->diagrama_fasorial;
  }

  public function setDiagrama_fasorial($diagrama_fasorial) {
      $this->diagrama_fasorial = $diagrama_fasorial;
  }

  public function getFotos_empalme() {
      return $this->fotos_empalme;
  }

  public function setFotos_empalme($fotos_empalme) {
      $this->fotos_empalme = $fotos_empalme;
  }

  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->data_perfil_carga = (isset($data['data_perfil_carga'])) ? $data['data_perfil_carga'] : null;
      $this->diagrama_fasorial = (isset($data['diagrama_fasorial'])) ? $data['diagrama_fasorial'] : null;
      $this->fotos_empalme = (isset($data['fotos_empalme'])) ? $data['fotos_empalme'] : null;
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

