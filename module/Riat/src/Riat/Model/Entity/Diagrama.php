<?php

namespace Riat\Model\Entity;

class Diagrama {

    private $ors_riat_id_ors_riat;
    private $elemento_v1;
    private $elemento_v2;
    private $elemento_v3;
    private $secuencia;
    private $flujo_potencia;

    public function __construct($ors_riat_id_ors_riat = null, $elemento_v1 = null, $elemento_v2 = null, $elemento_v3 = null, $secuencia = null, $flujo_potencia = null) {
        $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
        $this->elemento_v1 = $elemento_v1;
        $this->elemento_v2 = $elemento_v2;
        $this->elemento_v3 = $elemento_v3;
        $this->secuencia = $secuencia;
        $this->flujo_potencia = $flujo_potencia;
    }
    
    public function getOrs_riat_id_ors_riat() {
        return $this->ors_riat_id_ors_riat;
    }

    public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
        $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
    }
 
    public function getElemento_v1() {
        return $this->elemento_v1;
    }

    public function setElemento_v1($elemento_v1) {
        $this->elemento_v1 = $elemento_v1;
    }

    public function getElemento_v2() {
        return $this->elemento_v2;
    }

    public function setElemento_v2($elemento_v2) {
        $this->elemento_v2 = $elemento_v2;
    }

    public function getElemento_v3() {
        return $this->elemento_v3;
    }

    public function setElemento_v3($elemento_v3) {
        $this->elemento_v3 = $elemento_v3;
    }

    public function getSecuencia() {
        return $this->secuencia;
    }

    public function setSecuencia($secuencia) {
        $this->secuencia = $secuencia;
    }

    public function getFlujo_potencia() {
        return $this->flujo_potencia;
    }

    public function setFlujo_potencia($flujo_potencia) {
        $this->flujo_potencia = $flujo_potencia;
    }

    public function exchangeArray($data) {
        $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
        $this->elemento_v1 = (isset($data['elemento_v1'])) ? $data['elemento_v1'] : null;
        $this->elemento_v2 = (isset($data['elemento_v2'])) ? $data['elemento_v2'] : null;
        $this->elemento_v3 = (isset($data['elemento_v3'])) ? $data['elemento_v3'] : null;
        $this->secuencia = (isset($data['secuencia'])) ? $data['secuencia'] : null;
        $this->flujo_potencia = (isset($data['flujo_potencia'])) ? $data['flujo_potencia'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

