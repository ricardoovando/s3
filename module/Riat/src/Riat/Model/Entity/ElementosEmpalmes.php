<?php

namespace Avisos\Model\Entity;

class ElementosEmpalmes {

    private $idelementos_empalmes;
    private $descripcion;

    public function __construct($idelementos_empalmes = null, $descripcion = null) {
        $this->idelementos_empalmes = $idelementos_empalmes;
        $this->descripcion = $descripcion;
    }

    public function getIdelementos_empalmes() {
        return $this->idelementos_empalmes;
    }

    public function setIdelementos_empalmes($idelementos_empalmes) {
        $this->idelementos_empalmes = $idelementos_empalmes;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function exchangeArray($data) {
        $this->idelementos_empalmes = (isset($data['idelementos_empalmes'])) ? $data['idelementos_empalmes'] : null;
        $this->descripcion = (isset($data['descripcion'])) ? $data['descripcion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

