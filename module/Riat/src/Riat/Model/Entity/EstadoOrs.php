<?php

namespace Riat\Model\Entity;

class EstadoOrs {


    private $id_estado_ors;
    private $descripcion_estado_ors;
    private $clase_color;

    function __construct($id_estado_ors = null, $descripcion_estado_ors =null, $clase_color = null) {
        $this->id_estado_ors = $id_estado_ors;
        $this->descripcion_estado_ors = $descripcion_estado_ors;
        $this->clase_color = $clase_color;
    }
    
    public function getId_estado_ors() {
        return $this->id_estado_ors;
    }

    public function setId_estado_ors($id_estado_ors) {
        $this->id_estado_ors = $id_estado_ors;
    }

    public function getDescripcion_estado_ors() {
        return $this->descripcion_estado_ors;
    }

    public function setDescripcion_estado_ors($descripcion_estado_ors) {
        $this->descripcion_estado_ors = $descripcion_estado_ors;
    }

    public function getClase_color() {
        return $this->clase_color;
    }

    public function setClase_color($clase_color) {
        $this->clase_color = $clase_color;
    }

    public function exchangeArray($data) {
        $this->id_estado_ors = (isset($data['id_estado_ors'])) ? $data['id_estado_ors'] : null;
        $this->descripcion_estado_ors = (isset($data['descripcion_estado_ors'])) ? $data['descripcion_estado_ors'] : null;
        $this->clase_color = (isset($data['clase_color'])) ? $data['clase_color'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

