<?php

namespace Riat\Model\Entity;

class InspeccionCondicionSubestandar {

    private $id_ors_riat;
    private $cods_condicion_subestandar_idcods_condicion_subestandar;
    private $elementos_empalmes_idelementos_empalmes;

    public function __construct($id_ors_riat = null, $cods_condicion_subestandar_idcods_condicion_subestandar = null, $elementos_empalmes_idelementos_empalmes = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->cods_condicion_subestandar_idcods_condicion_subestandar = $cods_condicion_subestandar_idcods_condicion_subestandar;
        $this->elementos_empalmes_idelementos_empalmes = $elementos_empalmes_idelementos_empalmes;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    
    public function getCods_condicion_subestandar_idcods_condicion_subestandar() {
        return $this->cods_condicion_subestandar_idcods_condicion_subestandar;
    }

    public function setCods_condicion_subestandar_idcods_condicion_subestandar($cods_condicion_subestandar_idcods_condicion_subestandar) {
        $this->cods_condicion_subestandar_idcods_condicion_subestandar = $cods_condicion_subestandar_idcods_condicion_subestandar;
    }

    public function getElementos_empalmes_idelementos_empalmes() {
        return $this->elementos_empalmes_idelementos_empalmes;
    }

    public function setElementos_empalmes_idelementos_empalmes($elementos_empalmes_idelementos_empalmes) {
        $this->elementos_empalmes_idelementos_empalmes = $elementos_empalmes_idelementos_empalmes;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->cods_condicion_subestandar_idcods_condicion_subestandar = (isset($data['cods_condicion_subestandar_idcods_condicion_subestandar'])) ? $data['cods_condicion_subestandar_idcods_condicion_subestandar'] : null;
        $this->elementos_empalmes_idelementos_empalmes = (isset($data['elementos_empalmes_idelementos_empalmes'])) ? $data['elementos_empalmes_idelementos_empalmes'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

