<?php

namespace Riat\Model\Entity;

class InspeccionVisualEmpalme {

    private $ors_riat_id_ors_riat;
    private $acometida_empalme;
    private $limitador_potencia;
    private $medidor;
    private $alambrado_medidor_regleta;
    private $estado_regleta_block;
    private $caja_medidor;
    private $caja_ttcc;
    private $estado_equipo_compacto;
    private $estado_interruptor_reloj;
    private $conexion_tierra_proteccion;
    
    public function __construct($ors_riat_id_ors_riat = null,$acometida_empalme = null,$limitador_potencia = null,$medidor = null,$alambrado_medidor_regleta = null,$estado_regleta_block = null,$caja_medidor = null,$caja_ttcc = null,$estado_equipo_compacto = null,$estado_interruptor_reloj = null,$conexion_tierra_proteccion = null) {
        $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
        $this->acometida_empalme = $acometida_empalme;
        $this->limitador_potencia = $limitador_potencia;
        $this->medidor = $medidor;
        $this->alambrado_medidor_regleta = $alambrado_medidor_regleta;
        $this->estado_regleta_block = $estado_regleta_block;
        $this->caja_medidor = $caja_medidor;
        $this->caja_ttcc = $caja_ttcc;
        $this->estado_equipo_compacto = $estado_equipo_compacto;
        $this->estado_interruptor_reloj = $estado_interruptor_reloj;
        $this->conexion_tierra_proteccion = $conexion_tierra_proteccion;
    }

    public function getOrs_riat_id_ors_riat() {
        return $this->ors_riat_id_ors_riat;
    }

    public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
        $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
    }

    public function getAcometida_empalme() {
        return $this->acometida_empalme;
    }

    public function setAcometida_empalme($acometida_empalme) {
        $this->acometida_empalme = $acometida_empalme;
    }

    public function getLimitador_potencia() {
        return $this->limitador_potencia;
    }

    public function setLimitador_potencia($limitador_potencia) {
        $this->limitador_potencia = $limitador_potencia;
    }

    public function getMedidor() {
        return $this->medidor;
    }

    public function setMedidor($medidor) {
        $this->medidor = $medidor;
    }

    public function getAlambrado_medidor_regleta() {
        return $this->alambrado_medidor_regleta;
    }

    public function setAlambrado_medidor_regleta($alambrado_medidor_regleta) {
        $this->alambrado_medidor_regleta = $alambrado_medidor_regleta;
    }

    public function getEstado_regleta_block() {
        return $this->estado_regleta_block;
    }

    public function setEstado_regleta_block($estado_regleta_block) {
        $this->estado_regleta_block = $estado_regleta_block;
    }

    public function getCaja_medidor() {
        return $this->caja_medidor;
    }

    public function setCaja_medidor($caja_medidor) {
        $this->caja_medidor = $caja_medidor;
    }

    public function getCaja_ttcc() {
        return $this->caja_ttcc;
    }

    public function setCaja_ttcc($caja_ttcc) {
        $this->caja_ttcc = $caja_ttcc;
    }

    public function getEstado_equipo_compacto() {
        return $this->estado_equipo_compacto;
    }

    public function setEstado_equipo_compacto($estado_equipo_compacto) {
        $this->estado_equipo_compacto = $estado_equipo_compacto;
    }

    public function getEstado_interruptor_reloj() {
        return $this->estado_interruptor_reloj;
    }

    public function setEstado_interruptor_reloj($estado_interruptor_reloj) {
        $this->estado_interruptor_reloj = $estado_interruptor_reloj;
    }

    public function getConexion_tierra_proteccion() {
        return $this->conexion_tierra_proteccion;
    }

    public function setConexion_tierra_proteccion($conexion_tierra_proteccion) {
        $this->conexion_tierra_proteccion = $conexion_tierra_proteccion;
    }

    
    public function exchangeArray($data) {
        $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
        $this->acometida_empalme = (isset($data['acometida_empalme'])) ? $data['acometida_empalme'] : null;
        $this->limitador_potencia = (isset($data['limitador_potencia'])) ? $data['limitador_potencia'] : null;
        $this->medidor = (isset($data['medidor'])) ? $data['medidor'] : null;
        $this->alambrado_medidor_regleta = (isset($data['alambrado_medidor_regleta'])) ? $data['alambrado_medidor_regleta'] : null;
        $this->estado_regleta_block = (isset($data['estado_regleta_block'])) ? $data['estado_regleta_block'] : null;
        $this->caja_medidor = (isset($data['caja_medidor'])) ? $data['caja_medidor'] : null;
        $this->caja_ttcc = (isset($data['caja_ttcc'])) ? $data['caja_ttcc'] : null;
        $this->estado_equipo_compacto = (isset($data['estado_equipo_compacto'])) ? $data['estado_equipo_compacto'] : null;
        $this->estado_interruptor_reloj = (isset($data['estado_interruptor_reloj'])) ? $data['estado_interruptor_reloj'] : null;
        $this->conexion_tierra_proteccion = (isset($data['conexion_tierra_proteccion'])) ? $data['conexion_tierra_proteccion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

