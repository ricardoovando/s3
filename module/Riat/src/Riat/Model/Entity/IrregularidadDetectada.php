<?php

namespace Riat\Model\Entity;

class IrregularidadDetectada {

    private $id_ors_riat;
    private $cods_irreg_detec_idcod;
    private $respuesta;

   public function __construct($id_ors_riat = null, $cods_irreg_detec_idcod = null, $respuesta = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->cods_irreg_detec_idcod = $cods_irreg_detec_idcod;
        $this->respuesta = $respuesta;
      }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
      }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
      }

    public function getCods_irreg_detec_idcod() {
        return $this->cods_irreg_detec_idcod;
      }

    public function setCods_irreg_detec_idcod($cods_irreg_detec_idcod) {
        $this->cods_irreg_detec_idcod = $cods_irreg_detec_idcod;
      }
    
    public function getRespuesta() {
        return $this->respuesta;
      }

    public function setRespuesta($respuesta) {
        $this->respuesta = $respuesta;
      }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->cods_irreg_detec_idcod = (isset($data['cods_irreg_detec_idcod'])) ? $data['cods_irreg_detec_idcod'] : null;
        $this->respuesta = (isset($data['respuesta'])) ? $data['respuesta'] : null;
      }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

