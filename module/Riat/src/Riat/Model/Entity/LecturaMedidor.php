<?php

namespace Riat\Model\Entity;

class LecturaMedidor {

    private $id_ors_riat;
    private $lecturas_encontradas_dejadas;
    private $energia_activa_kwh;
    private $energia_reactiva_kvarh;
    private $demanda_max_suministrada_leida_kw;
    private $fecha_hora_dda_max_sum_leida;
    private $demanda_max_suministrada_acumulada_kw;
    private $demanda_max_horas_punta_leida_kw;
    private $fecha_hora_dda_max_horas_punta_leida;
    private $demanda_max_horas_punta_acumulada_kw;
    private $numero_reset_medidor;
    private $fecha_hora_ultimo_reset;
              

    public function __construct($id_ors_riat = null, $lecturas_encontradas_dejadas = null, $energia_activa_kwh = null, $energia_reactiva_kvarh = null, $demanda_max_suministrada_leida_kw = null, $fecha_hora_dda_max_sum_leida = null, $demanda_max_suministrada_acumulada_kw = null, $demanda_max_horas_punta_leida_kw = null, $fecha_hora_dda_max_horas_punta_leida = null, $demanda_max_horas_punta_acumulada_kw = null, $numero_reset_medidor = null, $fecha_hora_ultimo_reset = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->lecturas_encontradas_dejadas = $lecturas_encontradas_dejadas;
        $this->energia_activa_kwh = $energia_activa_kwh;
        $this->energia_reactiva_kvarh = $energia_reactiva_kvarh;
        $this->demanda_max_suministrada_leida_kw = $demanda_max_suministrada_leida_kw;
        $this->fecha_hora_dda_max_sum_leida = $fecha_hora_dda_max_sum_leida;
        $this->demanda_max_suministrada_acumulada_kw = $demanda_max_suministrada_acumulada_kw;
        $this->demanda_max_horas_punta_leida_kw = $demanda_max_horas_punta_leida_kw;
        $this->fecha_hora_dda_max_horas_punta_leida = $fecha_hora_dda_max_horas_punta_leida;
        $this->demanda_max_horas_punta_acumulada_kw = $demanda_max_horas_punta_acumulada_kw;
        $this->numero_reset_medidor = $numero_reset_medidor;
        $this->fecha_hora_ultimo_reset = $fecha_hora_ultimo_reset;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getLecturas_encontradas_dejadas() {
        return $this->lecturas_encontradas_dejadas;
    }

    public function setLecturas_encontradas_dejadas($lecturas_encontradas_dejadas) {
        $this->lecturas_encontradas_dejadas = $lecturas_encontradas_dejadas;
    }

    public function getEnergia_activa_kwh() {
        return $this->energia_activa_kwh;
    }

    public function setEnergia_activa_kwh($energia_activa_kwh) {
        $this->energia_activa_kwh = $energia_activa_kwh;
    }

    public function getEnergia_reactiva_kvarh() {
        return $this->energia_reactiva_kvarh;
    }

    public function setEnergia_reactiva_kvarh($energia_reactiva_kvarh) {
        $this->energia_reactiva_kvarh = $energia_reactiva_kvarh;
    }

    public function getDemanda_max_suministrada_leida_kw() {
        return $this->demanda_max_suministrada_leida_kw;
    }

    public function setDemanda_max_suministrada_leida_kw($demanda_max_suministrada_leida_kw) {
        $this->demanda_max_suministrada_leida_kw = $demanda_max_suministrada_leida_kw;
    }

    public function getFecha_hora_dda_max_sum_leida() {
        return $this->fecha_hora_dda_max_sum_leida;
    }

    public function setFecha_hora_dda_max_sum_leida($fecha_hora_dda_max_sum_leida) {
        $this->fecha_hora_dda_max_sum_leida = $fecha_hora_dda_max_sum_leida;
    }

    public function getDemanda_max_suministrada_acumulada_kw() {
        return $this->demanda_max_suministrada_acumulada_kw;
    }

    public function setDemanda_max_suministrada_acumulada_kw($demanda_max_suministrada_acumulada_kw) {
        $this->demanda_max_suministrada_acumulada_kw = $demanda_max_suministrada_acumulada_kw;
    }

    public function getDemanda_max_horas_punta_leida_kw() {
        return $this->demanda_max_horas_punta_leida_kw;
    }

    public function setDemanda_max_horas_punta_leida_kw($demanda_max_horas_punta_leida_kw) {
        $this->demanda_max_horas_punta_leida_kw = $demanda_max_horas_punta_leida_kw;
    }

    public function getFecha_hora_dda_max_horas_punta_leida() {
        return $this->fecha_hora_dda_max_horas_punta_leida;
    }

    public function setFecha_hora_dda_max_horas_punta_leida($fecha_hora_dda_max_horas_punta_leida) {
        $this->fecha_hora_dda_max_horas_punta_leida = $fecha_hora_dda_max_horas_punta_leida;
    }

    public function getDemanda_max_horas_punta_acumulada_kw() {
        return $this->demanda_max_horas_punta_acumulada_kw;
    }

    public function setDemanda_max_horas_punta_acumulada_kw($demanda_max_horas_punta_acumulada_kw) {
        $this->demanda_max_horas_punta_acumulada_kw = $demanda_max_horas_punta_acumulada_kw;
    }

    public function getNumero_reset_medidor() {
        return $this->numero_reset_medidor;
    }

    public function setNumero_reset_medidor($numero_reset_medidor) {
        $this->numero_reset_medidor = $numero_reset_medidor;
    }

    public function getFecha_hora_ultimo_reset() {
        return $this->fecha_hora_ultimo_reset;
    }

    public function setFecha_hora_ultimo_reset($fecha_hora_ultimo_reset) {
        $this->fecha_hora_ultimo_reset = $fecha_hora_ultimo_reset;
    }

    public function exchangeArray($data) {
        
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->lecturas_encontradas_dejadas = (isset($data['lecturas_encontradas_dejadas'])) ? $data['lecturas_encontradas_dejadas'] : null;
        $this->energia_activa_kwh = (isset($data['energia_activa_kwh'])) ? $data['energia_activa_kwh'] : null;
        $this->energia_reactiva_kvarh = (isset($data['energia_reactiva_kvarh'])) ? $data['energia_reactiva_kvarh'] : null;
        $this->demanda_max_suministrada_leida_kw = (isset($data['demanda_max_suministrada_leida_kw'])) ? $data['demanda_max_suministrada_leida_kw'] : null;
        $this->fecha_hora_dda_max_sum_leida = (isset($data['fecha_hora_dda_max_sum_leida'])) ? $data['fecha_hora_dda_max_sum_leida'] : null;
        $this->demanda_max_suministrada_acumulada_kw = (isset($data['demanda_max_suministrada_acumulada_kw'])) ? $data['demanda_max_suministrada_acumulada_kw'] : null;
        $this->demanda_max_horas_punta_leida_kw = (isset($data['demanda_max_horas_punta_leida_kw'])) ? $data['demanda_max_horas_punta_leida_kw'] : null;
        $this->fecha_hora_dda_max_horas_punta_leida = (isset($data['fecha_hora_dda_max_horas_punta_leida'])) ? $data['fecha_hora_dda_max_horas_punta_leida'] : null;
        $this->demanda_max_horas_punta_acumulada_kw = (isset($data['demanda_max_horas_punta_acumulada_kw'])) ? $data['demanda_max_horas_punta_acumulada_kw'] : null;
        $this->numero_reset_medidor = (isset($data['numero_reset_medidor'])) ? $data['numero_reset_medidor'] : null;
        $this->fecha_hora_ultimo_reset = (isset($data['fecha_hora_ultimo_reset'])) ? $data['fecha_hora_ultimo_reset'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

