<?php

namespace Riat\Model\Entity;

class LecturaMedidorInstalado {

  private $ors_riat_id_ors_riat;
  private $tipo_elemento_id_tipo;
  private $kwh;
  private $kvar;
  private $kw_punta;
  private $kw_sum;

  function __construct($ors_riat_id_ors_riat = null, $tipo_elemento_id_tipo = null, $kwh = null, $kvar = null, $kw_punta = null, $kw_sum = null) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
      $this->tipo_elemento_id_tipo = $tipo_elemento_id_tipo;
      $this->kwh = $kwh;
      $this->kvar = $kvar;
      $this->kw_punta = $kw_punta;
      $this->kw_sum = $kw_sum;
  }

  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  public function getTipo_elemento_id_tipo() {
      return $this->tipo_elemento_id_tipo;
  }

  public function setTipo_elemento_id_tipo($tipo_elemento_id_tipo) {
      $this->tipo_elemento_id_tipo = $tipo_elemento_id_tipo;
  }

  public function getKwh() {
      return $this->kwh;
  }

  public function setKwh($kwh) {
      $this->kwh = $kwh;
  }

  public function getKvar() {
      return $this->kvar;
  }

  public function setKvar($kvar) {
      $this->kvar = $kvar;
  }

  public function getKw_punta() {
      return $this->kw_punta;
  }

  public function setKw_punta($kw_punta) {
      $this->kw_punta = $kw_punta;
  }

  public function getKw_sum() {
      return $this->kw_sum;
  }

  public function setKw_sum($kw_sum) {
      $this->kw_sum = $kw_sum;
  }
  
  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->tipo_elemento_id_tipo = (isset($data['tipo_elemento_id_tipo'])) ? $data['tipo_elemento_id_tipo'] : null;
      $this->kwh = (isset($data['kwh'])) ? $data['kwh'] : null;
      $this->kvar = (isset($data['kvar'])) ? $data['kvar'] : null;
      $this->kw_punta = (isset($data['kw_punta'])) ? $data['kw_punta'] : null;
      $this->kw_sum = (isset($data['kw_sum'])) ? $data['kw_sum'] : null;
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

