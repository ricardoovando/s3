<?php

namespace Riat\Model\Entity;

class MantencionRealizada {

    private $id_ors_riat;
    private $cods_mant_realiz_idcods_mant_realiz;
    private $resultado_mantencion_realizada;    

    function __construct($id_ors_riat = null, $cods_mant_realiz_idcods_mant_realiz = null, $resultado_mantencion_realizada = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->cods_mant_realiz_idcods_mant_realiz = $cods_mant_realiz_idcods_mant_realiz;
        $this->resultado_mantencion_realizada = $resultado_mantencion_realizada;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getCods_mant_realiz_idcods_mant_realiz() {
        return $this->cods_mant_realiz_idcods_mant_realiz;
    }

    public function setCods_mant_realiz_idcods_mant_realiz($cods_mant_realiz_idcods_mant_realiz) {
        $this->cods_mant_realiz_idcods_mant_realiz = $cods_mant_realiz_idcods_mant_realiz;
    }

    public function getResultado_mantencion_realizada() {
        return $this->resultado_mantencion_realizada;
    }

    public function setResultado_mantencion_realizada($resultado_mantencion_realizada) {
        $this->resultado_mantencion_realizada = $resultado_mantencion_realizada;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->cods_mant_realiz_idcods_mant_realiz = (isset($data['cods_mant_realiz_idcods_mant_realiz'])) ? $data['cods_mant_realiz_idcods_mant_realiz'] : null;
        $this->resultado_mantencion_realizada = (isset($data['resultado_mantencion_realizada'])) ? $data['resultado_mantencion_realizada'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

