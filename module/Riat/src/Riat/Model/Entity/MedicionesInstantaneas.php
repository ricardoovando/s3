<?php

namespace Riat\Model\Entity;

class MedicionesInstantaneas {

    private $id_ors_riat;
    private $fase;
    private $voltaje_ff_v;
    private $voltaje_f_tierra_v;
    private $voltaje_f_n_v;
    private $angulo_voltaje;
    private $corrientes_a;
    private $angulos_corrientes;
    private $cos_fi;
    private $potencia_activa_kw;
    private $potencia_reactiva_kvar;
   
    
    public function __construct($id_ors_riat = null, $fase = null, $voltaje_ff_v = null, $voltaje_f_tierra_v = null, $voltaje_f_n_v = null, $angulo_voltaje = null, $corrientes_a = null, $angulos_corrientes = null, $cos_fi = null, $potencia_activa_kw = null, $potencia_reactiva_kvar = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->fase = $fase;
        $this->voltaje_ff_v = $voltaje_ff_v;
        $this->voltaje_f_tierra_v = $voltaje_f_tierra_v;
        $this->voltaje_f_n_v = $voltaje_f_n_v;
        $this->angulo_voltaje = $angulo_voltaje;
        $this->corrientes_a = $corrientes_a;
        $this->angulos_corrientes = $angulos_corrientes;
        $this->cos_fi = $cos_fi;
        $this->potencia_activa_kw = $potencia_activa_kw;
        $this->potencia_reactiva_kvar = $potencia_reactiva_kvar;
    }
    
    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getFase() {
        return $this->fase;
    }

    public function setFase($fase) {
        $this->fase = $fase;
    }

    public function getVoltaje_ff_v() {
        return $this->voltaje_ff_v;
    }

    public function setVoltaje_ff_v($voltaje_ff_v) {
        $this->voltaje_ff_v = $voltaje_ff_v;
    }

    public function getVoltaje_f_tierra_v() {
        return $this->voltaje_f_tierra_v;
    }

    public function setVoltaje_f_tierra_v($voltaje_f_tierra_v) {
        $this->voltaje_f_tierra_v = $voltaje_f_tierra_v;
    }

    public function getVoltaje_f_n_v() {
        return $this->voltaje_f_n_v;
    }

    public function setVoltaje_f_n_v($voltaje_f_n_v) {
        $this->voltaje_f_n_v = $voltaje_f_n_v;
    }

    public function getAngulo_voltaje() {
        return $this->angulo_voltaje;
    }

    public function setAngulo_voltaje($angulo_voltaje) {
        $this->angulo_voltaje = $angulo_voltaje;
    }

    public function getCorrientes_a() {
        return $this->corrientes_a;
    }

    public function setCorrientes_a($corrientes_a) {
        $this->corrientes_a = $corrientes_a;
    }

    public function getAngulos_corrientes() {
        return $this->angulos_corrientes;
    }

    public function setAngulos_corrientes($angulos_corrientes) {
        $this->angulos_corrientes = $angulos_corrientes;
    }

    public function getCos_fi() {
        return $this->cos_fi;
    }

    public function setCos_fi($cos_fi) {
        $this->cos_fi = $cos_fi;
    }

    public function getPotencia_activa_kw() {
        return $this->potencia_activa_kw;
    }

    public function setPotencia_activa_kw($potencia_activa_kw) {
        $this->potencia_activa_kw = $potencia_activa_kw;
    }

    public function getPotencia_reactiva_kvar() {
        return $this->potencia_reactiva_kvar;
    }

    public function setPotencia_reactiva_kvar($potencia_reactiva_kvar) {
        $this->potencia_reactiva_kvar = $potencia_reactiva_kvar;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->fase = (isset($data['fase'])) ? $data['fase'] : null;
        $this->voltaje_ff_v = (isset($data['voltaje_ff_v'])) ? $data['voltaje_ff_v'] : null;
        $this->voltaje_f_tierra_v = (isset($data['voltaje_f_tierra_v'])) ? $data['voltaje_f_tierra_v'] : null;
        $this->voltaje_f_n_v = (isset($data['voltaje_f_n_v'])) ? $data['voltaje_f_n_v'] : null;
        $this->angulo_voltaje = (isset($data['angulo_voltaje'])) ? $data['angulo_voltaje'] : null;
        $this->corrientes_a = (isset($data['corrientes_a'])) ? $data['corrientes_a'] : null;
        $this->angulos_corrientes = (isset($data['angulos_corrientes'])) ? $data['angulos_corrientes'] : null;
        $this->cos_fi = (isset($data['cos_fi'])) ? $data['cos_fi'] : null;
        $this->potencia_activa_kw = (isset($data['potencia_activa_kw'])) ? $data['potencia_activa_kw'] : null;
        $this->potencia_reactiva_kvar = (isset($data['potencia_reactiva_kvar'])) ? $data['potencia_reactiva_kvar'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

