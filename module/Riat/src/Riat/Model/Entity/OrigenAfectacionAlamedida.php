<?php

namespace Riat\Model\Entity;

class OrigenAfectacionAlamedida {

  private $ors_riat_id_ors_riat;
  private $origen_afectacion;
  private $comentario;
  
  function __construct($ors_riat_id_ors_riat = null, $origen_afectacion = null, $comentario = null) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
      $this->origen_afectacion = $origen_afectacion;
      $this->comentario = $comentario;
  }
  
  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  public function getOrigen_afectacion() {
      return $this->origen_afectacion;
  }

  public function setOrigen_afectacion($origen_afectacion) {
      $this->origen_afectacion = $origen_afectacion;
  }

  public function getComentario() {
      return $this->comentario;
  }

  public function setComentario($comentario) {
      $this->comentario = $comentario;
  }
      
  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->origen_afectacion = (isset($data['origen_afectacion'])) ? $data['origen_afectacion'] : null;
      $this->comentario = (isset($data['comentario'])) ? $data['comentario'] : null;
  }

  public function getArrayCopy() {
    return get_object_vars($this);
  }

}

