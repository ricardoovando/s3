<?php

namespace Riat\Model\Entity;

class OrsRiat {

  private $id_ors_riat;
  private $estados_ors_id_estado_ors;
  private $avisos_id_aviso;
  private $fecha_asignacion_riat;
  private $fecha_comprometido;
  private $fecha_entrega_dig;
  private $fecha_entrega_fisica;
  private $digitador_personal_id_persona;
  private $num_form_riat;
  private $fecha_atencion_servicio;

    
  /* private $estado_riat;
  private $anor_encajadeempalme;
  private $anor_enecm;
  private $anor_enmedidor;
  private $anor_esquemademedida;
  private $anor_faltamedidareactiva;
  private $anor_alarmadebateria;
  private $anor_cargabajavariable;
  private $anor_tarifanocorresponde;
  private $anor_faltasello;
  private $anor_ttcc;
  private $anor_otro;
  private $clas_cnr_1;
  private $clas_cnr_2;
  private $clas_cnr_3;
  private $clas_cnr_4;
  private $clas_cnr_5;
  private $clas_cnr_6;
  private $clas_cnr_7;
  private $clas_cnr_8;
  private $clas_cnr_9; */
  
  private $tecnico2_personal_id_persona;
  private $tiene_reloj;
  private $tiene_telemedida;
  private $comportamiento_carga;
	
    
  /*Objeto Relacional*/
  private $estado_ors;
  private $aviso;
  private $digitador_persona;
  private $instalacion;
  private $distribuidora;
  private $datos_adjuntos;
  private $antecedentesempalme;
  //private $reloj;
  private $regleta;
  //private $telemedida;
  private $ecm;
  private $calendariomedidor;
  private $inspeccionvisualempalme;
  private $diagrama;
  private $razonencontrada;
  private $origenafectacionalamedida;
  private $lecturamedidorinstalado;
  private $contactoautoriza;
  private $tecnico_persona;
  private $sistemadistribucion;
  private $comuna;
  private $provincia;  

  function __construct( $id_ors_riat = null, 
                        $estados_ors_id_estado_ors = null, 
                        $avisos_id_aviso = null, $fecha_asignacion_riat = null, 
                        $fecha_comprometido = null, $fecha_entrega_dig = null, 
                        $fecha_entrega_fisica = null, $digitador_personal_id_persona = null, 
                        $num_form_riat = null, $fecha_atencion_servicio = null, 
                        
                        $estado_riat = null, $anor_encajadeempalme = null, 
                        /*$anor_enecm = null, $anor_enmedidor = null, 
                        $anor_esquemademedida = null, $anor_faltamedidareactiva = null, 
                        $anor_alarmadebateria = null, $anor_cargabajavariable = null, 
                        $anor_tarifanocorresponde = null, $anor_faltasello = null, 
                        $anor_ttcc = null, $anor_otro = null, $clas_cnr_1 = null, 
                        $clas_cnr_2 = null, $clas_cnr_3 = null, $clas_cnr_4 = null, 
                        $clas_cnr_5 = null, $clas_cnr_6 = null, $clas_cnr_7 = null, 
                        $clas_cnr_8 = null, $clas_cnr_9 = null,*/ 
                        
                        $tecnico2_personal_id_persona = null,
                        $tiene_reloj = null,
                        $tiene_telemedida = null,
                        $comportamiento_carga = null ) {
                        	
      $this->id_ors_riat = $id_ors_riat;
      $this->estados_ors_id_estado_ors = $estados_ors_id_estado_ors;
      $this->avisos_id_aviso = $avisos_id_aviso;
      $this->fecha_asignacion_riat = $fecha_asignacion_riat;
      $this->fecha_comprometido = $fecha_comprometido;
      $this->fecha_entrega_dig = $fecha_entrega_dig;
      $this->fecha_entrega_fisica = $fecha_entrega_fisica;
      $this->digitador_personal_id_persona = $digitador_personal_id_persona;
      $this->num_form_riat = $num_form_riat;
      $this->fecha_atencion_servicio = $fecha_atencion_servicio;
    	   
			$this->estado_riat = $estado_riat;
		    /*  $this->anor_encajadeempalme = $anor_encajadeempalme;
		      $this->anor_enecm = $anor_enecm;
		      $this->anor_enmedidor = $anor_enmedidor;
		      $this->anor_esquemademedida = $anor_esquemademedida;
		      $this->anor_faltamedidareactiva = $anor_faltamedidareactiva;
		      $this->anor_alarmadebateria = $anor_alarmadebateria;
		      $this->anor_cargabajavariable = $anor_cargabajavariable;
		      $this->anor_tarifanocorresponde = $anor_tarifanocorresponde;
		      $this->anor_faltasello = $anor_faltasello;
		      $this->anor_ttcc = $anor_ttcc;
		      $this->anor_otro = $anor_otro;
		      $this->clas_cnr_1 = $clas_cnr_1;
		      $this->clas_cnr_2 = $clas_cnr_2;
		      $this->clas_cnr_3 = $clas_cnr_3;
		      $this->clas_cnr_4 = $clas_cnr_4;
		      $this->clas_cnr_5 = $clas_cnr_5;
		      $this->clas_cnr_6 = $clas_cnr_6;
		      $this->clas_cnr_7 = $clas_cnr_7;
		      $this->clas_cnr_8 = $clas_cnr_8;
		      $this->clas_cnr_9 = $clas_cnr_9;*/
      
      $this->tecnico2_personal_id_persona = $tecnico2_personal_id_persona;
	  $this->tiene_reloj = $tiene_reloj;
	  $this->tiene_telemedida = $tiene_telemedida;
	  $this->comportamiento_carga = $comportamiento_carga; 
	  
   }

  public function getId_ors_riat() {
      return $this->id_ors_riat;
  }

  public function setId_ors_riat($id_ors_riat) {
      $this->id_ors_riat = $id_ors_riat;
  }

  public function getEstados_ors_id_estado_ors() {
      return $this->estados_ors_id_estado_ors;
  }

  public function setEstados_ors_id_estado_ors($estados_ors_id_estado_ors) {
      $this->estados_ors_id_estado_ors = $estados_ors_id_estado_ors;
  }

  public function getAvisos_id_aviso() {
      return $this->avisos_id_aviso;
  }

  public function setAvisos_id_aviso($avisos_id_aviso) {
      $this->avisos_id_aviso = $avisos_id_aviso;
  }

  public function getFecha_asignacion_riat() {
      return $this->fecha_asignacion_riat;
  }

  public function setFecha_asignacion_riat($fecha_asignacion_riat) {
      $this->fecha_asignacion_riat = $fecha_asignacion_riat;
  }

  public function getFecha_comprometido() {
      return $this->fecha_comprometido;
  }

  public function setFecha_comprometido($fecha_comprometido) {
      $this->fecha_comprometido = $fecha_comprometido;
  }

  public function getFecha_entrega_dig() {
      return $this->fecha_entrega_dig;
  }

  public function setFecha_entrega_dig($fecha_entrega_dig) {
      $this->fecha_entrega_dig = $fecha_entrega_dig;
  }

  public function getFecha_entrega_fisica() {
      return $this->fecha_entrega_fisica;
  }

  public function setFecha_entrega_fisica($fecha_entrega_fisica) {
      $this->fecha_entrega_fisica = $fecha_entrega_fisica;
  }

  public function getDigitador_personal_id_persona() {
      return $this->digitador_personal_id_persona;
  }

  public function setDigitador_personal_id_persona($digitador_personal_id_persona) {
      $this->digitador_personal_id_persona = $digitador_personal_id_persona;
  }
 
  public function getNum_form_riat() {
      return $this->num_form_riat;
  }

  public function setNum_form_riat($num_form_riat) {
      $this->num_form_riat = $num_form_riat;
  }
  
  public function getFecha_atencion_servicio() {
      return $this->fecha_atencion_servicio;
  }

  public function setFecha_atencion_servicio($fecha_atencion_servicio) {
      $this->fecha_atencion_servicio = $fecha_atencion_servicio;
  }

  public function getTiene_reloj() {
      return $this->tiene_reloj;
  }

  public function setTiene_reloj($tiene_reloj) {
      $this->tiene_reloj = $tiene_reloj;
  }
		
  public function getTiene_telemedida() {
      return $this->tiene_telemedida;
  }
		
  public function setTiene_telemedida($tiene_telemedida) {
      $this->tiene_telemedida = $tiene_telemedida;
  }
		
  public function getComportamiento_carga() {
      return $this->comportamiento_carga;
  }
	
  public function setComportamiento_carga($comportamiento_carga) {
      $this->comportamiento_carga = $comportamiento_carga;
  }
		
 
  public function getEstado_riat() {
      return $this->estado_riat;
  }

  public function setEstado_riat($estado_riat) {
      $this->estado_riat = $estado_riat;
  } 
  
  
  /*
  public function getAnor_encajadeempalme() {
      return $this->anor_encajadeempalme;
  }

  public function setAnor_encajadeempalme($anor_encajadeempalme) {
      $this->anor_encajadeempalme = $anor_encajadeempalme;
  }

  public function getAnor_enecm() {
      return $this->anor_enecm;
  }

  public function setAnor_enecm($anor_enecm) {
      $this->anor_enecm = $anor_enecm;
  }

  public function getAnor_enmedidor() {
      return $this->anor_enmedidor;
  }

  public function setAnor_enmedidor($anor_enmedidor) {
      $this->anor_enmedidor = $anor_enmedidor;
  }

  public function getAnor_esquemademedida() {
      return $this->anor_esquemademedida;
  }

  public function setAnor_esquemademedida($anor_esquemademedida) {
      $this->anor_esquemademedida = $anor_esquemademedida;
  }

  public function getAnor_faltamedidareactiva() {
      return $this->anor_faltamedidareactiva;
  }

  public function setAnor_faltamedidareactiva($anor_faltamedidareactiva) {
      $this->anor_faltamedidareactiva = $anor_faltamedidareactiva;
  }

  public function getAnor_alarmadebateria() {
      return $this->anor_alarmadebateria;
  }

  public function setAnor_alarmadebateria($anor_alarmadebateria) {
      $this->anor_alarmadebateria = $anor_alarmadebateria;
  }

  public function getAnor_cargabajavariable() {
      return $this->anor_cargabajavariable;
  }

  public function setAnor_cargabajavariable($anor_cargabajavariable) {
      $this->anor_cargabajavariable = $anor_cargabajavariable;
  }

  public function getAnor_tarifanocorresponde() {
      return $this->anor_tarifanocorresponde;
  }

  public function setAnor_tarifanocorresponde($anor_tarifanocorresponde) {
      $this->anor_tarifanocorresponde = $anor_tarifanocorresponde;
  }

  public function getAnor_faltasello() {
      return $this->anor_faltasello;
  }

  public function setAnor_faltasello($anor_faltasello) {
      $this->anor_faltasello = $anor_faltasello;
  }

  public function getAnor_ttcc() {
      return $this->anor_ttcc;
  }

  public function setAnor_ttcc($anor_ttcc) {
      $this->anor_ttcc = $anor_ttcc;
  }

  public function getAnor_otro() {
      return $this->anor_otro;
  }

  public function setAnor_otro($anor_otro) {
      $this->anor_otro = $anor_otro;
  }

  public function getClas_cnr_1() {
      return $this->clas_cnr_1;
  }

  public function setClas_cnr_1($clas_cnr_1) {
      $this->clas_cnr_1 = $clas_cnr_1;
  }

  public function getClas_cnr_2() {
      return $this->clas_cnr_2;
  }

  public function setClas_cnr_2($clas_cnr_2) {
      $this->clas_cnr_2 = $clas_cnr_2;
  }

  public function getClas_cnr_3() {
      return $this->clas_cnr_3;
  }

  public function setClas_cnr_3($clas_cnr_3) {
      $this->clas_cnr_3 = $clas_cnr_3;
  }

  public function getClas_cnr_4() {
      return $this->clas_cnr_4;
  }

  public function setClas_cnr_4($clas_cnr_4) {
      $this->clas_cnr_4 = $clas_cnr_4;
  }

  public function getClas_cnr_5() {
      return $this->clas_cnr_5;
  }

  public function setClas_cnr_5($clas_cnr_5) {
      $this->clas_cnr_5 = $clas_cnr_5;
  }

  public function getClas_cnr_6() {
      return $this->clas_cnr_6;
  }

  public function setClas_cnr_6($clas_cnr_6) {
      $this->clas_cnr_6 = $clas_cnr_6;
  }

  public function getClas_cnr_7() {
      return $this->clas_cnr_7;
  }

  public function setClas_cnr_7($clas_cnr_7) {
      $this->clas_cnr_7 = $clas_cnr_7;
  }

  public function getClas_cnr_8() {
      return $this->clas_cnr_8;
  }

  public function setClas_cnr_8($clas_cnr_8) {
      $this->clas_cnr_8 = $clas_cnr_8;
  }
  
  public function getClas_cnr_9() {
      return $this->clas_cnr_9;
  }

  public function setClas_cnr_9($clas_cnr_9) {
      $this->clas_cnr_9 = $clas_cnr_9;
  }
    */
    
	
  public function getTecnico2_personal_id_persona() {
      return $this->tecnico2_personal_id_persona;
  }

  public function setTecnico2_personal_id_persona($tecnico2_personal_id_persona) {
      $this->tecnico2_personal_id_persona = $tecnico2_personal_id_persona;
  }
  
  public function getEstado_ors() {
      return $this->estado_ors;
  }

  public function setEstado_ors(EstadoOrs $estado_ors) {
      $this->estado_ors = $estado_ors;
  }

  public function getAviso() {
      return $this->aviso;
  }

  public function setAviso(Aviso $aviso) {
      $this->aviso = $aviso;
  }

  public function getDigitador_persona() {
      return $this->digitador_persona;
  }

  public function setDigitador_persona(Persona $digitador_persona) {
      $this->digitador_persona = $digitador_persona;
  }
  
  public function getInstalacion() {
      return $this->instalacion;
  }

  public function setInstalacion(Instalacion $instalacion) {
      $this->instalacion = $instalacion;
  }
  
  public function getDistribuidora() {
      return $this->distribuidora;
  }

  public function setDistribuidora($distribuidora) {
      $this->distribuidora = $distribuidora;
  }

  public function getDatos_adjuntos() {
      return $this->datos_adjuntos;
  }

  public function setDatos_adjuntos(DatosAdjuntos $datos_adjuntos) {
      $this->datos_adjuntos = $datos_adjuntos;
  }
  
  public function getAntecedentesempalme() {
      return $this->antecedentesempalme;
  }

  public function setAntecedentesempalme(AntecedentesEmpalme $antecedentesempalme) {
      $this->antecedentesempalme = $antecedentesempalme;
  } 
  
  public function getRegleta() {
      return $this->regleta;
  }
  
  public function setRegleta(Regletas $regleta) {
      $this->regleta = $regleta;
  }
  
  public function getCalendariomedidor() {
      return $this->calendariomedidor;
  }
  
  public function setCalendariomedidor(Calendariomedidor $calendariomedidor) {
      $this->calendariomedidor = $calendariomedidor;
  }
  
  public function getInspeccionvisualempalme() {
      return $this->inspeccionvisualempalme;
  }
  
  public function setInspeccionvisualempalme(InspeccionVisualEmpalme $inspeccionvisualempalme) {
      $this->inspeccionvisualempalme = $inspeccionvisualempalme;
  }
  
  public function getDiagrama() {
      return $this->diagrama;
  }

  public function setDiagrama(Diagrama $diagrama) {
      $this->diagrama = $diagrama;
  }

  public function getRazonencontrada() {
      return $this->razonencontrada;
  }

  public function setRazonencontrada(RazonEncontrada $razonencontrada) {
      $this->razonencontrada = $razonencontrada;
  }

  public function getOrigenafectacionalamedida() {
      return $this->origenafectacionalamedida;
  }

  public function setOrigenafectacionalamedida(OrigenAfectacionAlamedida $origenafectacionalamedida) {
      $this->origenafectacionalamedida = $origenafectacionalamedida;
  }

  public function getLecturamedidorinstalado() {
      return $this->lecturamedidorinstalado;
  }

  public function setLecturamedidorinstalado(LecturaMedidorInstalado $lecturamedidorinstalado) {
      $this->lecturamedidorinstalado = $lecturamedidorinstalado;
  }
  
  public function getContactoautoriza() {
      return $this->contactoautoriza;
  }

  public function setContactoautoriza(ContactoAutoriza $contactoautoriza) {
      $this->contactoautoriza = $contactoautoriza;
  }

  public function getTecnico_persona() {
      return $this->tecnico_persona;
  }

  public function setTecnico_persona(Personal $tecnico_persona) {
      $this->tecnico_persona = $tecnico_persona;
  }

  public function getSistemadistribucion() {
      return $this->sistemadistribucion;
  }

  public function setSistemadistribucion(SistemaDistribucion $sistemadistribucion) {
      $this->sistemadistribucion = $sistemadistribucion;
  }

  public function getComuna() {
      return $this->comuna;
  }

  public function setComuna(Comuna $comuna) {
      $this->comuna = $comuna;
  }

  public function getProvincia() {
      return $this->provincia;
  }

  public function setProvincia(Provincia $provincia) {
      $this->provincia = $provincia;
  }
  
  public function getEcm() {
      return $this->ecm;
  }

  public function setEcm(Ecm $ecm) {
      $this->ecm = $ecm;
  }
	  
	                    
  public function exchangeArray($data) {
      
      $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
      $this->estados_ors_id_estado_ors = (isset($data['estados_ors_id_estado_ors'])) ? $data['estados_ors_id_estado_ors'] : null;
      $this->avisos_id_aviso = (isset($data['avisos_id_aviso'])) ? $data['avisos_id_aviso'] : null;
      $this->fecha_asignacion_riat = (isset($data['fecha_asignacion_riat'])) ? $data['fecha_asignacion_riat'] : null;
      $this->fecha_comprometido = (isset($data['fecha_comprometido'])) ? $data['fecha_comprometido'] : null;
      $this->fecha_entrega_dig = (isset($data['fecha_entrega_dig'])) ? $data['fecha_entrega_dig'] : null;
      $this->fecha_entrega_fisica = (isset($data['fecha_entrega_fisica'])) ? $data['fecha_entrega_fisica'] : null;
      $this->digitador_personal_id_persona = (isset($data['digitador_personal_id_persona'])) ? $data['digitador_personal_id_persona'] : null;
      $this->num_form_riat = (isset($data['num_form_riat'])) ? $data['num_form_riat'] : null;
      $this->fecha_atencion_servicio = (isset($data['fecha_atencion_servicio'])) ? $data['fecha_atencion_servicio'] : null;    
      $this->tiene_reloj = (isset($data['tiene_reloj'])) ? $data['tiene_reloj'] : null; 
	  $this->tiene_telemedida = (isset($data['tiene_telemedida'])) ? $data['tiene_telemedida'] : null; 
	  $this->comportamiento_carga = (isset($data['comportamiento_carga'])) ? $data['comportamiento_carga'] : null;
	   
      $this->estado_riat = (isset($data['estado_riat'])) ? $data['estado_riat'] : null;
      
      /*$this->anor_encajadeempalme = (isset($data['anor_encajadeempalme'])) ? $data['anor_encajadeempalme'] : null;
      $this->anor_enecm = (isset($data['anor_enecm'])) ? $data['anor_enecm'] : null;
      $this->anor_enmedidor = (isset($data['anor_enmedidor'])) ? $data['anor_enmedidor'] : null;
      $this->anor_esquemademedida = (isset($data['anor_esquemademedida'])) ? $data['anor_esquemademedida'] : null;
      $this->anor_faltamedidareactiva = (isset($data['anor_faltamedidareactiva'])) ? $data['anor_faltamedidareactiva'] : null;
      $this->anor_alarmadebateria = (isset($data['anor_alarmadebateria'])) ? $data['anor_alarmadebateria'] : null;
      $this->anor_cargabajavariable = (isset($data['anor_cargabajavariable'])) ? $data['anor_cargabajavariable'] : null;
      $this->anor_tarifanocorresponde = (isset($data['anor_tarifanocorresponde'])) ? $data['anor_tarifanocorresponde'] : null;
      $this->anor_faltasello = (isset($data['anor_faltasello'])) ? $data['anor_faltasello'] : null;
      $this->anor_ttcc = (isset($data['anor_ttcc'])) ? $data['anor_ttcc'] : null;
      $this->anor_otro = (isset($data['anor_otro'])) ? $data['anor_otro'] : null;
      $this->clas_cnr_1 = (isset($data['clas_cnr_1'])) ? $data['clas_cnr_1'] : null;
      $this->clas_cnr_2 = (isset($data['clas_cnr_2'])) ? $data['clas_cnr_2'] : null;
      $this->clas_cnr_3 = (isset($data['clas_cnr_3'])) ? $data['clas_cnr_3'] : null;
      $this->clas_cnr_4 = (isset($data['clas_cnr_4'])) ? $data['clas_cnr_4'] : null;
      $this->clas_cnr_5 = (isset($data['clas_cnr_5'])) ? $data['clas_cnr_5'] : null;
      $this->clas_cnr_6 = (isset($data['clas_cnr_6'])) ? $data['clas_cnr_6'] : null;
      $this->clas_cnr_7 = (isset($data['clas_cnr_7'])) ? $data['clas_cnr_7'] : null;
      $this->clas_cnr_8 = (isset($data['clas_cnr_8'])) ? $data['clas_cnr_8'] : null;
      $this->clas_cnr_9 = (isset($data['clas_cnr_9'])) ? $data['clas_cnr_9'] : null; */
      
      $this->tecnico2_personal_id_persona = (isset($data['tecnico2_personal_id_persona'])) ? $data['tecnico2_personal_id_persona'] : null;
      
        /*ESTADO DEL ORS*/
        $this->estado_ors = new \Riat\Model\Entity\EstadoOrs();
        $this->estado_ors->setDescripcion_estado_ors((isset($data['descripcion_estado_ors'])) ? $data['descripcion_estado_ors'] : null);
        $this->estado_ors->setClase_color((isset($data['clase_color'])) ? $data['clase_color'] : null);

        /*AVISO DEL RIAT*/
        $this->aviso = new \Avisos\Model\Entity\Aviso();
        $this->aviso->setId_aviso((isset($data['id_aviso'])) ? $data['id_aviso'] : null);
        $this->aviso->setNumero_aviso((isset($data['numero_aviso'])) ? $data['numero_aviso'] : null);
        $this->aviso->setInstalaciones_Id_Instalacion((isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null);
        $this->aviso->setResponsable_personal_id_persona((isset($data['responsable_personal_id_persona'])) ? $data['responsable_personal_id_persona'] : null);
        $this->aviso->setTipo_aviso((isset($data['tipo_aviso'])) ? $data['tipo_aviso'] : null);

        /*INSTALACION DEL RIAT*/
        $this->instalacion = new \Instalaciones\Model\Entity\Instalacion();
        $this->instalacion->setId_instalacion((isset($data['id_instalacion'])) ? $data['id_instalacion'] : null);
        $this->instalacion->setDistribuidoras_id_distribuidora((isset($data['distribuidoras_id_distribuidora'])) ? $data['distribuidoras_id_distribuidora'] : null);
        $this->instalacion->setComunas_id_comuna((isset($data['comunas_id_comuna'])) ? $data['comunas_id_comuna'] : null);
        $this->instalacion->setNombre_cliente((isset($data['nombre_cliente'])) ? $data['nombre_cliente'] : null);
        $this->instalacion->setDireccion1((isset($data['direccion1'])) ? $data['direccion1'] : null);
        $this->instalacion->setDireccion2((isset($data['direccion2'])) ? $data['direccion2'] : null);
        $this->instalacion->setNum_instalacion((isset($data['num_instalacion'])) ? $data['num_instalacion'] : null);
        $this->instalacion->setTarifa((isset($data['tarifa'])) ? $data['tarifa'] : null); 
        $this->instalacion->setTarifa_terreno((isset($data['tarifa_terreno'])) ? $data['tarifa_terreno'] : null);
        
        $this->instalacion->setUnidad_lectura((isset($data['unidad_lectura'])) ? $data['unidad_lectura'] : null);
        $this->instalacion->setCapacidad_automatico((isset($data['capacidad_automatico'])) ? $data['capacidad_automatico'] : null);
        $this->instalacion->setPotencia_contratada((isset($data['potencia_contratada'])) ? $data['potencia_contratada'] : null);
        $this->instalacion->setGeo_poste((isset($data['geo_poste'])) ? $data['geo_poste'] : null);
        $this->instalacion->setNumero_poste_camara((isset($data['numero_poste_camara'])) ? $data['numero_poste_camara'] : null);
        $this->instalacion->setGeo_medidor((isset($data['geo_medidor'])) ? $data['geo_medidor'] : null);
        $this->instalacion->setTipo_calle((isset($data['tipo_calle'])) ? $data['tipo_calle'] : null);
        $this->instalacion->setNombre_calle((isset($data['nombre_calle'])) ? $data['nombre_calle'] : null);
        $this->instalacion->setNumero_calle((isset($data['numero_calle'])) ? $data['numero_calle'] : null);
        $this->instalacion->setBlog((isset($data['blog'])) ? $data['blog'] : null);
        $this->instalacion->setDepto_casa((isset($data['depto_casa'])) ? $data['depto_casa'] : null);
        $this->instalacion->setNombre_condomio((isset($data['nombre_condomio'])) ? $data['nombre_condomio'] : null);
        $this->instalacion->setTipo_condominio((isset($data['tipo_condominio'])) ? $data['tipo_condominio'] : null);
        $this->instalacion->setConstante((isset($data['constante'])) ? $data['constante'] : null);
        
        $this->distribuidora = new \Instalaciones\Model\Entity\Distribuidoras();
        $this->distribuidora->setIddistribuidora((isset($data['id_distribuidora'])) ? $data['id_distribuidora'] : null);
        $this->distribuidora->setNombre((isset($data['nombre'])) ? $data['nombre'] : null);
        $this->distribuidora->setDireccion((isset($data['direccion'])) ? $data['direccion'] : null);
        $this->distribuidora->setTelefono((isset($data['telefono'])) ? $data['telefono'] : null);
        
        
        /*COMUNA*/
        $this->comuna = new \Avisos\Model\Entity\Comuna(); 
        $this->comuna->setProvincias_id_provincia((isset($data['provincias_id_provincia'])) ? $data['provincias_id_provincia'] : null);
        /*PROVINCIA*/
        $this->provincia = new \Avisos\Model\Entity\Provincia();
        $this->provincia->setRegiones_id_region((isset($data['regiones_id_region'])) ? $data['regiones_id_region'] : null); 
        /*REGION*/
        //$this->region = new \Avisos\Model\Entity\Region();
      
        
        /*DATOS CAPTURADOS POR EL SERVICIO*/
//        $this->datos_adjuntos = new \Riat\Model\Entity\DatosAdjuntos();
//        $this->datos_adjuntos->setData_perfil_carga((isset($data['data_perfil_carga'])) ? $data['data_perfil_carga'] : null);
//        $this->datos_adjuntos->setDiagrama_fasorial((isset($data['diagrama_fasorial'])) ? $data['diagrama_fasorial'] : null);
//        $this->datos_adjuntos->setFotos_empalme((isset($data['fotos_empalme'])) ? $data['fotos_empalme'] : null);
  
        $this->calendariomedidor = new \Riat\Model\Entity\CalendarioMedidor();
        $this->calendariomedidor->setFecha_encontrada_medidor((isset($data['fecha_encontrada_medidor'])) ? $data['fecha_encontrada_medidor'] : null);
        $this->calendariomedidor->setFecha_oficial_pais((isset($data['fecha_oficial_pais'])) ? $data['fecha_oficial_pais'] : null);
        $this->calendariomedidor->setFecha_dejada_medidor((isset($data['fecha_dejada_medidor'])) ? $data['fecha_dejada_medidor'] : null);
        
        $this->inspeccionvisualempalme = new \Riat\Model\Entity\InspeccionVisualEmpalme();
        $this->inspeccionvisualempalme->setAcometida_empalme((isset($data['acometida_empalme'])) ? $data['acometida_empalme'] : null);
        $this->inspeccionvisualempalme->setLimitador_potencia((isset($data['limitador_potencia'])) ? $data['limitador_potencia'] : null);
        $this->inspeccionvisualempalme->setMedidor((isset($data['medidor'])) ? $data['medidor'] : null);
        $this->inspeccionvisualempalme->setAlambrado_medidor_regleta((isset($data['alambrado_medidor_regleta'])) ? $data['alambrado_medidor_regleta'] : null);
        $this->inspeccionvisualempalme->setEstado_regleta_block((isset($data['estado_regleta_block'])) ? $data['estado_regleta_block'] : null);
        $this->inspeccionvisualempalme->setCaja_medidor((isset($data['caja_medidor'])) ? $data['caja_medidor'] : null);
        $this->inspeccionvisualempalme->setCaja_ttcc((isset($data['caja_ttcc'])) ? $data['caja_ttcc'] : null);
        $this->inspeccionvisualempalme->setEstado_equipo_compacto((isset($data['estado_equipo_compacto'])) ? $data['estado_equipo_compacto'] : null);
        $this->inspeccionvisualempalme->setEstado_interruptor_reloj((isset($data['estado_interruptor_reloj'])) ? $data['estado_interruptor_reloj'] : null);
        $this->inspeccionvisualempalme->setConexion_tierra_proteccion((isset($data['conexion_tierra_proteccion'])) ? $data['conexion_tierra_proteccion'] : null);
                
        $this->diagrama  = new \Riat\Model\Entity\Diagrama();
        $this->diagrama->setElemento_v1((isset($data['elemento_v1'])) ? $data['elemento_v1'] : null);
        $this->diagrama->setElemento_v2((isset($data['elemento_v2'])) ? $data['elemento_v2'] : null);
        $this->diagrama->setElemento_v3((isset($data['elemento_v3'])) ? $data['elemento_v3'] : null);
        $this->diagrama->setSecuencia((isset($data['secuencia'])) ? $data['secuencia'] : null);
        $this->diagrama->setFlujo_potencia((isset($data['flujo_potencia'])) ? $data['flujo_potencia'] : null);
      
        $this->razonencontrada  = new \Riat\Model\Entity\RazonEncontrada();
        $this->razonencontrada->setRazon_tc_calculada((isset($data['razon_tc_calculada'])) ? $data['razon_tc_calculada'] : null);
        $this->razonencontrada->setRazon_tp_placa((isset($data['razon_tp_placa'])) ? $data['razon_tp_placa'] : null);
        $this->razonencontrada->setConstante_facturacion((isset($data['constante_facturacion'])) ? $data['constante_facturacion'] : null);
        
        $this->origenafectacionalamedida  = new \Riat\Model\Entity\OrigenAfectacionAlamedida();
        $this->origenafectacionalamedida->setOrigen_afectacion((isset($data['origen_afectacion'])) ? $data['origen_afectacion'] : null);
        $this->origenafectacionalamedida->setComentario((isset($data['comentario'])) ? $data['comentario'] : null);
        
        $this->lecturamedidorinstalado = new \Riat\Model\Entity\LecturaMedidorInstalado(); 
        $this->lecturamedidorinstalado->setTipo_elemento_id_tipo((isset($data['tipo_elemento_id_tipo'])) ? $data['tipo_elemento_id_tipo'] : null);
        $this->lecturamedidorinstalado->setKwh((isset($data['kwh'])) ? $data['kwh'] : null);
        $this->lecturamedidorinstalado->setKvar((isset($data['kvar'])) ? $data['kvar'] : null);
        $this->lecturamedidorinstalado->setKw_punta((isset($data['kw_punta'])) ? $data['kw_punta'] : null);
        $this->lecturamedidorinstalado->setKw_sum((isset($data['kw_sum'])) ? $data['kw_sum'] : null);       
        
        
        /*DATOS INSTALACIONES*/
        $this->antecedentesempalme = new \Instalaciones\Model\Entity\AntecedentesEmpalme();
        $this->antecedentesempalme->setTipo_empalme((isset($data['tipo_empalme'])) ? $data['tipo_empalme'] : null);
        $this->antecedentesempalme->setAcometida((isset($data['acometida'])) ? $data['acometida'] : null);
        $this->antecedentesempalme->setTipo_circuito((isset($data['tipo_circuito'])) ? $data['tipo_circuito'] : null);
        $this->antecedentesempalme->setConexion_circuito((isset($data['conexion_circuito'])) ? $data['conexion_circuito'] : null);
        $this->antecedentesempalme->setMedida((isset($data['medida'])) ? $data['medida'] : null);
		$this->antecedentesempalme->setVoltaje((isset($data['voltaje'])) ? $data['voltaje'] : null);
		
        
        /*$this->reloj = new \Instalaciones\Model\Entity\Reloj();
        $this->reloj->setCodigo_modelo_reloj((isset($data['codigo_modelo_reloj'])) ? $data['codigo_modelo_reloj'] : null);
        $this->reloj->setNumero_seria_reloj((isset($data['numero_seria_reloj'])) ? $data['numero_seria_reloj'] : null);
        $this->reloj->setMarca_reloj((isset($data['marca_reloj'])) ? $data['marca_reloj'] : null);*/
        
        $this->regleta = new \Instalaciones\Model\Entity\Regletas();
        $this->regleta->setMarca_regleta((isset($data['marca_regleta'])) ? $data['marca_regleta'] : null);
        $this->regleta->setModelo_regleta((isset($data['modelo_regleta'])) ? $data['modelo_regleta'] : null);
        $this->regleta->setNumero_terminales_regleta((isset($data['numero_terminales_regleta'])) ? $data['numero_terminales_regleta'] : null);
        $this->regleta->setPuentes_corrientes((isset($data['puentes_corrientes'])) ? $data['puentes_corrientes'] : null);
        $this->regleta->setPuentes_voltajes((isset($data['puentes_voltajes'])) ? $data['puentes_voltajes'] : null);
        
        /*$this->telemedida  = new \Instalaciones\Model\Entity\Telemedida();
        $this->telemedida->setModem((isset($data['modem'])) ? $data['modem'] : null);
        $this->telemedida->setAlim_auxiliar((isset($data['alim_auxiliar'])) ? $data['alim_auxiliar'] : null);
        $this->telemedida->setNum_serie((isset($data['num_serie'])) ? $data['num_serie'] : null);*/

        $this->ecm = new \Instalaciones\Model\Entity\Ecm();
        $this->ecm->setInstalaciones_id_instalacion((isset($data['instalaciones_id_instalacion'])) ? $data['instalaciones_id_instalacion'] : null);
        $this->ecm->setModelo((isset($data['modelo'])) ? $data['modelo'] : null);
         $this->ecm->setMarca((isset($data['marca'])) ? $data['marca'] : null);
        $this->ecm->setSerie((isset($data['serie'])) ? $data['serie'] : null);
        $this->ecm->setAnio((isset($data['anio'])) ? $data['anio'] : null);
        $this->ecm->setCantidad_elementos((isset($data['cantidad_elementos'])) ? $data['cantidad_elementos'] : null);
        
        $this->ecm->setIp1((isset($data['ip1'])) ? $data['ip1'] : null);
        $this->ecm->setIp2((isset($data['ip2'])) ? $data['ip2'] : null);
        $this->ecm->setIp3((isset($data['ip3'])) ? $data['ip3'] : null);
        $this->ecm->setIp4((isset($data['ip4'])) ? $data['ip4'] : null);
        
        $this->ecm->setIs_ecm((isset($data['is_ecm'])) ? $data['is_ecm'] : null);
        
        $this->ecm->setVs((isset($data['vs'])) ? $data['vs'] : null);
        $this->ecm->setVp((isset($data['vp'])) ? $data['vp'] : null);
        
        $this->ecm->setTtcc1((isset($data['ttcc1'])) ? $data['ttcc1'] : null);
        $this->ecm->setTtpp1((isset($data['ttpp1'])) ? $data['ttpp1'] : null);
        $this->ecm->setTtcc2((isset($data['ttcc2'])) ? $data['ttcc2'] : null);
        $this->ecm->setTtpp2((isset($data['ttpp2'])) ? $data['ttpp2'] : null);
        
        $this->sistemadistribucion = new \Instalaciones\Model\Entity\SistemaDistribucion();
        $this->sistemadistribucion->setPotencia_ssee((isset($data['potencia_ssee'])) ? $data['potencia_ssee'] : null);
        $this->sistemadistribucion->setNum_ssee((isset($data['num_ssee'])) ? $data['num_ssee'] : null);
        $this->sistemadistribucion->setAlimentador((isset($data['alimentador'])) ? $data['alimentador'] : null);
        
        
        
        /*CONTACTO AUTORIZA*/
        $this->contactoautoriza = new \Riat\Model\Entity\ContactoAutoriza();
        $this->contactoautoriza->setNombre_contacto((isset($data['nombre_contacto'])) ? $data['nombre_contacto'] : null);
        $this->contactoautoriza->setRun((isset($data['run'])) ? $data['run'] : null);
        $this->contactoautoriza->setFono((isset($data['fono'])) ? $data['fono'] : null);
        $this->contactoautoriza->setEmail((isset($data['email'])) ? $data['email'] : null);        
        
        /*PERSONA QUE DIGITO EL RIAT*/
        $this->digitador_persona = new \Personal\Model\Entity\Persona;
        $this->digitador_persona->setNombres((isset($data['nombres_dig'])) ? $data['nombres_dig'] : null);
        $this->digitador_persona->setApellidos((isset($data['apellidos_dig'])) ? $data['apellidos_dig'] : null);
        $this->digitador_persona->setEmail((isset($data['email_dig'])) ? $data['email_dig'] : null);
        $this->digitador_persona->setRut_persona((isset($data['rut_persona_dig'])) ? $data['rut_persona_dig'] : null);        
        $this->digitador_persona->setFoto_carnet((isset($data['foto_carnet_dig'])) ? $data['foto_carnet_dig'] : null); 
        
        /*PERSONA TECNICO EJECUTA EL RIAT*/
        $this->tecnico_persona = new \Personal\Model\Entity\Persona;
        $this->tecnico_persona->setNombres((isset($data['nombres_tec'])) ? $data['nombres_tec'] : null);
        $this->tecnico_persona->setApellidos((isset($data['apellidos_tec'])) ? $data['apellidos_tec'] : null);
        $this->tecnico_persona->setEmail((isset($data['email_tec'])) ? $data['email_tec'] : null);
        $this->tecnico_persona->setRut_persona((isset($data['rut_persona_tec'])) ? $data['rut_persona_tec'] : null);        
        $this->tecnico_persona->setFoto_carnet((isset($data['foto_carnet_tec'])) ? $data['foto_carnet_tec'] : null); 
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

