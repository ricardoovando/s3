<?php

namespace Riat\Model\Entity;

class PotenciaMedidorInduccion {

    private $id_ors_riat;
    private $tipo_medidor;
    private $n_revol;
    private $tiempo_segundos;
    private $potencia_calculada;              

    function __construct($id_ors_riat = null, $tipo_medidor = null, $n_revol = null, $tiempo_segundos = null, $potencia_calculada  = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->tipo_medidor = $tipo_medidor;
        $this->n_revol = $n_revol;
        $this->tiempo_segundos = $tiempo_segundos;
        $this->potencia_calculada = $potencia_calculada;
    }

        public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getTipo_medidor() {
        return $this->tipo_medidor;
    }

    public function setTipo_medidor($tipo_medidor) {
        $this->tipo_medidor = $tipo_medidor;
    }

    public function getN_revol() {
        return $this->n_revol;
    }

    public function setN_revol($n_revol) {
        $this->n_revol = $n_revol;
    }

    public function getTiempo_segundos() {
        return $this->tiempo_segundos;
    }

    public function setTiempo_segundos($tiempo_segundos) {
        $this->tiempo_segundos = $tiempo_segundos;
    }

    public function getPotencia_calculada() {
        return $this->potencia_calculada;
    }

    public function setPotencia_calculada($potencia_calculada) {
        $this->potencia_calculada = $potencia_calculada;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->tipo_medidor = (isset($data['tipo_medidor'])) ? $data['tipo_medidor'] : null;
        $this->n_revol = (isset($data['n_revol'])) ? $data['n_revol'] : null;
        $this->tiempo_segundos = (isset($data['tiempo_segundos'])) ? $data['tiempo_segundos'] : null;
        $this->potencia_calculada = (isset($data['potencia_calculada'])) ? $data['potencia_calculada'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

