<?php

namespace Riat\Model\Entity;

class RazonEncontrada {
    
    private $ors_riat_id_ors_riat;
    private $razon_tc_calculada;
    private $razon_tp_placa;
    private $constante_facturacion;
              
    public function __construct($ors_riat_id_ors_riat = null, $razon_tc_calculada = null, $razon_tp_placa = null , $constante_facturacion = null) {
        $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
        $this->razon_tc_calculada = $razon_tc_calculada;
        $this->razon_tp_placa = $razon_tp_placa;
        $this->constante_facturacion = $constante_facturacion;
      }

    public function getOrs_riat_id_ors_riat() {
          return $this->ors_riat_id_ors_riat;
    }

    public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
          $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
    }

    public function getRazon_tc_calculada() {
        return $this->razon_tc_calculada;
    }

    public function setRazon_tc_calculada($razon_tc_calculada) {
        $this->razon_tc_calculada = $razon_tc_calculada;
    }

    public function getRazon_tp_placa() {
        return $this->razon_tp_placa;
    }

    public function setRazon_tp_placa($razon_tp_placa) {
        $this->razon_tp_placa = $razon_tp_placa;
    }

    public function getConstante_facturacion() {
        return $this->constante_facturacion;
    }

    public function setConstante_facturacion($constante_facturacion) {
        $this->constante_facturacion = $constante_facturacion;
    }

    
    public function exchangeArray($data) {
        $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
        $this->razon_tc_calculada = (isset($data['razon_tc_calculada'])) ? $data['razon_tc_calculada'] : null;
        $this->razon_tp_placa = (isset($data['razon_tp_placa'])) ? $data['razon_tp_placa'] : null;
        $this->constante_facturacion = (isset($data['constante_facturacion'])) ? $data['constante_facturacion'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

