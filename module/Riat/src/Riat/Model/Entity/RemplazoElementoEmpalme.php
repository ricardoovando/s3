<?php

namespace Riat\Model\Entity;

class RemplazoElementoEmpalme {

  private $id_ors_riat;
  private $tipo_elementos_idtipo;
  private $numero_serie;
  private $marca;
  private $codigo_modelo;
  private $propiedad;
    
  function __construct($id_ors_riat = null, $tipo_elementos_idtipo = null, $numero_serie = null, $marca = null, $codigo_modelo = null, $propiedad = null) {
      $this->id_ors_riat = $id_ors_riat;
      $this->tipo_elementos_idtipo = $tipo_elementos_idtipo;
      $this->numero_serie = $numero_serie;
      $this->marca = $marca;
      $this->codigo_modelo = $codigo_modelo;
      $this->propiedad = $propiedad;
  }
  
  public function getId_ors_riat() {
      return $this->id_ors_riat;
  }

  public function setId_ors_riat($id_ors_riat) {
      $this->id_ors_riat = $id_ors_riat;
  }

  public function getTipo_elementos_idtipo() {
      return $this->tipo_elementos_idtipo;
  }

  public function setTipo_elementos_idtipo($tipo_elementos_idtipo) {
      $this->tipo_elementos_idtipo = $tipo_elementos_idtipo;
  }

  public function getNumero_serie() {
      return $this->numero_serie;
  }

  public function setNumero_serie($numero_serie) {
      $this->numero_serie = $numero_serie;
  }

  public function getMarca() {
      return $this->marca;
  }

  public function setMarca($marca) {
      $this->marca = $marca;
  }

  public function getCodigo_modelo() {
      return $this->codigo_modelo;
  }

  public function setCodigo_modelo($codigo_modelo) {
      $this->codigo_modelo = $codigo_modelo;
  }

  public function getPropiedad() {
      return $this->propiedad;
  }

  public function setPropiedad($propiedad) {
      $this->propiedad = $propiedad;
  }

  public function exchangeArray($data) {
      $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
      $this->tipo_elementos_idtipo = (isset($data['tipo_elementos_idtipo'])) ? $data['tipo_elementos_idtipo'] : null;
      $this->numero_serie = (isset($data['numero_serie'])) ? $data['numero_serie'] : null;
      $this->marca = (isset($data['marca'])) ? $data['marca'] : null;
      $this->codigo_modelo = (isset($data['codigo_modelo'])) ? $data['codigo_modelo'] : null;
      $this->propiedad = (isset($data['propiedad'])) ? $data['propiedad'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

