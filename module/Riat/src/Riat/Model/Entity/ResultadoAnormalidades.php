<?php

namespace Riat\Model\Entity;

class ResultadoAnormalidades {
  
  private $id_ors_riat;
  private $codigo_anormalidad;
  private $origen;
  private $tratamiento;
  private $mot_no_normalizado;
  
  function __construct($id_ors_riat = null, $codigo_anormalidad = null, $origen = null, $tratamiento = null, $mot_no_normalizado = null) {
  	
        $this->id_ors_riat = $id_ors_riat;
        $this->codigo_anormalidad = $codigo_anormalidad;
        $this->origen = $origen;
        $this->tratamiento = $tratamiento;
        $this->mot_no_normalizado = $mot_no_normalizado;
  	  
  }

  public function getOrs_riat_id_ors_riat() {
      return $this->ors_riat_id_ors_riat;
  }

  public function setOrs_riat_id_ors_riat($ors_riat_id_ors_riat) {
      $this->ors_riat_id_ors_riat = $ors_riat_id_ors_riat;
  }

  public function getData_perfil_carga() {
      return $this->data_perfil_carga;
  }

  public function setData_perfil_carga($data_perfil_carga) {
      $this->data_perfil_carga = $data_perfil_carga;
  }

  public function getDiagrama_fasorial() {
      return $this->diagrama_fasorial;
  }

  public function setDiagrama_fasorial($diagrama_fasorial) {
      $this->diagrama_fasorial = $diagrama_fasorial;
  }

  public function getFotos_empalme() {
      return $this->fotos_empalme;
  }

  public function setFotos_empalme($fotos_empalme) {
      $this->fotos_empalme = $fotos_empalme;
  }

  public function exchangeArray($data) {
      $this->ors_riat_id_ors_riat = (isset($data['ors_riat_id_ors_riat'])) ? $data['ors_riat_id_ors_riat'] : null;
      $this->data_perfil_carga = (isset($data['data_perfil_carga'])) ? $data['data_perfil_carga'] : null;
      $this->diagrama_fasorial = (isset($data['diagrama_fasorial'])) ? $data['diagrama_fasorial'] : null;
      $this->fotos_empalme = (isset($data['fotos_empalme'])) ? $data['fotos_empalme'] : null;
    }

  public function getArrayCopy() {
        return get_object_vars($this);
    }

}

