<?php

namespace Riat\Model\Entity;

class ResultadoInspeccionAuditoria {

    private $id_ors_riat;
    private $cods_result_inspec_audit_idcods_result_inspec_audit;
    private $resultado_inspeccion_terreno;
    
    public function __construct( $id_ors_riat = null, $cods_result_inspec_audit_idcods_result_inspec_audit = null, $resultado_inspeccion_terreno = null ) {
      $this->id_ors_riat = $id_ors_riat;
      $this->cods_result_inspec_audit_idcods_result_inspec_audit = $cods_result_inspec_audit_idcods_result_inspec_audit;
      $this->resultado_inspeccion_terreno = $resultado_inspeccion_terreno;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getCods_result_inspec_audit_idcods_result_inspec_audit() {
        return $this->cods_result_inspec_audit_idcods_result_inspec_audit;
    }

    public function setCods_result_inspec_audit_idcods_result_inspec_audit($cods_result_inspec_audit_idcods_result_inspec_audit) {
        $this->cods_result_inspec_audit_idcods_result_inspec_audit = $cods_result_inspec_audit_idcods_result_inspec_audit;
    }

    public function getResultado_inspeccion_terreno() {
        return $this->resultado_inspeccion_terreno;
    }

    public function setResultado_inspeccion_terreno($resultado_inspeccion_terreno) {
        $this->resultado_inspeccion_terreno = $resultado_inspeccion_terreno;
    }
        
    public function exchangeArray($data) {
      $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
      $this->cods_result_inspec_audit_idcods_result_inspec_audit = (isset($data['cods_result_inspec_audit_idcods_result_inspec_audit'])) ? $data['cods_result_inspec_audit_idcods_result_inspec_audit'] : null;
      $this->resultado_inspeccion_terreno = (isset($data['resultado_inspeccion_terreno'])) ? $data['resultado_inspeccion_terreno'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

