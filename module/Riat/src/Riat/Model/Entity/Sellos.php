<?php

namespace Riat\Model\Entity;

class Sellos {

    private $id_ors_riat;
    private $encontrado_dejado;
    private $cupula_medidor;
    private $block_medidor;
    private $reset_medidor;
    private $bateria_medidor;
    private $optico_medidor;
    private $regleta_prueba;
    private $int_horario;
    private $caja_medidor;
    private $ttcc;
    private $caja_ttcc;
    private $caja_ecm;

    public function __construct($id_ors_riat = null, $encontrado_dejado = null, $cupula_medidor = null, $block_medidor = null, $reset_medidor = null, $bateria_medidor = null, $optico_medidor = null, $regleta_prueba = null, $int_horario = null, $caja_medidor = null, $ttcc = null, $caja_ttcc = null, $caja_ecm = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->encontrado_dejado = $encontrado_dejado;
        $this->cupula_medidor = $cupula_medidor;
        $this->block_medidor = $block_medidor;
        $this->reset_medidor = $reset_medidor;
        $this->bateria_medidor = $bateria_medidor;
        $this->optico_medidor = $optico_medidor;
        $this->regleta_prueba = $regleta_prueba;
        $this->int_horario = $int_horario;
        $this->caja_medidor = $caja_medidor;
        $this->ttcc = $ttcc;
        $this->caja_ttcc = $caja_ttcc;
        $this->caja_ecm = $caja_ecm;
    }
    
    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getEncontrado_dejado() {
        return $this->encontrado_dejado;
    }

    public function setEncontrado_dejado($encontrado_dejado) {
        $this->encontrado_dejado = $encontrado_dejado;
    }

    public function getCupula_medidor() {
        return $this->cupula_medidor;
    }

    public function setCupula_medidor($cupula_medidor) {
        $this->cupula_medidor = $cupula_medidor;
    }

    public function getBlock_medidor() {
        return $this->block_medidor;
    }

    public function setBlock_medidor($block_medidor) {
        $this->block_medidor = $block_medidor;
    }

    public function getReset_medidor() {
        return $this->reset_medidor;
    }

    public function setReset_medidor($reset_medidor) {
        $this->reset_medidor = $reset_medidor;
    }

    public function getBateria_medidor() {
        return $this->bateria_medidor;
    }

    public function setBateria_medidor($bateria_medidor) {
        $this->bateria_medidor = $bateria_medidor;
    }

    public function getOptico_medidor() {
        return $this->optico_medidor;
    }

    public function setOptico_medidor($optico_medidor) {
        $this->optico_medidor = $optico_medidor;
    }

    public function getRegleta_prueba() {
        return $this->regleta_prueba;
    }

    public function setRegleta_prueba($regleta_prueba) {
        $this->regleta_prueba = $regleta_prueba;
    }

    public function getInt_horario() {
        return $this->int_horario;
    }

    public function setInt_horario($int_horario) {
        $this->int_horario = $int_horario;
    }

    public function getCaja_medidor() {
        return $this->caja_medidor;
    }

    public function setCaja_medidor($caja_medidor) {
        $this->caja_medidor = $caja_medidor;
    }

    public function getTtcc() {
        return $this->ttcc;
    }

    public function setTtcc($ttcc) {
        $this->ttcc = $ttcc;
    }

    public function getCaja_ttcc() {
        return $this->caja_ttcc;
    }

    public function setCaja_ttcc($caja_ttcc) {
        $this->caja_ttcc = $caja_ttcc;
    }

    public function getCaja_ecm() {
        return $this->caja_ecm;
    }

    public function setCaja_ecm($caja_ecm) {
        $this->caja_ecm = $caja_ecm;
    }

    
    public function exchangeArray($data) {

        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->encontrado_dejado = (isset($data['encontrado_dejado'])) ? $data['encontrado_dejado'] : null;
        $this->cupula_medidor = (isset($data['cupula_medidor'])) ? $data['cupula_medidor'] : null;
        $this->block_medidor = (isset($data['block_medidor'])) ? $data['block_medidor'] : null;
        $this->reset_medidor = (isset($data['reset_medidor'])) ? $data['reset_medidor'] : null;
        $this->bateria_medidor = (isset($data['bateria_medidor'])) ? $data['bateria_medidor'] : null;
        $this->optico_medidor = (isset($data['optico_medidor'])) ? $data['optico_medidor'] : null;
        $this->regleta_prueba = (isset($data['regleta_prueba'])) ? $data['regleta_prueba'] : null;
        $this->int_horario = (isset($data['int_horario'])) ? $data['int_horario'] : null;
        $this->caja_medidor = (isset($data['caja_medidor'])) ? $data['caja_medidor'] : null;
        $this->ttcc = (isset($data['ttcc'])) ? $data['ttcc'] : null;
        $this->caja_ttcc = (isset($data['caja_ttcc'])) ? $data['caja_ttcc'] : null;
        $this->caja_ecm = (isset($data['caja_ecm'])) ? $data['caja_ecm'] : null;
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

