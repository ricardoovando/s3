<?php

namespace Riat\Model\Entity;

class ValorTotalPotencias {

    private $id_ors_riat;
    private $tipo_registro;
    private $potencia_activa_sec_kw;
    private $potencia_activa_prim_kw;
    private $potencia_reactiva_kvar;
    private $cos_fi;

    public function __construct($id_ors_riat = null, $tipo_registro = null, $potencia_activa_sec_kw = null, $potencia_activa_prim_kw = null, $potencia_reactiva_kvar = null, $cos_fi = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->tipo_registro = $tipo_registro;
        $this->potencia_activa_sec_kw = $potencia_activa_sec_kw;
        $this->potencia_activa_prim_kw = $potencia_activa_prim_kw;
        $this->potencia_reactiva_kvar = $potencia_reactiva_kvar;
        $this->cos_fi = $cos_fi;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getTipo_registro() {
        return $this->tipo_registro;
    }

    public function setTipo_registro($tipo_registro) {
        $this->tipo_registro = $tipo_registro;
    }

    public function getPotencia_activa_sec_kw() {
        return $this->potencia_activa_sec_kw;
    }

    public function setPotencia_activa_sec_kw($potencia_activa_sec_kw) {
        $this->potencia_activa_sec_kw = $potencia_activa_sec_kw;
    }

    public function getPotencia_activa_prim_kw() {
        return $this->potencia_activa_prim_kw;
    }

    public function setPotencia_activa_prim_kw($potencia_activa_prim_kw) {
        $this->potencia_activa_prim_kw = $potencia_activa_prim_kw;
    }

    public function getPotencia_reactiva_kvar() {
        return $this->potencia_reactiva_kvar;
    }

    public function setPotencia_reactiva_kvar($potencia_reactiva_kvar) {
        $this->potencia_reactiva_kvar = $potencia_reactiva_kvar;
    }

    public function getCos_fi() {
        return $this->cos_fi;
    }

    public function setCos_fi($cos_fi) {
        $this->cos_fi = $cos_fi;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->tipo_registro = (isset($data['tipo_registro'])) ? $data['tipo_registro'] : null;
        $this->potencia_activa_sec_kw = (isset($data['potencia_activa_sec_kw'])) ? $data['potencia_activa_sec_kw'] : null;
        $this->potencia_activa_prim_kw = (isset($data['potencia_activa_prim_kw'])) ? $data['potencia_activa_prim_kw'] : null;
        $this->potencia_reactiva_kvar = (isset($data['potencia_reactiva_kvar'])) ? $data['potencia_reactiva_kvar'] : null;
        $this->cos_fi = (isset($data['cos_fi'])) ? $data['cos_fi'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

