<?php

namespace Riat\Model\Entity;

class ViajesFrustrados {

    private $id_ors_riat;
    private $cods_viaje_frustrado_idcods_viaje_frustrado;
    private $num_visita;
    private $fecha_visita;
    private $hora_visita;

    public function __construct($id_ors_riat = null, $cods_viaje_frustrado_idcods_viaje_frustrado = null, $num_visita = null, $fecha_visita = null, $hora_visita = null) {
        $this->id_ors_riat = $id_ors_riat;
        $this->cods_viaje_frustrado_idcods_viaje_frustrado = $cods_viaje_frustrado_idcods_viaje_frustrado;
        $this->num_visita = $num_visita;
        $this->fecha_visita = $fecha_visita;
        $this->hora_visita = $hora_visita;
    }

    public function getId_ors_riat() {
        return $this->id_ors_riat;
    }

    public function setId_ors_riat($id_ors_riat) {
        $this->id_ors_riat = $id_ors_riat;
    }

    public function getCods_viaje_frustrado_idcods_viaje_frustrado() {
        return $this->cods_viaje_frustrado_idcods_viaje_frustrado;
    }

    public function setCods_viaje_frustrado_idcods_viaje_frustrado($cods_viaje_frustrado_idcods_viaje_frustrado) {
        $this->cods_viaje_frustrado_idcods_viaje_frustrado = $cods_viaje_frustrado_idcods_viaje_frustrado;
    }

    public function getNum_visita() {
        return $this->num_visita;
    }

    public function setNum_visita($num_visita) {
        $this->num_visita = $num_visita;
    }

    public function getFecha_visita() {
        return $this->fecha_visita;
    }

    public function setFecha_visita($fecha_visita) {
        $this->fecha_visita = $fecha_visita;
    }

    public function getHora_visita() {
        return $this->hora_visita;
    }

    public function setHora_visita($hora_visita) {
        $this->hora_visita = $hora_visita;
    }

    public function exchangeArray($data) {
        $this->id_ors_riat = (isset($data['id_ors_riat'])) ? $data['id_ors_riat'] : null;
        $this->cods_viaje_frustrado_idcods_viaje_frustrado = (isset($data['cods_viaje_frustrado_idcods_viaje_frustrado'])) ? $data['cods_viaje_frustrado_idcods_viaje_frustrado'] : null;
        $this->num_visita = (isset($data['num_visita'])) ? $data['num_visita'] : null;
        $this->fecha_visita = (isset($data['fecha_visita'])) ? $data['fecha_visita'] : null;
        $this->hora_visita = (isset($data['hora_visita'])) ? $data['hora_visita'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

