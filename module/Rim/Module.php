<?php

namespace Rim;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
//use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

/*Entity*/
use Rim\Model\Dao\OrsRimDao;
use Rim\Model\Entity\OrsRim;
use Rim\Model\Dao\ejecutadosDao;
use Rim\Model\Entity\ejecutados;

class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, /*ServiceProviderInterface,*/ ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm -> getEventManager();
        $sharedEvents = $events -> getSharedManager();
        $sharedEvents -> attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        $sharedEvents -> attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
    }

    public function initAuth(MvcEvent $e) {
        $application = $e -> getApplication();
        $matches = $e -> getRouteMatch();
        $controller = $matches -> getParam('controller');
        $action = $matches -> getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Rim\Controller\Index" :
                    if (in_array($action, array('b2a553d36975dda502e24014d2161a27'))) {
                        return;
                    }
                    break;
            }

            $sm = $application -> getServiceManager();

            $auth = $sm -> get('Admin\Model\Login');

            if (!$auth -> isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e -> getTarget();
                return $controller -> redirect() -> toRoute('home');
            }
        }
    }

    public function initConfig(MvcEvent $e) {
        $application = $e -> getApplication();
        $services = $application -> getServiceManager();

        $services -> setFactory('ConfigIniRim', function($services) {
            $reader = new Ini();
            $data = $reader -> fromFile(__DIR__ . '/config/config.ini');
            return $data;
        });
    }

    public function getAutoloaderConfig() {
        return array('Zend\Loader\ClassMapAutoloader' => array(__DIR__ . '/autoload_classmap.php', ), 'Zend\Loader\StandardAutoloader' => array('namespaces' => array(
        // if we're in a namespace deeper than one level we need to fix the \ in the path
        __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__), ), ), );
    }

    public function getConfig() {
        return
        include __DIR__ . '/config/module.config.php';
    }

    /*Inyecccion de dependencia para el dao se carga la tabla de base de datos segun se indique*/
    public function getServiceConfig() {
        return array('factories' => array('Rim\Model\OrsRimDao' => function($sm) {
            $tableGateway = $sm -> get('TableGatewayOrsRim');
            $login = $sm -> get('Admin\Model\Login');
            $dbAdapter = $sm -> get('dbAdapter');
            $dao = new OrsRimDao($tableGateway, $login, $dbAdapter);
            return $dao;
        }, 'Rim\Model\ejecutadosDao' => function($sm) {
            $tableGateway = $sm -> get('TableGatewayEjecutados');
            $login = $sm -> get('Admin\Model\Login');
            $dbAdapter = $sm -> get('dbAdapter');
            $dao = new ejecutadosDao($tableGateway, $login, $dbAdapter);
            return $dao;
        },
        /** TABLAS **/
        'TableGatewayOrsRim' => function($sm) {
            $dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype -> setArrayObjectPrototype(new OrsRim());
            return new TableGateway('ors_rim', $dbAdapter, null, $resultSetPrototype);
        }, 'TableGatewayEjecutados' => function($sm) {
            $dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype -> setArrayObjectPrototype(new ejecutados());
            return new TableGateway('rim_ejecutados_tablet', $dbAdapter, null, $resultSetPrototype);
        }, ), );
    }

    /*Inyeccion de dependencia para el controlador se carga el objeto modelo usuario dao*/
    public function getControllerConfig() {
        return array('factories' => array('Rim\Controller\Index' => function($sm) {

            $locator = $sm -> getServiceLocator();

            $logger = $locator -> get('Zend\Log\GetTabletData');
            $config = $locator -> get('ConfigIniRim');
            $orsrimdao = $locator -> get('Rim\Model\OrsRimDao');
            $avisoDao = $locator -> get('Avisos\Model\AvisoDao');
            $ejecutadosdao = $locator -> get('Rim\Model\ejecutadosDao');

            //$DistribuidorasDao = $locator->get('Instalaciones\Model\DistribuidorasDao');
            //$InstalacionDao = $locator->get('Instalaciones\Model\InstalacionDao');
            //$MedidorInstalacionDao = $locator->get('Instalaciones\Model\MedidorInstalacionDao');
            //$ModelosMedidoresDao = $locator->get('Instalaciones\Model\ModelosMedidoresDao');

            $controller = new \Rim\Controller\IndexController($config);

            /*DATOS RIM*/
            $controller -> setOrsRimDao($orsrimdao);

            /*TABLA AVISO*/
            $controller -> setavisoDao($avisoDao);

            /*DESDE TABLET*/
            $controller -> setejecutadosDao($ejecutadosdao);

            /*LOG DE TODOS LOS REGISTROS QUE LLEGAN*/
            $controller -> setLogger($logger);

            /*TABLAS INSTALACION*/
            //$controller->setDistribuidorasDao($DistribuidorasDao);
            //$controller->setInstalacionDao($InstalacionDao);
            //$controller->setMedidorInstalacionDao($MedidorInstalacionDao);
            //$controller->setModelosMedidoresDao($ModelosMedidoresDao);

            return $controller;

        }, ));
    }

}
