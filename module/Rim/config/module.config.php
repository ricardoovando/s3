<?php

return array(
    'controllers' => array(
        'invokables' => array(
            //'Avisos\Controller\Cargar' => 'Avisos\Controller\CargarController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'rim' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/rim[/:controller][/:action][/:id]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Rim\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
            ),
          'paginatorrim' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rim[/:controller][/:action]/page[/:page]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Rim\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'page' => 1,
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Rim' => __DIR__ . '/../view',
        ),
        'strategies' => array(
          'ViewJsonStrategy', 
        ),
    ),
);
