<?php

namespace Rim\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/* RIM */
use Rim\Model\Dao\OrsRimDao;
use Rim\Model\Entity\OrsRim;

use Rim\Model\Dao\ejecutadosDao;
use Rim\Model\Entity\ejecutados;

use Riat\Model\Dao\OrsRiatDao;
use Riat\Model\Entity\OrsRiat;

/* AVISOS */
use Avisos\Model\Entity\Aviso;
use Avisos\Model\Dao\AvisoDao;


/* FORM */
use Rim\Form\RegistroRim;
use Rim\Form\RegistroRimValidator;


/* INSTALACIONES */

//use Instalaciones\Model\Dao\DistribuidorasDao;
//use Instalaciones\Model\Entity\Distribuidoras;
//use Instalaciones\Model\Dao\InstalacionDao;
//use Instalaciones\Model\Entity\Instalacion;

use DOMPDFModule\View\Model\PdfModel;

class IndexController extends AbstractActionController {

    private $config;
    private $login;
    private $logger;      

    /* DATOS RIM */
    private $OrsRimDao;
    private $ejecutadosDao;
   private $OrsRiatDao;

    /* AVISO */
    private $avisoDao;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }


   public function getOrsRiatDao() {

        if (!$this->OrsRiatDao) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Riat\Model\OrsRiatDao');
        }
        return $this->OrsRiatDao;
    }



    /*
     * 
     * RIM
     * 
     */

    private function getFormRegistroRim() {
        return new RegistroRim();
    }

    public function setOrsRimDao(OrsRimDao $orsrimdao) {
        $this->OrsRimDao = $orsrimdao;
    }

    public function getOrsRimDao() {
        return $this->OrsRimDao;
    }


    public function setejecutadosDao(ejecutadosDao $ejecutadosdao) {
        $this->ejecutadosDao = $ejecutadosdao;
    }

    public function getejecutadosDao() {
        return $this->ejecutadosDao;
    }

    /** AVISOS * */
    public function setavisoDao(AvisoDao $avisoDao) {
        $this->avisoDao = $avisoDao;
    }

    public function getavisoDao() {
        return $this->avisoDao;
    }

    /* REGISTRAR LOS DATOS QUE LLEGAN DESDE TABLET */

    public function setLogger($logger) {
        $this->logger = $logger;
    }

    public function getLogger() {
        return $this->logger;
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (empty($_SESSION['buscar_CM']))
            $_SESSION['buscar_CM'] = "";
        if (empty($_SESSION['buscar_INS']))
            $_SESSION['buscar_INS'] = "";
        if (empty($_SESSION['buscar_VCC']))
            $_SESSION['buscar_VCC'] = "";
        if (empty($_SESSION['buscar_VSC']))
            $_SESSION['buscar_VSC'] = "";
        if (empty($_SESSION['buscar_VF']))
            $_SESSION['buscar_VF'] = "";
        if (empty($_SESSION['buscar_REI']))
            $_SESSION['buscar_REI'] = "";
        if (empty($_SESSION['buscar_DEV']))
            $_SESSION['buscar_DEV'] = "";
        if (empty($_SESSION['buscar_AUTO']))
            $_SESSION['buscar_AUTO'] = "";
        if (empty($_SESSION['buscar_Normal']))
            $_SESSION['buscar_Normal'] = "";
        if (empty($_SESSION['buscar_CNR']))
            $_SESSION['buscar_CNR'] = "";
        if (empty($_SESSION['buscar_Subestandar']))
            $_SESSION['buscar_Subestandar'] = "";

        /*
         * Sino esta creada la variable de seccion se crea
         */

        if (empty($_SESSION['datosFiltran']))
            $_SESSION['datosFiltran'] = array();

        /* Si envia datos por medio de Post */
        if ($this->getRequest()->isPost()) {

            // Obtenemos los parámetros del formulario es similar a $_POST
            $postParams = $this->request->getPost();

            if ((!empty($postParams["buscar_numero"]) && (!empty($postParams["tipobusqueda"])))) {
                $_SESSION['datosFiltran']['buscar_numero'] = $postParams["buscar_numero"];
                $_SESSION['datosFiltran']['tipobusqueda'] = $postParams["tipobusqueda"];
            } else {
                unset($_SESSION['datosFiltran']['buscar_numero']);
                unset($_SESSION['datosFiltran']['tipobusqueda']);
            }

            if ((!empty($postParams["Normal"]))) {

                $_SESSION['datosFiltran']['buscar_por_resultado'] = 'NORMAL';
                $_SESSION['buscar_Normal'] = "active";
                $_SESSION['buscar_CNR'] = "";
                $_SESSION['buscar_Subestandar'] = "";
            } elseif ((!empty($postParams["CNR"]))) {

                $_SESSION['datosFiltran']['buscar_por_resultado'] = 'CNR';
                $_SESSION['buscar_Normal'] = "";
                $_SESSION['buscar_CNR'] = "active";
                $_SESSION['buscar_Subestandar'] = "";
            } elseif ((!empty($postParams["Subestandar"]))) {

                $_SESSION['datosFiltran']['buscar_por_resultado'] = 'Cond. Subestandar';
                $_SESSION['buscar_Normal'] = "";
                $_SESSION['buscar_CNR'] = "";
                $_SESSION['buscar_Subestandar'] = "active";
            }

            if ((!empty($postParams["buscar_CM"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 197;

                $_SESSION['buscar_CM'] = "active";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_INS"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 198;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "active";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_VCC"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 199;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "active";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_VSC"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 200;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "active";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_VF"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 201;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "active";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_REI"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 214;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "active";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_DEV"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 215;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "active";
                $_SESSION['buscar_AUTO'] = "";
            } elseif ((!empty($postParams["buscar_AUTO"]))) {

                $_SESSION['datosFiltran']['buscar_por_servicio_ejecutado'] = 216;

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "active";
            }

            if ($postParams["listartodos"]) {

                unset($_SESSION['datosFiltran']['buscar_por_resultado']);
                unset($_SESSION['datosFiltran']['buscar_por_servicio_ejecutado']);

                $_SESSION['buscar_Normal'] = "";
                $_SESSION['buscar_CNR'] = "";
                $_SESSION['buscar_Subestandar'] = "";

                $_SESSION['buscar_CM'] = "";
                $_SESSION['buscar_INS'] = "";
                $_SESSION['buscar_VCC'] = "";
                $_SESSION['buscar_VSC'] = "";
                $_SESSION['buscar_VF'] = "";
                $_SESSION['buscar_REI'] = "";
                $_SESSION['buscar_DEV'] = "";
                $_SESSION['buscar_AUTO'] = "";
            }
        }

        /* Filtrar solo por los estados Necesarios */
        $paginator = $this->getejecutadosDao()->obtenerTodos($_SESSION['datosFiltran']);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(10);

        /* Cantidades de Servicios Ejecutados */
        $CantidadServicios = $this->getejecutadosDao()->ObtenerCantidadServicios();

        /* Las Cantidad de Resultados Obtenidos de Terreno */
        $CantidadResultados = $this->getejecutadosDao()->ObtenerCantidadResultados();

        /* Cantidad de Viajes Frustrados Segunda Visita */
        $CantidadFrustrados2Visita = $this->getejecutadosDao()->ObtenerFrustrados2Visita();

        return new ViewModel(array('title' => 'Back Office the RIM', 
                                                            'rimEjecutados' => $paginator->getIterator(), 
                                                            'paginator' => $paginator, 
                                                            'CantidadServicios' => $CantidadServicios, 
                                                            'CantidadResultados' => $CantidadResultados, 
                                                            'CantidadFrustrados2Visita' => $CantidadFrustrados2Visita, 
                                                            'buscar_CM' => $_SESSION['buscar_CM'], 
                                                            'buscar_INS' => $_SESSION['buscar_INS'], 
                                                            'buscar_VCC' => $_SESSION['buscar_VCC'], 
                                                            'buscar_VSC' => $_SESSION['buscar_VSC'], 
                                                            'buscar_VF' => $_SESSION['buscar_VF'], 
                                                            'buscar_REI' => $_SESSION['buscar_REI'], 
                                                            'buscar_DEV' => $_SESSION['buscar_DEV'], 
                                                            'buscar_AUTO' => $_SESSION['buscar_AUTO'], 
                                                            'buscar_Normal' => $_SESSION['buscar_Normal'], 
                                                            'buscar_CNR' => $_SESSION['buscar_CNR'], 
                                                            'buscar_Subestandar' => $_SESSION['buscar_Subestandar']));


         }

         
    public function EditAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
          }

         /* EL formulario para cargar */
        $form = $this->getFormRegistroRim();

        $idAviso = $this->params()->fromRoute('id');

        /*Busca por el id de RIM*/
        $service_rim = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        /* PRIMERO SE CARGAN LOS DATOS DEL REGISTRO EN EL FORMULARIO */
        $form->bind($service_rim);

        $form->get('numero_aviso')->setValue($service_rim->getNum_avi());

        $form->get('distribuidora')->setValue($service_rim->getCodigo_distri());

        $form->get('nombre_cliente')->setValue($service_rim->getNom_cli());
        $form->get('telefono_cliente')->setValue($service_rim->getTel());
        $form->get('dir1_cliente')->setValue($service_rim->getDir1());
        $form->get('dir2_cliente')->setValue($service_rim->getDir2());

        $form->get('num_instalacion')->setValue($service_rim->getNum_insta());

        $form->get('numero_rim_papel')->setValue($service_rim->getNumero_rim());



        $fechaEje = new \DateTime($service_rim->getDatosDeAviso()->getFecha_carga());
        $form->get('fecha_solicitud')->setValue($fechaEje->format('d-m-Y'));


        $fechaEje = new \DateTime($service_rim->getFe_eje());
        $form->get('fecha_ejecucion')->setValue($fechaEje->format('d-m-Y'));


        $horaIni = new \DateTime($service_rim->getH_ini());
        $form->get('hora_inicio')->setValue($horaIni->format('H:i'));

        $horaTer = new \DateTime($service_rim->getH_ini());
        $form->get('hora_termino')->setValue($horaTer->format('H:i'));


        $form->get('tipo_servicio') ->setValue($service_rim->getTi_serv());

        $form->get('peritaje') ->setValue($service_rim->getSe_pe());

        $form->get('tipo_brigada')->setValue($service_rim->getTi_bri()); 
        $form->get('codigo_ayudante')->setValue($service_rim->getCote2());
        $form->get('patente')->setValue($service_rim->getPat_mo());

        $form->get('tipo_empalme')->setValue($service_rim->getTi_emp());
        $form->get('tipo_red')->setValue($service_rim->getR_dis());
        $form->get('tipo_acometida')->setValue($service_rim->getTi_aco());
        $form->get('tipo_condicion_empalme')->setValue($service_rim->getCod_insp_emp());

        $form->get('codigo_medidor')->setValue($service_rim->getCo_me());
        $form->get('numero_serie')->setValue($service_rim->getNu_se());
        $form->get('anio_fabricacion')->setValue($service_rim->getAn_fa());
        $form->get('lectura_inicial')->setValue($service_rim->getLec_ini());
        $form->get('lectura_final')->setValue($service_rim->getLec_fin());
        $form->get('placa_poste')->setValue($service_rim->getPla_pos());
        $form->get('automatico')->setValue($service_rim->getAuto());

        $form->get('mant_automatico')->setValue($service_rim->getMa_emp_int_automatico());
        $form->get('mant_acometida')->setValue($service_rim->getMa_emp_acometida());
        $form->get('mant_mastil')->setValue($service_rim->getMa_emp_mastil());
        $form->get('mant_mirilla')->setValue($service_rim->getMan_emp_mirilla());
        $form->get('mant_bajada')->setValue($service_rim->getMa_emp_bajada());
        $form->get('mant_caja')->setValue($service_rim->getMa_emp_caja_empalme());
        $form->get('mant_alambrado')->setValue($service_rim->getMa_emp_alambrado());
        $form->get('mant_tapa')->setValue($service_rim->getMan_emp_tapa());
 
        $form->get('voltaje_fn')->setValue($service_rim->getVo_fn());
        $form->get('voltaje_nt')->setValue($service_rim->getVo_nt());
        $form->get('voltaje_ft')->setValue($service_rim->getVo_ft());
        $form->get('corr_instantanea')->setValue($service_rim->getCo_in());
        
        $form->get('prueba_bc_1')->setValue($service_rim->getBf_11());
        $form->get('prueba_bc_2')->setValue($service_rim->getBf_12());
        
        $form->get('prueba_pc_1')->setValue($service_rim->getPf_11());
        $form->get('prueba_pc_2')->setValue($service_rim->getPf_12());
        
        $form->get('verif_rap_1')->setValue($service_rim->getVr_er1());
        $form->get('verif_rap_2')->setValue($service_rim->getVr_er2());
        
        $form->get('serie_patron')->setValue($service_rim->getPa_se());
        $form->get('marca_patron')->setValue($service_rim->getPa_ma());
        $form->get('modelo_patron')->setValue($service_rim->getPa_mo());

        $form->get('cubierta_enc')->setValue($service_rim->getCu_en());
        $form->get('block_enc')->setValue($service_rim->getBl_en());
        $form->get('caja_enc')->setValue($service_rim->getCm_en());
        $form->get('cubierta_dej')->setValue($service_rim->getCu_de());
        $form->get('block_dej')->setValue($service_rim->getBl_de());
        $form->get('caja_dej')->setValue($service_rim->getCm_de());

        $form->get('cod_medidor_dej')->setValue($service_rim->getCo_me_ins());
        $form->get('num_serie_medidor_dej')->setValue($service_rim->getNu_se_ins());
        $form->get('anio_fabricacion_medidor_dej')->setValue($service_rim->getA_fa_ins());
        $form->get('lectura_inicial_medidor_dej')->setValue($service_rim->getLe_ini_ins());
        $form->get('estado_medidor_dej')->setValue($service_rim->getEs_ins());

        $form->get('nombre_residente')->setValue($service_rim->getNo_res());
        $form->get('rut_residente')->setValue($service_rim->getRu_res());
        $form->get('telefono_residente')->setValue($service_rim->getTe_res());

        $form->get('comentario_final')->setValue($service_rim->getCo_fin());
 
        $codigo_anormalidad = array();
        $codigo_anormalidad[] =  $service_rim->getRes_ev_ins_0();
        $codigo_anormalidad[] =  $service_rim->getRes_ev_ins_1();
        $codigo_anormalidad[] =  $service_rim->getRes_ev_ins_2();
        $codigo_anormalidad[] =  $service_rim->getRes_ev_ins_3();

        $detalle = explode('-',$service_rim->getRes_ev_ins_0_detalle());
        if($detalle[0] != ""){
                @$form->get('origen_anormalidad1')->setValue($detalle[0]);
                @$form->get('tratamiento_anormalidad1')->setValue($detalle[1]);
                @$form->get('mot_no_normalizado_anormalidad1')->setValue($detalle[2]);
        }

        $detalle = explode('-',$service_rim->getRes_ev_ins_1_detalle());
        if($detalle[0] != ""){
               @$form->get('origen_anormalidad2')->setValue($detalle[0]);
               @$form->get('tratamiento_anormalidad2')->setValue($detalle[1]);
               @$form->get('mot_no_normalizado_anormalidad2')->setValue($detalle[2]);
         }

        $detalle = explode('-',$service_rim->getRes_ev_ins_2_detalle());
        if($detalle[0] != ""){
               @$form->get('origen_anormalidad3')->setValue($detalle[0]);
               @$form->get('tratamiento_anormalidad3')->setValue($detalle[1]);
               @$form->get('mot_no_normalizado_anormalidad3')->setValue($detalle[2]);
        }

        $detalle = explode('-',$service_rim->getRes_ev_ins_3_detalle());
         if($detalle[0] != ""){
               @$form->get('origen_anormalidad4')->setValue($detalle[0]);
               @$form->get('tratamiento_anormalidad4')->setValue($detalle[1]);
               @$form->get('mot_no_normalizado_anormalidad4')->setValue($detalle[2]);
        }

        $code_abnormality  = $this->getejecutadosDao()->getCodeAbnormalityAll();

        $viewModel = new ViewModel(array(
                                          'idAviso' => $idAviso, 
                                          'form' => $form,
                                          'codigo_anormalidad' => $codigo_anormalidad,
                                          'code_abnormality' => $code_abnormality
										  ,'flashMessages' => $this->flashMessenger()->getMessages()
										  
                                         ));

        return $viewModel;

        
    }

    
    public function GuardarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {

            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        /* Parametros Post desde el formulario riat */
        $postParams = $this->request->getPost();

        /* Array con todos los FILES ELEMENTS */
        $fileParams = $this->params()->fromFiles();



        /*******************************************************************************/
        /*    El Numero de Formulario RIM                                                  */
        /*******************************************************************************/

        if ($this->getRequest()->isPost()) {

            $data['idaviso'] = $postParams["id_aviso"];
            $data['numero_aviso'] = $postParams["numero_aviso"];
            $data['distribuidora'] = $postParams["distribuidora"];
            $data['nombre_cliente'] = $postParams["nombre_cliente"];
            $data['telefono_cliente'] = $postParams["telefono_cliente"];
            $data['dir1_cliente'] = $postParams["dir1_cliente"];
            $data['dir2_cliente'] = $postParams["dir2_cliente"];
 
            $data['numero_rim'] = $postParams["numero_rim_papel"];
     
	        $data['fecha_solicitud'] = $postParams["fecha_solicitud"];
	 
			$data['fecha_ejecucion'] = $postParams["fecha_ejecucion"];
			$data['hora_inicio'] = $postParams["hora_inicio"];
			$data['hora_termino'] = $postParams["hora_termino"];
			$data['tipo_servicio'] = $postParams["tipo_servicio"];
            $data['peritaje'] = $postParams["peritaje"];

			$data['tipo_brigada'] = $postParams["tipo_brigada"];
			$data['codigo_ayudante'] = $postParams["codigo_ayudante"];
			$data['patente'] = $postParams["patente"];

            $data['num_instalacion'] = $postParams["num_instalacion"];

			$data['tipo_empalme'] = $postParams["tipo_empalme"];
			$data['tipo_red'] = $postParams["tipo_red"];
			$data['tipo_acometida'] = $postParams["tipo_acometida"];
			$data['tipo_condicion_empalme'] = $postParams["tipo_condicion_empalme"];
			$data['codigo_medidor'] = $postParams["codigo_medidor"];
			$data['numero_serie'] = $postParams["numero_serie"];
			$data['anio_fabricacion'] = $postParams["anio_fabricacion"];
			$data['lectura_inicial'] = $postParams["lectura_inicial"];
			$data['lectura_final'] = $postParams["lectura_final"];
			$data['placa_poste'] = $postParams["placa_poste"];
			$data['automatico'] = $postParams["automatico"];
			$data['mant_automatico'] = $postParams["mant_automatico"];
			$data['mant_acometida'] = $postParams["mant_acometida"];
			$data['mant_mastil'] = $postParams["mant_mastil"];
			$data['mant_mirilla'] = $postParams["mant_mirilla"];
			$data['mant_bajada'] = $postParams["mant_bajada"];
			$data['mant_caja'] = $postParams["mant_caja"];
			$data['mant_alambrado'] = $postParams["mant_alambrado"];
			$data['mant_tapa'] = $postParams["mant_tapa"];
			$data['voltaje_fn'] = $postParams["voltaje_fn"];
			$data['voltaje_nt'] = $postParams["voltaje_nt"];
			$data['voltaje_ft'] = $postParams["voltaje_ft"];
			$data['corr_instantanea'] = $postParams["corr_instantanea"];
			$data['prueba_bc_1'] = $postParams["prueba_bc_1"];
			$data['prueba_bc_2'] = $postParams["prueba_bc_2"];
			$data['prueba_pc_1'] = $postParams["prueba_pc_1"];
			$data['prueba_pc_2'] = $postParams["prueba_pc_2"];
			$data['verif_rap_1'] = $postParams["verif_rap_1"];
			$data['verif_rap_2'] = $postParams["verif_rap_2"];
			$data['serie_patron'] = $postParams["serie_patron"];
			$data['marca_patron'] = $postParams["marca_patron"];
			$data['modelo_patron'] = $postParams["modelo_patron"];
			$data['cubierta_enc'] = $postParams["cubierta_enc"];
			$data['block_enc'] = $postParams["block_enc"];
			$data['caja_enc'] = $postParams["caja_enc"];
			$data['cubierta_dej'] = $postParams["cubierta_dej"];
			$data['block_dej'] = $postParams["block_dej"];
			$data['caja_dej'] = $postParams["caja_dej"];
			$data['cod_medidor_dej'] = $postParams["cod_medidor_dej"];
			$data['num_serie_medidor_dej'] = $postParams["num_serie_medidor_dej"];
			$data['anio_fabricacion_medidor_dej'] = $postParams["anio_fabricacion_medidor_dej"];
			$data['lectura_inicial_medidor_dej'] = $postParams["lectura_inicial_medidor_dej"];
			$data['estado_medidor_dej'] = $postParams["estado_medidor_dej"];
			$data['nombre_residente'] = $postParams["nombre_residente"];
			$data['rut_residente'] = $postParams["rut_residente"];
			$data['telefono_residente'] = $postParams["telefono_residente"];
			$data['comentario_final'] = $postParams["comentario_final"];
 
            $data['res_ev_ins_0'] = $postParams["codigo_anormalidad1"];
			
            $data['res_ev_ins_0_detalle'] = $postParams["origen_anormalidad1"] .'-'. $postParams["tratamiento_anormalidad1"] .'-'. $postParams["mot_no_normalizado_anormalidad1"];
            
            $data['res_ev_ins_1'] = $postParams["codigo_anormalidad2"];
			
            $data['res_ev_ins_1_detalle'] = $postParams["origen_anormalidad2"] .'-'. $postParams["tratamiento_anormalidad2"] .'-'. $postParams["mot_no_normalizado_anormalidad2"];
            
            $data['res_ev_ins_2'] = $postParams["codigo_anormalidad3"];
			
            $data['res_ev_ins_2_detalle'] = $postParams["origen_anormalidad3"] .'-'. $postParams["tratamiento_anormalidad3"] .'-'. $postParams["mot_no_normalizado_anormalidad3"];
            
            
            $data['res_ev_ins_3'] = $postParams["codigo_anormalidad4"];
			
			if(empty($postParams["mot_no_normalizado_anormalidad4"])){
				
                 $data['res_ev_ins_3_detalle'] = $postParams["origen_anormalidad4"] .'-'. $postParams["tratamiento_anormalidad4"];
			
			} else {
					
				 $data['res_ev_ins_3_detalle'] = $postParams["origen_anormalidad4"] .'-'. $postParams["tratamiento_anormalidad4"] .'-'. $postParams["mot_no_normalizado_anormalidad4"];
				
			}
            
            $fechaEje = new \DateTime();
            $data['fecha_actualiza'] = ($fechaEje->format('d-m-Y H:i:sP'));

            /* Guardando en la base de datos */
            $r = $this->getejecutadosDao()->updateNumeroRim($data);

        }

		/*
		* Antes de Empezar a grabar los archivos, se hace la validacion de peso para todos
		*/
		/* Certificado laboratorio */
		$bFlagError = false;
        if(isset($fileParams["pdf_certificado_lab"]["name"]) && !empty($fileParams["pdf_certificado_lab"]["name"])){
          $extension = pathinfo($fileParams["pdf_certificado_lab"]["name"], PATHINFO_EXTENSION);
		  if ($_FILES["pdf_certificado_lab"]["size"] > 1000000){
			$this->flashMessenger()->addMessage('El certificado de laboratorio debe tener un tamaño menor a 1 Megabyte');
			$bFlagError = true;
		  }			
          if ($extension!="pdf"){
			$this->flashMessenger()->addMessage('El certificado de laboratorio debe tener extension Pdf');				
			$bFlagError = true;			
		  }		  
		}
		/* Certificado laboratorio */		
		/* Certificado Rim Papel */		
        if(isset($fileParams["pdf_rim_papel"]["name"]) && !empty($fileParams["pdf_rim_papel"]["name"])){
          $extension = pathinfo($fileParams["pdf_rim_papel"]["name"], PATHINFO_EXTENSION);
		  if ($_FILES["pdf_rim_papel"]["size"] > 1000000){
			$this->flashMessenger()->addMessage('El Recibo del cliente debe tener un tamaño menor a 1 Megabyte');
			$bFlagError = true;			
		  }			
          if ($extension!="pdf"){
			$this->flashMessenger()->addMessage('El Recibo del cliente debe tener extension Pdf');
			$bFlagError = true;			
		  }
		}
		/* Certificado Rim Papel */				
		/* Certificado Medidor Dejado Nuevo */		
        if (isset($fileParams["pdf_certif_medidor_dejado"]["name"]) && !empty($fileParams["pdf_certif_medidor_dejado"]["name"])){
          $extension = pathinfo($fileParams["pdf_certif_medidor_dejado"]["name"], PATHINFO_EXTENSION);
		  if ($_FILES["pdf_certif_medidor_dejado"]["size"] > 1000000){
			$this->flashMessenger()->addMessage('El Certificado del Medidor debe tener un tamaño menor a 1 Megabyte');			  
			$bFlagError = true;			
          }
          if ($extension != "pdf"){
			$this->flashMessenger()->addMessage('El Certificado del Medidor debe tener extension Pdf');		  
			$bFlagError = true;			
          } 		  
        }
		/* Certificado Medidor Dejado Nuevo */				
		/* Fotos Verificacion */
        if(isset($fileParams["img_rim"]) && $fileParams["img_rim"][0]['name']!=""){
          foreach ($fileParams["img_rim"] as $key => $img) {
            if($fileParams["img_rim"][$key]["size"] > 1000000){
			  $this->flashMessenger()->addMessage('La Foto de Verificacion "'.$fileParams["img_rim"][$key]["name"].'" debe tener un tamaño menor a 1 Megabyte');
			  $bFlagError = true;			  
			}
          }
		}
		/* Fotos Verificacion */		
		/* Fotos Laboratorio */
        if(isset($fileParams["img_lab"]) && $fileParams["img_lab"][0]['name']!=""){
          foreach ($fileParams["img_lab"] as $key => $img) {
            if($fileParams["img_lab"][$key]["size"] > 1000000){
			  $this->flashMessenger()->addMessage('La Foto de Laboratorio "'.$fileParams["img_lab"][$key]["name"].'" debe tener un tamaño menor a 1 Megabyte');
			  $bFlagError = true;			  
			}
          }
		}
		/* Fotos Laboratorio */
		if($bFlagError){
          return $this->redirect()->toRoute('rim', array('controller' => 'index', 'action' => 'edit' , 'id' => $postParams["id_aviso"]));			
		}
		
        /****************************************************************************************************** 
         *  El Certificado de Laboratorio se daja adjunto en la aplicacion asociado al registro de auditoria  *
         ******************************************************************************************************/
        if (!empty($fileParams["pdf_certificado_lab"]["name"])) {
            $extension = pathinfo($fileParams["pdf_certificado_lab"]["name"], PATHINFO_EXTENSION);
            if ($_FILES["pdf_certificado_lab"]["size"] <= 1000000) {
                if ($extension == "pdf") {
                    $nombre_imagen = 'cert' . '_' . $postParams["id_aviso"] . '.' . strtolower($extension);
                    move_uploaded_file($_FILES["pdf_certificado_lab"]["tmp_name"], '/home/meditres/public_html/public/CertificadosLab/' . $nombre_imagen);
                }
            }
        }
        /****************************************************************************************************** 
         *  Aqui se Carga el RIM en PDF de Papel                                                              *
         * ****************************************************************************************************/
        if (!empty($fileParams["pdf_rim_papel"]["name"])) {
            $extension = pathinfo($fileParams["pdf_rim_papel"]["name"], PATHINFO_EXTENSION);
            if ($_FILES["pdf_rim_papel"]["size"] <= 1000000) {
                if ($extension == "pdf") {
                   $nombre_imagen = 'rim' . '_' . $postParams["id_aviso"] . '.' . strtolower($extension);
                   move_uploaded_file($_FILES["pdf_rim_papel"]["tmp_name"], '/home/meditres/public_html/public/RimPapel/' . $nombre_imagen);
                }
            }
        }
       /****************************************************************************************************** 
        *  Aqui se Carga el Cartificado de Medidor Dejado Nuevo                                                              *
        *****************************************************************************************************/
        if (!empty($fileParams["pdf_certif_medidor_dejado"]["name"])) {
          $extension = pathinfo($fileParams["pdf_certif_medidor_dejado"]["name"], PATHINFO_EXTENSION);
          if ($_FILES["pdf_certif_medidor_dejado"]["size"] <= 1000000) {
            if ($extension == "pdf") {
              $nombre_imagen = 'certif_medidor_dejado' . '_' . $postParams["id_aviso"] . '.' . strtolower($extension);
              move_uploaded_file($_FILES["pdf_certif_medidor_dejado"]["tmp_name"], '/home/meditres/public_html/public/Cert_Med_Dej/' . $nombre_imagen);
            } 
          }
        }

        /********************************************************************* 
         * Fotos Verificación
         * Cargador de Fotos primero las paso a binario luego base64 luego   *
         * se actualiza el registro rim en la base para integrar las fotos   *
         * ******************************************************************* */
        if (isset($fileParams["img_rim"]) && $fileParams["img_rim"][0]['name'] != "") {
          $data = array();
          foreach ($fileParams["img_rim"] as $key => $img) {
            if($fileParams["img_rim"][$key]["size"] <= 1000000){
              $im = file_get_contents($img["tmp_name"]);
              $imdata = base64_encode($im);
              //echo "<img src='data:image/jpg;base64,".$imdata."' />"; 
              //echo '<br>';
              $key = $key + 1;
              $data['foto'.$key] = 'data:image/jpg;base64,'.$imdata;  
			}
          }
          $data['idaviso'] = $postParams["id_aviso"];
          /* Guardando en la base de datos */
          $r = $this->getejecutadosDao()->insertarFotosTerrenoRim($data);
          unset($data);
        }

        /********************************************************************* 
         * Fotos Laboratorio
         * Cargador de Fotos primero las paso a binario luego base64 luego   *
         * se actualiza el registro rim en la base para integrar las fotos   *
         * ******************************************************************* */

        if( isset($fileParams["img_lab"]) && $fileParams["img_lab"][0]['name'] != "") {
           $data = array();
           foreach ($fileParams["img_lab"] as $key => $img) {
             $im = file_get_contents($img["tmp_name"]);
             $imdata = base64_encode($im);
             //echo "<img src='data:image/jpg;base64,".$imdata."' />"; 
             //echo '<br>';
             $key = $key + 1;
             $data['fotoLab' . $key] = 'data:image/jpg;base64,' . $imdata;
           }
           $data['idaviso'] = $postParams["id_aviso"];
           /* Guardando en la base de datos */
           $r = $this->getejecutadosDao()->insertarFotosLabRim($data);
           unset($data);
        }

                 
        return $this->redirect()->toRoute('rim', array('controller' => 'index', 'action' => 'edit' , 'id' => $postParams["id_aviso"]));
		
		
    }



    /****
     * 
     * Buscar Codigo de Anormalidad
     * 
     * */

    public function buscarCodigoAnormalidadAction(){
         $codigo = "";
                         
         $codAnor = (String) $this->getRequest()->getPost("codAnor", 0);
        
         if($codAnor!=""){  
              $codigo = $this->getejecutadosDao()->obtenerCodigoAnormalidad($codAnor);
         }
                 
         $view = new ViewModel(array(
                'resultcodigo' => $codigo,
           ));
            
         $view->setTerminal(true);
    
         return $view;
        
    }


    public function fotosAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');

        $rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        /* Obtener codigo de Anormalidad */
        $CodigoAnormalidad = $this->getejecutadosDao()->ObtenerCodigoAnormalidad($rimBuscado->getRes_ev_ins_0());

        $viewModel = new ViewModel(array('rimBuscado' => $rimBuscado));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("rim/index/fotos");

        $viewModel->setTerminal(true);

        return $viewModel;
    }

    
    
    public function fotosLabAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');

        $rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        /* Obtener codigo de Anormalidad */
        $CodigoAnormalidad = $this->getejecutadosDao()->ObtenerCodigoAnormalidad($rimBuscado->getRes_ev_ins_0());

        $viewModel = new ViewModel(array('rimBuscado' => $rimBuscado));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("rim/index/fotoslab");

        $viewModel->setTerminal(true);

        return $viewModel;
    }
    
    

    public function ittAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');

        $rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        $viewModel = new ViewModel(array('rimBuscado' => $rimBuscado));


        /*************************************** 
		 * 
		 * Cambio del Templede del Accion 
		 * 
		 * 
		 **************************************/
        $viewModel->setTemplate("rim/index/itt");
        $viewModel->setTerminal(true);
        return $viewModel;

    }


    public function rimAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');

        $rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        $viewModel = new ViewModel(array('rimBuscado' => $rimBuscado ) );

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("rim/index/rim");
        $viewModel->setTerminal(true);
        return $viewModel;
        
    }


    public function rim2Action() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isGet()) {
            $this->redirect()->toRoute('rim', array('controller' => 'index'));
        }

        $idAviso = $this->params()->fromRoute('id');

        $rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);

        $viewModel = new ViewModel(array('rimBuscado' => $rimBuscado ) );

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("rim/index/rim2");
        $viewModel->setTerminal(true);
        return $viewModel;
        
    }


    /*
     *
     * Finalizamos el RIM luego si no es finalizar puedo derivar a ejecutar otro servicio
     * con la funcion reprocesar
     * el aviso asociado a este RIM quedara con el estado Valorizado.
     * el usuario tiene que confirmar la valorizacion en el modulo correspondiente.
	 * se crea un nuevo aviso en la bandeja de asignacion el cual queda pendiente.
     *
     * */

    public function finalizarAction() {
    	
		$this->layout()->usuario = $this->getLogin()->getIdentity();

        if ($this->getRequest()->isPost()) {

            $postParams = $this->request->getPost();

            $TipoServicioSolicitado = $postParams['tipoaccionsolicitada'];
            $arraysIdAvisos = array();
			

            if (!empty($postParams['id_aviso']))
                $arraysIdAvisos = array($postParams['id_aviso']);
            else
                $arraysIdAvisos = explode('_', $postParams['lista_avisos']);



            foreach ($arraysIdAvisos as $key => $idAviso) {

			                /* Finalizar el Rim */
			                $data = array();
			                $data['idaviso'] = $idAviso;
			                $data['estado_rim'] = 1;
			                $fechaFinalizar = new \DateTime();
			                $data['fecha_finalizado'] = $fechaFinalizar->format('d-m-Y');
			                $data['id_persona_backoffice'] = $this->getLogin()->getIdentity()->personal_id_personal;
			
			               // if ($TipoServicioSolicitado == 218)
			                //			$data['devolucion_distribuidora'] = TRUE; 
			
			                $ejecutados = new ejecutados();
			                $ejecutados->exchangeArray($data);		
			                $this->getejecutadosDao()->finalizarRimEjecutado($ejecutados);
			
							  
			                /* 
							 * 
							 * Actualizar el Aviso a Valorizar luego sera finalizado cuando 
							 * Se confirme la valorizacion y se genere el proceso de facturacion 
							 */
							 
			                $data = array();
			                $data['id_aviso'] = $idAviso;
			                $data['estados_avisos_id_estado_aviso'] = 3;
			                $fechaValorizacion = new \DateTime();
			                $data['fecha_valorizacion'] = $fechaValorizacion->format('d-m-Y');
			
			                $aviso = new Aviso();
			                $aviso->exchangeArray($data);
			
			                $this->getAvisoDao()->updateConfirmaValoriza($aviso);
							
							
							
							/*****************************************************************
							 * 
							 *  Si se envio un servicio para reprocesar el aviso se ejecuta 
							 *
							 *****************************************************************/
							 
			                if ($TipoServicioSolicitado > 1)
							  if ($TipoServicioSolicitado <> 218){
			                            	
			                            $this->getAvisoDao()->ReprocesarAvisoNuevoServicio($idAviso, $TipoServicioSolicitado);
							        
							        }
							  
							  
			
			                //$rimBuscado = $this->getejecutadosDao()->obtenerPorIdaviso($idAviso);      
			                //$rimBuscado->getTi_serv();           
			                //$rimBuscado->getSe_pe(); 
			                //$rimBuscado->getH_ini();
			                //$rimBuscado->getH_te();

              }
        }


        return $this->redirect()->toRoute('rim', array('controller' => 'index'));
		
    }



    public function b2a553d36975dda502e24014d2161a27Action() {

        error_reporting(E_ALL ^ E_NOTICE);

        /*SERVICIO SUSPENDIDO*/
        /*$view = new ViewModel(array('callback' => '{ "success":false, "idAviso": 0, "error": "Servicio suspendido por mantención"}',));
        $view->setTerminal(true);
        return $view;*/


        /* Extrae contenido del sitio que es json */
        $Content = $this->getRequest()->getContent();

        if (empty($Content)) {
            $view = new ViewModel(array('callback' => '',));
            $view->setTerminal(true);
            return $view;
          }

        /* Pasar a array php */
        $json = json_decode($Content);

        $this -> getLogger() -> info("Rim de Terreno : " . $Content . '/n');
          
         //$view = new ViewModel(array( 
          //              'callback' => $json->ResEvIns
               //     ));
        //        $view->setTerminal(true);
        //        return $view;

        try {

            $data = array();

            //"idAviso" BIGINT
            $data['idaviso'] = $json->idAviso;

            //"numAvi" VARCHAR(30) NOT NULL
            $data['num_avi'] = $json->numAvi;

            /* NUEVO */
            $data['numero_rim'] = $json->nuRim;

            //"codigoTipoServicio" VARCHAR(30)
            $data['codigo_tipo_servicio'] = $json->codigoTipoServicio;

            //"codigoDistri" INTEGER
            $data['codigo_distri'] = (empty($json->codigoDistri)) ? NULL : $json->codigoDistri;

            //"nomCli" VARCHAR(60)
            $data['nom_cli'] = $json->nomCli;

            //tel VARCHAR(20)
            $data['tel'] = trim($json->tel);

            //dir1 VARCHAR(100)
            $data['dir1'] = $json->dir1;

            //dir2 VARCHAR(100)
            $data['dir2'] = $json->dir2;

            //com INTEGER
            $data['com'] = $json->codigoComuna;

            //"numInsta" VARCHAR(15) NOT NULL
            $data['num_insta'] = $json->numInsta;

            //"tiEmp" INTEGER
            $data['ti_emp'] = (empty($json->tiEmp)) ? NULL : $json->tiEmp;

            //"rDis" INTEGER
            $data['r_dis'] = (empty($json->rDis)) ? NULL : $json->rDis;

            //"tiAco" INTEGER
            $data['ti_aco'] = (empty($json->tiAco)) ? NULL : $json->tiAco;


            /* NUEVO */
            //"codInstEmp" NUMERIC(1,0)
            $data['cod_insp_emp'] = (empty($json->condInstEmp)) ? NULL : $json->condInstEmp;



            //"plaPos" NUMERIC(15,0)
            $data['pla_pos'] = (empty($json->plaPos)) ? NULL : $json->plaPos;

            //"coMe" INTEGER
            $data['co_me'] = (empty($json->coMe)) ? NULL : $json->coMe;

            //"nuSe" NUMERIC(15,0)
            $data['nu_se'] = (empty($json->nuSe)) ? NULL : $json->nuSe;

            //"anFa" INTEGER
            $data['an_fa'] = (empty($json->anFa)) ? NULL : $json->anFa;

            //"lecIni" DOUBLE PRECISION
            $data['lec_ini'] = (empty($json->lecIni)) ? NULL : $json->lecIni;

            //"lecFin" DOUBLE PRECISION
            $data['lec_fin'] = (empty($json->lecFin)) ? NULL : $json->lecFin;

            //auto VARCHAR(10)
            $data['auto'] = $json->auto;


            //geoInvalid DOOBLE(TRUE)
            $data['geo_invalid'] = ($json->geoInvalid == true) ? 'true' : 'false';

            //lat VARCHAR(100)
            $data['lat'] = $json->lat;

            //lon VARCHAR(100)
            $data['lon'] = $json->lon;


            /*********************************************************************************** */


            if (empty($json->feEje)) {
                $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "Sin fecha de ejecucion"}',));
                $view->setTerminal(true);
                return $view;
            }
            $data['fe_eje'] = $json->feEje;


            if (empty($json->hIni)) {
                $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "Sin hora de inicio"}',));
                $view->setTerminal(true);
                return $view;
            }
            $data['h_ini'] = $json->hIni;


            if (empty($json->hTe)) {
                $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "Sin hora de termino"}',));
                $view->setTerminal(true);
                return $view;
            }
            $data['h_te'] = $json->hTe;


            if (empty($json->tiServ)) {
                $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "Sin codigo servicio"}',));
                $view->setTerminal(true);
                return $view;
            }

            $data['ti_serv'] = (empty($json->tiServ)) ? NULL : $json->tiServ;


            /* Servicio Peritaje */
            $data['se_pe'] = ($json->sePe == true) ? 'true' : 'false';

            /*             * ********************************************************************************* */

            //"tiViFru" INTEGER
            $data['ti_vi_fru'] = (empty($json->tiViFru)) ? NULL : $json->tiViFru;

            //"maEmp" VARCHAR,

            $data['ma_emp_int_automatico'] = $json->maEmp->intAutomatico;
            $data['ma_emp_acometida'] = $json->maEmp->acometida;
            $data['ma_emp_mastil'] = $json->maEmp->mastil;
            $data['ma_emp_bajada'] = $json->maEmp->bajada;
            $data['ma_emp_caja_empalme'] = $json->maEmp->cajaEmpalme;
            $data['ma_emp_alambrado'] = $json->maEmp->alambrado;

            /* NUEVOS */
            $data['man_emp_mirilla'] = $json->maEmp->mirilla;
            $data['man_emp_tapa'] = $json->maEmp->tapa;


            //"voFn" VARCHAR(6)
            $data['vo_fn'] = $json->voFn;

            //"voNt" VARCHAR(6)
            $data['vo_nt'] = $json->voNt;

            //"voFt" VARCHAR(6)
            $data['vo_ft'] = $json->voFt;

            //"coIn" VARCHAR(6)
            $data['co_in'] = $json->coIn;

            //"bF11" VARCHAR(6)
            $data['bf_11'] = $json->bF11;

            //"bF12" VARCHAR(6)
            $data['bf_12'] = $json->bF12;

            //"pF11" VARCHAR(6)
            $data['pf_11'] = $json->pF11;

            //"pF12" VARCHAR(6)
            $data['pf_12'] = $json->pF12;

            //"vrEr1" VARCHAR(6)
            $data['vr_er1'] = $json->vrEr1;

            //"vrEr2" VARCHAR(6)
            $data['vr_er2'] = $json->vrEr2;

            //"cuEn" VARCHAR(15)
            $data['cu_en'] = $json->cuEn;

            //"blEn" VARCHAR(15)
            $data['bl_en'] = $json->blEn;

            //"cmEn" VARCHAR(15)
            $data['cm_en'] = $json->cmEn;

            //"cuDe" VARCHAR(15)
            $data['cu_de'] = $json->cuDe;

            //"blDe" VARCHAR(15)
            $data['bl_de'] = $json->blDe;

            //"cmDe" VARCHAR(15)
            $data['cm_de'] = $json->cmDe;

            //"paSe" VARCHAR(30)
            $data['pa_se'] = $json->paSe;

            //"paMaMo" VARCHAR(30)
            //$data['pa_ma_mo'] = $json -> paMaMo;


            /* NUEVO */

            /* NUMERIC(1,0) */
            $data['pa_ma'] = $json->paMa;

            /* NUMERIC(1,0) */
            $data['pa_mo'] = $json->paMo;



            //"coMeIns" INTEGER
            $data['co_me_ins'] = (empty($json->coMeIns)) ? NULL : $json->coMeIns;

            //"leIniIns" DOUBLE PRECISION
            $data['le_ini_ins'] = (empty($json->leIniIns)) ? NULL : $json->leIniIns;

            //"nuSeIns" NUMERIC(15,0)
            $data['nu_se_ins'] = (empty($json->nuSeIns)) ? NULL : $json->nuSeIns;

            //"aFaIns" INTEGER
            $data['a_fa_ins'] = (empty($json->aFaIns)) ? NULL : $json->aFaIns;

            //"esIns" INTEGER
            $data['es_ins'] = (empty($json->esIns)) ? NULL : $json->esIns;

            /* hay casos donde no esta el codigo de anormalidad */

            if ($json->ResEvIns[0] != 'normal') {

                /** CODIGOS DE ANORMALIDADES CON DETALLE * */
                if (!empty($json->ResEvIns[0])){

                    $ResEvIns_0 = explode('-', $json->ResEvIns[0]);

                }elseif (empty($json->ResEvIns[0])) {  
                           if(!empty($json->tiServ)){
                               if( ($json->tiServ == 197) || ($json->tiServ == 198)  || ($json->tiServ == 199)  || ($json->tiServ == 200)  || ($json->tiServ == 216) ){
        
                                       $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "Falta codigo anormalidad"}',));
                                       $view->setTerminal(true);
                                       return $view;
        
                                    }
                          }
                  }


                /*Estos codigos si podran ir en vacio*/
                if (!empty($json->ResEvIns[1]))
                    $ResEvIns_1 = explode('-', $json->ResEvIns[1]);
                if (!empty($json->ResEvIns[2]))
                    $ResEvIns_2 = explode('-', $json->ResEvIns[2]);
                if (!empty($json->ResEvIns[3]))
                    $ResEvIns_3 = explode('-', $json->ResEvIns[3]);

                // "resEvIns" VARCHAR //
                // CODIGOS DE ANORMALIDADES //
                if (!empty($ResEvIns_0[0]))
                    $data['res_ev_ins_0'] = $ResEvIns_0[0];
                if (!empty($ResEvIns_1[0]))
                    $data['res_ev_ins_1'] = $ResEvIns_1[0];
                if (!empty($ResEvIns_2[0]))
                    $data['res_ev_ins_2'] = $ResEvIns_2[0];
                if (!empty($ResEvIns_3[0]))
                    $data['res_ev_ins_3'] = $ResEvIns_3[0];

                /** DETALLES DE ANORMALIDES * */
                if (!empty($ResEvIns_0[0]))
                    $data['res_ev_ins_0_detalle'] = $ResEvIns_0[1] . '-' . $ResEvIns_0[2] . '-' . $ResEvIns_0[3];
                if (!empty($ResEvIns_1[0]))
                    $data['res_ev_ins_1_detalle'] = $ResEvIns_1[1] . '-' . $ResEvIns_1[2] . '-' . $ResEvIns_1[3];
                if (!empty($ResEvIns_2[0]))
                    $data['res_ev_ins_2_detalle'] = $ResEvIns_2[1] . '-' . $ResEvIns_2[2] . '-' . $ResEvIns_2[3];
                if (!empty($ResEvIns_3[0]))
                    $data['res_ev_ins_3_detalle'] = $ResEvIns_3[1] . '-' . $ResEvIns_3[2] . '-' . $ResEvIns_3[3];

            }else {

                $data['res_ev_ins_0'] = strtoupper($json->ResEvIns[0]);
                $data['res_ev_ins_0_detalle'] = strtoupper($json->ResEvIns[0]);

            }

            //"noRes" VARCHAR(60)
            $data['no_res'] = $json->noRes;

            //"ruRes" VARCHAR(30)
            $data['ru_res'] = $json->ruRes;

            //"teRes" VARCHAR(12)
            $data['te_res'] = $json->teRes;

            //"firRes" TEXT
            //$data['fir_res'] = $json -> firRes;
            //"coTe1" INTEGER NOT NULL
            $data['cote1'] = (empty($json->coTe1)) ? NULL : $json->coTe1;

            //"coTe2" INTEGER
            $data['cote2'] = (empty($json->coTe2)) ? NULL : $json->coTe2;

            //"patMo" VARCHAR
            $data['pat_mo'] = $json->patMo;

            //"tiBri" INTEGER NOT NULL
            $data['ti_bri'] = (empty($json->tiBri)) ? NULL : $json->tiBri;

            //"coFin" VARCHAR(150)
            $data['co_fin'] = $json->coFin;



            $data['fotos1'] = $json->fotos1;
            $data['fotos2'] = $json->fotos2;
            $data['fotos3'] = $json->fotos3;
            $data['fotos4'] = $json->fotos4;
            $data['fotos5'] = $json->fotos5;
            $data['fotos6'] = $json->fotos6;
            $data['fotos7'] = $json->fotos7;
            $data['fotos8'] = $json->fotos8;
            $data['fotos9'] = $json->fotos9;
            $data['fotos10'] = $json->fotos10;


        } catch (Exception $exc) {

            $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": ' . $json->idAviso . ', "error": "' . $exc->getMessage() . '"}',));
            $view->setTerminal(true);
            return $view;

        }

        //        $view = new ViewModel(array(
        //                'callback' => $data
        //            ));
        //
		//            $view->setTerminal(true);
        //            return $view;
        //         $ejecutados = new ejecutados();
        //         $ejecutados->exchangeArray($data);



        /* Guardando en la base de datos */
        $r = $this->getejecutadosDao()->guardar($data);


        /* CONTANDO CUANTAS FOTOS GUARDO */
        $data['fotos1'] = (empty($json->fotos1)) ? 0 : 1;
        $data['fotos2'] = (empty($json->fotos2)) ? 0 : 1;
        $data['fotos3'] = (empty($json->fotos3)) ? 0 : 1;
        $data['fotos4'] = (empty($json->fotos4)) ? 0 : 1;
        $data['fotos5'] = (empty($json->fotos5)) ? 0 : 1;
        $data['fotos6'] = (empty($json->fotos6)) ? 0 : 1;
        $data['fotos7'] = (empty($json->fotos7)) ? 0 : 1;
        $data['fotos8'] = (empty($json->fotos8)) ? 0 : 1;
        $data['fotos9'] = (empty($json->fotos9)) ? 0 : 1;
        $data['fotos10'] = (empty($json->fotos10)) ? 0 : 1;


        //var_dump($data);
        //exit;


        $cadena = '';
        foreach ($data as $value) {
            $cadena .= '[' . $value . ']';
        }

        $this->getLogger()->info("Datos a Insertar : " . $cadena . '/n');

        
        if(empty($data['idaviso'])) $data['idaviso']  = 1;
        

        if ($r == true) {
            $view = new ViewModel(array('callback' => '{ "success":true, "idAviso": ' .  $data['idaviso']  . ' }',));
        } else {
            $view = new ViewModel(array('callback' => '{ "success":false, "idAviso": "null" , "error": "' . $r . '"}',));
        }

        $view->setTerminal(true);
        return $view;
    }



    /**** DESCARGAR TODOS LOS DOCUMENTOS DEL AVISO *****/
    public function descargarZipAction(){
    	
        
	       $this->layout()->usuario = $this->getLogin()->getIdentity();
	              
	       $num_instalacion = $this->params()->fromRoute('id', 0);
	       
	       //$num_aviso = $this->params()->fromRoute('br', 0);
	
	       
	       /*Creamos el Archivo Zip*/ 
	       $zip = new \ZipArchive();
	       
	       $filename = "/home/meditres/public_html/module/Rim/view/rim/index/test.zip";
	            
	       if ($zip->open($filename, \ZIPARCHIVE::CREATE )!==TRUE) {
	            printf('Erróneo con el código %d', $ret);
	            exit;
	          }
	        
	       
	       $file = '/home/meditres/public_html/public/Cert_Med_Dej/certif_medidor_dejado_31505.pdf';
	       $zip->addFile($file,"certif_medidor_dejado_31505.pdf");
		   
           $file = '/home/meditres/public_html/public/CertificadosLab/cert_16069.pdf';
	       $zip->addFile($file,"cert_16069.pdf");
		
           $file = '/home/meditres/public_html/public/RimPapel/rim_15975.pdf';
	       $zip->addFile($file,"rim_15975.pdf");
		
		
		/*implementar DOMPDF*/
		
		
		   
	       /*
	       $directory = '/home/meditres/public_html/public/Cert_Med_Dej/certif_medidor_dejado_'.$num_aviso.'.pdf';
	            
	       if(file_exists($directory)){
	       
	            $scanned_directory = array_diff(scandir($directory), array('..', '.'));
	            
	            foreach ($scanned_directory as $value) {
	              $zip->addFile( $directory . $value ,"/".$value);
	             }
	            
	       */
               
				
	            $zip->close();
				
	
	            header("Content-type: application/zip");
	            header("Content-Disposition: attachment; filename=aviso_1200056998.zip");
	            header("Cache-Control: no-cache, must-revalidate");
	            header("Expires: 0");
	
	            readfile($filename);
	            
	            
	            /*Elimino cada vez el archivo para evitar que continuen ingresando archivos al zip*/
	            unlink( $filename );
	       
	            
	       //}
	       
	       exit; 
	        
	       /*Fin de Crear el Archivo Zip*/
	       
	       $view = new ViewModel(array(
	            'url' => $url,
	        ));
	
	        $view->setTerminal(true);
	
	        return $view;
		
		
        
      }



}
