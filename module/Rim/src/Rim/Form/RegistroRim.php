<?php

namespace Rim\Form;

use Zend\Form\Form;

class RegistroRim extends Form {

    public function __construct($name = null) {
        parent::__construct('formRim');

        $this->add(array(
            'name' => 'id_ors_riat',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'id_aviso',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));   
         
        $this->add(array(
            'name' => 'id_instalacion',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));  
        
        $this->add(array(    
            'name' => 'csrf', 
            'type' => 'Zend\Form\Element\Csrf', 
        )); 
        
        
        
        /*
         * 
         * Carga Fotografias  
         *
         */
        
        $this->add(array(
            'name' => 'img_riat[]',
            'type' => 'File',
            'attributes' => array(
                'multiple' => 10,
                'accept' => 'image/*',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Fotos de la Auditoria Max. (25) (Formato JPGE y Bmp. peso menor a 1 MB):  ',
            ),
        ));
        
        
       $this->add(array(
            'name' => 'img_form_riat',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Formulario RIAT (Formato PDF. peso menor a 1 MB):  ',
            ),
        ));


       $this->add(array(
            'name' => 'img_certificado_lab',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Certificado Laboratorio:',
            ),
          )
        );



       $this->add(array(
            'name' => 'img_devo',
            'type' => 'File',
            'attributes' => array(
                'accept' => 'application/pdf',
                //'required' => 'required', 
            ),
            'options' => array(
                'label' => 'Imagen del Form. Devolucion:',
            ),
          )
        );
       
       /**+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico',
            'options' => array(
                'label' => 'Tecnico 1 : ',
            ),
            'attributes' => array(
                'style' => 'width:300px;margin:auto;',
                'id' => 'persona_tecnico',
                'required' => 'required', 
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'persona_tecnico_2',
            'options' => array(
                'label' => 'Tecnico 2 : ',
            ),
            'attributes' => array(
                'style' => 'width:300px;margin:auto;',
                'id' => 'persona_tecnico_2',
                'pattern' => '^[1-9][0-9]*$',
                //'required' => 'required', 
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_aviso',
            'options' => array(
                'label' => 'N° Aviso / OT : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                 'pattern' => '.{3,}',
                 'title' => "minimo 3 caracteres" ,
                 'maxlength' => '12',
                 //'readonly' => 'readonly',
                 'required' => 'required',
              )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'distribuidora',
            'options' => array(
                'label' => 'Distribuidora : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccione',
                    1 => 'CGED',
                    2 => 'CONAFE',
                    3 => 'EMEL',
                ),
            )
        ));

$this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombre_cliente',
            'options' => array(
                'label' => 'Nombre Cliente : ',
            ),
            'attributes' => array(
                 'style' => 'width:200px;',
                 'pattern' => '^([A-Za-z\s.-ñÑ]){3,50}$',
                 'title' => "minimo 3 caracteres maximo 30 Solo Letras" ,
                 //'required' => 'required', 
                )
        ));

$this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'telefono_cliente',
            'options' => array(
                'label' => 'Teléfono : ',
            ),
            'attributes' => array(
                 'style' => 'width:200px;',
                 'pattern'=> "[0-9]{8,10}",
                 'title' => "minimo 8 caracteres maximo 10 Solo Numeros" ,
                 //'required' => 'required', 
                )
        ));

$this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'dir1_cliente',
            'options' => array(
                'label' => 'Dirección 1 : ',
            ),
            'attributes' => array(
                 'style' => 'width:200px;',
                  'pattern' => '{3,60}',
                 //'required' => 'required', 
                )
        ));

$this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'dir2_cliente',
            'options' => array(
                'label' => 'Dirección 2  : ',
            ),
            'attributes' => array(
                 'style' => 'width:200px;',
                  'pattern' => '{3,60}',
                 //'required' => 'required', 
                )
          ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'numero_rim_papel',
            'options' => array(
                'label' => 'N° Form Rim : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                  'pattern' => '^[0-9]+$',
                 //'required' => 'required', 
                )
        ));



      $this->add(array(
      'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_solicitud',
            'options' => array(
                'label' => 'Fecha Solicitud : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                 'required' => 'required', 
                 'pattern'=> '(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}',
                 'title' => "Formato DD-MM-AAAA" ,
                )
        ));
		

		
      $this->add(array(
      'type' => 'Zend\Form\Element\Text',
            'name' => 'fecha_ejecucion',
            'options' => array(
                'label' => 'Fecha Ejecución : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                 'required' => 'required', 
                 'pattern'=> '(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}',
                 'title' => "Formato DD-MM-AAAA" ,
                )
        ));
		

      $this->add(array(
      'type' => 'Zend\Form\Element\Text',
            'name' => 'hora_inicio',
            'options' => array(
                'label' => 'Hora Inicio : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                 'required' => 'required', 
                 //'pattern'=> '(0[1-9]|1[0-9]|2[0-4]):(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])',
                 'pattern'=> '^(09|1[0-7]{1}):([0-5]{1}[0-9]{1})$',
                 'title' => "Formato HH:MM desde las 09:00 a las 17:59" ,
                )
        ));

       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_servicio',
            'options' => array(
                'label' => 'Servicio Ejecutado : ',
            ),
            'attributes' => array(
                'style' => 'width:200px;',
                'required' => 'required', 
                'options' => array(
                    0 => 'Seleccionar',
                    197 => 'Cambio Medidor',
                    198 => 'Inspección Visual',
                    199 => 'Verificación Con Cambio',
                    200 => 'Verificación Sin Cambio',
                    201 => 'Viaje Frustrado'
                ),
            )
        ));

       $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'peritaje',
                    'options' => array(
                        'label' => 'Peritaje : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:150px;',
                        'options' => array(
                            0 => 'No',
                            1 => 'Si',
                        ),
                    )
            ));

      $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'num_instalacion',
            'options' => array(
                'label' => 'N° Instalación : ',
            ),
            'attributes' => array(
                 'style' => 'width:150px;',
                 //'readonly' => 'readonly',
                 'required' => 'required',
                )
        ));

       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_empalme',
            'options' => array(
                'label' => 'Tipo Empalme : ',
            ),
            'attributes' => array(
                'style' => 'width:200px;',
                'options' => array(
                    0 => 'Seleccionar',
                    1 => 'Residencial',
                    2 => 'Comercial',
                    3 => 'Industrial',
                    4 => 'Agrícola',
                    5 => 'Alumbrado Público'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_red',
            'options' => array(
                'label' => 'Tipo Red Dist. : ',
            ),
            'attributes' => array(
                'style' => 'width:200px;',
                'options' => array(
                    0 => 'Seleccionar',
                    1 => 'Aérea',
                    2 => 'Subterránea'
                ),
            )
        ));

       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_acometida',
            'options' => array(
                'label' => 'Tipo Acometida : ',
            ),
            'attributes' => array(
                'style' => 'width:200px;',
                'options' => array(
                    0 => 'Seleccionar',
                    1 => 'PI',
                    2 => 'Concéntrico',
                    3 => 'XLPE Subterráneo'
                ),
            )
        ));


       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tipo_condicion_empalme',
            'options' => array(
                'label' => 'Condición Esp. Empalme : ',
            ),
            'attributes' => array(
                'style' => 'width:200px;',
                'options' => array(
                         0 => 'Normal',
                         1 => 'Pareado',
                         2 => 'Medidor Interior',
                         3 => 'Medidor en Nicho' ),
                       )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_automatico',
                    'options' => array(
                    'label' => 'Automatíco : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                    0 => 'Seleccionar',
                    1 => 'Afiance',
                    2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_acometida',
                    'options' => array(
                    'label' => 'Acometida : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                    0 => 'Seleccionar',
                    1 => 'Afiance',
                    2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_mastil',
                    'options' => array(
                    'label' => 'Mastil : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_mirilla',
                    'options' => array(
                    'label' => 'Mirilla : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_bajada',
                    'options' => array(
                    'label' => 'Bajada : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_caja',
                    'options' => array(
                    'label' => 'Caja Empalme : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_alambrado',
                    'options' => array(
                    'label' => 'Alambrado : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'mant_tapa',
                    'options' => array(
                    'label' => 'Tapa : ',
                    ),
                    'attributes' => array(
                    'style' => 'width:200px;',
                    'options' => array(
                        0 => 'Seleccionar',
                        1 => 'Afiance',
                        2 => 'Cambio'
                    ),
                    )
                    ));

                    $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'codigo_medidor',
                    'options' => array(
                        'label' => 'Codigo Medidor : ',
                    ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

         $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'numero_serie',
                    'options' => array(
                        'label' => 'Numero Serie : ',
                    ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

        $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'anio_fabricacion',
                    'options' => array(
                        'label' => 'Año Fabricación : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

       $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'lectura_inicial',
                    'options' => array(
                        'label' => 'Lectura Inicial : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

         $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'lectura_final',
                    'options' => array(
                        'label' => 'Lectura final : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

        $this->add(array(
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'placa_poste',
                    'options' => array(
                        'label' => 'Placa Poste : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                         'pattern' => '^([0-9]+|-1)$',
                        )
                ));

         $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'automatico',
                    'options' => array(
                        'label' => 'Automático : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));



          $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'voltaje_fn',
                    'options' => array(
                        'label' => 'Voltaje F-N (V) : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


           $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'voltaje_nt',
                    'options' => array(
                        'label' => 'Voltaje N-T (V): ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

           $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'voltaje_ft',
                    'options' => array(
                        'label' => 'Voltaje F-T (V) : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

            $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'corr_instantanea',
                    'options' => array(
                        'label' => 'Corr. Instantanéas (A) : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


          $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'prueba_bc_1',
                    'options' => array(
                        'label' => 'Prueba 1 BC : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


           $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'prueba_bc_2',
                    'options' => array(
                        'label' => 'Prueba 2 BC : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


            $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'prueba_pc_1',
                    'options' => array(
                        'label' => 'Prueba 1 PC : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

            $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'prueba_pc_2',
                    'options' => array(
                        'label' => 'Prueba 2 PC : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'verif_rap_1',
                    'options' => array(
                        'label' => 'Verif. Rápida : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'verif_rap_2',
                    'options' => array(
                        'label' => 'Verif. Rápida : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'serie_patron',
                    'options' => array(
                        'label' => 'Nº de Serie : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


          $this->add(array(
                        'type' => 'Zend\Form\Element\Select',
                        'name' => 'marca_patron',
                        'options' => array(
                        'label' => 'Marca : ',
                        ),
                        'attributes' => array(
                        'style' => 'width:200px;',
                        'options' => array(
                        0 => 'Seleccionar',
                                1 => 'MTE',
                                2 => 'ECA'
                        ),
                    )
             ));


        $this->add(array(
                            'type' => 'Zend\Form\Element\Select',
                            'name' => 'modelo_patron',
                            'options' => array(
                            'label' => 'Modelo : ',
                            ),
                            'attributes' => array(
                            'style' => 'width:200px;',
                            'options' => array(
                                0 => 'Seleccionar',
                                1 => 'PTS 2.1',
                                1 => '200',
                                2 => 'CheckMeter 2.1',
                                3 => 'CheckSystem 2.1'
                            ),
                        )
                   ));



            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'cubierta_enc',
                                'options' => array(
                                    'label' => 'Cubierta E. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                     'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'cubierta_dej',
                                'options' => array(
                                    'label' => 'Cubierta D. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                     'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'block_enc',
                                'options' => array(
                                    'label' => 'Block E. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                      'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'block_dej',
                                'options' => array(
                                    'label' => 'Block D. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                      'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'caja_enc',
                                'options' => array(
                                    'label' => 'Caja del Medidor E. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                      'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

            $this->add(array(    
                          'type' => 'Zend\Form\Element\Text',
                                'name' => 'caja_dej',
                                'options' => array(
                                    'label' => 'Caja del Medidor D. : ',
                                 ),
                                'attributes' => array(
                                     'style' => 'width:150px;',
                                     'pattern'=> '^[A-Za-z0-9\s]+$',
                                     //'required' => 'required', 
                                    )
                            ));

$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'cod_medidor_dej',
                    'options' => array(
                        'label' => 'Cod de Medidor : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));
		
$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'num_serie_medidor_dej',
                    'options' => array(
                        'label' => 'Nº de Serie : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'anio_fabricacion_medidor_dej',
                    'options' => array(
                        'label' => 'Año de Fabricación : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));

$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'lectura_inicial_medidor_dej',
                    'options' => array(
                        'label' => 'Lectura Inicial : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        )
                ));


        $this->add(array(
                            'type' => 'Zend\Form\Element\Select',
                            'name' => 'estado_medidor_dej',
                            'options' => array(
                            'label' => 'Estado Medidor : ',
                            ),
                            'attributes' => array(
                            'style' => 'width:200px;',
                            'options' => array(
                                0 => 'Seleccionar',
                                1 => 'DEFINITIVO',
                                2 => 'PROVISORIO',
                            ),
                        )
                   ));

	$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'nombre_residente',
                    'options' => array(
                        'label' => 'Nombre Contacto : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:200px;',
                         'pattern' => '^[A-Za-z\s-]+$',
                         //'required' => 'required', 
                        )
                ));	

    $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'rut_residente',
                    'options' => array(
                        'label' => 'Rut : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:200px;',
                         'pattern' => '^[A-Za-z0-9\s-]+$',
                         //'required' => 'required', 
                      )
                ));	
				
	$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'telefono_residente',
                    'options' => array(
                        'label' => 'Telefono : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:200px;',
                         'pattern' => '^[0-9]+$',
                         //'required' => 'required', 
                        )
                ));	
					
					
					
        $this->add(array(
                         'type' => 'Zend\Form\Element\Select',
                         'name' => 'tipo_brigada',
                         'options' => array(
                         'label' => 'Tipo de Brigada :',
                         ),
                          'attributes' => array(
                          'style' => 'width:200px;',
                          'options' => array(
                                0 => 'Seleccionar',
                                1 => 'Normal',
								2 => 'Individual',
								3 => 'Auto. Ind.',
								4 => 'Autogestionada'
                            ),
                        )
                   ));
			
			
        $this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'codigo_ayudante',
                    'options' => array(
                        'label' => 'Cod Tec 2 : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         //'required' => 'required', 
                        'pattern' => '^[1-9][0-9]*$',
                        )
                    ));	    
				
					
		$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'patente',
                    'options' => array(
                        'label' => 'Patente : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         'required' => 'required', 
                         'pattern' => '^[A-Za-z0-9]{6}$',
                       )
                   ));
				   
		
		$this->add(array(    
              'type' => 'Zend\Form\Element\Text',
                    'name' => 'hora_termino',
                    'options' => array(
                        'label' => 'Hora de Término : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:150px;',
                         'required' => 'required', 
                         'pattern'=> '^(09|1[0-7]{1}):([0-5]{1}[0-9]{1})$',
                         'title' => "Formato HH:MM desde las 09:00 a las 17:59" ,
                        )
                   ));
				   		   
		
		$this->add(array(    
              'type' => 'Zend\Form\Element\Textarea',
                    'name' => 'comentario_final',
                    'options' => array(
                        'label' => 'Comentario Final : ',
                     ),
                    'attributes' => array(
                         'style' => 'width:800px;margin:auto',
                         //'required' => 'required',
                          'pattern' => '{150}',
                         'cols' => 50,
                         'rows' => 10 
                        )
                   ));
				   
				   		   								
							
        /* TERMINO */



 /*++++++++++++++++++++++++++++++++++++++++++++++*/
 
 // ANORMALIDADES Y CNR 

 $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad1',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                   '-' => 'Seleccine',
                    1 => 'Falla interna',
                    2 => 'Intervencion Terceros',
					3 => 'Falta de mantenimiento',
					4 => 'Fuera de norma',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad1',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));     
        
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad1',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condición Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                    5 => 'A solicitud de EE.DD.',
                ),
            )
        ));
    
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad2',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                   '-' => 'Seleccine',
                    1 => 'Falla interna',
                    2 => 'Intervencion Terceros',
					3 => 'Falta de mantenimiento',
					4 => 'Fuera de norma',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad2',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));     
        
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad2',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                    5 => 'A solicitud de EE.DD.',
                ),
            )
        )); 
  
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad3',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                   '-' => 'Seleccine',
                    1 => 'Falla interna',
                    2 => 'Intervencion Terceros',
					3 => 'Falta de mantenimiento',
					4 => 'Fuera de norma',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad3',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));     
        
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad3',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                    5 => 'A solicitud de EE.DD.',
                ),
            )
        ));     
    
    
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'origen_anormalidad4',
            'options' => array(
                'label' => 'Origen : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                   '-' => 'Seleccine',
                    1 => 'Falla interna',
                    2 => 'Intervencion Terceros',
					3 => 'Falta de mantenimiento',
					4 => 'Fuera de norma',
                ),
            )
        ));
   
   $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tratamiento_anormalidad4',
            'options' => array(
                'label' => 'Tratamiento : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Normalizado',
                    2 => 'No Normalizado',
                ),
            )
        ));     
        
    $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mot_no_normalizado_anormalidad4',
            'options' => array(
                'label' => 'Mot. No Normalizado : ',
            ),
            'attributes' => array(
                'style' => 'width:150px;',
                'options' => array(
                    0 => 'Seleccine',
                    1 => 'Cliente no Autoriza',
                    2 => 'Condiciòn Insegura',
                    3 => 'Mant. Mayor',
                    4 => 'Colgado Acometida',
                    5 => 'A solicitud de EE.DD.',
                ),
            )
        )); 
    
        
  /********************************************************
   * ++++++++++++++++++++++++++++++++++++++++++++++
   * *******************************************************/

        /*BONOTES DE ENVIO DE DATOS*/    
        $this->add(array(
            'name' => 'send',   
            'attributes' => array(
                'type' => 'button',
                'value' => 'Guardar',
                'onclick' => "javascript:return salida()",
                'class' => 'btn btn-primary btn-lg btn-block',
                'style' => 'margin-left:280px;margin-top:30px;width:610px;height:50px;',
            ),
        ));



     }

}

