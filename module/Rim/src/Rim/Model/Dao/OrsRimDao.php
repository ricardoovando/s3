<?php

namespace Rim\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Rim\Model\Entity\OrsRim;

class OrsRimDao {

    protected $tableGateway;
    protected $login;

    public function __construct(TableGateway $tableGateway, $login) {
            $this->tableGateway = $tableGateway;
            $this->login = $login->getIdentity();
        }

    public function obtenerTodos() {

            $id_usuario = $this->login->id;

            $select = $this->tableGateway->getSql()->select();

//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('numero_aviso' => 'numero_aviso'));
//
//            $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avi.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//            
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//           $select->where(array('ors_riat.estados_ors_id_estado_ors <> 4')); 
//            
//           $select->order('ors_riat.estados_ors_id_estado_ors ASC');
           
    //        $select->join(array('est' => 'estados_avisos'), 'est.id_estado_aviso = avisos.estados_avisos_id_estado_aviso', array('nombre_estado_aviso' => 'nombre_estado_aviso', 'clase_color' => 'clase_color'));
    //        $select->join(array('resper' => 'personal'), 'resper.id_persona = avisos.responsable_personal_id_persona', array('nombres' => 'nombres','apellidos' => 'apellidos'), 'left');
    //        $select->where(array('avisos.sucursales_id_sucursal' => $sucursales_id_sucursal ));
    //        $select->order('id_aviso ASC');

            $dbAdapter = $this->tableGateway->getAdapter();
            $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

            $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
            $paginator = new Paginator($adapter);
            return $paginator;

         }

//    public function obtenerPorNumAviso($num_aviso) {
//
//            //$num_aviso = (int) $num_aviso;
//
//            $id_usuario = $this->login->id;
//
//            $select = $this->tableGateway->getSql()->select();
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('numero_aviso' => 'numero_aviso'));
//            $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avi.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            $select->where(array('avi.numero_aviso' => $num_aviso));
//            $select->order('ors_riat.id_ors_riat ASC');
//        
//            $dbAdapter = $this->tableGateway->getAdapter();
//            $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//            $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//            $paginator = new Paginator($adapter);
//            return $paginator;
//
//         } 
         
         
         
//    public function obtenerPorNumInstalacion($numeroinstalacion) {
//
//            //$num_aviso = (int) $num_aviso;
//
//            $id_usuario = $this->login->id;
//
//            $select = $this->tableGateway->getSql()->select();
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('numero_aviso' => 'numero_aviso'));
//            $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avi.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            $select->where(array('inst.num_instalacion' => $numeroinstalacion));
//            $select->order('ors_riat.id_ors_riat ASC');
//        
//            $dbAdapter = $this->tableGateway->getAdapter();
//            $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//            $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//            $paginator = new Paginator($adapter);
//            return $paginator;
//
//         }
         
    
//    public function obtenerPorSucursal($id_sucursal) {
//
//            $id_sucursal = (int) $id_sucursal;
//
//            $id_usuario = $this->login->id;
//
//            $select = $this->tableGateway->getSql()->select();
//
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('numero_aviso' => 'numero_aviso'));
//
//            $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avi.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//                        
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            $select->where(array('avi.sucursales_id_sucursal' => $id_sucursal));
//            $select->order('ors_riat.id_ors_riat ASC');
//
//            $dbAdapter = $this->tableGateway->getAdapter();
//            $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//            $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//            $paginator = new Paginator($adapter);
//            return $paginator;
//
//         }   
   
//    public function obtenerPorEstado($id_estado) {
//
//            $id_estado = (int) $id_estado;
//
//            $id_usuario = $this->login->id;
//
//            $select = $this->tableGateway->getSql()->select();
//
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('numero_aviso' => 'numero_aviso'));
//
//            $select->join(array('inst' => 'instalaciones'), 'inst.id_instalacion = avi.instalaciones_id_instalacion', array('num_instalacion' => 'num_instalacion'));
//
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            $select->where(array('ors_riat.estados_ors_id_estado_ors' => $id_estado));
//            
//            $select->order('ors_riat.id_ors_riat ASC');
//
//            $dbAdapter = $this->tableGateway->getAdapter();
//            $resultSetPrototype = $this->tableGateway->getResultSetPrototype();
//
//            $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
//            $paginator = new Paginator($adapter);
//            return $paginator;
//
//         }  
         
//    public function obtenerPorId($id) {
//
//            $id_usuario = $this->login->id;
//
//            $id = (int) $id;
//
//            $select = $this->tableGateway->getSql()->select();
//
//            $select->columns(array('id_ors_riat' => 'id_ors_riat',
//                                    'estados_ors_id_estado_ors' => 'estados_ors_id_estado_ors',
//                                    'avisos_id_aviso' => 'avisos_id_aviso',
//                                    'fecha_asignacion_riat' => 'fecha_asignacion_riat',
//                                    'fecha_comprometido' => 'fecha_comprometido',
//                                    'fecha_entrega_dig' => 'fecha_entrega_dig',
//                                    'fecha_entrega_fisica' => 'fecha_entrega_fisica',
//                                    'digitador_personal_id_persona' => 'digitador_personal_id_persona',
//                                    'num_form_riat' => 'num_form_riat',
//                                    'fecha_atencion_servicio' => new \Zend\Db\Sql\Expression("TO_CHAR(ors_riat.fecha_atencion_servicio,'DD-MM-YYYY')"),
//                                    'estado_riat' => 'estado_riat',
//                
//                                     /*SE PUEDE MIGRAR A LA TABLA IRREGULARIDADES*/
//                                     'anor_encajadeempalme' => 'anor_encajadeempalme',
//                                     'anor_enecm' => 'anor_enecm',
//                                     'anor_enmedidor' => 'anor_enmedidor',
//                                     'anor_esquemademedida' => 'anor_esquemademedida',
//                                     'anor_faltamedidareactiva' => 'anor_faltamedidareactiva',
//                                     'anor_alarmadebateria' => 'anor_alarmadebateria',
//                                     'anor_cargabajavariable' => 'anor_cargabajavariable',
//                                     'anor_tarifanocorresponde' => 'anor_tarifanocorresponde',
//                                     'anor_faltasello' => 'anor_faltasello',
//                                     'anor_ttcc' => 'anor_ttcc',
//                                     'anor_otro' => 'anor_otro',
//                                     'clas_cnr_1' => 'clas_cnr_1',
//                                     'clas_cnr_2' => 'clas_cnr_2',
//                                     'clas_cnr_3' => 'clas_cnr_3',
//                                     'clas_cnr_4' => 'clas_cnr_4',
//                                     'clas_cnr_5' => 'clas_cnr_5',
//                                     'clas_cnr_6' => 'clas_cnr_6',
//                                     'clas_cnr_7' => 'clas_cnr_7',
//                                     'clas_cnr_8' => 'clas_cnr_8',   
//                                     'clas_cnr_9' => 'clas_cnr_9', 
//                                     'tecnico2_personal_id_persona' => 'tecnico2_personal_id_persona',
//                                  )
//                               );
//
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//
//            $select->join(array('da' => 'datos_adjuntos'), 'da.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('ca' => 'calendario_medidor'), 'ca.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('invi' => 'inspeccion_visual_empalme'), 'invi.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('dia' => 'diagrama'), 'dia.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('razen' => 'razon_encontrada'), 'razen.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('orig' => 'origen_afectacion_alamedida'), 'orig.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('lecmedinst' => 'lectura_medidor_instalado'), 'lecmedinst.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('*'));
//
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//
//            $select->join(array('tecpers' => 'personal'), 'tecpers.id_persona = avi.responsable_personal_id_persona', array(
//                'nombres_tec' => 'nombres',
//                'apellidos_tec' => 'apellidos',
//                'email_tec' => 'email',
//                'rut_persona_tec' => 'rut_persona',
//                'foto_carnet_tec' => 'foto_carnet',
//                    ), 'left');
//
//            
//            $select->join(array('int' => 'instalaciones'), 'int.id_instalacion = avi.instalaciones_id_instalacion', array('*'));
//            $select->join(array('dist' => 'distribuidoras'), 'dist.id_distribuidora = int.distribuidoras_id_distribuidora', array('*'));
//            
//            $select->join(array('com' => 'comunas'), 'com.id_comuna = int.comunas_id_comuna', array('provincias_id_provincia' => 'provincias_id_provincia'), 'left');
//            $select->join(array('pro' => 'provincias'), 'pro.id_provincia = com.provincias_id_provincia', array('regiones_id_region' => 'regiones_id_region'), 'left');
//            //$select->join(array('reg' => 'regiones'), 'reg.id_region = pro.regiones_id_region', array('id_region' => 'id_region'),'left');
//            $select->join(array('anem' => 'antecedentes_empalme'), 'anem.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('rel' => 'reloj'), 'rel.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('reg' => 'regletas'), 'reg.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('tel' => 'telemedida'), 'tel.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('sisdis' => 'sistema_distribucion'), 'sisdis.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('e' => 'ecm'), 'e.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//
//            $select->join(array('cliente' => 'contacto_autoriza'), 'cliente.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array(
//                'nombre_contacto' => 'nombre_contacto',
//                'run' => 'run',
//                'fono' => 'fono',
//                'email' => 'email',
//                    ), 'left');
//
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            $select->where(array('ors_riat.id_ors_riat' => $id));
//            
//            $rowSet = $this->tableGateway->selectWith($select);
//            $row = $rowSet->current();
//
//            if (!$row) {
//                throw new \Exception("Could not find row $id");
//            }
//            
//            return $row;
//            
//        }
        
//    public function obtenerPorNumAvisoCnRiat($numAviso,$t) {
//
//            $id_usuario = $this->login->id;
//
//            $select = $this->tableGateway->getSql()->select();
//
//            $select->columns(array('id_ors_riat' => 'id_ors_riat',
//                                    'estados_ors_id_estado_ors' => 'estados_ors_id_estado_ors',
//                                    'avisos_id_aviso' => 'avisos_id_aviso',
//                                    'fecha_asignacion_riat' => 'fecha_asignacion_riat',
//                                    'fecha_comprometido' => 'fecha_comprometido',
//                                    'fecha_entrega_dig' => 'fecha_entrega_dig',
//                                    'fecha_entrega_fisica' => 'fecha_entrega_fisica',
//                                    'digitador_personal_id_persona' => 'digitador_personal_id_persona',
//                                    'num_form_riat' => 'num_form_riat',
//                                    'fecha_atencion_servicio' => new \Zend\Db\Sql\Expression("TO_CHAR(ors_riat.fecha_atencion_servicio,'DD-MM-YYYY')"),
//                                    'estado_riat' => 'estado_riat',
//                
//                                     /*SE PUEDE MIGRAR A LA TABLA IRREGULARIDADES*/
//                                     'anor_encajadeempalme' => 'anor_encajadeempalme',
//                                     'anor_enecm' => 'anor_enecm',
//                                     'anor_enmedidor' => 'anor_enmedidor',
//                                     'anor_esquemademedida' => 'anor_esquemademedida',
//                                     'anor_faltamedidareactiva' => 'anor_faltamedidareactiva',
//                                     'anor_alarmadebateria' => 'anor_alarmadebateria',
//                                     'anor_cargabajavariable' => 'anor_cargabajavariable',
//                                     'anor_tarifanocorresponde' => 'anor_tarifanocorresponde',
//                                     'anor_faltasello' => 'anor_faltasello',
//                                     'anor_ttcc' => 'anor_ttcc',
//                                     'anor_otro' => 'anor_otro',
//                                     'clas_cnr_1' => 'clas_cnr_1',
//                                     'clas_cnr_2' => 'clas_cnr_2',
//                                     'clas_cnr_3' => 'clas_cnr_3',
//                                     'clas_cnr_4' => 'clas_cnr_4',
//                                     'clas_cnr_5' => 'clas_cnr_5',
//                                     'clas_cnr_6' => 'clas_cnr_6',
//                                     'clas_cnr_7' => 'clas_cnr_7',
//                                     'clas_cnr_8' => 'clas_cnr_8',
//                                     'clas_cnr_9' => 'clas_cnr_9',
//                                     'tecnico2_personal_id_persona' => 'tecnico2_personal_id_persona',
//                                  )
//                               );
//
//            $select->join(array('est' => 'estados_ors'), 'est.id_estado_ors = ors_riat.estados_ors_id_estado_ors', array('descripcion_estado_ors' => 'descripcion_estado_ors', 'clase_color' => 'clase_color'));
//
//            $select->join(array('da' => 'datos_adjuntos'), 'da.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('ca' => 'calendario_medidor'), 'ca.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('invi' => 'inspeccion_visual_empalme'), 'invi.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('dia' => 'diagrama'), 'dia.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('razen' => 'razon_encontrada'), 'razen.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('orig' => 'origen_afectacion_alamedida'), 'orig.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('lecmedinst' => 'lectura_medidor_instalado'), 'lecmedinst.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array('*'), 'left');
//
//            $select->join(array('avi' => 'avisos'),'avi.id_aviso = ors_riat.avisos_id_aviso', array('*'));
//
//            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
//
//            $select->join(array('tecpers' => 'personal'), 'tecpers.id_persona = avi.responsable_personal_id_persona', array(
//                'nombres_tec' => 'nombres',
//                'apellidos_tec' => 'apellidos',
//                'email_tec' => 'email',
//                'rut_persona_tec' => 'rut_persona',
//                'foto_carnet_tec' => 'foto_carnet',
//                    ), 'left');
//
//            
//            $select->join(array('int' => 'instalaciones'), 'int.id_instalacion = avi.instalaciones_id_instalacion', array('*'));
//            $select->join(array('dist' => 'distribuidoras'), 'dist.id_distribuidora = int.distribuidoras_id_distribuidora', array('*'));
//            
//            $select->join(array('com' => 'comunas'), 'com.id_comuna = int.comunas_id_comuna', array('provincias_id_provincia' => 'provincias_id_provincia'), 'left');
//            $select->join(array('pro' => 'provincias'), 'pro.id_provincia = com.provincias_id_provincia', array('regiones_id_region' => 'regiones_id_region'), 'left');
//            //$select->join(array('reg' => 'regiones'), 'reg.id_region = pro.regiones_id_region', array('id_region' => 'id_region'),'left');
//            $select->join(array('anem' => 'antecedentes_empalme'), 'anem.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('rel' => 'reloj'), 'rel.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('reg' => 'regletas'), 'reg.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('tel' => 'telemedida'), 'tel.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('sisdis' => 'sistema_distribucion'), 'sisdis.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//            $select->join(array('e' => 'ecm'), 'e.instalaciones_id_instalacion = int.id_instalacion', array('*'), 'left');
//
//            $select->join(array('cliente' => 'contacto_autoriza'), 'cliente.ors_riat_id_ors_riat = ors_riat.id_ors_riat', array(
//                'nombre_contacto' => 'nombre_contacto',
//                'run' => 'run',
//                'fono' => 'fono',
//                'email' => 'email',
//                    ), 'left');
//
//            $select->join(array('digper' => 'personal'), 'digper.id_persona = ors_riat.digitador_personal_id_persona', array(
//                'nombres_dig' => 'nombres',
//                'apellidos_dig' => 'apellidos',
//                'email_dig' => 'email',
//                'rut_persona_dig' => 'rut_persona',
//                'foto_carnet_dig' => 'foto_carnet',
//                    ), 'left');
//
//            
//            if($t===1){
//            
//                $select->where(array( 'avi.numero_aviso' => $numAviso ));
//
//            }else if($t===2){
//                
//                $select->where(array( 'avi.id_aviso' => $numAviso ));
//                
//            }
//            
//            $rowSet = $this->tableGateway->selectWith($select);
//            $row = $rowSet->current();
//
//            if (!$row) {
//                throw new \Exception("Could not find row $numAviso");
//            }
//
//            return $row;
//        }

        
    public function obtenerPorIdaviso($idaviso) {

            $select = $this->tableGateway->getSql()->select();
            $select->where(array('avisos_id_aviso' => $idaviso));
            $select->where->in('estados_ors_id_estado_ors', array(1,2,3));
            
            $rowset = $this->tableGateway->selectWith($select);
            $row = $rowset->current();
            return $row;
            
        }
    

        
    public function insertNuevoOrsRim(OrsRim $ors_rim) {

            $data = array(
                      'estados_ors_id_estado_ors' => $ors_rim->getEstados_ors_id_estado_ors(),
                      'avisos_id_aviso' => $ors_rim->getAvisos_id_aviso(),
                      );

            /*Solo si no tiene una orden de registro pendiente se creara este nuevo registro*/
            if (! $this->obtenerPorIdaviso($ors_rim->getAvisos_id_aviso()) )
            {
                
              $this->tableGateway->insert($data);
              
             }
            
        }

//    public function obtenerRegiones() {
//
//            $sql = new Sql($this->tableGateway->getAdapter());
//            $select = $sql->select();
//            $select->from('regiones');
//
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $results = $statement->execute();
//            $regiones = new \ArrayObject();
//
//            foreach ($results as $row) {
//                $region = new \Avisos\Model\Entity\Region();
//                $region->exchangeArray($row);
//                $regiones->append($region);
//            }
//
//            return $regiones;
//        }

//    public function obtenerRegionesSelect() {
//
//            $regiones = $this->obtenerRegiones();
//
//            $result = array();
//
//            $result[0] = 'Seleccionar';
//            foreach ($regiones as $reg) {
//
//                $result[$reg->getId_region()] = $reg->getNombre_region();
//            }
//
//            return $result;
//        }

//    public function obtenerProvincias($regId) {
//
//            $sql = new Sql($this->tableGateway->getAdapter());
//            $select = $sql->select();
//            $select->from('provincias');
//            $select->where(array('regiones_id_region' => $regId));
//
//
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $results = $statement->execute();
//            $provincias = new \ArrayObject();
//
//            foreach ($results as $row) {
//                $provincia = new \Avisos\Model\Entity\Provincia();
//                $provincia->exchangeArray($row);
//                $provincias->append($provincia);
//            }
//
//            $result = array();
//
//            $result[0] = 'Seleccionar';
//            foreach ($provincias as $reg) {
//                $result[$reg->getId_provincia()] = $reg->getNombre_provincia();
//            }
//
//            return $result;
//        }

//    public function obtenerComunas($proId) {
//
//            $sql = new Sql($this->tableGateway->getAdapter());
//            $select = $sql->select();
//            $select->from('comunas');
//            $select->where(array('provincias_id_provincia' => $proId));
//
//            $statement = $sql->prepareStatementForSqlObject($select);
//            $results = $statement->execute();
//            $comunas = new \ArrayObject();
//
//            foreach ($results as $row) {
//                $comuna = new \Avisos\Model\Entity\Comuna();
//                $comuna->exchangeArray($row);
//                $comunas->append($comuna);
//            }
//
//            $result = array();
//
//            $result[0] = 'Seleccionar';
//            foreach ($comunas as $reg) {
//                $result[$reg->getId_comuna()] = $reg->getNombre_comuna();
//            }
//
//            return $result;
//        }

//    public function updateOrsRiat(OrsRiat $orsRiat) {
//
//            $data = array(
//                            'num_form_riat' => (int) $orsRiat->getNum_form_riat(),
//                            'fecha_atencion_servicio' => $orsRiat->getFecha_atencion_servicio(),
//                            'estado_riat' => (int) $orsRiat->getEstado_riat(),
//
//                            /*Esto podria ir en la tabla de irregularidades como codigos esperamos a migrar*/
//                            'anor_encajadeempalme' => (int) $orsRiat->getAnor_encajadeempalme(),
//                            'anor_enecm' => (int) $orsRiat->getAnor_enecm(),
//                            'anor_enmedidor' => (int) $orsRiat->getAnor_enmedidor(),
//                            'anor_esquemademedida' => (int) $orsRiat->getAnor_esquemademedida(),
//                            'anor_faltamedidareactiva' => (int) $orsRiat->getAnor_faltamedidareactiva(),
//                            'anor_alarmadebateria' => (int) $orsRiat->getAnor_alarmadebateria(),
//                            'anor_cargabajavariable' => (int) $orsRiat->getAnor_cargabajavariable(),
//                            'anor_tarifanocorresponde' => (int) $orsRiat->getAnor_tarifanocorresponde(),
//                            'anor_faltasello' => (int) $orsRiat->getAnor_faltasello(),
//                            'anor_ttcc' => (int) $orsRiat->getAnor_ttcc(),
//                            'anor_otro' => (int) $orsRiat->getAnor_otro(),
//                            'clas_cnr_1' => (int) $orsRiat->getClas_cnr_1(),
//                            'clas_cnr_2' => (int) $orsRiat->getClas_cnr_2(),
//                            'clas_cnr_3' => (int) $orsRiat->getClas_cnr_3(),
//                            'clas_cnr_4' => (int) $orsRiat->getClas_cnr_4(),
//                            'clas_cnr_5' => (int) $orsRiat->getClas_cnr_5(),
//                            'clas_cnr_6' => (int) $orsRiat->getClas_cnr_6(),
//                            'clas_cnr_7' => (int) $orsRiat->getClas_cnr_7(),
//                            'clas_cnr_8' => (int) $orsRiat->getClas_cnr_8(),
//                            'clas_cnr_9' => (int) $orsRiat->getClas_cnr_9(),
//                            'tecnico2_personal_id_persona' => (int) $orsRiat->getTecnico2_personal_id_persona(),
//                         );
//
//            $id = (int) $orsRiat->getId_ors_riat();
//
//            if ($this->obtenerPorId($id)) {
//
//                $this->tableGateway->update($data, array('id_ors_riat' => $id));
//            } else {
//                throw new \Exception('Form id does not exist');
//            }
//        }

//    public function updateOrsRiatComprometido(OrsRiat $orsRiat) {
//
//            $data = array(
//                'fecha_comprometido' => $orsRiat->getFecha_comprometido(),
//                'estados_ors_id_estado_ors' => $orsRiat->getEstados_ors_id_estado_ors(),
//            );
//
//            $id = (int) $orsRiat->getId_ors_riat();
//
//            if ($this->obtenerPorId($id)) {
//
//                $this->tableGateway->update($data, array('id_ors_riat' => $id));
//            } else {
//                throw new \Exception('Form id does not exist');
//            }
//        }
        
//    public function updateOrsRiatFinalizados(OrsRiat $orsRiat) {
//
//            $data = array(
//                'fecha_entrega_dig' => $orsRiat->getFecha_entrega_dig(),
//                'fecha_entrega_fisica' => $orsRiat->getFecha_entrega_fisica(),
//                'estados_ors_id_estado_ors' => $orsRiat->getEstados_ors_id_estado_ors(),
//            );
//
//            $id = (int) $orsRiat->getId_ors_riat();
//
//            if ($this->obtenerPorId($id)) {
//
//                $this->tableGateway->update($data, array('id_ors_riat' => $id));
//                
//            } else {
//                throw new \Exception('Form id does not exist');
//            }
//        }     

//    public function updateOrsRiatAsignarDigitador(OrsRiat $orsRiat) {
//
//            $personal_id_personal = $this->login->personal_id_personal;
//
//            $data = array(
//                'fecha_asignacion_riat' => $orsRiat->getFecha_asignacion_riat(),
//                'estados_ors_id_estado_ors' => $orsRiat->getEstados_ors_id_estado_ors(),
//                'digitador_personal_id_persona' => $personal_id_personal,
//            );
//
//            $id = (int) $orsRiat->getId_ors_riat();
//
//            if ($this->obtenerPorId($id)) {
//
//                $this->tableGateway->update($data, array('id_ors_riat' => $id));
//            } else {
//                throw new \Exception('Form id does not exist');
//            }
//        }
    
//    public function obtenerEstados() {
//
//        $sql = new Sql($this->tableGateway->getAdapter());
//        $select = $sql->select();
//        $select->from('estados_ors');
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $results = $statement->execute();
//        $estados_riat = new \ArrayObject();
//
//        foreach ($results as $row) {
//
//            $estado = new \Riat\Model\Entity\EstadoOrs;
//            $estado->exchangeArray($row);
//            $estados_riat->append($estado);
//            
//        }
//
//        return $estados_riat;
//        
//    }

//    public function obtenerEstadosSelect() {
//
//        $estados_riat = $this->obtenerEstados();
//
//        $result = array();
//
//        foreach ($estados_riat as $estado) {
//
//            $result[$estado->getId_estado_ors()] = ucwords(strtolower($estado->getDescripcion_estado_ors()));
//            
//         }
//
//        return $result;
//        
//      }    
        
      
//      public function obtenerMedidores() {
//
//        $sql = new Sql($this->tableGateway->getAdapter());
//        $select = $sql->select();
//        $select->from('modelos_medidores_new');
//
//        $statement = $sql->prepareStatementForSqlObject($select);
//        $results = $statement->execute();
//        $medidores = new \ArrayObject();
//
//        foreach ($results as $row) {
//
//            $medidores->append($row);
//        }
//
//        return $medidores;
//    }
    

 }
