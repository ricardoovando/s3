<?php

namespace Rim\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Paginator;

/* ENTITYS */
use Rim\Model\Entity\ejecutados;

class ejecutadosDao {

    protected $tableGateway;
    protected $login;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway, $login , Adapter $dbAdapter) {
            $this->tableGateway = $tableGateway;
            $this->login = $login->getIdentity();
            $this->dbAdapter = $dbAdapter;
        }

    public function obtenerTodos($datosFiltran) {
            
            $id_usuario = $this->login->id;
            
            $select = $this->tableGateway->getSql()->select();
            
            $select->join(array('cam' => 'catalogo_anormalidades_monofasico'), 'cam.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_0',
                                                             array('codigo_anormalidad_0' => 'codigo_anormalidad',
                                                                         'componente_0' => 'componente', 
                                                                         'descripcion_anormalidad_0' => 'descripcion_anormalidad',
                                                                         'tipo_anormalidad_0' => 'tipo_anormalidad',
                                                                         'ubicacion_anormalidad_0' => 'ubicacion_anormalidad' ) ,'left');  

            $select->join(array('avi' => 'avisos'),'avi.id_aviso = rim_ejecutados_tablet.idaviso', array('*'),'left');
            
            $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
            
            $select->join(array('suc' => 'sucursales'),'suc.id_sucursal = avi.sucursales_id_sucursal', array('*'));
            
            $select->join(array('ftr' => 'fotos_terreno_rim'),'ftr.idaviso = rim_ejecutados_tablet.idaviso', array('*'));

            $select->join(array('flm' => 'fotos_lab_rim'),'flm.idaviso = rim_ejecutados_tablet.idaviso', array( 'fotolab1' => 'fotolab1',
                                                                                                                'fotolab2' => 'fotolab2',
                                                                                                                'fotolab3' => 'fotolab3',
                                                                                                                'fotolab4' => 'fotolab4',
                                                                                                                'fotolab5' => 'fotolab5',
                                                                                                                'fotolab6' => 'fotolab6',
                                                                                                                'fotolab7' => 'fotolab7',
                                                                                                                'fotolab8' => 'fotolab8',
                                                                                                                'fotolab9' => 'fotolab9',
                                                                                                                'fotolab10' => 'fotolab10'
                                                                                                                                            ),'left');
            
            

            if ((!empty($datosFiltran["tipobusqueda"]))){
                
                if($datosFiltran["tipobusqueda"]==1){
                     $select->where(array('rim_ejecutados_tablet.num_avi' => (String) $datosFiltran["buscar_numero"]));  
                }
                
                if($datosFiltran["tipobusqueda"]==2){
                    $select->where(array('rim_ejecutados_tablet.num_insta' => (String) $datosFiltran["buscar_numero"]));
                }
                
                if($datosFiltran["tipobusqueda"]==3){
                   $select->where(array('rim_ejecutados_tablet.cote1' => (Int) $datosFiltran["buscar_numero"])); 
                }

                if($datosFiltran["tipobusqueda"]==4){
                   $select->where(array('rim_ejecutados_tablet.nu_se' => (Int) $datosFiltran["buscar_numero"])); 
                }
                
            }else{

               //$select->where(array( new \Zend\Db\Sql\Predicate\IsNull('rim_ejecutados_tablet.estado_rim'))); 

            }
            
            
            if ((!empty($datosFiltran["buscar_por_servicio_ejecutado"]))){
                
               $select->where(array('rim_ejecutados_tablet.ti_serv' => (Int) $datosFiltran["buscar_por_servicio_ejecutado"])); 
                
            } 
         
 
            if ((!empty($datosFiltran["buscar_por_resultado"]))){
                
               $select->where(array('cam.tipo_anormalidad' => (String) $datosFiltran["buscar_por_resultado"])); 
                
            }


           /*Ordenar Registro*/
           $select->order('rim_ejecutados_tablet.fe_eje DESC');     
       
           $select->order('rim_ejecutados_tablet.num_avi');

      	  
           /*echo '<pre>';
           echo $select->getSqlString();
           exit;*/

                  
           $dbAdapter = $this->tableGateway->getAdapter();
           $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

           $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
           $paginator = new Paginator($adapter);
           return $paginator;


         }

        
    public function obtenerPorIdaviso($idAviso) {
            
            $id_usuario = $this->login->id;
            $id = (int) $idAviso;
            
            $select = $this->tableGateway->getSql()->select();

			$select->join(array('cam_0' => 'catalogo_anormalidades_monofasico'), 'cam_0.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_0', 
                                                              array('codigo_anormalidad_0' => 'codigo_anormalidad',
                                                                         'componente_0' => 'componente', 
                                                                         'descripcion_anormalidad_0' => 'descripcion_anormalidad',
                                                                         'tipo_anormalidad_0' => 'tipo_anormalidad',
                                                                         'ubicacion_anormalidad_0' => 'ubicacion_anormalidad' ) ,'left');               

            $select->join(array('cam_1' => 'catalogo_anormalidades_monofasico'), 'cam_1.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_1', 
                                                              array('codigo_anormalidad_1' => 'codigo_anormalidad',
                                                                         'componente_1' => 'componente', 
                                                                         'descripcion_anormalidad_1' => 'descripcion_anormalidad',
                                                                         'tipo_anormalidad_1' => 'tipo_anormalidad',
                                                                         'ubicacion_anormalidad_1' => 'ubicacion_anormalidad' ) ,'left');  

            $select->join(array('cam_2' => 'catalogo_anormalidades_monofasico'), 'cam_2.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_2', 
                                                              array('codigo_anormalidad_2' => 'codigo_anormalidad',
                                                                         'componente_2' => 'componente', 
                                                                         'descripcion_anormalidad_2' => 'descripcion_anormalidad',
                                                                         'tipo_anormalidad_2' => 'tipo_anormalidad',
                                                                         'ubicacion_anormalidad_2' => 'ubicacion_anormalidad' ) ,'left'); 
 
          $select->join(array('cam_3' => 'catalogo_anormalidades_monofasico'), 'cam_3.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_3', 
                                                              array('codigo_anormalidad_3' => 'codigo_anormalidad',
                                                                         'componente_3' => 'componente', 
                                                                         'descripcion_anormalidad_3' => 'descripcion_anormalidad',
                                                                         'tipo_anormalidad_3' => 'tipo_anormalidad',
                                                                         'ubicacion_anormalidad_3' => 'ubicacion_anormalidad' ) ,'left');

           $select->join(array('com' => 'comunas'), 'com.id_comuna = rim_ejecutados_tablet.com',array('*'),'left');

           $select->join(array('med_encontrado' => 'modelos_medidores_new'), 'med_encontrado.codigo_modelo = rim_ejecutados_tablet.co_me',
                                                                     array('marca_m_enc' => 'marca' , 
                                                                                'modelo_m_enc'  => 'modelo' , 
                                                                                'norma_medida_enc' => 'norma_medida' , 
                                                                                'clase_medidor_enc' => 'clase_medidor',
                                                                                'voltaje_enc' => 'voltaje',     
                                                                                'amperes_enc' => 'amperes',
                                                                                'kp_enc' => 'kp'   
                                                                                ),'left');

           $select->join(array('med_dejado' => 'modelos_medidores_new'), 'med_dejado.codigo_modelo = rim_ejecutados_tablet.co_me_ins',
                                                                     array('marca_m_dej' => 'marca' , 
                                                                                'modelo_m_dej'  => 'modelo',
                                                                                'norma_medida_dej' => 'norma_medida' , 
                                                                                'clase_medidor_dej' => 'clase_medidor',
                                                                                'voltaje_dej' => 'voltaje',     
                                                                                'amperes_dej' => 'amperes',
                                                                                'kp_dej' => 'kp' 
                                                                                ),'left');

           $select->join(array('persona_tec1' => 'personal'), 'persona_tec1.id_persona = rim_ejecutados_tablet.cote1',
                                                                     array('nombre_tec1' => 'nombres' , 
                                                                               'apellido_tec1'  => 'apellidos' , 
                                                                               'rut_tec1' => 'rut_persona' , 
                                                                               'foto_firma1' => 'foto_firma'),'left');
 
           $select->join(array('persona_tec2' => 'personal'), 'persona_tec2.id_persona = rim_ejecutados_tablet.cote2',
                                                                     array(  'nombre_tec2' => 'nombres' , 
                                                                                 'apellidos_tec2'  => 'apellidos' , 
                                                                                 'rut_tec2' => 'rut_persona',
                                                                                 'foto_firma2' => 'foto_firma'),'left');

           $select->join(array('avi' => 'avisos'),'avi.id_aviso = rim_ejecutados_tablet.idaviso',array('*'),'left');

           $select->join(array('soli' => 'solicitantes'),'soli.id_solicitante_distribuidora = avi.solicitante_id_solicitante', array('*'));

	       $select->join(array('dist' => 'distribuidoras'),'dist.id_distribuidora = soli.distribuidoras_id_distribuidora', array('*'));
						
           $select->join(array('inst' => 'instalaciones'),'inst.id_instalacion = avi.instalaciones_id_instalacion', array('*'));
            			
           $select->join(array('ussuc' => 'usuario_sucursal'), new \Zend\Db\Sql\Expression('ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = ' . $id_usuario . ''), array());
            
           $select->join(array('persona_supervisor' => 'personal'), new \Zend\Db\Sql\Expression("persona_supervisor.sucursales_id_sucursal = avi.sucursales_id_sucursal AND persona_supervisor.cargos_idcargo = 2" ),
                                                                     array( 'nombres_supervisor' => 'nombres' , 
                                                                                 'apellidos_supervisor'  => 'apellidos' , 
                                                                                 'rut_supervisor' => 'rut_persona',
                                                                                 'foto_firma_supervisor' => 'foto_firma'),'left');   

          
		   $select->join(array('ftr' => 'fotos_terreno_rim'),'ftr.idaviso = rim_ejecutados_tablet.idaviso', array('*'),'left');

           $select->join(array('flm' => 'fotos_lab_rim'),'flm.idaviso = rim_ejecutados_tablet.idaviso', array('*'),'left');
           
		   
		   
           $select->where(array('rim_ejecutados_tablet.idaviso' => $id));
   
           /*echo '<pre>';
           echo $select->getSqlString();
           exit;*/

            $rowset = $this->tableGateway->selectWith($select);
            $row = $rowset->current();

            if (!$row) {
                throw new \Exception("Could not find row $id");
            }
            
            return $row;

        }

        
    public function insertNuevoOrsRim(OrsRim $ors_rim) {

            $data = array(
                      'estados_ors_id_estado_ors' => $ors_rim->getEstados_ors_id_estado_ors(),
                      'avisos_id_aviso' => $ors_rim->getAvisos_id_aviso(),
                      );

            /*Solo si no tiene una orden de registro pendiente se creara este nuevo registro*/
            if (! $this->obtenerPorIdaviso($ors_rim->getAvisos_id_aviso()) )
            {
                
              $this->tableGateway->insert($data);
              
             }
            
        }

  
    public function guardar($ejecutados) { 

        $respuesta = true;
	  
        try {

            /*var_dump($ejecutados);
            exit;*/

           $this->tableGateway->insert($ejecutados);

        } catch (\Zend\Db_Adapter\Exception $e) {
              
            $respuesta = $e->getMessage();
            
        } catch (Exception $e) {

           $respuesta = $e->getMessage(); 
       }

        return $respuesta;
                   
     } 
     
	 
 
    
     /************************************
      * 
      * 
      *    INSERTAR FOTOS EN TABLA NUEVA
      * 
      * 
      ***********************************/
     
     public function insertarFotosTerrenoRim($dat) {

            $id = (int) $dat['idaviso'];
            
            $foto1 = (empty($dat['foto1']))?null:$dat['foto1'];
            $foto2 = (empty($dat['foto2']))?null:$dat['foto2'];
            $foto3 = (empty($dat['foto3']))?null:$dat['foto3'];
            $foto4 = (empty($dat['foto4']))?null:$dat['foto4'];
            $foto5 = (empty($dat['foto5']))?null:$dat['foto5'];
            $foto6 = (empty($dat['foto6']))?null:$dat['foto6'];
            $foto7 = (empty($dat['foto7']))?null:$dat['foto7'];
            $foto8 = (empty($dat['foto8']))?null:$dat['foto8'];
            $foto9 = (empty($dat['foto9']))?null:$dat['foto9'];
            $foto10 = (empty($dat['foto10']))?null:$dat['foto10'];

            try {


                    $statement = $this -> dbAdapter -> query("DELETE FROM servicio_rim.fotos_terreno_rim WHERE idaviso = {$id};"); 
                    $results = $statement -> execute();

                    $statement = $this -> dbAdapter -> query("INSERT INTO servicio_rim.fotos_terreno_rim
                                                                                            (foto1,foto2,foto3,foto4,foto5,foto6,foto7,foto8,foto9,foto10,idaviso)
                                                                                            VALUES ('{$foto1}','{$foto2}','{$foto3}','{$foto4}','{$foto5}','{$foto6}','{$foto7}','{$foto8}','{$foto9}','{$foto10}',{$id});");
                    $results = $statement -> execute();
        
                    return true;
            
            } catch (Exception $e) {

                    /*echo 'Excepción Message : '. $e -> getMessage()."\n";
                    echo 'Excepción Code : '. $e -> getCode()."\n";
                    echo 'Excepción File : '. $e -> getFile()."\n";
                    echo 'Excepción Line : '. $e -> getLine()."\n";*/

              exit;

            }
            
        } 


        
        
     /*********************************
      * 
      *   INSERTAR FOTOS LABORATORIO
      * 
      *******************************/
        
     public function insertarFotosLabRim($dat) {

            $id = (int) $dat['idaviso'];
            
            $fotoLab1 = (empty($dat['fotoLab1']))?null:$dat['fotoLab1'];
            $fotoLab2 = (empty($dat['fotoLab2']))?null:$dat['fotoLab2'];
            $fotoLab3 = (empty($dat['fotoLab3']))?null:$dat['fotoLab3'];
            $fotoLab4 = (empty($dat['fotoLab4']))?null:$dat['fotoLab4'];
            $fotoLab5 = (empty($dat['fotoLab5']))?null:$dat['fotoLab5'];
            $fotoLab6 = (empty($dat['fotoLab6']))?null:$dat['fotoLab6'];
            $fotoLab7 = (empty($dat['fotoLab7']))?null:$dat['fotoLab7'];
            $fotoLab8 = (empty($dat['fotoLab8']))?null:$dat['fotoLab8'];
            $fotoLab9 = (empty($dat['fotoLab9']))?null:$dat['fotoLab9'];
            $fotoLab10 = (empty($dat['fotoLab10']))?null:$dat['fotoLab10'];

            try {

                
                $statement = $this->dbAdapter->query("SELECT * FROM servicio_rim.fotos_lab_rim WHERE idaviso = {$id};");
		$results = $statement->execute();
		foreach ($results as $value) {
			$result = $value;
		}
                
                if(!empty($result)){
                    $statement = $this -> dbAdapter -> query("DELETE FROM servicio_rim.fotos_lab_rim WHERE idaviso = {$id};"); 
                    $results = $statement -> execute();
                  }
                    
                    $statement = $this -> dbAdapter -> query("INSERT INTO servicio_rim.fotos_lab_rim
                                                             (fotolab1,fotolab2,fotolab3,fotolab4,fotolab5,fotolab6,fotolab7,fotolab8,fotolab9,fotolab10,idaviso)
                                                             VALUES ('{$fotoLab1}','{$fotoLab2}','{$fotoLab3}','{$fotoLab4}','{$fotoLab5}','{$fotoLab6}','{$fotoLab7}','{$fotoLab8}','{$fotoLab9}','{$fotoLab10}',{$id});");
                    $results = $statement -> execute();
        
                    return true;
            
            } catch (Exception $e) {

                    /*echo 'Excepción Message : '. $e -> getMessage()."\n";
                    echo 'Excepción Code : '. $e -> getCode()."\n";
                    echo 'Excepción File : '. $e -> getFile()."\n";
                    echo 'Excepción Line : '. $e -> getLine()."\n";*/

              exit;

            }
            
        } 
        
        
        
     
      public function updateNumeroRim($dat) {    

            $id_usuario = $this->login->id;
            $id = (int) $dat['idaviso'];
			
			$data = array(
			
                            'numero_rim' => (empty(trim($dat['numero_rim']))) ? NULL : $dat['numero_rim'], 
				 
                           	'fe_solicitud' => $dat["fecha_solicitud"],
				 				 
                           	'fe_eje' => $dat["fecha_ejecucion"],
							'h_ini' => $dat["hora_inicio"],
							'h_te' => $dat["hora_termino"],

							'ti_serv' => (empty(trim($dat['tipo_servicio']))) ? NULL : $dat['tipo_servicio'], 

                            'se_pe' => (empty(trim($dat['peritaje']))) ? NULL : $dat['peritaje'], 

							'ti_bri' => (empty(trim($dat['tipo_brigada']))) ? NULL : $dat['tipo_brigada'], 
							'cote2' => (empty(trim($dat['codigo_ayudante']))) ? NULL : $dat['codigo_ayudante'], 
							'pat_mo' => $dat["patente"],
							
                            'num_avi' => $dat["numero_aviso"],
                            'codigo_distri' => (empty(trim($dat['distribuidora']))) ? NULL : $dat['distribuidora'],
                            'nom_cli' => $dat["nombre_cliente"],
                            'tel' => $dat["telefono_cliente"],
                            'dir1' => $dat["dir1_cliente"],
                            'dir2' => $dat["dir2_cliente"],
                            'num_insta' => $dat["num_instalacion"],

							'ti_emp' => (empty(trim($dat['tipo_empalme']))) ? NULL : $dat['tipo_empalme'],
							'r_dis' => (empty(trim($dat['tipo_red']))) ? NULL : $dat['tipo_red'],
							'ti_aco' => (empty(trim($dat['tipo_acometida']))) ? NULL : $dat['tipo_acometida'],
							'cod_insp_emp' => (empty(trim($dat['tipo_condicion_empalme']))) ? NULL : $dat['tipo_condicion_empalme'],
							
							'co_me' => (empty(trim($dat['codigo_medidor']))) ? NULL : $dat['codigo_medidor'],
							'nu_se' => (empty(trim($dat['numero_serie']))) ? NULL : $dat['numero_serie'],
							'an_fa' => (empty(trim($dat['anio_fabricacion']))) ? NULL : $dat['anio_fabricacion'],
							'lec_ini' => (empty(trim($dat['lectura_inicial']))) ? NULL : $dat['lectura_inicial'],
							'lec_fin' => (empty(trim($dat['lectura_final']))) ? NULL : $dat['lectura_final'],
							'pla_pos' => (empty(trim($dat['placa_poste']))) ? NULL : $dat['placa_poste'],
							'auto' => $dat["automatico"],
							
							'ma_emp_int_automatico' => $dat["mant_automatico"],
							'ma_emp_acometida' => $dat["mant_acometida"],
							'ma_emp_mastil' => $dat["mant_mastil"],
							'man_emp_mirilla' => $dat["mant_mirilla"],
							'ma_emp_bajada' => $dat["mant_bajada"],
							'ma_emp_caja_empalme' => $dat["mant_caja"],
							'ma_emp_alambrado' => $dat["mant_alambrado"],
							'man_emp_tapa' => $dat["mant_tapa"],
							
							'vo_fn' => $dat["voltaje_fn"],
							'vo_nt' => $dat["voltaje_nt"],
							'vo_ft' => $dat["voltaje_ft"],
							'co_in' => $dat["corr_instantanea"],
							'bf_11' => (empty(trim($dat['prueba_bc_1']))) ? NULL : $dat['prueba_bc_1'],
							'bf_12' => (empty(trim($dat['prueba_bc_2']))) ? NULL : $dat['prueba_bc_2'],
							'pf_11' => (empty(trim($dat['prueba_pc_1']))) ? NULL : $dat['prueba_pc_1'],
							'pf_12' => (empty(trim($dat['prueba_pc_2']))) ? NULL : $dat['prueba_pc_2'],
							'vr_er1' => (empty(trim($dat['verif_rap_1']))) ? NULL : $dat['verif_rap_1'],
							'vr_er2' => (empty(trim($dat['verif_rap_2']))) ? NULL : $dat['verif_rap_2'],
							
							'pa_se' => $dat["serie_patron"],
							'pa_ma' => (empty(trim($dat['marca_patron']))) ? NULL : $dat['marca_patron'],
							'pa_mo' => (empty(trim($dat['modelo_patron']))) ? NULL : $dat['modelo_patron'],
														
							'cu_en' => $dat["cubierta_enc"],
							'bl_en' => $dat["block_enc"],
							'cm_en' => $dat["caja_enc"],
							'cu_de' => $dat["cubierta_dej"],
							'bl_de' => $dat["block_dej"],
							'cm_de' => $dat["caja_dej"],
														
							'co_me_ins' => (empty(trim($dat['cod_medidor_dej']))) ? NULL : $dat['cod_medidor_dej'],
							'nu_se_ins' => (empty(trim($dat['num_serie_medidor_dej']))) ? NULL : $dat['num_serie_medidor_dej'],
							'a_fa_ins' => (empty(trim($dat['anio_fabricacion_medidor_dej']))) ? NULL : $dat['anio_fabricacion_medidor_dej'],
							'le_ini_ins' => (empty(trim($dat['lectura_inicial_medidor_dej']))) ? NULL : $dat['lectura_inicial_medidor_dej'],
							'es_ins' => (empty(trim($dat['estado_medidor_dej']))) ? NULL : $dat['estado_medidor_dej'],
							
							'no_res' => $dat["nombre_residente"],
							'ru_res' => $dat["rut_residente"],
							'te_res' => $dat["telefono_residente"],
							
							'co_fin' => $dat["comentario_final"],
                           	
                            'res_ev_ins_0' => $dat['res_ev_ins_0'], 
                            'res_ev_ins_0_detalle' => $dat['res_ev_ins_0_detalle'],
                            'res_ev_ins_1' => $dat['res_ev_ins_1'], 
                            'res_ev_ins_1_detalle' => $dat['res_ev_ins_1_detalle'],
                            'res_ev_ins_2' => $dat['res_ev_ins_2'], 
                            'res_ev_ins_2_detalle' => $dat['res_ev_ins_2_detalle'],
                            'res_ev_ins_3' => $dat['res_ev_ins_3'], 
                            'res_ev_ins_3_detalle' => $dat['res_ev_ins_3_detalle'],

                            'id_persona_actualiza' => $id_usuario,
                            'fecha_actualiza' => $dat['fecha_actualiza'], 
                            
                         );

            if ($this->obtenerPorIdaviso($id)) {

                $this->tableGateway->update($data, array('idaviso' => $id));
				
            } else {
            	
                throw new \Exception('id does not exist');
				
            }
			
        }
	 
	 
    public function ObtenerCantidadServicios() {
         
         $id_usuario = $this->login->id;
         $id_sucursal = $this->login->sucursales_id_sucursal;
         
         $statement = $this->dbAdapter->query("SELECT 'cm' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 197
                                                                                UNION ALL
                                                                                SELECT 'ins' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 198
                                                                                UNION ALL
                                                                                SELECT 'vcc' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 199
                                                                                UNION ALL
                                                                                SELECT 'vsc' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 200
                                                                                UNION ALL
                                                                                SELECT 'vf' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 201              
                                                                                UNION ALL
                                                                                SELECT 'vm' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 213 
                                                                                UNION ALL
                                                                                SELECT 'rei' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso 
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 214     
                                                                                UNION ALL
                                                                                SELECT 'dev' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso  
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}  
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 215
                                                                                UNION ALL
                                                                                SELECT 'auto' AS nombre_servicios,
                                                                                COUNT(rim_ejecutados_tablet.ti_serv) AS cantidad_servicios
                                                                                FROM rim_ejecutados_tablet
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso
                                                                                JOIN usuario_sucursal us ON us.sucursales_id_sucursal = avi.sucursales_id_sucursal AND us.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE rim_ejecutados_tablet.ti_serv = 216");

         $results = $statement->execute();
         $result = array(); 

         foreach ($results as $value) {
            $result[] = $value;    
         }

         return $result;
            
      }
      
	public function ObtenerCantidadResultados() {
         
         $id_usuario = $this->login->id;
         $id_sucursal = $this->login->sucursales_id_sucursal;
         
         $statement = $this->dbAdapter->query("SELECT 
                                                                                'NORMAL' AS tiporesultado,
                                                                                COUNT(rim_ejecutados_tablet.idaviso) AS cantidad_resultado
                                                                                FROM rim_ejecutados_tablet
                                                                                LEFT JOIN catalogo_anormalidades_monofasico cam ON cam.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_0
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso 
                                                                                JOIN usuario_sucursal ussuc ON ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE cam.tipo_anormalidad = 'NORMAL' 
                                                                                UNION ALL
                                                                                SELECT 'CNR' AS tiporesultado,
                                                                                COUNT(rim_ejecutados_tablet.idaviso) AS cantidad_resultado
                                                                                FROM rim_ejecutados_tablet
                                                                                LEFT JOIN catalogo_anormalidades_monofasico cam ON cam.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_0
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso 
                                                                                JOIN usuario_sucursal ussuc ON ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE cam.tipo_anormalidad = 'CNR'     
                                                                                UNION ALL
                                                                                SELECT 'SUBESTANDAR' AS tiporesultado,
                                                                                COUNT(rim_ejecutados_tablet.idaviso) AS cantidad_resultado
                                                                                FROM rim_ejecutados_tablet
                                                                                LEFT JOIN catalogo_anormalidades_monofasico cam ON cam.codigo_anormalidad = rim_ejecutados_tablet.res_ev_ins_0
                                                                                JOIN avisos avi ON avi.id_aviso = rim_ejecutados_tablet.idaviso 
                                                                                JOIN usuario_sucursal ussuc ON ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = {$id_usuario}
                                                                                WHERE cam.tipo_anormalidad = 'Cond. Subestandar';");

         $results = $statement->execute();
         $result = array(); 

         foreach ($results as $value) {
            $result[] = $value;    
         }
         
         return $result;
            
      }  
	  

	public function ObtenerFrustrados2Visita() {
		$id_usuario = $this->login->id;
		$statement = $this->dbAdapter->query("SELECT COUNT(x.*) as cantidad FROM
											  (SELECT num_avi,count(*) as cantidad
											   FROM
												rim_ejecutados_tablet as rim
												INNER JOIN avisos AS avi ON avi.id_aviso = rim.idaviso
												INNER JOIN usuario_sucursal AS ussuc ON ussuc.sucursales_id_sucursal = avi.sucursales_id_sucursal AND ussuc.usuarios_id_usuario = {$id_usuario}
												WHERE
												rim.ti_serv = 201
												GROUP BY 1 HAVING count(*) > 1) as x;");
		$results = $statement->execute();
		foreach ($results as $value) {
			$result = $value;
		}
	   return $result;
	}

	
	public function finalizarRimEjecutado(ejecutados $ejecutado){

	  $data = array(
            'estado_rim' => $ejecutado->getEstado_rim(),
            'fecha_finalizado' => $ejecutado->getFecha_finalizado(),
            'id_persona_backoffice' => $ejecutado->getId_persona_backoffice(),
           );

        $idAviso = (int) $ejecutado->getIdaviso();
        
        if ($this->obtenerPorIdaviso($idAviso)) {
        	
            $this->tableGateway->update($data, array('idaviso' => $idAviso));
            
        } else {
        	
            throw new \Exception('Form id does not exist');
            
        }
        
    }  
     
    
   /**************************************
     * 
     *        ANORMALIDADES
     * 
     **************************************/
 
     public function obtenerCodigoAnormalidad($codAnor) {
                
        $sql = new Sql($this->tableGateway->getAdapter());
          
        $select = $sql->select();          
        $select->from('catalogo_anormalidades_monofasico');
        $select->where(array('codigo_anormalidad' => $codAnor));
                
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $result = "";
    
                foreach ($results as $reg) {
                    $result = $reg;
                }
        
        return $result;
        
     }


   public function getCodeAbnormalityAll(){

        $sql = new Sql($this->tableGateway->getAdapter());
          
        $select = $sql->select();          
        $select->from('catalogo_anormalidades_monofasico');
                
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;

    }


/*********************************************************************/

        
 }
