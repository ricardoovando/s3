<?php

namespace Rim\Model\Entity;

class AnormalidadesMonofasico {

    private $tipo_anormalidad;
    private $ubicacion_anormalidad;
    private $componente;
    private $codigo_anormalidad;
    private $descripcion_anormalidad;

    function __construct($tipo_anormalidad = null, $ubicacion_anormalidad = null, $componente = null, $codigo_anormalidad = null, $descripcion_anormalidad = null) {
        $this->tipo_anormalidad = $tipo_anormalidad;
        $this->ubicacion_anormalidad = $ubicacion_anormalidad;
        $this->componente = $componente;
        $this->codigo_anormalidad = $codigo_anormalidad;
        $this->descripcion_anormalidad = $descripcion_anormalidad;
    }

    public function getTipo_anormalidad() {
        return $this->tipo_anormalidad;
    }

    public function setTipo_anormalidad($tipo_anormalidad) {
        $this->tipo_anormalidad = $tipo_anormalidad;
    }

    public function getUbicacion_anormalidad() {
        return $this->ubicacion_anormalidad;
    }

    public function setUbicacion_anormalidad($ubicacion_anormalidad) {
        $this->ubicacion_anormalidad = $ubicacion_anormalidad;
    }

    public function getComponente() {
        return $this->componente;
    }

    public function setComponente($componente) {
        $this->componente = $componente;
    }

    public function getCodigo_anormalidad() {
        return $this->codigo_anormalidad;
    }

    public function setCodigo_anormalidad($codigo_anormalidad) {
        $this->codigo_anormalidad = $codigo_anormalidad;
    }

    public function getDescripcion_anormalidad() {
        return $this->descripcion_anormalidad;
    }

    public function setDescripcion_anormalidad($descripcion_anormalidad) {
        $this->descripcion_anormalidad = $descripcion_anormalidad;
    }

    public function exchangeArray($data) {
        $this->tipo_anormalidad = (isset($data['tipo_anormalidad'])) ? $data['tipo_anormalidad'] : null;
        $this->ubicacion_anormalidad = (isset($data['ubicacion_anormalidad'])) ? $data['ubicacion_anormalidad'] : null;
        $this->componente = (isset($data['componente'])) ? $data['componente'] : null;
        $this->codigo_anormalidad = (isset($data['codigo_anormalidad'])) ? $data['codigo_anormalidad'] : null;
        $this->descripcion_anormalidad = (isset($data['descripcion_anormalidad'])) ? $data['descripcion_anormalidad'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
