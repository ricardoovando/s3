<?php

namespace Rim\Model\Entity;

class OrsRim {

     private $id_ors_rim;
     private $estados_ors_id_estado_ors;
     private $avisos_id_aviso;
     private $fecha_asignacion_rim;
     private $fecha_comprometido;
     private $fecha_entrega_dig;
     private $fecha_entrega_fisica;
     private $digitador_personal_id_personal;
     private $numero_form_rim;
     private $fecha_atencion_servicio;
     private $estado_rim;
     private $tecnico2_personal_id_personal;
     private $tipo_brigada;
     private $comentario;

     function __construct($id_ors_rim = null, 
             $estados_ors_id_estado_ors = null, 
             $avisos_id_aviso = null, 
             $fecha_asignacion_rim = null, 
             $fecha_comprometido = null, 
             $fecha_entrega_dig = null, 
             $fecha_entrega_fisica = null, 
             $digitador_personal_id_personal = null, 
             $numero_form_rim = null, 
             $fecha_atencion_servicio = null, 
             $estado_rim = null, 
             $tecnico2_personal_id_personal = null, 
             $tipo_brigada = null, 
             $comentario = null) {
         
         $this->id_ors_rim = $id_ors_rim;
         $this->estados_ors_id_estado_ors = $estados_ors_id_estado_ors;
         $this->avisos_id_aviso = $avisos_id_aviso;
         $this->fecha_asignacion_rim = $fecha_asignacion_rim;
         $this->fecha_comprometido = $fecha_comprometido;
         $this->fecha_entrega_dig = $fecha_entrega_dig;
         $this->fecha_entrega_fisica = $fecha_entrega_fisica;
         $this->digitador_personal_id_personal = $digitador_personal_id_personal;
         $this->numero_form_rim = $numero_form_rim;
         $this->fecha_atencion_servicio = $fecha_atencion_servicio;
         $this->estado_rim = $estado_rim;
         $this->tecnico2_personal_id_personal = $tecnico2_personal_id_personal;
         $this->tipo_brigada = $tipo_brigada;
         $this->comentario = $comentario;
         
     }
     
     public function getId_ors_rim() {
         return $this->id_ors_rim;
     }

     public function setId_ors_rim($id_ors_rim) {
         $this->id_ors_rim = $id_ors_rim;
     }

     public function getEstados_ors_id_estado_ors() {
         return $this->estados_ors_id_estado_ors;
     }

     public function setEstados_ors_id_estado_ors($estados_ors_id_estado_ors) {
         $this->estados_ors_id_estado_ors = $estados_ors_id_estado_ors;
     }

     public function getAvisos_id_aviso() {
         return $this->avisos_id_aviso;
     }

     public function setAvisos_id_aviso($avisos_id_aviso) {
         $this->avisos_id_aviso = $avisos_id_aviso;
     }

     public function getFecha_asignacion_rim() {
         return $this->fecha_asignacion_rim;
     }

     public function setFecha_asignacion_rim($fecha_asignacion_rim) {
         $this->fecha_asignacion_rim = $fecha_asignacion_rim;
     }

     public function getFecha_comprometido() {
         return $this->fecha_comprometido;
     }

     public function setFecha_comprometido($fecha_comprometido) {
         $this->fecha_comprometido = $fecha_comprometido;
     }

     public function getFecha_entrega_dig() {
         return $this->fecha_entrega_dig;
     }

     public function setFecha_entrega_dig($fecha_entrega_dig) {
         $this->fecha_entrega_dig = $fecha_entrega_dig;
     }

     public function getFecha_entrega_fisica() {
         return $this->fecha_entrega_fisica;
     }

     public function setFecha_entrega_fisica($fecha_entrega_fisica) {
         $this->fecha_entrega_fisica = $fecha_entrega_fisica;
     }

     public function getDigitador_personal_id_personal() {
         return $this->digitador_personal_id_personal;
     }

     public function setDigitador_personal_id_personal($digitador_personal_id_personal) {
         $this->digitador_personal_id_personal = $digitador_personal_id_personal;
     }

     public function getNumero_form_rim() {
         return $this->numero_form_rim;
     }

     public function setNumero_form_rim($numero_form_rim) {
         $this->numero_form_rim = $numero_form_rim;
     }

     public function getFecha_atencion_servicio() {
         return $this->fecha_atencion_servicio;
     }

     public function setFecha_atencion_servicio($fecha_atencion_servicio) {
         $this->fecha_atencion_servicio = $fecha_atencion_servicio;
     }

     public function getEstado_rim() {
         return $this->estado_rim;
     }

     public function setEstado_rim($estado_rim) {
         $this->estado_rim = $estado_rim;
     }

     public function getTecnico2_personal_id_personal() {
         return $this->tecnico2_personal_id_personal;
     }

     public function setTecnico2_personal_id_personal($tecnico2_personal_id_personal) {
         $this->tecnico2_personal_id_personal = $tecnico2_personal_id_personal;
     }

     public function getTipo_brigada() {
         return $this->tipo_brigada;
     }

     public function setTipo_brigada($tipo_brigada) {
         $this->tipo_brigada = $tipo_brigada;
     }

     public function getComentario() {
         return $this->comentario;
     }

     public function setComentario($comentario) {
         $this->comentario = $comentario;
     }
         
     public function exchangeArray($data) {

         $this->id_ors_rim = (isset($data['id_ors_rim'])) ? $data['id_ors_rim'] : null;
         $this->estados_ors_id_estado_ors = (isset($data['estados_ors_id_estado_ors'])) ? $data['estados_ors_id_estado_ors'] : null;
         $this->avisos_id_aviso = (isset($data['avisos_id_aviso'])) ? $data['avisos_id_aviso'] : null;
         $this->fecha_asignacion_rim = (isset($data['fecha_asignacion_rim'])) ? $data['fecha_asignacion_rim'] : null;
         $this->fecha_comprometido = (isset($data['fecha_comprometido'])) ? $data['fecha_comprometido'] : null;
         $this->fecha_entrega_dig = (isset($data['fecha_entrega_dig'])) ? $data['fecha_entrega_dig'] : null;
         $this->fecha_entrega_fisica = (isset($data['fecha_entrega_fisica'])) ? $data['fecha_entrega_fisica'] : null;
         $this->digitador_personal_id_personal = (isset($data['digitador_personal_id_personal'])) ? $data['digitador_personal_id_personal'] : null;
         $this->numero_form_rim = (isset($data['numero_form_rim'])) ? $data['numero_form_rim'] : null;
         $this->fecha_atencion_servicio = (isset($data['fecha_atencion_servicio'])) ? $data['fecha_atencion_servicio'] : null;
         $this->estado_rim = (isset($data['estado_rim'])) ? $data['estado_rim'] : null;
         $this->tecnico2_personal_id_personal = (isset($data['tecnico2_personal_id_personal'])) ? $data['tecnico2_personal_id_personal'] : null;
         $this->tipo_brigada = (isset($data['tipo_brigada'])) ? $data['tipo_brigada'] : null;
         $this->comentario = (isset($data['comentario'])) ? $data['comentario'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

