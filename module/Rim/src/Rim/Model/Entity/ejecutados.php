<?php

namespace Rim\Model\Entity;

class ejecutados {

    private $idaviso;
    /* BIGINT */
    private $num_avi;
    /* VARCHAR(30) NOT NULL */
    private $codigo_tipo_servicio;
    /* VARCHAR(30) */
    private $codigo_distri;
    /* INTEGER */
    private $nom_cli;
    /* VARCHAR(60) */
    private $tel;
    /* VARCHAR(20) */
    private $dir1;
    /* VARCHAR(100) */
    private $dir2;
    /* VARCHAR(100) */
    private $com;
    /* INTEGER */
    private $num_insta;
    /* VARCHAR(15) NOT NULL */
    private $ti_emp;
    /* INTEGER */
    private $r_dis;
    /* INTEGER */
    private $ti_aco;
    /* INTEGER */
    private $pla_pos;
    /* NUMERIC(15,0) */
    private $co_me;
    /* INTEGER */
    private $nu_se;
    /* NUMERIC(15,0) */
    private $an_fa;
    /* INTEGER */
    private $lec_ini;
    /* DOUBLE PRECISION */
    private $lec_fin;
    /* DOUBLE PRECISION */
    private $auto;
    /* VARCHAR(10) */
    private $lat;
    /* VARCHAR(100) */
    private $lon;
    /* VARCHAR(100) */
    private $fe_eje;
    /* DATE NOT NULL */
    private $h_ini;
    /* TIME WITHOUT TIME ZONE NOT NULL */
    private $h_te;
    /* TIME WITHOUT TIME ZONE NOT NULL */
    private $ti_serv;
    /* INTEGER NOT NULL */
    private $ti_vi_fru;
    /* INTEGER */
    private $vo_fn;
    /* VARCHAR(6) */
    private $vo_nt;
    /* VARCHAR(6) */
    private $vo_ft;
    /* VARCHAR(6) */
    private $co_in;
    /* VARCHAR(6) */
    private $bf_11;
    /* VARCHAR(6) */
    private $bf_12;
    /* VARCHAR(6) */
    private $pf_11;
    /* VARCHAR(6) */
    private $pf_12;
    /* VARCHAR(6) */
    private $vr_er1;
    /* VARCHAR(6) */
    private $vr_er2;
    /* VARCHAR(6) */
    private $cu_en;
    /* VARCHAR(15) */
    private $bl_en;
    /* VARCHAR(15) */
    private $cm_en;
    /* VARCHAR(15) */
    private $cu_de;
    /* VARCHAR(15) */
    private $bl_de;
    /* VARCHAR(15) */
    private $cm_de;
    /* VARCHAR(15) */
    private $pa_se;
    /* VARCHAR(30) */
    private $pa_ma_mo;
    /* VARCHAR(30) */
    private $co_me_ins;
    /* INTEGER */
    private $le_ini_ins;
    /* DOUBLE PRECISION */
    private $nu_se_ins;
    /* NUMERIC(15,0) */
    private $a_fa_ins;
    /* INTEGER */
    private $es_ins;
    /* INTEGER */
    private $res_ev_ins_0;
    /* VARCHAR */
    private $no_res;
    /* VARCHAR(60) */
    private $ru_res;
    /* VARCHAR(30) */
    private $te_res;
    /* VARCHAR(12) */
    private $fir_res;
    /* TEXT */
    private $cote1;
    /* INTEGER NOT NULL */
    private $cote2;
    /* INTEGER */
    private $pat_mo;
    /* VARCHAR */
    private $ti_bri;
    /* INTEGER NOT NULL */
    private $co_fin;
    /* VARCHAR(150) */
    private $fotos1;
    /* TEXT */
    private $fotos2;
    /* TEXT */
    private $fotos3;
    /* TEXT */
    private $fotos4;
    /* TEXT */
    private $fotos5;
    /* TEXT */
    private $fotos6;
    /* TEXT */
    private $fotos7;
    /* TEXT */
    private $fotos8;
    /* TEXT */
    private $fotos9;
    /* TEXT */
    private $fotos10;
    /* TEXT */
    private $comentario_laboratorio;
    /* TEXT */
    private $ma_emp_int_automatico;
    /* VARCHAR(1) */
    private $ma_emp_acometida;
    /* VARCHAR(1) */
    private $ma_emp_mastil;
    /* VARCHAR(1) */
    private $ma_emp_bajada;
    /* VARCHAR(1) */
    private $ma_emp_caja_empalme;
    /* VARCHAR(1) */
    private $ma_emp_alambrado;
    /* VARCHAR(1) */
    private $res_ev_ins_1;
    /* VARCHAR */
    private $res_ev_ins_2;
    /* VARCHAR */
    private $res_ev_ins_3;
    /* VARCHAR */
    private $res_ev_ins_0_detalle;
    /* VARCHAR */
    private $res_ev_ins_1_detalle;
    /* VARCHAR */
    private $res_ev_ins_2_detalle;
    /* VARCHAR */
    private $res_ev_ins_3_detalle;
    /* VARCHAR */
    private $estado_rim;
    /* NUMERIC(1,0) */
    private $id_persona_backoffice;
    /* INTEGER */
    private $fecha_finalizado;
    /* DATE */

    /* NUEVOS */
    private $pa_ma;
    /* NUMERIC(1,0) */
    private $pa_mo;
    /* NUMERIC(1,0) */
    private $cod_insp_emp;
    /* NUMERIC(1,0) */

    /* NUEVOS */
    private $geo_invalid;
    private $se_pe;
    private $man_emp_mirilla;
    private $man_emp_tapa;
    private $numero_rim;


    /* NUMERIC(1,0) */
    private $id_persona_actualiza;
    /* INTEGER */
    private $fecha_actualiza;
    /* DATE */
    private $devolucion_distribuidora;
    /* BOOLEAN */


    /* Otras Tablas */
    private $AnormalidadesMonofasico_0;
    private $AnormalidadesMonofasico_1;
    private $AnormalidadesMonofasico_2;
    private $AnormalidadesMonofasico_3;
    private $DatosDeAviso;
    private $DatosComuna;
    private $DatosSucursal;
    private $DatosSolicitante;
    private $DatosDistribuidora;
    private $DatosInstalacion;
    private $DatosModeloMedidorEnc;
    private $DatosModeloMedidorDej;
    private $DatosPersonaTecnico1;
    private $DatosPersonaTecnico2;
    private $DatosPersonaSupervisor;
    private $FotosTerrenoRim;
    private $FotosLabMon;

    function __construct($idaviso = null, $num_avi = null, $codigo_tipo_servicio = null, $codigo_distri = null, $nom_cli = null, $tel = null, $dir1 = null, $dir2 = null, $com = null, $num_insta = null, $ti_emp = null, $r_dis = null, $ti_aco = null, $pla_pos = null, $co_me = null, $nu_se = null, $an_fa = null, $lec_ini = null, $lec_fin = null, $auto = null, $lat = null, $lon = null, $fe_eje = null, $h_ini = null, $h_te = null, $ti_serv = null, $ti_vi_fru = null, $vo_fn = null, $vo_nt = null, $vo_ft = null, $co_in = null, $bf_11 = null, $bf_12 = null, $pf_11 = null, $pf_12 = null, $vr_er1 = null, $vr_er2 = null, $cu_en = null, $bl_en = null, $cm_en = null, $cu_de = null, $bl_de = null, $cm_de = null, $pa_se = null, $pa_ma_mo = null, $co_me_ins = null, $le_ini_ins = null, $nu_se_ins = null, $a_fa_ins = null, $es_ins = null, $res_ev_ins_0 = null, $no_res = null, $ru_res = null, $te_res = null, $fir_res = null, $cote1 = null, $cote2 = null, $pat_mo = null, $ti_bri = null, $co_fin = null, $fotos1 = null, $fotos2 = null, $fotos3 = null, $fotos4 = null, $fotos5 = null, $fotos6 = null, $fotos7 = null, $fotos8 = null, $fotos9 = null, $fotos10 = null, $comentario_laboratorio = null, $ma_emp_int_automatico = null, $ma_emp_acometida = null, $ma_emp_mastil = null, $ma_emp_bajada = null, $ma_emp_caja_empalme = null, $ma_emp_alambrado = null, $res_ev_ins_1 = null, $res_ev_ins_2 = null, $res_ev_ins_3 = null, $res_ev_ins_0_detalle = null, $res_ev_ins_1_detalle = null, $res_ev_ins_2_detalle = null, $res_ev_ins_3_detalle = null, $estado_rim = null, $id_persona_backoffice = null, $fecha_finalizado = null, $pa_ma = null, $pa_mo = null, $cod_insp_emp = null, $geo_invalid = null, $se_pe = null, $man_emp_mirilla = null, $man_emp_tapa = null, $numero_rim = null, $id_persona_actualiza = null, $fecha_actualiza = null) {


        $this->idaviso = $idaviso;
        $this->num_avi = $num_avi;
        $this->codigo_tipo_servicio = $codigo_tipo_servicio;
        $this->codigo_distri = $codigo_distri;
        $this->nom_cli = $nom_cli;
        $this->tel = $tel;
        $this->dir1 = $dir1;
        $this->dir2 = $dir2;
        $this->com = $com;
        $this->num_insta = $num_insta;
        $this->ti_emp = $ti_emp;
        $this->r_dis = $r_dis;
        $this->ti_aco = $ti_aco;
        $this->pla_pos = $pla_pos;
        $this->co_me = $co_me;
        $this->nu_se = $nu_se;
        $this->an_fa = $an_fa;
        $this->lec_ini = $lec_ini;
        $this->lec_fin = $lec_fin;
        $this->auto = $auto;
        $this->lat = $lat;
        $this->lon = $lon;
        $this->fe_eje = $fe_eje;
        $this->h_ini = $h_ini;
        $this->h_te = $h_te;
        $this->ti_serv = $ti_serv;
        $this->ti_vi_fru = $ti_vi_fru;

        $this->vo_fn = $vo_fn;
        $this->vo_nt = $vo_nt;
        $this->vo_ft = $vo_ft;
        $this->co_in = $co_in;
        $this->bf_11 = $bf_11;
        $this->bf_12 = $bf_12;
        $this->pf_11 = $pf_11;
        $this->pf_12 = $pf_12;
        $this->vr_er1 = $vr_er1;
        $this->vr_er2 = $vr_er2;

        $this->cu_en = $cu_en;
        $this->bl_en = $bl_en;
        $this->cm_en = $cm_en;
        $this->cu_de = $cu_de;
        $this->bl_de = $bl_de;
        $this->cm_de = $cm_de;
        $this->pa_se = $pa_se;
        $this->pa_ma_mo = $pa_ma_mo;

        $this->co_me_ins = $co_me_ins;
        $this->le_ini_ins = $le_ini_ins;
        $this->nu_se_ins = $nu_se_ins;
        $this->a_fa_ins = $a_fa_ins;
        $this->es_ins = $es_ins;
        $this->res_ev_ins_0 = $res_ev_ins_0;

        $this->no_res = $no_res;
        $this->ru_res = $ru_res;
        $this->te_res = $te_res;
        $this->fir_res = $fir_res;

        $this->cote1 = $cote1;
        $this->cote2 = $cote2;
        $this->pat_mo = $pat_mo;
        $this->ti_bri = $ti_bri;

        $this->co_fin = $co_fin;

        $this->fotos1 = $fotos1;
        $this->fotos2 = $fotos2;
        $this->fotos3 = $fotos3;
        $this->fotos4 = $fotos4;
        $this->fotos5 = $fotos5;
        $this->fotos6 = $fotos6;
        $this->fotos7 = $fotos7;
        $this->fotos8 = $fotos8;
        $this->fotos9 = $fotos9;
        $this->fotos10 = $fotos10;

        $this->comentario_laboratorio = $comentario_laboratorio;

        $this->ma_emp_int_automatico = $ma_emp_int_automatico;
        $this->ma_emp_acometida = $ma_emp_acometida;
        $this->ma_emp_mastil = $ma_emp_mastil;
        $this->ma_emp_bajada = $ma_emp_bajada;
        $this->ma_emp_caja_empalme = $ma_emp_caja_empalme;
        $this->ma_emp_alambrado = $ma_emp_alambrado;

        $this->res_ev_ins_1 = $res_ev_ins_1;
        $this->res_ev_ins_2 = $res_ev_ins_2;
        $this->res_ev_ins_3 = $res_ev_ins_3;

        $this->res_ev_ins_0_detalle = $res_ev_ins_0_detalle;
        $this->res_ev_ins_1_detalle = $res_ev_ins_1_detalle;
        $this->res_ev_ins_2_detalle = $res_ev_ins_2_detalle;
        $this->res_ev_ins_3_detalle = $res_ev_ins_3_detalle;

        $this->estado_rim = $estado_rim;
        $this->id_persona_backoffice = $id_persona_backoffice;
        $this->fecha_finalizado = $fecha_finalizado;

        /* NUEVOS */
        $this->pa_ma = $pa_ma;
        $this->pa_mo = $pa_mo;
        $this->cod_insp_emp = $cod_insp_emp;

        /* NUEVOS */
        $this->geo_invalid = $geo_invalid;
        $this->se_pe = $se_pe;
        $this->man_emp_mirilla = $man_emp_mirilla;
        $this->man_emp_tapa = $man_emp_tapa;
        $this->numero_rim = $numero_rim;

        $this->id_persona_actualiza = $id_persona_actualiza;
        $this->fecha_actualiza = $fecha_actualiza;
    }

    public function getIdaviso() {
        return $this->idaviso;
    }

    public function setIdaviso($idaviso) {
        $this->idaviso = $idaviso;
    }

    public function getNum_avi() {
        return $this->num_avi;
    }

    public function setNum_avi($num_avi) {
        $this->num_avi = $num_avi;
    }

    public function getCodigo_tipo_servicio() {
        return $this->codigo_tipo_servicio;
    }

    public function setCodigo_tipo_servicio($codigo_tipo_servicio) {
        $this->codigo_tipo_servicio = $codigo_tipo_servicio;
    }

    public function getCodigo_distri() {
        return $this->codigo_distri;
    }

    public function setCodigo_distri($codigo_distri) {
        $this->codigo_distri = $codigo_distri;
    }

    public function getNom_cli() {
        return $this->nom_cli;
    }

    public function setNom_cli($nom_cli) {
        $this->nom_cli = $nom_cli;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function getDir1() {
        return $this->dir1;
    }

    public function setDir1($dir1) {
        $this->dir1 = $dir1;
    }

    public function getDir2() {
        return $this->dir2;
    }

    public function setDir2($dir2) {
        $this->dir2 = $dir2;
    }

    public function getCom() {
        return $this->com;
    }

    public function setCom($com) {
        $this->com = $com;
    }

    public function getNum_insta() {
        return $this->num_insta;
    }

    public function setNum_insta($num_insta) {
        $this->num_insta = $num_insta;
    }

    public function getTi_emp() {
        return $this->ti_emp;
    }

    public function setTi_emp($ti_emp) {
        $this->ti_emp = $ti_emp;
    }

    public function getR_dis() {
        return $this->r_dis;
    }

    public function setR_dis($r_dis) {
        $this->r_dis = $r_dis;
    }

    public function getTi_aco() {
        return $this->ti_aco;
    }

    public function setTi_aco($ti_aco) {
        $this->ti_aco = $ti_aco;
    }

    public function getPla_pos() {
        return $this->pla_pos;
    }

    public function setPla_pos($pla_pos) {
        $this->pla_pos = $pla_pos;
    }

    public function getCo_me() {
        return $this->co_me;
    }

    public function setCo_me($co_me) {
        $this->co_me = $co_me;
    }

    public function getNu_se() {
        return $this->nu_se;
    }

    public function setNu_se($nu_se) {
        $this->nu_se = $nu_se;
    }

    public function getAn_fa() {
        return $this->an_fa;
    }

    public function setAn_fa($an_fa) {
        $this->an_fa = $an_fa;
    }

    public function getLec_ini() {
        return $this->lec_ini;
    }

    public function setLec_ini($lec_ini) {
        $this->lec_ini = $lec_ini;
    }

    public function getLec_fin() {
        return $this->lec_fin;
    }

    public function setLec_fin($lec_fin) {
        $this->lec_fin = $lec_fin;
    }

    public function getAuto() {
        return $this->auto;
    }

    public function setAuto($auto) {
        $this->auto = $auto;
    }

    public function getLat() {
        return $this->lat;
    }

    public function setLat($lat) {
        $this->lat = $lat;
    }

    public function getLon() {
        return $this->lon;
    }

    public function setLon($lon) {
        $this->lon = $lon;
    }

    public function getFe_eje() {
        return $this->fe_eje;
    }

    public function setFe_eje($fe_eje) {
        $this->fe_eje = $fe_eje;
    }

    public function getH_ini() {
        return $this->h_ini;
    }

    public function setH_ini($h_ini) {
        $this->h_ini = $h_ini;
    }

    public function getH_te() {
        return $this->h_te;
    }

    public function setH_te($h_te) {
        $this->h_te = $h_te;
    }

    public function getTi_serv() {
        return $this->ti_serv;
    }

    public function setTi_serv($ti_serv) {
        $this->ti_serv = $ti_serv;
    }

    public function getTi_vi_fru() {
        return $this->ti_vi_fru;
    }

    public function setTi_vi_fru($ti_vi_fru) {
        $this->ti_vi_fru = $ti_vi_fru;
    }

    public function getVo_fn() {
        return $this->vo_fn;
    }

    public function setVo_fn($vo_fn) {
        $this->vo_fn = $vo_fn;
    }

    public function getVo_nt() {
        return $this->vo_nt;
    }

    public function setVo_nt($vo_nt) {
        $this->vo_nt = $vo_nt;
    }

    public function getVo_ft() {
        return $this->vo_ft;
    }

    public function setVo_ft($vo_ft) {
        $this->vo_ft = $vo_ft;
    }

    public function getCo_in() {
        return $this->co_in;
    }

    public function setCo_in($co_in) {
        $this->co_in = $co_in;
    }

    public function getBf_11() {
        return $this->bf_11;
    }

    public function setBf_11($bf_11) {
        $this->bf_11 = $bf_11;
    }

    public function getBf_12() {
        return $this->bf_12;
    }

    public function setBf_12($bf_12) {
        $this->bf_12 = $bf_12;
    }

    public function getPf_11() {
        return $this->pf_11;
    }

    public function setPf_11($pf_11) {
        $this->pf_11 = $pf_11;
    }

    public function getPf_12() {
        return $this->pf_12;
    }

    public function setPf_12($pf_12) {
        $this->pf_12 = $pf_12;
    }

    public function getVr_er1() {
        return $this->vr_er1;
    }

    public function setVr_er1($vr_er1) {
        $this->vr_er1 = $vr_er1;
    }

    public function getVr_er2() {
        return $this->vr_er2;
    }

    public function setVr_er2($vr_er2) {
        $this->vr_er2 = $vr_er2;
    }

    public function getCu_en() {
        return $this->cu_en;
    }

    public function setCu_en($cu_en) {
        $this->cu_en = $cu_en;
    }

    public function getBl_en() {
        return $this->bl_en;
    }

    public function setBl_en($bl_en) {
        $this->bl_en = $bl_en;
    }

    public function getCm_en() {
        return $this->cm_en;
    }

    public function setCm_en($cm_en) {
        $this->cm_en = $cm_en;
    }

    public function getCu_de() {
        return $this->cu_de;
    }

    public function setCu_de($cu_de) {
        $this->cu_de = $cu_de;
    }

    public function getBl_de() {
        return $this->bl_de;
    }

    public function setBl_de($bl_de) {
        $this->bl_de = $bl_de;
    }

    public function getCm_de() {
        return $this->cm_de;
    }

    public function setCm_de($cm_de) {
        $this->cm_de = $cm_de;
    }

    public function getPa_se() {
        return $this->pa_se;
    }

    public function setPa_se($pa_se) {
        $this->pa_se = $pa_se;
    }

    public function getPa_ma_mo() {
        return $this->pa_ma_mo;
    }

    public function setPa_ma_mo($pa_ma_mo) {
        $this->pa_ma_mo = $pa_ma_mo;
    }

    public function getCo_me_ins() {
        return $this->co_me_ins;
    }

    public function setCo_me_ins($co_me_ins) {
        $this->co_me_ins = $co_me_ins;
    }

    public function getLe_ini_ins() {
        return $this->le_ini_ins;
    }

    public function setLe_ini_ins($le_ini_ins) {
        $this->le_ini_ins = $le_ini_ins;
    }

    public function getNu_se_ins() {
        return $this->nu_se_ins;
    }

    public function setNu_se_ins($nu_se_ins) {
        $this->nu_se_ins = $nu_se_ins;
    }

    public function getA_fa_ins() {
        return $this->a_fa_ins;
    }

    public function setA_fa_ins($a_fa_ins) {
        $this->a_fa_ins = $a_fa_ins;
    }

    public function getEs_ins() {
        return $this->es_ins;
    }

    public function setEs_ins($es_ins) {
        $this->es_ins = $es_ins;
    }

    public function getRes_ev_ins_0() {
        return $this->res_ev_ins_0;
    }

    public function setRes_ev_ins_0($res_ev_ins_0) {
        $this->res_ev_ins_0 = $res_ev_ins_0;
    }

    public function getNo_res() {
        return $this->no_res;
    }

    public function setNo_res($no_res) {
        $this->no_res = $no_res;
    }

    public function getRu_res() {
        return $this->ru_res;
    }

    public function setRu_res($ru_res) {
        $this->ru_res = $ru_res;
    }

    public function getTe_res() {
        return $this->te_res;
    }

    public function setTe_res($te_res) {
        $this->te_res = $te_res;
    }

    public function getFir_res() {
        return $this->fir_res;
    }

    public function setFir_res($fir_res) {
        $this->fir_res = $fir_res;
    }

    public function getCote1() {
        return $this->cote1;
    }

    public function setCote1($cote1) {
        $this->cote1 = $cote1;
    }

    public function getCote2() {
        return $this->cote2;
    }

    public function setCote2($cote2) {
        $this->cote2 = $cote2;
    }

    public function getPat_mo() {
        return $this->pat_mo;
    }

    public function setPat_mo($pat_mo) {
        $this->pat_mo = $pat_mo;
    }

    public function getTi_bri() {
        return $this->ti_bri;
    }

    public function setTi_bri($ti_bri) {
        $this->ti_bri = $ti_bri;
    }

    public function getCo_fin() {
        return $this->co_fin;
    }

    public function setCo_fin($co_fin) {
        $this->co_fin = $co_fin;
    }

    public function getFotos1() {
        return $this->fotos1;
    }

    public function setFotos1($fotos1) {
        $this->fotos1 = $fotos1;
    }

    public function getFotos2() {
        return $this->fotos2;
    }

    public function setFotos2($fotos2) {
        $this->fotos2 = $fotos2;
    }

    public function getFotos3() {
        return $this->fotos3;
    }

    public function setFotos3($fotos3) {
        $this->fotos3 = $fotos3;
    }

    public function getFotos4() {
        return $this->fotos4;
    }

    public function setFotos4($fotos4) {
        $this->fotos4 = $fotos4;
    }

    public function getFotos5() {
        return $this->fotos5;
    }

    public function setFotos5($fotos5) {
        $this->fotos5 = $fotos5;
    }

    public function getFotos6() {
        return $this->fotos6;
    }

    public function setFotos6($fotos6) {
        $this->fotos6 = $fotos6;
    }

    public function getFotos7() {
        return $this->fotos7;
    }

    public function setFotos7($fotos7) {
        $this->fotos7 = $fotos7;
    }

    public function getFotos8() {
        return $this->fotos8;
    }

    public function setFotos8($fotos8) {
        $this->fotos8 = $fotos8;
    }

    public function getFotos9() {
        return $this->fotos9;
    }

    public function setFotos9($fotos9) {
        $this->fotos9 = $fotos9;
    }

    public function getFotos10() {
        return $this->fotos10;
    }

    public function setFotos10($fotos10) {
        $this->fotos10 = $fotos10;
    }

    public function getComentario_laboratorio() {
        return $this->comentario_laboratorio;
    }

    public function setComentario_laboratorio($comentario_laboratorio) {
        $this->comentario_laboratorio = $comentario_laboratorio;
    }

    public function getMa_emp_int_automatico() {
        return $this->ma_emp_int_automatico;
    }

    public function setMa_emp_int_automatico($ma_emp_int_automatico) {
        $this->ma_emp_int_automatico = $ma_emp_int_automatico;
    }

    public function getMa_emp_acometida() {
        return $this->ma_emp_acometida;
    }

    public function setMa_emp_acometida($ma_emp_acometida) {
        $this->ma_emp_acometida = $ma_emp_acometida;
    }

    public function getMa_emp_mastil() {
        return $this->ma_emp_mastil;
    }

    public function setMa_emp_mastil($ma_emp_mastil) {
        $this->ma_emp_mastil = $ma_emp_mastil;
    }

    public function getMa_emp_bajada() {
        return $this->ma_emp_bajada;
    }

    public function setMa_emp_bajada($ma_emp_bajada) {
        $this->ma_emp_bajada = $ma_emp_bajada;
    }

    public function getMa_emp_caja_empalme() {
        return $this->ma_emp_caja_empalme;
    }

    public function setMa_emp_caja_empalme($ma_emp_caja_empalme) {
        $this->ma_emp_caja_empalme = $ma_emp_caja_empalme;
    }

    public function getMa_emp_alambrado() {
        return $this->ma_emp_alambrado;
    }

    public function setMa_emp_alambrado($ma_emp_alambrado) {
        $this->ma_emp_alambrado = $ma_emp_alambrado;
    }

    public function getRes_ev_ins_1() {
        return $this->res_ev_ins_1;
    }

    public function setRes_ev_ins_1($res_ev_ins_1) {
        $this->res_ev_ins_1 = $res_ev_ins_1;
    }

    public function getRes_ev_ins_2() {
        return $this->res_ev_ins_2;
    }

    public function setRes_ev_ins_2($res_ev_ins_2) {
        $this->res_ev_ins_2 = $res_ev_ins_2;
    }

    public function getRes_ev_ins_3() {
        return $this->res_ev_ins_3;
    }

    public function setRes_ev_ins_3($res_ev_ins_3) {
        $this->res_ev_ins_3 = $res_ev_ins_3;
    }

    public function getRes_ev_ins_0_detalle() {
        return $this->res_ev_ins_0_detalle;
    }

    public function setRes_ev_ins_0_detalle($res_ev_ins_0_detalle) {
        $this->res_ev_ins_0_detalle = $res_ev_ins_0_detalle;
    }

    public function getRes_ev_ins_1_detalle() {
        return $this->res_ev_ins_1_detalle;
    }

    public function setRes_ev_ins_1_detalle($res_ev_ins_1_detalle) {
        $this->res_ev_ins_1_detalle = $res_ev_ins_1_detalle;
    }

    public function getRes_ev_ins_2_detalle() {
        return $this->res_ev_ins_2_detalle;
    }

    public function setRes_ev_ins_2_detalle($res_ev_ins_2_detalle) {
        $this->res_ev_ins_2_detalle = $res_ev_ins_2_detalle;
    }

    public function getRes_ev_ins_3_detalle() {
        return $this->res_ev_ins_3_detalle;
    }

    public function setRes_ev_ins_3_detalle($res_ev_ins_3_detalle) {
        $this->res_ev_ins_3_detalle = $res_ev_ins_3_detalle;
    }

    public function getEstado_rim() {
        return $this->estado_rim;
    }

    public function setEstado_rim($estado_rim) {
        $this->estado_rim = $estado_rim;
    }

    public function getId_persona_backoffice() {
        return $this->id_persona_backoffice;
    }

    public function setId_persona_backoffice($id_persona_backoffice) {
        $this->id_persona_backoffice = $id_persona_backoffice;
    }

    public function getFecha_finalizado() {
        return $this->fecha_finalizado;
    }

    public function setFecha_finalizado($fecha_finalizado) {
        $this->fecha_finalizado = $fecha_finalizado;
    }

    /* NUEVOS */

    public function getPa_ma() {
        return $this->pa_ma;
    }

    public function setPa_ma($pa_ma) {
        $this->pa_ma = $pa_ma;
    }

    public function getPa_mo() {
        return $this->pa_mo;
    }

    public function setPa_mo($pa_mo) {
        $this->pa_mo = $pa_mo;
    }

    public function getCod_insp_emp() {
        return $this->cod_insp_emp;
    }

    public function setCod_insp_emp($cod_insp_emp) {
        $this->cod_insp_emp = $cod_insp_emp;
    }

    /* NUEVOS */

    public function getGeo_invalid() {
        return $this->geo_invalid;
    }

    public function setGeo_invalid($geo_invalid) {
        $this->geo_invalid = $geo_invalid;
    }

    public function getSe_pe() {
        return $this->se_pe;
    }

    public function setSe_pe($se_pe) {
        $this->se_pe = $se_pe;
    }

    public function getMan_emp_mirilla() {
        return $this->man_emp_mirilla;
    }

    public function setMan_emp_mirilla($man_emp_mirilla) {
        $this->man_emp_mirilla = $man_emp_mirilla;
    }

    public function getMan_emp_tapa() {
        return $this->man_emp_tapa;
    }

    public function setMan_emp_tapa($man_emp_tapa) {
        $this->man_emp_tapa = $man_emp_tapa;
    }

    public function getNumero_rim() {
        return $this->numero_rim;
    }

    public function setNumero_rim($numero_rim) {
        $this->numero_rim = $numero_rim;
    }

    public function getId_persona_actualiza() {
        return $this->id_persona_actualiza;
    }

    public function setId_persona_actualiza($id_persona_actualiza) {
        $this->id_persona_actualiza = $id_persona_actualiza;
    }

    public function getFecha_actualiza() {
        return $this->fecha_actualiza;
    }

    public function setFecha_actualiza($fecha_actualiza) {
        $this->fecha_actualiza = $fecha_actualiza;
    }

    /* Otras Tablas */

    public function getAnormalidadesMonofasico_0() {
        return $this->AnormalidadesMonofasico_0;
    }

    public function setAnormalidadesMonofasico_0($AnormalidadesMonofasico_0) {
        $this->AnormalidadesMonofasico_0 = $AnormalidadesMonofasico_0;
    }

    public function getAnormalidadesMonofasico_1() {
        return $this->AnormalidadesMonofasico_1;
    }

    public function setAnormalidadesMonofasico_1($AnormalidadesMonofasico_1) {
        $this->AnormalidadesMonofasico_1 = $AnormalidadesMonofasico_1;
    }

    public function getAnormalidadesMonofasico_2() {
        return $this->AnormalidadesMonofasico_2;
    }

    public function setAnormalidadesMonofasico_2($AnormalidadesMonofasico_2) {
        $this->AnormalidadesMonofasico_2 = $AnormalidadesMonofasico_2;
    }

    public function getAnormalidadesMonofasico_3() {
        return $this->AnormalidadesMonofasico_3;
    }

    public function setAnormalidadesMonofasico_3($AnormalidadesMonofasico_3) {
        $this->AnormalidadesMonofasico_3 = $AnormalidadesMonofasico_3;
    }

    /*     * ******************************************************************** */

    public function getDatosDeAviso() {
        return $this->DatosDeAviso;
    }

    public function setDatosDeAviso(Aviso $DatosDeAviso) {
        $this->DatosDeAviso = $DatosDeAviso;
    }

    public function getDatosComuna() {
        return $this->DatosComuna;
    }

    public function setDatosComuna(Aviso $DatosComuna) {
        $this->DatosComuna = $DatosComuna;
    }

    /* ++++++++++++++++++++++++++++++++++++++ */

    public function getDatosSolicitante() {
        return $this->DatosSolicitante;
    }

    public function setDatosSolicitante(Solicitante $DatosSolicitante) {
        $this->DatosSolicitante = $DatosSolicitante;
    }

    public function getDatosSucursal() {
        return $this->DatosSucursal;
    }

    public function setDatosSucursal(Sucursal $DatosSucursal) {
        $this->DatosSucursal = $DatosSucursal;
    }

    public function getDatosDistribuidora() {
        return $this->DatosDistribuidora;
    }

    public function setDatosDistribuidora(Distribuidoras $DatosDistribuidora) {
        $this->DatosDistribuidora = $DatosDistribuidora;
    }

    public function getDatosInstalacion() {
        return $this->DatosInstalacion;
    }

    public function setDatosInstalacion(Instalacion $DatosInstalacion) {
        $this->DatosInstalacion = $DatosInstalacion;
    }

    public function getDatosModeloMedidorEnc() {
        return $this->DatosModeloMedidorEnc;
    }

    public function setDatosModeloMedidorEnc(ModeloMedidor $DatosModeloMedidorEnc) {
        $this->DatosModeloMedidorEnc = $DatosModeloMedidorEnc;
    }

    public function getDatosModeloMedidorDej() {
        return $this->DatosModeloMedidorDej;
    }

    public function setDatosModeloMedidorDej(ModeloMedidor $DatosModeloMedidorDej) {
        $this->DatosModeloMedidorDej = $DatosModeloMedidorDej;
    }

    public function getDatosPersonaTecnico1() {
        return $this->DatosPersonaTecnico1;
    }

    public function setDatosPersonaTecnico1(Persona $DatosPersonaTecnico1) {
        $this->DatosPersonaTecnico1 = $DatosPersonaTecnico1;
    }

    public function getDatosPersonaTecnico2() {
        return $this->DatosPersonaTecnico2;
    }

    public function setDatosPersonaTecnico2(Persona $DatosPersonaTecnico2) {
        $this->DatosPersonaTecnico2 = $DatosPersonaTecnico2;
    }

    public function getDatosPersonaSupervisor() {
        return $this->DatosPersonaSupervisor;
    }

    public function setDatosPersonaSupervisor(Persona $DatosPersonaSupervisor) {
        $this->DatosPersonaSupervisor = $DatosPersonaSupervisor;
    }

    public function getFotosTerrenoRim() {
        return $this->FotosTerrenoRim;
    }

    public function setFotosTerrenoRim(FotosTerrenoRim $FotosTerrenoRim) {
        $this->FotosTerrenoRim = $FotosTerrenoRim;
    }

    public function getFotosLabMon() {
        return $this->FotosLabMon;
    }

    public function setFotosLabMon(FotosLabMon $FotosLabMon) {
        $this->FotosLabMon = $FotosLabMon;
    }

    public function exchangeArray($data) {

        $this->idaviso = (isset($data['idaviso'])) ? $data['idaviso'] : null;
        $this->num_avi = (isset($data['num_avi'])) ? $data['num_avi'] : null;
        $this->codigo_tipo_servicio = (isset($data['codigo_tipo_servicio'])) ? $data['codigo_tipo_servicio'] : null;
        $this->codigo_distri = (isset($data['codigo_distri'])) ? $data['codigo_distri'] : null;
        $this->nom_cli = (isset($data['nom_cli'])) ? $data['nom_cli'] : null;
        $this->tel = (isset($data['tel'])) ? $data['tel'] : null;
        $this->dir1 = (isset($data['dir1'])) ? $data['dir1'] : null;
        $this->dir2 = (isset($data['dir2'])) ? $data['dir2'] : null;
        $this->com = (isset($data['com'])) ? $data['com'] : null;
        $this->num_insta = (isset($data['num_insta'])) ? $data['num_insta'] : null;
        $this->ti_emp = (isset($data['ti_emp'])) ? $data['ti_emp'] : null;
        $this->r_dis = (isset($data['r_dis'])) ? $data['r_dis'] : null;
        $this->ti_aco = (isset($data['ti_aco'])) ? $data['ti_aco'] : null;
        $this->pla_pos = (isset($data['pla_pos'])) ? $data['pla_pos'] : null;
        $this->co_me = (isset($data['co_me'])) ? $data['co_me'] : null;
        $this->nu_se = (isset($data['nu_se'])) ? $data['nu_se'] : null;
        $this->an_fa = (isset($data['an_fa'])) ? $data['an_fa'] : null;
        $this->lec_ini = (isset($data['lec_ini'])) ? $data['lec_ini'] : null;
        $this->lec_fin = (isset($data['lec_fin'])) ? $data['lec_fin'] : null;
        $this->auto = (isset($data['auto'])) ? $data['auto'] : null;
        $this->lat = (isset($data['lat'])) ? $data['lat'] : null;
        $this->lon = (isset($data['lon'])) ? $data['lon'] : null;
        $this->fe_eje = (isset($data['fe_eje'])) ? $data['fe_eje'] : null;
        $this->h_ini = (isset($data['h_ini'])) ? $data['h_ini'] : null;
        $this->h_te = (isset($data['h_te'])) ? $data['h_te'] : null;
        $this->ti_serv = (isset($data['ti_serv'])) ? $data['ti_serv'] : null;
        $this->ti_vi_fru = (isset($data['ti_vi_fru'])) ? $data['ti_vi_fru'] : null;
        $this->vo_fn = (isset($data['vo_fn'])) ? $data['vo_fn'] : null;
        $this->vo_nt = (isset($data['vo_nt'])) ? $data['vo_nt'] : null;
        $this->vo_ft = (isset($data['vo_ft'])) ? $data['vo_ft'] : null;
        $this->co_in = (isset($data['co_in'])) ? $data['co_in'] : null;

        $this->bf_11 = (isset($data['bf_11'])) ? $data['bf_11'] : null;
        $this->bf_12 = (isset($data['bf_12'])) ? $data['bf_12'] : null;
        $this->pf_11 = (isset($data['pf_11'])) ? $data['pf_11'] : null;
        $this->pf_12 = (isset($data['pf_12'])) ? $data['pf_12'] : null;

        $this->vr_er1 = (isset($data['vr_er1'])) ? $data['vr_er1'] : null;
        $this->vr_er2 = (isset($data['vr_er2'])) ? $data['vr_er2'] : null;

        $this->cu_en = (isset($data['cu_en'])) ? $data['cu_en'] : null;
        $this->bl_en = (isset($data['bl_en'])) ? $data['bl_en'] : null;
        $this->cm_en = (isset($data['cm_en'])) ? $data['cm_en'] : null;
        $this->cu_de = (isset($data['cu_de'])) ? $data['cu_de'] : null;
        $this->bl_de = (isset($data['bl_de'])) ? $data['bl_de'] : null;
        $this->cm_de = (isset($data['cm_de'])) ? $data['cm_de'] : null;

        $this->pa_se = (isset($data['pa_se'])) ? $data['pa_se'] : null;
        $this->pa_ma_mo = (isset($data['pa_ma_mo'])) ? $data['pa_ma_mo'] : null;
        $this->co_me_ins = (isset($data['co_me_ins'])) ? $data['co_me_ins'] : null;
        $this->le_ini_ins = (isset($data['le_ini_ins'])) ? $data['le_ini_ins'] : null;
        $this->nu_se_ins = (isset($data['nu_se_ins'])) ? $data['nu_se_ins'] : null;
        $this->a_fa_ins = (isset($data['a_fa_ins'])) ? $data['a_fa_ins'] : null;
        $this->es_ins = (isset($data['es_ins'])) ? $data['es_ins'] : null;
        $this->res_ev_ins_0 = (isset($data['res_ev_ins_0'])) ? $data['res_ev_ins_0'] : null;
        $this->no_res = (isset($data['no_res'])) ? $data['no_res'] : null;
        $this->ru_res = (isset($data['ru_res'])) ? $data['ru_res'] : null;
        $this->te_res = (isset($data['te_res'])) ? $data['te_res'] : null;
        $this->fir_res = (isset($data['fir_res'])) ? $data['fir_res'] : null;
        $this->cote1 = (isset($data['cote1'])) ? $data['cote1'] : null;
        $this->cote2 = (isset($data['cote2'])) ? $data['cote2'] : null;
        $this->pat_mo = (isset($data['pat_mo'])) ? $data['pat_mo'] : null;
        $this->ti_bri = (isset($data['ti_bri'])) ? $data['ti_bri'] : null;
        $this->co_fin = (isset($data['co_fin'])) ? $data['co_fin'] : null;
        $this->fotos1 = (isset($data['fotos1'])) ? $data['fotos1'] : null;
        $this->fotos2 = (isset($data['fotos2'])) ? $data['fotos2'] : null;
        $this->fotos3 = (isset($data['fotos3'])) ? $data['fotos3'] : null;
        $this->fotos4 = (isset($data['fotos4'])) ? $data['fotos4'] : null;
        $this->fotos5 = (isset($data['fotos5'])) ? $data['fotos5'] : null;
        $this->fotos6 = (isset($data['fotos6'])) ? $data['fotos6'] : null;
        $this->fotos7 = (isset($data['fotos7'])) ? $data['fotos7'] : null;
        $this->fotos8 = (isset($data['fotos8'])) ? $data['fotos8'] : null;
        $this->fotos9 = (isset($data['fotos9'])) ? $data['fotos9'] : null;
        $this->fotos10 = (isset($data['fotos10'])) ? $data['fotos10'] : null;
        $this->comentario_laboratorio = (isset($data['comentario_laboratorio'])) ? $data['comentario_laboratorio'] : null;
        $this->ma_emp_int_automatico = (isset($data['ma_emp_int_automatico'])) ? $data['ma_emp_int_automatico'] : null;
        $this->ma_emp_acometida = (isset($data['ma_emp_acometida'])) ? $data['ma_emp_acometida'] : null;
        $this->ma_emp_mastil = (isset($data['ma_emp_mastil'])) ? $data['ma_emp_mastil'] : null;
        $this->ma_emp_bajada = (isset($data['ma_emp_bajada'])) ? $data['ma_emp_bajada'] : null;
        $this->ma_emp_caja_empalme = (isset($data['ma_emp_caja_empalme'])) ? $data['ma_emp_caja_empalme'] : null;
        $this->ma_emp_alambrado = (isset($data['ma_emp_alambrado'])) ? $data['ma_emp_alambrado'] : null;
        $this->res_ev_ins_1 = (isset($data['res_ev_ins_1'])) ? $data['res_ev_ins_1'] : null;
        $this->res_ev_ins_2 = (isset($data['res_ev_ins_2'])) ? $data['res_ev_ins_2'] : null;
        $this->res_ev_ins_3 = (isset($data['res_ev_ins_3'])) ? $data['res_ev_ins_3'] : null;

        $this->res_ev_ins_0_detalle = (isset($data['res_ev_ins_0_detalle'])) ? $data['res_ev_ins_0_detalle'] : null;
        $this->res_ev_ins_1_detalle = (isset($data['res_ev_ins_1_detalle'])) ? $data['res_ev_ins_1_detalle'] : null;
        $this->res_ev_ins_2_detalle = (isset($data['res_ev_ins_2_detalle'])) ? $data['res_ev_ins_2_detalle'] : null;
        $this->res_ev_ins_3_detalle = (isset($data['res_ev_ins_3_detalle'])) ? $data['res_ev_ins_3_detalle'] : null;

        $this->estado_rim = (isset($data['estado_rim'])) ? $data['estado_rim'] : null;
        $this->id_persona_backoffice = (isset($data['id_persona_backoffice'])) ? $data['id_persona_backoffice'] : null;
        $this->fecha_finalizado = (isset($data['fecha_finalizado'])) ? $data['fecha_finalizado'] : null;

        /* NUEVOS */
        $this->pa_ma = (isset($data['pa_ma'])) ? $data['pa_ma'] : null;
        $this->pa_mo = (isset($data['pa_mo'])) ? $data['pa_mo'] : null;
        $this->cod_insp_emp = (isset($data['cod_insp_emp'])) ? $data['cod_insp_emp'] : null;

        /* NUEVOS */
        $this->geo_invalid = (isset($data['geo_invalid'])) ? $data['geo_invalid'] : null;
        $this->se_pe = (isset($data['se_pe'])) ? $data['se_pe'] : null;
        $this->man_emp_mirilla = (isset($data['man_emp_mirilla'])) ? $data['man_emp_mirilla'] : null;
        $this->man_emp_tapa = (isset($data['man_emp_tapa'])) ? $data['man_emp_tapa'] : null;
        $this->numero_rim = (isset($data['numero_rim'])) ? $data['numero_rim'] : null;

        $this->id_persona_actualiza = (isset($data['id_persona_actualiza'])) ? $data['id_persona_actualiza'] : null;
        $this->fecha_actualiza = $fecha_actualiza = (isset($data['fecha_actualiza'])) ? $data['fecha_actualiza'] : null;

        /* Otras Tablas */
        $this->AnormalidadesMonofasico_0 = new \Rim\Model\Entity\AnormalidadesMonofasico();
        $this->AnormalidadesMonofasico_0->setCodigo_anormalidad((isset($data['codigo_anormalidad_0'])) ? $data['codigo_anormalidad_0'] : null);
        $this->AnormalidadesMonofasico_0->setComponente((isset($data['componente_0'])) ? $data['componente_0'] : null);
        $this->AnormalidadesMonofasico_0->setDescripcion_anormalidad((isset($data['descripcion_anormalidad_0'])) ? $data['descripcion_anormalidad_0'] : null);
        $this->AnormalidadesMonofasico_0->setTipo_anormalidad((isset($data['tipo_anormalidad_0'])) ? $data['tipo_anormalidad_0'] : null);
        $this->AnormalidadesMonofasico_0->setUbicacion_anormalidad((isset($data['ubicacion_anormalidad_0'])) ? $data['ubicacion_anormalidad_0'] : null);

        $this->AnormalidadesMonofasico_1 = new \Rim\Model\Entity\AnormalidadesMonofasico();
        $this->AnormalidadesMonofasico_1->setCodigo_anormalidad((isset($data['codigo_anormalidad_1'])) ? $data['codigo_anormalidad_1'] : null);
        $this->AnormalidadesMonofasico_1->setComponente((isset($data['componente_1'])) ? $data['componente_1'] : null);
        $this->AnormalidadesMonofasico_1->setDescripcion_anormalidad((isset($data['descripcion_anormalidad_1'])) ? $data['descripcion_anormalidad_1'] : null);
        $this->AnormalidadesMonofasico_1->setTipo_anormalidad((isset($data['tipo_anormalidad_1'])) ? $data['tipo_anormalidad_1'] : null);
        $this->AnormalidadesMonofasico_1->setUbicacion_anormalidad((isset($data['ubicacion_anormalidad_1'])) ? $data['ubicacion_anormalidad_1'] : null);

        $this->AnormalidadesMonofasico_2 = new \Rim\Model\Entity\AnormalidadesMonofasico();
        $this->AnormalidadesMonofasico_2->setCodigo_anormalidad((isset($data['codigo_anormalidad_2'])) ? $data['codigo_anormalidad_2'] : null);
        $this->AnormalidadesMonofasico_2->setComponente((isset($data['componente_2'])) ? $data['componente_2'] : null);
        $this->AnormalidadesMonofasico_2->setDescripcion_anormalidad((isset($data['descripcion_anormalidad_2'])) ? $data['descripcion_anormalidad_2'] : null);
        $this->AnormalidadesMonofasico_2->setTipo_anormalidad((isset($data['tipo_anormalidad_2'])) ? $data['tipo_anormalidad_2'] : null);
        $this->AnormalidadesMonofasico_2->setUbicacion_anormalidad((isset($data['ubicacion_anormalidad_2'])) ? $data['ubicacion_anormalidad_2'] : null);

        $this->AnormalidadesMonofasico_3 = new \Rim\Model\Entity\AnormalidadesMonofasico();
        $this->AnormalidadesMonofasico_3->setCodigo_anormalidad((isset($data['codigo_anormalidad_3'])) ? $data['codigo_anormalidad_3'] : null);
        $this->AnormalidadesMonofasico_3->setComponente((isset($data['componente_3'])) ? $data['componente_3'] : null);
        $this->AnormalidadesMonofasico_3->setDescripcion_anormalidad((isset($data['descripcion_anormalidad_3'])) ? $data['descripcion_anormalidad_3'] : null);
        $this->AnormalidadesMonofasico_3->setTipo_anormalidad((isset($data['tipo_anormalidad_3'])) ? $data['tipo_anormalidad_3'] : null);
        $this->AnormalidadesMonofasico_3->setUbicacion_anormalidad((isset($data['ubicacion_anormalidad_3'])) ? $data['ubicacion_anormalidad_3'] : null);

        /*         * *********************************************************************************************** */


        $this->DatosDeAviso = new \Avisos\Model\Entity\Aviso();
        $this->DatosDeAviso->setDescripcion((isset($data['descripcion'])) ? $data['descripcion'] : null);
        $this->DatosDeAviso->setNumero_aviso((isset($data['numero_aviso'])) ? $data['numero_aviso'] : null);
        $this->DatosDeAviso->setFecha_carga((isset($data['fecha_carga'])) ? $data['fecha_carga'] : null);
        $this->DatosDeAviso->setClaseaviso((isset($data['claseaviso'])) ? $data['claseaviso'] : null);

        $this->DatosComuna = new\Avisos\Model\Entity\Comuna();
        $this->DatosComuna->setNombre_comuna((isset($data['nombre_comuna'])) ? $data['nombre_comuna'] : null);

        $this->DatosSolicitante = new\Avisos\Model\Entity\Solicitante();
        $this->DatosSolicitante->setDepartamento_distri((isset($data['departamento_distri'])) ? $data['departamento_distri'] : null);

        $this->DatosSucursal = new \Avisos\Model\Entity\Sucursal();
        $this->DatosSucursal->setNombre_sucursal((isset($data['nombre_sucursal'])) ? $data['nombre_sucursal'] : null);

        $this->DatosDistribuidora = new\Instalaciones\Model\Entity\Distribuidoras();
        $this->DatosDistribuidora->setNombre((isset($data['nombre'])) ? $data['nombre'] : null);

        $this->DatosInstalacion = new\Instalaciones\Model\Entity\Instalacion();
        $this->DatosInstalacion->setTarifa((isset($data['tarifa'])) ? $data['tarifa'] : null);
        $this->DatosInstalacion->setPropiedad_medidor((isset($data['propiedad_medidor'])) ? $data['propiedad_medidor'] : null);



        /*         * *************************
         * 
         *    MEIDIDOR ENCONTRADO
         * 
         * 
         * ************************ */
        $this->DatosModeloMedidorEnc = new \Instalaciones\Model\Entity\ModelosMedidores();
        $this->DatosModeloMedidorEnc->setMarca((isset($data['marca_m_enc'])) ? $data['marca_m_enc'] : null);
        $this->DatosModeloMedidorEnc->setModelo((isset($data['modelo_m_enc'])) ? $data['modelo_m_enc'] : null);
        $this->DatosModeloMedidorEnc->setNorma_medida((isset($data['norma_medida_enc'])) ? $data['norma_medida_enc'] : null);
        $this->DatosModeloMedidorEnc->setClase_medidor((isset($data['clase_medidor_enc'])) ? $data['clase_medidor_enc'] : null);
        $this->DatosModeloMedidorEnc->setVoltaje((isset($data['voltaje_enc'])) ? $data['voltaje_enc'] : null);
        $this->DatosModeloMedidorEnc->setAmperes((isset($data['amperes_enc'])) ? $data['amperes_enc'] : null);
        $this->DatosModeloMedidorEnc->setKp((isset($data['kp_enc'])) ? $data['kp_enc'] : null);

        /*         * *************************
         * 
         * MEIDIDOR DEJADO
         * 
         * 
         * ************************ */
        $this->DatosModeloMedidorDej = new \Instalaciones\Model\Entity\ModelosMedidores();
        $this->DatosModeloMedidorDej->setMarca((isset($data['marca_m_dej'])) ? $data['marca_m_dej'] : null);
        $this->DatosModeloMedidorDej->setModelo((isset($data['modelo_m_dej'])) ? $data['modelo_m_dej'] : null);
        $this->DatosModeloMedidorDej->setNorma_medida((isset($data['norma_medida_dej'])) ? $data['norma_medida_dej'] : null);
        $this->DatosModeloMedidorDej->setClase_medidor((isset($data['clase_medidor_dej'])) ? $data['clase_medidor_dej'] : null);
        $this->DatosModeloMedidorDej->setVoltaje((isset($data['voltaje_dej'])) ? $data['voltaje_dej'] : null);
        $this->DatosModeloMedidorDej->setAmperes((isset($data['amperes_dej'])) ? $data['amperes_dej'] : null);
        $this->DatosModeloMedidorDej->setKp((isset($data['kp_dej'])) ? $data['kp_dej'] : null);



        $this->DatosPersonaTecnico1 = new \Personal\Model\Entity\Persona();
        $this->DatosPersonaTecnico1->setNombres((isset($data['nombre_tec1'])) ? $data['nombre_tec1'] : null);
        $this->DatosPersonaTecnico1->setApellidos((isset($data['apellido_tec1'])) ? $data['apellido_tec1'] : null);
        $this->DatosPersonaTecnico1->setRut_persona((isset($data['rut_tec1'])) ? $data['rut_tec1'] : null);
        $this->DatosPersonaTecnico1->setFoto_firma((isset($data['foto_firma1'])) ? $data['foto_firma1'] : null);

        $this->DatosPersonaTecnico2 = new \Personal\Model\Entity\Persona();
        $this->DatosPersonaTecnico2->setNombres((isset($data['nombre_tec2'])) ? $data['nombre_tec2'] : null);
        $this->DatosPersonaTecnico2->setApellidos((isset($data['apellido_tec2'])) ? $data['apellido_tec2'] : null);
        $this->DatosPersonaTecnico2->setRut_persona((isset($data['rut_tec2'])) ? $data['rut_tec2'] : null);
        $this->DatosPersonaTecnico2->setFoto_firma((isset($data['foto_firma2'])) ? $data['foto_firma2'] : null);

        $this->DatosPersonaSupervisor = new \Personal\Model\Entity\Persona();
        $this->DatosPersonaSupervisor->setNombres((isset($data['nombres_supervisor'])) ? $data['nombres_supervisor'] : null);
        $this->DatosPersonaSupervisor->setApellidos((isset($data['apellidos_supervisor'])) ? $data['apellidos_supervisor'] : null);
        $this->DatosPersonaSupervisor->setRut_persona((isset($data['rut_supervisor'])) ? $data['rut_supervisor'] : null);
        $this->DatosPersonaSupervisor->setFoto_firma((isset($data['foto_firma_supervisor'])) ? $data['foto_firma_supervisor'] : null);

        /* FOTOS DEL RIM TERRENO EN OTRA TABLA */
        $this->FotosTerrenoRim = new \Rim\Model\Entity\fotosTerrenoRim();
        $this->FotosTerrenoRim->setFoto1((isset($data['foto1'])) ? $data['foto1'] : null);
        $this->FotosTerrenoRim->setFoto2((isset($data['foto2'])) ? $data['foto2'] : null);
        $this->FotosTerrenoRim->setFoto3((isset($data['foto3'])) ? $data['foto3'] : null);
        $this->FotosTerrenoRim->setFoto4((isset($data['foto4'])) ? $data['foto4'] : null);
        $this->FotosTerrenoRim->setFoto5((isset($data['foto5'])) ? $data['foto5'] : null);
        $this->FotosTerrenoRim->setFoto6((isset($data['foto6'])) ? $data['foto6'] : null);
        $this->FotosTerrenoRim->setFoto7((isset($data['foto7'])) ? $data['foto7'] : null);
        $this->FotosTerrenoRim->setFoto8((isset($data['foto8'])) ? $data['foto8'] : null);
        $this->FotosTerrenoRim->setFoto9((isset($data['foto9'])) ? $data['foto9'] : null);
        $this->FotosTerrenoRim->setFoto10((isset($data['foto10'])) ? $data['foto10'] : null);
        //$this -> FotosTerrenoRim -> setIdaviso((isset($data['idaviso'])) ? $data['idaviso'] : null); 

        /* FOTOS LABORATORIO MONOFASICO */
        $this->FotosLabMon = new \Rim\Model\Entity\fotosLabMon();
        $this->FotosLabMon->setFotoLab1((isset($data['fotolab1'])) ? $data['fotolab1'] : null);
        $this->FotosLabMon->setFotoLab2((isset($data['fotolab2'])) ? $data['fotolab2'] : null);
        $this->FotosLabMon->setFotoLab3((isset($data['fotolab3'])) ? $data['fotolab3'] : null);
        $this->FotosLabMon->setFotoLab4((isset($data['fotolab4'])) ? $data['fotolab4'] : null);
        $this->FotosLabMon->setFotoLab5((isset($data['fotolab5'])) ? $data['fotolab5'] : null);
        $this->FotosLabMon->setFotoLab6((isset($data['fotolab6'])) ? $data['fotolab6'] : null);
        $this->FotosLabMon->setFotoLab7((isset($data['fotolab7'])) ? $data['fotolab7'] : null);
        $this->FotosLabMon->setFotoLab8((isset($data['fotolab8'])) ? $data['fotolab8'] : null);
        $this->FotosLabMon->setFotoLab9((isset($data['fotolab9'])) ? $data['fotolab9'] : null);
        $this->FotosLabMon->setFotoLab10((isset($data['fotolab10'])) ? $data['fotolab10'] : null);
        
        
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}
