<?php

namespace Rim\Model\Entity;

class fotosLabMon {

     private $fotolab1;
     private $fotolab2;
     private $fotolab3;
     private $fotolab4;
     private $fotolab5;
     private $fotolab6;
     private $fotolab7;
     private $fotolab8;
     private $fotolab9;
     private $fotolab10;
     private $idaviso;


     function __construct( $fotolab1 = null,
                           $fotolab2 = null,
                           $fotolab3 = null,
                           $fotolab4 = null,
                           $fotolab5 = null,
                           $fotolab6 = null,
                           $fotolab7 = null,
                           $fotolab8 = null,
                           $fotolab9 = null,
                           $fotolab10 = null,
                           $idaviso = null ) {

                 $this->fotolab1 = $fotolab1;
                 $this->fotolab2 = $fotolab2;
                 $this->fotolab3 = $fotolab3;
                 $this->fotolab4 = $fotolab4;
                 $this->fotolab5 = $fotolab5;
                 $this->fotolab6 = $fotolab6;
                 $this->fotolab7 = $fotolab7;
                 $this->fotolab8 = $fotolab8;
                 $this->fotolab9 = $fotolab9;
                 $this->fotolab10 = $fotolab10;
                 $this->idaviso = $idaviso;
            
                 }
     
                 public function getFotoLab1() {
                     return $this->fotolab1;
                 }

                 public function getFotoLab2() {
                     return $this->fotolab2;
                 }

                 public function getFotoLab3() {
                     return $this->fotolab3;
                 }

                 public function getFotoLab4() {
                     return $this->fotolab4;
                 }

                 public function getFotoLab5() {
                     return $this->fotolab5;
                 }

                 public function getFotoLab6() {
                     return $this->fotolab6;
                 }

                 public function getFotoLab7() {
                     return $this->fotolab7;
                 }

                 public function getFotoLab8() {
                     return $this->fotolab8;
                 }

                 public function getFotoLab9() {
                     return $this->fotolab9;
                 }

                 public function getFotoLab10() {
                     return $this->fotolab10;
                 }

                 public function getIdaviso() {
                     return $this->idaviso;
                 }

                 public function setFotoLab1($fotolab1) {
                     $this->fotolab1 = $fotolab1;
                 }

                 public function setFotoLab2($fotolab2) {
                     $this->fotolab2 = $fotolab2;
                 }
                 public function setFotoLab3($fotolab3) {
                     $this->fotolab3 = $fotolab3;
                 }

                 public function setFotoLab4($fotolab4) {
                     $this->fotolab4 = $fotolab4;
                 }

                 public function setFotoLab5($fotolab5) {
                     $this->fotolab5 = $fotolab5;
                 }

                 public function setFotoLab6($fotolab6) {
                     $this->fotolab6 = $fotolab6;
                 }

                 public function setFotoLab7($fotolab7) {
                     $this->fotolab7 = $fotolab7;
                 }

                 public function setFotoLab8($fotolab8) {
                     $this->fotolab8 = $fotolab8;
                 }

                 public function setFotoLab9($fotolab9) {
                     $this->fotolab9 = $fotolab9;
                 }

                 public function setFotoLab10($fotolab10) {
                     $this->fotolab10 = $fotolab10;
                 }

                 public function setIdaviso($idaviso) {
                     $this->idaviso = $idaviso;
                 }

                          
     public function exchangeArray($data) {

        $this->idaviso = (isset($data['idaviso'])) ? $data['idaviso'] : null;

        $this->fotolab1 = (isset($data['fotolab1'])) ? $data['fotolab1'] : null;
        $this->fotolab2 = (isset($data['fotolab2'])) ? $data['fotolab2'] : null;
        $this->fotolab3 = (isset($data['fotolab3'])) ? $data['fotolab3'] : null;
        $this->fotolab4 = (isset($data['fotolab4'])) ? $data['fotolab4'] : null;
        $this->fotolab5 = (isset($data['fotolab5'])) ? $data['fotolab5'] : null;
        $this->fotolab6 = (isset($data['fotolab6'])) ? $data['fotolab6'] : null;
        $this->fotolab7 = (isset($data['fotolab7'])) ? $data['fotolab7'] : null;
        $this->fotolab8 = (isset($data['fotolab8'])) ? $data['fotolab8'] : null;
        $this->fotolab9 = (isset($data['fotolab9'])) ? $data['fotolab9'] : null;
        $this->fotolab10 = (isset($data['fotolab10'])) ? $data['fotolab10'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

