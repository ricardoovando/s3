<?php

namespace Rim\Model\Entity;

class fotosTerrenoRim {

     private $foto1;
     private $foto2;
     private $foto3;
     private $foto4;
     private $foto5;
     private $foto6;
     private $foto7;
     private $foto8;
     private $foto9;
     private $foto10;
     private $idaviso;


     function __construct( $foto1 = null,
                                         $foto2 = null,
                                         $foto3 = null,
                                         $foto4 = null,
                                         $foto5 = null,
                                         $foto6 = null,
                                         $foto7 = null,
                                         $foto8 = null,
                                         $foto9 = null,
                                         $foto10 = null,
                                         $idaviso = null ) {
                     
                 $this->foto1 = $foto1;
                 $this->foto2 = $foto2;
                 $this->foto3 = $foto3;
                 $this->foto4 = $foto4;
                 $this->foto5 = $foto5;
                 $this->foto6 = $foto6;
                 $this->foto7 = $foto7;
                 $this->foto8 = $foto8;
                 $this->foto9 = $foto9;
                 $this->foto10 = $foto10;
                 $this->idaviso = $idaviso;
            
            
                 }
     
     public function getFoto1() {
         return $this->foto1;
     }

     public function setFoto1($foto1) {
         $this->foto1 = $foto1;
     }

     public function getFoto2() {
         return $this->foto2;
     }

     public function setFoto2($foto2) {
         $this->foto2 = $foto2;
     }

     public function getFoto3() {
         return $this->foto3;
     }

     public function setFoto3($foto3) {
         $this->foto3 = $foto3;
     }

     public function getFoto4() {
         return $this->foto4;
     }

     public function setFoto4($foto4) {
         $this->foto4 = $foto4;
     }

     public function getFoto5() {
         return $this->foto5;
     }

     public function setFoto5($foto5) {
         $this->foto5 = $foto5;
     }

     public function getFoto6() {
         return $this->foto6;
     }

     public function setFoto6($foto6) {
         $this->foto6 = $foto6;
     }

     public function getFoto7() {
         return $this->foto7;
     }

     public function setFoto7($foto7) {
         $this->foto7 = $foto7;
     }

     public function getFoto8() {
         return $this->foto8;
     }

     public function setFoto8($foto8) {
         $this->foto8 = $foto8;
     }

     public function getFoto9() {
         return $this->foto9;
     }

     public function setFoto9($foto9) {
         $this->foto9 = $foto9;
     }

     public function getFoto10() {
         return $this->foto10;
     }

     public function setFoto10($foto10) {
         $this->foto10 = $foto10;
     }

     public function getIdaviso() {
         return $this->idaviso;
     }

     public function setIdaviso($idaviso) {
         $this->$idaviso = $idaviso;
     }

    
         
     public function exchangeArray($data) {

         $this->idaviso = (isset($data['idaviso'])) ? $data['idaviso'] : null;

        $this->foto1 = (isset($data['foto1'])) ? $data['foto1'] : null;
        $this->foto2 = (isset($data['foto2'])) ? $data['foto2'] : null;
        $this->foto3 = (isset($data['foto3'])) ? $data['foto3'] : null;
        $this->foto4 = (isset($data['foto4'])) ? $data['foto4'] : null;
        $this->foto5 = (isset($data['foto5'])) ? $data['foto5'] : null;
        $this->foto6 = (isset($data['foto6'])) ? $data['foto6'] : null;
        $this->foto7 = (isset($data['foto7'])) ? $data['foto7'] : null;
        $this->foto8 = (isset($data['foto8'])) ? $data['foto8'] : null;
        $this->foto9 = (isset($data['foto9'])) ? $data['foto9'] : null;
        $this->foto10 = (isset($data['foto10'])) ? $data['foto10'] : null;

    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

