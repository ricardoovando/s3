<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Servicios;

use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\Config\Reader\Ini;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Servicios\Model\Dao\ServicioDao;
use Servicios\Model\Entity\Servicio;

class Module implements InitProviderInterface, ConfigProviderInterface, AutoloaderProviderInterface, ServiceProviderInterface, ControllerProviderInterface {

    public function init(ModuleManagerInterface $mm) {
        $events = $mm->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Application', 'bootstrap', array($this, 'initConfig'));
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'initAuth'), 100);
    }

    public function initAuth(MvcEvent $e) {
        $application = $e->getApplication();
        $matches = $e->getRouteMatch();
        $controller = $matches->getParam('controller');
        $action = $matches->getParam('action');

        if (0 === strpos($controller, __NAMESPACE__, 0)) {

            switch ($controller) {
                case "Admin\Controller\Login":
                    if (in_array($action, array('index', 'autenticar'))) {
                        // Validamos cuando el controlador sea LoginController
                        // exepto las acciones index y autenticar.
                        return;
                    }
                    break;
                case "Admin\Controller\Index":
                    if (in_array($action, array('index'))) {
                        // Validamos cuando el controlador sea IndexController
                        // exepto las acciones index.
                        return;
                    }
                    break;
            }

            $sm = $application->getServiceManager();

            $auth = $sm->get('Admin\Model\Login');

            if (!$auth->isLoggedIn()) {
                // No existe Session, redirigimos al login.
                $controller = $e->getTarget();
                return $controller->redirect()->toRoute('admin/default', array('controller' => 'login'));
            }
        }
    }

    public function initConfig(MvcEvent $e) {
        $application = $e->getApplication();
        $services = $application->getServiceManager();

        $services->setFactory('ConfigIniServicio', function ($services) {
                    $reader = new Ini();
                    $data = $reader->fromFile(__DIR__ . '/config/config.ini');
                    return $data;
                });
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Servicios\Model\ServicioDao' => function($sm) {
                    $tableGateway = $sm->get('ServiciosTableGateway');
                    $dbAdapter = $sm->get('dbAdapter'); 
                    $login = $sm->get('Admin\Model\Login');
                    $dao = new ServicioDao($tableGateway,$dbAdapter,$login);
                    return $dao;
                },
                'ServiciosTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Servicio());
                    return new TableGateway('servicios', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAdapter' => function ($sm) {    
                    return $sm->get('Zend\Db\Adapter\Adapter'); /*Se inserta instancia de base de datos*/
                },
            ),
        );
    }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Servicios\Controller\Index' => function ($sm) {
                    $locator = $sm->getServiceLocator();
                    $config = $locator->get('ConfigIniServicio');
                    $ServicioDao = $locator->get('Servicios\Model\ServicioDao');
                    $controller = new \Servicios\Controller\IndexController($config);
                    $controller->setServicioDao($ServicioDao);
                    return $controller;
                }
            )
        );
    }

}
