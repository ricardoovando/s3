<?php

namespace Servicios\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Servicios\Form\Registro as ServicioForm;
use Servicios\Form\RegistroValidator;
use Servicios\Form\BuscadorServicio as BuscadorForm;
use Servicios\Form\BuscadorServicioValidator;

use Servicios\Model\Entity\Servicio;

class IndexController extends AbstractActionController {

    private $ServicioDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setServicioDao($serviciodao) {
        $this->ServicioDao = $serviciodao;
    }

    public function getServicioDao() {
        return $this->ServicioDao;
    }

    public function getConfig() {
        return $this->config;
    }

    private function getServicioForm() {
        return new ServicioForm();
    }

    private function getFormBuscador() {
        return new BuscadorForm();
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        
        $form = $this->getFormBuscador();
        
        $paginator = $this->getServicioDao()->obtenerTodos();
        
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        
        $paginator->setItemCountPerPage(20);

        return new ViewModel(array(
            'title' => 'Listado de Servicios',
            'listadeservicios' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $form,
        ));
        
    }

    public function crearAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $form = $this->getServicioForm();
        return new ViewModel(array('title' => 'Crear Registro', 'form' => $form));
    }

    public function buscarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('servicios', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getFormBuscador();
        $form->setInputFilter(new BuscadorServicioValidator());
        $form->setData($postParams);

        if (!$form->isValid()) {
            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('listadeservicios' => $this->getServicioDao()->obtenerTodos(), 'title' => 'Lista de Servicios', 'form' => $form));
            
            $modelView->setTemplate('servicios/index/index');
            
            return $modelView;
        }

        $values = $form->getData();

        /* Reconfiguracion de Paginador */
        $paginator = $this->getServicioDao()->buscarPorNombreServicio($values['nombreservicio']);
        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(20);

        /* Configuracion de Vista */
        $viewModel = new ViewModel(array(
            'title' => sprintf('Datos Encontrados', $paginator->count()),
            'listadeservicios' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $this->getFormBuscador(),
        ));

        /* Cambio del Templede del Accion */
        $viewModel->setTemplate("servicios/index/index");

        return $viewModel;
    }

    public function registrarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('servicios', array('controller' => 'index'));
        }

        $postParams = $this->request->getPost();

        $form = $this->getServicioForm();
        $form->setInputFilter(new RegistroValidator());

        $form->setData($postParams);

        if (!$form->isValid()) {
            $modelView = new ViewModel(array('titulo' => 'Datos mal Ingresados', 'form' => $form));
            $modelView->setTemplate('servicios/index/crear');
            return $modelView;
        }

        $values = $form->getData();

        $servicio = new Servicio();
        $servicio->exchangeArray($values);

        $this->getServicioDao()->guardar($servicio);

        return $this->redirect()->toRoute('servicios', array('controller' => 'index'));
        
    }

    public function editarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('servicios', array('controller' => 'index'));
        }

        $form = $this->getServicioForm();

        $servicio = $this->getServicioDao()->obtenerPorId($id);

        $form->bind($servicio);

        $form->get('send')->setAttribute('value', 'Editar');

        $modelView = new ViewModel(array('title' => 'Editar Registro', 'form' => $form));

        $modelView->setTemplate('servicios/index/crear');

        return $modelView;
    }

    public function eliminarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('servicios', array('controller' => 'index'));
           }

        $servicio = new Servicio();
        $servicio->setId($id);

        $this->getServicioDao()->eliminar($servicio);

        return $this->redirect()->toRoute('servicios', array('controller' => 'index'));
    }

}
