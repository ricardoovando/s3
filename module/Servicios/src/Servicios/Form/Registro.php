<?php

namespace Servicios\Form;

use Zend\Form\Form;

class Registro extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id_servicio',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombre_servicio',
            'options' => array(
                'label' => 'Nombre Servicio : ',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'detalleservicio',
            'attributes' => array(
             'rows' => '2', 
             'cols' =>'33',
            ),
            'options' => array(
                'label' => 'Detalle del Servicio : ',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'contrato',
            'options' => array(
                'label' => 'Numero Contrato : ',
            )
        ));

        /*TERMINO*/
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
        
        
    }

}

