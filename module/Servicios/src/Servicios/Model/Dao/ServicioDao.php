<?php

namespace Servicios\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Servicios\Model\Entity\Servicio;

class ServicioDao {

    protected $tableGateway;
    protected $dbAdapter;
    protected $login;

    public function __construct(TableGateway $tableGateway, Adapter $dbAdapter , $login) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $dbAdapter;
        $this->login = $login->getIdentity();
    }
    
    public function obtenerTodos() {
        
        $select = $this->tableGateway->getSql()->select();
        $select->order("id_servicio");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_servicio' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

//    public function eliminar(Servicio $servicio) {
//        $this->tableGateway->delete(array('id_servicio' => $servicio->getId()));
//    }

    public function guardar(Servicio $servicio) {
        $data = array(
            'nombre_servicio' => $servicio->getNombre_servicio(),
            'detalleservicio' => $servicio->getDetalleservicio(),
            'contrato' => $servicio->getContrato(),
        );


        $id = (int) $servicio->getId_servicio();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_servicio' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorNombreServicio($nombreservicio) {
        
        $nombreservicio = (String) $nombreservicio;
        
        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $select = $this->tableGateway->getSql()->select();
        $select->where->like('nombre_servicio_min', '%' . $nombreservicio . '%');
        $select->order("id_servicio");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    
    public function buscarPorNombreServiciosNoCargados($nombreservicio,$id) {
        
        $id = (int) $id;
        $nombreservicio = (String) $nombreservicio;
        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('list' => 'listadeprecios'), 'list.servicios_id_servicio = servicios.id_servicio', array('precio_uf' => 'precio_uf', 'sucursales_id_sucursal' => 'sucursales_id_sucursal'));
        $select->join(array('dsp' => 'detalle_servicio_prestado'), new \Zend\Db\Sql\Expression('dsp.servicios_id_servicio = servicios.id_servicio AND dsp.avisos_id_aviso = 7'), array(), 'left');
        $select->where(array('list.sucursales_id_sucursal' => $sucursales_id_sucursal ));
        $select->where->like('nombre_servicio_min', '%' . $nombreservicio . '%');
        $select->where->isNull('dsp.avisos_id_aviso');
        $select->where->isNull('dsp.servicios_id_servicio');
        $select->order("servicios.id_servicio");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }
    
    public function obtenerServiciosNoCargados($id) {

        $id = (int) $id;
   
        $sucursales_id_sucursal = $this->login->sucursales_id_sucursal;
        
        $select = $this->tableGateway->getSql()->select();
        $select->join(array('list' => 'listadeprecios'), 'list.servicios_id_servicio = servicios.id_servicio', array('precio_uf' => 'precio_uf', 'sucursales_id_sucursal' => 'sucursales_id_sucursal'));
        $select->join(array('dsp' => 'detalle_servicio_prestado'), new \Zend\Db\Sql\Expression('dsp.servicios_id_servicio = servicios.id_servicio AND dsp.avisos_id_aviso = '.$id.' '), array(), 'left');
        $select->where(array('list.sucursales_id_sucursal' => $sucursales_id_sucursal ));
        $select->where->isNull('dsp.avisos_id_aviso');
        $select->where->isNull('dsp.servicios_id_servicio');
        $select->order("servicios.id_servicio");

        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

}
