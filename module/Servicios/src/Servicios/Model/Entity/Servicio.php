<?php

namespace Servicios\Model\Entity;

class Servicio {
    
    private $id_servicio;
    private $nombre_servicio;
    private $detalleservicio;
    private $contrato;
    
    private $listadeprecios; // listado de precios. 
    

    function __construct($id_servicio = null, $nombre_servicio = null, $detalleservicio = null, $contrato = null) {
        $this->id_servicio = $id_servicio;
        $this->nombre_servicio = $nombre_servicio;
        $this->detalleservicio = $detalleservicio;
        $this->contrato = $contrato;
    }
    
    public function getId_servicio() {
        return $this->id_servicio;
    }

    public function setId_servicio($id_servicio) {
        $this->id_servicio = $id_servicio;
    }

    public function getNombre_servicio() {
        return $this->nombre_servicio;
    }

    public function setNombre_servicio($nombre_servicio) {
        $this->nombre_servicio = $nombre_servicio;
    }

    public function getDetalleservicio() {
        return $this->detalleservicio;
    }

    public function setDetalleservicio($detalleservicio) {
        $this->detalleservicio = $detalleservicio;
    }

    public function getContrato() {
        return $this->contrato;
    }

    public function setContrato($contrato) {
        $this->contrato = $contrato;
    }
    
    public function getListadeprecios() {
        return $this->listadeprecios;
    }

    public function setListadeprecios(Listadeprecios $listadeprecios) {
        $this->listadeprecios = $listadeprecios;
    }
    
    public function exchangeArray($data) {
        
        $this->id_servicio = (isset($data['id_servicio'])) ? $data['id_servicio'] : null;
        $this->nombre_servicio = (isset($data['nombre_servicio'])) ? $data['nombre_servicio'] : null;
        $this->detalleservicio = (isset($data['detalleservicio'])) ? $data['detalleservicio'] : null;
        $this->contrato = (isset($data['contrato'])) ? $data['contrato'] : null;
        
        
        $this->listadeprecios = new \Listadeprecios\Model\Entity\Listadeprecios();
        $this->listadeprecios->setPrecio_uf((isset($data['precio_uf'])) ? $data['precio_uf'] : null);
        $this->listadeprecios->setSucursales_id_sucursal((isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null);
                
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

