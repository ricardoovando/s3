<?php

namespace Usuarios\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Usuarios\Form\Buscador;

class IndexController extends AbstractActionController {

    private $usuarioDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    
    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }
    
    public function setUsuarioDao($usuarioDao) {
        $this->usuarioDao = $usuarioDao;
    }

    public function getUsuarioDao() {
        return $this->usuarioDao;
    }

    private function getForm() {
        return new Buscador();
    }

    public function indexAction() {
        return $this->forward()->dispatch('Usuarios\Controller\Index', array('action' => 'listar'));
    }

    public function listarAction() {
        
        $this->layout()->usuario = $this->getLogin()->getIdentity();
                
        return new ViewModel(array(
                              'listaUsuario' => $this->getUsuarioDao()->obtenerTodos(),
                              'form' => $this->getForm(),
                              'titulo' => $this->config['titulo']['usuario']['listado'],
                               ) );
        
                            }

    public function verAction() {
        $id = (int) $this->params()->fromRoute("id", 0);

        $usuario = $this->getUsuarioDao()->obtenerPorId($id);

        if (null === $usuario) {
            return $this->redirect()->toRoute('usuarios', array('controller' => 'index', 'action' => 'listar'));
        }
        return new ViewModel(array('usuario' => $usuario,
                    'titulo' => sprintf($this->config['titulo']['usuario']['ver'], $usuario->getNombre())));
    }

    public function buscarAction() {

        $nombre = $this->getRequest()->getPost("nombre");

        if (null === $nombre || empty($nombre)) {
            return $this->redirect()->toRoute('usuarios', array('controller' => 'index', 'action' => 'listar'));
        }

        $listaUsuario = $this->getUsuarioDao()->buscarPorNombre($nombre);

        $viewModel = new ViewModel(array('listaUsuario' => $listaUsuario,
                    'form' => $this->getForm(),
                    'titulo' => sprintf($this->config['titulo']['usuario']['buscar'], $listaUsuario->count())));

        $viewModel->setTemplate("usuarios/index/listar");

        return $viewModel;
    }

}
