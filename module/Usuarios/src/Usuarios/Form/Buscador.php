<?php

namespace Usuarios\Form;
use Zend\Form\Form;

class Buscador extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'nombre',
            'options' => array(
                'label' => 'Buscar Nombre Usuario :',
            ),
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Buscar',
                'class' => 'btn btn-primary',
                'style' => 'height:30px;',
            ),
        ));
    }

}

