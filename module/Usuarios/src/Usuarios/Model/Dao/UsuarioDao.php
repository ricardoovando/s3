<?php

namespace Usuarios\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Usuarios\Model\Entity\Usuario;

class UsuarioDao implements IUsuarioDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function obtenerTodos() {
        try {
            return $this->tableGateway->select();
        } catch (\Exception $e) {
            \Zend\Debug\Debug::dump($e->__toString());
            exit;
        }
    }

    public function obtenerPorId($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }

        return $row;
    }

    public function comprobarusuariopass(Usuario $usuario) {

        $id = (int) $usuario->getId();
        $clave = $usuario->getClave();

        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id' => $id, 'clave' => $clave));
        return $this->tableGateway->selectWith($select);
    }

    public function eliminar(Usuario $usuario) {
        $this->tableGateway->delete(array('id' => $usuario->getId()));
    }

    public function guardar(Usuario $usuario) {
        $data = array(
            'nombre' => $usuario->getNombre(),
            'apellido' => $usuario->getApellido(),
            'email' => $usuario->getEmail(),
            'clave' => $usuario->getClave(),
        );

        $id = (int) $usuario->getId();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function updatepass(Usuario $usuario) {

        $data = array(
            'clave' => $usuario->getClave(),
        );

        $id = (int) $usuario->getId();

        if ($this->obtenerPorId($id)) {
            $this->tableGateway->update($data, array('id' => $id));
        } else {
            throw new \Exception('Form id does not exist');
        }
    }

    public function buscarPorNombre($nombre) {
        $select = $this->tableGateway->getSql()->select();
        $select->where->like('nombre', '%' . $nombre . '%');
        $select->order("nombre");

        return $this->tableGateway->selectWith($select);
    }

    public function obtenerCuenta($email, $clave) {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('email' => $email, 'clave' => $clave,));

        return $this->tableGateway->selectWith($select)->current();
    }

}
