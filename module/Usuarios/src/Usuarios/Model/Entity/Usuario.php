<?php

namespace Usuarios\Model\Entity;

class Usuario {

    private $id;
    private $clave;
    private $rol;
    private $sucursales_id_sucursal;
    private $personal_id_personal;
    private $estado;

    function __construct($id = null, $clave = null, $rol = null, $sucursales_id_sucursal = null, $personal_id_personal = null, $estado = null) {
        $this->id = $id;
        $this->clave = $clave;
        $this->rol = $rol;
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
        $this->personal_id_personal = $personal_id_personal;
        $this->estado = $estado;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getClave() {
        return $this->clave;
    }

    public function setClave($clave) {
        $this->clave = $clave;
    }

    public function getRol() {
        return $this->rol;
    }

    public function setRol($rol) {
        $this->rol = $rol;
    }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getPersonal_id_personal() {
        return $this->personal_id_personal;
    }

    public function setPersonal_id_personal($personal_id_personal) {
        $this->personal_id_personal = $personal_id_personal;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function exchangeArray($data) {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->clave = (isset($data['clave'])) ? $data['clave'] : null;
        $this->rol = (isset($data['rol'])) ? $data['rol'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
        $this->personal_id_personal = (isset($data['personal_id_personal'])) ? $data['personal_id_personal'] : null;
        $this->estado = (isset($data['estado'])) ? $data['estado'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

