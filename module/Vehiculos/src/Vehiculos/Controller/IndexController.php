<?php

namespace Vehiculos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Vehiculos\Model\Entity\Vehiculo;
use Vehiculos\Form\Registro as VehiculoForm;
use Vehiculos\Form\RegistroValidator;
use Vehiculos\Form\Buscador as BuscadorForm;
use Vehiculos\Form\BuscadorValidator;

class IndexController extends AbstractActionController {

    private $vehiculoDao;
    private $config;
    private $login;

    function __construct($config = null) {
        $this->config = $config;
    }

    public function getLogin() {
        if (!$this->login) {
            $sm = $this->getServiceLocator();
            $this->login = $sm->get('Admin\Model\Login');
        }
        return $this->login;
    }

    public function setVehiculoDao($vehiculoDao) {
        $this->vehiculoDao = $vehiculoDao;
    }

    public function getVehiculoDao() {
        return $this->vehiculoDao;
    }

    public function getConfig() {
        return $this->config;
    }

    private function getFormVehiculo() {
        return new VehiculoForm();
    }

    private function getFormBuscador() {
        return new BuscadorForm();
    }

    public function indexAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $form = $this->getFormBuscador();
        //return new ViewModel(array('listadoVehiculos' => $this->getVehiculoDao()->obtenerTodos(), 'form' => $form));
        $paginator = $this->getVehiculoDao()->obtenerTodos();
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));
        $paginator->setItemCountPerPage(4);

        return new ViewModel(array(
            'title' => 'Lista de Vehiculos',
            'listadoVehiculos' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $form,
        ));
    }

    public function crearAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();
        $form = $this->getFormVehiculo();
        return new ViewModel(array('title' => 'Crear Registro', 'form' => $form));
    }

    public function buscarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getFormBuscador();
        $form->setInputFilter(new BuscadorValidator());
        $form->setData($postParams);

        if (!$form->isValid()) {
            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('listadoVehiculos' => $this->getVehiculoDao()->obtenerTodos(), 'title' => 'Gestor de Vehiculos malos', 'form' => $form));
            $modelView->setTemplate('vehiculos/index/index');
            return $modelView;
        }

        $values = $form->getData();

        /*Reconfiguracion de Paginador*/
        $paginator = $this->getVehiculoDao()->buscarPorPatente($values['patente']);
        $paginator->setCurrentPageNumber(1);
        $paginator->setItemCountPerPage(4);
                
        /*Configuracion de Vista*/
        $viewModel = new ViewModel(array(
            'title' => sprintf('Datos Encontrados', $paginator->count()),
            'listadoVehiculos' => $paginator->getIterator(),
            'paginator' => $paginator,
            'form' => $this->getFormBuscador(),
        ));

        /*Cambio del Templede del Accion*/
        $viewModel->setTemplate("vehiculos/index/index");

        return $viewModel;
    }

    public function registrarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
        }

        // Obtenemos los parámetros del formulario es similar a $_POST
        $postParams = $this->request->getPost();

        $form = $this->getFormVehiculo();
        $form->setInputFilter(new RegistroValidator());

        $form->setData($postParams);

        if (!$form->isValid()) {

            // Falla la validación; volvemos a generar el formulario 
            $modelView = new ViewModel(array('titulo' => 'Datos mal Ingresados', 'form' => $form));
            $modelView->setTemplate('vehiculos/index/crear');
            return $modelView;
        }

        $values = $form->getData();

        $values['sucursales_id_sucursal'] = 1;

        $vehiculo = new Vehiculo();
        $vehiculo->exchangeArray($values);

        $this->getVehiculoDao()->guardar($vehiculo);

        return $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
    }

    public function editarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
        }

        $form = $this->getFormVehiculo();

        $vehiculo = $this->getVehiculoDao()->obtenerPorId($id);

        $form->bind($vehiculo);

        $form->get('send')->setAttribute('value', 'Editar');

        $modelView = new ViewModel(array('title' => 'Editar Registro', 'form' => $form));

        $modelView->setTemplate('vehiculos/index/crear');

        return $modelView;
    }

    public function eliminarAction() {

        $this->layout()->usuario = $this->getLogin()->getIdentity();

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
        }

        $vehiculo = new Vehiculo();
        $vehiculo->setId_vehiculo($id);

        $this->getVehiculoDao()->eliminar($vehiculo);

        return $this->redirect()->toRoute('vehiculos', array('controller' => 'index'));
    }

}
