<?php

namespace Vehiculos\Form;

use Zend\Form\Form;

class Registro extends Form {

    public function __construct($name = null) {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id_vehiculo',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'patente',
            'options' => array(
                'label' => 'Patente',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'cod_vehiculo',
            'options' => array(
                'label' => 'Codigo',
            )
        ));

        /*TERMINO*/
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Guardar',
                'class' => 'btn btn-primary btn-lg'
            ),
        ));
        
        
    }

}

