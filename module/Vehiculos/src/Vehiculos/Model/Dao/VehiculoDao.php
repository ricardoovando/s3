<?php

namespace Vehiculos\Model\Dao;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Vehiculos\Model\Entity\Vehiculo;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class VehiculoDao {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

//    public function obtenerTodos() {
//        try {
//            
//           $select = $this->tableGateway->getSql()->select();
//           $select->where(array('sucursales_id_sucursal' => 1 ));
//           $select->order("id_vehiculo");
// 
//           return $this->tableGateway->selectWith($select);
//            
//        } catch (\Exception $e) {
//            \Zend\Debug\Debug::dump($e->__toString());
//            exit;
//        }
//    }

    public function obtenerTodos() {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('sucursales_id_sucursal' => 1 ));
        $select->order("id_vehiculo");
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator;
    }

    public function obtenerPorId($id) {

        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id_vehiculo' => $id));
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function eliminar(Vehiculo $vehiculo) {
        $this->tableGateway->delete(array('id_vehiculo' => $vehiculo->getId_vehiculo()));
    }

    public function guardar(Vehiculo $vehiculo) {

        $data = array(
            'patente' => $vehiculo->getPatente(),
            'cod_vehiculo' => $vehiculo->getCod_vehiculo(),
            'sucursales_id_sucursal' => $vehiculo->getSucursales_id_sucursal(),
        );

        $id = (int) $vehiculo->getId_vehiculo();

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->obtenerPorId($id)) {
                $this->tableGateway->update($data, array('id_vehiculo' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function buscarPorPatente($patente) {
        $patente = (String) $patente;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('sucursales_id_sucursal' => 1 ));
        $select->where->like('patente', '%' . $patente . '%');
        $select->order('id_vehiculo');
        
        $dbAdapter = $this->tableGateway->getAdapter();
        $resultSetPrototype = $this->tableGateway->getResultSetPrototype();

        $adapter = new DbSelect($select, $dbAdapter, $resultSetPrototype);
        $paginator = new Paginator($adapter);
        return $paginator; 
    }

//    public function buscarPorNombre($nombre) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where->like('nombre', '%' . $nombre . '%');
//        $select->order("nombre");
//        return $this->tableGateway->selectWith($select);
//    }
//
//    public function obtenerCuenta($email, $clave) {
//        $select = $this->tableGateway->getSql()->select();
//        $select->where(array('email' => $email, 'clave' => $clave,));
//        return $this->tableGateway->selectWith($select)->current();
//    }
}
