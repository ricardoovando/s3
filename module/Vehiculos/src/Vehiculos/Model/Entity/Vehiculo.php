<?php

namespace Vehiculos\Model\Entity;

class Vehiculo {

    private $id_vehiculo;
    private $patente;
    private $cod_vehiculo;
    private $sucursales_id_sucursal;

    public function __construct(
    $id_vehiculo = null, $patente = null, $cod_vehiculo = null, $sucursales_id_sucursal = null
    ) {
        $this->id_vehiculo = $id_vehiculo;
        $this->patente = $patente;
        $this->cod_vehiculo = $cod_vehiculo;
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function getId_vehiculo() {
        return $this->id_vehiculo;
    }

    public function setId_vehiculo($id_vehiculo) {
        $this->id_vehiculo = $id_vehiculo;
    }

    public function getPatente() {
        return $this->patente;
    }

    public function setPatente($patente) {
        $this->patente = $patente;
    }

    public function getCod_vehiculo() {
        return $this->cod_vehiculo;
    }

    public function setCod_vehiculo($cod_vehiculo) {
        $this->cod_vehiculo = $cod_vehiculo;
    }

    public function getSucursales_id_sucursal() {
        return $this->sucursales_id_sucursal;
    }

    public function setSucursales_id_sucursal($sucursales_id_sucursal) {
        $this->sucursales_id_sucursal = $sucursales_id_sucursal;
    }

    public function exchangeArray($data) {
        $this->id_vehiculo = (isset($data['id_vehiculo'])) ? $data['id_vehiculo'] : null;
        $this->patente = (isset($data['patente'])) ? $data['patente'] : null;
        $this->cod_vehiculo = (isset($data['cod_vehiculo'])) ? $data['cod_vehiculo'] : null;
        $this->sucursales_id_sucursal = (isset($data['sucursales_id_sucursal'])) ? $data['sucursales_id_sucursal'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

}

